minYaxisVal = maxYaxisVal = treeChartDepthLevel = treeChartDepthLevelZero = treeChartDepthLevelVal = newtreeChartWidth = orderId = 0;
order = orderDetails = lasteventDate = tradeCpty = '';
orderExtArrayVal = tradeEventTimeArrayVal = [];
arrayLevelOrder = pieChartFilterTrade = [];
IEM.OrderTab = function () {
    var orderCharts = {},
            tradeColorCodes = IEM.Mapping.getMap('TradeColorCodes'),
            tradeStatusMap = IEM.Mapping.getMap('TradeStatusMap'),
            orderFormParams = {},
            orderDetailChart, orderChartYExtents = [], orderChartXExtent, orderDetail, orderChartStream, rateExtent, orderChartRExtent, origOrderChartXExtent, orderStream, orderProvider,
            tradesForOrder, origOrderChartYExtents, orderExecutionSummary, streamsForOrder = [], maskLPMaps = {};
    var minYaxisVal, maxYaxisVal;
    function setupGetOrderByIdFormSubmit() {
        $('form[name="orderIdForm"]').submit(function (e) {
            e.preventDefault();
            orderStream = null;
            orderProvider = null;
            origOrderChartXExtent = null;
            origOrderChartYExtents = null;
            orderChartYExtents = [];
            orderCharts = {};
            rateExtent = null;
            streamsForOrder = [];
            $('.undoZoom').addClass('disabled');
            /*Sending request to get order life cycle*/
            getOrderDetailsByIdForOrderLifeCycle(function (o) {
                handleOrderDetailsResponseForOrderLifeCycle(o);
            }, function () {
                alert('Retrieving Order Life Cycle Failed. Try Again!');
            });
            getOrderDetailsById(function (o) {
                if (orderDetailChart) {
                    orderDetailChart.resetSvg();
                }
                handleOrderDetailsResponse(o);
            }, function () {
                alert('Retrieving Order Failed. Try Again!');
            });

            getOrderDetailsByIdForTreePlot(function (o) {
                handleOrderDetailsResponseForTreePlot(o);
            }, function () {
                alert('Retrieving Order Failed. Try Again!');
            });

            myVar = true;
            orderChartStream = '';
            $("#oprovider").change(function () {
                myVar = true;
            });
            $('form[name="getStreamForm"]').submit(function (e) {
                e.preventDefault();
                if (myVar) {
                    rateExtent = null;
                    orderChartStream = $('#oprovider').val();
                    getStreamForOrder(handleStreamData);
                    myVar = false;
                }

            });

            //Zoom-out
            $('.zoomOut').unbind().click(function (e) {
                handleOrderZoomOut();
            });

            $('.undoZoom').click(function (e) {
                handleUndoZoom();
            });

            $('.vlscape').click(function (e) {
                $('#tab2').click();
                $("#ccyPair").val(orderDetail.CcyPair);
                $('#ccyPair').selectpicker('refresh');
                $('#oType').val(orderDetail.OrderType).selectpicker('refresh');
                $('#oMorT').val("Taker").selectpicker('refresh');
                $("#customer").val(orderDetail.Org).selectpicker('refresh');

                var timeInMillis = orderDetail.Created - (new Date(orderDetail.Created)).getMilliseconds();
                timeInMillis = timeInMillis + (new Date(timeInMillis)).getTimezoneOffset() * 60000;

                $('.date-select').val(formatTimeString(timeInMillis - 300000) + " - " + formatTimeString(timeInMillis + 360000));
                $('form[name="getOrdersForm"]').submit();

                // $('.date-select').val("04/02/2014 02:00 - 05/02/2014 02:00")
            });

            function formatTimeString(timeString) {
                var dateObj = new Date(timeString);
                var displayDate = addZeroIfNeeded(dateObj.getMonth() + 1) + '/' + addZeroIfNeeded(dateObj.getDate()) + '/' + dateObj.getFullYear() + ' ' + addZeroIfNeeded(dateObj.getHours()) + ':' + addZeroIfNeeded(dateObj.getMinutes());
                return displayDate;
            }

            $('#orderChartContainer').on('getStreamTriggered', function (event, obj) {
                var proStr = (obj.CPTY + "/" + obj.data.TickStream).trim();
                if ($("#oprovider option[value='" + proStr + "']").length == 0)
                    return;
                $('#oprovider option[value="' + proStr + '"]').attr('selected', 'selected');
                $('#oprovider').selectpicker('refresh');
                $('form[name="getStreamForm"]').trigger("submit");
            });
        });
    }

    $('.btn-order-tree-view').click(function (e) {
        $('.orderNavigateTreeView3 div').html('');
        getOrderDetailsByIdForTreePlot(function (o) {
            handleOrderDetailsResponseForOnlyTreePlot(o);
        }, function () {
            alert('Retrieving Order Failed. Try Again!');
        });
    });

    function getOrderDetailsById(successHandler, errorHandler) {
        var orderId = $('#orderId').val().trim();
        if (!orderId) {
            alert("Order ID Missing!!");
            return;
        }
        var url = IEM.cURL + 'orderswithtrades/orderid/' + orderId;
        IEM.Utils.ajaxRequest(url, successHandler, errorHandler,
                'Getting Order & Trades for ' + orderId,
                'Getting Order and Trades...'
                );
    }
    /**
     *  This function is used to get data from API for plotting tree 
     *  view for plotting order graph in EMScope.
     *  @author    Anil    <nl.saini1@gmail.com >  
     */
    function getOrderDetailsByIdForTreePlot(successHandler, errorHandler) {
        orderId = $('#orderId').val().trim();
        if (!orderId) {
            alert("Order ID Missing!!");
            return;
        }
        var orderUrl = IEM.cURL + 'graph/navigate/' + orderId;
        IEM.Utils.ajaxRequest(orderUrl, successHandler, errorHandler, 'Getting Order Tree Navigation for ' + orderId, 'Getting Order and Trades...');
    }
    var tradeEvents = '';

    /**
     *  This function is used to get data from API for getting Order life-cycle
     *  (Price Improvement or Slippage) and plotting a bubble.
     *  @author    Anil    <nl.saini1@gmail.com >  
     */
    function getOrderDetailsByIdForOrderLifeCycle(successHandler, errorHandler) {
        orderId = $('#orderId').val().trim();
        if (!orderId) {
            alert("Order ID Missing!!");
            return;
        }
        var orderUrl = '../rest/orderswithtrades/orderdetails/' + orderId + '/';
        IEM.Utils.ajaxRequest(orderUrl, successHandler, errorHandler,
                'Getting Order Details ' + orderId, 'Getting Order Details...');
    }
    /**
     * 
     * @Author  Anil    <nl.sainil@gmail.com>
     */
    function handleOrderDetailsResponseForOrderLifeCycle(orderDetail) {
        orderDetails = orderDetail;
    }
    function handleOrderDetailsResponse(orderDetails) {
        if (orderDetails.ORDERDETAILS) {
            fillOrderDetails(orderDetails.ORDERDETAILS);
        } else {
            alert("Order Not Found");
            return;
        }
        if (orderDetails.ORDERNARRATIVE) {
            fillNarrative(orderDetails.ORDERNARRATIVE);
        }
        if (orderDetails.TRADEDETAILS) {
            fillTradeDetails(orderDetails.TRADEDETAILS);
            populateTradeFilter(orderDetails.TRADEDETAILS);
        }
        if (orderDetails.EXECUTIONSUMMARY) {
            populateExecutionSummary(orderDetails.EXECUTIONSUMMARY);
        }
        if (orderDetails.TRADERATEEVENTS) {
            tradeEvents = orderDetails.TRADERATEEVENTS
            populateTradeEvents(orderDetails.TRADERATEEVENTS);
        }
    }

    function handleOrderDetailsResponseForTreePlot(orderDetails) {
        $('.TreeChartContainer').show();
        plotOrderTreeChart(orderDetails);
        //plotOrderTreeChart3(orderDetails);
    }
    function handleOrderDetailsResponseForOnlyTreePlot(orderDetails) {
        $('.TreeChartContainer').show();
        plotOrderTreeChart(orderDetails);
        plotOrderTreeChart3(orderDetails);
    }
    function fillNarrative(narrative) {
        var narrativeEle = $('#emscopetab-2 .page-2').find('.narrative');
        narrativeEle.html(narrative.replace(/~/g, '<br/>'));
    }

    function fillOrderDetails(orders) {
        if (!orders.length) {
            alert("Order not found");
            return;
        }
        order = orders[0];
        var rate = parseFloat(order.OrderRate);
        var orderDetailFieldMap = IEM.Mapping.getMap('OrderDetailFieldMap');
        var status = IEM.Mapping.getMap('OrderStatusMap')[order.OrderStatus];
        if (status) {
            order['OrderStatus'] = status;
        }
        orderDetail = order;
        //handling incorrect values of format "Range 0.00100000 from 98.40499878"
        if (isNaN(rate)) {
            var actualRate = order.OrderRate;
            var index = actualRate.indexOf('from ');

            if (index != -1) {
                rate = parseFloat(actualRate.substr(index + 5, actualRate.length));
                order.Rate = rate;
                order.OrderRate = rate;
                order.MarketRange = actualRate.split(' ')[1];
            }
        } else {
            order.Rate = order.OrderRate;
        }

        var pCon = $('#emscopetab-2 .page-2'), col, table1Html = '', table2Html = '';
        var table1Ele = pCon.find('.orderDetails1');
        var table2Ele = pCon.find('.orderDetails2');

        var fields = Object.keys(orderDetailFieldMap);

        for (var i = 0, len = fields.length / 2; i < len; i++) {
            var field = fields[i];
            var displayVal = order[field];
            if (field == 'Created') {
                displayVal = new Date(displayVal);
            }
            if (field == 'PI') {
                if (typeof displayVal != 'undefined' && null != displayVal && 'null' != displayVal)
                {
                    displayVal = displayVal.toFixed(7);
                }
            }
            table1Html += '<tr><td class="title"><strong>' + orderDetailFieldMap[field] + '</strong></td><td>' + format(displayVal) + '</td></tr>';
        }

        for (var i = Math.ceil(fields.length / 2), len = fields.length; i < len; i++) {
            var field = fields[i];
            var displayVal = order[field];
            if (field == 'Created') {
                displayVal = new Date(displayVal);
            }
            table2Html += '<tr><td class="title"><strong>' + orderDetailFieldMap[field] + '</strong></td><td>' + format(displayVal) + '</td></tr>';
        }
        table2Html += '<tr><td colspan="2" class="moreinfotd"><span id="moreInfoSpan" title="More Order Details.." class="btn-info moreinfobtn"> More Info >></span>' +
                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style="position: relative;" target="_blank" href="https://live8.fxinside.net/grdmon/integral/Monitor/util/Search.jsp?searchType=order&txid=' + order["OrderID"] + '&userOrg=MAIN"><button title="FXGrid Monitor" style="font-size:12px;padding:2px" class="btn btn-primary"><span> GM&nbsp;<span class="glyphicon glyphicon-new-window"></span></span></button></a></td></tr>';

        table1Ele.html(table1Html);
        table2Ele.html(table2Html);
        $('#emscopetab-2 .page-1_1').show();
        $('#emscopetab-2 .setClassLeft').addClass('col-md-7 col-lg-7');
        pCon.show();
        drawOrderDetailChart(order);
        $('#moreInfoSpan').data('orderId', order["OrderID"]);
        $('#moreInfoSpan').click(function (e) {
            window.open('./orderdetails.html?orderId=' + $(this).data('orderId'));
        })
    }

    function drawTradesCptyChart() {
        var tradesCptyConfirmedChart = dc.pieChart('#tradeCptyConfirmedChartContainer');
        var tradesCptyRejectedChart = dc.pieChart('#tradeCptyRejectedChartContainer');
        var data = tradesForOrder;

        var ndx = crossfilter(data);
        var cptyDim = ndx.dimension(function (d) {
            return d.CPTY;
        });
        var confirmedGroup = cptyDim.group().reduce(
                function (p, v) {
                    if (v.TRADESTATUS == 'Confirmed') {
                        return p + v.BASEAMT;
                    } else {
                        return p;
                    }
                },
                function (p, v) {
                    if (v.TRADESTATUS == 'Confirmed') {
                        return p - v.BASEAMT;
                    } else {
                        return p;
                    }
                },
                function (p) {
                    return 0;
                }
        );

        var rejectedGroup = cptyDim.group().reduce(
                function (p, v) {
                    if (v.TRADESTATUS == 'Rejected') {
                        return p + v.BASEAMT;
                    } else {
                        return p;
                    }
                },
                function (p, v) {
                    if (v.TRADESTATUS == 'Rejected') {
                        return p - v.BASEAMT;
                    } else {
                        return p;
                    }
                },
                function (p) {
                    return 0;
                }
        );

        if (confirmedGroup.all().length > 1) {
            $('.confirmedTrades').show();
            tradesCptyConfirmedChart
                    .width(200)
                    .height(200)
                    .radius(100)
                    .dimension(cptyDim)
                    .group(confirmedGroup)
                    .label(function (d) {
                        return d.key;
                    })
                    .title(function (d) {
                        return 'Counter Party: ' + d.key + '\n' + 'Total Amount: ' + d.value;
                    }).render();
        } else {
            $('.confirmedTrades').hide();
        }

        if (rejectedGroup.all().length > 1) {
            $('.rejectedTrades').show();
            tradesCptyRejectedChart
                    .width(200)
                    .height(200)
                    .radius(100)
                    .dimension(cptyDim)
                    .group(rejectedGroup)
                    .label(function (d) {
                        return d.key;
                    })
                    .title(function (d) {
                        return 'Counter Party: ' + d.key + '\n' + 'Total Amount: ' + d.value;
                    }).render();
        } else {
            $('.rejectedTrades').hide();
        }

    }

    function fillTradeDetails(trades) {
        tradesForOrder = trades;
        var tableOptions = {}, tradeId, trade, status;

        tableOptions.aaSorting = [[1, "asc"]];
        tableOptions.columns = [
            {'sTitle': 'Trade ID'},
            {'sTitle': 'Exec Time'},
            {'sTitle': 'Rate'},
            {'sTitle': 'Amount'},
            {'sTitle': 'CPTY'},
            {'sTitle': 'Stream'},
            {'sTitle': 'Status'},
            {'sTitle': 'Buy/Sell'},
            {'sTitle': 'Type'},
            {'sTitle': 'Org'}
        ];

        tableOptions.fnRowCallback = function (nRow, aaData, iDisplayIndex) {
            var className = '';
            if (aaData[6] == 'Confirmed') {
                className = 'success';
            } else if (aaData[6] == 'Rejected') {
                className = 'rejected';
            } else if (aaData[6] == 'Failed') {
                className = 'failed';
            }
            $(nRow).addClass(className);
        };

        var tableData = [];
        var buySell = '';

        for (var i = 0, len = tradesForOrder.length; i < len; i++) {
            trade = tradesForOrder[i];
            tradeId = trade.TRADEID;
            status = tradeStatusMap[trade.TRADESTATUS] || trade.TRADESTATUS;
            if (trade.BUYSELL) {
                buySell = (trade.BUYSELL == 'B' || trade.BUYSELL == "Buy") ? "Buy" : "Sell";
            } else
            {
                buySell = "-";
            }
            var tickStream;
            if (typeof trade.MaskLP != 'undefined' && null != trade.MaskLP && 'null' != trade.MaskLP)
            {
                tickStream = trade.MaskLP;
            }
            else
            {
                tickStream = trade.TickStream;
            }
            tableData.push([
                '<a style="cursor: pointer" title="Trade Details" class="tradeLink">' + tradeId + '</a>' + '&nbsp;&nbsp;&nbsp;' + '<a target="_blank" href="https://live8.fxinside.net/grdmon/integral/Monitor/util/Search.jsp?searchType=deal&txid=' + tradeId + '&userOrg=MAIN"><span title="FXGrid Monitor" class="glyphicon glyphicon-new-window"></span></a>',
                new Date(trade.EXECTIME),
                trade.RATE,
                '<td>' + trade.BASEAMT,
                trade.CPTY,
                tickStream,
                status,
                buySell,
                trade.TYPE,
                trade.ORG
            ]);

            /*create streams for order list to show Combination of cpty and stream exists in show in orderStreams*/
            if (trade.STREAM && trade.CPTY) {
                if ($.inArray(trade.CPTY + ' - ' + trade.TickStream, streamsForOrder) == -1) {
                    if (typeof trade.MaskLP != 'undefined' && null != trade.MaskLP && 'null' != trade.MaskLP)
                    {
                        maskLPMaps[trade.CPTY + ' - ' + trade.TickStream] = trade.MaskLP;
                    }
                    streamsForOrder.push(trade.CPTY + ' - ' + trade.TickStream);
                }
            }
        }
        IEM.Mapping.setMap('MaskStreams', maskLPMaps);
        tableData.sort(function (a, b) {
            return Number(a[1]) - Number(b[1]);
        });

        if (typeof trade != "undefined") {
            IEM.Utils.createTable('oTTradesTable', 'tTable', 'oTTradesTableContainer', tableData, tableOptions);

            $('#oTTradesTable').on('click', '.tradeLink', {orderID: trade.ORDERID, tradeID: trade.TRADEID}, function (e) {
                var tradeId = this.text;
                var url = './tradeDetails.html?orderId=' + e.data.orderID + '&tradeId=' + tradeId;
                /*window.open('./tradeDetails.html?orderId='+$(this).html(),'&tradeId='+$(this).html()); */
                window.open(url);
            });

            fillStreamsContainer();
            drawTradeDetailChart(tradesForOrder);
        }
    }

    function populateTradeEventsDot(tradeEvents, resize)
    {
        $('body').on('click', '.chart-body g.node', function (e) {
            if (!e.ctrlKey && !e.altKey) {
                trade_id = $(this).find('title').text();
                var containsTrade = trade_id.indexOf('Trade ID') >= 0;

                if (containsTrade) {
                    var SelectedTradeId = trade_id.substring(34, 48);
                    var count = 0
                    $.each(tradeEvents, function (key, value) {
                        var traidId = value.split(':')[0]
                        if (traidId.trim() == SelectedTradeId.trim()) {
                            count++;
                            var data = [
                                {
                                    "EventKey": value.split(':')[3],
                                    "EventClassification": value.split(':')[3],
                                    "Rate": value.split(':')[2],
                                    "Timestamp": parseInt(value.split(':')[1].trim()),
                                    "TradeId": value.split(':')[0]
                                }]
                            addRateEventLog(data, count);
                        }
                    });
                }
                d3.selectAll('#orderChartContainer g.node').classed('deselected', false);
                d3.selectAll('#orderChartContainer g.node').classed('selected', false);
            }
        });
    }

    /**
     * function for populating rate events.
     *
     * @param tradeEvents
     */
    var trade_id = '';
    function populateTradeEvents(tradeEvents, resize)
    {
        $('body').on('click', '.chart-body g.node', function (e) {
            if (!e.ctrlKey && !e.altKey) {
                trade_id = $(this).find('title').text();
                var containsTrade = trade_id.indexOf('Trade ID') >= 0;
                if (containsTrade) {
                    trade_id.split('Trade ID:')[1]
                    // console.log(trade_id.split('Trade ID:')[1].split('Rate')[0]);
                    //var SelectedTradeId = trade_id.substring(34, 48);
                    var SelectedTradeId = trade_id.split('Trade ID:')[1].split('Rate')[0]
                    var count = 0
                    $.each(tradeEvents, function (key, value) {
                        var traidId = value.split(':')[0]
                        if (traidId.trim() == SelectedTradeId.trim()) {
                            count++;
                            var data = [
                                {
                                    "EventKey": value.split(':')[3],
                                    "EventClassification": value.split(':')[3],
                                    "Rate": value.split(':')[2],
                                    "Timestamp": parseInt(value.split(':')[1].trim()),
                                    "TradeId": value.split(':')[0]
                                }]
                            addRateEventLog(data, count);
                        }
                    });
                }
                removePopulateTradeFilterLines();
                d3.selectAll('#orderChartContainer g.node').classed('deselected', false);
                d3.selectAll('#orderChartContainer g.node').classed('selected', false);
            }
        });
    }

    function addRateEventLog(responseObj, count) {
        var ss = responseObj;
        if (typeof resize == 'undefined') {
            resize = true;
        }
        $.each(responseObj, function (index, value) {

            if (value.EventClassification === 'OrderRecivedByServer' || value.EventClassification === 'OrderMatchedByServer' ||
                    value.EventClassification === 'TradeRequestSentToProvider' || value.EventClassification === 'TradeRejectReceivedFromProvider' ||
                    value.EventClassification === 'TradeVerifiedReceivedFromProvider' || value.EventClassification === 'RateAggregatedByServer') {
                var diff = maxYaxisVal - minYaxisVal;
                // maxYaxisVal = 2 * maxYaxisVal;
                //minYaxisVal = minYaxisVal ;
                plotParticularTradeEventDetails(value, resize, maxYaxisVal, minYaxisVal, count);
            }

        });
        d3.selectAll('#orderChartContainer g.node').classed('deselected', false);
        d3.selectAll('#orderChartContainer g.node').classed('selected', false);
        if (orderCharts['tier1AskGraph']) {
            orderCharts.tier2BidGraph.legendToggle("Bid Price - Tier 2");
            orderCharts.tier3BidGraph.legendToggle("Bid Price - Tier 3");
            orderCharts.tier2AskGraph.legendToggle("Ask Price - Tier 2");
            orderCharts.tier3AskGraph.legendToggle("Ask Price - Tier 3");
            if (orderDetail.BuySell == 'Buy') {
                // Buy turn off bid
                orderCharts.tier1BidGraph.legendToggle("Bid Price - Tier 1");
                $('g.dc-legend g.dc-legend-item').each(function () {
                    var legend_text = $(this).find('text').text();
                    if (legend_text.toLowerCase().indexOf("ask price - tier 1") >= 0) {
                        $(this).css({'fill-opacity': '1'});
                    }
                    else if (legend_text.toLowerCase().indexOf("price - tier") >= 0) {
                        $(this).css({'fill-opacity': '0.5'});
                    }
                })
            } else {
                // sell so turn off ask
                orderCharts.tier1AskGraph.legendToggle("Ask Price - Tier 1");
                $('g.dc-legend g.dc-legend-item').each(function () {
                    var legend_text = $(this).find('text').text();
                    if (legend_text.toLowerCase().indexOf("bid price - tier 1") >= 0) {
                        $(this).css({'fill-opacity': '1'});
                    }
                    else if (legend_text.toLowerCase().indexOf("price - tier") >= 0) {
                        $(this).css({'fill-opacity': '0.5'});
                    }
                })

            }
        }
    }


    function fillStreamsContainer() {
        var allStreams = IEM.Mapping.getMap('ProviderStream').slice(0);
        var maskStreamsMap;
        if (IEM.Mapping.getMap('MaskStreams'))
        {
            maskStreamsMap = IEM.Mapping.getMap('MaskStreams');
        }

        var oStreams = streamsForOrder, arrayIndex, oOptGroup = '', aOptGroup = '', oStream, aStream;
        $('#oprovider').html('');
        if (oStreams.length > 0) {
            oOptGroup = '<optgroup label="Order Streams">';

            for (var i = 0, len = oStreams.length; i < len; i++) {
                oStream = oStreams[i];
                arrayIndex = $.inArray(oStream, allStreams);
                if (arrayIndex != -1) {
                    allStreams.splice(arrayIndex, 1);
                    oStream = oStream.split(' - ');
                    if (maskStreamsMap[oStream])
                    {
                        var displayName = oStream[0] + ' - ' + maskStreamsMap[oStream];
                        oOptGroup += '<option value="' + oStream[0] + '/' + oStream[1] + '">' + displayName + '</option>'
                    }
                    else
                    {
                        oOptGroup += '<option value="' + oStream[0] + '/' + oStream[1] + '">' + oStreams[i] + '</option>';
                    }
                }
            }
            oOptGroup += '</optgroup>';
        }
        aOptGroup = '<optgroup label="Other Streams">';
        for (var i = 0, len = allStreams.length; i < len; i++) {
            aStream = allStreams[i].split(' - ');
            aOptGroup += '<option value="' + aStream[0] + '/' + aStream[1] + '">' + allStreams[i] + '</option>';
        }
        aOptGroup += '</optgroup>';
        $('#oprovider').html(oOptGroup + aOptGroup).selectpicker('refresh');
    }

    function populateExecutionSummary(summary) {
        orderExecutionSummary = summary
        var summaryInfo = summary;
        var tableOptions = {};
        tableOptions.sDom = 'r<"scroller"t>';
        tableOptions.bPaginate = false;
        tableOptions.sScrollY = '175px';
        tableOptions.aaSorting = [[2, "desc"]];

        tableOptions.columns = [
            {'sTitle': 'Provider'},
            {'sTitle': 'Confirmed Trades'},
            {'sTitle': 'Confirmed Trades Amount'},
            {'sTitle': 'Rejected Trades'},
            {'sTitle': 'Rejected Trades Amount'},
            {'sTitle': 'Cancelled Trades'},
            {'sTitle': 'Cancelled Trades Amount'},
            {'sTitle': 'Failed Trades'},
            {'sTitle': 'Failed Trades Amount'},
            {'sTitle': 'Weighted Average Rate (WAR)'}
        ];
        var tableData = [];

        for (var provider in summaryInfo) {
            var summaryRow = summaryInfo[provider];
            tableData.push([
                provider,
                summaryRow.confirmedTradeCount,
                getFormattedAmount(summaryRow.comfirmedAmount),
                summaryRow.rejectedTradesCount,
                getFormattedAmount(summaryRow.rejectedTradesAmount),
                summaryRow.cancelledTradeCount,
                getFormattedAmount(summaryRow.cancelledTradeAmount),
                summaryRow.failedTradesCount,
                getFormattedAmount(summaryRow.failedTradesAmount),
                summaryRow.weightedAvgRate
            ]);
        }

        if (tableData.length > 1) {
            $('.execSummaryContainer').show();
            IEM.Utils.createTable('execSummaryTable', 'execSummaryTable', 'execSummary', tableData, tableOptions);
        } else {
            $('.execSummaryContainer').hide();
        }
    }

    function handleBenchmarkResponse(benchmark) {
        var tableOptions = {}, tradeId, trade;

        tableOptions.columns = [
            {'sTitle': 'Time'},
            {'sTitle': 'Median'},
            {'sTitle': 'Range'}
        ];
        var tableData = [], rate;

        for (var i = 0, len = benchmark.length; i < len; i++) {
            rate = benchmark[i];

            tableData.push([
                new Date(rate.timeStamp),
                rate.medianMidPrice,
                rate.midPriceRange
            ]);
        }

        tableData.sort(function (a, b) {
            return Number(a[0]) - Number(b[0]);
        });

        IEM.Utils.createTable('oTBenchmarkTable', 'bTable', 'oTBenchmarkTableContainer', tableData, tableOptions);
        drawBenchmarkAroundOrderGraph(benchmark);


    }

    function getStreamForOrder(successHandler) {
        if (!orderChartStream) {
            return;
        }
        var startTime, endTime, provider, stream, ccyPair;
        startTime = new Date(orderChartXExtent[0]).getTime();
        endTime = new Date(orderChartXExtent[1]).getTime();
        provider = orderChartStream.split('/')[0];
        stream = orderChartStream.split('/')[1];
        ccyPair = orderDetail.CcyPair.replace('/', '');
        var url;
        var startTimeAdj = new Date(orderDetail.Created).getTime() - 5000;
        var endTimeAdj = new Date(orderDetail.Created).getTime() + 5000;

        if (endTime - startTime < 10)
        {
            url = IEM.cURL + "rates1/";
            url += provider + '/' + stream + '/' + ccyPair + '/' + startTimeAdj + '/' + endTimeAdj;
        }
        else
        {
            url = IEM.cURL + "rates1/";
            url += provider + '/' + stream + '/' + ccyPair + '/' + startTime + '/' + endTime;
        }

        IEM.Utils.ajaxRequest(url, function (o) {
            orderStream = stream;
            orderProvider = provider;
            if (o.type == 'error' || o.type == 'warning') {
                alert(o.type + ' : ' + o.errorMessage);
                filterTradeByStream(); // calling filter trade function even of get stream request is responding with error 
                return;
            }
            successHandler.call(this, o);
        },
                function () {
                    alert("Problem fetching Tick Data - Call Failed.");
                }, 'Stream Data for ' + provider + ' - ' + stream, "Getting Tick Data..."
                );

    }

    function getProfileForOrder(successHandler) {
        if (!orderChartStream) {
            return;
        }
        var startTime, endTime, provider, stream, ccyPair;
        startTime = new Date(orderChartXExtent[0]).getTime();
        endTime = new Date(orderChartXExtent[1]).getTime() + 1000 * 5;

        provider = orderChartStream.split('/')[0];
        stream = orderChartStream.split('/')[1];
        ccyPair = orderDetail.CcyPair.replace('/', '');

        var url = IEM.cURL + "profile/";
        url += provider + '/' + stream + '/' + ccyPair + '/' + startTime + '/' + endTime;
        profileUrl = url;
        IEM.Utils.ajaxRequest(url, function (o) {
            orderStream = stream;
            orderProvider = provider;
            if (o.type == 'error' || o.type == 'warning') {
                alert(o.type + ' : ' + o.errorMessage);
                return;
            }
            successHandler.call(this, o);
        },
                function () {
                    //alert("Problem fetching Profile Data - Call Failed.");
                }, 'Profile Data for ' + provider + ' - ' + stream, "Getting Profile Data..."
                );

    }

    function roundOff(val) {
        if (val != undefined)
        {
            return  val.toFixed(7);
        }
        else
        {
            return "-";
        }
    }

    function filterTradeByStream() {
        if ($("#filterByStreamToggle").is(':checked') && orderProvider && orderStream && tradesForOrder) {
            var tradesFilter = crossfilter(tradesForOrder);
            var tradesCptyDim = tradesFilter.dimension(function (d) {
                return d.CPTY;
            });
            var tradesStreamDim = tradesFilter.dimension(function (d) {
                return d.TickStream;
            });
            tradesCptyDim.filter(orderProvider);
            tradesStreamDim.filter(orderStream);

            drawTradeDetailChart(tradesStreamDim.top(Infinity), false, false);
        }
    }

    function handleStreamData(rates) {
        if (!rates.length) {
            alert('No Data Available');
            return;
        }
        fillRatesTable(rates);
        var ndx = crossfilter(rates);
        var tierDim = ndx.dimension(function (d) {
            return d.lvl;
        });
        var tier1AskData, tier2AskData, tier3AskData, tier1BidData, tier2BidData, tier3BidData;
        tier1AskData = tierDim.filter(1).top(Infinity);
        tier1BidData = tierDim.filter(1).top(Infinity);
        tier2AskData = tierDim.filter(2).top(Infinity);
        tier2BidData = tierDim.filter(2).top(Infinity);
        tier3AskData = tierDim.filter(3).top(Infinity);
        tier3BidData = tierDim.filter(3).top(Infinity)

        tier1AskData = addAskInactiveRates(tier1AskData);
        tier2AskData = addAskInactiveRates(tier2AskData);
        tier3AskData = addAskInactiveRates(tier3AskData);
        tier1BidData = addBidInactiveRates(tier1BidData);
        tier2BidData = addBidInactiveRates(tier2BidData);
        tier3BidData = addBidInactiveRates(tier3BidData);

        var ryex = mergeExtents([
            /*Bid Data seta*/
            d3.extent(tier1BidData, function (d) {
                return d.bid_price;
            }),
            d3.extent(tier2BidData, function (d) {
                return d.bid_price;
            }),
            d3.extent(tier3BidData, function (d) {
                return d.bid_price;
            }),
            /*Ask data Sets*/
            d3.extent(tier1AskData, function (d) {
                return d.ask_price;
            }),
            d3.extent(tier2AskData, function (d) {
                return d.ask_price;
            }),
            d3.extent(tier3AskData, function (d) {
                return d.ask_price;
            })
        ]);

        var t1B = getRateCFGroupDim(tier1BidData);
        var t2B = getRateCFGroupDim(tier2BidData);
        var t3B = getRateCFGroupDim(tier3BidData);
        var t1A = getRateCFGroupDim(tier1AskData);
        var t2A = getRateCFGroupDim(tier2AskData);
        var t3A = getRateCFGroupDim(tier3AskData);

        /*Bid Line Charts*/
        orderCharts['tier1BidGraph'] = dc.lineChart(orderDetailChart)
                .dimension(t1B[0])
                .group(t1B[1], "Bid Price - Tier 1")
                .colors('#d62728')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.rateData.bid_price;
                })
                .renderDataPoints(true)
                .interpolate('step-after')
                .renderlet(function (chart) {
                    dc.events.trigger(function () {
                        //console.log(this.filters())
                    });
                });
        orderCharts['tier2BidGraph'] = dc.lineChart(orderDetailChart)
                .dimension(t2B[0])
                .group(t2B[1], "Bid Price - Tier 2")
                .colors('#9467bd')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.rateData.bid_price;
                })
                .renderDataPoints(true)
                .interpolate('step-after');

        orderCharts['tier3BidGraph'] = dc.lineChart(orderDetailChart)
                .dimension(t3B[0])
                .group(t3B[1], "Bid Price - Tier 3")
                .colors('#8c564b')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.rateData.bid_price;
                })
                .renderDataPoints(true)
                .interpolate('step-after');

        /*Ask Line Charts*/
        orderCharts['tier1AskGraph'] = dc.lineChart(orderDetailChart)
                .dimension(t1A[0])
                .group(t1A[1], "Ask Price - Tier 1")
                .colors('#1f77b4')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.rateData.ask_price;
                })
                .renderDataPoints(true)
                .interpolate('step-after');

        orderCharts['tier2AskGraph'] = dc.lineChart(orderDetailChart)
                .dimension(t2A[0])
                .group(t2A[1], "Ask Price - Tier 2")
                .colors('#ff7f0e')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.rateData.ask_price;
                })
                .renderDataPoints(true)
                .interpolate('step-after');

        orderCharts['tier3AskGraph'] = dc.lineChart(orderDetailChart)
                .dimension(t3A[0])
                .group(t3A[1], "Ask Price - Tier 3")
                .colors('#2ca02c')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.rateData.ask_price;
                })
                .renderDataPoints(true)
                .interpolate('step-after');

        rateExtent = ryex;

        orderDetailChart.compose(getChartArrFromObj(orderCharts))
                .y(d3.scale.linear().domain(getOrderChartYExtent()));

        orderDetailChart.render();
        orderCharts.tier2BidGraph.legendToggle("Bid Price - Tier 2");
        orderCharts.tier3BidGraph.legendToggle("Bid Price - Tier 3");
        orderCharts.tier2AskGraph.legendToggle("Ask Price - Tier 2");
        orderCharts.tier3AskGraph.legendToggle("Ask Price - Tier 3");

        if (orderDetail.BuySell == 'Buy') {
            // Buy turn off bid
            orderCharts.tier1BidGraph.legendToggle("Bid Price - Tier 1");
            $('g.dc-legend g.dc-legend-item').each(function () {
                var legend_text = $(this).find('text').text();
                if (legend_text.toLowerCase().indexOf("ask price - tier 1") >= 0) {
                    $(this).css({'fill-opacity': '1'});
                }
                else if (legend_text.toLowerCase().indexOf("price - tier") >= 0) {
                    $(this).css({'fill-opacity': '0.5'});
                }
            })
        } else {
            // sell so turn off ask
            orderCharts.tier1AskGraph.legendToggle("Ask Price - Tier 1");
            $('g.dc-legend g.dc-legend-item').each(function () {
                var legend_text = $(this).find('text').text();
                if (legend_text.toLowerCase().indexOf("bid price - tier 1") >= 0) {
                    $(this).css({'fill-opacity': '1'});
                }
                else if (legend_text.toLowerCase().indexOf("price - tier") >= 0) {
                    $(this).css({'fill-opacity': '0.5'});
                }
            })

        }
        var containsTrade = trade_id.indexOf('Trade ID') >= 0;
        if (containsTrade) {
            var SelectedTradeId = trade_id.substring(34, 48);
            var count = 0
            $.each(tradeEvents, function (key, value) {
                var traidId = value.split(':')[0]
                if (traidId.trim() == SelectedTradeId.trim()) {
                    count++;
                    var data = [
                        {
                            "EventKey": value.split(':')[3],
                            "EventClassification": value.split(':')[3],
                            "Rate": value.split(':')[2],
                            "Timestamp": parseInt(value.split(':')[1].trim()),
                            "TradeId": value.split(':')[0]
                        }]
                    addRateEventLog(data, count);
                }
            });
        }
        d3.selectAll('#orderChartContainer g.node').classed('deselected', false);
        d3.selectAll('#orderChartContainer g.node').classed('selected', false);




        filterTradeByStream();
        removePopulateTradeFilterLines();
        //getProfileForOrder(handleProfileData);
    }

    function handleProfileData(profileData)
    {
        var tableOptions = {}, orderId, order;

        tableOptions.sDom = 'r<"scroller"t>';
        tableOptions.bPaginate = false;
        //tableOptions.sScrollY = '600px';
        //tableOptions.scrollY = 200;
        tableOptions.scrollX = true;
        tableOptions.bScrollCollapse = true;
        //tableOptions.sScrollX= '100%';
        //tableOptions.sScrollXInner='110%';


        tableOptions.columns = [
            {'sTitle': 'Timestamp'},
            {'sTitle': 'Tier'},
            {'sTitle': 'Has Data'},
            {'sTitle': 'Two Sided Tick Count'},
            {'sTitle': 'Mid Price Mean'},
            {'sTitle': 'Spread Average'},
            {'sTitle': 'Spread Range'},
            {'sTitle': 'Last Tick Tmstmp'},
            {'sTitle': 'Last Tick Age'},
            {'sTitle': 'Bid Size Average'},
            {'sTitle': 'Ask Size Average'},
            {'sTitle': 'Bid Price Maximum'},
            {'sTitle': 'Bid Price Minimum'},
            {'sTitle': 'Ask Price Maximum'},
            {'sTitle': 'Ask Price Minimum'},
            {'sTitle': 'Last Active Bid'},
            {'sTitle': 'Last Active Ask'},
            {'sTitle': 'active Tick Count'},
            {'sTitle': 'Inactive Tick Count'},
            {'sTitle': 'Number Of Bad Data'},
            {'sTitle': 'Last Declared Status'}

            //bid Size, then Bid Price, then Ask Price and then the Ask Size.
        ];
        var tableData = [];

        for (var i = 0, len = profileData.length; i < len; i++) {
            var profileD = profileData[i];

            tableData.push([
                new Date(profileD.time),
                profileD.tier,
                profileD.hasDataForCurrentSecond,
                profileD.twoSidedTickCount,
                roundOff(profileD.midPriceMean),
                roundOff(profileD.spreadAverage),
                roundOff(profileD.spreadRange),
                new Date(profileD.lastTickTimeStamp),
                profileD.lastTickAge,
                (profileD.averageBidSize).toFixed(2),
                format(profileD.averageAskSize),
                format(profileD.maxBidPrice),
                format(profileD.minBidPrice),
                format(profileD.maxAskPrice),
                format(profileD.minAskPrice),
                format(profileD.lastActiveBid),
                format(profileD.lastActiveAsk),
                format(profileD.activeTickCount),
                format(profileD.inactiveTickCount),
                format(profileD.numberOfBadRecords),
                format(profileD.lastDeclaredStatus)

            ]);


        }

        IEM.Utils.createTable('oTProfileTable', 'rTable', 'oTProfileTableContainer', tableData, tableOptions);
    }

    function format(val) {
        return val != 0 ? val : " ";
    }
    function fillRatesTable(rates) {
        var tableOptions = {}, orderId, order;

        tableOptions.columns = [
            {'sTitle': 'Time'},
            {'sTitle': 'Bid Size'},
            {'sTitle': 'Bid Price'},
            {'sTitle': 'Ask Price'},
            {'sTitle': 'Ask Size'},
            {'sTitle': 'Status'},
            {'sTitle': 'GUID', 'sWidth': '25%'},
            {'sTitle': 'Tier'}

            //bid Size, then Bid Price, then Ask Price and then the Ask Size.
        ];
        var tableData = [];

        var validTimesMap = {};
        var ratesMap = {};

        for (var j = 0, tradelen = tradesForOrder.length; j < tradelen; j++)
        {
            var trade = tradesForOrder[j];
            var stream = orderChartStream.split('/')[1];
            if (trade.TickStream == stream)
            {
                for (var i = 0, len = rates.length; i < len; i++)
                {
                    var rate = rates[i];
                    if (trade.RATE == rate.ask_price)
                    {
                        ratesMap[rate.ask_price] = rate.ask_price;
                        break;
                    }
                    else if (trade.RATE == rate.bid_price)
                    {
                        ratesMap[rate.bid_price] = rate.bid_price;
                        break;
                    }

                    if (rate.tmstmp == trade.EXECTIME)
                    {
                        // add it to the map of timestamps
                        validTimesMap[rate.tmstmp] = rate.tmstmp;
                        validTimesMap[rates[i - 1].tmstmp] = rates[i - 1].tmstmp;
                        break;
                    }
                    else if (rate.tmstmp > trade.EXECTIME)
                    {
                        // get the previous rate timestamp and add it
                        validTimesMap[rates[i - 1].tmstmp] = rates[i - 1].tmstmp;
                        break;
                    }
                }
            }
        }

        tableOptions.iDisplayLength = 100;
        tableOptions.fnRowCallback = function (nRow, aaData, iDisplayIndex) {
            var className = '';
            if (validTimesMap[+aaData[0]] || ratesMap[aaData[2]] || ratesMap[aaData[3]])
            {
                className = 'highlightrate';
            }

            $(nRow).addClass(className);
        };

        for (var i = 0, len = rates.length; i < len; i++)
        {
            var rate = rates[i];
            tableData.push([
                new Date(rate.tmstmp),
                rate.bid_size,
                rate.bid_price,
                rate.ask_price,
                rate.ask_size,
                rate.status,
                rate.guid,
                rate.lvl
            ]);
        }

        IEM.Utils.createTable('oTRatesTable', 'rTable', 'oTRatesTableContainer', tableData, tableOptions);
    }

    function getRateCFGroupDim(data) {
        var rdx = crossfilter(data);
        var rDim = rdx.dimension(function (d) {
            return d.date;
        });
        var topObj = rDim.top(1);
        orderChartXExtent = convertDateExtent(orderChartXExtent);
        if (topObj.length && orderChartXExtent && topObj.tmstmp != orderChartXExtent[1]) {
            var extendedRate = $.extend({}, topObj[0]);
            extendedRate.tmstmp = orderChartXExtent[1];
            extendedRate.date = new Date(extendedRate.tmstmp);
            rdx.add([extendedRate]);
        }
        return [rDim, rDim.group().reduce(
                    function (p, v) {
                        if (p.rateData == null) {
                            p.rateData = v;
                        }
                        return p;
                    },
                    function (p, v) {
                    },
                    function (p, v) {
                        return {rateData: null}
                    }
            )];
    }

    function addAskInactiveRates(data) {
        var lvlAskPrice;
        var newData = [];

        data.forEach(function (d) {
            d.date = new Date(d.tmstmp);
            if (d.ask_price != 0) {
                newData.push(d);
                lvlAskPrice = d.ask_price;
            }
            if (d.status == 'InActive') {
                if (lvlAskPrice) {
                    d.ask_price = lvlAskPrice;
                    newData.push(d);
                }
            }
        });
        return newData;
    }

    function addBidInactiveRates(data) {
        var lvlBidPrice;
        var newData = [];

        data.forEach(function (d) {
            d.date = new Date(d.tmstmp);
            if (d.bid_price != 0) {
                newData.push(d);
                lvlBidPrice = d.bid_price;
            }
            if (d.status == 'InActive') {
                if (lvlBidPrice) {
                    d.bid_price = lvlBidPrice;
                    newData.push(d);
                }
            }
        });
        return newData;
    }

    function getBenchmarkRates(timeFrame, successHandler, callBack) {
        console.log(timeFrame);
        var startTime = new Date(timeFrame[0]).getTime();
        var endTime = new Date(timeFrame[1]).getTime();
        var ccyPair = orderDetail.CcyPair.replace('/', '');
        var url;

        var startTimeAdj = new Date(orderDetail.Created).getTime() - 5000;
        var endTimeAdj = new Date(orderDetail.Created).getTime() + 5000;
        /* if (endTime - startTime < 2000) {
         startTime -= 1000;
         endTime += 1000;
         }  */

        if (endTime - startTime > 600000) {
            url = IEM.cURL + 'benchmarks1/sample/600/ccypair/';
        } else {
            url = IEM.cURL + 'benchmarks1/ccypair/';
        }
        if (endTime - startTime < 10)
        {
            url += ccyPair + '/fromtime/' + startTimeAdj + '/totime/' + endTimeAdj + '/tier/1';
            //url += ccyPair + '/fromtime/' + startTime + '/totime/' + endTime + '/tier/1';
        }
        else
        {
            url += ccyPair + '/fromtime/' + startTime + '/totime/' + endTime + '/tier/1';
        }

        IEM.Utils.ajaxRequest(url, function (o) {
            successHandler.call(this, o);
            if (typeof callBack != 'undefined') {
                callBack.apply(this, []);
            }
        },
                function () {
                    alert("Problem in retrieving Benchmark Rates");
                }, 'Benchmark Rates', 'Getting Benchmark Rates...');
    }

    function drawOrderDetailChart(order) {

        orderDetailChart = dc.compositeChart('#orderChartContainer');
        order['date'] = new Date(order.Created);
        var crf = crossfilter([order]);
        var dim = crf.dimension(function (d) {
            return d.date;
        });
        var orderGroup = dim.group().reduce(
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.OrderID = v.OrderID;
                    p.OrderAmt = v.OrderAmt;
                    p.OrderRate = v.OrderRate;
                    p.Date = v.date;
                    return p;
                },
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.OrderID = v.OrderID;
                    p.OrderAmt = v.OrderAmt;
                    p.OrderRate = v.OrderRate;
                    p.Date = v.date;
                    return p;
                },
                function (p, v) {
                    return {
                        OrderID: 0,
                        OrderAmt: 0,
                        OrderRate: 0,
                        Date: 0,
                        data: null
                    }
                }
        );
        orderChartRExtent = [0, 100];

        orderChartYExtents.push([parseFloat(order.Rate), parseFloat(order.Rate)]);
        orderChartXExtent = [order.Created, order.Created];
        var mainZoomChart = dc.lineChart('#ordzoomChart');
        mainZoomChart
                .width(1300)
                .height(50)
                .renderArea(true)
                .margins({top: 0, right: 150, bottom: 20, left: 72})
                .dimension(dim)
                .group(orderGroup)
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.data.Rate;
                })
                .x(d3.time.scale().domain([orderChartXExtent[0] - 2500, orderChartXExtent[0] + 2500]))
                .y(d3.scale.linear().domain(orderChartYExtents))
                .xAxis().tickFormat(function (d) {
            return d3.time.format.utc("%H:%M:%S.%L")(d);
        });

        mainZoomChart.render();
        orderCharts['orderChart'] = dc.bubbleChart(orderDetailChart)
                .dimension(dim)
                .group(orderGroup, "Orders")
                .colors('black')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.data.Rate;
                })
                .radiusValueAccessor(function (p) {
                    return 10;
                })
                .maxBubbleRelativeSize(0.005)
                .r(d3.scale.linear().domain(orderChartRExtent))
                .renderLabel(false);

        orderDetailChart
                .width(1300)
                .height(600)
                .transitionDuration(250)
                .margins({top: 30, right: 190, bottom: 20, left: 60})
                .yAxisLabel("Rate")
                .x(d3.time.scale().domain(adjustXExtent(orderChartXExtent)))
                .y(d3.scale.linear().domain(adjustYExtent(getOrderChartYExtent())))
                .renderHorizontalGridLines(true)
                .renderVerticalGridLines(true)
                .rangeChart(mainZoomChart)
                .on("zoomed", function (chart) {
                    //console.log("Zoom started");
                    //handleExtent(chart);
                })
                .on("zoomend", function (chart) {
                    //handleOrderZoomOut();
                    //console.log("Zoom ended");
                    var timeParams = chart.rangeChart().filter();
                    timeParams = [timeParams[0].getTime(), timeParams[1].getTime()];
                    handleSliderZoom(timeParams);
                })
                .elasticY(false)
                .brushOn(false)
                .renderTitle(true)
                .title(mainChartTitle)
                .compose(getChartArrFromObj(orderCharts))
                .legend(dc.legend().x(1140).y(50).itemWidth(130).legendWidth(150).gap(5))
                .xAxis().tickFormat(function (d) {
            return d3.time.format.utc("%H:%M:%S.%L")(d);
        });
        getBenchmarkRates(orderChartXExtent, handleBenchmarkResponse);
        orderDetailChart.render();
        var adjustXExtVal = adjustXExtent(orderChartXExtent);
        var adjustYExtVal = adjustYExtent(getOrderChartYExtent());

        if (typeof orderDetails[0] != 'undefined') {
            setTimeout(function () {
                plotOrderLifeCycleDot(adjustXExtVal, adjustYExtVal);
            }, 1000);
        }
    }

    function drawTradeDetailChart(trades, resize, getBenchmark) {
        if (typeof resize == 'undefined') {
            resize = true;
        }
        if (typeof getBenchmark == 'undefined') {
            getBenchmark = true;
        }
        var data = {
            'Confirmed': [],
            'Rejected': [],
            'Failed': [],
            'Cancelled': [],
            'Verified': [],
            'Other': []
        };

        for (var chart in orderCharts) {
            if (chart.match('trades-')) {
                delete orderCharts[chart];
            }
        }

        if (!trades.length && orderDetailChart) {
            orderDetailChart.compose(getChartArrFromObj(orderCharts)).render();
        }

        for (var i = 0, len = trades.length; i < len; i++) {
            var trade = trades[i];
            trade['key'] = trade.TRADEID;
            trade['Type'] = 'Trades';
            trade.TRADESTATUS = tradeStatusMap[trade.TRADESTATUS] ? tradeStatusMap[trade.TRADESTATUS] : trade.TRADESTATUS;
            var status = trade.TRADESTATUS;
            trade['date'] = new Date(trade.EXECTIME);
            if (status == 'Confirmed' || status == 'Rejected' || status == 'Failed' || status == 'Cancelled' || status == 'Verified') {
                data[status].push(trade);
            } else {
                data['Other'].push(trade);
            }
        }

        for (var tradeType in data) {
            if (data[tradeType].length) {
                plotTradeDetails(data[tradeType], tradeType, resize);
            }
            /*orderDetailChart.renderlet(function(chart) {
             var children = chart.children();
             for (var i=0, len=children.length; i<len; i++) {
             var child = children[i];
             if (child._groupName && child._groupName.match('Trades')) {
             var nodes = child.chartBodyG().selectAll('circle.bubble')[0];
             detectCollisions(nodes);
             }
             }
             }).render();*/
        }

        if (resize) {
            orderChartXExtent = convertDateExtent(adjustXExtent(orderChartXExtent));
            orderChartXExtent = [IEM.Utils.roundTStoSec(orderChartXExtent[0]), IEM.Utils.roundTStoSec(orderChartXExtent[1], true)];
            /*if (orderChartXExtent[1] - orderChartXExtent[0] < 2000) {
             orderChartXExtent = [orderChartXExtent[0]-1000, orderChartXExtent[1]+1000];
             } */
            orderDetailChart.x(d3.time.scale().domain(orderChartXExtent));
            drawOtherOrderDetails();
        }
        getBenchmark && getBenchmarkRates(orderChartXExtent, handleBenchmarkResponse);
        orderDetailChart.render();

        if (typeof orderCharts.tier1BidGraph != 'undefined')
        {
            orderCharts.tier2BidGraph.legendToggle("Bid Price - Tier 2");
            orderCharts.tier3BidGraph.legendToggle("Bid Price - Tier 3");
            orderCharts.tier2AskGraph.legendToggle("Ask Price - Tier 2");
            orderCharts.tier3AskGraph.legendToggle("Ask Price - Tier 3");
            if (orderDetail.BuySell == 'Buy') {
                // Buy turn off bid
                orderCharts.tier1BidGraph.legendToggle("Bid Price - Tier 1");
                $('g.dc-legend g.dc-legend-item').each(function () {
                    var legend_text = $(this).find('text').text();
                    if (legend_text.toLowerCase().indexOf("ask price - tier 1") >= 0) {
                        $(this).css({'fill-opacity': '1'});
                    }
                    else if (legend_text.toLowerCase().indexOf("price - tier") >= 0) {
                        $(this).css({'fill-opacity': '0.5'});
                    }
                })
            } else {
                // sell so turn off ask
                orderCharts.tier1AskGraph.legendToggle("Ask Price - Tier 1");
                $('g.dc-legend g.dc-legend-item').each(function () {
                    var legend_text = $(this).find('text').text();
                    if (legend_text.toLowerCase().indexOf("bid price - tier 1") >= 0) {
                        $(this).css({'fill-opacity': '1'});
                    }
                    else if (legend_text.toLowerCase().indexOf("price - tier") >= 0) {
                        $(this).css({'fill-opacity': '0.5'});
                    }
                })

            }
        }

        drawTradesCptyChart();
    }

    function drawOtherOrderDetails() {
        if (orderDetail.MarketRange) {
            drawOrderDetailRange();
        }
        if (orderDetail.FillRate) {
            drawOrderFillRate();
        }
    }

    function drawOrderFillRate() {

        // Do only if the fill rate is greater than 0
        var fillRate = parseInt(orderDetail.FillRate);

        if (fillRate > 0)
        {
            var fillData = [];
            fillData[0] = {
                date: orderChartXExtent[0],
                value: orderDetail.FillRate
            };
            fillData[1] = {
                date: orderChartXExtent[1],
                value: orderDetail.FillRate
            };

            var sndx = crossfilter(fillData);
            var sdateDim = sndx.dimension(function (d)
            {
                return d.date;
            });
            var fillGroup = sdateDim.group().reduceSum(function (d)
            {
                return d.value;
            });
            orderCharts['orderFillChart'] = dc.lineChart(orderDetailChart)
                    .dimension(sdateDim)
                    .group(fillGroup, "Fill Rate")
                    .colors('#0003CF');

            orderDetailChart.compose(getChartArrFromObj(orderCharts))
                    .legend(dc.legend().x(1140).y(50).itemWidth(130).legendWidth(150).gap(5));
            orderDetailChart.render();
        }
    }

    function drawOrderDetailRange() {
        var startData = [], endData = [], endRate;
        if (orderDetail.MarketRange) {
            if (orderDetail.BuySell == 'Buy') {
                endRate = parseFloat(orderDetail.Rate) + orderDetail.MarketRange;
            } else {
                endRate = parseFloat(orderDetail.Rate) - orderDetail.MarketRange;
            }
        }
        var cp = orderDetail.CcyPair.split('/').join('');
        /*
         check for he pip of the currency pair
         */
        var currencyPipMap = IEM.Mapping.getMap('CurrencyPip');
        var adjust = (1.0 / (currencyPipMap[cp])) * 20.0;

        // get the max extent of the order rate

        if (orderChartYExtents != undefined)
        {
            var maxExtent = d3.max(d3.max(orderChartYExtents)) + adjust;
            var minExtent = d3.min(d3.min(orderChartYExtents)) - adjust;

            if (endRate < minExtent || endRate > maxExtent)
            {
                return;
            }
        }

        startData[0] = {
            date: orderChartXExtent[0],
            value: orderDetail.Rate
        };
        startData[1] = {
            date: orderChartXExtent[1],
            value: orderDetail.Rate
        };
        endData[0] = {
            date: orderChartXExtent[0],
            value: endRate
        };
        endData[1] = {
            date: orderChartXExtent[1],
            value: endRate
        };

        var sndx = crossfilter(startData);
        var sdateDim = sndx.dimension(function (d) {
            return d.date;
        });
        var startGroup = sdateDim.group().reduceSum(function (d) {
            return d.value;
        });
        var endx = crossfilter(endData);
        var edateDim = endx.dimension(function (d) {
            return d.date;
        });
        var endGroup = edateDim.group().reduceSum(function (d) {
            return d.value;
        });
        var yExtent = [endRate, endRate];
        orderChartYExtents.push(yExtent);

        orderCharts['orderRangeStartChart'] = dc.lineChart(orderDetailChart)
                .dimension(sdateDim)
                .group(startGroup, "Order Rate")
                .colors('#02B5B2');

        orderCharts['orderRangeEndChart'] = dc.lineChart(orderDetailChart)
                .dimension(edateDim)
                .group(endGroup, "Order Range")
                .colors('#02ABC2');

        orderDetailChart.compose(getChartArrFromObj(orderCharts))
                .legend(dc.legend().x(1140).y(50).itemWidth(130).legendWidth(150).gap(5))
                .y(d3.scale.linear().domain(getOrderChartYExtent()));
        orderDetailChart.render();
    }

    function plotTradeDetails(data, type, resize) {
        if (typeof resize == 'undefined') {
            resize = true;
        }
        var ndx = crossfilter(data);
        var tradeDim = ndx.dimension(function (d) {
            return d.TRADEID;
        });
        var dateDim = ndx.dimension(function (d) {
            return d.date;
        });
        var ratesGroup = tradeDim.group().reduce(
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.TRADEID = v.TRADEID;
                    p.ORDERID = v.ORDERID;
                    p.RATE = v.RATE;
                    p.Date = v.date;
                    p.CPTY = v.CPTY;
                    return p;
                },
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.TRADEID = v.TRADEID;
                    p.ORDERID = v.ORDERID;
                    p.RATE = v.RATE;
                    p.Date = v.date;
                    p.CPTY = v.CPTY;
                    return p;
                },
                function (p, v) {
                    return {
                        TRADEID: 0,
                        ORDERID: 0,
                        RATE: 0,
                        Date: 0,
                        data: null,
                        CPTY: 0
                    };
                }
        );

        if (resize) {
            var xExtent = d3.extent(data, function (d) {
                return d.EXECTIME;
            });
            var yExtent = d3.extent(data, function (d) {
                return d.RATE;
            });
            orderChartXExtent = mergeExtents([orderChartXExtent, xExtent]);
            orderChartYExtents.push(yExtent);
        }

        orderCharts['trades-' + type + 'Chart'] = dc.bubbleChart(orderDetailChart)
                .dimension(dateDim)
                .group(ratesGroup, "Trades (" + type + ")")
                .keyAccessor(function (p) {
                    if (p.value) {
//			console.log(p.value.Date);
                        return p.value.Date;
                    }
                })
                .colors(tradeColorCodes[type])
                .valueAccessor(function (p) {
                    return p.value.RATE;
                })
                .radiusValueAccessor(function (p) {
                    return 70;
                })
                .maxBubbleRelativeSize(0.0009)
                .r(d3.scale.linear().domain(orderChartRExtent))
                .renderLabel(false);

        orderDetailChart.compose(getChartArrFromObj(orderCharts))
                .legend(dc.legend().x(1140).y(50).itemWidth(130).legendWidth(150).gap(5));

        if (resize) {
            orderDetailChart
                    .x(d3.time.scale().domain(orderChartXExtent))
                    .y(d3.scale.linear().domain(getOrderChartYExtent()));
        }
    }

    /**
     * The function will plot the vertical lines for the trade details
     * @author    Maninder Singh    <manindersingh221@gmail.com>     
     */
    function plotParticularTradeEventDetails(data, resize, highest, lowest, count) {
        var data1 = [];
        var color = '#000066';
        if (data.EventClassification === 'OrderRecivedByServer') {
            color = '#003300';
        }
        else if (data.EventClassification === 'OrderMatchedByServer') {
            color = '#333333';
        }
        else if (data.EventClassification === 'TradeRequestSentToProvider') {
            color = '#663399';
        }
        else if (data.EventClassification === 'TradeRejectReceivedFromProvider') {
            color = '#990000';
        }
        else if (data.EventClassification === 'TradeVerifiedReceivedFromProvider') {
            color = '#33CC00';
        }
        else if (data.EventClassification === 'RateAggregatedByServer') {
            color = '#CCCCCC';
        }

        data1 = [
            {
                "EventKey": data.EventKey + '_' + count + '_min',
                "EventName": data.EventClassification,
                "Rate": lowest,
                "Timestamp": data.Timestamp,
                "TradeId": data.TradeId
            },
            {
                "EventKey": data.EventKey + '_' + count + '_max',
                "EventName": data.EventClassification,
                "Rate": highest,
                "Timestamp": data.Timestamp,
                "TradeId": data.TradeId

            }
        ];
        if (typeof resize == 'undefined') {
            resize = true;
        }
        var ndx = crossfilter(data1);
        var dateDim = ndx.dimension(function (d) {
            return d.EventKey;
        });
        var tradeEventsGroup = dateDim.group().reduce(
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.Rate = v.Rate;
                    p.EventName = v.EventName;
                    p.EventKey = v.EventKey;
                    p.TradeId = v.TradeId;
                    return p;
                },
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.Rate = v.Rate;
                    p.EventName = v.EventName;
                    p.EventKey = v.EventKey;
                    p.TradeId = v.TradeId;
                    return p;
                },
                function (p, v) {
                    return {
                        Rate: 0,
                        data: null
                    };
                }
        );

        if (resize) {
            var xExtent = d3.extent(data1, function (d) {
                return d.EventTime;
            });
            var yExtent = d3.extent(data1, function (d) {
                return d.Rate;
            });
            orderChartXExtent = mergeExtents([orderChartXExtent, xExtent]);
            //  orderChartYExtents.push(yExtent);
        }

        var lineColors = ['#000066', '#003300', '#33FF33', '#663399', '#990000', '#CCCCCC']
        orderCharts['trades-Event_Chart' + count] = dc.lineChart(orderDetailChart)
                .dimension(dateDim)
                .group(tradeEventsGroup, data.EventClassification)
                .keyAccessor(function (p) {
                    return p.value.data.Timestamp;
                })

                .valueAccessor(function (p) {
                    return p.value.data.Rate;
                })
                .dashStyle([5, 5])
                .colors(color)
                .renderDataPoints(true)
                .interpolate('step-after')

        orderDetailChart.compose(getChartArrFromObj(orderCharts))
                .legend(dc.legend().x(1140).y(50).itemWidth(130).legendWidth(150).gap(5));
        orderCharts['trades-Event_Chart' + count].dashStyle([4, 4]);
        if (resize) {
            orderDetailChart
                    //   .x(d3.time.scale().domain(orderChartXExtent))
                    .y(d3.scale.linear().domain(getOrderChartYExtent())).render();
        }

        $('g.dc-tooltip-list g.dc-tooltip circle.dot').attr('r', 0);
    }
    function renameOrderLifeCycleDot(tradeEventTimeArrayVal, adjustYExtVal) {
        var changed = true;
        var legendText = 'Order Life Cycle';
        var legendTextLine = 'Order Life Cycle Connection Line';
        if (lasteventDate.getTime() > tradeEventTimeArrayVal[0] && lasteventDate.getTime() < tradeEventTimeArrayVal[1]) {
            legendText = 'Order Life Cycle';
            legendTextLine = 'Order Life Cycle Connection Line';
        } else {
            changed = false;
            legendText = '*Order Life Cycle';
            legendTextLine = '*Order Life Cycle Connection Line';
        }
        if (changed) {
            if (orderDetails[0].fillrate > adjustYExtVal[0] && orderDetails[0].fillrate < adjustYExtVal[1]) {
                legendText = 'Order Life Cycle';
                legendTextLine = 'Order Life Cycle Connection Line';
            } else {
                legendText = '*Order Life Cycle';
                legendTextLine = '*Order Life Cycle Connection Line';
            }
        }
        $('g.dc-legend g.dc-legend-item').each(function () {
            var text = $(this).text();
            if (text.indexOf('Order Life Cycle') >= 0) {
                $(this).find('text').text(legendText);
            }
            if (text.indexOf('Order Life Cycle Connection Line') >= 0) {
                $(this).find('text').text(legendTextLine);
            }
        })
    }
    /**
     * This function is used to plot order Life cycle dot.
     * @author    Anil    <nl.sainil@gmail.com>  
     */
    function plotOrderLifeCycleDot(adjustXExtVal, adjustYExtVal) {

        lasteventDate = orderDetails[0].lastevent.replace(" ", "T");
        lasteventDate = lasteventDate.concat('Z');
        lasteventDate = new Date(lasteventDate);
        /*Check the last event node is on window or not*/

        var legendText = 'Order Life Cycle';

        var crf = crossfilter([lasteventDate]);
        var dim = crf.dimension(function (d) {
            return d;
        });
        var orderLifeGroup = dim.group().reduce(
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.fillrate = orderDetails[0].fillrate;
                    p.lastevent = v.lastevent;
                    p.ORDERID = orderDetails[0].orderid;
                    p.RATE = orderDetails[0].fillrate;
                    return p;
                },
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.fillrate = v.fillrate;
                    p.lastevent = v.lastevent;
                    p.ORDERID = orderDetails[0].orderid;
                    p.RATE = orderDetails[0].fillrate;
                    return p;
                },
                function (p, v) {
                    return {
                        OrderID: orderDetails[0].orderid,
                        OrderAmt: 0,
                        OrderRate: orderDetails[0].fillrate,
                        Date: 0,
                        data: null
                    }
                }
        );

        orderCharts['orderChartLife'] = dc.bubbleChart(orderDetailChart)
                .dimension(dim)
                .group(orderLifeGroup, legendText)
                .colors('black')
                .keyAccessor(function (p) {
                    return lasteventDate;
                })
                .valueAccessor(function (p) {
                    return orderDetails[0].fillrate;
                })
                .radiusValueAccessor(function (p) {
                    return 30;
                })
                .maxBubbleRelativeSize(0.0005)
                .r(d3.scale.linear().domain([0, 100]))
                .renderLabel(false);

        /*Line chart Section*/
        var data = [
            {
                "Rate": orderDetails[0].fillrate,
                "Timestamp": lasteventDate.getTime()
            },
            {
                "Rate": parseFloat(order.OrderRate),
                "Timestamp": order.date.getTime()
            }
        ];
        var ndx = crossfilter(data);
        var dateDim = ndx.dimension(function (d) {
            return d.Timestamp;
        });
        var orderLifeLineGroup = dateDim.group().reduce(
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.Rate = v.Rate;
                    p.Timestamp = v.Timestamp;
                    return p;
                },
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.Rate = v.Rate;
                    p.Timestamp = v.Timestamp;
                    return p;
                },
                function (p, v) {
                    return {
                        Rate: 0,
                        data: null
                    };
                }
        );

        var colorForLine = '';
        if (orderDetails[0].buysell == 'Buy') {
            if ((orderDetails[0].fillrate - orderDetails[0].orderrate) > 0) {
                // slippage 
                colorForLine = 'red';
            } else {
                //improvement 
                colorForLine = 'green';
            }
        } else if (orderDetails[0].buysell == 'Sell') {
            if ((orderDetails[0].fillrate - orderDetails[0].orderrate) > 0) {
                //improvement 
                colorForLine = 'green';
            } else {
                // slippage 
                colorForLine = 'red';
            }
        }
        orderCharts['orderChartLifeLine'] = dc.lineChart(orderDetailChart)
                .dimension(dateDim)
                .group(orderLifeLineGroup, "Order Life Cycle Connection Line")
                .keyAccessor(function (p) {
                    return p.value.data.Timestamp;
                })
                .valueAccessor(function (p) {
                    return p.value.data.Rate;
                })
                .colors(colorForLine)
                .renderDataPoints(true)

        /*rename the order life cycle legend name*/
        renameOrderLifeCycleDot(tradeEventTimeArrayVal, adjustYExtVal);
    }


    function mainChartTitle(d) {
//        console.log(d);
        if (typeof d.value.TRADEID != 'undefined') {
            return (d.value.TRADEID == d.key ? d.value.Date : d.key) + '\nTrade ID: ' + d.value.TRADEID + '\nRate: ' + d.value.RATE + '\nOrder ID: ' + d.value.ORDERID + '\nBuy/Sell: ' + d.value.data.BUYSELL + '\nCPTY: ' + d.value.data.CPTY + '\nTickStream: ' + d.value.data.TickStream + '\nSize: ' + d.value.data.BASEAMT;
        } else if (typeof d.value.rateData != 'undefined') {
            return d.key + '\nAsk Price: ' + d.value.rateData.ask_price + '\nBid Price: ' + d.value.rateData.bid_price + '\nAsk Size: ' + getFormattedAmount(d.value.rateData.ask_size) + '\nBid Size: ' + getFormattedAmount(d.value.rateData.bid_size) + '\nTier: ' + d.value.rateData.lvl + '\nStatus: ' + d.value.rateData.status;
        } else if (typeof d.value.benchmarkData != 'undefined') {
            return new Date(d.key).toString() + '\nBenchmark Price: ' + d.value.benchmarkData.medianMidPrice + '\nRange: ' + d.value.benchmarkData.midPriceRange;
        }

        if (typeof d.value.EventName != 'undefined') {
            return (d.value.TradeId == d.key ? d.value.Timestamp : d.key) + '\nTrade ID: ' + d.value.TradeId + '\nRate: ' + d.value.Rate + '\nEvent Name: ' + d.value.EventName + '\nTimestamp: ' + d.value.data.Timestamp; // + '\nBuy/Sell: ' + d.value.data.BUYSELL+ '\nCPTY: ' + d.value.data.CPTY+ '\nTickStream: ' + d.value.data.TickStream+'\nSize: ' + d.value.data.BASEAMT;
        }
        return d.key + '\nOrder ID: ' + d.value.OrderID + '\nOrder Rate: ' + d.value.OrderRate;
    }

    function drawBenchmarkAroundOrderGraph(benchmarkData) {

        if (!benchmarkData.length) {
            removePopulateTradeFilterLines();
            //alert('Benchmark Data Not Available'); removed alert as modal is not reuired when bmd na.
            return;
        }
        for (var i = 0, len = benchmarkData.length; i < len; i++) {
            benchmarkData[i].date = new Date(benchmarkData[i].timeStamp);
        }
        ;
        var rateExtent = d3.extent(benchmarkData, function (d) {
            return d.medianMidPrice;
        });
        var timeExtent = d3.extent(benchmarkData, function (d) {
            return d.timeStamp;
        });
        var startTime = new Date(orderChartXExtent[0]).getTime();
        var endTime = new Date(orderChartXExtent[1]).getTime();
        if (endTime - startTime < 10)
        {
            orderChartXExtent = mergeExtents([orderChartXExtent, timeExtent]);
        }
        orderChartYExtents.push(rateExtent);

        var ndx = crossfilter(benchmarkData);
        var dateDim = ndx.dimension(function (d) {
            return d.timeStamp;
        });
        var bmGroup = dateDim.group().reduce(
                function (p, v) {
                    p.benchmarkData = v;
                    return p;
                },
                function (p, v) {
                },
                function (p, v) {
                    return {benchmarkData: null}
                }
        );

        orderCharts['benchmarkGraph'] = dc.lineChart(orderDetailChart)
                .dimension(dateDim)
                .group(bmGroup, "Benchmark")
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.benchmarkData.medianMidPrice;
                })
                .colors('#FF00DD')
                .renderDataPoints(true)
                .interpolate('step-before');

//        orderDetailChart.compose(getChartArrFromObj(orderCharts))
//                .y(d3.scale.linear().domain(getOrderChartYExtent()))
//                .legend(dc.legend().x(1140).y(50).itemWidth(130).legendWidth(150).gap(5))
//                .render();
        if (endTime - startTime < 10) {
            orderDetailChart.compose(getChartArrFromObj(orderCharts))
                    .x(d3.time.scale().domain(orderChartXExtent))
                    .y(d3.scale.linear().domain(getOrderChartYExtent()))
                    .legend(dc.legend().x(1140).y(50).itemWidth(130).legendWidth(150).gap(5))
                    .render();

        } else {
            orderDetailChart.compose(getChartArrFromObj(orderCharts))
                    .y(d3.scale.linear().domain(getOrderChartYExtent()))
                    .legend(dc.legend().x(1140).y(50).itemWidth(130).legendWidth(150).gap(5))
                    .render();
        }

        drawOtherOrderDetails();

        handleExtent(orderDetailChart);

        renameOrderLifeCycleDot(orderChartXExtent, rateExtent);

        removePopulateTradeFilterLines();
    }

    function getChartArrFromObj(charts) {
        var chartArr = [];
        for (var chart in charts) {
            if (charts.hasOwnProperty(chart))
                chartArr.push(charts[chart]);
        }
        return chartArr;
    }

    function adjustYExtent(ext) {

        // add first and last element to remove padding.
        ext = [parseFloat(ext[0]), parseFloat(ext[1])];
        if (ext[0] == ext[1]) {
            var diff = (ext[0] - parseInt(ext[0])) / 100;
            return [ext[0] - diff, ext[1] + diff];
        }
        return [parseFloat(ext[0]) - (ext[1] - ext[0]) / 20, parseFloat(ext[1]) + (ext[1] - ext[0]) / 20];
    }

    function adjustXExtent(ext) {
        // add first and last element to remove padding.
        ext = convertDateExtent(ext);
        if (ext[0] == ext[1]) {
            return [new Date(ext[0] - 5000), new Date(ext[0] + 5000)];
        }
        var diff = ext[1] - ext[0];
        if (diff / 10 > 1) {
            diff = diff / 10;
        }
        tradeEventTimeArrayVal = [];
        tradeEventTimeArrayVal.push(ext[0] - diff);
        tradeEventTimeArrayVal.push(ext[1] + diff);
        return [new Date(ext[0] - diff), new Date(ext[1] + diff)];
    }

    function convertDateExtent(ext) {
        if (ext[0] instanceof Date) {
            ext = [ext[0].getTime(), ext[1].getTime()];
        }
        return ext;
    }

    function mergeExtents(extents) {
        var min = [], max = [];
        extents.forEach(function (d) {
            (!isNaN(d[0])) && min.push(d[0]);
            (!isNaN(d[1])) && max.push(d[1]);
        });
        return [Math.min.apply(null, min), Math.max.apply(null, max)];
    }

    function handleExtent(chart) {
        chart = chart || mainChart;
        var chartChildren = chart.children(), extentArr = [];
        if (chartChildren.length <= 1) {
            return;
        }
        for (var i = 0, len = chartChildren.length; i < len; i++) {
            var child = chartChildren[i];
            if (child.data()[0]) {
                if (child.data()[0].values) {
                    extentArr.push(d3.extent(chartChildren[i].data()[0].values, function (d) {
                        return d.y;
                    }));
                } else if (child.data()[0].value.OrderID && chart.rangeChart()) {
                    var data = child.dimension().filter(chart.rangeChart().filter()).top(Infinity);
                    if (data.length)
                        extentArr.push(d3.extent(data, function (d) {
                            return d.OrderRate;
                        }));
                }
            }
        }
        var yExtent = mergeExtents(extentArr);
        yExtent = adjustYExtent(yExtent);
        chart.y(d3.scale.linear().domain(yExtent));
        chart.redraw();
    }

    function getOrderChartYExtent(extents) {
        var extentArr = (extents || orderChartYExtents).slice(0);
        if (rateExtent) {
            extentArr.push(rateExtent);
        }
        minYaxisVal = adjustYExtent(mergeExtents(extentArr))[0];
        maxYaxisVal = adjustYExtent(mergeExtents(extentArr))[1];
        return adjustYExtent(mergeExtents(extentArr));
    }

    function runTimeline() {
        var chartContainer = $('#orderChartContainer');
        var rectEle = chartContainer.find('svg rect.background');
        var maskEle = $('.maskEle');
        if (!maskEle.length) {
            var divEle = $('<div></div>');
            divEle.attr('class', 'maskEle');
            chartContainer.append(divEle);
            maskEle = $('.maskEle');
        }
        chartContainer.css('position', 'relative');

        maskEle.css({
            top: rectEle.position().top + 'px',
            left: (rectEle.position().left + 1) + 'px',
            width: (rectEle.attr('width') - 1) + 'px',
            height: rectEle.attr('height') + 'px'
        });

        maskEle.animate({
            marginLeft: parseInt(maskEle.css('marginLeft'), 10) == 0 ? maskEle.outerWidth() : 0,
            width: maskEle.width() - parseInt(maskEle.css('marginLeft'), 10)
        }, 20000);
    }

    function addMsgToPerfConsole(msg) {
        if (!addMsgToPerfConsole.console) {
            addMsgToPerfConsole.console = $('.perf-console .messages');
        }
        var consoleEle = addMsgToPerfConsole.console;
        consoleEle.append('<div>' + msg + '</div>');
        consoleEle.animate({
            scrollTop: consoleEle.outerHeight(true)
        }, 100);
    }

    function handleOrderZoomOut() {
        if (orderDetailChart && orderChartXExtent) {
            $('.undoZoom').removeClass('disabled');
            if (!origOrderChartXExtent) {
                origOrderChartXExtent = orderChartXExtent;
            }
            if (!origOrderChartYExtents) {
                origOrderChartYExtents = orderChartYExtents;
            }
            orderChartXExtent = [new Date(orderChartXExtent[0]).getTime() - 1000, new Date(orderChartXExtent[1]).getTime() + 1000];
            orderDetailChart && orderDetailChart.x(d3.time.scale().domain(orderChartXExtent)).render();
            drawOtherOrderDetails();
            getBenchmarkRates(orderChartXExtent, handleBenchmarkResponse, function () {
                if (orderCharts['tier1AskGraph']) {
                    getStreamForOrder(handleStreamData);
                }
            });
        }
    }

    function handleSliderZoom(timeParams) {
        $('.undoZoom').removeClass('disabled');
        if (!origOrderChartXExtent) {
            origOrderChartXExtent = orderChartXExtent;
        }
        if (!origOrderChartYExtents) {
            origOrderChartYExtents = orderChartYExtents;
        }
        if (orderDetailChart && timeParams) {
            $('.undoZoom').removeClass('disabled');
            orderChartXExtent = [timeParams[0], timeParams[1]];
            orderDetailChart && orderDetailChart.x(d3.time.scale().domain(orderChartXExtent)).render();
            drawOtherOrderDetails();
            //  console.log(orderChartXExtent);
            getBenchmarkRates(orderChartXExtent, handleBenchmarkResponse, function () {
                if (orderCharts['tier1AskGraph']) {
                    getStreamForOrder(handleStreamData);
                }
            });
        }
    }

    function handleUndoZoom() {
        if (orderDetailChart && origOrderChartXExtent && origOrderChartYExtents) {
            orderChartXExtent = origOrderChartXExtent;
            orderChartYExtents = origOrderChartYExtents;
            origOrderChartXExtent = null;
            origOrderChartYExtents = null;

            orderDetailChart
                    && orderDetailChart
                    .x(d3.time.scale().domain(orderChartXExtent))
                    .y(d3.time.scale().domain(getOrderChartYExtent(origOrderChartYExtents)))
                    .render();
            drawOtherOrderDetails();
            getBenchmarkRates(orderChartXExtent, handleBenchmarkResponse, function () {
                getStreamForOrder(handleStreamData);
            });
            $('.undoZoom').addClass('disabled')
        }
    }

    function handleFilterTradeSwitch(switchOn) {
        if (switchOn) {
            filterTradeByStream();
        } else {
            if (typeof orderChartStream != 'undefined' && orderChartStream) {
                drawTradeDetailChart(tradesForOrder, false, false);
            }
        }
    }

    return {
        setupGetOrderByIdFormSubmit: setupGetOrderByIdFormSubmit,
        runTimeline: runTimeline,
        handleFilterTradeSwitch: handleFilterTradeSwitch
    };
    /**
     * This script is used to draw treeChart for order trade
     */
    function plotOrderTreeChart(orderDetails) {
        var dataSet = [];
        //$('.btn-order-tree-view').css({'display': 'inline'});
        $('.orderNavigateTreeView').css({'display': 'inline'});
        var dataSet = orderDetails;

        var treeData = '';
        treeData += '<ul style="margin-top: 20px;">';
        var href = (document.location.href).split("web1")[0] + 'web1/home1.jsp?orderid=' + dataSet.id;
        /*Placing first node of the tree*/
        if (dataSet.type == 'ORDER') {
            if (parseInt(dataSet.id) == orderId) {
                treeData += '<li class="firstTreeNode hasSubmenu"><a class="orderNode" target="_blank" href="' + href + '" style="color:black">' + dataSet.id + '</a>';
            } else {
                treeData += '<li class="firstTreeNode hasSubmenu"><a class="orderNode" target="_blank" href="' + href + '" style="color:green">' + dataSet.id + '</a>';
            }
        } else if (dataSet.type == 'BROKER') {
            treeData += '<li class="firstTreeNode hasSubmenu"><a target="_blank" style="color:#a718b2">' + dataSet.id + '</a>';
        }
        dataSet['parentId'] = 'NULL';
        count = 0;
        var orderIdOfNode = dataSet.id;
        if (dataSet.childGraphNodes) {
            var _childGraphNodes = dataSet.childGraphNodes;
            createChildNode(_childGraphNodes, dataSet.id);
        }
        /*This function is used to place node*/
        var parentIdForNext = '';

        function createChildNode(_childGraphNodes, parentID) {
            treeData += '<ul style="margin-top: 20px;">';
            for (var item in _childGraphNodes) {
                var value = _childGraphNodes[item];
                var nodeID = value.id;
                var nodeparentID = parentID;
                if (_childGraphNodes[item].type == 'ORDER') {
                    _childGraphNodes[item]['parentID'] = nodeID;
                    nodeparentID = nodeID
                    arrayLevelOrder.push({
                        level: ++count,
                        nodeparentID: nodeparentID
                    });
                }
                if (nodeID != null)
                {
                    if (_childGraphNodes[item].type == 'ORDER') {
                        parentIdForNext = _childGraphNodes[item].id;
                        var href = (document.location.href).split("web1")[0] + 'web1/home1.jsp?orderid=' + _childGraphNodes[item].id;
                        if (_childGraphNodes[item].id == orderId) {
                            treeData += '<li class="hasSubmenu"><a class="orderNode" target="_blank" href="' + href + '" style="color:black">' + _childGraphNodes[item].id + '</a>';
                        } else {
                            treeData += '<li class="hasSubmenu"><a class="orderNode" target="_blank" href="' + href + '" style="color:#02ABC2">' + _childGraphNodes[item].id + '</a>';
                        }
                    } else if (_childGraphNodes[item].type == 'LP') {
                        treeData += '<li class="hasSubmenu"><a target="_blank" style="color:blue">' + _childGraphNodes[item].id + '</a>';
                    } else if (_childGraphNodes[item].type == 'TRADE') {
                        var href = (document.location.href).split("web1")[0] + 'web1/tradeDetails.html?orderId=' + arrayLevelOrder[arrayLevelOrder.length - 1].nodeparentID + '&tradeId=' + _childGraphNodes[item].id;
                        if (_childGraphNodes[item].attributes.status == 'R') {
                            treeData += '<li><a target="_blank" href="' + href + '" style="color:red">' + _childGraphNodes[item].id + '</a>';
                        } else if (_childGraphNodes[item].attributes.status == 'C') {
                            treeData += '<li><a target="_blank" href="' + href + '" style="color:green">' + _childGraphNodes[item].id + '</a>';
                        } else if (_childGraphNodes[item].attributes.status == 'F') {
                            treeData += '<li><a target="_blank" href="' + href + '" style="color:#D97307">' + _childGraphNodes[item].id + '</a>';
                        }
                    } else if (_childGraphNodes[item].type == 'BROKER') {
                        treeData += '<li class="hasSubmenu"><a style="color:#a718b2">' + _childGraphNodes[item].id + '</a>';
                    } else {
                        treeData += '<li class="hasSubmenu"><a target="_blank" style="color:blue">' + _childGraphNodes[item].id + '</a>';
                    }
                }
//                console.log(_childGraphNodes[item]);
//                console.log(_childGraphNodes[item].childGraphNodes);
//                console.log(_childGraphNodes[item].childGraphNodes.length);
                if (_childGraphNodes[item].childGraphNodes != 'undefined') {
//                    console.log(_childGraphNodes[item].childGraphNodes, 'maninder');
                    if (_childGraphNodes[item]) {
                        createChildNode(value.childGraphNodes, nodeID);
                    }
                }
                if (_childGraphNodes[item].type == 'ORDER') {
                    arrayLevelOrder.splice(arrayLevelOrder.length - 1, 1);
                }
                treeData += '</li>';
                treeData += '</ul>';
            }
        }
        treeData += '</li>';
        treeData += '</ul>';
        $('.orderNavigateTreeView div').html(treeData);
        $('.orderNavigateTreeView button').css({'height': '300px'});

//        $('.orderNavigateTreeView ul').each(function () {
//            console.log($(this).find("li"));
//            if ($.isEmptyObject($(this).find("li"))) {
//            } else {
//                $(this).find("li").has("ul").addClass("hasSubmenu");
//            }
//        });

        /* Find the last li in each level */
        $('.orderNavigateTreeView li:last-child').each(function () {

            $this = $(this);
            /* Check if LI has children*/
            if ($this.children('ul').length === 0) {
                /* Add border-left in every UL where the last LI has not children*/
                $this.closest('ul').css("border-left", "1px solid red");
            } else {
                /* Add border in child LI, except in the last one*/
                $this.closest('ul').children("li").not(":last").css("border-left", "1px solid gray");
                /* Add the class "addBorderBefore" to create the pseudo-element :defore in the last li*/
                if ($this.closest('ul').children("li").hasClass('hasSubmenu')) {
                    $this.closest('ul').children("li").last().children("a").addClass("addBorderBefore");
                } else if ($this.closest('ul').children("li").hasClass('firstTreeNode')) {

                } else {
                    $this.closest('ul').children("li").css("border-left", "1px solid gray");
                }
            }
            /* Add margin in the first level of the list*/
            $this.closest('ul').css("margin-top", "20px");
            /* Add margin in other levels of the list*/
            $this.closest('ul').find("li").children("ul").css("margin-top", "20px");
        });
        /* Add bold in li and levels above*/
        $('.orderNavigateTreeView ul li').each(function () {
            $(this).mouseenter(function () {
                $(this).children("a").css({"font-weight": "bold"});
            });
            $(this).mouseleave(function () {
                $(this).children("a").css({"font-weight": "normal"});
            });
        });
        /* Add button to expand and condense - Using FontAwesome*/
        $('.orderNavigateTreeView ul li.hasSubmenu').each(function () {
            if ($(this).hasClass('firstTreeNode')) {
                $(this).prepend("<a href='javascript:void(0)'><span class='glyphicon glyphicon-minus-sign'></span><span style='display:none;' class='glyphicon glyphicon-plus-sign'></span></a>");
                $(this).children("a").not(":last").removeClass().addClass("toogle");
            } else {
                $(this).prepend("<a href='javascript:void(0)'><span class='glyphicon glyphicon-plus-sign'></span><span style='display:none;' class='glyphicon glyphicon-minus-sign'></span></a>");
                $(this).children("a").not(":last").removeClass().addClass("toogle");
            }
        });
        /* Collapse till one level */
        $('.orderNavigateTreeView ul li ul li').each(function () {
            var currentExecuteNodeStatus = true;
            $(this).find('a').filter(function () {
                /*If node is equal to current node, Than expand it*/
                if ($(this).hasClass('orderNode') >= 0) {
                    var currentExecuteNode = parseInt($(this).text().trim());
                    if (currentExecuteNode == orderId && currentExecuteNodeStatus) {
                        currentExecuteNodeStatus = false;
                        /*parent node*/
                        var _parent = $(this).parent().parent().parent();
                        if ($(_parent).attr('class') == 'firstTreeNode hasSubmenu') {

                        } else {
                            signChangeCollapse(_parent);
                        }
                        /*current node*/
                        $(this).parent().find('.toogle').children("span").css({'display': 'none'});
                        $(this).parent().find('.toogle').children("span").next().css({'display': 'inline'});

                        /*child node*/
                        $(this).parent().find('ul li').each(function () {
                            $(this).children(".toogle").children("span").css({'display': 'inline'});
                            $(this).children(".toogle").children("span").next().css({'display': 'none'});
                        });
                    }
                    return false;
                }
            }).first();
            /*This function is used to change sign of the parent nodes of current node*/
            function signChangeCollapse(_parent) {
                $(_parent).find('.toogle').first().children("span").css({'display': 'none'});
                $(_parent).find('.toogle').first().children("span").next().css({'display': 'inline'});
                if ($(_parent).parent().parent().attr('class') == 'hasSubmenu') {
                    var _parent = $(_parent).parent().parent();
                    signChangeCollapse(_parent)
                }
            }
            if (currentExecuteNodeStatus) {
                /*If node is not equal to current node*/
                $(this).closest("li").children("ul").toggle("slow");
                $(this).children("span").toggle();
            }
        });

        /* Actions to expand and consense on click on the node*/
        $('.orderNavigateTreeView ul li.hasSubmenu a.toogle').click(function () {
            $(this).closest("li").children("ul").toggle("slow");
            $(this).children("span").toggle();
            setTimeout(function () {
                if ($('.orderNavigateTreeView div ul').height() > 120) {
                    $('.orderNavigateTreeView button').css({'height': $('.orderNavigateTreeView div ul').height() + 30 + 'px'});
                } else {
                    $('.orderNavigateTreeView button').css({'height': '150px'});
                }
                orderNavigateTreeViewWidth = -$('.orderNavigateTreeView').width() + 9;
            }, 500);
            return false;
        });
        /*Calcuale height of order tree div and set to side button*/
        setTimeout(function () {
            if ($('.orderNavigateTreeView div ul').height() > 120) {
                $('.orderNavigateTreeView button').css({'height': $('.orderNavigateTreeView div ul').height() + 30 + 'px'});
            } else {
                $('.orderNavigateTreeView button').css({'height': '150px'});
            }
        }, 800);
        /*set width of orderNavigateTreeView div*/
        setTimeout(function () {
            orderNavigateTreeViewWidth = -$('.orderNavigateTreeView').width() + 9;
            $('.orderNavigateTreeView').css({'margin-left': orderNavigateTreeViewWidth + 'px'});
        }, 1000);
    }


    /**
     * This script is used to perfom click event on piechart.
     * 
     * @author    Anil    <nl.sainil@gmail.com> 
     */
    function populateTradeFilter(pieChartFilterTrade) {
        $('body').on('click', 'svg g.pie-slice', function (e) {
            tradeCptyChartType = '';
            tradeCptySelectedClassR = '';
            tradeCptySelectedClassRPre = '';
            tradeCptySelectedClassC = '';
            tradeCptySelectedClassCPre = '';
            if ($(this).parent().parent().parent().attr('id') == 'tradeCptyRejectedChartContainer') {
                tradeCptyChartType = 'rejected';
                tradeCptySelectedClassR = $(this).attr('class');
                tradeCptySelectedClassRPre.push(tradeCptySelectedClassR);
            } else {
                tradeCptyChartType = 'conformed';
                tradeCptySelectedClassC = $(this).attr('class');
                tradeCptySelectedClassCPre.push(tradeCptySelectedClassC);
            }
            var count = 0;
            var countStatus = true;
            var selectedProvider = $(this).find('title').text();
            selectedProvider = (selectedProvider.split(":")[1]).split("Total Amount")[0].trim();
            var orderTempData = {
                "Rate": order.Rate,
                "Timestamp": order.Created,
                "EventName": 'TradeByProvider',
                "EventKey": count + '_min',
                "TradeId": ''
            }
            $.eachAsync(pieChartFilterTrade, {
                delay: 1,
                bulk: 0,
                loop: function (key, value)
                {
                    if (value.CPTY == selectedProvider) {
                        var data = [];
                        data.push(orderTempData);
                        data.push({
                            "Rate": value.RATE,
                            "Timestamp": value.EXECTIME,
                            "EventName": 'TradeByProvider',
                            "EventKey": count + '_max',
                            "TradeId": ''
                        });
                        count = parseInt(count) + 1;
                        drawLineForSameProvider(data, count);
                    }
                },
                end: function ()
                {
                    orderDetailChart.compose(getChartArrFromObj(orderCharts))
                            .legend(dc.legend().x(1140).y(50).itemWidth(130).legendWidth(150).gap(5));

                    if (resize) {
                        orderDetailChart
                                .y(d3.scale.linear().domain(getOrderChartYExtent())).render();
                    }
                    removePopulateTradeFilterLines();
                    if (tradeCptyChartType == 'rejected') {
                        $('#tradeCptyRejectedChartContainer g.pie-slice').each(function () {
                            if (tradeCptySelectedClassR != $(this).attr('class') && tradeCptySelectedClassR != '') {
                                if ($(this).attr('class').indexOf('deselected') < 0) {
                                    d3.select(this).classed('selected', false);
                                    d3.select(this).classed('deselected', true);
                                }
                            }
                        });
                        $('#tradeCptyConfirmedChartContainer g.pie-slice').each(function () {
//                            if (tradeCptySelectedClassC != $(this).attr('class') && tradeCptySelectedClassC != '') {
//                                if ($(this).attr('class').indexOf('deselected') < 0) {
                                    d3.select(this).classed('selected', false);
                                    d3.select(this).classed('deselected', false);
//                                }
//                            }
                        });
                    }
                    if (tradeCptyChartType == 'conformed') {
                        $('#tradeCptyConfirmedChartContainer g.pie-slice').each(function () {
                            if (tradeCptySelectedClassC != $(this).attr('class') && tradeCptySelectedClassC != '') {
                                if ($(this).attr('class').indexOf('deselected') < 0) {
                                    d3.select(this).classed('selected', false);
                                    d3.select(this).classed('deselected', true);
                                }
                            }
                        });
                        $('#tradeCptyRejectedChartContainer g.pie-slice').each(function () {
//                            if (tradeCptySelectedClassR != $(this).attr('class') && tradeCptySelectedClassR != '') {
//                                if ($(this).attr('class').indexOf('deselected') < 0) {
                                    d3.select(this).classed('selected', false);
                                    d3.select(this).classed('deselected', false);
//                                }
//                            }
                        });
                    }
                }
            });
        });
    }

    /**
     * This function is used to hide populated filter lines.
     * @author    Anil    <nl.sainil@gmail.com>
     */
    function removePopulateTradeFilterLines() {
        var countNumber = parseInt(0);
        var legendStarttrans = false;
        var countStatus = true;

        $('g.dc-legend g.dc-legend-item').each(function () {
            var legend_text = $(this).find('text').text();
            if (legend_text.indexOf("TradeByProvider") >= 0) {
                if (countStatus) {
                    countStatus = false;
                    countNumber++;
                    $(this).attr("transform",
                            "translate(" + 0 + "," + ((countNumber) * 17) + ")");
                } else {
                    $(this).remove();
                    legendStarttrans = true;
                }
            } else {
                countNumber++;
                $(this).attr("transform",
                        "translate(" + 0 + "," + ((countNumber) * 17) + ")");
            }
        });

    }

    /**
     * This function is used to connect ordernode with selectedtrade of same provider.
     * @author    Anil    <nl.sainil@gmail.com> 
     */
    function drawLineForSameProvider(data, count) {
        var color = '#EB773D'
        if (typeof resize == 'undefined') {
            resize = true;
        }
        var ndx = crossfilter(data);
        var dateDim = ndx.dimension(function (d) {
            return d.Timestamp;
        });
        var tradeEventsGroup = dateDim.group().reduce(
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.Rate = v.Rate;
                    p.EventName = v.EventName;
                    p.Timestamp = v.Timestamp;
                    p.EventKey = v.EventKey;
                    p.TradeId = v.TradeId;
                    return p;
                },
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.Rate = v.Rate;
                    p.EventName = v.EventName;
                    p.Timestamp = v.Timestamp;
                    p.EventKey = v.EventKey;
                    p.TradeId = v.TradeId;
                    return p;
                },
                function (p, v) {
                    return {
                        Rate: 0,
                        data: null
                    };
                }
        );
        orderCharts['TradeByProvider' + '_' + count] = dc.lineChart(orderDetailChart)
                .dimension(dateDim)
                .group(tradeEventsGroup, 'TradeByProvider')
//                .group(tradeEventsGroup, 'FilterServiceProvider')
                .keyAccessor(function (p) {
                    return p.value.data.Timestamp;
                })
                .valueAccessor(function (p) {
                    return p.value.data.Rate;
                })
                .colors(color)
                .renderDataPoints(true)

    }
};


/**
 * This script is used to draw treeChart for order navigater
 * @author    Anil    <nl.sainil@gmail.com>     
 * 
 */
function plotOrderTreeChart3(orderDetails) {
    //array set for data values
    var dataSet = [];
    $('.orderNavigateTreeView3 div').html('');
    $('.btn-order-tree-view').css({'display': 'inline'});
    var dataSet = orderDetails;
    var i = 0;
    var tree = d3.layout.tree()
            .size([height, width]);
    var diagonal = d3.svg.diagonal()
            .projection(function (d) {
                return [d.y, d.x];
            });
    var margin = {top: 24, right: 20, bottom: 20, left: 20},
    width = 700,
            height = 500,
            i = 0,
            duration = 500;
    var tree = d3.layout.tree().nodeSize([110, 40])
            .children(function (d) {
                return d.childGraphNodes;
            });
    var diagonal = d3.svg.diagonal()
            .projection(function (d) {
                return [d.x, d.y];
            });
    var svg = d3.select(".orderNavigateTreeView3 div")
            .append("svg")
            .attr("class", "nodeClass")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + margin.top + ")");
    dataSet.x0 = 0;
    dataSet.y0 = height / 2;

    function collapse(d) {
        if (d.type == 'LP') {
            if (d.childGraphNodes) {
                d._childGraphNodes = d.childGraphNodes;
                d._childGraphNodes.forEach(collapse);
                d.childGraphNodes = null;
            }
        }
    }
    var nodesTest = tree.nodes(dataSet).reverse();
    nodesTest.forEach(function (d) {
        if (d.type == 'LP') {
            if (d.childGraphNodes) {
                d._childGraphNodes = d.childGraphNodes;
                d._childGraphNodes.forEach(collapse);
                d.childGraphNodes = null;
            }
        }
    })

    updateOrderTreeChart(dataSet);
    function updateOrderTreeChart(source) {
        // Compute the new tree layout.
        var nodes = tree.nodes(dataSet).reverse(),
                links = tree.links(nodes);
        // Normalize for fixed-depth.
        nodes.forEach(function (d) {
            if (treeChartDepthLevel < d.depth) {
                treeChartDepthLevel = d.depth;
            }
            d.y = d.depth * 80;
        });
        $('.nodeClass').attr("height", (treeChartDepthLevel + 1) * 80);
        // Update the nodes…
        var node = svg.selectAll("g.node")
                .data(nodes, function (d) {
                    return d.ida || (d.ida = ++i);
                });
        // Enter any new nodes at the parent's previous position.
        var nodeEnter = node.enter().append("g")
                .attr("class", "node")
                .attr("transform", function (d) {
                    return "translate(" + source.y0 + "," + source.x0 + ")";
                })
                .on("click", clickToRedrawchildGraphNode);
        nodeEnter.append("circle")
                .attr("r", 6)
                .attr('class', function (d) {
                    if (Object.keys(d.attributes).length > 0) {
                        return 'orderTreeNode';
                    }
                })
                .style("stroke", function (d) {
                    if (d.type == 'ORDER') {
                        if (orderId == d.id) {
                            return "black";
                        } else {
                            return "#7F7F7F";
                        }
                    } else if (d.type == 'TRADE') {
                        if (d.attributes.status == 'C') {
                            return "#00A300";
                        } else if (d.attributes.status == 'R') {
                            return "#DE0202";
                        } else if (d.attributes.status == 'F') {
                            return "#004444";
                        }
                    } else if (d.type == 'LP') {
                        if (d.status == 'C') {
                            return "#FF6600";
                        } else if (d.status == 'R') {
                            return "#FF3399";
                        } else if (d.status == 'F') {
                            return "#9933FF";
                        }
                    }
                })
                .attr("stroke-width", 1)
                .style("fill", function (d) {
                    return d._childGraphNodes ? "lightsteelblue" : "#fff";
                });
        nodeEnter.append("a")
                .attr('class', function (d) {
                    return d.type;
                })
                .attr("xlink:href", function (d) {
                    if (d.type == "ORDER") {
                        return (document.location.href).split("web1")[0] + 'web1/home1.jsp?orderid=' + d.id;
                    } else if (d.type == 'TRADE') {
                        return (document.location.href).split("web1")[0] + 'web1/tradeDetails.html?orderId=' + d.parent.parent.id + '&tradeId=' + d.id;
                    }
                })
                .attr("target", "_blank")
                .append("text")
                .attr("x", function (d) {
                    return 0;
                })
                .attr("y", function (d) {
                    return -18;
                })
                .attr("dy", function (d) {
                    return d.childGraphNodes || d._childGraphNodes ? "0.35em" : "2em";
                })
                .attr("text-anchor", function (d) {
                    return "middle";
                })
                .text(function (d) {
                    return d.id;
                })
                .style("fill-opacity", 1e-6);
        /**
         * This script is used to show tooltip on hover on node
         * 
         * @author    Anil    <nl.sainil@gmail.com>    
         */
        $('.orderNavigateTreeView3 div svg g g.node circle.orderTreeNode').tipsy({
            gravity: 's',
            html: true,
            title: function () {
                var d = this.__data__;
                if (d.type == 'ORDER') {
                    return "<table><tbody>\n\
                            <tr> <td style='text-align: left;'>Org </td><td style='text-align: right;'>" + d.attributes.org + "</td > </tr>\n\
                             </tbody></table>";
                } else if (d.type == 'TRADE') {
                    return "<table><tbody>\n\
                            <tr> <td style='text-align: left;'>Buysell </td><td style='text-align: right;'>" + d.attributes.buysell + "</td > </tr>\n\
                            <tr> <td style='text-align: left;'>Stream </td><td style='text-align: right;'>" + d.attributes.stream + "</td > </tr>\n\
                            <tr> <td style='text-align: left;'>Status </td><td style='text-align: right;'>" + d.attributes.status + "</td > </tr>\n\
                            <tr> <td style='text-align: left;'>Matchedrate </td><td style='text-align: right;'>" + d.attributes.matchedrate + "</td > </tr>\n\
                            <tr> <td style='text-align: left;'>Usdamt </td><td style='text-align: right;'>" + d.attributes.usdamt + "</td > </tr>\n\
                            <tr> <td style='text-align: left;'>Cpty </td><td style='text-align: right;'>" + d.attributes.cpty + "</td > </tr>\n\
                            </tbody></table>";
                } else if (d.type == 'LP') {
                    //no attributes is coming 
                } else {
                    return '';
                    // nothing happens
//                    return "<table><tbody>\n\
//                            <tr> <td style='text-align: left;'>Org </td><td style='text-align: right;'>" + d.attributes.org + "</td > </tr>\n\
//                             </tbody></table>";
                }
            }
        });
        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });
        nodeUpdate.select("circle")
                .attr("r", 6)
                .attr("stroke", "black")
                .attr("stroke-width", 1)
                .style("fill", function (d) {
                    return d._childGraphNodes ? "lightsteelblue" : "#fff";
                });
        nodeUpdate.select("text")
                .style("fill-opacity", 1);
        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + source.x + "," + source.y + ")";
                })
                .remove();
        nodeExit.select("circle")
                .attr("r", 6)
                .attr("stroke", "black")
                .attr("stroke-width", 1)
                .style("fill", function (d) {
                    return d._childGraphNodes ? "lightsteelblue" : "#fff";
                });
        nodeExit.select("text");
        // Update the links…
        var link = svg.selectAll("path.link")
                .data(links, function (d) {
                    return d.target.ida;
                });
        // Enter any new links at the parent's previous position.
        link.enter().insert("path", "g")
                .attr("class", "link")
                .attr("stroke", function (d) {
//                    if (d.target.youAreHere) {
//                        return '#000';
//                    }
//                    else {
//                        return '#ccc';
//                    }
                    return '#ccc';
                    /*else {
                     var x = '#ccc';
                     $(d.target.children).each(function(key, val) {
                     if (val.youAreHere) {
                     console.log('true');
                     x = '#000';
                     } else {
                     $(d.target.children).each(function(key, val) {
                     if (val.youAreHere) {
                     console.log('true');
                     x = '#000';
                     } else {
                     x = '#ccc';
                     }
                     });
                     }
                     });
                     return x;
                     } */
                })
                .attr("d", function (d) {
                    var o = {
                        x: source.x0,
                        y: source.y0
                    };
                    return diagonal({
                        source: o,
                        target: o
                    });
                });
        // Transition links to their new position.
        link.transition()
                .duration(duration)
                .attr("d", diagonal);
        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
                .duration(duration)
                .attr("d", function (d) {
                    var o = {
                        x: source.x,
                        y: source.y
                    };
                    return diagonal({
                        source: o,
                        target: o
                    });
                })
                .remove();
        // Stash the old positions for transition.
        nodes.forEach(function (d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });
        setTimeout(function () {
            updateOrderTreeChartAttr();
        }, 1500);
    }
    // update width and height for svg and parent div
    function updateOrderTreeChartAttr() {
        var levelWidth = [1];
        var hightDepth = [1];
        var chartWidth = [];
        newtreeChartWidth = 400;
        //calculate new width on adding and removing new node.
        $('svg.nodeClass g.node').each(function () {
            var xPoint = $(this).attr('transform');
            var xPoint = xPoint.split("(").pop();
            var xPointVal = xPoint.substr(0, xPoint.indexOf(','));
            chartWidth.push(parseInt(xPointVal));
        });
        var childCount = function (level, n) {
            //calculate height for chart as per adding or removing new node
            hightDepth.push(n.depth);
            if (n.children && n.children.length > 0) {
                if (levelWidth.length <= level + 1)
                    levelWidth.push(0);
                levelWidth[level + 1] += n.children.length;
                n.children.forEach(function (d) {
                    childCount(level + 1, d);
                });
            }
        };
        childCount(0, dataSet);
        var newHeight = d3.max(hightDepth);
        var chartMinWidth = d3.min(chartWidth);
        var chartMaxWidth = d3.max(chartWidth);
        if (chartMinWidth > 0) {
            newtreeChartWidth = chartMinWidth + chartMaxWidth + 100;
        } else {
            newtreeChartWidth = chartMaxWidth - chartMinWidth + 100;
        }
        if (newtreeChartWidth < 400) {
            newtreeChartWidth = 440;
        }

        treeChartDepthLevelVal = ((newHeight + 1) * 80);
        if (newtreeChartWidth > ($(window).width() - 100)) {
            $('#orderTreeViewModal div.modal-dialog').css("height", treeChartDepthLevelVal + 'px');
            $('#orderTreeViewModal div.modal-dialog').css("width", $(window).width() - 100 + "px");
            $('.orderNavigateTreeView3').css("height", treeChartDepthLevelVal + 50 + 'px');
            $('.orderNavigateTreeView3').css("width", $(window).width() - 130 + "px");
            $('.orderNavigateTreeView3 div').css("height", treeChartDepthLevelVal + 'px');
            $('.orderNavigateTreeView3 div').css("width", newtreeChartWidth + "px");
        } else {
            $('#orderTreeViewModal div.modal-dialog').css("height", treeChartDepthLevelVal + 'px');
            $('#orderTreeViewModal div.modal-dialog').css("width", newtreeChartWidth + 10 + "px");
            $('.orderNavigateTreeView3').css("height", treeChartDepthLevelVal + 'px');
            $('.orderNavigateTreeView3').css("width", newtreeChartWidth + "px");
            $('.orderNavigateTreeView3 div').css("height", treeChartDepthLevelVal + 'px');
            $('.orderNavigateTreeView3 div').css("width", newtreeChartWidth - 10 + "px");
        }

        var traslateMiddlePoint = 201;
        if (chartMinWidth > 0) {
            traslateMiddlePoint = parseInt(chartMinWidth) + 50;
        } else if (chartMinWidth < 0) {
            traslateMiddlePoint = -(parseInt(chartMinWidth) - 50);
        }
        $('.nodeClass').attr("height", (newHeight + 1) * 80)
                .attr("width", newtreeChartWidth);
        svg.attr("transform",
                "translate(" + traslateMiddlePoint + "," + margin.top + ")");
        width = newtreeChartWidth;
        height = (newHeight + 1) * 80;
        dataSet.x0 = 0;
        dataSet.y0 = height / 2;
    }

    // Toggle childGraphNodes on click.
    function clickToRedrawchildGraphNode(d) {
        var className = $(this).find('a').attr('class');
        if (d.childGraphNodes) {
            d._childGraphNodes = d.childGraphNodes;
            d.childGraphNodes = null;
        } else {
            d.childGraphNodes = d._childGraphNodes;
            d._childGraphNodes = null;
        }
        updateOrderTreeChart(d);
        setTimeout(function () {
            updateOrderTreeChartAttr();
        }, 1000);
    }
}
;

/**
 * This script is used to show tooltip on hover on trade event lines
 * 
 * @author    Anil    <nl.sainil@gmail.com>    
 */
$('body').on('mouseover', 'path', function (e) {
    var searchEventName = $(this).parent().parent().siblings().find('title').text();

    if (searchEventName.indexOf('Event Name') >= 0) {
        $('.plotTradeEventDetails').remove();

        var tradeText = "Trade: " + ((searchEventName.split(":")[1]).split("Rate")[0]).trim();
        var eventText = "Event: " + ((searchEventName.split(":")[3]).split("Timestamp")[0]).trim();
        var timeText = "Time: " + new Date(parseInt((searchEventName.split(":")[4]).trim()));

        $("body").append("<div class='plotTradeEventDetails'>\n\
                    <span class='plotTradeEventDetailSpan'>" + tradeText + "</span><br/>\n\
                    <span class='plotTradeEventDetailSpan'>" + eventText + "</span><br/>\n\
                    <span class='plotTradeEventDetailSpan'>" + timeText + "</span></div>")
        $(".plotTradeEventDetails").offset({left: e.pageX, top: e.pageY});
        $(".plotTradeEventDetails").css('width', '275px');
        $(".plotTradeEventDetails").css('border', '1px solid #555');
        $(".plotTradeEventDetails").css('border-radius', '4px');
        $(".plotTradeEventDetails").css('background-color', '#fff');
        $(".plotTradeEventDetails").css('padding', '4px');
    }
});
/**
 * This script is used to hide tooltip on hover on trade event lines
 * 
 * @author    Anil    <nl.sainil@gmail.com>    
 */
$('body').on('mouseout', 'path', function (e) {
    $('.plotTradeEventDetails').remove();
});

/**
 * This script is used to collapse and expand orderTree div to hide and show
 * 
 * @author    Anil    <nl.sainil@gmail.com> 
 */
$(document).ready(function () {
    $('body').on('click', '.orderNavigateTreeView button', function () {
        var curwidth = $(this).parent().offset();

        if (curwidth.left > -1) //compare margin-left value
        {
            /*animate margin-left value to -490px */
            $(this).parent().animate({marginLeft: orderNavigateTreeViewWidth}, 300);
            $(this).html('<span>O<br/>R<br/>D<br/>E<br/>R<br/><br/>T<br/>R<br/>E<br/>E</span>');
        } else {
            /*animate margin-left value 0px */
            $(this).parent().animate({marginLeft: "0"}, 300);
            $(this).html('<span>O<br/>R<br/>D<br/>E<br/>R<br/><br/>T<br/>R<br/>E<br/>E</span>');
        }
    });
});


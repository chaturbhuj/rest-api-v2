//Landscape Mode - More than an hour - separate orders/trades call - just getting summary. Benchmark Data shown. No Tick Data
//Zoomedin Mode1 - <= 30 min && >= 10 min - Orders/Trades Detailed Call, showing blotter tables with all info, Only benchmark Shown, No Tick Data
//Zoomedin Mode2 - <=10min - Orders/Trades Detailed Call, show blotter Tabs, Benchmark & Tickdata shown

//Utility function for adding zero to date & time when required.
var arrayTickValues = []; /* declaring an array for tick values */
globalWaitTag = true;
pieChartFilterData = [];
function addZeroIfNeeded(number) {
    if (number <= 9) {
        return '0' + number;
    }
    return number;
}

// Date String function. Specific format for EMScope. This always returns time in GMT. using getTimezoneOffset to handle this.
function dateToString() {
    var dateObj = this;
    var timezoneOffset = dateObj.getTimezoneOffset() * 60000;
    dateObj = new Date(dateObj.getTime() + timezoneOffset);
    var displayDate = dateObj.getFullYear() + '-' + addZeroIfNeeded(dateObj.getMonth() + 1) + '-' + addZeroIfNeeded(dateObj.getDate()) + ' ' + addZeroIfNeeded(dateObj.getHours()) + ':' + addZeroIfNeeded(dateObj.getMinutes()) + ':' + addZeroIfNeeded(dateObj.getSeconds());
    var milliseconds = dateObj.getMilliseconds().toString();
    if (milliseconds.length < 3) {
        for (var i = 0, len = 3 - milliseconds.length, str = ''; i < len; i++) {
            str += '0';
        }
        milliseconds = str + milliseconds;
    }
    displayDate += '.' + milliseconds;
    return displayDate;
}

Date.prototype.toString = dateToString;

IEM.EMScope = (function () {

    var charts = {},
            tradeColorCodes = IEM.Mapping.getMap('TradeColorCodes'),
            tradeStatusMap = IEM.Mapping.getMap('TradeStatusMap'),
            mainFormParams = {},
            orderData = [], orderTableData = [],
            tradeData = [], tradeTableData = [],
            benchmarkData = {}, benchmarckTableData = [],
            profileData = {}, profileTableData = [],
            mainChart, mainZoomChart, mainChartYExtent, globalRateExtent;

    // main function which sets up the forms in EMScope Tab
    function setupGetOrdersFormSubmit() {
        $('form[name="getOrdersForm"]').submit(function (e) {
            e.preventDefault();
            if ($('#timeRange').val() == "") {
                alert("Invalid Time Range ..!");
                return;
            }
            globalWaitTag = true;
            getOrdersFormSubmit();
//            if (pieChartFilterData.length > 0) {
//                console.log(pieChartFilterData);
//            }
        });

        $('#strmsbtn').click(function (e) {
            var timeRange = $('#timeRange').val().split(' - ');
            var startTime = new Date(timeRange[0]);
            var endTime = new Date(timeRange[1]);
            if ($('#timeRange').val() != "") {
                startTime = startTime.getTime();
                endTime = endTime.getTime();
            } else {
                alert("Invalid Time Range..");
                return;
            }

            if ((endTime - startTime) / 60000 > 10) {
                alert("Range is more than 10 min..");
                return;
            }
            globalWaitTag = true;
            getOrdersFormSubmit();
            $('#provider').trigger("change");
        });
        // This is for getting tick data on changing the provider-stream dropdown
        $('#provider').change(function () {
            if (mainChart && charts['ordersChart']) {
                var proStreams = $('#provider').val();
                globalRateExtent = null;
                mainFormParams.proStreams = [];
                mainFormParams.proStreams.push(proStreams.split('/'));
                var timeframe = mainChart.rangeChart().filter();
                if ((!timeframe && (mainFormParams.endTime - mainFormParams.startTime > 600000)) || (timeframe && (timeframe[1].getTime() - timeframe[0].getTime() > 600000))) {
                    return;
                }
                globalWaitTag = true;
                getBenchmarkSampleRates(mainChart.rangeChart().filter(), handleBenchmarkResponse, true);
            }
        });
    }


    function getOrdersFormSubmit(formEle) {
        var orderParams = {};
        // Get Form Field Values
        orderParams.ccyPair = ($("#ccyPair").val()) ? $("#ccyPair").val().split('/').join('') : null;
        orderParams.customer = $("#customer").val();
        orderParams.makertaker = $('#oMorT').val();
        var timeRange = $('#timeRange').val().split(' - ');
        var startTime = new Date(timeRange[0]);
        var endTime = new Date(timeRange[1]);
        orderParams.startTime = startTime.getTime() - (startTime.getTimezoneOffset() * 60000); // convert to GMT
        orderParams.endTime = endTime.getTime() - (endTime.getTimezoneOffset() * 60000); //convert to GMT
        orderParams.orderType = ($('#oType').val()) ? $('#oType').val() : null;

        mainFormParams = orderParams; // all form data is stored in the namespace global variable mainFormParams
        mainFormParams.proStreams = [];

        var proStreams = $('#provider').val();
        mainFormParams.proStreams.push(proStreams.split('/'));

        getOrdersWithTrades(orderParams);
    }

    // Landscape and Full Order-Trade Call based on time frame.
    function getOrdersWithTrades(oParams) {
        var ordersURL = IEM.cURL + "orderswithtrades/full1/org/" + oParams.customer + "/ccypair/" + oParams.ccyPair + "/from/" + oParams.startTime + "/to/" + oParams.endTime + "/ordertype/" + oParams.orderType + "/minamount/1/type/" + oParams.makertaker;
        setTimeout(function () {
            if (jQuery.active && globalWaitTag) {
                globalWaitTag = false;
                $('#loadingTimeModal').modal('show');
            } else {
                globalWaitTag = false;
                $('#loadingTimeModal').modal('hide');
            }
        }, 5000);

        if (oParams.endTime - oParams.startTime > 1800000) {
            ordersURL = IEM.cURL + "orderswithtrades/landscape/org/" + oParams.customer + "/ccypair/" + oParams.ccyPair + "/from/" + oParams.startTime + "/to/" + oParams.endTime + "/ordertype/" + oParams.orderType + "/minamount/1/type/" + oParams.makertaker;
            IEM.Utils.ajaxRequest(ordersURL, function (orderTrades) {
                $('.dataTablesContainer').hide();
                handleGetOrdersTradesSummaryResponse(orderTrades, [oParams.startTime, oParams.endTime]);
            }, /*
             function (txt) {
             alert("Problem fetching Orders & Trades...");
             },*/ 'Order-Trade-Summary', 'Getting Orders & Trades...');
        } else {
            IEM.Utils.ajaxRequest(ordersURL, function (orderTrades) {
                $('.dataTablesContainer').show();
                handleGetOrdersTradesResponse(orderTrades, [oParams.startTime, oParams.endTime]);
            }, /*
             function (txt) {
             alert("Problem fetching Orders & Trades...");
             },*/ 'Order-Trade-Details', "Getting Order & Trade Details..");
        }
    }

    function getStream(timeFrame, successHandler) {
        var startTime, endTime, provider, stream, ccyPair, tempExtent;
        startTime = timeFrame[0];
        endTime = timeFrame[1];
        provider = mainFormParams.proStreams[0][0];
        stream = mainFormParams.proStreams[0][1];
        ccyPair = mainFormParams.ccyPair;

        if (getStream.provider && getStream.provider == provider && getStream.stream && getStream.stream == stream) {
            if (globalRateExtent) {
                tempExtent = mergeExtents([globalRateExtent, timeFrame]);
                if (tempExtent[0] == globalRateExtent[0] && tempExtent[1] == globalRateExtent[1]) {
                    return;
                }
            }
        }
        globalRateExtent = timeFrame;
        getStream.provider = provider;
        getStream.stream = stream;
        var url = IEM.cURL + "rates1/";
        url += provider + '/' + stream + '/' + ccyPair + '/' + startTime + '/' + endTime;


        setTimeout(function () {
            if (jQuery.active && globalWaitTag) {
                globalWaitTag = false;
                $('#loadingTimeModal').modal('show');
            } else {
                globalWaitTag = false;
                $('#loadingTimeModal').modal('hide');
            }
        }, 5000);
        IEM.Utils.ajaxRequest(url, function (o) {
            if (o.type == 'error' || o.type == 'warning') {
                alert(o.type + ' : ' + o.errorMessage);
                return;
            }
            successHandler.call(this, o);
        }, /*
         function() {
         alert("Problem fetching Tick Data - Call Failed.");
         },*/ 'Stream Data for ' + provider + ' - ' + stream, "Getting Tick Data..."
                );

    }

    function fillRatesTable(rates) {
        var tableOptions = {}, orderId, order;

        tableOptions.columns = [
            {'sTitle': 'Time'},
            {'sTitle': 'Ask Price'},
            {'sTitle': 'Ask Size'},
            {'sTitle': 'Bid Price'},
            {'sTitle': 'Bid Size'},
            {'sTitle': 'Status'},
            {'sTitle': 'GUID'},
            {'sTitle': 'Tier'}
        ];
        var tableData = [];

        for (var i = 0, len = rates.length; i < len; i++) {
            var rate = rates[i];
            tableData.push([
                new Date(rate.tmstmp),
                rate.ask_price,
                rate.ask_size,
                rate.bid_price,
                rate.bid_size,
                rate.status,
                rate.guid,
                rate.lvl
            ]);
        }

        rateData = rates;
        rateTableData = tableData;
        IEM.Utils.createTable('ratesTable', 'rTable', 'ratesTableContainer', tableData, tableOptions);
    }

    // HandleRates handles the Rate API response. Draws just tier 1 graphs right now. Tier 2 & 3 graphs commented.
    function handleRates(rates) {
        fillRatesTable(rates);
        var ndx = crossfilter(rates);
        var tierDim = ndx.dimension(function (d) {
            return d.lvl;
        });
        var tier1AskData, tier2AskData, tier3AskData, tier4AskData, tier1BidData, tier2BidData, tier3BidData, tier4BidData;
        tier1AskData = tier1BidData = tierDim.filter(1).top(Infinity);
        tier2AskData = tier2BidData = tierDim.filter(2).top(Infinity);
        tier3AskData = tier3BidData = tierDim.filter(3).top(Infinity);
        tier4AskData = tier4BidData = tierDim.filter(4).top(Infinity);


        tier1AskData = addAskInactiveRates(tier1AskData);
        tier2AskData = addAskInactiveRates(tier2AskData);
        tier3AskData = addAskInactiveRates(tier3AskData);
        tier4AskData = addAskInactiveRates(tier4AskData);
        tier1BidData = addBidInactiveRates(tier1BidData);
        tier2BidData = addBidInactiveRates(tier2BidData);
        tier3BidData = addBidInactiveRates(tier3BidData);
        tier4BidData = addBidInactiveRates(tier4BidData);

        var ryex = mergeExtents([d3.extent(tier1AskData, function (d) {
                return d.ask_price;
            }),
            d3.extent(tier2AskData, function (d) {
                return d.ask_price;
            }),
            d3.extent(tier3AskData, function (d) {
                return d.ask_price;
            }),
            d3.extent(tier4AskData, function (d) {
                return d.ask_price;
            }),
            d3.extent(tier1BidData, function (d) {
                return d.bid_price;
            }),
            d3.extent(tier2BidData, function (d) {
                return d.bid_price;
            }),
            d3.extent(tier3BidData, function (d) {
                return d.bid_price;
            }),
            d3.extent(tier4BidData, function (d) {
                return d.bid_price;
            })]);

        var rxex = d3.extent(rates, function (d) {
            return new Date(d.tmstmp)
        });

        var r1Adx = crossfilter(tier1AskData);
        var r1ADim = r1Adx.dimension(function (d) {
            return d.date
        });
        var tier1AskGroup = r1ADim.group().reduce(
                function (p, v) {
                    if (p.rateData == null)
                        p.rateData = v;
                    return p;
                },
                function (p, v) {
                },
                function (p, v) {
                    return {rateData: null}
                }
        );

        var r2Adx = crossfilter(tier2AskData);
        var r2ADim = r2Adx.dimension(function (d) {
            return d.date
        });
        var tier2AskGroup = r2ADim.group().reduce(
                function (p, v) {
                    if (p.rateData == null)
                        p.rateData = v;
                    return p;
                },
                function (p, v) {
                },
                function (p, v) {
                    return {rateData: null}
                }
        );

        var r3Adx = crossfilter(tier3AskData);
        var r3ADim = r3Adx.dimension(function (d) {
            return d.date
        });
        var tier3AskGroup = r3ADim.group().reduce(
                function (p, v) {
                    if (p.rateData == null)
                        p.rateData = v;
                    return p;
                },
                function (p, v) {
                },
                function (p, v) {
                    return {rateData: null}
                }
        );

        var r4Adx = crossfilter(tier4AskData);
        var r4ADim = r4Adx.dimension(function (d) {
            return d.date
        });
        var tier4AskGroup = r4ADim.group().reduce(
                function (p, v) {
                    if (p.rateData == null)
                        p.rateData = v;
                    return p;
                },
                function (p, v) {
                },
                function (p, v) {
                    return {rateData: null}
                }
        );

        var r1Bdx = crossfilter(tier1BidData);
        var r1BDim = r1Bdx.dimension(function (d) {
            return d.date
        });
        var tier1BidGroup = r1BDim.group().reduce(
                function (p, v) {
                    if (p.rateData == null)
                        p.rateData = v;
                    return p;
                },
                function (p, v) {
                },
                function (p, v) {
                    return {rateData: null}
                }
        );

        var r2Bdx = crossfilter(tier2BidData);
        var r2BDim = r2Bdx.dimension(function (d) {
            return d.date
        });
        var tier2BidGroup = r2BDim.group().reduce(
                function (p, v) {
                    if (p.rateData == null)
                        p.rateData = v;
                    return p;
                },
                function (p, v) {
                },
                function (p, v) {
                    return {rateData: null}
                }
        );

        var r3Bdx = crossfilter(tier3BidData);
        var r3BDim = r3Bdx.dimension(function (d) {
            return d.date
        });
        var tier3BidGroup = r3BDim.group().reduce(
                function (p, v) {
                    if (p.rateData == null)
                        p.rateData = v;
                    return p;
                },
                function (p, v) {
                },
                function (p, v) {
                    return {rateData: null}
                }
        );

        var r4Bdx = crossfilter(tier4BidData);
        var r4BDim = r4Bdx.dimension(function (d) {
            return d.date
        });
        var tier4BidGroup = r4BDim.group().reduce(
                function (p, v) {
                    if (p.rateData == null)
                        p.rateData = v;
                    return p;
                },
                function (p, v) {
                },
                function (p, v) {
                    return {rateData: null}
                }
        );

        charts['tier1BidGraph'] = dc.lineChart(mainChart)
                .dimension(r1BDim)
                .group(tier1BidGroup, "Bid Price - Tier 1")
                .colors('#d62728')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.rateData.bid_price;
                })
                .renderDataPoints(true)
                .interpolate('step-after');
        charts['tier2BidGraph'] = dc.lineChart(mainChart)
                .dimension(r2BDim)
                .group(tier2BidGroup, "Bid Price - Tier 2")
                .colors('#9467bd')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.rateData.bid_price;
                })
                .renderDataPoints(true)
                .interpolate('step-after');

        charts['tier3BidGraph'] = dc.lineChart(mainChart)
                .dimension(r3ADim)
                .group(tier3BidGroup, "Bid Price - Tier 3")
                .colors('#8c564b')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.rateData.bid_price;
                })
                .renderDataPoints(true)
                .interpolate('step-after');
        charts['tier4BidGraph'] = dc.lineChart(mainChart)
                .dimension(r4ADim)
                .group(tier4BidGroup, "Bid Price - Tier 4")
                .colors('#750000')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.rateData.bid_price;
                })
                .renderDataPoints(true)
                .interpolate('step-after');

        charts['tier1AskGraph'] = dc.lineChart(mainChart)
                .dimension(r1ADim)
                .group(tier1AskGroup, "Ask Price - Tier 1")
                .colors('#1f77b4')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.rateData.ask_price;
                })
                .renderDataPoints(true)
                .interpolate('step-after');

        charts['tier2AskGraph'] = dc.lineChart(mainChart)
                .dimension(r2ADim)
                .group(tier2AskGroup, "Ask Price - Tier 2")
                .colors('#ff7f0e')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.rateData.ask_price;
                })
                .renderDataPoints(true)
                .interpolate('step-after');

        charts['tier3AskGraph'] = dc.lineChart(mainChart)
                .dimension(r3ADim)
                .group(tier3AskGroup, "Ask Price - Tier 3")
                .colors('#2ca02c')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.rateData.ask_price;
                })
                .renderDataPoints(true)
                .interpolate('step-after');

        charts['tier4AskGraph'] = dc.lineChart(mainChart)
                .dimension(r4ADim)
                .group(tier4AskGroup, "Ask Price - Tier 4")
                .colors('#004444')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.rateData.ask_price;
                })
                .renderDataPoints(true)
                .interpolate('step-after');

        mainChartYExtent = adjustYExtent(mergeExtents([mainChartYExtent, ryex]));

        mainChart.compose(getChartArrFromObj(charts))
                .y(d3.scale.linear().domain(mainChartYExtent))
                .legend(dc.legend().x(1080).y(50).itemWidth(130).legendWidth(150).gap(5));

        mainChart.render();
        handleExtent(mainChart);
    }

    // This function handles inactive rates. If there is an inactive rate for a particular timestamp, then change it to the previous rate without changing it to 0.
    function addAskInactiveRates(data) {
        var lvlAskPrice;
        var newData = [];

        data.forEach(function (d) {
            d.date = new Date(d.tmstmp);
            if (d.ask_price != 0) {
                newData.push(d);
                lvlAskPrice = d.ask_price;
            }
            if (d.status == 'InActive') {
                if (lvlAskPrice) {
                    d.ask_price = lvlAskPrice;
                    newData.push(d);
                }
            }
        });
        return newData;
    }

    // Same as the previous function but for bid rates
    function addBidInactiveRates(data) {
        var lvlBidPrice;
        var newData = [];

        data.forEach(function (d) {
            d.date = new Date(d.tmstmp);
            if (d.bid_price != 0) {
                newData.push(d);
                lvlBidPrice = d.bid_price;
            }
            if (d.status == 'InActive') {
                if (lvlBidPrice) {
                    d.bid_price = lvlBidPrice;
                    newData.push(d);
                }
            }
        });
        return newData;
    }

    function getBenchmarkSampleRates(timeFrame, successHandler, getProfileAndTick, callBack) {
        var startTime = timeFrame ? new Date(timeFrame[0]).getTime() : mainFormParams.startTime;
        var endTime = timeFrame ? new Date(timeFrame[1]).getTime() : mainFormParams.endTime;
        var ccyPair = mainFormParams.ccyPair;

        var count = 600;

        if (endTime - startTime < 2000) {
            startTime -= 1000;
            endTime += 1000;
        }
        if (endTime - startTime < 600000) {
            count = parseInt((endTime - startTime) / 1000);
        }

        var url = IEM.cURL + 'benchmarks1/sample/' + count + '/ccypair/';
        url += ccyPair + '/fromtime/' + startTime + '/totime/' + endTime + '/tier/1';

        // Simple caching mechanism. If 2 consecutive benchmark URLs are same, then no need to make the call.
        /* if (getBenchmarkSampleRates.url == url) {
         successHandler.call(this, benchmarkData);
         if (endTime - startTime <= 600000 && getProfileAndTick) {
         getStream([startTime, endTime], handleRates);
         return;
         }
         return;
         } else {*/
        getBenchmarkSampleRates.url = url;
        //  }
        setTimeout(function () {
            if (jQuery.active && globalWaitTag) {
                globalWaitTag = false;
                $('#loadingTimeModal').modal('show');
            } else {
                globalWaitTag = false;
                $('#loadingTimeModal').modal('hide');
            }
        }, 5000);
        IEM.Utils.ajaxRequest(url, function (o) {
            successHandler.call(this, o);
            if (endTime - startTime <= 600000 && getProfileAndTick) {
                getStream([startTime, endTime], handleRates);
                return;
            }
            if (typeof callBack != 'undefined') {
                callBack.apply(this, []);
            }
        },
                function () {
                    alert("Problem in retrieving Benchmark Rates");
                }, 'Benchmark Rates', 'Getting Benchmark Rates...');
        globalWaitTag = false;
    }

    function handleBenchmarkResponse(benchmark) {
        if (!benchmark.length) {
            //alert("Benchmark Data Not Available");  will be added later when streams is seperated from benchmark response.
            $('#benchmarkTable').find('tbody > tr').remove();
            return;
        }
        var tableOptions = {}, tradeId, trade;

        tableOptions.columns = [
            {'sTitle': 'Time'},
            {'sTitle': 'Median'},
            {'sTitle': 'Range'}
        ];
        var tableData = [], rate;

        for (var i = 0, len = benchmark.length; i < len; i++) {
            rate = benchmark[i];

            tableData.push([
                new Date(rate.timeStamp),
                rate.medianMidPrice,
                rate.midPriceRange
            ]);
        }

        tableData.sort(function (a, b) {
            return Number(a[0]) - Number(b[0]);
        });

        benchmarkData = benchmark;
        benchmarkTableData = tableData;
        IEM.Utils.createTable('benchmarkTable', 'bTable', 'benchmarkTableContainer', tableData, tableOptions);
        drawDCBenchmarkGraph();
    }

    function handleGetOrdersTradesResponse(ordersAndTrades, timeFrame) {

        var tableOptions = {}, orderId, order;
        if (!ordersAndTrades) {
            return;
        }
        orderData = ordersAndTrades['OrderSummary'];
        tradeData = ordersAndTrades['TradeSummary'];

        tableOptions.columns = [
            {'sTitle': 'Time'},
            {'sTitle': 'Order ID'},
            {'sTitle': 'Buy/Sell'},
            {'sTitle': 'Currency Pair'},
            {'sTitle': 'Order Rate'},
            {'sTitle': 'Fill Rate'},
            {'sTitle': 'Filled Amount',
                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).text($(nTd).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                }
            },
            {'sTitle': 'Filled %age'},
            {'sTitle': 'Order Amount',
                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).text($(nTd).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                }
            },
            {'sTitle': 'Order Type'},
            {'sTitle': 'Org'}
        ];
        var tableData = [];

        for (var i = 0, len = orderData.length; i < len; i++) {

            order = orderData[i];

            var rate = parseFloat(order.OrderRate);

            //handling incorrect values of format "Range 0.00100000 from 98.40499878"
            if (isNaN(rate)) {
                var actualRate = order.OrderRate;
                var index = actualRate.indexOf('from ');
                if (index != -1) {
                    rate = parseFloat(actualRate.substr(index + 5, actualRate.length));
                    order.OrderRate = rate;
                }
            }

            tableData.push([
                new Date(order.Created),
                /*
                 '<a target="_blank" href="https://live8.fxinside.net/grdmon/integral/Monitor/util/Search.jsp?searchType=order&txid=' + order.OrderID + '&userOrg=MAIN">' + order.OrderID + '</a>',
                 */
                '<a target="_blank" href="./home1.jsp?orderid=' + order.OrderID + '">' + order.OrderID + '</a>',
                order.BuySell,
                order.CcyPair,
                order.OrderRate,
                order.FillRate,
                order.FilledAmt,
                order.FilledPct,
                order.OrderAmt,
                order.OrderType,
                order.Org
            ]);
        }

        tableData.sort(function (a, b) {
            return Number(a[0]) - Number(b[0]);
        });

        orderTableData = tableData;
        $('#blotterTabs').show();
        IEM.Utils.createTable('ordersTable', 'oTable', 'ordersTableContainer', tableData, tableOptions);
        drawDCOrderGraph(timeFrame);
        getBenchmarkSampleRates((timeFrame ? timeFrame : [mainFormParams.startTime, mainFormParams.endTime]), handleBenchmarkResponse, true);
        processTradeData();
    }

    function handleGetOrdersTradesSummaryResponse(ordersAndTrades, timeFrame) {
        orderData = ordersAndTrades['OrderSummary'];
        tradeData = ordersAndTrades['TradeSummary'];
        var order, len;

        for (var i = 0, len = orderData.length; i < len; i++) {
            order = orderData[i];

            var rate = parseFloat(order.OrderRate);

            //handling incorrect values of format "Range 0.00100000 from 98.40499878"
            if (isNaN(rate)) {
                var actualRate = order.OrderRate;
                var index = actualRate.indexOf('from ');
                if (index != -1) {
                    rate = parseFloat(actualRate.substr(index + 5, actualRate.length));
                    order.OrderRate = rate;
                }
            }
        }

        drawDCOrderGraph(timeFrame);
        //IEM.Utils.createTable('ordersTable', 'oTable', 'ordersTableContainer', [], {columns: ''});
        getBenchmarkSampleRates((timeFrame ? timeFrame : [mainFormParams.startTime, mainFormParams.endTime]), handleBenchmarkResponse, true);
        drawDCTradeGraph();
        //IEM.Utils.createTable('tradesTable', 'tTable', 'tradesTableContainer', [], {columns: ''});
    }

    function processTradeData() {
        var tableOptions = {}, tradeId, trade;

        tableOptions.columns = [
            {'sTitle': 'Time'},
            {'sTitle': 'Order ID'},
            {'sTitle': 'Trade ID'},
            {'sTitle': 'Status'},
            {'sTitle': 'Buy/Sell'},
            {'sTitle': 'Provider - Stream'},
            {'sTitle': 'Currency Pair'},
            {'sTitle': 'Rate'},
            {'sTitle': 'Org'},
            {'sTitle': 'Type'},
            {'sTitle': 'Maker/Taker'}
        ];

        tableOptions.fnRowCallback = function (nRow, aaData, iDisplayIndex) {
            var className = '';
            if (aaData[3] == 'Confirmed') {
                className = 'success';
            } else if (aaData[3] == 'Rejected') {
                className = 'rejected';
            } else if (aaData[3] == 'Failed') {
                className = 'failed';
            }
            $(nRow).addClass(className);
        };

        var tableData = [];
        var tradeDetailsMap = IEM.Mapping.getMap('tradeDetailFieldMap');
        for (var i = 0, len = tradeData.length; i < len; i++) {
            trade = tradeData[i];
            tradeData[i].TRADESTATUS = tradeStatusMap[trade.TRADESTATUS] || trade.TRADESTATUS;

            tableData.push([
                new Date(trade.EXECTIME),
                /* '<a target="_blank" href="https://live8.fxinside.net/grdmon/integral/Monitor/util/Search.jsp?searchType=order&txid=' + trade.ORDERID + '&userOrg=MAIN">' + trade.ORDERID + '</a>',
                 '<a target="_blank" href="https://live8.fxinside.net/grdmon/integral/Monitor/util/Search.jsp?searchType=deal&txid=' + trade.TRADEID + '&userOrg=MAIN">' + trade.TRADEID + '</a>',*/
                '<a target="_blank" href="./home1.jsp?orderid=' + trade.ORDERID + '">' + trade.ORDERID + '</a>',
                '<a target="_blank" href="./tradeDetails.html?orderId=' + trade.ORDERID + '&tradeId=' + trade.TRADEID + '">' + trade.TRADEID + '</a>',
                trade.TRADESTATUS,
                format(tradeDetailsMap[trade.BUYSELL]),
                trade.CPTY + ' - ' + trade.TickStream,
                trade.CCYPAIR,
                trade.RATE,
                trade.ORG,
                trade.TYPE,
                trade.MKRTKR
            ]);
        }

        tableData.sort(function (a, b) {
            return Number(a[0]) - Number(b[0]);
        });

        tradeTableData = tableData;
        IEM.Utils.createTable('tradesTable', 'tTable', 'tradesTableContainer', tableData, tableOptions);
        drawDCTradeGraph();
    }

    /*function collide(node, node_sibling) {
     var node_dim = node.parentNode.getAttribute('transform').match(/[-.0-9]+/g);
     var sibling_dim = node_sibling.parentNode.getAttribute('transform').match(/[-.0-9]+/g);
     var r = node.getAttribute('r'),
     n1y1 = parseFloat(node_dim[0]) - parseFloat(r),
     n1y2 = parseFloat(node_dim[0]) + parseFloat(r);
     var r2 = node_sibling.getAttribute('r'),
     n2y1 = parseFloat(sibling_dim[0]) - parseFloat(r2),
     n2y2 = parseFloat(sibling_dim[0]) + parseFloat(r2);
     result = (n1y1 < n2y2 && n1y1 > n2y1) || (n1y2 > n2y2 && n1y2 < n2y1);
     
     return result;
     }
     
     function detectCollisions(nodes) {
     nodes.forEach(function(d, index) {
     for (var i=0, len=nodes.length; i < nodes.length; i++) {
     if(d != nodes[i]) {
     if(collide(d, nodes[i])) {
     console.log(d);
     }
     }
     }
     });
     } */

    function drawDCOrderGraph(timeFrame) {
        charts = {};
        globalRateExtent = null;
        mainChart = dc.compositeChart('#chartContainer');
        var data = [], order;
        for (var i = 0, len = orderData.length; i < len; i++) {
            order = orderData[i];
            order.date = new Date(order.Created)
            data.push(order);
        }

        var ndx = crossfilter(data);
        var dateDim = ndx.dimension(function (d) {
            return d.date;
        });
        var ratesGroup = dateDim.group().reduce(
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.OrderID = v.OrderID;
                    p.OrderAmt = v.OrderAmt;
                    p.OrderRate = v.OrderRate;
                    p.Date = v.date;
                    return p;
                },
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.OrderID = v.OrderID;
                    p.OrderAmt = v.OrderAmt;
                    p.OrderRate = v.OrderRate;
                    p.Date = v.date;
                    return p;
                },
                function (p, v) {
                    return {
                        OrderID: 0,
                        OrderAmt: 0,
                        OrderRate: 0,
                        Date: 0,
                        data: null
                    }
                }
        );

        var yExtent = d3.extent(data, function (d) {
            return d.OrderRate;
        });
        yExtent = adjustYExtent(yExtent);

        /**
         * Click event : Pushing the values to array fot tick values
         * @author    Anil    <nl.saini1@gmail.com >     
         */
        createTick();
        function createTick() {
            if (timeFrame) {
                var diffTime = (timeFrame[1] - timeFrame[0]) / 10;
                arrayTickValues = [];
                arrayTickValues.push(new Date(timeFrame[0]));
                for (var i = 1; i < 11; i++) {
                    arrayTickValues.push((new Date(timeFrame[0] + diffTime * i)));
                }
                return arrayTickValues;
            } else {
                var diffTime = (mainFormParams.endTime - mainFormParams.startTime) / 10;
                arrayTickValues = [];
                arrayTickValues.push(new Date(mainFormParams.startTime))
                for (var i = 1; i < 11; i++) {
                    arrayTickValues.push((new Date(mainFormParams.startTime + diffTime * i)));
                }
                return arrayTickValues;
            }
        }

        mainChartYExtent = yExtent;

        if (!timeFrame || (timeFrame[0] == mainFormParams.startTime && timeFrame[1] == mainFormParams.endTime)) {
            mainZoomChart = dc.lineChart('#zoomChart');
            mainZoomChart
                    .width(1200)
                    .height(50)
                    .renderArea(true)
                    .margins({top: 0, right: 150, bottom: 20, left: 72})
                    .dimension(dateDim)
                    .group(ratesGroup)
                    .keyAccessor(function (p) {
                        return p.key;
                    })
                    .valueAccessor(function (p) {
                        return p.value.OrderRate;
                    })
                    .x(d3.time.scale().domain([new Date(mainFormParams.startTime), new Date(mainFormParams.endTime)]))
                    .y(d3.scale.linear().domain(yExtent))
                    .xAxis().tickValues([arrayTickValues[0], arrayTickValues[1], arrayTickValues[2],
                arrayTickValues[3], arrayTickValues[4], arrayTickValues[5],
                arrayTickValues[6], arrayTickValues[7], arrayTickValues[8],
                arrayTickValues[9], arrayTickValues[10]])
                    .tickFormat(function (d, i) {
                        return d3.time.format.utc("%H:%M:%S.%L")(d);
                    });
            mainZoomChart.render();
        }

        var rExtent = [0, 100];

        charts['ordersChart'] = dc.bubbleChart(mainChart)
                .dimension(dateDim)
                .group(ratesGroup, "Orders")
                .colors('#7F7F7F')
                .keyAccessor(function (p) {
                    return p.key;
                })
                .valueAccessor(function (p) {
                    return p.value.OrderRate;
                })
                .radiusValueAccessor(function (p) {
                    return 60;
                })
                .maxBubbleRelativeSize(0.00005)
                .r(d3.scale.linear().domain(rExtent))
                //.renderTitle(true)
                //.title(function(d) {
                //    return d.key + '\nOrder ID: ' + d.value.OrderID + '\nOrder Rate: ' + d.value.OrderRate;
                //})
                .renderLabel(false);

        mainChart
                .width(1200)
                .height(600)
                .transitionDuration(250)
                .margins({top: 30, right: 150, bottom: 20, left: 60})
                .yAxisLabel("Rate")
                .x(d3.time.scale().domain(timeFrame || [new Date(mainFormParams.startTime), new Date(mainFormParams.endTime)]))
                .y(d3.scale.linear().domain(yExtent))
                .renderHorizontalGridLines(true)
                .renderVerticalGridLines(true)
                .rangeChart(mainZoomChart)
                .elasticY(false)
                .brushOn(false)
                .renderTitle(true)
                .title(mainChartTitle)
                .compose(getChartArrFromObj(charts))
                .on("zoomed", function (chart) {
                    handleExtent(chart);
                })
                .on("zoomend", function (chart) {
                    var diffTime = mainFormParams.endTime - mainFormParams.startTime;
                    var xExtent = chart.rangeChart().filter();
                    if (xExtent) {
                        getOrdersWithTrades(
                                {
                                    startTime: xExtent[0].getTime(),
                                    endTime: xExtent[1].getTime(),
                                    customer: mainFormParams.customer,
                                    ccyPair: mainFormParams.ccyPair,
                                    orderType: mainFormParams.orderType,
                                    makertaker: mainFormParams.makertaker
                                }
                        )
                    }
                })
                .renderlet(function (chart) {
                    // Code to mark Inactive Rates
                    var dots = d3.select(chart.g().node()).selectAll('g.sub').selectAll('circle');
                    var xExtent = chart.rangeChart().filter();
                    var yExtent = mainChartYExtent;
                    if (xExtent && yExtent) {
                        var xDomain = d3.time.scale().domain(xExtent);
                        var yDomain = d3.scale.linear().domain(yExtent);
                        d3.selectAll('svg g path.inactivePath').data([]).exit().remove();
                        dots.each(function (d, i) {
                            if (d.data && d.data.value && d.data.value.rateData && d.data.value.rateData.status == 'InActive') {
                                var nextElement = this.nextElementSibling, nextData;
                                d3.select(nextElement).each(function (d) {
                                    nextData = d;
                                });
                                var lineData = [
                                    {
                                        x: chart.x()(d.x) + chart.margins().left,
                                        y: chart.y()(d.y) + chart.margins().top
                                    },
                                    {
                                        x: chart.x()(nextData.x) + chart.margins().left,
                                        y: chart.y()(nextData.y) + chart.margins().top
                                    }
                                ];
                                var line = d3.svg.line()
                                        .x(function (d) {
                                            return d.x;
                                        })
                                        .y(function (d) {
                                            return d.y;
                                        })
                                        .interpolate("step-after");

                                d3.select('svg g').append('path')
                                        .attr('class', 'inactivePath')
                                        .attr('stroke', 'rgb(0,0,0)')
                                        .attr('d', line(lineData))
                                        .attr('fill', 'none');
                                //.attr("x1", chart.x()(d.x) + chart.margins().left).attr('y1', chart.y()(d.y) + chart.margins().top).attr('x2', chart.x()(nextData.x)+chart.margins().left).attr('y2', chart.y()(nextData.y)+chart.margins().top).style('stroke', 'rgb(0,0,0)').interpolate('step-after');
                            }
                        });
                    }
                })
                .legend(dc.legend().x(1080).y(50).itemWidth(130).legendWidth(150).gap(5))
                .xAxis().tickValues(function () {
            return createTick();
        })
                .tickFormat(function (d, i) {
                    return d3.time.format.utc("%H:%M:%S.%L")(d);
                });

        mainChart.render();
    }

    // Title function for the graph
    function mainChartTitle(d) {
        if (typeof d.value.TRADEID != 'undefined') {
            return (d.value.TRADEID == d.key ? new Date(d.value.Date).toString() : d.key) + '\nTrade ID: ' + d.value.TRADEID + '\nRate: ' + d.value.RATE + '\nOrder ID: ' + d.value.ORDERID + (d.value.data.BUYSELL ? '\nBuy/Sell: ' + d.value.data.BUYSELL : '');
        } else if (typeof d.value.rateData != 'undefined') {
            return d.key + '\nAsk Price: ' + d.value.rateData.ask_price + '\nBid Price: ' + d.value.rateData.bid_price + '\nAsk Size: ' + getFormattedAmount(d.value.rateData.ask_size) + '\nBid Size: ' + getFormattedAmount(d.value.rateData.bid_size) + '\nTier: ' + d.value.rateData.lvl + '\nStatus: ' + d.value.rateData.status;
        } else if (typeof d.value.benchmarkData != 'undefined') {
            return new Date(d.key).toString() + '\nBenchmark Price: ' + d.value.benchmarkData.medianMidPrice + '\nRange: ' + d.value.benchmarkData.midPriceRange;
        }
        return d.key + '\nOrder ID: ' + d.value.OrderID + '\nOrder Rate: ' + d.value.OrderRate;
    }

    function drawDCTradeGraph() {
        var data = {
            'Confirmed': [],
            'Rejected': [],
            'Failed': [],
            'Cancelled': [],
            'Verified': [],
            'Other': []
        };
        var tradeStatusMap = IEM.Mapping.getMap('TradeStatusMap');

        for (var i = 0, len = tradeData.length; i < len; i++) {
            var trade = tradeData[i];
            var status = tradeStatusMap[trade.TRADESTATUS] || trade.TRADESTATUS;
            trade['key'] = trade.TRADEID;
            trade['Type'] = 'Trades';
            trade['date'] = new Date(trade.EXECTIME);
            if (status == 'Confirmed' || status == 'Rejected' || status == 'Failed' || status == 'Cancelled' || status == 'Verified') {
                data[status].push(trade);
            } else {
                data['Other'].push(trade);
            }
        }

        for (var tradeType in data) {
            if (data[tradeType].length) {
                plotTrades(data[tradeType], tradeType);
            }
        }
    }

    function plotTrades(data, type) {
        var rateExtent = d3.extent(data, function(d) {
            return d.RATE;
        });
        var yExtent = yExtent || mainChartYExtent;
        var cex1 = mergeExtents([yExtent, rateExtent]);
        yExtent = adjustYExtent(cex1);
        
        var ndx = crossfilter(data);
        var dateDim = ndx.dimension(function (d) {
            return d.date;
        });
        var ratesGroup = dateDim.group().reduce(
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.TRADEID = v.TRADEID;
                    p.ORDERID = v.ORDERID;
                    p.RATE = v.RATE;
                    p.Date = v.date;
                    return p;
                },
                function (p, v) {
                    if (!p) {
                        p = {};
                    }
                    p.data = v;
                    p.OrderID = v.OrderID;
                    p.OrderAmt = v.OrderAmt;
                    p.OrderRate = v.OrderRate;
                    p.Date = v.date;
                    return p;
                },
                function (p, v) {
                    return {
                        TRADEID: 0,
                        ORDERID: 0,
                        RATE: 0,
                        Date: 0,
                        data: null
                    };
                }
        );

        charts['trades-' + type + 'Chart'] = dc.bubbleChart(mainChart)
                .dimension(dateDim)
                .group(ratesGroup, "Trades (" + type + ")")
                .keyAccessor(function (p) {
                    return p.key;
                })
                .colors(tradeColorCodes[type])
                .valueAccessor(function (p) {
                    return p.value.RATE;
                })
                .radiusValueAccessor(function (p) {
                    return 80;
                })
                .maxBubbleRelativeSize(0.00005)
                .r(d3.scale.linear().domain([0, 100]))
                .renderLabel(false);

        mainChart.compose(getChartArrFromObj(charts))
                .y(d3.scale.linear().domain(yExtent))
                .legend(dc.legend().x(1080).y(50).itemWidth(130).legendWidth(150).gap(5));
        mainChart.render();
    }

    function drawDCBenchmarkGraph() {
        delete(charts['tier1AskGraph']);
        delete(charts['tier2AskGraph']);
        delete(charts['tier3AskGraph']);
        delete(charts['tier4AskGraph']);

        delete(charts['tier1BidGraph']);
        delete(charts['tier2BidGraph']);
        delete(charts['tier3BidGraph']);
        delete(charts['tier4BidGraph']);

        for (var i = 0, len = benchmarkData.length; i < len; i++) {
            benchmarkData[i].date = new Date(benchmarkData[i].timeStamp);
        }
        ;
        var rateExtent = d3.extent(benchmarkData, function (d) {
            return d.medianMidPrice;
        });
        var yExtent = yExtent || mainChartYExtent;
        var cex = mergeExtents([yExtent, rateExtent]);
        yExtent = adjustYExtent(cex);

        var ndx = crossfilter(benchmarkData);
        var dateDim = ndx.dimension(function (d) {
            return d.timeStamp;
        });
        var bmGroup = dateDim.group().reduce(
                function (p, v) {
                    p.benchmarkData = v;
                    return p;
                },
                function (p, v) {
                },
                function (p, v) {
                    return {benchmarkData: null}
                }
        );

        charts['benchmarkGraph'] = dc.lineChart(mainChart)
                .dimension(dateDim)
                .group(bmGroup, "Benchmark")
                .keyAccessor(function (p) {
                    return p.key;
                })
                .colors('#FF00DD')
                .valueAccessor(function (p) {
                    return p.value.benchmarkData.medianMidPrice;
                })
                //.renderDataPoints(true)
                .interpolate('step-before');

        mainChart.compose(getChartArrFromObj(charts))
                .y(d3.scale.linear().domain(yExtent))
                .render();

        handleExtent(mainChart);
    }

    // Convert charts object to array for use in compose method for DC composite charts
    function getChartArrFromObj(charts) {
        var chartArr = [];
        for (var chart in charts) {
            if (charts.hasOwnProperty(chart))
                chartArr.push(charts[chart]);
        }
        return chartArr;
    }

    // adjust Y extent to give some padding to Y axis
    function adjustYExtent(ext) {
        ext = [parseFloat(ext[0]), parseFloat(ext[1])];
        if (ext[0] == ext[1]) {
            var diff = (ext[0] - parseInt(ext[0])) / 100;
            return [ext[0] - diff, ext[1] + diff];
        }
        return [parseFloat(ext[0]) - (ext[1] - ext[0]) / 20, parseFloat(ext[1]) + (ext[1] - ext[0]) / 20];
    }

    // adjust X extent to give some padding to X axis
    function adjustXExtent(ext) {
        ext = convertDateExtent(ext);
        if (ext[0] == ext[1]) {
            return [new Date(ext[0] - 5000), new Date(ext[0] + 5000)];
        }
        var diff = ext[1] - ext[0];
        if (diff / 10 > 1) {
            diff = diff / 10;
        }
        return [new Date(ext[0] - diff), new Date(ext[1] + diff)];
    }

    function convertDateExtent(ext) {
        if (ext[0] instanceof Date) {
            ext = [ext[0].getTime(), ext[1].getTime()];
        }
        return ext;
    }

    // Just combine multiple extents into one
    function mergeExtents(extents) {
        var min = [], max = [];
        extents.forEach(function (d) {
            (!isNaN(d[0])) && min.push(d[0]);
            (!isNaN(d[1])) && max.push(d[1]);
        });
        return [Math.min.apply(null, min), Math.max.apply(null, max)];
    }

    // Handling the extent on zooming the chart and on rendering.
    // TODO: Can be rewritten in a better way
    function handleExtent(chart) {
        chart = chart || mainChart;
        var chartChildren = chart.children(), extentArr = [];
        if (chartChildren.length <= 1) {
            return;
        }
        for (var i = 0, len = chartChildren.length; i < len; i++) {
            var child = chartChildren[i];
            if (child.data()[0]) {
                if (child.data()[0].values) {
                    extentArr.push(d3.extent(chartChildren[i].data()[0].values, function (d) {
                        return d.y;
                    }));
                } else if (child.data()[0].value.OrderID && chart.rangeChart()) {
                    var data = child.dimension().filter(chart.rangeChart().filter()).top(Infinity);
                    if (data.length)
                        extentArr.push(d3.extent(data, function (d) {
                            return d.OrderRate;
                        }));
                }
            }
        }
        var yExtent = mergeExtents(extentArr);
        yExtent = adjustYExtent(yExtent);
        chart.y(d3.scale.linear().domain(yExtent));
        chart.redraw();
    }

    return {
        setupGetOrdersFormSubmit: setupGetOrdersFormSubmit
    }
})();
//Landscape Mode - More than an hour - separate orders/trades call - just getting summary. Benchmark Data shown. No Tick Data
//Zoomedin Mode1 - <= 30 min && >= 10 min - Orders/Trades Detailed Call, showing blotter tables with all info, Only benchmark Shown, No Tick Data
//Zoomedin Mode2 - <=10min - Orders/Trades Detailed Call, show blotter Tabs, Benchmark & Tickdata shown

//Utility function for adding zero to date & time when required.
function addZeroIfNeeded(number) {
    if (number <= 9) {
        return '0' + number;
    }
    return number;
}


// Date String function. Specific format for EMScope. This always returns time in GMT. using getTimezoneOffset to handle this.
function dateToString() {
    var dateObj = this;
    var timezoneOffset = dateObj.getTimezoneOffset()*60000;
    dateObj = new Date(dateObj.getTime() + timezoneOffset);
    var displayDate = dateObj.getFullYear() + '-' + addZeroIfNeeded(dateObj.getMonth() + 1) + '-' + addZeroIfNeeded(dateObj.getDate()) + ' ' + addZeroIfNeeded(dateObj.getHours()) + ':' + addZeroIfNeeded(dateObj.getMinutes()) + ':' + addZeroIfNeeded(dateObj.getSeconds());
    var milliseconds = dateObj.getMilliseconds().toString();
    if (milliseconds.length < 3) {
        for (var i=0, len = 3-milliseconds.length, str = ''; i<len;i++) {
            str += '0';
        }
        milliseconds = str + milliseconds;
    }
    displayDate += '.' + milliseconds;
    return displayDate;
}

Date.prototype.toString = dateToString;

IEM.OC = (function() {
     var profileUrl ;
    // main function which sets up the forms in Order Collection Tab
    function setupGetOrderCollectionFormSubmit() {
        $('form[name="getOrderCollectionForm"]').submit(function(e){
            e.preventDefault();
            // TODO : check for valid entries
            var date =  $('#searchDate').val();
            if(date == "" || date =='undefined' || date==null){
                alert("Invalid Date ");
                return;
            }
            getOrderCollectionsFormSubmit();
        });
    }


    function getOrderCollectionsFormSubmit(formEle) {
        var OrderCollectionParams = {};

        // Get Form Field Values
       // OrderCollectionParams.ccyPair = ($("#searchCcyPair").val());
        var ccyPairList='';
        var ccyList = $("#searchCcyPair").val();
        if (ccyList != 'undefined' && ccyList != null && ccyList != '')
        {
            for ( var j = 0, tradelen = ccyList.length; j < tradelen; j++ )
            {
                ccyPairList=ccyPairList + ccyList[j].split('/').join('');
                if (j != (ccyList.length - 1))
                {
                    ccyPairList=ccyPairList +',';
                }
            }
        }
        else
        {
            ccyPairList='none';
        }

        var channelList='';
        var chanList = $("#searchChannel").val();
        if (chanList != 'undefined' && chanList != null && chanList != '')
        {
            for ( var k = 0, chanlen = chanList.length; k < chanlen; k++ )
            {
                channelList=channelList + chanList[k].split('/').join('-');
                if (k != (chanList.length - 1))
                {
                    channelList=channelList +',';
                }
            }
        }
        else
        {
            channelList='none';
        }


        var ordertypeList='';
        var ordtypeList = $("#searchOrderType").val();
        if (ordtypeList != 'undefined' && ordtypeList != null && ordtypeList != '')
        {
            for ( var k = 0, ordtypelen = ordtypeList.length; k < ordtypelen; k++ )
            {
                ordertypeList=ordertypeList + ordtypeList[k].split('/').join('-');
                if (k != (ordtypeList.length - 1))
                {
                    ordertypeList=ordertypeList +',';
                }
            }
        }
        else
        {
            ordertypeList='none';
        }

        var dateRangeList='';
        var dateList = $("#searchDate").val();
        if (dateList != 'undefined' && dateList != null && dateList != '')
        {
            for ( var l = 0, datelen = dateList.length; l < datelen; l++ )
            {
                var startime1 = new Date(dateList[l]+' 00:00:00' );
                var endtime1 = new Date(dateList[l]+' 11:59:00' );
                var startTime1 = startime1.getTime() - (startime1.getTimezoneOffset()*60000); // convert to GMT
                var endTime1 = endtime1.getTime() - (endtime1.getTimezoneOffset()*60000);
                dateRangeList = dateRangeList +startTime1+':'+endTime1;

                if (l != (dateList.length - 1))
                {
                    dateRangeList=dateRangeList +',';
                }
            }
        }
        else
        {
            dateRangeList='none';
        }

        OrderCollectionParams.ccyPair = ccyPairList;//($("#searchCcyPair").val()) ? $("#searchCcyPair").val()[0].split('/').join('') : 'none' ;


        var starttime = new Date($('#searchDate').val()+' 00:00:00' );
        var endtime = new Date($('#searchDate').val()+' 11:59:00' );
        OrderCollectionParams.timeRange= dateRangeList;
        OrderCollectionParams.startTime = starttime.getTime() - (starttime.getTimezoneOffset()*60000); // convert to GMT
        OrderCollectionParams.endTime = endtime.getTime() - (endtime.getTimezoneOffset()*60000);
        /*OrderCollectionParams.startTime= starttime;
        OrderCollectionParams.endTime= endtime;*/
        OrderCollectionParams.takerOrg=$('#searchTakerOrg' ).val() != 'undefined'
                                        && $('#searchTakerOrg' ).val() != null
                                        && $('#searchTakerOrg' ).val() != ''?
                                           $('#searchTakerOrg' ).val():'none';

        OrderCollectionParams.status=$('#searchStatus' ).val() != 'undefined'
            && $('#searchStatus' ).val() != null
            && $('#searchStatus' ).val() != ''?
            $('#searchStatus' ).val():'none';

        OrderCollectionParams.orderType=ordertypeList;/*$('#searchOrderType' ).val() != 'undefined'
            && $('#searchOrderType' ).val() != null
            && $('#searchOrderType' ).val() != ''?
            $('#searchOrderType' ).val():'none';*/

        OrderCollectionParams.buysell=$('#searchBuySell' ).val() != 'undefined'
            && $('#searchBuySell' ).val() != null
            && $('#searchBuySell' ).val() != ''?
            $('#searchBuySell' ).val():'none';

        OrderCollectionParams.origOrg=$('#searchOrigOrg' ).val() != 'undefined'
            && $('#searchOrigOrg' ).val() != null
            && $('#searchOrigOrg' ).val() != ''?
            $('#searchOrigOrg' ).val():'none';

        OrderCollectionParams.channel=channelList;/*$('#searchChannel' ).val() != 'undefined'
            && $('#searchChannel' ).val() != null
            && $('#searchChannel' ).val() != ''?
            $('#searchChannel' ).val()[0].replace('/','-'):'none';*/
        OrderCollectionParams.tif=$('#searchTif' ).val() != 'undefined'
            && $('#searchTif' ).val() != null
            && $('#searchTif' ).val() != ''?
            $('#searchTif' ).val():'none';
        OrderCollectionParams.orderBy=$('#searchOrderBy' ).val() != 'undefined'
            && $('#searchOrderBy' ).val() != null
            && $('#searchOrderBy' ).val() != ''?
            $('#searchOrderBy' ).val():'none';
        OrderCollectionParams.groupBy=$('#searchGroupBy' ).val() != 'undefined'
            && $('#searchGroupBy' ).val() != null
            && $('#searchGroupBy' ).val() != ''?
            $('#searchGroupBy' ).val():'none';
        //mainFormParams = OrderCollectionParams; // all form data is stored in the namespace global variable mainFormParams

        getOrderCollections(OrderCollectionParams,fillOrdersTable);
    }

    // Landscape and Full Order-Trade Call based on time frame.
    function getOrderCollections(oParams,successHandler) {

        /*var ordersURL = IEM.cURL + "orderswithtrades/ordercollection/" + oParams.takerOrg + "/"+
            oParams.ccyPair + "/" +
            oParams.startTime + "/" +
            oParams.endTime + "/" + oParams.orderType  +"/" +
            oParams.status +"/" +
            oParams.channel +"/" +oParams.tif +"/" +oParams.origOrg  +"/" +oParams.buysell;*/
        var ordersURL = IEM.cURL + "orderswithtrades/ordercollection/" + oParams.takerOrg + "/"+
            oParams.ccyPair + "/" +
            oParams.timeRange + "/" + oParams.orderType  +"/" +
            oParams.status +"/" +
            oParams.channel +"/" +oParams.tif +"/" +oParams.origOrg  +"/" +oParams.buysell+"/"+oParams.orderBy+"/"+oParams.groupBy;
        //TODO : remove later for testing
       /* var ordersURL = IEM.cURL + "orderswithtrades/ordercollection/" +
            oParams.startTime + "/" +
            oParams.endTime;*/
        profileUrl = ordersURL;
        IEM.Utils.ajaxRequest(ordersURL,function(o) {
                if (o.type == 'error' || o.type == 'warning') {
                    alert(o.type + ' : ' + o.errorMessage);
                    return;
                }
                successHandler.call(this, o);
            },
            function() {
                alert("Problem fetching Order Collection - Call Failed.");
            }, 'Order Collection', "Getting Order Collection..."
        );
    }

    function fillOrdersTable(orders) {
        var tableOptions = {}, orderId, order;
        //Index	OrderID	OrderState	TakerOrg	CcyPair	Buy/Sell	OrderAmtUSD	OrderAmt	FilledAmount	OrderType	TIF	Channel
        tableOptions.columns = [
            { 'sTitle': 'OrderId'},
            { 'sTitle': 'OrderState'},
            { 'sTitle': 'TakerOrg'},
            { 'sTitle': 'CcyPair'},
            { 'sTitle': 'Buy/Sell'},
            { 'sTitle': 'OrderAmtUSD'},
            { 'sTitle': 'OrderAmt'},
            { 'sTitle': 'FilledAmount'},
            { 'sTitle': 'OrderType'},
            { 'sTitle': 'TIF'},
            { 'sTitle': 'Channel'}
        ];

        var tableData = [];
        var orderSet = JSON.parse(orders.orderSet);
        var rowCount =  orders.rowCount;
        for (var i=0, len=orderSet.length; i< len; i++)
        {
            var order = orderSet[i];
            // SELECT CREATED,ORDERID, ORDERSTATE, ORG, CCYPAIR, BUYSELL, ORDERAMTUSD, ORDERAMT, FILLEDAMT, ORDERTYPE, TIMEINFORCE, CHANNEL
            tableData.push(['<a target="_blank" href="./home1.jsp?orderid=' + order.orderid + '">' + order.orderid + '</a>',
                order.orderstate,
                order.org,
                order.ccypair,
                order.buysell,
                order.orderamtusd,
                order.orderamt,
                order.filledamt,
                order.ordertype,
                order.timeinforce,
                order.channel]);
        }
        var message = "The total number of rows "+rowCount;
        document.getElementById("rowtext").innerHTML = message ;
       // $('#rowtext' ).textContent="The total number of rows "+rowCount;
       // $('#rowtext' ).show().css( 'visibility', 'visible' );
        tableOptions.bPaginate = false;
        createTable('orderCollectionTable', 'ordTable', 'orderCollectionTableContainer', tableData, tableOptions);
    }


    function createTable(tableId, tableName, tableContainer, tableData, tableOptions) {
        if (IEM[tableName] != 'undefined') {
            $('#' + tableId + '_wrapper').remove();
            delete IEM[tableName];
        }

        var tableEle = $('<table></table>');
        tableEle.attr({
            'id': tableId,
            'class': 'table table-bordered',
        }).appendTo($('#' + tableContainer));

        var optionsObj = {
            "sDom": tableOptions.sDom || '<"clear">lTr<"scroller"t>ip',
            'aoColumns': tableOptions.columns,
            'bSort': false,
            'bInfo': false,
            'aaData': tableData,
            'bPaginate': (typeof tableOptions.bPaginate != 'undefined' ? tableOptions.bPaginate : true),
            'sPaginationType': 'simple',
          //  'aaSorting': [[]],//tableOptions.aaSorting || [[0,"asc"]],
            'fnRowCallback': tableOptions.fnRowCallback ? tableOptions.fnRowCallback : null,
             'iDisplayLength' : 50,
            "oTableTools": {
                "sSwfPath": "/emscope/web1/assets/swf/copy_csv_xls_pdf.swf",
                "aButtons": [
                    "print",
                    "xls",
                    "pdf"
                ]
            }
        };

        if (tableOptions.sScrollY) {
            optionsObj.sScrollY = tableOptions.sScrollY;
        }
        if (tableOptions.scrollY) {
            optionsObj.scrollY = tableOptions.scrollY;
        }
        if (tableOptions.sScrollX) {
            optionsObj.sScrollX = tableOptions.sScrollX;
        }
        if (tableOptions.scrollX) {
            optionsObj.scrollX = tableOptions.scrollX;
        }
        if (tableOptions.sScrollXInner) {
            optionsObj.sScrollXInner = tableOptions.sScrollXInner;
        }
        if (tableOptions.iDisplayLength) {
            optionsObj.iDisplayLength = tableOptions.iDisplayLength;
        }

        if (tableOptions.processing)
        {
            optionsObj.processing = tableOptions.processing;
        }

        if (tableOptions.serverSide)
        {
            optionsObj.serverSide= tableOptions.serverSide;
            optionsObj.ajax= tableOptions.ajax;
        }

        IEM[tableName] = $('#' + tableId).dataTable(optionsObj);
        $('#buttondiv' ).show().css( 'visibility', 'visible' );
    }



    return {
        setupGetOrderCollectionFormSubmit: setupGetOrderCollectionFormSubmit
    }



})();/**
 * Created by kasi on 9/8/2014.
 */


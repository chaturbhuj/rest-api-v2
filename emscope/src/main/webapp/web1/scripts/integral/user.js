$(document).ready(function() {
   if ($('.login-body').length) {
        IEM.User.setupLoginForm();
   }
   if ($('#logout').length) {
        $('#logout').click(IEM.User.logout);
   }
});

IEM.User = (function() {
    var username = '', password = '', organization='';
	var landingPage = '';

    function requestForLogin() {
        var url = contextPath + '/LoginServlet';
		landingPage = getLandingPage();
        $.blockUI({message: "Logging you in..."});
        $.ajax({
            type: 'GET',
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: {
                username: username,
                password: password,
				organization: organization
            },
            success: function(o) {
                $.unblockUI();
                handleLoginResponse.call(this, o);
            },
            error: function(o) {
                $.unblockUI();
                handleLoginFormError.call(this, o);
            }
        });
    }


		function getLandingPage() {
			var sURL = window.document.URL.toString();
			if(sURL.indexOf("?") > 0) {
				var arrParams = sURL.split("?");
				var arrURLParams = arrParams[1].split("&");
				var arrParamNames = new Array(arrURLParams.length);
				var arrParamValues = new Array(arrURLParams.length);
				var i = 0;

				for (i=0;i<arrURLParams.length;i++) {
					var sParam =  arrURLParams[i].split("=");
					arrParamNames[i] = sParam[0];

					if (sParam[1] != "") {
						arrParamValues[i] = unescape(sParam[1]);
					} else {
						arrParamValues[i] = "No Value";
					}
					for (i=0;i<arrURLParams.length;i++) {
						if(arrParamNames[i] == 'path'){
							return arrParamValues[i];
						}
					}
				}
				return '';
			}
			return '';
		}

    function handleLoginResponse(o) {
        if (o.isError) {
            showErrorMessage(o.message);
        } else {
		    if(landingPage != '') {
			    window.location.href= landingPage;
			} else {
				window.location.href= contextPath + "/web1/home1.jsp";
			}
        }
    }

    function handleLoginFormError() {
		showErrorMessage('Authentication failed. Please check your inputs and try again.');
    }

    function setupLoginForm() {
        $('form[name="loginForm"]').submit(function(e) {
            e.preventDefault();
            $('.errMsgContainer').hide();
            var validate = validateLoginForm();
            if (!validate) {
                return false;
            }
            requestForLogin();
        });
    }

    function validateLoginForm() {
        username = $('#username').val();
        password = $('#password').val();
		organization = $('#organization').val();

        if (!username || !password) {
            showErrorMessage('Please enter username and password.');
            return false;
        }
        return true;
    }

    function showErrorMessage(msg) {
        var errContainer = $('.errMsgContainer');
        errContainer.html('<div class="errMsg">' + msg + '</div>');
        errContainer.show();
    }

    function logout() {
        var url = contextPath + '/LogoutServlet';
        $.blockUI({message: "Logging you out..."});
        $.ajax({
            type: 'GET',
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(o) {
                $.unblockUI();
                window.location.href=contextPath + "/web1/login.jsp";
            },
            error: function(o) {
                $.unblockUI();
                IEM.User.logout();
            }
        });
    }

    return {
        setupLoginForm: setupLoginForm,
        logout: logout
    };
})();
/**
 *  This script File is used to enable users to start a topic related to the order
 *  that is populated in the order display view of EMScope. Users should be able 
 *  to answer existing questions or start a new question related to this 
 *  particular order. Currently this would be enabled for order.
 *  
 * @since		1.0.0
 * @version		1.0.0
 * @author              Anil    <nl.sainil@gmail.com> 
 */


/*----------------------------------------------------------------------------*/
/**
 * Click event : Redirect to order tab on clicking the order bubble
 * @author    Anil    <nl.sainil@gmail.com> 
 */
orderId = contextQNAs = '';
$('form[name="orderIdForm"]').submit(function (e) {
    orderId = $('#orderId').val().trim();

    /*Sending request to get order qontextual QnA*/
    getOrderContextualDetailsById(function (o) {
        handlegetOrderContextualDetailsById(o);
    }, function () {
        alert('Retrieving Order Life Cycle Failed. Try Again!');
    });
});
/**
 * This script is used to get contextual data for order id.
 * 
 */
function getOrderContextualDetailsById(successHandler, errorHandler) {
    if (!orderId) {
        alert("Order ID Missing!!");
        return;
    }
    var url = IEM.cURL + 'qa/question/order/' + orderId;
    IEM.Utils.ajaxRequest(url, successHandler, errorHandler,
            'Getting Order Contextual Help ' + orderId,
            'Getting OOrder Contextual Help...'
            );
}
function handlegetOrderContextualDetailsById(contextQNA) {
    $('div.narrativeContainer h4 span.contextualHelp').remove();
    $('div.narrativeContainer h4').append('<span class="contextualHelp" data-toggle="tooltip" data-placement="right" title=""></span>');
    if (contextQNA.length != 0) {
        $('.contextualHelp').attr('title', 'There is already a question associated with this order id');
        $('.contextualHelp').removeClass("contextBlueIcon");
        $('.contextualHelp').addClass("contextGreenIcon");
    } else {
        $('.contextualHelp').attr('title', 'Currently there are no question associated with this order');
        $('.contextualHelp').removeClass("contextGreenIcon");
        $('.contextualHelp').addClass("contextBlueIcon");
    }
    handlerContextHelp(contextQNA);
}
$(document).on("click", '.contextualHelp', function () {
    $('#contextualHelpModal').modal('show');
});
function handlerContextHelp(contextQNA) {
    var contextQNAData = '';
    contextQNAs = contextQNA;
    contextQNAData += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
    if (contextQNA.length != 0) {
        $.each(contextQNA, function (index, value) {
            contextQNAData +=
                    '<div class="panel panel-default">\n\
                            <div class="panel-heading" role="tab" id="' + contextQNA[index].questionId + '">\n\
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse' + contextQNA[index].questionId + '" aria-expanded="true" aria-controls="collapseOne">\n\
                                <h4 class="panel-title">' + contextQNA[index].questionTitle + '<span style="float: right;font-size: 12px;">Author: ' + contextQNA[index].author + '</span></h4>\n\
                            </a>\n\
                        </div>\n\
                        <div id="collapse' + contextQNA[index].questionId + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">\n\
                            <div class="panel-body">\n\
                                <table class="table table-hover table-bordered">\n\
                                    <tr><td><strong>' + contextQNA[index].questionTitle + ' : </strong><i>' + contextQNA[index].questionContent + '</i></td></tr>\n\
                                    </td></tr>\n\
                                </table>\n\
                                <table class="table table-hover table-bordered">';
            $.each(contextQNA[index].answerList, function (indAns, valAns) {
                contextQNAData += '<tr><td>' + valAns.answerContent + '<span style="float: right;font-size: 12px;">Author: ' + valAns.author + '</span></td></tr>';
            });
            contextQNAData += '<tr class="form' + contextQNA[index].questionId + '"><td>\n\
            <form role="form" id="answerFormQnA' + contextQNA[index].questionId + '">\n\
                <div><strong>Add your Comment</strong></div>\n\
                <div class="row">\n\
                <div class="form-group col-md-10 col-lg-10">\n\
                    <input type="hidden" class="form-control orderIdVal" name="orderIdVal" value="' + orderId + '">\n\
                <input type="hidden" name="questionId" class="questionId" value="' + contextQNA[index].questionId + '">\n\
                </div>\n\
                <div class="form-group col-md-10 col-lg-10">\n\
                    <textarea name="answer"  class="form-control answer" rows="1" cols="50" placeholder="Enter your answer"></textarea>\n\
                </div>\n\
                <div class="col-md-2 col-lg-2">\n\
                <button type="button" class="btn btn-success btnAns">Submit</button>\n\
            </form></td></tr>';
            contextQNAData += '</table>\n\
                            </div>\n\
                        </div>\n\
                    </div>';
        });
    }

    contextQNAData += '\
    <div class="panel panel-default question-panel">\n\
        <div class="panel-heading" role="tab" id="askQuestionPanel" style="background-color:#acacac">\n\
            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#askQuestion" aria-expanded="false" aria-controls="collapseTwo">\n\
                <h4 class="panel-title text-center">Add New Question </h4>\n\
            </a>\n\
        </div>\n\
    <div id="askQuestion" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">\n\
        <div class="panel-body">\n\
            <form role="form" name="askQuestionFormQnA" id="askQuestionFormQnA">\n\
                <div class="form-group">\n\
                    <input type="hidden" class="form-control orderIdVal" name="orderIdVal" value="' + orderId + '">\n\
                </div>\n\
                <div class="form-group">\n\
                    <input type="text" class="form-control questionTitle" name="questionTitle" placeholder="Question Title">\n\
                </div>\n\
                <div class="form-group">\n\
                    <textarea name="quesDesc" class="form-control quesDesc" rows="3" cols="50" placeholder="Description"></textarea>\n\
                </div>\n\
                <button type="button" class="btn btn-success btn1">Submit</button>\n\
            </form>\n\
      </div>\n\
    </div>\n\
  </div>';
    contextQNAData += '</div>';

    $('#contextualHelpModal div.modal-header h4.modal-title').html("Order Id: " + orderId);
    $('#contextualHelpModal div.modal-body').css({"padding": "10px"});
    $('#contextualHelpModal div.modal-body').html(contextQNAData);
    $('#contextualHelpModal div.modal-body button').css({"background-image": "none"});
    $('#contextualHelpModal div.panel-heading').css({"background-image": "none"});
    $('#contextualHelpModal div.modal-dialog').css({"width": "70% !important"});
}


//mouse-over event handler
$("body").on('mouseover', '.contextualHelp', function () {
    $('.contextualHelp').tooltip('show');
});
//mouse-out event handler
$("body").on('mouseout', '.contextualHelp', function () {
//    $("#divToolTip").css("display", "none");
    $('.contextualHelp').tooltip('show');
});

/**
 * This nscript is used to request to add new question for that orderId
 */
$('body').on('click', '.btn1', function () {

    var questionTitle = $('.questionTitle').val();
    var quesDesc = $('.quesDesc').val();
    if (!questionTitle) {
        alert("Question Title is required.");
        return false;
    }
    if (!quesDesc) {
        alert("Question Description is required.");
        return false;
    }

    var data = {
        title: questionTitle,
        content: quesDesc};
    var url = IEM.cURL + 'qa/question/order/' + orderId + '';
    $.ajax({
        type: "POST",
        url: url,
        cache: true,
        data: data,
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        success: function (o) {

            /*Sending request to get order qontextual QnA*/
            getOrderContextualDetailsById(function (o) {
                handleQuestionContextualDetailsById(o, questionTitle, quesDesc);
            }, function () {
                alert('Retrieving Order Life Cycle Failed. Try Again!');
            });

            $('.questionTitle').val('');
            $('.quesDesc').val('');
            $('div.narrativeContainer h4 span').tooltip('hide')
                    .attr('data-original-title', 'There is already a question associated with this order id')
                    .tooltip('fixTitle')
                    .tooltip('show');
            $('.contextualHelp').removeClass("contextBlueIcon");
            $('.contextualHelp').addClass("contextGreenIcon");
        },
        error: function (o) {
            alert('Failed to sending your question! Refresh and try again.');
        }
    });
});
/*Handler request on answer add*/
function handleQuestionContextualDetailsById(contextQNAQus, questionTitle, quesDesc) {
    var qus_id, authorName = '';
    if (contextQNAQus.length != 0) {
        authorName = contextQNAQus[contextQNAQus.length - 1].author;
        qus_id = contextQNAQus[contextQNAQus.length - 1].questionId;
    }
    var newQuestion = '<div class="panel panel-default">\n\
                            <div class="panel-heading" role="tab" id="' + qus_id + '">\n\
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse' + qus_id + '" aria-expanded="true" aria-controls="collapseOne">\n\
                                <h4 class="panel-title">' + questionTitle + '<span style="float: right;font-size: 12px;">Author: ' + authorName + '</span></h4>\n\
                            </a>\n\
                        </div>\n\
                        <div id="collapse' + qus_id + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">\n\
                            <div class="panel-body">\n\
                                <table class="table table-hover table-bordered">\n\
                                    <tr><td><strong>' + questionTitle + ' : </strong><i>' + quesDesc + '</i></td></tr></td></tr></table>\n\
                                    <table class="table table-hover table-bordered"><tr class="form' + qus_id + '"><td>\n\
                                    <form role="form" id="answerFormQnA' + qus_id + '">\n\
                                        <div><strong>Add your Comment</strong></div>\n\
                                        <div class="row">\n\
                                        <div class="form-group col-md-10 col-lg-10">\n\
                                            <input type="hidden" class="form-control orderIdVal" name="orderIdVal" value="' + orderId + '">\n\
                                        <input type="hidden" name="questionId" class="questionId" value="' + qus_id + '">\n\
                                        </div>\n\
                                        <div class="form-group col-md-10 col-lg-10">\n\
                                            <textarea name="answer"  class="form-control answer" rows="1" cols="50" placeholder="Enter your answer"></textarea>\n\
                                        </div>\n\
                                        <div class="col-md-2 col-lg-2">\n\
                                        <button type="button" class="btn btn-success btnAns">Submit</button>\n\
                                    </form></td></tr></table>\n\
                            </div>\n\
                        </div>\n\
                    </div>';
    $(newQuestion).insertBefore('div.question-panel');
}
/**
 * This script is used to add answer to particular question and orderid
 */
$('body').on('click', '.btnAns', function () {
    var parentFormId, parentForm = '';
    parentForm = $(this).parent().parent().parent();
    var questionId = $(parentForm).find('.questionId').val();
    var answer = $(parentForm).find('.answer').val();

    if (!answer) {
        alert("Answer is required.");
        return false;
    }

    data = {
        questionid: questionId,
        answer: answer};
    var url = IEM.cURL + 'qa/answer/order/' + orderId + '';
    $.ajax({
        type: "POST",
        url: url,
        cache: true,
        data: data,
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        success: function (o) {
            $(parentForm).find('.answer').val('');
            /*Sending request to get order qontextual QnA*/
            getOrderContextualDetailsById(function (o) {
                handleAnswerContextualDetailsById(o, answer, questionId, parentForm, parentFormId);
            }, function () {
                alert('Retrieving Order Life Cycle Failed. Try Again!');
            });
        },
        error: function (o) {
            alert('Failed to sending your comment! Refresh and try again.');
        }
    });
});
/*Handler request on answer add*/
function handleAnswerContextualDetailsById(contextQNA, answer, questionId, parentForm, parentFormId) {
    if (contextQNA.length != 0) {
        $.each(contextQNA, function (index, value) {
            if (value.questionId == questionId) {
                if (value.answerList.length != 0) {
                    parentFormId = $(parentForm).attr('id').split("answerFormQnA")[1];
                    $('<tr><td>' + answer + '<span style="float: right;font-size: 12px;">Author: ' + value.answerList[value.answerList.length - 1].author + '<span></td></tr>').insertBefore('tr.form' + parentFormId);
                }
            }
        });
    }
}
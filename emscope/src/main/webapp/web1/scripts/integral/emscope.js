// SETUP Dom ready function for the application
$(document).ready(function () {
    IEM.EMScope.setupGetOrdersFormSubmit();
    IEM.DO.setupGetDisplayOrderFormSubmit();
    IEM.OC.setupGetOrderCollectionFormSubmit();
    var orderTab = new IEM.OrderTab();
    orderTab.setupGetOrderByIdFormSubmit();
    checkURLforOrder();
    $('#filterByStreamToggle').on('switchChange', function (e, data) {
        orderTab.handleFilterTradeSwitch(data.value);
    });
    setupWidgets();
});
/**
 * Click event : Redirect to order tab on clicking the order bubble
 * @author    Maninder Singh    <manindersingh221@gmail.com>     
 */
$(document).on("dblclick", '#chartContainer ._0 circle', function () {
    var title = $(this).siblings('title').text();
    var titleArr = title.split('Order ID:');
    var orderArr = titleArr[1].split('Order Rate:');
    var orderId = $.trim(orderArr[0]);
    var url = $(location).attr('href');
    window.open(url + '?orderid=' + orderId, '_blank');
});


/**
 * Click event : This function is used to refresh modal, 
 * if time is taking more than 5 seconds and user want to refresh the page
 * @author    Anil    <nl.sainil@gmail.com>     
 */
$(document).on("click", '.refreshModal', function () {
    location.reload();
});

/**
 * This script is used to show and hide and order tree view
 * @author    Anil    <nl.sainil@gmail.com>     
 */
$('body').on('click', '.btn-order-tree-view', function () {
    $('#orderTreeViewModal').modal('show');
});

/**
 * This script is used to change the status of globalWaitTag.
 * This varible disable to load model once click on continue
 * button.
 * @author    Anil    <nl.sainil@gmail.com>     
 */
$('body').on('click', '.continueModal', function () {
    globalWaitTag = false;
    $('#loadingTimeModal').modal('hide');
});

function setupWidgets() {
    var yesterday = new Date();

    $('.date-select').daterangepicker({
        format: "MM/DD/YYYY HH:mm",
        timePicker: true,
        timePickerIncrement: 1,
        minDate: "12/20/2013",
        maxDate: yesterday,
        timePicker12Hour: false,
        dateLimit: {hours: 24}
    });

    $('.datetime-select').datetimepicker({
        format: "MM/DD/YYYY HH:mm:ss",
        timePicker: true,
        timePickerIncrement: 1,
        maxDate: yesterday,
        startDate: yesterday,
        timePicker12Hour: false,
        dateLimit: {hours: 24}
    });

    $('.multiselect').selectpicker({
        size: 7
    });

    $('.nav-tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('#blotterTabs a[data-toggle="tab"], #orderTabDataTabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var table = TableTools.fnGetInstance($(e.target).attr('data-tablename'));
        if (table != null && table.fnResizeRequired()) {
            table.fnResizeButtons();
        }
    });

    $('.pc-close').click(function (e) {
        $('.perf-console').remove();
    });

    $("[name='filterByStreamToggle']").bootstrapSwitch();
}

// To get query parameters of the URL as an object
function getQueryParams(qs) {
    qs = qs.split("+").join(" ");

    var params = {}, tokens,
            re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
                = decodeURIComponent(tokens[2]);
    }
    return params;
}

// Checking URL for Order ID
function checkURLforOrder() {
    var params = getQueryParams(document.location.search);
    if (params['orderid']) {
        $.blockUI({message: "Getting Order and Trades..."});
        $('#orderId').val(params['orderid']);
        $('form[name="orderIdForm"]').submit();
    }
}


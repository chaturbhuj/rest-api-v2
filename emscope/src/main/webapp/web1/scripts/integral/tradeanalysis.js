Array.prototype.compare = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].compare(array[i]))
                return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
};

function addZeroIfNeeded(number) {
    if (number <= 9) {
        return '0' + number;
    }
    return number;
}

function dateToString() {
    var dateObj = this;
    var displayDate = dateObj.getFullYear() + '-' + addZeroIfNeeded(dateObj.getMonth() + 1) + '-' + addZeroIfNeeded(dateObj.getDate()) + ' ' + addZeroIfNeeded(dateObj.getHours()) + ':' + addZeroIfNeeded(dateObj.getMinutes()) + ':' + addZeroIfNeeded(dateObj.getSeconds());
    displayDate += '.' + dateObj.getMilliseconds();
    return displayDate;
}

Date.prototype.toString = dateToString;

IEM.TradeAnalysis = {
    setupForm: function() {
    },

    setupGetOrdersFormSubmit: function() {
        $('form[name="getOrdersForm"]').submit(function(e){
            e.preventDefault();
            IEM.TradeAnalysis.getOrdersFormSubmit(this);
        });
    },

    getOrdersFormSubmit: function(formEle) {
        var orderParams = {};

        // Get Form Field Values
        orderParams.ccyPair = ($("#ccyPair").val()) ? $("#ccyPair").val().split('/').join('') : null;
        orderParams.customer = $("#customer").val();
        orderParams.startTime = new Date($("#startDt").val()).getTime();
        orderParams.endTime = new Date($("#endDt").val()).getTime();
        orderParams.orderType = ($('#oType').val()) ? $('#oType').val(): null;

        this.params = orderParams;
        this.params.provider = $('#provider').val().split('/')[0];
        this.params.stream = $('#provider').val().split('/')[1];

        IEM.TradeAnalysis.getOrders(orderParams);
    },

    getOrders: function(oParams) {
        var ordersURL = 'http://pittsburg:8080/' + "complete/rest/orders/org/" + oParams.customer + "/ccypair/" + oParams.ccyPair + "/from/" + oParams.startTime + "/to/" + oParams.endTime + "/ordertype/" + oParams.orderType + "/minamount/1";
        $.blockUI({message: "Getting Orders.."});
        $.ajax({
           type: "GET",
           url: IEM.ajaxURL,
           cache: true,
           data: {
               url: encodeURI(ordersURL)
           },
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           success: function (orders) {
                var tableOptions = {}, orderId, order;

                tableOptions.columns = [
                                       { 'sTitle': 'Time'},
                                       { 'sTitle': 'Order ID'},
                                       { 'sTitle': 'Buy/Sell'},
                                       { 'sTitle': 'Currency Pair'},
                                       { 'sTitle': 'Order Rate'},
                                       { 'sTitle': 'Fill Rate'},
                                       { 'sTitle': 'Filled Amount'},
                                       { 'sTitle': 'Filled Percentage'},
                                       { 'sTitle': 'Order Amount'},
                                       { 'sTitle': 'Order Type'},
                                       { 'sTitle': 'Organization'}
                                   ];
                var tableData = [];

                for (orderId in orders) {
                    /*if (typeof orderId != Number) {
                        break;
                    } */
                    order = orders[orderId];

                    var rate = parseFloat(order.OrderRate);

                     //handling incorrect values of format "Range 0.00100000 from 98.40499878"
                    if (isNaN(rate)) {
                       var actualRate = order.OrderRate;
                       var index = actualRate.indexOf('from ');
                       if (index != -1) {
                           rate = parseFloat(actualRate.substr(index + 5, actualRate.length));
                           orders[orderId].OrderRate = rate;
                       }
                    }

                    tableData.push([
                        new Date(order.Created),
                        '<a target="_blank" href="http://bqc2-app2-602:9680/grdmon/integral/Monitor/util/Search.jsp?searchType=order&txid=' + order.OrderID + '&userOrg=MAIN">' + order.OrderID + '</a>',
                        order.BuySell,
                        order.CcyPair,
                        order.OrderRate,
                        order.FillRate,
                        order.FilledAmt,
                        order.FilledPct,
                        order.OrderAmt,
                        order.OrderType,
                        order.Org
                    ]);
                }

                tableData.sort(function (a,b) {
                    return Number(a[0]) - Number(b[0]);
                });

                IEM.TradeAnalysis.orderData = orders;
                IEM.TradeAnalysis.orderTableData = tableData;
                IEM.TradeAnalysis.drawDCOrderGraph(orders);
                $('#blotterTabs').show();
                IEM.TradeAnalysis.createTable('ordersTable', 'oTable', 'ordersTableContainer', tableData, tableOptions);
                $.unblockUI();
                IEM.TradeAnalysis.getTrades();
           },
           error: function (txt) {
                $.unblockUI();
                alert("Problem fetching Orders..");
           }
        });
    },

    getTrades: function() {
        var orderCount = IEM.TradeAnalysis.orderTableData.length, url;
        var orderIds = [], splitOrders = [];

        for (var orderId in IEM.TradeAnalysis.orderData) {
            orderIds.push(orderId);
        }

        while (orderIds.length) {
            splitOrders.push(orderIds.splice(0,14));
        }

        var callCount = splitOrders.length;
        var responseCount = 0;
        for (var i=0; i< splitOrders.length; i++) {
            url = "http://pittsburg:8080/" + "complete/rest/orders/orderidswithtimestamp/";
            url += splitOrders[i].join(',');
            url += '/trades/';
            url += IEM.TradeAnalysis.orderData[splitOrders[i][0]].Created;
            $.blockUI({message: 'Getting Trades...'});
            $.ajax({
                        type: "GET",
                        url: "server/fetcher.php",
                        cache: true,
                        data: {
                            url: url
                        },
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (trades) {
                            if (typeof IEM.TradeAnalysis.tradeData == 'undefined') {
                                IEM.TradeAnalysis.tradeData = {};
                            }
                            $.extend(IEM.TradeAnalysis.tradeData, trades);
                            responseCount++;
                            if (responseCount == callCount) {
                                IEM.TradeAnalysis.processTradeData(IEM.TradeAnalysis.tradeData)
                                IEM.TradeAnalysis.drawDCTradeGraph(IEM.TradeAnalysis.tradeData);
                                $.unblockUI();
                                IEM.TradeAnalysis.getRates();
                                //renderTrades();
                            }
                        },
                        error: function() {
                            responseCount++;
                            if (responseCount == callCount) {
                                IEM.TradeAnalysis.drawDCTradeGraph(IEM.TradeAnalysis.tradeData);
                                IEM.TradeAnalysis.getRates();
                                $.unblockUI();
                                //renderTrades();
                            }
                            $.unblockUI();
                            alert("Problem in retrieving Trades");
                        }
            });
        }
    },

    processTradeData: function(trades) {
        var tableOptions = {}, tradeId, trade;

        tableOptions.columns = [
                               { 'sTitle': 'Time'},
                               { 'sTitle': 'Order ID'},
                               { 'sTitle': 'Trade ID'},
                               { 'sTitle': 'Buy/Sell'},
                               { 'sTitle': 'Currency Pair'},
                               { 'sTitle': 'Rate'},
                               { 'sTitle': 'Org'},
                               { 'sTitle': 'Type'},
                               { 'sTitle': 'Maker/Taker'}
                           ];
        var tableData = [];

        for (tradeId in trades) {
            trade = trades[tradeId];

            tableData.push([
                new Date(trade.EXECTIME),
                trade.ORDERID,
                trade.TRADEID,
                trade.BUYSELL,
                trade.CCYPAIR,
                trade.RATE,
                trade.ORG,
                trade.TYPE,
                trade.MKRTKR
            ]);
        }

        tableData.sort(function (a,b) {
            return Number(a[0]) - Number(b[0]);
        });

        IEM.TradeAnalysis.tradeData = trades;
        IEM.TradeAnalysis.tradeTableData = tableData;
        IEM.TradeAnalysis.createTable('tradesTable', 'tTable', 'tradesTableContainer', tableData, tableOptions);
    },

    manageRateExtent: function(rExt) {
        if (!this.rateExtent) {
            this.rateExtent = rExt;
            return rExt;
        }
        var grExt = this.rateExtent;
        //if ()
    },

    getBenchmarkRates: function(timeFrame) {
        var startTime = timeFrame ? new Date(timeFrame[0]).getTime() : IEM.TradeAnalysis.params.startTime;
        var endTime = timeFrame ? new Date(timeFrame[1]).getTime() : IEM.TradeAnalysis.params.endTime;

        if (endTime - startTime < 600000) {
            return;
        }

        var url = IEM.cURL + 'complete/rest/benchmarks/sample/600/ccypair/';
            url += this.params.ccyPair + '/fromtime/' + startTime + '/totime/' + endTime + '/tier/1';

        $.blockUI({message: 'Getting Benchmark Rates...'});

        $.ajax({
            type: "GET",
            cache: true,
            url: "server/fetcher.php",
            data: {
                url: url
            },

            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(benchmark) {
                var tableOptions = {}, tradeId, trade;

                tableOptions.columns = [
                                       { 'sTitle': 'Time'},
                                       { 'sTitle': 'Mean'},
                                       { 'sTitle': 'Median'},
                                       { 'sTitle': 'Range'}
                                   ];
                var tableData = [], rate;

                for (var i=0, len=benchmark.length; i<len; i++) {
                    rate = benchmark[i];

                    tableData.push([
                        new Date(rate.timeStamp),
                        rate.meanMidPrice,
                        rate.medianMidPrice,
                        rate.midPriceRange
                    ]);
                }

                tableData.sort(function (a,b) {
                    return Number(a[0]) - Number(b[0]);
                });

                IEM.TradeAnalysis.benchmarkData = benchmark;
                IEM.TradeAnalysis.benchmarkTableData = tableData;
                IEM.TradeAnalysis.createTable('benchmarkTable', 'bTable', 'benchmarkTableContainer', tableData, tableOptions);
                $.unblockUI();
                IEM.TradeAnalysis.drawDCBenchmarkGraph(benchmark);
            },
            error: function() {
                $.unblockUI();
                alert("Problem in retrieving Benchmark Rates");
            }
        });
    },

    getRates: function(timeFrame, yExtent) {
        var startTime = timeFrame ? new Date(timeFrame[0]).getTime() : IEM.TradeAnalysis.params.startTime;
        var endTime = timeFrame ? new Date(timeFrame[1]).getTime() : IEM.TradeAnalysis.params.endTime;
        if (endTime - startTime > 300000) {
            this.gotRates = false;
            return;
        } else {
            this.manageRateExtent([startTime, endTime]);
            this.gotRates = true;
        }
        var url = 'http://pittsburg:8080/' + "complete/rest/rates/";
            url += this.params.provider + '/' + this.params.stream + '/' + this.params.ccyPair + '/' + startTime + '/' + endTime;
        $.blockUI({message: 'Getting Rates...'});
        $.ajax({
            type: "GET",
            cache: true,
            url: "server/fetcher.php",
            data: {
                url: url
            },

            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(rates) {
                var tableOptions = {}, orderId, order;

                tableOptions.columns = [
                                       { 'sTitle': 'Time'},
                                       { 'sTitle': 'Ask Price'},
                                       { 'sTitle': 'Ask Size'},
                                       { 'sTitle': 'Bid Price'},
                                       { 'sTitle': 'Bid Size'},
                                       { 'sTitle': 'Status'},
                                       { 'sTitle': 'Adaptor'},
                                       { 'sTitle': 'GUID'},
                                       { 'sTitle': 'Tier'},
                                       { 'sTitle': 'Server'}
                                   ];
                var tableData = [];

                for (var i=0, len=rates.length; i< len; i++) {
                    var rate = rates[i];
                    tableData.push([
                        new Date(rate.tmstmp),
                        rate.ask_price,
                        rate.ask_size,
                        rate.bid_price,
                        rate.bid_size,
                        rate.status,
                        rate.adptr,
                        rate.guid,
                        rate.lvl,
                        rate.srvr
                    ]);
                }

                IEM.TradeAnalysis.rateData = rates;
                IEM.TradeAnalysis.rateTableData = tableData;
                IEM.TradeAnalysis.createTable('ratesTable', 'rTable', 'ratesTableContainer', tableData, tableOptions);
                $.unblockUI();
                IEM.TradeAnalysis.drawDCRateGraph(rates, yExtent);
            },
            error: function() {
                $.unblockUI();
                alert("Problem in retrieving Rates");
            }
        });
    },

    createTable: function(tableId, tableName, tableContainer, tableData, tableOptions) {
        if (IEM[tableName] != 'undefined') {
            $('#' + tableId + '_wrapper').remove();
            delete IEM[tableName];
        }
        var tableEle = $('<table></table>');
                tableEle.attr({
                    'id': tableId,
                    'class': 'table table-bordered',
                }).appendTo($('#' + tableContainer));

        IEM[tableName] = $('#' + tableId).dataTable({
            "sDom": 'lr<"scroller"t>ip',
            'aoColumns': tableOptions.columns,
            'aaData': tableData,
            'bPaginate': true,
            'sPaginationType': 'full_numbers'
        });
    },

    drawDCBenchmarkGraph: function(rates) {
        var IEMT = IEM.TradeAnalysis;

        for (var i=0, len = rates.length; i<len; i++) {
            rates[i].date = new Date(rates[i].timeStamp);
        };
        var rateExtent = d3.extent(rates, function(d) {return d.meanMidPrice;});
        var yExtent = yExtent || IEMT.chartYExtent;
        var cex = IEMT.mergeExtents([yExtent, rateExtent]);
        yExtent = [parseFloat(cex[0])-(cex[1]-cex[0])/6, parseFloat(cex[1])+(cex[1]-cex[0])/6];

        var ndx = crossfilter(rates);
        var dateDim = ndx.dimension(function(d) { return d.timeStamp; });
        var bmGroup = dateDim.group().reduce(
                    function(p,v) { p.rateData = v; return p;},
                    function(p,v) {},
                    function(p,v) {return {rateData : null}}
                );

        IEMT.benchmarkGraph = dc.lineChart(IEMT.chart)
                                  .dimension(dateDim)
                                  .group(bmGroup, "Benchmark(Mean)")
                                  .keyAccessor(function (p) {
                                      return p.key;
                                  })
                                  .valueAccessor(function (p) {
                                      return p.value.rateData.meanMidPrice;
                                  })
                                  .renderDataPoints(true)
                                  .interpolate('step-after');

        IEMT.chart.compose([IEMT.ordersChart/*, IEMT.tradesChart*/, IEMT.benchmarkGraph])
                .y(d3.scale.linear().domain(yExtent))
                .render();
    },

    drawDCRateGraph: function(rates, yExtent) {
        var IEMT = IEM.TradeAnalysis;

        var ndx = crossfilter(rates);
        var tierDim = ndx.dimension(function(d) {return d.lvl;});
        var tier1AskData, tier2AskData, tier3AskData, tier1BidData, tier2BidData, tier3BidData;
        tier1AskData = tier1BidData = tierDim.filter(1).top(Infinity);
        tier2AskData = tier2BidData = tierDim.filter(2).top(Infinity);
        tier3AskData = tier3BidData = tierDim.filter(3).top(Infinity);

        tier1AskData = IEMT.addAskInactiveRates(tier1AskData);
        tier2AskData = IEMT.addAskInactiveRates(tier2AskData);
        tier3AskData = IEMT.addAskInactiveRates(tier3AskData);
        tier1BidData = IEMT.addBidInactiveRates(tier1BidData);
        tier2BidData = IEMT.addBidInactiveRates(tier2BidData);
        tier3BidData = IEMT.addBidInactiveRates(tier3BidData);

        var ryex = IEMT.mergeExtents([d3.extent(tier1AskData, function(d) { return d.ask_price;}),
                            d3.extent(tier2AskData, function(d) { return d.ask_price;}),
                            d3.extent(tier3AskData, function(d) { return d.ask_price;}),
                            d3.extent(tier1BidData, function(d) { return d.bid_price;}),
                            d3.extent(tier2BidData, function(d) { return d.bid_price;}),
                            d3.extent(tier3BidData, function(d) { return d.bid_price;})]);

        var rxex = d3.extent(rates, function(d) {return new Date(d.tmstmp)});

        var r1Adx = crossfilter(tier1AskData);
        var r1ADim = r1Adx.dimension(function(d) { return d.date});
        var tier1AskGroup = r1ADim.group().reduce(
            function(p,v) { p.rateData = v; return p;},
            function(p,v) {},
            function(p,v) {return {rateData : null}}
        );

        var r2Adx = crossfilter(tier2AskData);
        var r2ADim = r2Adx.dimension(function(d) { return d.date});
        var tier2AskGroup = r2ADim.group().reduce(
            function(p,v) { p.rateData = v; return p;},
            function(p,v) {},
            function(p,v) {return {rateData : null}}
        );

        var r3Adx = crossfilter(tier3AskData);
        var r3ADim = r3Adx.dimension(function(d) { return d.date});
        var tier3AskGroup = r3ADim.group().reduce(
            function(p,v) { p.rateData = v; return p;},
            function(p,v) {},
            function(p,v) {return {rateData : null}}
        );

        var r1Bdx = crossfilter(tier1BidData);
        var r1BDim = r1Bdx.dimension(function(d) { return d.date});
        var tier1BidGroup = r1BDim.group().reduce(
            function(p,v) { p.rateData = v; return p;},
            function(p,v) {},
            function(p,v) {return {rateData : null}}
        );

        var r2Bdx = crossfilter(tier2BidData);
        var r2BDim = r2Bdx.dimension(function(d) { return d.date});
        var tier2BidGroup = r2BDim.group().reduce(
            function(p,v) { p.rateData = v; return p;},
            function(p,v) {},
            function(p,v) {return {rateData : null}}
        );

        var r3Bdx = crossfilter(tier3BidData);
        var r3BDim = r3Bdx.dimension(function(d) { return d.date});
        var tier3BidGroup = r3BDim.group().reduce(
            function(p,v) { p.rateData = v; return p;},
            function(p,v) {},
            function(p,v) {return {rateData : null}}
        );

        IEMT.tier1AskGraph = dc.lineChart(IEMT.chart)
                                  .dimension(r1ADim)
                                  .group(tier1AskGroup, "Ask Price - Tier 1")
                                  .colors('#1f77b4')
                                  .keyAccessor(function (p) {
                                      return p.key;
                                  })
                                  .valueAccessor(function (p) {
                                      return p.value.rateData.ask_price;
                                  })
                                  .renderDataPoints(true)
                                  .interpolate('step-after');

        IEMT.tier2AskGraph = dc.lineChart(IEMT.chart)
                                  .dimension(r2ADim)
                                  .group(tier2AskGroup, "Ask Price - Tier 2")
                                  .colors('#ff7f0e')
                                  .keyAccessor(function (p) {
                                      return p.key;
                                  })
                                  .valueAccessor(function (p) {
                                      return p.value.rateData.ask_price;
                                  })
                                  .renderDataPoints(true)
                                  .interpolate('step-after');

        IEMT.tier3AskGraph = dc.lineChart(IEMT.chart)
                                  .dimension(r3ADim)
                                  .group(tier3AskGroup, "Ask Price - Tier 3")
                                  .colors('#2ca02c')
                                  .keyAccessor(function (p) {
                                      return p.key;
                                  })
                                  .valueAccessor(function (p) {
                                      return p.value.rateData.ask_price;
                                  })
                                  .renderDataPoints(true)
                                  .interpolate('step-after');

        IEMT.tier1BidGraph = dc.lineChart(IEMT.chart)
                                  .dimension(r1BDim)
                                  .group(tier1BidGroup, "Bid Price - Tier 1")
                                  .colors('#d62728')
                                  .keyAccessor(function (p) {
                                      return p.key;
                                  })
                                  .valueAccessor(function (p) {
                                      return p.value.rateData.bid_price;
                                  })
                                  .renderDataPoints(true)
                                  .interpolate('step-after');
        IEMT.tier2BidGraph = dc.lineChart(IEMT.chart)
                                  .dimension(r2BDim)
                                  .group(tier2BidGroup, "Bid Price - Tier 2")
                                  .colors('#9467bd')
                                  .keyAccessor(function (p) {
                                      return p.key;
                                  })
                                  .valueAccessor(function (p) {
                                      return p.value.rateData.bid_price;
                                  })
                                  .renderDataPoints(true)
                                  .interpolate('step-after');

        IEMT.tier3BidGraph = dc.lineChart(IEMT.chart)
                                  .dimension(r3ADim)
                                  .group(tier3BidGroup, "Bid Price - Tier 3")
                                  .colors('#8c564b')
                                  .keyAccessor(function (p) {
                                      return p.key;
                                  })
                                  .valueAccessor(function (p) {
                                      return p.value.rateData.bid_price;
                                  })
                                  .renderDataPoints(true)
                                  .interpolate('step-after');

        yExtent = yExtent || IEMT.chartYExtent;
        var cex = IEMT.mergeExtents([yExtent, ryex]);
        yExtent = [parseFloat(cex[0])-(cex[1]-cex[0])/6, parseFloat(cex[1])+(cex[1]-cex[0])/6];

        var chartArr = [];
        if (IEMT.ordersChart) {
            chartArr.push(IEMT.ordersChart);
        }
        if (IEMT.tradesChart) {
            chartArr.push(IEMT.tradesChart);
        }
        chartArr.push(IEMT.tier1AskGraph, IEMT.tier2AskGraph, IEMT.tier3AskGraph, IEMT.tier1BidGraph, IEMT.tier2BidGraph, IEMT.tier3BidGraph);

        IEMT.chart.compose(chartArr)
                    .y(d3.scale.linear().domain(yExtent))
                    .x(d3.time.scale().domain(rxex));
        IEMT.zoomChart.y(d3.scale.linear().domain(IEMT.chartYExtent)).render();
        IEMT.chart.render();
    },

    mergeExtents: function(extents) {
        var min = [], max = [];
        extents.forEach(function (d) {
            d[0] && min.push(d[0]);
            d[1] && max.push(d[1]);
        });
        return [Math.min.apply(null, min), Math.max.apply(null, max)];
    },

    addAskInactiveRates: function(data) {
        var lvlAskPrice;
        var newData = [];

        data.forEach(function(d) {
            d.date = new Date(d.tmstmp);
            if (d.ask_price != 0) {
                newData.push(d);
                lvlAskPrice = d.ask_price;
            }
            if (d.status == 'InActive') {
                if (lvlAskPrice) {
                    d.ask_price = lvlAskPrice;
                    newData.push(d);
                }
            }
        });
        return newData;
    },

    addBidInactiveRates: function(data) {
        var lvlBidPrice;
        var newData = [];

        data.forEach(function(d) {
            d.date = new Date(d.tmstmp);
            if (d.bid_price != 0) {
                newData.push(d);
                lvlBidPrice = d.bid_price;
            }
            if (d.status == 'InActive') {
                if (lvlBidPrice) {
                    d.bid_price = lvlBidPrice;
                    newData.push(d);
                }
            }
        });
        return newData;
    },

    drawDCOrderGraph: function(orders) {
        this.chart = dc.compositeChart('#chartContainer');
        this.zoomChart = dc.lineChart('#zoomChart');
        var data = [];

        for (var orderId in orders) {
            orders[orderId]['key'] = orderId;
            orders[orderId]['Type'] = 'Orders';
            data.push(orders[orderId]);
        }
        data.forEach(function(d) {
            d.date = new Date(d.Created);
        });

        var ndx = crossfilter(data);
        var dateDim = ndx.dimension(function(d) { return d.date;});
        var ratesGroup = dateDim.group().reduce(
            function (p,v) {
                if (!p) {
                    p = {};
                }
                p.data = v;
                p.OrderID = v.OrderID;
                p.OrderAmt = v.OrderAmt;
                p.OrderRate = v.OrderRate;
                p.Date = v.date;
                return p;
            },
            function (p,v) {
                if (!p) {
                    p = {};
                }
                p.data = v;
                p.OrderID = v.OrderID;
                p.OrderAmt = v.OrderAmt;
                p.OrderRate = v.OrderRate;
                p.Date = v.date;
                return p;
            },
            function (p,v) {
                return {
                            OrderID: 0,
                            OrderAmt: 0,
                            OrderRate: 0,
                            Date: 0,
                            data: null
                       }
                }
            );
        var zoomDim = ndx.dimension(function(d) { return d.date;});
        var zoomGroup = zoomDim.group().reduceSum(function(d) { return d.OrderRate;});
        var yExtent = d3.extent(data, function(d) { return d.OrderRate;});
        yExtent = [parseFloat(yExtent[0])-(yExtent[1]-yExtent[0])/6, parseFloat(yExtent[1])+(yExtent[1]-yExtent[0])/6];

        this.chartYExtent = yExtent;

        this.zoomChart
            .width(1200)
            .height(50)
            .renderArea(true)
            .margins({top: 0, right: 50, bottom: 20, left: 60})
            .dimension(zoomDim)
            .group(zoomGroup, "Orders")
            .x(d3.time.scale().domain([new Date(this.params.startTime), new Date(this.params.endTime)]))
            .y(d3.scale.linear().domain(yExtent));

        this.zoomChart.render();

        var rExtent = [0,100];

        this.ordersChart = dc.bubbleChart(this.chart)
                              .dimension(dateDim)
                              .group(ratesGroup, "Orders")
                              .colors('red')
                              .keyAccessor(function (p) {
                                  return p.key;
                              })
                              .valueAccessor(function (p) {
                                  return p.value.OrderRate;
                              })
                              .radiusValueAccessor(function (p) {
                                  return 60;
                              })
                              .maxBubbleRelativeSize(0.00005)
                              .r(d3.scale.linear().domain(rExtent))
                              //.renderTitle(true)
                              //.title(function(d) {
                              //    return d.key + '\nOrder ID: ' + d.value.OrderID + '\nOrder Rate: ' + d.value.OrderRate;
                              //})
                              .renderLabel(false);

        this.chart
            .width(1200)
            .height(600)
            .transitionDuration(250)
            .margins({top: 10, right: 50, bottom: 20, left: 60})
            .yAxisLabel("Rate")
            .x(d3.time.scale().domain([new Date(this.params.startTime), new Date(this.params.endTime)]))
            .y(d3.scale.linear().domain(yExtent))
            .renderHorizontalGridLines(true)
            .renderVerticalGridLines(true)
            .rangeChart(this.zoomChart)
            .elasticY(false)
            .brushOn(false)
            .renderTitle(true)
            .title(function(d) {
                if (typeof d.value.TRADEID != 'undefined') {
                    return d.key + '\nTrade ID: ' + d.value.TRADEID + '\nRate: ' + d.value.RATE + '\nOrder ID: ' + d.value.ORDERID + '\nBuy/Sell: ' + d.value.data.BUYSELL;
                } else if (typeof d.value.rateData != 'undefined') {
                    return d.key + '\nAsk Price: ' + d.value.rateData.ask_price + '\nBid Price: ' + d.value.rateData.bid_price + '\nTier: ' + d.value.rateData.lvl + '\nStatus: ' + d.value.rateData.status;
                }
                return d.key + '\nOrder ID: ' + d.value.OrderID + '\nOrder Rate: ' + d.value.OrderRate;
            })
            .compose([
                this.ordersChart
            ])
            .on("zoomed", function(chart, filter) {
                var chartChildren = chart.children(), extentArr = [];
                for (var i=0, len = chartChildren.length; i<len; i++) {
                    var child = chartChildren[i];
                    if (child.data()[0]) {
                        if (child.data()[0].values) {
                            extentArr.push(d3.extent(chartChildren[i].data()[0].values, function(d) {return d.y;}));
                        } else if (child.data()[0].value.OrderID) {
                            var data = child.dimension().filter(chart.rangeChart().filter()).top(Infinity);
                            if (data.length)
                                extentArr.push(d3.extent(data, function(d) {return d.OrderRate;}));
                        }
                    }
                }
                var yExtent = IEM.TradeAnalysis.mergeExtents(extentArr);
                yExtent = [yExtent[0]-(yExtent[1]-yExtent[0])/6, yExtent[1]+(yExtent[1]-yExtent[0])/6];
                chart.y(d3.scale.linear().domain(yExtent));
                chart.redraw();
            })
            .on("postRedraw", function(chart) {
                /*var xExtent = chart.rangeChart().filter();
                xExtent = [new Date(xExtent[0]).getTime(), new Date(xExtent[1]).getTime()];
                //console.log(xExtent);
                IEM.TradeAnalysis.getBenchmarkRates(xExtent); */
                if (IEM.TradeAnalysis.postRedrawTimer) {
                    clearTimeout(IEM.TradeAnalysis.postRedrawTimer);
                    delete IEM.TradeAnalysis.postRedrawTimer;
                }
                IEM.TradeAnalysis.postRedrawTimer = setTimeout(function() {
                    var xExtent = chart.rangeChart().filter();
                    xExtent = [new Date(xExtent[0]).getTime(), new Date(xExtent[1]).getTime()];
                    var chartChildren = chart.children(), extentArr = [];
                    for (var i=0, len = chartChildren.length; i<len; i++) {
                        var child = chartChildren[i];
                        if (child.data()[0]) {
                            if (child.data()[0].values) {
                                extentArr.push(d3.extent(chartChildren[i].data()[0].values, function(d) {return d.y;}));
                            } else if (child.data()[0].value.OrderID) {
                                var data = child.dimension().filter(chart.rangeChart().filter()).top(Infinity);
                                if (data.length)
                                    extentArr.push(d3.extent(data, function(d) {return d.OrderRate;}));
                            }
                        }
                    }
                    var yExtent = IEM.TradeAnalysis.mergeExtents(extentArr);
                    yExtent = [yExtent[0]-(yExtent[1]-yExtent[0])/6, yExtent[1]+(yExtent[1]-yExtent[0])/6];
                    if (!IEM.TradeAnalysis.gotRates || !IEM.TradeAnalysis.previousXExtent || !(IEM.TradeAnalysis.mergeExtents([xExtent, IEM.TradeAnalysis.previousXExtent]).compare(IEM.TradeAnalysis.previousXExtent))) {
                        IEM.TradeAnalysis.getRates(chart.rangeChart().filter(), yExtent);
                    }
                    IEM.TradeAnalysis.previousXExtent = xExtent;
                    delete(IEM.TradeAnalysis.postRedrawTimer);
                }, 500);
            })
            .renderlet(function(chart) {
                /*$('html, body').animate({
                    scrollTop: $("#chartContainer").offset().top
                }, 100);*/
                var dots = d3.select(chart.g().node()).selectAll('g.sub').selectAll('circle');
                var xExtent = chart.rangeChart().filter();
                var yExtent = IEM.TradeAnalysis.previousXExtent || IEM.TradeAnalysis.chartYExtent;
                if (xExtent && yExtent) {
                    var xDomain = d3.time.scale().domain(xExtent);
                    var yDomain = d3.scale.linear().domain(yExtent);
                    d3.selectAll('svg g line').data([]).exit().remove();
                    dots.each(function(d,i) {
                        if (d.data && d.data.value && d.data.value.rateData && d.data.value.rateData.status == 'InActive') {
                            var nextElement = this.nextElementSibling, nextData;
                            d3.select(nextElement).each(function(d) {nextData =  d;});
                            d3.select('svg g').append("svg:line")
                                .attr("x1", chart.x()(d.x) + chart.margins().left).attr('y1', chart.y()(d.y) + chart.margins().top).attr('x2', chart.x()(nextData.x)+chart.margins().left).attr('y2', chart.y()(nextData.y)+chart.margins().top).style('stroke', 'rgb(0,0,0)');
                        }
                    });
                }
            })
            .xAxis().tickFormat(function(d) { return d3.time.format("%H:%M:%S.%L")(new Date(d));});

        this.chart.render();
    },

    drawDCTradeGraph: function(trades) {
        var data = [];

        for (var tradeId in trades) {
            trades[tradeId]['key'] = tradeId;
            trades[tradeId]['Type'] = 'Trades';
            data.push(trades[tradeId]);
        }

        data.forEach(function (d) {
            d['date'] = new Date(d.EXECTIME);
        });

        var ndx = crossfilter(data);
        var dateDim = ndx.dimension(function(d) { return d.date;});
        var ratesGroup = dateDim.group().reduce(
            function (p,v) {
                if (!p) {
                    p = {};
                }
                p.data = v;
                p.TRADEID = v.TRADEID;
                p.ORDERID = v.ORDERID;
                p.RATE = v.RATE;
                p.Date = v.date;
                return p; },
            function (p,v) {
                if (!p) {
                    p = {};
                }
                p.data = v;
                p.OrderID = v.OrderID;
                p.OrderAmt = v.OrderAmt;
                p.OrderRate = v.OrderRate;
                p.Date = v.date;
                return p;
            },
            function (p,v) {
                return {
                            TRADEID: 0,
                            ORDERID: 0,
                            RATE: 0,
                            Date: 0,
                            data: null
                       };
            }
        );

        IEM.TradeAnalysis.tradesChart = dc.bubbleChart(IEM.TradeAnalysis.chart)
                                  .dimension(dateDim)
                                  .group(ratesGroup, "Trades")
                                  .keyAccessor(function (p) {
                                      return p.key;
                                  })
                                  .colors('blue')
                                  .valueAccessor(function (p) {
                                      return p.value.RATE;
                                  })
                                  .radiusValueAccessor(function (p) {
                                      return 80;
                                  })
                                  .maxBubbleRelativeSize(0.00005)
                                  .r(d3.scale.linear().domain([0,100]))
                                  .renderLabel(false);

        IEM.TradeAnalysis.chart.compose([IEM.TradeAnalysis.ordersChart, IEM.TradeAnalysis.tradesChart])
                                                    .legend(dc.legend().x(300).y(10).horizontal(true).itemWidth(110).legendWidth(810).gap(5));
        IEM.TradeAnalysis.chart.render();
    }

};
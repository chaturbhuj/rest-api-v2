//Landscape Mode - More than an hour - separate orders/trades call - just getting summary. Benchmark Data shown. No Tick Data
//Zoomedin Mode1 - <= 30 min && >= 10 min - Orders/Trades Detailed Call, showing blotter tables with all info, Only benchmark Shown, No Tick Data
//Zoomedin Mode2 - <=10min - Orders/Trades Detailed Call, show blotter Tabs, Benchmark & Tickdata shown

//Utility function for adding zero to date & time when required.
function addZeroIfNeeded(number) {
    if (number <= 9) {
        return '0' + number;
    }
    return number;
}


// Date String function. Specific format for EMScope. This always returns time in GMT. using getTimezoneOffset to handle this.
function dateToString() {
    var dateObj = this;
    var timezoneOffset = dateObj.getTimezoneOffset()*60000;
    dateObj = new Date(dateObj.getTime() + timezoneOffset);
    var displayDate = dateObj.getFullYear() + '-' + addZeroIfNeeded(dateObj.getMonth() + 1) + '-' + addZeroIfNeeded(dateObj.getDate()) + ' ' + addZeroIfNeeded(dateObj.getHours()) + ':' + addZeroIfNeeded(dateObj.getMinutes()) + ':' + addZeroIfNeeded(dateObj.getSeconds());
    var milliseconds = dateObj.getMilliseconds().toString();
    if (milliseconds.length < 3) {
        for (var i=0, len = 3-milliseconds.length, str = ''; i<len;i++) {
            str += '0';
        }
        milliseconds = str + milliseconds;
    }
    displayDate += '.' + milliseconds;
    return displayDate;
}

Date.prototype.toString = dateToString;

IEM.DO = (function() {
     
    // main function which sets up the forms in Display Order Tab
    function setupGetDisplayOrderFormSubmit() {
        $('form[name="getDisplayOrdersForm"]').submit(function(e){
            e.preventDefault();
            if($('#time').val() == ""){
                alert("Invalid Time ..!");
                return;
            }
			getDisplayOrdersFormSubmit();
        });
         $('#doDetailsBtn').click(function(e){
           e.preventDefault();
           if($('#timeinmillis').val() == ""){
                alert("Invalid Time  ..!");
                return;
            }
            getSpecificTimeDisplayOrdersFormSubmit();
         });
    }


    function getSpecificTimeDisplayOrdersFormSubmit(formEle) 
    {
    
		var displayOrderParams = {};

        // Get Form Field Values
        displayOrderParams.ccyPair = ($("#doCcyPair2").val()) ? $("#doCcyPair2").val().split('/').join('') : null;
        var timeRange = $('#timeinmillis').val().split(' - ');
        var startTime = new Date(timeRange[0]);
        displayOrderParams.startTime = startTime.getTime() - (startTime.getTimezoneOffset()*60000); // convert to GMT
        
		mainFormParams = displayOrderParams; // all form data is stored in the namespace global variable mainFormParams

        getSpecificTimeDisplayOrders(displayOrderParams,fillRatesTable);
    }
    

    function getDisplayOrdersFormSubmit(formEle) {
        var displayOrderParams = {};

        // Get Form Field Values
        displayOrderParams.ccyPair = ($("#doCcyPair").val()) ? $("#doCcyPair").val().split('/').join('') : null;
        var timeRange = $('#time').val().split(' - ');
        var startTime = new Date(timeRange[0]);
        displayOrderParams.startTime = startTime.getTime() - (startTime.getTimezoneOffset()*60000); // convert to GMT
        
		mainFormParams = displayOrderParams; // all form data is stored in the namespace global variable mainFormParams

        getDisplayOrders(displayOrderParams,fillRatesTable);
    }

    // Landscape and Full Order-Trade Call based on time frame.
    function getSpecificTimeDisplayOrders(oParams,successHandler) {
        var ordersURL = IEM.cURL + "displayorderrates/at/"+oParams.ccyPair+"/"+oParams.startTime;
		IEM.Utils.ajaxRequest(ordersURL,function(o) {
                if (o.type == 'error' || o.type == 'warning') {
                    alert(o.type + ' : ' + o.errorMessage);
                    return;
                }
                successHandler.call(this, o);
            },
            function() {
                alert("Problem fetching Tick Data - Call Failed.");
            }, 'Display order Rates', "Getting Display Order Rates..."
        );
    }



    // Landscape and Full Order-Trade Call based on time frame.
    function getDisplayOrders(oParams,successHandler) {
        var ordersURL = IEM.cURL + "displayorderrates/"+oParams.ccyPair+"/"+oParams.startTime;
		IEM.Utils.ajaxRequest(ordersURL,function(o) {
                if (o.type == 'error' || o.type == 'warning') {
                    alert(o.type + ' : ' + o.errorMessage);
                    return;
                }
                successHandler.call(this, o);
            },
            function() {
                alert("Problem fetching Tick Data - Call Failed.");
            }, 'Display order Rates', "Getting Display Order Rates..."
        );
    }
    
	 
	
	 
    function fillRatesTable(rates) {
        var tableOptions = {}, orderId, order;

        tableOptions.columns = [
                               { 'sTitle': 'Timestamp'},
							   { 'sTitle': 'OrderId'},
							   { 'sTitle': 'Provider'},
							   { 'sTitle': 'Bid Size'},
							   { 'sTitle': 'Tier'},
							   { 'sTitle': 'Bid Rate'},
							   { 'sTitle': 'Offer Rate'},
							   { 'sTitle': 'Tier'},
							   { 'sTitle': 'Offer Size'},							   
							   { 'sTitle': 'Provider'},
							   { 'sTitle': 'OrderId'},
							   { 'sTitle': 'Timestamp'}
                           ];
        var tableData = [];

		/**if(rates.length > 0)
		{
		  $('#do_details').style.visibility="none";
		}
		else
		{
			$('#do_details').style.visibility="hidden";
		}**/
		 
		var bidRates=0;
		var offerRates=0;
        rates.DISPLAYORDERRATES;
        
        for (var i=0, len=rates.length; i< len; i++) {
            var rate = rates[i];
			
			if(rate.guid == "BENCHMARK")
			{
				var tmstmp=Number(rate.tmstmp);
		
				tableData.push([
	                (rate.bidPrice > 0) ? new Date(tmstmp) : "",
					"",
					(rate.bidPrice > 0) ? "BENCHMARK" : "",
					"",
					"",
					(rate.bidPrice > 0) ? rate.bidPrice + getBidDifference(rate.metaInfo) : "",
	                (rate.askPrice > 0) ? rate.askPrice + getAskDifference(rate.metaInfo) : "",
					"",
	                "",
					(rate.askPrice > 0) ? "BENCHMARK" : "",
					"",
					(rate.askPrice > 0) ? new Date(tmstmp) : "",
	            ]);
	            				
			}
			else
			{
				var bidtmstmp=Number(getBidOrderTmstmp(rate.guid));
				var offertmstmp=Number(getOfferOrderTmstmp(rate.guid));
				
	            tableData.push([
	                (rate.bidSize > 0) ? new Date(bidtmstmp) : "",
					(rate.bidSize > 0) ? getBidOrderId(rate.guid) : "",
					(rate.bidSize > 0) ? getBidProvider(rate.guid) : "",
					(rate.bidSize > 0) ? rate.bidSize : "",
					(rate.bidSize > 0) ? rate.tier : "",
					(rate.bidSize > 0) ? rate.bidPrice : "",
	                (rate.askSize > 0) ? rate.askPrice : "",
					(rate.askSize > 0) ? rate.tier : "",
	                (rate.askSize > 0) ? rate.askSize : "",
					(rate.askSize > 0) ? getOfferProvider(rate.guid,rate.bidSize > 0) : "",
					(rate.askSize > 0) ? getOfferOrderId(rate.guid) : "",
					(rate.askSize > 0) ? new Date(offertmstmp) : "",
	            ]);
	            
	            if( rate.bidSize > 0)
				   bidRates++;
				if(rate.askSize > 0)
				   offerRates++;
		  	}
        }
		tableOptions.aaSorting = []
		
		tableOptions.iDisplayLength =100;
		tableOptions.bSort=false;
		
        tableOptions.fnRowCallback = function(nRow, aaData, iDisplayIndex) {
            var className = '';
			
             if (aaData[2] == "BENCHMARK" || aaData[9] == "BENCHMARK")
             {
				 if(iDisplayIndex == 0)
                 	className='highlightbenchmarkrategreen';
				else
				    className='highlightbenchmarkrateorange';
					
             }
            $(nRow).addClass(className);
        };
		

        rateData = rates;
        rateTableData = tableData;
        IEM.Utils.createTable('displayOrderRatesTable', 'dorTable', 'displayOrderRatesTableContainer', tableData, tableOptions);
    }
	
	function getBidDifference(metaInfo)
	{
		var bidDifferenceEndIndex = metaInfo.indexOf(":");
		return " ( "+metaInfo.substring(14,bidDifferenceEndIndex)+" )";
	}
	
	function getAskDifference(metaInfo)
	{
		var askDifferenceEndIndex = metaInfo.lastIndexOf(":");
		var askDifferenceStartIndex = metaInfo.indexOf("AskDifference-");
		return " ( "+metaInfo.substring(askDifferenceStartIndex+14,askDifferenceEndIndex)+" )";
	}
	
	function getBidOrderTmstmp(guid)
	{
		var bidTmstmpLocation = guid.indexOf("Tmstmp-");
		if(bidTmstmpLocation == 0)
		{
			return 0;
		}
		var bidTmstmpDetails = guid.substring(bidTmstmpLocation + 7,bidTmstmpLocation + 21)
		var bidTmstmpDash = bidTmstmpDetails.indexOf("-");
		var bidTmstmp = bidTmstmpDetails.substring(0,bidTmstmpDash);
		return bidTmstmp;
	}
	
	function getBidOrderId(guid)
	{
		var bidOrderIdLocation = guid.indexOf("OrderId-");
		if(bidOrderIdLocation == 0)
		{
			return "";
		}
		var bidOrderIdDetails = guid.substring(bidOrderIdLocation + 8,bidOrderIdLocation + 18)
		var bidOrderIdDash = bidOrderIdDetails.indexOf("-");
		var bidOrderId = bidOrderIdDetails.substring(0,bidOrderIdDash);
		if(bidOrderId != "-")
		{	
			return "<a href=./orderdetails.html?orderId="+bidOrderId+" target=\"_blank\">"+bidOrderId+"</a>";
		}
		return "";
	}
	
	function nthOccurance() {
		var str = arguments[0];
		var c = arguments[1];
		var n = arguments[2];
		var pos = str.indexOf(c, 0);
		while (n-- > 0 && pos != -1)
			pos = str.indexOf(c, pos+1);
		return pos;
	}
	
	function getBidProvider(guid)
	{
		var start = nthOccurance(guid,'-',3);
		var end = nthOccurance(guid,'-',4);
		return guid.substring(start+1,end);
	}
	
	function getOfferProvider(guid,isBidPresent)
	{
		if(isBidPresent)
		{
			var start = nthOccurance(guid,'-',20);
			var end = nthOccurance(guid,'-',21);
			return guid.substring(start+1,end);	
		}
		else
		{
			var start = nthOccurance(guid,'-',5);
			var end = nthOccurance(guid,'-',6);
			return guid.substring(start+1,end);	
		}
	}

	function getOfferOrderTmstmp(guid)
	{
		var offerTmstmpLocation = guid.lastIndexOf("Tmstmp-");
		if(offerTmstmpLocation == 0)
		{
			return 0;
		}
		var offerTmstmp = guid.substring(offerTmstmpLocation + 7,offerTmstmpLocation + 21)
		return offerTmstmp;
	}
	
	function getOfferOrderId(guid)
	{
		var offerOrderIdLocation = guid.lastIndexOf("OrderId-");
		if(offerOrderIdLocation == 0)
		{
			return "";
		}
		var offerOrderIdDetails = guid.substring(offerOrderIdLocation + 8,offerOrderIdLocation + 18)
		var offerOrderIdDash = offerOrderIdDetails.indexOf("-");
		var offerOrderId = offerOrderIdDetails.substring(0,offerOrderIdDash);
		if(offerOrderId != "-")
		{	
			return "<a href=./orderdetails.html?orderId="+offerOrderId+" target=\"_blank\">"+offerOrderId+"</a>";
		
		}
		return "";
	}
	
	function getBenchmarkRates(oParams, successHandler) {
        var startTime = oParams.startTime;
        var endTime = oParams.startTime;
        var ccyPair = oParams.ccyPair;
        var url = IEM.cURL + 'benchmarks1/ccypair/';
        
        url += ccyPair + '/fromtime/' + startTime + '/totime/' + endTime + '/tier/1';

        IEM.Utils.ajaxRequest(url, function(o) {
            successHandler.call(this, o);
        },
        function() {
            alert("Problem in retrieving Benchmark Rates");
        }, 'Benchmark Rates', 'Getting Benchmark Rates...');
    }
    
	function handleBenchmarkResponse(benchmark) {
        var tableOptions = {}, tradeId, trade;

        tableOptions.columns = [
                               { 'sTitle': 'Time'},
                               { 'sTitle': 'Median'},
                               { 'sTitle': 'Range'}
                           ];
        var tableData = [], rate;

        for (var i=0, len=benchmark.length; i<len; i++) {
            rate = benchmark[i];

            tableData.push([
                new Date(rate.timeStamp),
                rate.medianMidPrice,
                rate.midPriceRange
            ]);
        }

        tableData.sort(function (a,b) {
            return Number(a[0]) - Number(b[0]);
        });

        IEM.Utils.createTable('oTBenchmarkTable', 'bTable', 'oTBenchmarkTableContainer', tableData, tableOptions);        
    }
	
	function createLinkForOrderId(guid)
	{
		var bidOrderIdLocation = guid.indexOf("OrderId-");
		var bidOrderIdDetails = guid.substring(bidOrderIdLocation + 8,bidOrderIdLocation + 18)
		var bidOrderIdDash = bidOrderIdDetails.indexOf("-");
		var bidOrderId = bidOrderIdDetails.substring(0,bidOrderIdDash);
		if(bidOrderId != "-")
		{	
			var bidOrderIdLink = "<a href=./orderdetails.html?orderId="+bidOrderId+" target=\"_blank\">"+bidOrderId+"</a>";
			guid = guid.replace(bidOrderId,bidOrderIdLink);
		}
		
		var offerOrderIdLocation = guid.lastIndexOf("OrderId-");
		var offerOrderIdDetails = guid.substring(offerOrderIdLocation + 8,offerOrderIdLocation + 18)
		var offerOrderIdDash = offerOrderIdDetails.indexOf("-");
		var offerOrderId = offerOrderIdDetails.substring(0,offerOrderIdDash);
		if(offerOrderId != "-")
		{	
			var offerOrderIdLink = "<a href=./orderdetails.html?orderId="+offerOrderId+" target=\"_blank\">"+offerOrderId+"</a>";
			guid = guid.replace(offerOrderId,offerOrderIdLink);
		}
        return guid;
	}

    return {
        setupGetDisplayOrderFormSubmit: setupGetDisplayOrderFormSubmit
    }

})();
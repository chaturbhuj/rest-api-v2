//Landscape Mode - More than an hour - separate orders/trades call - just getting summary. Benchmark Data shown. No Tick Data
//Zoomedin Mode1 - <= 30 min && >= 10 min - Orders/Trades Detailed Call, showing blotter tables with all info, Only benchmark Shown, No Tick Data
//Zoomedin Mode2 - <=10min - Orders/Trades Detailed Call, show blotter Tabs, Benchmark & Tickdata shown

//Utility function for adding zero to date & time when required.
function addZeroIfNeeded(number) {
    if (number <= 9) {
        return '0' + number;
    }
    return number;
}


// Date String function. Specific format for EMScope. This always returns time in GMT. using getTimezoneOffset to handle this.
function dateToString() {
    var dateObj = this;
    var timezoneOffset = dateObj.getTimezoneOffset() * 60000;
    dateObj = new Date(dateObj.getTime() + timezoneOffset);
    var displayDate = dateObj.getFullYear() + '-' + addZeroIfNeeded(dateObj.getMonth() + 1) + '-' + addZeroIfNeeded(dateObj.getDate()) + ' ' + addZeroIfNeeded(dateObj.getHours()) + ':' + addZeroIfNeeded(dateObj.getMinutes()) + ':' + addZeroIfNeeded(dateObj.getSeconds());
    var milliseconds = dateObj.getMilliseconds().toString();
    if (milliseconds.length < 3) {
        for (var i = 0, len = 3 - milliseconds.length, str = ''; i < len; i++) {
            str += '0';
        }
        milliseconds = str + milliseconds;
    }
    displayDate += '.' + milliseconds;
    return displayDate;
}

Date.prototype.toString = dateToString;

IEM.TS = (function() {
    // main function which sets up the forms in Order Collection Tab
    function setupTradeSummaryReport() {
        var tradeSummaryParams = {};
        getTradeSummaryStatistics(tradeSummaryParams, fillTradeSummaryStats);
    }

    // Landscape and Full Order-Trade Call based on time frame.
    function getTradeSummaryStatistics(oParams, successHandler) {


        var ordersURL = IEM.cURL + "trades2/tradesummarystats/none/none";


        IEM.Utils.ajaxRequest(ordersURL, function(o) {
            if (o.type == 'error' || o.type == 'warning') {
                alert(o.type + ' : ' + o.errorMessage);
                return;
            }
            successHandler.call(this, o);
        },
                function() {
                    alert("Problem fetching trade Summary - Call Failed.");
                }, 'Trade Summary', "Getting Trade Summary..."
                );
    }

    function fillTradeSummaryStats(tradeStats) {
        var tableOptions = {}, orderId, order;
        //Index	OrderID	OrderState	TakerOrg	CcyPair	Buy/Sell	OrderAmtUSD	OrderAmt	FilledAmount	OrderType	TIF	Channel
        tableOptions.columns = [
            {'sTitle': 'OrderId'},
            {'sTitle': 'OrderState'},
            {'sTitle': 'TakerOrg'},
            {'sTitle': 'CcyPair'},
            {'sTitle': 'Buy/Sell'},
            {'sTitle': 'OrderAmtUSD'},
            {'sTitle': 'OrderAmt'},
            {'sTitle': 'FilledAmount'},
            {'sTitle': 'OrderType'},
            {'sTitle': 'TIF'},
            {'sTitle': 'Channel'}
        ];

        var tableData = [];
        var statsSummary = JSON.parse(tradeStats.tradeStatsSummary);
        var typeSummary = JSON.parse(tradeStats.tradeTypeSummary);

        for (var i = 0, len = statsSummary.length; i < len; i++)
        {
            /* var order = orderSet[i];
             // SELECT CREATED,ORDERID, ORDERSTATE, ORG, CCYPAIR, BUYSELL, ORDERAMTUSD, ORDERAMT, FILLEDAMT, ORDERTYPE, TIMEINFORCE, CHANNEL
             tableData.push(['<a target="_blank" href="./home1.jsp?orderid=' + order.orderid + '">' + order.orderid + '</a>',
             order.orderstate,
             order.org,
             order.ccypair,
             order.buysell,
             order.orderamtusd,
             order.orderamt,
             order.filledamt,
             order.ordertype,
             order.timeinforce,
             order.channel]);*/
        }


        tableOptions.bPaginate = false;
        /*tableOptions.fnDrawCallback=function(oSettings ) {
         // $(oSettings.nTHead).hide();
         $(oSettings.nTFoot).hide();
         }*/
        // createTable('orderCollectionTable', 'ordTable', 'orderCollectionTableContainer', tableData, tableOptions);
    }


    function createTable(tableId, tableName, tableContainer, tableData, tableOptions) {
        if (IEM[tableName] != 'undefined') {
            $('#' + tableId + '_wrapper').remove();
            delete IEM[tableName];
        }

        var tableEle = $('<table></table>');
        tableEle.attr({
            'id': tableId,
            'class': 'table table-bordered',
        }).appendTo($('#' + tableContainer));

        var optionsObj = {
            "sDom": tableOptions.sDom || '<"clear">lTr<"scroller"t>ip',
            'aoColumns': tableOptions.columns,
            'bSort': false,
            'bInfo': false,
            'aaData': tableData,
            'bPaginate': (typeof tableOptions.bPaginate != 'undefined' ? tableOptions.bPaginate : true),
            'sPaginationType': 'simple',
            //  'aaSorting': [[]],//tableOptions.aaSorting || [[0,"asc"]],
            'fnRowCallback': tableOptions.fnRowCallback ? tableOptions.fnRowCallback : null,
            'fnDrawCallback': tableOptions.fnDrawCallback ? tableOptions.fnDrawCallback : null,
            'iDisplayLength': 50,
            "oTableTools": {
                "sSwfPath": "/emscope/web1/assets/swf/copy_csv_xls_pdf.swf",
                "aButtons": [
                    "print",
                    "xls",
                    "pdf"
                ]
            }
        };

        if (tableOptions.sScrollY) {
            optionsObj.sScrollY = tableOptions.sScrollY;
        }
        if (tableOptions.scrollY) {
            optionsObj.scrollY = tableOptions.scrollY;
        }
        if (tableOptions.sScrollX) {
            optionsObj.sScrollX = tableOptions.sScrollX;
        }
        if (tableOptions.scrollX) {
            optionsObj.scrollX = tableOptions.scrollX;
        }
        if (tableOptions.sScrollXInner) {
            optionsObj.sScrollXInner = tableOptions.sScrollXInner;
        }
        if (tableOptions.iDisplayLength) {
            optionsObj.iDisplayLength = tableOptions.iDisplayLength;
        }

        if (tableOptions.processing)
        {
            optionsObj.processing = tableOptions.processing;
        }

        if (tableOptions.serverSide)
        {
            optionsObj.serverSide = tableOptions.serverSide;
            optionsObj.ajax = tableOptions.ajax;
        }

        IEM[tableName] = $('#' + tableId).dataTable(optionsObj);
        // $('#buttondiv' ).show().css( 'visibility', 'visible' );
    }



    return {
        setupTradeSummaryReport: setupTradeSummaryReport
    }



})();/**
 * Created by kasi on 9/8/2014.
 */


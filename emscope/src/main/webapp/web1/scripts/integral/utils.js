IEM.Utils = {
    //Rounds the given timestamp ts to nearest second, by default, floors it, ceil would ceil it.
    roundTStoSec: function (ts, ceil) {
        if (ceil) {
            return Math.ceil(ts / 1000) * 1000;
        }
        return Math.floor(ts / 1000) * 1000;
    },
    ajaxRequest: function (url, successHandler, errorHandler, type, blockMsg, method) {

        var loadStatus = true;
        var start_time = new Date().getTime();
        $.blockUI({message: blockMsg});
        $.blockUI({message: blockMsg}); // CHROME 33.0.1750.117 m Issue. Only if I call twice, the Overlay layer is showing up.
        if (!method) {
            method = "GET";
        }       
        $.ajax({
            type: method,
            url: url,
            cache: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
//            async: false,
            success: function (o) {
                loadStatus = false;
                $.unblockUI();
                if (typeof o.isError != 'undefined' && o.isError == true) {
                    if (o.message) {
                        alert(o.message);
                        window.location.href = "/emscope/web1/login.jsp";
                        return;
                    }
                }
                if (type)
                    IEM.Utils.addMsgToPerfConsole('<span class="success">' + type + ': <span class="boldtext">' + (new Date().getTime() - start_time) + 'ms</span></span>');
                successHandler.call(this, o);
                hideTradeEventLegend();
            },
            error: function (o) {
                loadStatus = false;
                $('#loadingTimeModal').modal('hide');
                $.unblockUI();
                if (type)
                    IEM.Utils.addMsgToPerfConsole('<span class="failed">' + type + ': <span class="boldtext">' + (new Date().getTime() - start_time) + 'ms</span></span>');
                errorHandler.call(this, o);
            }
        });
    },
    syncajaxRequest: function (url, successHandler, errorHandler, type, blockMsg, method) {

        var loadStatus = true;
        var start_time = new Date().getTime();
        $.blockUI({message: blockMsg});
        $.blockUI({message: blockMsg}); // CHROME 33.0.1750.117 m Issue. Only if I call twice, the Overlay layer is showing up.
        if (!method) {
            method = "GET";
        }
//        setTimeout(function () {
//            if (loadStatus) {
//                $('#loadingTimeModal').modal('show');
//            }
//        }, 5000);
        $.ajax({            
            type: method,
            url: url,
            cache: true,
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (o) {
                loadStatus = false;
                $('#loadingTimeModal').modal('hide');
                $.unblockUI();
                if (typeof o.isError != 'undefined' && o.isError == true) {
                    if (o.message) {
                        alert(o.message);
                        window.location.href = "/emscope/web1/login.jsp";
                        return;
                    }
                }
                if (type)
                    IEM.Utils.addMsgToPerfConsole('<span class="success">' + type + ': <span class="boldtext">' + (new Date().getTime() - start_time) + 'ms</span></span>');
                successHandler.call(this, o);
            },
            error: function (o) {
                loadStatus = false;
                $('#loadingTimeModal').modal('hide');
                $.unblockUI();
                if (type)
                    IEM.Utils.addMsgToPerfConsole('<span class="failed">' + type + ': <span class="boldtext">' + (new Date().getTime() - start_time) + 'ms</span></span>');
                errorHandler.call(this, o);
            }
        });
    },
    addMsgToPerfConsole: function addMsgToPerfConsole(msg) {
        if (!addMsgToPerfConsole.console || (addMsgToPerfConsole.console && !addMsgToPerfConsole.console.length)) {
            addMsgToPerfConsole.console = $('.perf-console .messages');
            if (!addMsgToPerfConsole.console.length) {
                return;
            }
        }
        var consoleEle = addMsgToPerfConsole.console;
        consoleEle.append('<div>' + msg + '</div>');
        consoleEle.animate({
            scrollTop: consoleEle.outerHeight(true)
        }, 100);
    },
    createTable: function (tableId, tableName, tableContainer, tableData, tableOptions) {
        if (IEM[tableName] != 'undefined') {
            $('#' + tableId + '_wrapper').remove();
            delete IEM[tableName];
        }

        var tableEle = $('<table></table>');
        tableEle.attr({
            'id': tableId,
            'class': 'table table-bordered',
        }).appendTo($('#' + tableContainer));

        var optionsObj = {
            "sDom": tableOptions.sDom || '<"clear">lTr<"scroller"t>ip',
            'aoColumns': tableOptions.columns,
            'aaData': tableData,
            'bPaginate': (typeof tableOptions.bPaginate != 'undefined' ? tableOptions.bPaginate : true),
            'sPaginationType': 'full_numbers',
            'bSort': (typeof tableOptions.bSort != 'undefined' ? tableOptions.bSort : true),
            'aaSorting': tableOptions.aaSorting || [[0, "asc"]],
            'fnRowCallback': tableOptions.fnRowCallback ? tableOptions.fnRowCallback : null,
            // 'iDisplayLength' : 100,
            "oTableTools": {
                "sSwfPath": "/emscope/web1/assets/swf/copy_csv_xls_pdf.swf",
                "aButtons": [
                    "print",
                    "xls",
                    "pdf"
                ]
            }
        };

        if (tableOptions.sScrollY) {
            optionsObj.sScrollY = tableOptions.sScrollY;
        }
        if (tableOptions.scrollY) {
            optionsObj.scrollY = tableOptions.scrollY;
        }
        if (tableOptions.sScrollX) {
            optionsObj.sScrollX = tableOptions.sScrollX;
        }
        if (tableOptions.scrollX) {
            optionsObj.scrollX = tableOptions.scrollX;
        }
        if (tableOptions.sScrollXInner) {
            optionsObj.sScrollXInner = tableOptions.sScrollXInner;
        }
        if (tableOptions.iDisplayLength) {
            optionsObj.iDisplayLength = tableOptions.iDisplayLength;
        }

        if (tableOptions.processing)
        {
            optionsObj.processing = tableOptions.processing;
        }

        if (tableOptions.serverSide)
        {
            optionsObj.serverSide = tableOptions.serverSide;
            optionsObj.ajax = tableOptions.ajax;
        }
        IEM[tableName] = $('#' + tableId).dataTable(optionsObj);
    }

};
function fillOrdersTable(orders) {
    var tableOptions = {}, orderId, order;
    //Index	OrderID	OrderState	TakerOrg	CcyPair	Buy/Sell	OrderAmtUSD	OrderAmt	FilledAmount	OrderType	TIF	Channel
    tableOptions.columns = [
        {'sTitle': 'OrderId'},
        {'sTitle': 'OrderState'},
        {'sTitle': 'TakerOrg'},
        {'sTitle': 'CcyPair'},
        {'sTitle': 'Buy/Sell'},
        {'sTitle': 'OrderAmtUSD'},
        {'sTitle': 'OrderAmt'},
        {'sTitle': 'FilledAmount'},
        {'sTitle': 'OrderType'},
        {'sTitle': 'TIF'},
        {'sTitle': 'Channel'}
    ];

    var tableData = [];
    for (var i = 0, len = orders.length; i < len; i++)
    {
        var order = orders[i];
        // SELECT CREATED,ORDERID, ORDERSTATE, ORG, CCYPAIR, BUYSELL, ORDERAMTUSD, ORDERAMT, FILLEDAMT, ORDERTYPE, TIMEINFORCE, CHANNEL
        tableData.push([order.orderid,
            order.orderstate,
            order.org,
            order.ccypair,
            order.buysell,
            order.orderamtusd,
            order.orderamt,
            order.filledamt,
            order.ordertype,
            order.timeinforce,
            order.channel]);
    }

    tableOptions.bPaginate = false;
    IEM.Utils.createTable('orderCollectionTable', 'ordTable', 'orderCollectionTableContainer', tableData, tableOptions);
}

function prev() {
    var ordersURL = IEM.cURL + "orderswithtrades/prev";


    IEM.Utils.ajaxRequest(ordersURL, function (o) {
        if (o.type == 'error' || o.type == 'warning') {
            alert(o.type + ' : ' + o.errorMessage);
            return;
        }
        fillOrdersTable.call(this, o);
    },
            function () {
                alert("Problem fetching Order Collection - Call Failed.");
            }, 'Order Collection', "Getting Order Collection..."
            );
}

function next() {
    var ordersURL = IEM.cURL + "orderswithtrades/next";


    IEM.Utils.ajaxRequest(ordersURL, function (o) {
        if (o.type == 'error' || o.type == 'warning') {
            alert(o.type + ' : ' + o.errorMessage);
            return;
        }
        fillOrdersTable.call(this, o);
    },
            function () {
                alert("Problem fetching Order Collection - Call Failed.");
            }, 'Order Collection', "Getting Order Collection..."
            );
}
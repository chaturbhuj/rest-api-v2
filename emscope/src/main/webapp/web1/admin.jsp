<%@ page import="java.util.*" %>
<%@ page import="com.integral.ds.boot.ResultWrapper" %>
<%@ page import="com.integral.ds.event.EventAggregationBean" %>
<jsp:useBean id="RESULT" class="com.integral.ds.boot.ResultWrapper" scope="request" />
<html>
<head>
<title>
EMScope Admin Page
</title>

<style>
table {
    border-collapse: collapse;
}

table, td, th {
    border: 2px solid black;padding: 3px;
}
</style>

</head>
<body>
<div id="header" style="margin-left:17cm;">
<h3>
Emscope Admin portal
</h3>
</div>
<div id="table" style="margin-left:2cm;width:1310px;margin-left:10cm;">
<table width="700" cellspacing="0" cellpadding="0" style="border: 1px solid black;">
    <tr style="font-weight:normal;font-weight:bold;font-weight:900;">
        <td>
        EVENT DATE
        </td>
        <td>
        USER NAME
        </td>
        <td>
        EVENT TYPE
        </td>
        <td>
        EVENT NAME
        </td>
        <td>
        COUNT
        </td>
    </tr>
    <%
        EventAggregationBean current = null;
        List<EventAggregationBean> tuples = (List<EventAggregationBean>) RESULT.getBean();
        for(int i=0; i<tuples.size(); i++)
        {
            current = tuples.get(i);
        %>
    <tr>
        <td>
        <%
            out.print(current.getEventDate());
        %>
        </td>
        <td>
        <%
            out.print(current.getUserName());
        %>
        </td>
        <td>
        <%
            out.print(current.getEventType());
        %>
        </td>
        <td>
       <%
            out.print(current.getEventName());
        %>
        </td>
        <td>
        <%
           out.print(current.getCount());
        %>
       </td>
    </tr>
    <% }%>
</table>
</div>

</body>
</html>

<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<html>

<head>
    <title>EMScope</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <link rel=stylesheet type="text/css" href="/web1/styles/bootstrap/3.0.2/bootstrap.min.css"/>
    <link rel=stylesheet type="text/css" href="/web1/styles/bootstrap/3.0.2/bootstrap-theme.min.css"/>
    <link rel="stylesheet" type="text/css" href="/web1/styles/style.css" />

    <script type="text/javascript">
        var contextPath = "<%=request.getContextPath()%>";
    </script>
</head>
<body>
    <img src="/web1/img/login_background.jpg" class="login-bg" />
    <div class="mask-login"></div>
    <div class="login-body">
        <div class="login-container">
            <div class="login-header">
                <h3>
                    Integral EMScope
                </h3>
            </div>
            <div class="login-form-content">
                <div class="errMsgContainer"></div>
                <form class="form" role="form" name="loginForm" method="POST">
                    <div class="form-group">
                        <label for="username">Username</label>
                            <input type="text" autofocus class="form-control" id="username" name="username" placeholder="Enter Username" required/>
                    </div>
                    <div class="form-group ">
                        <label for="password">Password</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Enter Password" required/>
                    </div>
					<div class="form-group ">
                        <label for="organization">Organization</label>
                        <input type="text" id="organization" name="organization" class="form-control" placeholder="Enter organization"/>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block login-btn">Submit</button>
                </form>
                    <div class="versionDetails">Build Number: 2.4</div>
            </div>
        </div>
    </div>
    <div class="perf-console">
        <h6 class="pc-header">
            <span>Performance Console</span>
            <div class="pc-close">
                X
            </div>
        </h6>
        <div class="messages">

        </div>
    </div>
    <!-- jQuery JS -->
    <script src="/web1/scripts/jquery/1.10.2/jquery.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="/web1/scripts/bootstrap/3.0.2/bootstrap.min.js"></script>
    <script src="/web1/scripts/bootstrap/plugins/jquery.blockUI.js"></script>

    <script src="/web1/scripts/integral/main.js"></script>
    <script src="/web1/scripts/integral/utils.js"></script>
    <script src="/web1/scripts/integral/user.js"></script>
</body>
</html>
<%
    response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0);
%>
<%@ page import="java.util.Date" %>

<!DOCTYPE html>
<html>
    <%!
        long timestamp = new Date().getTime();
    %>
    <head>
        <title>EMScope</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="content-type" content="text/html; charset=UTF8">

        <link rel=stylesheet type="text/css" href="styles/bootstrap/3.0.2/bootstrap.min.css"/>
        <link rel=stylesheet type="text/css" href="styles/bootstrap/3.0.2/bootstrap-theme.min.css"/>

        <link type="text/css" href="styles/bootstrap/plugins/daterangepicker-bs3.css" rel="stylesheet">
        <link type="text/css" href="styles/bootstrap/plugins/datatables.css" rel="stylesheet">
        <link type="text/css" href="styles/bootstrap/plugins/dc.css" rel="stylesheet">
        <link type="text/css" href="styles/bootstrap/plugins/bootstrap-select.css" rel="stylesheet">
        <link type="text/css" href="styles/bootstrap/plugins/dataTables.tableTools.min.css" rel="stylesheet">
        <link type="text/css" href="styles/bootstrap/plugins/bootstrap-switch.min.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="styles/style_5.css" />
        <%--<link type="text/css" href="jquery/plugins/bootstrap-multiselect.css" rel="stylesheet">--%>
        <%-- <link rel="stylesheet" type="text/css" href="styles/integral.css" />

   <!-- <link rel="stylesheet" type="text/css" href="jquery/css/ui.jqgrid.css" />  -->
    <script src="jquery/js/jquery.jqGrid.min.js"></script>
    <script src="jquery/js/jquery.jquery.jqGrid.src.js"></script>
    <script src="jquery/js/jquery-1.11.0.min.js"></script>--%>
        <%--<script src="jquery/plugins/bootstrap-multiselect.js"></script>--%>


        <script type="text/javascript">
            var contextPath = "<%=request.getContextPath()%>";
            function orderTypeSelectall() {
                var x = document.getElementById("searchOrderType");
                for (var i = 0; i < x.length; i++)
                {
                    x.options[i].selected = true;
                }
            }

            function orderTypeUnselectall() {
                var x = document.getElementById("searchOrderType");
                for (var i = 0; i < x.length; i++)
                {
                    x.options[i].selected = false;
                }
            }

            function orderDateSelectall() {
                var y = document.getElementById("searchDate");
                for (var i = 0; i < y.length; i++)
                {
                    y.options[i].selected = true;
                }
            }

            function orderDateUnselectall() {
                var y = document.getElementById("searchDate");
                for (var i = 0; i < y.length; i++)
                {
                    y.options[i].selected = false;
                }
            }

            function takerOrgSelectall() {
                var z = document.getElementById("searchTakerOrg");
                for (var i = 0; i < z.length; i++)
                {
                    z.options[i].selected = true;
                }
            }

            function takerOrgUnselectall() {
                var z = document.getElementById("searchTakerOrg");
                for (var i = 0; i < z.length; i++)
                {
                    z.options[i].selected = false;
                }
            }

            function ccyPairSelectall() {
                var a = document.getElementById("searchCcyPair");
                for (var i = 0; i < a.length; i++)
                {
                    a.options[i].selected = true;
                }
            }

            function ccyPairUnselectall() {
                var a = document.getElementById("searchCcyPair");
                for (var i = 0; i < a.length; i++)
                {
                    a.options[i].selected = false;
                }
            }

            function origOrgSelectall() {
                var b = document.getElementById("searchOrigOrg");
                for (var i = 0; i < b.length; i++)
                {
                    b.options[i].selected = true;
                }
            }

            function origOrgUnselectall() {
                var b = document.getElementById("searchOrigOrg");
                for (var i = 0; i < b.length; i++)
                {
                    b.options[i].selected = false;
                }
            }

        </script>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Integral EMScope</a>
                </div>
                <div class="navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a id = "tab1" href='#emscopetab-2' data-toggle="tab">By Order ID</a></li>
                        <li><a id = "tab2" href='#emscopetab-1' data-toggle="tab">EMScope</a></li>
                        <li><a id = "tab3" href='#emscopetab-3' data-toggle="tab">Display Order</a></li>
                        <li><a id = "tab4" href='#emscopetab-4' data-toggle="tab">Order Set</a></li>
                        <li><a id = "tab5" href='#emscopetab-5' data-toggle="tab">Control Charts</a></li>
                        <li><a id = "tab6" href='#emscopetab-6' data-toggle="tab">D3 OrderChart</a></li>
                        <li><a href="#" id="logout"><span class="glyphicon glyphicon-user"></span> Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="body-content" style="width: 95%;">
            <ul class="nav nav-tabs page-tabs" style="display:none;">
                <li class="active"><a id = "tab1" href='#emscopetab-2' data-toggle="tab">By Order ID</a></li>
                <li><a id = "tab2" href='#emscopetab-1' data-toggle="tab">EMScope</a></li>
                <li><a id = "tab3" href='#emscopetab-3' data-toggle="tab">Display Order</a></li>
                <li><a id = "tab4" href='#emscopetab-4' data-toggle="tab">Order Set</a></li>
                    <%--<li onclick="javascript:tradesummarydisplay();"><a id = "tab5" href='#emscopetab-5' data-toggle="tab">Trade-Summary</a></li>--%>
            </ul>
            <div class="tab-content page-tab-content row" style="margin-top: -62px;margin-left: 23px;">                
                <div class="tab-pane fade row" id="emscopetab-1">
                    <div class="form-holder" style="margin-left: 40px;">
                        <form class="form-inline" role="form" name="getOrdersForm">
                            <div class="row">
                                <div class="form-group col-md-2">
                                    <label for="ccyPair">Currency Pair</label>
                                    <select class="form-control multiselect" id="ccyPair" name="ccyPair" data-live-search="true">
                                        <option value="EUR/USD">EUR/USD</option> <option value="AED/BHD">AED/BHD</option> <option value="AED/CHF">AED/CHF</option> <option value="AED/EUR">AED/EUR</option> <option value="AED/GBP">AED/GBP</option> <option value="AED/JPY">AED/JPY</option> <option value="AED/KWD">AED/KWD</option> <option value="AED/OMR">AED/OMR</option> <option value="AED/QAR">AED/QAR</option> <option value="AED/SAR">AED/SAR</option> <option value="AED/USD">AED/USD</option> <option value="AED/ZAR">AED/ZAR</option> <option value="AUD/AED">AUD/AED</option> <option value="AUD/CAD">AUD/CAD</option> <option value="AUD/CHF">AUD/CHF</option> <option value="AUD/CNH">AUD/CNH</option> <option value="AUD/DKK">AUD/DKK</option> <option value="AUD/EUR">AUD/EUR</option> <option value="AUD/GBP">AUD/GBP</option> <option value="AUD/HKD">AUD/HKD</option> <option value="AUD/IDR">AUD/IDR</option> <option value="AUD/JPY">AUD/JPY</option> <option value="AUD/KWD">AUD/KWD</option> <option value="AUD/MXN">AUD/MXN</option> <option value="AUD/NOK">AUD/NOK</option> <option value="AUD/NZD">AUD/NZD</option> <option value="AUD/RUB">AUD/RUB</option> <option value="AUD/SAR">AUD/SAR</option> <option value="AUD/SEK">AUD/SEK</option> <option value="AUD/SGD">AUD/SGD</option> <option value="AUD/THB">AUD/THB</option> <option value="AUD/USD">AUD/USD</option> <option value="AUD/ZAR">AUD/ZAR</option> <option value="BHD/AED">BHD/AED</option> <option value="BRL/USD">BRL/USD</option> <option value="CAD/AED">CAD/AED</option> <option value="CAD/AUD">CAD/AUD</option> <option value="CAD/CHF">CAD/CHF</option> <option value="CAD/CZK">CAD/CZK</option> <option value="CAD/DKK">CAD/DKK</option> <option value="CAD/EUR">CAD/EUR</option> <option value="CAD/GBP">CAD/GBP</option> <option value="CAD/HKD">CAD/HKD</option> <option value="CAD/JPY">CAD/JPY</option> <option value="CAD/MXN">CAD/MXN</option> <option value="CAD/NOK">CAD/NOK</option> <option value="CAD/PLN">CAD/PLN</option> <option value="CAD/RUB">CAD/RUB</option> <option value="CAD/SAR">CAD/SAR</option> <option value="CAD/SEK">CAD/SEK</option> <option value="CAD/SGD">CAD/SGD</option> <option value="CAD/THB">CAD/THB</option> <option value="CAD/USD">CAD/USD</option> <option value="CAD/ZAR">CAD/ZAR</option> <option value="CHF/AED">CHF/AED</option> <option value="CHF/CAD">CHF/CAD</option> <option value="CHF/CNH">CHF/CNH</option> <option value="CHF/CZK">CHF/CZK</option> <option value="CHF/DKK">CHF/DKK</option> <option value="CHF/EUR">CHF/EUR</option> <option value="CHF/HKD">CHF/HKD</option> <option value="CHF/HUF">CHF/HUF</option> <option value="CHF/JPY">CHF/JPY</option> <option value="CHF/KES">CHF/KES</option> <option value="CHF/MXN">CHF/MXN</option> <option value="CHF/NOK">CHF/NOK</option> <option value="CHF/PLN">CHF/PLN</option> <option value="CHF/RON">CHF/RON</option> <option value="CHF/RUB">CHF/RUB</option> <option value="CHF/SAR">CHF/SAR</option> <option value="CHF/SEK">CHF/SEK</option> <option value="CHF/SGD">CHF/SGD</option> <option value="CHF/THB">CHF/THB</option> <option value="CHF/TRL">CHF/TRL</option> <option value="CHF/TRY">CHF/TRY</option> <option value="CHF/USD">CHF/USD</option> <option value="CHF/ZAR">CHF/ZAR</option> <option value="CNH/JPY">CNH/JPY</option> <option value="CNH/RUB">CNH/RUB</option> <option value="CZK/CHF">CZK/CHF</option> <option value="CZK/JPY">CZK/JPY</option> <option value="CZK/SEK">CZK/SEK</option> <option value="DKK/CHF">DKK/CHF</option> <option value="DKK/CZK">DKK/CZK</option> <option value="DKK/EUR">DKK/EUR</option> <option value="DKK/JPY">DKK/JPY</option> <option value="DKK/NOK">DKK/NOK</option> <option value="DKK/SEK">DKK/SEK</option> <option value="DKK/USD">DKK/USD</option> <option value="EUR/AED">EUR/AED</option> <option value="EUR/AUD">EUR/AUD</option> <option value="EUR/BGN">EUR/BGN</option> <option value="EUR/CAD">EUR/CAD</option> <option value="EUR/CHF">EUR/CHF</option> <option value="EUR/CLP">EUR/CLP</option> <option value="EUR/CNH">EUR/CNH</option> <option value="EUR/CNY">EUR/CNY</option> <option value="EUR/CZK">EUR/CZK</option> <option value="EUR/DKK">EUR/DKK</option> <option value="EUR/GBP">EUR/GBP</option> <option value="EUR/HKD">EUR/HKD</option> <option value="EUR/HRK">EUR/HRK</option> <option value="EUR/HUF">EUR/HUF</option> <option value="EUR/ILS">EUR/ILS</option> <option value="EUR/INR">EUR/INR</option> <option value="EUR/ISK">EUR/ISK</option> <option value="EUR/JPY">EUR/JPY</option> <option value="EUR/KWD">EUR/KWD</option> <option value="EUR/LVL">EUR/LVL</option> <option value="EUR/MXN">EUR/MXN</option> <option value="EUR/NOK">EUR/NOK</option> <option value="EUR/NZD">EUR/NZD</option> <option value="EUR/PLN">EUR/PLN</option> <option value="EUR/RON">EUR/RON</option><option value="EUR/RSD">EUR/RSD</option><option value="EUR/RU0">EUR/RU0</option> <option value="EUR/RUB">EUR/RUB</option><option value="EUR/SAR">EUR/SAR</option><option value="EUR/SEK">EUR/SEK</option> <option value="EUR/SGD">EUR/SGD</option><option value="EUR/THB">EUR/THB</option><option value="EUR/TRL">EUR/TRL</option> <option value="EUR/TRY">EUR/TRY</option><option value="EUR/ZAR">EUR/ZAR</option> <option value="GBP/AED">GBP/AED</option><option value="GBP/AUD">GBP/AUD</option><option value="GBP/BGN">GBP/BGN</option> <option value="GBP/BHD">GBP/BHD</option><option value="GBP/CAD">GBP/CAD</option><option value="GBP/CHF">GBP/CHF</option> <option value="GBP/CLP">GBP/CLP</option><option value="GBP/CZK">GBP/CZK</option><option value="GBP/DKK">GBP/DKK</option> <option value="GBP/EUR">GBP/EUR</option><option value="GBP/HKD">GBP/HKD</option><option value="GBP/HUF">GBP/HUF</option> <option value="GBP/ILS">GBP/ILS</option><option value="GBP/INR">GBP/INR</option><option value="GBP/JPY">GBP/JPY</option> <option value="GBP/MUR">GBP/MUR</option><option value="GBP/MXN">GBP/MXN</option><option value="GBP/NOK">GBP/NOK</option> <option value="GBP/NZD">GBP/NZD</option><option value="GBP/OMR">GBP/OMR</option><option value="GBP/PHP">GBP/PHP</option> <option value="GBP/PLN">GBP/PLN</option><option value="GBP/QAR">GBP/QAR</option><option value="GBP/RUB">GBP/RUB</option> <option value="GBP/SAR">GBP/SAR</option><option value="GBP/SEK">GBP/SEK</option><option value="GBP/SGD">GBP/SGD</option> <option value="GBP/THB">GBP/THB</option><option value="GBP/TRL">GBP/TRL</option><option value="GBP/TRY">GBP/TRY</option> <option value="GBP/TWD">GBP/TWD</option><option value="GBP/USD">GBP/USD</option><option value="GBP/ZAR">GBP/ZAR</option> <option value="HKD/CNH">HKD/CNH</option><option value="HKD/EUR">HKD/EUR</option><option value="HKD/JPY">HKD/JPY</option> <option value="HKD/NOK">HKD/NOK</option><option value="HKD/THB">HKD/THB</option><option value="HKD/USD">HKD/USD</option> <option value="HUF/EUR">HUF/EUR</option><option value="ILS/USD">ILS/USD</option><option value="INR/CHF">INR/CHF</option> <option value="ISK/CHF">ISK/CHF</option><option value="JPY/AED">JPY/AED</option><option value="JPY/BGN">JPY/BGN</option> <option value="JPY/CHF">JPY/CHF</option><option value="JPY/EUR">JPY/EUR</option><option value="JPY/HKD">JPY/HKD</option> <option value="JPY/KRW">JPY/KRW</option><option value="JPY/MYR">JPY/MYR</option><option value="JPY/SGD">JPY/SGD</option> <option value="JPY/TRY">JPY/TRY</option><option value="JPY/USD">JPY/USD</option><option value="KWD/AED">KWD/AED</option> <option value="KWD/BHD">KWD/BHD</option><option value="KWD/CAD">KWD/CAD</option><option value="MXN/JPY">MXN/JPY</option> <option value="MXN/SEK">MXN/SEK</option><option value="MXN/USD">MXN/USD</option><option value="NOK/CAD">NOK/CAD</option> <option value="NOK/CHF">NOK/CHF</option><option value="NOK/CZK">NOK/CZK</option><option value="NOK/DKK">NOK/DKK</option> <option value="NOK/GBP">NOK/GBP</option><option value="NOK/HKD">NOK/HKD</option><option value="NOK/JPY">NOK/JPY</option> <option value="NOK/NZD">NOK/NZD</option><option value="NOK/SEK">NOK/SEK</option><option value="NOK/USD">NOK/USD</option> <option value="NZD/AED">NZD/AED</option><option value="NZD/AUD">NZD/AUD</option><option value="NZD/CAD">NZD/CAD</option> <option value="NZD/CHF">NZD/CHF</option><option value="NZD/DKK">NZD/DKK</option><option value="NZD/GBP">NZD/GBP</option> <option value="NZD/HKD">NZD/HKD</option><option value="NZD/JPY">NZD/JPY</option><option value="NZD/MYR">NZD/MYR</option> <option value="NZD/NOK">NZD/NOK</option><option value="NZD/SAR">NZD/SAR</option><option value="NZD/SEK">NZD/SEK</option> <option value="NZD/SGD">NZD/SGD</option><option value="NZD/USD">NZD/USD</option><option value="OMR/AED">OMR/AED</option> <option value="PLN/CHF">PLN/CHF</option><option value="PLN/CZK">PLN/CZK</option><option value="PLN/DKK">PLN/DKK</option> <option value="PLN/HUF">PLN/HUF</option><option value="PLN/JPY">PLN/JPY</option><option value="PLN/NOK">PLN/NOK</option> <option value="PLN/SEK">PLN/SEK</option><option value="QAR/AED">QAR/AED</option><option value="RUB/BGN">RUB/BGN</option> <option value="RUB/JPY">RUB/JPY</option><option value="SAR/AED">SAR/AED</option><option value="SAR/CHF">SAR/CHF</option> <option value="SAR/JPY">SAR/JPY</option><option value="SAR/SEK">SAR/SEK</option><option value="SAR/USD">SAR/USD</option> <option value="SEK/BGN">SEK/BGN</option><option value="SEK/CHF">SEK/CHF</option><option value="SEK/DKK">SEK/DKK</option> <option value="SEK/EUR">SEK/EUR</option><option value="SEK/GBP">SEK/GBP</option><option value="SEK/JPY">SEK/JPY</option> <option value="SEK/PLN">SEK/PLN</option><option value="SEK/RUB">SEK/RUB</option><option value="SEK/THB">SEK/THB</option> <option value="SEK/TRY">SEK/TRY</option><option value="SEK/USD">SEK/USD</option><option value="SGD/AED">SGD/AED</option> <option value="SGD/AUD">SGD/AUD</option><option value="SGD/CHF">SGD/CHF</option><option value="SGD/CNH">SGD/CNH</option> <option value="SGD/GBP">SGD/GBP</option><option value="SGD/HKD">SGD/HKD</option><option value="SGD/JPY">SGD/JPY</option> <option value="SGD/NOK">SGD/NOK</option><option value="SGD/NZD">SGD/NZD</option><option value="SGD/PLN">SGD/PLN</option> <option value="SGD/THB">SGD/THB</option><option value="SGD/USD">SGD/USD</option><option value="THB/JPY">THB/JPY</option> <option value="THB/USD">THB/USD</option><option value="TRY/JPY">TRY/JPY</option><option value="TRY/PLN">TRY/PLN</option> <option value="TRY/USD">TRY/USD</option><option value="USD/AE0">USD/AE0</option><option value="USD/AE1">USD/AE1</option> <option value="USD/AED">USD/AED</option><option value="USD/ARS">USD/ARS</option><option value="USD/AUD">USD/AUD</option> <option value="USD/BBD">USD/BBD</option><option value="USD/BDT">USD/BDT</option><option value="USD/BGN">USD/BGN</option> <option value="USD/BH1">USD/BH1</option><option value="USD/BHD">USD/BHD</option><option value="USD/BIF">USD/BIF</option> <option value="USD/BND">USD/BND</option><option value="USD/BOB">USD/BOB</option><option value="USD/BRL">USD/BRL</option> <option value="USD/BYR">USD/BYR</option><option value="USD/CAD">USD/CAD</option><option value="USD/CHF">USD/CHF</option> <option value="USD/CLP">USD/CLP</option><option value="USD/CNH">USD/CNH</option><option value="USD/CNY">USD/CNY</option> <option value="USD/COP">USD/COP</option><option value="USD/CRC">USD/CRC</option><option value="USD/CZK">USD/CZK</option> <option value="USD/DJF">USD/DJF</option><option value="USD/DKK">USD/DKK</option><option value="USD/DOP">USD/DOP</option> <option value="USD/EGP">USD/EGP</option><option value="USD/ETB">USD/ETB</option><option value="USD/EUR">USD/EUR</option> <option value="USD/GBP">USD/GBP</option><option value="USD/GMD">USD/GMD</option><option value="USD/GTQ">USD/GTQ</option> <option value="USD/HKD">USD/HKD</option><option value="USD/HNL">USD/HNL</option><option value="USD/HRK">USD/HRK</option> <option value="USD/HTG">USD/HTG</option><option value="USD/HUF">USD/HUF</option><option value="USD/IDR">USD/IDR</option> <option value="USD/ILS">USD/ILS</option><option value="USD/INR">USD/INR</option><option value="USD/ISK">USD/ISK</option> <option value="USD/JMD">USD/JMD</option><option value="USD/JOD">USD/JOD</option><option value="USD/JPY">USD/JPY</option> <option value="USD/KES">USD/KES</option><option value="USD/KRW">USD/KRW</option><option value="USD/KWD">USD/KWD</option> <option value="USD/KZT">USD/KZT</option><option value="USD/LBP">USD/LBP</option><option value="USD/LKR">USD/LKR</option> <option value="USD/LSL">USD/LSL</option><option value="USD/LTL">USD/LTL</option><option value="USD/LVL">USD/LVL</option> <option value="USD/MAD">USD/MAD</option><option value="USD/MRO">USD/MRO</option><option value="USD/MUR">USD/MUR</option> <option value="USD/MWK">USD/MWK</option><option value="USD/MXN">USD/MXN</option><option value="USD/MYR">USD/MYR</option> <option value="USD/NAD">USD/NAD</option><option value="USD/NGN">USD/NGN</option><option value="USD/NIO">USD/NIO</option> <option value="USD/NOK">USD/NOK</option><option value="USD/NPR">USD/NPR</option><option value="USD/NZD">USD/NZD</option> <option value="USD/OM1">USD/OM1</option><option value="USD/OMR">USD/OMR</option><option value="USD/PEN">USD/PEN</option> <option value="USD/PHP">USD/PHP</option><option value="USD/PKR">USD/PKR</option><option value="USD/PLN">USD/PLN</option> <option value="USD/PYG">USD/PYG</option><option value="USD/QA1">USD/QA1</option><option value="USD/QAR">USD/QAR</option> <option value="USD/RON">USD/RON</option><option value="USD/RSD">USD/RSD</option><option value="USD/RU0">USD/RU0</option> <option value="USD/RUB">USD/RUB</option><option value="USD/RUX">USD/RUX</option><option value="USD/SA1">USD/SA1</option> <option value="USD/SAR">USD/SAR</option><option value="USD/SBD">USD/SBD</option><option value="USD/SEK">USD/SEK</option> <option value="USD/SGD">USD/SGD</option><option value="USD/SZL">USD/SZL</option><option value="USD/THB">USD/THB</option> <option value="USD/TND">USD/TND</option><option value="USD/TRL">USD/TRL</option><option value="USD/TRY">USD/TRY</option> <option value="USD/TTD">USD/TTD</option><option value="USD/TWD">USD/TWD</option><option value="USD/TZS">USD/TZS</option> <option value="USD/UGX">USD/UGX</option><option value="USD/UYU">USD/UYU</option><option value="USD/VND">USD/VND</option> <option value="USD/VUV">USD/VUV</option><option value="USD/XAF">USD/XAF</option><option value="USD/ZAR">USD/ZAR</option> <option value="USD/ZMK">USD/ZMK</option><option value="XAG/AUD">XAG/AUD</option><option value="XAG/CAD">XAG/CAD</option> <option value="XAG/CHF">XAG/CHF</option><option value="XAG/EUR">XAG/EUR</option><option value="XAG/GBP">XAG/GBP</option> <option value="XAG/USD">XAG/USD</option><option value="XAU/AUD">XAU/AUD</option><option value="XAU/CAD">XAU/CAD</option> <option value="XAU/CHF">XAU/CHF</option><option value="XAU/EUR">XAU/EUR</option><option value="XAU/GBP">XAU/GBP</option> <option value="XAU/TRY">XAU/TRY</option><option value="XAU/USD">XAU/USD</option><option value="XPD/USD">XPD/USD</option> <option value="XPT/EUR">XPT/EUR</option><option value="XPT/USD">XPT/USD</option><option value="ZAR/CHF">ZAR/CHF</option> <option value="ZAR/EUR">ZAR/EUR</option><option value="ZAR/JPY">ZAR/JPY</option><option value="ZAR/USD">ZAR/USD</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="customer">Customer</label>
                                    <select id="customer" name="customer" class="form-control multiselect" data-live-search="true">
                                        <!--<option value="ALL">All Customers</option>-->
                                        <%--<option value="000131WFNA">000131WFNA</option><option value="0002GFT">0002GFT</option><option value="001601WFNA">001601WFNA</option><option value="001775WFNA">001775WFNA</option><option value="002USBNK">002USBNK</option><option value="003754WFNA">003754WFNA</option><option value="003USBNK">003USBNK</option><option value="004319WFNA">004319WFNA</option><option value="005USBNK">005USBNK</option><option value="006USBNK">006USBNK</option><option value="007931WFNA">007931WFNA</option><option value="007USBNK">007USBNK</option><option value="008USBNK">008USBNK</option><option value="009USBNK">009USBNK</option><option value="010USBNK">010USBNK</option><option value="011826WFNA">011826WFNA</option><option value="011869WFNA">011869WFNA</option><option value="011USBNK">011USBNK</option><option value="012USBNK">012USBNK</option><option value="013USBNK">013USBNK</option><option value="014USBNK">014USBNK</option><option value="017USBNK">017USBNK</option><option value="018USBNK">018USBNK</option><option value="020USBNK">020USBNK</option><option value="022USBNK">022USBNK</option><option value="023USBNK">023USBNK</option><option value="024USBNK">024USBNK</option><option value="025USBNK">025USBNK</option><option value="026USBNK">026USBNK</option><option value="027USBNK">027USBNK</option><option value="028USBNK">028USBNK</option><option value="029USBNK">029USBNK</option><option value="030USBNK">030USBNK</option><option value="031USBNK">031USBNK</option><option value="032264WFNA">032264WFNA</option><option value="032USBNK">032USBNK</option><option value="033USBNK">033USBNK</option><option value="034USBNK">034USBNK</option><option value="036USBNK">036USBNK</option><option value="037USBNK">037USBNK</option><option value="038USBNK">038USBNK</option><option value="039USBNK">039USBNK</option><option value="041304WFNA">041304WFNA</option><option value="041USBNK">041USBNK</option><option value="042032WFNA">042032WFNA</option><option value="042227WFNA">042227WFNA</option><option value="042454WFNA">042454WFNA</option><option value="042702WFNA">042702WFNA</option><option value="042USBNK">042USBNK</option><option value="043887WFNA">043887WFNA</option><option value="044206WFNA">044206WFNA</option><option value="044644WFNA">044644WFNA</option><option value="044USBNK">044USBNK</option><option value="045022WFNA">045022WFNA</option><option value="045436WFNA">045436WFNA</option><option value="045815WFNA">045815WFNA</option><option value="046316WFNA">046316WFNA</option><option value="046USBNK">046USBNK</option><option value="047USBNK">047USBNK</option><option value="048USBNK">048USBNK</option><option value="049USBNK">049USBNK</option><option value="050868WFNA">050868WFNA</option><option value="051203WFNA">051203WFNA</option><option value="051262WFNA">051262WFNA</option><option value="051502WFNA">051502WFNA</option><option value="051828FXWFNA">051828FXWFNA</option><option value="051828TWFNA">051828TWFNA</option><option value="051828WFNA">051828WFNA</option><option value="051USBNK">051USBNK</option><option value="052070WFNA">052070WFNA</option><option value="052185WFNA">052185WFNA</option><option value="052660FWFNA">052660FWFNA</option><option value="052660WFNA">052660WFNA</option><option value="052775WFNA">052775WFNA</option><option value="052978WFNA">052978WFNA</option><option value="052USBNK">052USBNK</option><option value="053022WFNA">053022WFNA</option><option value="053137WFNA">053137WFNA</option><option value="053USBNK">053USBNK</option><option value="055USBNK">055USBNK</option><option value="058USBNK">058USBNK</option><option value="060USBNK">060USBNK</option><option value="064USBNK">064USBNK</option><option value="065USBNK">065USBNK</option><option value="066USBNK">066USBNK</option><option value="067USBNK">067USBNK</option><option value="068USBNK">068USBNK</option><option value="069USBNK">069USBNK</option><option value="070USBNK">070USBNK</option><option value="071USBNK">071USBNK</option><option value="072USBNK">072USBNK</option><option value="074USBNK">074USBNK</option><option value="077USBNK">077USBNK</option><option value="078USBNK">078USBNK</option><option value="079USBNK">079USBNK</option><option value="080USBNK">080USBNK</option><option value="081USBNK">081USBNK</option><option value="082USBNK">082USBNK</option><option value="083USBNK">083USBNK</option><option value="084USBNK">084USBNK</option><option value="085USBNK">085USBNK</option><option value="086USBNK">086USBNK</option><option value="087USBNK">087USBNK</option><option value="089USBNK">089USBNK</option><option value="090USBNK">090USBNK</option><option value="091USBNK">091USBNK</option><option value="092USBNK">092USBNK</option><option value="094USBNK">094USBNK</option><option value="097USBNK">097USBNK</option><option value="100014WFNA">100014WFNA</option><option value="100058WFNA">100058WFNA</option><option value="1000CXS">1000CXS</option><option value="1000DENIB">1000DENIB</option><option value="1000FCFX">1000FCFX</option><option value="1000USBNK">1000USBNK</option><option value="1000WFNA">1000WFNA</option><option value="100112WFNA">100112WFNA</option><option value="1001CFXM">1001CFXM</option><option value="100288WFNA">100288WFNA</option><option value="1002CFXM">1002CFXM</option><option value="1003CFXM">1003CFXM</option><option value="1003CHAPD">1003CHAPD</option><option value="100669WFNA">100669WFNA</option><option value="100698WFNA">100698WFNA</option><option value="1006CFXM">1006CFXM</option><option value="100747WFNA">100747WFNA</option><option value="100833WFNA">100833WFNA</option><option value="100895WFNA">100895WFNA</option><option value="1008CFXM">1008CFXM</option><option value="100934WFNA">100934WFNA</option><option value="1010CFXM">1010CFXM</option><option value="1011CFXM">1011CFXM</option><option value="101367WFNA">101367WFNA</option><option value="101512WFNA">101512WFNA</option><option value="1015CHAPD">1015CHAPD</option><option value="101697WFNA">101697WFNA</option><option value="1016CHAPD">1016CHAPD</option><option value="101882WFNA">101882WFNA</option><option value="101955WFNA">101955WFNA</option><option value="1020FCFX">1020FCFX</option><option value="102186WFNA">102186WFNA</option><option value="103539WFNA">103539WFNA</option><option value="1040CHAPD">1040CHAPD</option><option value="1040WFNA">1040WFNA</option><option value="104181WFNA">104181WFNA</option><option value="1041CFXM">1041CFXM</option><option value="1041CHAPD">1041CHAPD</option><option value="104363WFNA">104363WFNA</option><option value="1045CHAPD">1045CHAPD</option><option value="1050CFXM">1050CFXM</option><option value="1051CFXM">1051CFXM</option><option value="1052CHAPD">1052CHAPD</option><option value="1053CHAPD">1053CHAPD</option><option value="1054CHAPD">1054CHAPD</option><option value="107819WFNA">107819WFNA</option><option value="107950WFNA">107950WFNA</option><option value="108073WFNA">108073WFNA</option><option value="1080FCFX">1080FCFX</option><option value="108278WFNA">108278WFNA</option><option value="1090CHAPD">1090CHAPD</option><option value="1090FCFX">1090FCFX</option><option value="110997WFNA">110997WFNA</option><option value="111051WFNA">111051WFNA</option><option value="111186WFNA">111186WFNA</option><option value="111209WFNA">111209WFNA</option><option value="112622WFNA">112622WFNA</option><option value="113241WFNA">113241WFNA</option><option value="113729WFNA">113729WFNA</option><option value="115562WFNA">115562WFNA</option><option value="115611WFNA">115611WFNA</option><option value="115723WFNA">115723WFNA</option><option value="116175WFNA">116175WFNA</option><option value="118354WFNA">118354WFNA</option><option value="11FRS">11FRS</option><option value="121769WFNA">121769WFNA</option><option value="121878WFNA">121878WFNA</option><option value="122036WFNA">122036WFNA</option><option value="122057WFNA">122057WFNA</option><option value="122703WFNA">122703WFNA</option><option value="123024WFNA">123024WFNA</option><option value="124138WFNA">124138WFNA</option><option value="124725WFNA">124725WFNA</option><option value="125424WFNA">125424WFNA</option><option value="125485WFNA">125485WFNA</option><option value="126219WFNA">126219WFNA</option><option value="126501WFNA">126501WFNA</option><option value="127302WFNA">127302WFNA</option><option value="128468WFNA">128468WFNA</option><option value="131155WFNA">131155WFNA</option><option value="132273WFNA">132273WFNA</option><option value="134833WFNA">134833WFNA</option><option value="135158WFNA">135158WFNA</option><option value="136171WFNA">136171WFNA</option><option value="138976WFNA">138976WFNA</option><option value="139582WFNA">139582WFNA</option><option value="1400LCG">1400LCG</option><option value="140324WFNA">140324WFNA</option><option value="141093WFNA">141093WFNA</option><option value="14337FINFX">14337FINFX</option><option value="150042WFNA">150042WFNA</option><option value="150409WFNA">150409WFNA</option><option value="150474WFNA">150474WFNA</option><option value="15762FINFX">15762FINFX</option><option value="158402WFNA">158402WFNA</option><option value="16248FINFX">16248FINFX</option><option value="163018WFNA">163018WFNA</option><option value="163202WFNA">163202WFNA</option><option value="163613WFNA">163613WFNA</option><option value="164231WFNA">164231WFNA</option><option value="164334WFNA">164334WFNA</option><option value="164365WFNA">164365WFNA</option><option value="164788WFNA">164788WFNA</option><option value="166269WFNA">166269WFNA</option><option value="166468WFNA">166468WFNA</option><option value="167945WFNA">167945WFNA</option><option value="171117WFNA">171117WFNA</option><option value="174720WFNA">174720WFNA</option><option value="175054WFNA">175054WFNA</option><option value="175229WFNA">175229WFNA</option><option value="175235WFNA">175235WFNA</option><option value="175301WFNA">175301WFNA</option><option value="175304WFNA">175304WFNA</option><option value="175306WFNA">175306WFNA</option><option value="175359WFNA">175359WFNA</option><option value="175370WFNA">175370WFNA</option><option value="175371WFNA">175371WFNA</option><option value="175422WFNA">175422WFNA</option><option value="175429WFNA">175429WFNA</option><option value="175447WFNA">175447WFNA</option><option value="175490WFNA">175490WFNA</option><option value="175493WFNA">175493WFNA</option><option value="175533WFNA">175533WFNA</option><option value="175540WFNA">175540WFNA</option><option value="175656WFNA">175656WFNA</option><option value="175925WFNA">175925WFNA</option><option value="178713WFNA">178713WFNA</option><option value="179043WFNA">179043WFNA</option><option value="179145WFNA">179145WFNA</option><option value="181956WFNA">181956WFNA</option><option value="186347WFNA">186347WFNA</option><option value="186951WFNA">186951WFNA</option><option value="186985WFNA">186985WFNA</option><option value="187095WFNA">187095WFNA</option><option value="187660WFNA">187660WFNA</option><option value="191865WFNA">191865WFNA</option><option value="199497WFNA">199497WFNA</option><option value="200173WFNA">200173WFNA</option><option value="200245WFNA">200245WFNA</option><option value="200388WFNA">200388WFNA</option><option value="200477WFNA">200477WFNA</option><option value="200523WFNA">200523WFNA</option><option value="200542WFNA">200542WFNA</option><option value="200550WFNA">200550WFNA</option><option value="200725WFNA">200725WFNA</option><option value="201604WFNA">201604WFNA</option><option value="2020CHAPD">2020CHAPD</option><option value="202286WFNA">202286WFNA</option><option value="2030CHAPD">2030CHAPD</option><option value="204">204</option><option value="2040CHAPD">2040CHAPD</option><option value="204939WFNA">204939WFNA</option><option value="205986WFNA">205986WFNA</option><option value="208469WFNA">208469WFNA</option><option value="210">210</option><option value="2110WFNA">2110WFNA</option><option value="216387WFNA">216387WFNA</option><option value="218881WFNA">218881WFNA</option><option value="220151WFNA">220151WFNA</option><option value="2222WFNA">2222WFNA</option><option value="222785WFNA">222785WFNA</option><option value="225">225</option><option value="225625WF">225625WF</option><option value="225842WFNA">225842WFNA</option><option value="226667WFNA">226667WFNA</option><option value="227128WFNA">227128WFNA</option><option value="227148WFNA">227148WFNA</option><option value="227179WFNA">227179WFNA</option><option value="227203WFNA">227203WFNA</option><option value="227235WFNA">227235WFNA</option><option value="227318WFNA">227318WFNA</option><option value="227328WFNA">227328WFNA</option><option value="227349WFNA">227349WFNA</option><option value="227377WFNA">227377WFNA</option><option value="227416WFNA">227416WFNA</option><option value="228164WFNA">228164WFNA</option><option value="229">229</option><option value="231347WFNA">231347WFNA</option><option value="231570WFNA">231570WFNA</option><option value="231949WFNA">231949WFNA</option><option value="232">232</option><option value="232258WFNA">232258WFNA</option><option value="232384WFNA">232384WFNA</option><option value="232795WFNA">232795WFNA</option><option value="233061WFNA">233061WFNA</option><option value="233290WFNA">233290WFNA</option><option value="234444WFNA">234444WFNA</option><option value="234d">234d</option><option value="235335WFNA">235335WFNA</option><option value="236">236</option><option value="237817WFNA">237817WFNA</option><option value="237825WFNA">237825WFNA</option><option value="241">241</option><option value="242">242</option><option value="246002WFNA">246002WFNA</option><option value="246554WFNA">246554WFNA</option><option value="246693WFNA">246693WFNA</option><option value="246733WFNA">246733WFNA</option><option value="246734WFNA">246734WFNA</option><option value="246766WFNA">246766WFNA</option><option value="246773WFNA">246773WFNA</option><option value="246858WFNA">246858WFNA</option><option value="248099WFNA">248099WFNA</option><option value="248100WFNA">248100WFNA</option><option value="248357WFNA">248357WFNA</option><option value="248699WFNA">248699WFNA</option><option value="248957WFNA">248957WFNA</option><option value="248987WFNA">248987WFNA</option><option value="249024WFNA">249024WFNA</option><option value="249028WFNA">249028WFNA</option><option value="249032WFNA">249032WFNA</option><option value="249067WFNA">249067WFNA</option><option value="249091WFNA">249091WFNA</option><option value="249123WFNA">249123WFNA</option><option value="249185WFNA">249185WFNA</option><option value="249197WFNA">249197WFNA</option><option value="249213WFNA">249213WFNA</option><option value="249234WFNA">249234WFNA</option><option value="249235WFNA">249235WFNA</option><option value="249247WFNA">249247WFNA</option><option value="249293WFNA">249293WFNA</option><option value="249297WFNA">249297WFNA</option><option value="249313WFNA">249313WFNA</option><option value="249367WFNA">249367WFNA</option><option value="249371WFNA">249371WFNA</option><option value="249463WFNA">249463WFNA</option><option value="249471WFNA">249471WFNA</option><option value="249477WFNA">249477WFNA</option><option value="249505FWFNA">249505FWFNA</option><option value="249505WFNA">249505WFNA</option><option value="249515WFNA">249515WFNA</option><option value="249517WFNA">249517WFNA</option><option value="249527WFNA">249527WFNA</option><option value="249585WFNA">249585WFNA</option><option value="249600WFNA">249600WFNA</option><option value="249612WFNA">249612WFNA</option><option value="249614WFNA">249614WFNA</option><option value="249616WFNA">249616WFNA</option><option value="249668WFNA">249668WFNA</option><option value="249670WFNA">249670WFNA</option><option value="250157WFNA">250157WFNA</option><option value="250835WFNA">250835WFNA</option><option value="252136WFNA">252136WFNA</option><option value="252245WFNA">252245WFNA</option><option value="252828WFNA">252828WFNA</option><option value="253925WFNA">253925WFNA</option><option value="254508WFNA">254508WFNA</option><option value="255603WFNA">255603WFNA</option><option value="255606WFNA">255606WFNA</option><option value="255722WFNA">255722WFNA</option><option value="256033WFNA">256033WFNA</option><option value="256770WFNA">256770WFNA</option><option value="257195WFNA">257195WFNA</option><option value="257258WFNA">257258WFNA</option><option value="257564WFNA">257564WFNA</option><option value="257738WFNA">257738WFNA</option><option value="257751FXWFNA">257751FXWFNA</option><option value="258123WFNA">258123WFNA</option><option value="258454WFNA">258454WFNA</option><option value="258780WFNA">258780WFNA</option><option value="258837FXWFNA">258837FXWFNA</option><option value="258837WFNA">258837WFNA</option><option value="259414WFNA">259414WFNA</option><option value="259489WFNA">259489WFNA</option><option value="260146WFNA">260146WFNA</option><option value="260475WFNA">260475WFNA</option><option value="260670WFNA">260670WFNA</option><option value="260930FWFNA">260930FWFNA</option><option value="260930FXWFNA">260930FXWFNA</option><option value="260930WFNA">260930WFNA</option><option value="260936WFNA">260936WFNA</option><option value="261044WFNA">261044WFNA</option><option value="261433WFNA">261433WFNA</option><option value="261493WFNA">261493WFNA</option><option value="261660WFNA">261660WFNA</option><option value="262543WFNA">262543WFNA</option><option value="265117WFNA">265117WFNA</option><option value="265528WFNA">265528WFNA</option><option value="266043WFNA">266043WFNA</option><option value="267185WFNA">267185WFNA</option><option value="267332WFNA">267332WFNA</option><option value="268214WFNA">268214WFNA</option><option value="268400WFNA">268400WFNA</option><option value="268734WFNA">268734WFNA</option><option value="269588WFNA">269588WFNA</option><option value="269809WFNA">269809WFNA</option><option value="271166WFNA">271166WFNA</option><option value="271533WFNA">271533WFNA</option><option value="271578WFNA">271578WFNA</option><option value="271885WFNA">271885WFNA</option><option value="272104WFNA">272104WFNA</option><option value="274515WFNA">274515WFNA</option><option value="274972WFNA">274972WFNA</option><option value="275547WFNA">275547WFNA</option><option value="275621WFNA">275621WFNA</option><option value="276498WFNA">276498WFNA</option><option value="277883WFNA">277883WFNA</option><option value="3000GFT">3000GFT</option><option value="3030RCGC">3030RCGC</option><option value="312">312</option><option value="314">314</option><option value="326">326</option><option value="332">332</option><option value="360TBRAZ">360TBRAZ</option><option value="360TNBAD">360TNBAD</option><option value="360TRBIV">360TRBIV</option><option value="402">402</option><option value="4040RCGC">4040RCGC</option><option value="5223">5223</option><option value="540041WFNA">540041WFNA</option><option value="600069WFNA">600069WFNA</option><option value="600070WFNA">600070WFNA</option><option value="600125WFNA">600125WFNA</option><option value="600269WFNA">600269WFNA</option><option value="600280WFNA">600280WFNA</option><option value="600753WFNA">600753WFNA</option><option value="601036WFNA">601036WFNA</option><option value="6126">6126</option><option value="613884WFNA">613884WFNA</option><option value="6335">6335</option><option value="673810WFNA">673810WFNA</option><option value="6755">6755</option><option value="677464WFNA">677464WFNA</option><option value="677602WFNA">677602WFNA</option><option value="680267WFNA">680267WFNA</option><option value="682024WFNA">682024WFNA</option><option value="682244WFNA">682244WFNA</option><option value="683054WFNA">683054WFNA</option><option value="683671WFNA">683671WFNA</option><option value="6847">6847</option><option value="685340WFNA">685340WFNA</option><option value="6932">6932</option><option value="7103">7103</option><option value="790817WFNA">790817WFNA</option><option value="798830WFNA">798830WFNA</option><option value="808329WFNA">808329WFNA</option><option value="808539WFNA">808539WFNA</option><option value="812528WFNA">812528WFNA</option><option value="812674WFNA">812674WFNA</option><option value="814466WFNA">814466WFNA</option><option value="814521WFNA">814521WFNA</option><option value="819807WFNA">819807WFNA</option><option value="820190WFNA">820190WFNA</option><option value="820410WFNA">820410WFNA</option><option value="82BK">82BK</option><option value="8519FINFX">8519FINFX</option><option value="903891WFNA">903891WFNA</option><option value="903912WFNA">903912WFNA</option><option value="904202WFNA">904202WFNA</option><option value="904333WFNA">904333WFNA</option><option value="914435WFNA">914435WFNA</option><option value="914806WFNA">914806WFNA</option><option value="915294WFNA">915294WFNA</option><option value="916086WFNA">916086WFNA</option><option value="925169WFNA">925169WFNA</option><option value="997757WFNA">997757WFNA</option><option value="997763WFNA">997763WFNA</option><option value="997764WFNA">997764WFNA</option><option value="997782FWFNA">997782FWFNA</option><option value="997798WFNA">997798WFNA</option><option value="997802WFNA">997802WFNA</option><option value="A645RCGC">A645RCGC</option><option value="AACB">AACB</option><option value="AACBWL">AACBWL</option><option value="ABC1UBPGE">ABC1UBPGE</option><option value="ABERCFSA">ABERCFSA</option><option value="ABN">ABN</option><option value="ACAP2SUCD">ACAP2SUCD</option><option value="ACAPSUCD">ACAPSUCD</option><option value="ACCUCFSA">ACCUCFSA</option><option value="ACMECFSA">ACMECFSA</option><option value="ACUCFSA">ACUCFSA</option><option value="ADM">ADM</option><option value="ADMD">ADMD</option><option value="ADSS">ADSS</option><option value="ADSSFIXI">ADSSFIXI</option><option value="ADSTAKER">ADSTAKER</option><option value="AEQTFX">AEQTFX</option><option value="AETOS">AETOS</option><option value="AETOSMT4">AETOSMT4</option><option value="AIABFIXI">AIABFIXI</option><option value="AIZXPSBF">AIZXPSBF</option><option value="AJIB">AJIB</option><option value="AKSJTSUCD">AKSJTSUCD</option><option value="AKSSUCD">AKSSUCD</option><option value="ALEFSUCD">ALEFSUCD</option><option value="ALFAB">ALFAB</option><option value="ALIOR">ALIOR</option><option value="ALIORPT">ALIORPT</option><option value="ALPR">ALPR</option><option value="ALPRANZECNNEW">ALPRANZECNNEW</option><option value="ALPRSVECN">ALPRSVECN</option><option value="ALTA">ALTA</option><option value="ALTABASIS">ALTABASIS</option><option value="ALTACREDEX">ALTACREDEX</option><option value="ALTADIGBANK">ALTADIGBANK</option><option value="ALTAEEPAYS">ALTAEEPAYS</option><option value="ALTAKUZN">ALTAKUZN</option><option value="ALTAMFBANK">ALTAMFBANK</option><option value="ALTAPRINT">ALTAPRINT</option><option value="ALTATAURUS">ALTATAURUS</option><option value="AMAC">AMAC</option><option value="AMFX">AMFX</option><option value="APBTPSBF">APBTPSBF</option><option value="APIQTDK">APIQTDK</option><option value="ARAB">ARAB</option><option value="ARMASUCD">ARMASUCD</option><option value="ARSJPSBF">ARSJPSBF</option><option value="ASIAWFNA">ASIAWFNA</option><option value="ASTRPSBF">ASTRPSBF</option><option value="AVAC">AVAC</option><option value="AXICORP">AXICORP</option><option value="AXIHINTL">AXIHINTL</option><option value="AXIINSTO">AXIINSTO</option><option value="AXILMIO">AXILMIO</option><option value="AXIPT">AXIPT</option><option value="B789RCGC">B789RCGC</option><option value="BankHapoalimNY">BankHapoalimNY</option><option value="BankJuliusBaer">BankJuliusBaer</option><option value="BankofNYHK">BankofNYHK</option><option value="BARX">BARX</option><option value="BATFSCOT">BATFSCOT</option><option value="BBGErsteBank">BBGErsteBank</option><option value="BBGPSBF">BBGPSBF</option><option value="BBGSDNBADRFS">BBGSDNBADRFS</option><option value="BBHCo">BBHCo</option><option value="BBRCGC">BBRCGC</option><option value="BBVABancomer">BBVABancomer</option><option value="BBVAHKTraders">BBVAHKTraders</option><option value="BBVAMadrid">BBVAMadrid</option><option value="BBVAMadridHKDesk">BBVAMadridHKDesk</option><option value="BBVANY">BBVANY</option><option value="BBVANYTraders">BBVANYTraders</option><option value="BCCS1RBRB">BCCS1RBRB</option><option value="BCEQTFX">BCEQTFX</option><option value="BCP">BCP</option><option value="BCP000001FI">BCP000001FI</option><option value="BCP000002FI">BCP000002FI</option><option value="BCP000004FI">BCP000004FI</option><option value="BCP000006FI">BCP000006FI</option><option value="BCP000007FI">BCP000007FI</option><option value="BCP000010FI">BCP000010FI</option><option value="BCP000011FI">BCP000011FI</option><option value="BCP000013FI">BCP000013FI</option><option value="BCP000018FI">BCP000018FI</option><option value="BCP000019FI">BCP000019FI</option><option value="BCPFMA">BCPFMA</option><option value="BCPTestClient">BCPTestClient</option><option value="BCUCFSA">BCUCFSA</option><option value="BDBMPSBF">BDBMPSBF</option><option value="BELV">BELV</option><option value="BGAZBLTC">BGAZBLTC</option><option value="BGCPINTL">BGCPINTL</option><option value="BHFBank">BHFBank</option><option value="BHFsales">BHFsales</option><option value="BHPBSCOT">BHPBSCOT</option><option value="BIDDINTL">BIDDINTL</option><option value="BIERB1RBIV">BIERB1RBIV</option><option value="BJBSing">BJBSing</option><option value="BLTKPSBF">BLTKPSBF</option><option value="BMDFRCGC">BMDFRCGC</option><option value="BMOChi">BMOChi</option><option value="BMOL">BMOL</option><option value="BMRCGC">BMRCGC</option><option value="BNOTES1RBIV">BNOTES1RBIV</option><option value="BNPCUSTRCGC">BNPCUSTRCGC</option><option value="BNPP">BNPP</option><option value="BNTE">BNTE</option><option value="BNYMShanghai">BNYMShanghai</option><option value="BOAN">BOAN</option><option value="BOAP">BOAP</option><option value="BOASFORCGC">BOASFORCGC</option><option value="BOATESTRCGC">BOATESTRCGC</option><option value="BOCCO">BOCCO</option><option value="BOCY">BOCY</option><option value="BOINDM">BOINDM</option><option value="BoNYMellonTaipei">BoNYMellonTaipei</option><option value="BPFX">BPFX</option><option value="BPLCFSA">BPLCFSA</option><option value="BPRCGC">BPRCGC</option><option value="BRAZ">BRAZ</option><option value="BRAZFIXI">BRAZFIXI</option><option value="BrazosBSFX">BrazosBSFX</option><option value="BROKERONLINEITDN">BROKERONLINEITDN</option><option value="BRUTHRCGC">BRUTHRCGC</option><option value="BSALES1RBIV">BSALES1RBIV</option><option value="BSDINTL">BSDINTL</option><option value="BSFX">BSFX</option><option value="BSGP">BSGP</option><option value="BSGPMT4">BSGPMT4</option><option value="BTTPBT">BTTPBT</option><option value="BTTPView">BTTPView</option><option value="Bulltick">Bulltick</option><option value="BulltickBRA">BulltickBRA</option><option value="BulltickCorreval">BulltickCorreval</option><option value="BulltickSerfinco">BulltickSerfinco</option><option value="BZWBK">BZWBK</option><option value="C001TKFB">C001TKFB</option><option value="C151">C151</option><option value="C222">C222</option><option value="C249">C249</option><option value="C255">C255</option><option value="C266">C266</option><option value="C266API">C266API</option><option value="C307T">C307T</option><option value="C401">C401</option><option value="C417">C417</option><option value="C425">C425</option><option value="C426">C426</option><option value="C431">C431</option><option value="C467">C467</option><option value="CABJIB">CABJIB</option><option value="CaceisLux">CaceisLux</option><option value="CALRCGC">CALRCGC</option><option value="CAPHU">CAPHU</option><option value="CARCGC">CARCGC</option><option value="CarlosVOADMD">CarlosVOADMD</option><option value="Caxton">Caxton</option><option value="CBAA">CBAA</option><option value="CBAL">CBAL</option><option value="CBAN">CBAN</option><option value="CBASYD">CBASYD</option><option value="CCBAsia">CCBAsia</option><option value="CCFLCFSA">CCFLCFSA</option><option value="CCFXCUST">CCFXCUST</option><option value="CCM">CCM</option><option value="CCMBPO">CCMBPO</option><option value="CCMEFC">CCMEFC</option><option value="CCMIBG">CCMIBG</option><option value="CCMLV">CCMLV</option><option value="CCMLVC">CCMLVC</option><option value="CCMPPI">CCMPPI</option><option value="CCMSFX">CCMSFX</option><option value="CCMT2SUCD">CCMT2SUCD</option><option value="CCMT3SUCD">CCMT3SUCD</option><option value="CCMTSUCD">CCMTSUCD</option><option value="CCU1CFSA">CCU1CFSA</option><option value="CCUCFSA">CCUCFSA</option><option value="CCULCFSA">CCULCFSA</option><option value="CCWL">CCWL</option><option value="CESCSUCD">CESCSUCD</option><option value="Cesk1Erste">Cesk1Erste</option><option value="Cesk2Erste">Cesk2Erste</option><option value="CFMCFSA">CFMCFSA</option><option value="CFSA">CFSA</option><option value="CFXM">CFXM</option><option value="CGPLCFSA">CGPLCFSA</option><option value="CHAPD">CHAPD</option><option value="CHPR">CHPR</option><option value="CHPRAUD">CHPRAUD</option><option value="CHPT">CHPT</option><option value="CITI">CITI</option><option value="CITIP">CITIP</option><option value="CITIPB">CITIPB</option><option value="CITIPView">CITIPView</option><option value="CITYFUNDADM">CITYFUNDADM</option><option value="CLFYCFSA">CLFYCFSA</option><option value="CMCM">CMCM</option><option value="COMMINTL">COMMINTL</option><option value="COOMCFSA">COOMCFSA</option><option value="COSFINFX">COSFINFX</option><option value="COSMOSINTL">COSMOSINTL</option><option value="COSMT4">COSMT4</option><option value="CPTRSCOT">CPTRSCOT</option><option value="CRSU">CRSU</option><option value="CRSUView">CRSUView</option><option value="CRTC">CRTC</option><option value="CSALES1RBIV">CSALES1RBIV</option><option value="CSDCFSA">CSDCFSA</option><option value="CSMRCGC">CSMRCGC</option><option value="CSRMRCGC">CSRMRCGC</option><option value="CSTEST1">CSTEST1</option><option value="CTLXCFSA">CTLXCFSA</option><option value="CUFX">CUFX</option><option value="CustCCUCFSA">CustCCUCFSA</option><option value="CustQTDK">CustQTDK</option><option value="CYPRPSBF">CYPRPSBF</option><option value="DB">DB</option><option value="DB7RCGC">DB7RCGC</option><option value="DBP">DBP</option><option value="DBSB">DBSB</option><option value="DBView">DBView</option><option value="DCMVOSAUDAXI">DCMVOSAUDAXI</option><option value="DEMO2WFNA">DEMO2WFNA</option><option value="DEMOCFSA">DEMOCFSA</option><option value="DENIB">DENIB</option><option value="DENIBCT1">DENIBCT1</option><option value="DENIBCT10">DENIBCT10</option><option value="DENIBCT11">DENIBCT11</option><option value="DENIBCT14">DENIBCT14</option><option value="DENIBCT15">DENIBCT15</option><option value="DENIBCT17">DENIBCT17</option><option value="DENIBCT18">DENIBCT18</option><option value="DENIBCT19">DENIBCT19</option><option value="DENIBCT2">DENIBCT2</option><option value="DENIBCT20">DENIBCT20</option><option value="DENIBCT22">DENIBCT22</option><option value="DENIBCT23">DENIBCT23</option><option value="DENIBCT24">DENIBCT24</option><option value="DENIBCT25">DENIBCT25</option><option value="DENIBCT26">DENIBCT26</option><option value="DENIBCT27">DENIBCT27</option><option value="DENIBCT29">DENIBCT29</option><option value="DENIBCT3">DENIBCT3</option><option value="DENIBCT30">DENIBCT30</option><option value="DENIBCT32">DENIBCT32</option><option value="DENIBCT33">DENIBCT33</option><option value="DENIBCT34">DENIBCT34</option><option value="DENIBCT3400">DENIBCT3400</option><option value="DENIBCT35">DENIBCT35</option><option value="DENIBCT36">DENIBCT36</option><option value="DENIBCT37">DENIBCT37</option><option value="DENIBCT38">DENIBCT38</option><option value="DENIBCT39">DENIBCT39</option><option value="DENIBCT4">DENIBCT4</option><option value="DENIBCT40">DENIBCT40</option><option value="DENIBCT41">DENIBCT41</option><option value="DENIBCT42">DENIBCT42</option><option value="DENIBCT43">DENIBCT43</option><option value="DENIBCT44">DENIBCT44</option><option value="DENIBCT45">DENIBCT45</option><option value="DENIBCT46">DENIBCT46</option><option value="DENIBCT47">DENIBCT47</option><option value="DENIBCT48">DENIBCT48</option><option value="DENIBCT49">DENIBCT49</option><option value="DENIBCT5">DENIBCT5</option><option value="DENIBCT50">DENIBCT50</option><option value="DENIBCT51">DENIBCT51</option><option value="DENIBCT52">DENIBCT52</option><option value="DENIBCT53">DENIBCT53</option><option value="DENIBCT54">DENIBCT54</option><option value="DENIBCT6">DENIBCT6</option><option value="DENIBCT8">DENIBCT8</option><option value="DENIBCT9">DENIBCT9</option><option value="DENIBCTCT">DENIBCTCT</option><option value="DENIBCTS">DENIBCTS</option><option value="DENIY">DENIY</option><option value="DENIYMT4">DENIYMT4</option><option value="DESKFIXI">DESKFIXI</option><option value="DEWANINTL">DEWANINTL</option><option value="DIBRE">DIBRE</option><option value="DIBREMT4">DIBREMT4</option><option value="DIGICFSA">DIGICFSA</option><option value="DONGINTL">DONGINTL</option><option value="DOVCSCOT">DOVCSCOT</option><option value="DRCRCGC">DRCRCGC</option><option value="DSTK">DSTK</option><option value="DSTKAPI">DSTKAPI</option><option value="DTLRCGC">DTLRCGC</option><option value="DTSERCGC">DTSERCGC</option><option value="DTSRCGC">DTSRCGC</option><option value="DZBank">DZBank</option><option value="DZSing">DZSing</option><option value="EAGLERCGC">EAGLERCGC</option><option value="EAMLCFSA">EAMLCFSA</option><option value="EASY">EASY</option><option value="ECM1WFNA">ECM1WFNA</option><option value="ECNTFIXI">ECNTFIXI</option><option value="ECNTPBFIXI">ECNTPBFIXI</option><option value="ECP101RJO">ECP101RJO</option><option value="ECP106RJO">ECP106RJO</option><option value="ECP107RJO">ECP107RJO</option><option value="ECP201RJO">ECP201RJO</option><option value="ECP202RJO">ECP202RJO</option><option value="ECP204RJO">ECP204RJO</option><option value="ECP205RJO">ECP205RJO</option><option value="ECP206RJO">ECP206RJO</option><option value="EDFM">EDFM</option><option value="EDGE">EDGE</option><option value="EDMAR">EDMAR</option><option value="EE100800RJO">EE100800RJO</option><option value="EFRCGC">EFRCGC</option><option value="EGFX">EGFX</option><option value="EGFXF4PRO">EGFXF4PRO</option><option value="EGFXMT4">EGFXMT4</option><option value="EGTFSUCD">EGTFSUCD</option><option value="EHGHErste">EHGHErste</option><option value="EMKINGRCGC">EMKINGRCGC</option><option value="EN100700RJO">EN100700RJO</option><option value="EN100800RJO">EN100800RJO</option><option value="ENLACE">ENLACE</option><option value="ErsteBank">ErsteBank</option><option value="ETOSUCD">ETOSUCD</option><option value="ETXINTL">ETXINTL</option><option value="EURP">EURP</option><option value="FAIRSUCD">FAIRSUCD</option><option value="FAP01ADMD">FAP01ADMD</option><option value="FAP03ADMD">FAP03ADMD</option><option value="FAP08ADMD">FAP08ADMD</option><option value="FAP11ADMD">FAP11ADMD</option><option value="FAP13ADMD">FAP13ADMD</option><option value="FAP14ADMD">FAP14ADMD</option><option value="FAP15ADMD">FAP15ADMD</option><option value="FAP17ADMD">FAP17ADMD</option><option value="FAP19ADMD">FAP19ADMD</option><option value="FAP20ADMD">FAP20ADMD</option><option value="FAP21ADMD">FAP21ADMD</option><option value="FAP23ADMD">FAP23ADMD</option><option value="FAP32ADMD">FAP32ADMD</option><option value="FAPBS1ADMD">FAPBS1ADMD</option><option value="FAROS">FAROS</option><option value="FAX02ADMD">FAX02ADMD</option><option value="FCDM272">FCDM272</option><option value="FCDM506">FCDM506</option><option value="FCDM518">FCDM518</option><option value="FCDM520A">FCDM520A</option><option value="FCDM520B">FCDM520B</option><option value="FCDM520NBA">FCDM520NBA</option><option value="FCDM528">FCDM528</option><option value="FCDMCC520COD">FCDMCC520COD</option><option value="FCDMCC520DM">FCDMCC520DM</option><option value="FCDMCC520JLA">FCDMCC520JLA</option><option value="FCDMCC520JR">FCDMCC520JR</option><option value="FCEX">FCEX</option><option value="FCFM120">FCFM120</option><option value="FCFM120C">FCFM120C</option><option value="FCFM136">FCFM136</option><option value="FCFX">FCFX</option><option value="FCFX262">FCFX262</option><option value="FCFX310">FCFX310</option><option value="FCFX354">FCFX354</option><option value="FCFX428">FCFX428</option><option value="FCFX442A">FCFX442A</option><option value="FCFX468">FCFX468</option><option value="FCFX478A">FCFX478A</option><option value="FCFX484F">FCFX484F</option><option value="FCFX497">FCFX497</option><option value="FCFX523">FCFX523</option><option value="FCFX529">FCFX529</option><option value="FCFX531">FCFX531</option><option value="FCFX535">FCFX535</option><option value="FCFX561">FCFX561</option><option value="FCFXHTGBARBARY">FCFXHTGBARBARY</option><option value="FCGX">FCGX</option><option value="FCSCCFSA">FCSCCFSA</option><option value="FECQTFX">FECQTFX</option><option value="FIAB">FIAB</option><option value="FIBBSUCD">FIBBSUCD</option><option value="FIBCSUCD">FIBCSUCD</option><option value="FIBCTSUCD">FIBCTSUCD</option><option value="FIBI">FIBI</option><option value="FIDX">FIDX</option><option value="FifthThird">FifthThird</option><option value="FINFX">FINFX</option><option value="FINRCGC">FINRCGC</option><option value="FINSSCOT">FINSSCOT</option><option value="FINUPSBF">FINUPSBF</option><option value="FITRCGC">FITRCGC</option><option value="FIXI">FIXI</option><option value="FLAMINGORCGC">FLAMINGORCGC</option><option value="FLOW">FLOW</option><option value="FLOWAACB">FLOWAACB</option><option value="FLUCSCOT">FLUCSCOT</option><option value="FMDB001NBAD">FMDB001NBAD</option><option value="FMDB004NBAD">FMDB004NBAD</option><option value="FMDB013NBAD">FMDB013NBAD</option><option value="FMDB014NBAD">FMDB014NBAD</option><option value="FMDB017NBAD">FMDB017NBAD</option><option value="FMDB018NBAD">FMDB018NBAD</option><option value="FMDB020NBAD">FMDB020NBAD</option><option value="FMDB024NBAD">FMDB024NBAD</option><option value="FMDB026NBAD">FMDB026NBAD</option><option value="FMDB030NBAD">FMDB030NBAD</option><option value="FMDB033NBAD">FMDB033NBAD</option><option value="FMDB040NBAD">FMDB040NBAD</option><option value="FMDB041NBAD">FMDB041NBAD</option><option value="FMDB042NBAD">FMDB042NBAD</option><option value="FMDB043NBAD">FMDB043NBAD</option><option value="FMDB044NBAD">FMDB044NBAD</option><option value="FMDB045NBAD">FMDB045NBAD</option><option value="FMDB046NBAD">FMDB046NBAD</option><option value="FMDB050NBAD">FMDB050NBAD</option><option value="FMDB052NBAD">FMDB052NBAD</option><option value="FMDB053NBAD">FMDB053NBAD</option><option value="FMDB054NBAD">FMDB054NBAD</option><option value="FMDB055NBAD">FMDB055NBAD</option><option value="FMDB057NBAD">FMDB057NBAD</option><option value="FMDB060NBAD">FMDB060NBAD</option><option value="FMDC001NBAD">FMDC001NBAD</option><option value="FMDC013NBAD">FMDC013NBAD</option><option value="FMDC015NBAD">FMDC015NBAD</option><option value="FMDC016NBAD">FMDC016NBAD</option><option value="FMDC022NBAD">FMDC022NBAD</option><option value="FMDC023NBAD">FMDC023NBAD</option><option value="FMDC075NBAD">FMDC075NBAD</option><option value="FMDC077NBAD">FMDC077NBAD</option><option value="FMDF005NBAD">FMDF005NBAD</option><option value="FMDF008NBAD">FMDF008NBAD</option><option value="FMDF008XNBAD">FMDF008XNBAD</option><option value="FMDF009NBAD">FMDF009NBAD</option><option value="FMDF016XNBAD">FMDF016XNBAD</option><option value="FMDF018NBAD">FMDF018NBAD</option><option value="FMDF019NBAD">FMDF019NBAD</option><option value="FMDF022NBAD">FMDF022NBAD</option><option value="FMDF024NBAD">FMDF024NBAD</option><option value="FNARSUCD">FNARSUCD</option><option value="FORDSCOT">FORDSCOT</option><option value="FortisBRU">FortisBRU</option><option value="FRKTBRAZ">FRKTBRAZ</option><option value="FROGINTL">FROGINTL</option><option value="FSRCGC">FSRCGC</option><option value="FTILPSBF">FTILPSBF</option><option value="FUJITSU">FUJITSU</option><option value="FX03910ADMD">FX03910ADMD</option><option value="FX0392ADMD">FX0392ADMD</option><option value="FX0394ADMD">FX0394ADMD</option><option value="FX0395ADMD">FX0395ADMD</option><option value="FX0396ADMD">FX0396ADMD</option><option value="FX0397ADMD">FX0397ADMD</option><option value="FX0398ADMD">FX0398ADMD</option><option value="FX0399ADMD">FX0399ADMD</option><option value="FX0401ADMD">FX0401ADMD</option><option value="FX0402ADMD">FX0402ADMD</option><option value="FX0403ADMD">FX0403ADMD</option><option value="FX0405ADMD">FX0405ADMD</option><option value="FX042ADMD">FX042ADMD</option><option value="FX0972ADMD">FX0972ADMD</option><option value="FX097ADMD">FX097ADMD</option><option value="FX165AADMD">FX165AADMD</option><option value="FX165ADMD">FX165ADMD</option><option value="FX165ADMDBX">FX165ADMDBX</option><option value="FX204ADMD">FX204ADMD</option><option value="FX263ADMD">FX263ADMD</option><option value="FX264ADMD">FX264ADMD</option><option value="FX266ADMD">FX266ADMD</option><option value="FX400ADMD">FX400ADMD</option><option value="FXCH">FXCH</option><option value="FXCHSUCD">FXCHSUCD</option><option value="FXCMPRO">FXCMPRO</option><option value="FXCMPRO6033">FXCMPRO6033</option><option value="FXCMPRO636">FXCMPRO636</option><option value="FXCMPRO7000">FXCMPRO7000</option><option value="FXCMPRO7001">FXCMPRO7001</option><option value="FXCMPRO7005">FXCMPRO7005</option><option value="FXCMPRO7006">FXCMPRO7006</option><option value="FXCMPRO7017">FXCMPRO7017</option><option value="FXCMPRO7021">FXCMPRO7021</option><option value="FXCMPRO7030">FXCMPRO7030</option><option value="FXCMPRO7044">FXCMPRO7044</option><option value="FXCMPRO7084">FXCMPRO7084</option><option value="FXCMPRO7090">FXCMPRO7090</option><option value="FXCMPRO7102">FXCMPRO7102</option><option value="FXCMPRO9201">FXCMPRO9201</option><option value="FXCMPRO9202">FXCMPRO9202</option><option value="FXCMPRO9282">FXCMPRO9282</option><option value="FXCMPRO9293">FXCMPRO9293</option><option value="FXCMPROVIEW17">FXCMPROVIEW17</option><option value="FXDESK1RBIV">FXDESK1RBIV</option><option value="FXOPINTL">FXOPINTL</option><option value="FXPRO">FXPRO</option><option value="FXPROcTRADER">FXPROcTRADER</option><option value="FXPROcTRADERUK">FXPROcTRADERUK</option><option value="FXPROMT4">FXPROMT4</option><option value="FXS1QTFX">FXS1QTFX</option><option value="FXS2QTFX">FXS2QTFX</option><option value="FXS5QTFX">FXS5QTFX</option><option value="FxTIMEINTL">FxTIMEINTL</option><option value="FXTM">FXTM</option><option value="FXTMAMANAH">FXTMAMANAH</option><option value="GACFSA">GACFSA</option><option value="GAIDFINFX">GAIDFINFX</option><option value="GAIDMT4">GAIDMT4</option><option value="GAMSUCD">GAMSUCD</option><option value="GAVRCGC">GAVRCGC</option><option value="GCCNBAD">GCCNBAD</option><option value="GCORSCOT">GCORSCOT</option><option value="GCSQTFX">GCSQTFX</option><option value="GDM">GDM</option><option value="GDMMT4">GDMMT4</option><option value="GESA">GESA</option><option value="GESAEAJ">GESAEAJ</option><option value="GESAMT4">GESAMT4</option><option value="GESATEST1">GESATEST1</option><option value="GFT">GFT</option><option value="GGPL">GGPL</option><option value="GGPL2MT4">GGPL2MT4</option><option value="GGPLMT4">GGPLMT4</option><option value="GHCCXS">GHCCXS</option><option value="GHCSCFSA">GHCSCFSA</option><option value="GIYLFXFIXI">GIYLFXFIXI</option><option value="GKALFIXI">GKALFIXI</option><option value="GLBRCGC">GLBRCGC</option><option value="GLDWAY">GLDWAY</option><option value="GLDWAYMT4">GLDWAYMT4</option><option value="GlobalCustodyADM">GlobalCustodyADM</option><option value="GlobeOpBSFX">GlobeOpBSFX</option><option value="GLORYSKY">GLORYSKY</option><option value="GLORYSKYWL">GLORYSKYWL</option><option value="GMTL">GMTL</option><option value="GNRCGC">GNRCGC</option><option value="GOLDPSBF">GOLDPSBF</option><option value="GPFX">GPFX</option><option value="GPFXMT4">GPFXMT4</option><option value="GRADPSBF">GRADPSBF</option><option value="GRINTL">GRINTL</option><option value="GRPL">GRPL</option><option value="GRTY">GRTY</option><option value="GrupoSantander">GrupoSantander</option><option value="GSFX">GSFX</option><option value="GTQTFX">GTQTFX</option><option value="GUNM">GUNM</option><option value="GVSCFSA">GVSCFSA</option><option value="HALK">HALK</option><option value="HALKMT4">HALKMT4</option><option value="HALYK">HALYK</option><option value="HBCP">HBCP</option><option value="HERTSCOT">HERTSCOT</option><option value="HHRCGC">HHRCGC</option><option value="HK1SUCD">HK1SUCD</option><option value="HLBRCGC">HLBRCGC</option><option value="HLRCGC">HLRCGC</option><option value="HNLY587">HNLY587</option><option value="HNLY587A">HNLY587A</option><option value="HNLY589">HNLY589</option><option value="HNLY590">HNLY590</option><option value="HNLY593">HNLY593</option><option value="HPCM">HPCM</option><option value="HPFX">HPFX</option><option value="HSBC">HSBC</option><option value="HSBCTOR">HSBCTOR</option><option value="HSFX">HSFX</option><option value="HTECSUCD">HTECSUCD</option><option value="HTFXSUCD">HTFXSUCD</option><option value="IASG">IASG</option><option value="IBCRCGC">IBCRCGC</option><option value="IBGINTL">IBGINTL</option><option value="IBGRCGC">IBGRCGC</option><option value="ICAPINTL">ICAPINTL</option><option value="ICAPODLS">ICAPODLS</option><option value="ICCBSCOT">ICCBSCOT</option><option value="ICM">ICM</option><option value="ICM2RCGC">ICM2RCGC</option><option value="ICM3RCGC">ICM3RCGC</option><option value="ICM3SUCD">ICM3SUCD</option><option value="ICMCTSUCD">ICMCTSUCD</option><option value="ICMCUST11">ICMCUST11</option><option value="ICMCUST12">ICMCUST12</option><option value="ICMRCGC">ICMRCGC</option><option value="ICMSUCD">ICMSUCD</option><option value="ICMTSUCD">ICMTSUCD</option><option value="ICUCFSA">ICUCFSA</option><option value="IDBT">IDBT</option><option value="IGBI">IGBI</option><option value="IGBI309">IGBI309</option><option value="ILQ">ILQ</option><option value="ILQPURE100">ILQPURE100</option><option value="ILQPURE1000">ILQPURE1000</option><option value="ILQPURE1100">ILQPURE1100</option><option value="ILQPURE1200">ILQPURE1200</option><option value="ILQPURE500">ILQPURE500</option><option value="ILQPURE600">ILQPURE600</option><option value="ILQPURE700">ILQPURE700</option><option value="ILQPUREVO">ILQPUREVO</option><option value="ILQPUREVO1">ILQPUREVO1</option><option value="ILQRISK">ILQRISK</option><option value="INDVAFMT4">INDVAFMT4</option><option value="INEKPSBF">INEKPSBF</option><option value="INFX">INFX</option><option value="INSTOFXCITIAXI">INSTOFXCITIAXI</option><option value="INSTOFXIAXI">INSTOFXIAXI</option><option value="INSTOGIVEAXI">INSTOGIVEAXI</option><option value="INTGBLTC">INTGBLTC</option><option value="INTGPSBF">INTGPSBF</option><option value="INTL">INTL</option><option value="INTLHNLY">INTLHNLY</option><option value="INTSUCD">INTSUCD</option><option value="INV360TBRAZ">INV360TBRAZ</option><option value="INVAST">INVAST</option><option value="IPBMPSBF">IPBMPSBF</option><option value="IRISHBG">IRISHBG</option><option value="IRISHFIXI">IRISHFIXI</option><option value="IRJO">IRJO</option><option value="IRONSUCD">IRONSUCD</option><option value="ISFCFSA">ISFCFSA</option><option value="ISINMT4">ISINMT4</option><option value="ISINPMT4ISYAT">ISINPMT4ISYAT</option><option value="ISWLCFSA">ISWLCFSA</option><option value="ISYAT">ISYAT</option><option value="ISYATB">ISYATB</option><option value="ISYATD">ISYATD</option><option value="ISYATMT4">ISYATMT4</option><option value="ITBKINTL">ITBKINTL</option><option value="ITBKSUCD">ITBKSUCD</option><option value="ITDN">ITDN</option><option value="ITFX">ITFX</option><option value="ITRCGC">ITRCGC</option><option value="JASONFIDX">JASONFIDX</option><option value="JBINTL">JBINTL</option><option value="jcbank">jcbank</option><option value="JCBRCGC">JCBRCGC</option><option value="JCRCGC">JCRCGC</option><option value="JDRCGC">JDRCGC</option><option value="JFX">JFX</option><option value="JFXMT4">JFXMT4</option><option value="JFXMT4A">JFXMT4A</option><option value="JIB">JIB</option><option value="JIT">JIT</option><option value="JLRCGC">JLRCGC</option><option value="JLSRCGC">JLSRCGC</option><option value="JPM">JPM</option><option value="JPMPB">JPMPB</option><option value="JSMRCGC">JSMRCGC</option><option value="JTIPSUCD">JTIPSUCD</option><option value="JUBEFINFX">JUBEFINFX</option><option value="JWPAODLS">JWPAODLS</option><option value="KABSUCD">KABSUCD</option><option value="KBCBrussels">KBCBrussels</option><option value="KBFX">KBFX</option><option value="Keybank">Keybank</option><option value="KOARPSBF">KOARPSBF</option><option value="KOSBPSBF">KOSBPSBF</option><option value="KRANPSBF">KRANPSBF</option><option value="KRCGC">KRCGC</option><option value="KTQTFX">KTQTFX</option><option value="LAPBPSBF">LAPBPSBF</option><option value="LCG">LCG</option><option value="LCMSUCD">LCMSUCD</option><option value="LENZErste">LENZErste</option><option value="LEVBPSBF">LEVBPSBF</option><option value="Livedemo1">Livedemo1</option><option value="LIVEVIEW">LIVEVIEW</option><option value="LIVEVIEWPICT">LIVEVIEWPICT</option><option value="LMELINTL">LMELINTL</option><option value="LMRCGC">LMRCGC</option><option value="LOKOPSBF">LOKOPSBF</option><option value="LRFCFSA">LRFCFSA</option><option value="MAMQTFX">MAMQTFX</option><option value="MAPLEINTL">MAPLEINTL</option><option value="Market77">Market77</option><option value="MASFXI">MASFXI</option><option value="MAYFCFSA">MAYFCFSA</option><option value="MBCUCFSA">MBCUCFSA</option><option value="MBHINTL">MBHINTL</option><option value="MCBBKK">MCBBKK</option><option value="MCBBMI">MCBBMI</option><option value="MCBCA">MCBCA</option><option value="MCBDD">MCBDD</option><option value="MCBHK">MCBHK</option><option value="MCBHN">MCBHN</option><option value="MCBL">MCBL</option><option value="MCBL0001">MCBL0001</option><option value="MCBL0003">MCBL0003</option><option value="MCBL0005">MCBL0005</option><option value="MCBL0006">MCBL0006</option><option value="MCBL0007">MCBL0007</option><option value="MCBL0008">MCBL0008</option><option value="MCBL0010">MCBL0010</option><option value="MCBL0011">MCBL0011</option><option value="MCBL0012">MCBL0012</option><option value="MCBL0013">MCBL0013</option><option value="MCBL0014">MCBL0014</option><option value="MCBL0015">MCBL0015</option><option value="MCBL0017">MCBL0017</option><option value="MCBL0018">MCBL0018</option><option value="MCBL0020">MCBL0020</option><option value="MCBL0022">MCBL0022</option><option value="MCBL0023">MCBL0023</option><option value="MCBL0024">MCBL0024</option><option value="MCBL0025">MCBL0025</option><option value="MCBL0027">MCBL0027</option><option value="MCBL0028">MCBL0028</option><option value="MCBL0029">MCBL0029</option><option value="MCBL0031">MCBL0031</option><option value="MCBL0032">MCBL0032</option><option value="MCBL0034">MCBL0034</option><option value="MCBL0035">MCBL0035</option><option value="MCBL0036">MCBL0036</option><option value="MCBL0037">MCBL0037</option><option value="MCBL0038">MCBL0038</option><option value="MCBL0041">MCBL0041</option><option value="MCBL0045">MCBL0045</option><option value="MCBL0046">MCBL0046</option><option value="MCBL0047">MCBL0047</option><option value="MCBL0048">MCBL0048</option><option value="MCBL0049">MCBL0049</option><option value="MCBL0051">MCBL0051</option><option value="MCBL0052">MCBL0052</option><option value="MCBL0053">MCBL0053</option><option value="MCBL0054">MCBL0054</option><option value="MCBL0055">MCBL0055</option><option value="MCBL0056">MCBL0056</option><option value="MCBL0057">MCBL0057</option><option value="MCBL0058">MCBL0058</option><option value="MCBL0059">MCBL0059</option><option value="MCBL0060">MCBL0060</option><option value="MCBL0062">MCBL0062</option><option value="MCBL0063">MCBL0063</option><option value="MCBL0064">MCBL0064</option><option value="MCBL0065">MCBL0065</option><option value="MCBL0067">MCBL0067</option><option value="MCBL0069">MCBL0069</option><option value="MCBL0070">MCBL0070</option><option value="MCBL0071">MCBL0071</option><option value="MCBL0075">MCBL0075</option><option value="MCBL0076">MCBL0076</option><option value="MCBL0077">MCBL0077</option><option value="MCBL0078">MCBL0078</option><option value="MCBL0079">MCBL0079</option><option value="MCBL0080">MCBL0080</option><option value="MCBL0081">MCBL0081</option><option value="MCBL0082">MCBL0082</option><option value="MCBL0083">MCBL0083</option><option value="MCBL0084">MCBL0084</option><option value="MCBL0085">MCBL0085</option><option value="MCBL0086">MCBL0086</option><option value="MCBL0089">MCBL0089</option><option value="MCBL0090">MCBL0090</option><option value="MCBL0091">MCBL0091</option><option value="MCBL0092">MCBL0092</option><option value="MCBL0093">MCBL0093</option><option value="MCBL0095">MCBL0095</option><option value="MCBL0098">MCBL0098</option><option value="MCBL0099">MCBL0099</option><option value="MCBL9999">MCBL9999</option><option value="MCBLA">MCBLA</option><option value="MCBMNL">MCBMNL</option><option value="MCBN">MCBN</option><option value="MCBN0001">MCBN0001</option><option value="MCBN0002">MCBN0002</option><option value="MCBN0009">MCBN0009</option><option value="MCBN0014">MCBN0014</option><option value="MCBN0021">MCBN0021</option><option value="MCBN0024">MCBN0024</option><option value="MCBN0025">MCBN0025</option><option value="MCBN0027">MCBN0027</option><option value="MCBN0030">MCBN0030</option><option value="MCBN0032">MCBN0032</option><option value="MCBN0034">MCBN0034</option><option value="MCBN0035">MCBN0035</option><option value="MCBN0038">MCBN0038</option><option value="MCBN0040">MCBN0040</option><option value="MCBN0042">MCBN0042</option><option value="MCBN0045">MCBN0045</option><option value="MCBN0046">MCBN0046</option><option value="MCBN0049">MCBN0049</option><option value="MCBN0050">MCBN0050</option><option value="MCBN0051">MCBN0051</option><option value="MCBN0053">MCBN0053</option><option value="MCBN0055">MCBN0055</option><option value="MCBN0057">MCBN0057</option><option value="MCBN0058">MCBN0058</option><option value="MCBN0059">MCBN0059</option><option value="MCBN0060">MCBN0060</option><option value="MCBN0062">MCBN0062</option><option value="MCBN0063">MCBN0063</option><option value="MCBN0064">MCBN0064</option><option value="MCBN0065">MCBN0065</option><option value="MCBN0066">MCBN0066</option><option value="MCBN0067">MCBN0067</option><option value="MCBN0068">MCBN0068</option><option value="MCBN0069">MCBN0069</option><option value="MCBN0070">MCBN0070</option><option value="MCBN0072">MCBN0072</option><option value="MCBN0073">MCBN0073</option><option value="MCBN0074">MCBN0074</option><option value="MCBN0075">MCBN0075</option><option value="MCBN0076">MCBN0076</option><option value="MCBN0077">MCBN0077</option><option value="MCBN0079">MCBN0079</option><option value="MCBN0082">MCBN0082</option><option value="MCBN0083">MCBN0083</option><option value="MCBN0085">MCBN0085</option><option value="MCBN0086">MCBN0086</option><option value="MCBN0089">MCBN0089</option><option value="MCBN0090">MCBN0090</option><option value="MCBN0091">MCBN0091</option><option value="MCBN0092">MCBN0092</option><option value="MCBN0094">MCBN0094</option><option value="MCBN0095">MCBN0095</option><option value="MCBN0096">MCBN0096</option><option value="MCBN0097">MCBN0097</option><option value="MCBN0098">MCBN0098</option><option value="MCBN0101">MCBN0101</option><option value="MCBN0102">MCBN0102</option><option value="MCBN0107">MCBN0107</option><option value="MCBN0108">MCBN0108</option><option value="MCBN0110">MCBN0110</option><option value="MCBN0112">MCBN0112</option><option value="MCBN0115">MCBN0115</option><option value="MCBN0116">MCBN0116</option><option value="MCBN0117">MCBN0117</option><option value="MCBN0118">MCBN0118</option><option value="MCBN0122">MCBN0122</option><option value="MCBN0124">MCBN0124</option><option value="MCBN0125">MCBN0125</option><option value="MCBN0126">MCBN0126</option><option value="MCBN0131">MCBN0131</option><option value="MCBN0132">MCBN0132</option><option value="MCBN0133">MCBN0133</option><option value="MCBN0134">MCBN0134</option><option value="MCBN0136">MCBN0136</option><option value="MCBN0137">MCBN0137</option><option value="MCBN0138">MCBN0138</option><option value="MCBN0139">MCBN0139</option><option value="MCBN0140">MCBN0140</option><option value="MCBN0141">MCBN0141</option><option value="MCBN9995">MCBN9995</option><option value="MCBN9999">MCBN9999</option><option value="MCBNL">MCBNL</option><option value="MCBPR">MCBPR</option><option value="MCBSH">MCBSH</option><option value="MCBSL">MCBSL</option><option value="MCBSP">MCBSP</option><option value="MCBSYD">MCBSYD</option><option value="MCBSYD001">MCBSYD001</option><option value="MCBSYD003">MCBSYD003</option><option value="MCBSYD004">MCBSYD004</option><option value="MCBSYD005">MCBSYD005</option><option value="MCBT">MCBT</option><option value="MCBTP">MCBTP</option><option value="MCEUSCOT">MCEUSCOT</option><option value="MCICFSA">MCICFSA</option><option value="MDBI">MDBI</option><option value="MDQTFX">MDQTFX</option><option value="MellonBank">MellonBank</option><option value="MetalsDesk">MetalsDesk</option><option value="MHDP">MHDP</option><option value="MHINT">MHINT</option><option value="MHSC">MHSC</option><option value="MHSCAsia">MHSCAsia</option><option value="MHSW">MHSW</option><option value="MHTB">MHTB</option><option value="MHTBLU">MHTBLU</option><option value="MICBINTL">MICBINTL</option><option value="MIGB">MIGB</option><option value="MIGBCOVER">MIGBCOVER</option><option value="MIGBDEALINGTEST">MIGBDEALINGTEST</option><option value="MIGBFI0003">MIGBFI0003</option><option value="MIGBFI0003MT4">MIGBFI0003MT4</option><option value="MIGBFI0004">MIGBFI0004</option><option value="MIGBFI0004MT4">MIGBFI0004MT4</option><option value="MIGBFI0007">MIGBFI0007</option><option value="MIGBFI0007MT4">MIGBFI0007MT4</option><option value="MIGBFI0012">MIGBFI0012</option><option value="MIGBFI0015">MIGBFI0015</option><option value="MIGBFI0016">MIGBFI0016</option><option value="MIGBFI0017">MIGBFI0017</option><option value="MIGBFI0017MT4">MIGBFI0017MT4</option><option value="MIGBFI0022">MIGBFI0022</option><option value="MIGBFI0022MT4">MIGBFI0022MT4</option><option value="MIGBFI0023">MIGBFI0023</option><option value="MIGBMT4INTERNAL">MIGBMT4INTERNAL</option><option value="MIZI">MIZI</option><option value="MLFX">MLFX</option><option value="MMCBErste">MMCBErste</option><option value="MMPBPSBF">MMPBPSBF</option><option value="MMPIErste">MMPIErste</option><option value="MMQTDK">MMQTDK</option><option value="MNYPFX">MNYPFX</option><option value="MNYPFXAPI">MNYPFXAPI</option><option value="MONOPSBF">MONOPSBF</option><option value="MOOSERCGC">MOOSERCGC</option><option value="MSARCGC">MSARCGC</option><option value="MSCOLRCGC">MSCOLRCGC</option><option value="MSCORCGC">MSCORCGC</option><option value="MSFX">MSFX</option><option value="MSFXP">MSFXP</option><option value="MSFXTest">MSFXTest</option><option value="MST">MST</option><option value="MT4BZWBK">MT4BZWBK</option><option value="MT4MICROFINFX">MT4MICROFINFX</option><option value="MT4MSFINFX">MT4MSFINFX</option><option value="MTBBPSBF">MTBBPSBF</option><option value="MTLDPSBF">MTLDPSBF</option><option value="MTRCGC">MTRCGC</option><option value="MVCUCFSA">MVCUCFSA</option><option value="MYAT1">MYAT1</option><option value="MYAT2">MYAT2</option><option value="MYPFX">MYPFX</option><option value="MYPFX2">MYPFX2</option><option value="MYPFX2MT4">MYPFX2MT4</option><option value="MYPFXMT4">MYPFXMT4</option><option value="NBAD">NBAD</option><option value="NBBNSCOT">NBBNSCOT</option><option value="NBEEINTL">NBEEINTL</option><option value="NGBRCGC">NGBRCGC</option><option value="NJC">NJC</option><option value="NLBDSCOT">NLBDSCOT</option><option value="NNBQ">NNBQ</option><option value="Nordea">Nordea</option><option value="NORDFX">NORDFX</option><option value="NORDFXMT4">NORDFXMT4</option><option value="NORDSUCD">NORDSUCD</option><option value="NORVIK">NORVIK</option><option value="NORVIKINTL">NORVIKINTL</option><option value="NORVIKM">NORVIKM</option><option value="NOVIKOM">NOVIKOM</option><option value="NOVIKOMOPER">NOVIKOMOPER</option><option value="NOVIKOMROSTOV">NOVIKOMROSTOV</option><option value="NOVIKOMSPB">NOVIKOMSPB</option><option value="NOVIKOMTEST">NOVIKOMTEST</option><option value="NOVIKOMTLT">NOVIKOMTLT</option><option value="NSC">NSC</option><option value="NTFX">NTFX</option><option value="NTRS">NTRS</option><option value="OCSDPSBF">OCSDPSBF</option><option value="OFEQTFX">OFEQTFX</option><option value="OFT">OFT</option><option value="OMFSUCD">OMFSUCD</option><option value="OPLCFSA">OPLCFSA</option><option value="OSTNPSBF">OSTNPSBF</option><option value="OTPR">OTPR</option><option value="P266">P266</option><option value="PACEFIDX">PACEFIDX</option><option value="PALGOIGBI">PALGOIGBI</option><option value="PAMSUCD">PAMSUCD</option><option value="PAREX">PAREX</option><option value="PAREXCUST">PAREXCUST</option><option value="PAREXMT4">PAREXMT4</option><option value="PARH">PARH</option><option value="PARXODLS">PARXODLS</option><option value="PB">PB</option><option value="PCBR1RBRB">PCBR1RBRB</option><option value="PCCUCFSA">PCCUCFSA</option><option value="PCFGErste">PCFGErste</option><option value="PDRCGC">PDRCGC</option><option value="PDSCFSA">PDSCFSA</option><option value="PEDDECFSA">PEDDECFSA</option><option value="PFXSUCD">PFXSUCD</option><option value="PHILSCOT">PHILSCOT</option><option value="PICT">PICT</option><option value="PICTBLA">PICTBLA</option><option value="PICTCBR">PICTCBR</option><option value="PINASCOT">PINASCOT</option><option value="PIRB1RBRB">PIRB1RBRB</option><option value="PLIMFIXI">PLIMFIXI</option><option value="PM00573INTLHNLY">PM00573INTLHNLY</option><option value="PM00574INTLHNLY">PM00574INTLHNLY</option><option value="PM00583INTLHNLY">PM00583INTLHNLY</option><option value="PM00586INTLHNLY">PM00586INTLHNLY</option><option value="PMIB0001INTLHNLY">PMIB0001INTLHNLY</option><option value="PNCBank">PNCBank</option><option value="PPSSUCD">PPSSUCD</option><option value="PrimeVCL">PrimeVCL</option><option value="PRMTPSBF">PRMTPSBF</option><option value="PROCPSBF">PROCPSBF</option><option value="PROPNBAD">PROPNBAD</option><option value="PSBDealers">PSBDealers</option><option value="PSBF">PSBF</option><option value="PSBFilials">PSBFilials</option><option value="PSBSales">PSBSales</option><option value="PXCLOSE">PXCLOSE</option><option value="PXIBFO2FIXI">PXIBFO2FIXI</option><option value="PXIBFOBX">PXIBFOBX</option><option value="PXIBFOCS">PXIBFOCS</option><option value="PXIBFOFIXI">PXIBFOFIXI</option><option value="PXIBFONOMU">PXIBFONOMU</option><option value="PXMFixi">PXMFixi</option><option value="PXMTMSFIXI">PXMTMSFIXI</option><option value="QMSC">QMSC</option><option value="QRES">QRES</option><option value="QTFX">QTFX</option><option value="RABIPSBF">RABIPSBF</option><option value="RABO">RABO</option><option value="RAIN2SUCD">RAIN2SUCD</option><option value="RAIN3SUCD">RAIN3SUCD</option><option value="RAINHMSUCD">RAINHMSUCD</option><option value="RAINSUCD">RAINSUCD</option><option value="RBBG1RBIV">RBBG1RBIV</option><option value="RBBH1RBIV">RBBH1RBIV</option><option value="RBCCMLondon">RBCCMLondon</option><option value="RBCZ1RBIV">RBCZ1RBIV</option><option value="RBGRErste">RBGRErste</option><option value="RBHR1RBIV">RBHR1RBIV</option><option value="RBHU1RBIV">RBHU1RBIV</option><option value="RBIV">RBIV</option><option value="RBKO1RBIV">RBKO1RBIV</option><option value="RBPL1RBIV">RBPL1RBIV</option><option value="RBRB">RBRB</option><option value="RBRS1RBIV">RBRS1RBIV</option><option value="RBRU1RBIV">RBRU1RBIV</option><option value="RBRUFIX1RBIV">RBRUFIX1RBIV</option><option value="RBRVZErste">RBRVZErste</option><option value="RBS">RBS</option><option value="RBSG1RBIV">RBSG1RBIV</option><option value="RBSI1RBIV">RBSI1RBIV</option><option value="RBStest">RBStest</option><option value="RCGA">RCGA</option><option value="RCGChicago">RCGChicago</option><option value="RCGFXRCGC">RCGFXRCGC</option><option value="RCKPCFSA">RCKPCFSA</option><option value="RCUI">RCUI</option><option value="RDGTFIXI">RDGTFIXI</option><option value="RESPPSBF">RESPPSBF</option><option value="RETNBAD">RETNBAD</option><option value="RfcVCL">RfcVCL</option><option value="RFSITDN">RFSITDN</option><option value="RGNACFSA">RGNACFSA</option><option value="RGQTFX">RGQTFX</option><option value="RHNGCFSA">RHNGCFSA</option><option value="RIBM">RIBM</option><option value="RICASUCD">RICASUCD</option><option value="RIETCUST">RIETCUST</option><option value="RIETUM">RIETUM</option><option value="RIETUMPT">RIETUMPT</option><option value="RIETUMT4">RIETUMT4</option><option value="RJO">RJO</option><option value="RJOC01RJO">RJOC01RJO</option><option value="RLBAODLS">RLBAODLS</option><option value="RLBK1RBIV">RLBK1RBIV</option><option value="RLBNW1RBIV">RLBNW1RBIV</option><option value="RLBOOE1RBIV">RLBOOE1RBIV</option><option value="RLBT1RBIV">RLBT1RBIV</option><option value="RLBV1RBIV">RLBV1RBIV</option><option value="RLST1RBIV">RLST1RBIV</option><option value="RMBMPSBF">RMBMPSBF</option><option value="RMRCGC">RMRCGC</option><option value="RNFX">RNFX</option><option value="RNRE">RNRE</option><option value="RNSR">RNSR</option><option value="RNSRBANK24">RNSRBANK24</option><option value="RNSRCLIENT24">RNSRCLIENT24</option><option value="RNSRRCFX">RNSRRCFX</option><option value="ROCKRCGC">ROCKRCGC</option><option value="ROSAPSBF">ROSAPSBF</option><option value="ROSCAP">ROSCAP</option><option value="ROSCAPVIP01">ROSCAPVIP01</option><option value="ROST1INTL">ROST1INTL</option><option value="ROSTINTL">ROSTINTL</option><option value="ROSTPSBF">ROSTPSBF</option><option value="RSCUFX">RSCUFX</option><option value="RTRCGC">RTRCGC</option><option value="RVDM">RVDM</option><option value="RVSZ1RBIV">RVSZ1RBIV</option><option value="SAFRABRAZ">SAFRABRAZ</option><option value="SALESFXG">SALESFXG</option><option value="SALESNBAD">SALESNBAD</option><option value="SALEST1NBAD">SALEST1NBAD</option><option value="SANKO">SANKO</option><option value="SANKOMT4">SANKOMT4</option><option value="SANTADMD">SANTADMD</option><option value="SantanderInvestment">SantanderInvestment</option><option value="SBBR">SBBR</option><option value="SBBRDELT">SBBRDELT</option><option value="SBMBPSBF">SBMBPSBF</option><option value="SBNR">SBNR</option><option value="SBNY">SBNY</option><option value="ScotiaTOR">ScotiaTOR</option><option value="SD2INTL">SD2INTL</option><option value="SDMBPSBF">SDMBPSBF</option><option value="SECBRAZ">SECBRAZ</option><option value="SECO1RBIV">SECO1RBIV</option><option value="SEKER">SEKER</option><option value="SEKER10MMT4">SEKER10MMT4</option><option value="SEKER15MMT4">SEKER15MMT4</option><option value="SEKER1MMT4">SEKER1MMT4</option><option value="SEKER30MMT4">SEKER30MMT4</option><option value="SEKER5MMT4">SEKER5MMT4</option><option value="SEKERMT4">SEKERMT4</option><option value="SEKINSTMT4">SEKINSTMT4</option><option value="SENMPSBF">SENMPSBF</option><option value="SFSQTFX">SFSQTFX</option><option value="SG">SG</option><option value="SGENSUCD">SGENSUCD</option><option value="SGIISCOT">SGIISCOT</option><option value="SGMF">SGMF</option><option value="SGNYOrderBook">SGNYOrderBook</option><option value="SGPD">SGPD</option><option value="SGView">SGView</option><option value="SHCUCFSA">SHCUCFSA</option><option value="SHHCFSA">SHHCFSA</option><option value="SICAVBSFX">SICAVBSFX</option><option value="SINDIQTFX">SINDIQTFX</option><option value="SKBAPSBF">SKBAPSBF</option><option value="SKOSUCD">SKOSUCD</option><option value="SMRCGC">SMRCGC</option><option value="SOBINADM">SOBINADM</option><option value="SocGenNYTraderAgency">SocGenNYTraderAgency</option><option value="SocGenNYTradersNight">SocGenNYTradersNight</option><option value="SocGenParis">SocGenParis</option><option value="SocGenParisNY">SocGenParisNY</option><option value="SOLID">SOLID</option><option value="SOLIDFIXI">SOLIDFIXI</option><option value="SONGWS">SONGWS</option><option value="SOVTPSBF">SOVTPSBF</option><option value="SPRCGC">SPRCGC</option><option value="SPRTPSBF">SPRTPSBF</option><option value="SPXUSCOT">SPXUSCOT</option><option value="SQNS">SQNS</option><option value="STBSUCD">STBSUCD</option><option value="STSDESK1RBIV">STSDESK1RBIV</option><option value="SUCD">SUCD</option><option value="SUCDMT">SUCDMT</option><option value="SUDINTL">SUDINTL</option><option value="SUDMPSBF">SUDMPSBF</option><option value="Suntrust">Suntrust</option><option value="SUSPSUCD">SUSPSUCD</option><option value="SVASUCD">SVASUCD</option><option value="SWAGErste">SWAGErste</option><option value="SWED">SWED</option><option value="SX14JADMD">SX14JADMD</option><option value="SXQTFX">SXQTFX</option><option value="TAMI">TAMI</option><option value="TBSK1RBIV">TBSK1RBIV</option><option value="TCBMINTL">TCBMINTL</option><option value="TCUCFSA">TCUCFSA</option><option value="TestFIXI">TestFIXI</option><option value="TESTQTFX">TESTQTFX</option><option value="TESTTKFB">TESTTKFB</option><option value="TFCUFX">TFCUFX</option><option value="TFIINTL">TFIINTL</option><option value="TGFCXS">TGFCXS</option><option value="TGQTFX">TGQTFX</option><option value="THRCGC">THRCGC</option><option value="TKFB">TKFB</option><option value="TLUXINTL">TLUXINTL</option><option value="TOPFX">TOPFX</option><option value="TOPFX113">TOPFX113</option><option value="TOPFX113MT4">TOPFX113MT4</option><option value="TOPFX117">TOPFX117</option><option value="TOPFX122">TOPFX122</option><option value="TOPFX123">TOPFX123</option><option value="TOPFX131">TOPFX131</option><option value="TOPFX132">TOPFX132</option><option value="TOPFX132MT4">TOPFX132MT4</option><option value="TOPFX143">TOPFX143</option><option value="TOPFX146">TOPFX146</option><option value="TOPFX148">TOPFX148</option><option value="TOPFX156">TOPFX156</option><option value="TOPFX159">TOPFX159</option><option value="TOPFX160">TOPFX160</option><option value="TOPFX161">TOPFX161</option><option value="TOPFX162">TOPFX162</option><option value="TOPFX166">TOPFX166</option><option value="TOPFX168">TOPFX168</option><option value="TOPFX174">TOPFX174</option><option value="TOPFX175">TOPFX175</option><option value="TOPFXcTLive1">TOPFXcTLive1</option><option value="TOPFXM">TOPFXM</option><option value="TOPFXTWC">TOPFXTWC</option><option value="TOPFXTWCMT4">TOPFXTWCMT4</option><option value="TPSD">TPSD</option><option value="TPSD005">TPSD005</option><option value="TPSD006">TPSD006</option><option value="TPSD007">TPSD007</option><option value="TPSD008">TPSD008</option><option value="TPSD010">TPSD010</option><option value="TPSD501">TPSD501</option><option value="TPSD901">TPSD901</option><option value="TPSD902">TPSD902</option><option value="TPSD903">TPSD903</option><option value="TPSD904">TPSD904</option><option value="TRCL">TRCL</option><option value="TRLNK">TRLNK</option><option value="TRNLSCOT">TRNLSCOT</option><option value="TRNSPSBF">TRNSPSBF</option><option value="TRNXCust10">TRNXCust10</option><option value="TRNXWL">TRNXWL</option><option value="TRXINTL">TRXINTL</option><option value="TSB">TSB</option><option value="TSBLFIXI">TSBLFIXI</option><option value="TSS">TSS</option><option value="TSTSErste">TSTSErste</option><option value="TULLINTL">TULLINTL</option><option value="TVRSUCD">TVRSUCD</option><option value="TXRCGC">TXRCGC</option><option value="TYGZSUCD">TYGZSUCD</option><option value="UBPGE">UBPGE</option><option value="UBPRMGE">UBPRMGE</option><option value="UBPRMTURKGE">UBPRMTURKGE</option><option value="UBPZH">UBPZH</option><option value="UBS">UBS</option><option value="UE103301RJO">UE103301RJO</option><option value="UedaHarlowLtd">UedaHarlowLtd</option><option value="UMTrading">UMTrading</option><option value="UN100004RJO">UN100004RJO</option><option value="UN100200RJO">UN100200RJO</option><option value="UN100400RJO">UN100400RJO</option><option value="UNTL">UNTL</option><option value="US1LIVES">US1LIVES</option><option value="US2LIVES">US2LIVES</option><option value="US3LIVES">US3LIVES</option><option value="US5LIVES">US5LIVES</option><option value="US6LIVES">US6LIVES</option><option value="USBNK">USBNK</option><option value="UXOFErste">UXOFErste</option><option value="UXOLErste">UXOLErste</option><option value="UXPUErste">UXPUErste</option><option value="UXQFErste">UXQFErste</option><option value="UXQHErste">UXQHErste</option><option value="UXQPErste">UXQPErste</option><option value="UXQYErste">UXQYErste</option><option value="UXQZErste">UXQZErste</option><option value="UXRAErste">UXRAErste</option><option value="UXSJErste">UXSJErste</option><option value="UXSLErste">UXSLErste</option><option value="UXTCErste">UXTCErste</option><option value="UXTJErste">UXTJErste</option><option value="UXUXErste">UXUXErste</option><option value="UXVKErste">UXVKErste</option><option value="UXVOErste">UXVOErste</option><option value="UXWSErste">UXWSErste</option><option value="UXWUErste">UXWUErste</option><option value="UXXTErste">UXXTErste</option><option value="UXZTErste">UXZTErste</option><option value="UYAUErste">UYAUErste</option><option value="UYCQErste">UYCQErste</option><option value="UYDKErste">UYDKErste</option><option value="UYFUErste">UYFUErste</option><option value="UYSFErste">UYSFErste</option><option value="UYZXErste">UYZXErste</option><option value="UZQGErste">UZQGErste</option><option value="VAF">VAF</option><option value="VAKOPSBF">VAKOPSBF</option><option value="VALH">VALH</option><option value="ValhallaBSFX">ValhallaBSFX</option><option value="VBAT360TBRAZ">VBAT360TBRAZ</option><option value="VCL">VCL</option><option value="VCLEJ">VCLEJ</option><option value="VCRCGC">VCRCGC</option><option value="VECMT4">VECMT4</option><option value="VERSOINTL">VERSOINTL</option><option value="VIKINGRCGC">VIKINGRCGC</option><option value="VKLAPSBF">VKLAPSBF</option><option value="VOESErste">VOESErste</option><option value="VPEBank">VPEBank</option><option value="VPM3RCGC">VPM3RCGC</option><option value="VPMRCGC">VPMRCGC</option><option value="VTFX">VTFX</option><option value="VYATPSBF">VYATPSBF</option><option value="WALLINTL">WALLINTL</option><option value="WBCPRCGC">WBCPRCGC</option><option value="WF901853">WF901853</option><option value="WF925679">WF925679</option><option value="WFCO2">WFCO2</option><option value="WFDE">WFDE</option><option value="WFIT">WFIT</option><option value="WFNA">WFNA</option><option value="WFNAD">WFNAD</option><option value="WFNAD7">WFNAD7</option><option value="WFRE">WFRE</option><option value="WFSGErste">WFSGErste</option><option value="WHBRCGC">WHBRCGC</option><option value="WLBH">WLBH</option><option value="WLDemoRBIV">WLDemoRBIV</option><option value="WLDemoRBRB">WLDemoRBRB</option><option value="WMSISCOT">WMSISCOT</option><option value="WST">WST</option><option value="WSTSONGWS1">WSTSONGWS1</option><option value="WSTSONGWS2">WSTSONGWS2</option><option value="WWAYLEVSUCD">WWAYLEVSUCD</option><option value="WWAYSUCD">WWAYSUCD</option><option value="XOOM">XOOM</option><option value="YAPI">YAPI</option><option value="YAPIDESK">YAPIDESK</option><option value="YAPIMT4">YAPIMT4</option><option value="YATFINISYAT">YATFINISYAT</option><option value="YATFINPISYAT">YATFINPISYAT</option><option value="YATFINPPISYAT">YATFINPPISYAT</option><option value="YOTAPSBF">YOTAPSBF</option><option value="YUANTA">YUANTA</option><option value="ZeroISYAT">ZeroISYAT</option><option value="ZIRY">ZIRY</option><option value="ZKBZ">ZKBZ</option>
                                        --%>  </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="oType">Order Type</label>
                                    <select id="oType" name="oType" class="form-control multiselect" data-live-search="true">
                                        <!--<option value="ALL">All</option>-->
                                        <option value="MARKET">Market</option>
                                        <option value="LIMIT">Limit</option>
                                        <option value="PQ">PQ</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="oMorT">Maker/Taker</label>
                                    <select id="oMorT" name="oMorT" class="form-control multiselect" data-live-search="true">
                                        <option value="Taker">Taker</option>
                                        <option value="Maker">Maker</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
                                <div class="form-group col-md-3">
                                    <label for="timeRange">Time Range</label>
                                    <input type="text" class="form-control date-select" readonly id="timeRange" name="timeRange" placeholder="Select Time Range" required="required">
                                </div>

                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary top-buffer2">Get Orders & Trades</button>
                                    <button type="button" id ="strmsbtn" class="btn btn-primary top-buffer2"> Get Streams</button>
                                </div>
                            </div>
                            <div class="row top-buffer2 stream-row">
                                <div class="form-group">
                                    <label for="provider" class="control-label col-sm-10">Select Provider - Stream</label>
                                    <div class="col-sm-8 input-group date">
                                        <select id="provider" name="provider" class="form-control multiselect" data-live-search="true">
                                            <%--  <option value="ADSS/Stream2">ADSS - Stream2</option>
                                              <option value="BARX/TIER3">BARX - TIER3</option>
                                              <option value="BOAN/BandF">BOAN - BandF</option>
                                              <option value="BOAN/BandC">BOAN - BandC</option>
                                              <option value="BOAN/BandD">BOAN - BandD</option>
                                              <option value="BOAN/BandG">BOAN - BandG</option>
                                              <option value="BLP/RCG">BLP - RCG</option>
                                              <option value="CHPT/StreamLD">CHPT - StreamLD</option>
                                              <option value="CITI/Integral1">CITI - Integral1</option>
                                              <option value="CRSU/FXIN_C">CRSU - FXIN_C</option>
                                              <option value="CRSU/FXIN">CRSU - FXIN</option>
                                              <option value="DB/PRD019">DB - PRD019</option>
                                              <option value="EWLP/StreamA">EWLP - StreamA</option>
                                              <option value="GSFX/Retail">GSFX - Retail</option>
                                              <option value="GSFX/Priority">GSFX - Priority</option>
                                              <option value="HSFX/FXPT">HSFX - FXPT</option>
                                              <option value="HSBC/IntGloInst1">HSBC - IntGloInst1</option>
                                              <option value="JPM/Silver">JPM - Silver</option>
                                              <option value="JPM/Gold">JPM - Gold</option>
                                              <option value="JPM/Blue">JPM - Blue</option>
                                              <option value="JPM/Platinum">JPM - Platinum</option>
                                              <option value="JPM/Metals">JPM - Metals</option>
                                              <option value="KCG/KCG_tier2">KCG - KCG_tier2</option>
                                              <option value="MHDP/StreamMC">MHDP - StreamMC</option>
                                              <option value="MSFX/FXIN_M_1">MSFX - FXIN_M_1</option>
                                              <option value="MSFX/FXIN_11">MSFX - FXIN_11</option>
                                              <option value="RBS/integralfull">RBS - integralfull</option>
                                              <option value="RBS/integralset0">RBS - integralset0</option>
                                              <option value="RBS/integralset1a">RBS - integralset1a</option>
                                              <option value="SG/PREA_1">SG - PREA_1</option>
                                              <option value="UBS/FXPRO">UBS - FXPRO</option>
                                              <option value="UBS/Super">UBS - Super</option>
                                              <option value="UBS/Medium">UBS - Medium</option>
                                              <option value="UBS/MIG_NTG">UBS - MIG_NTG</option>
                                              <option value="UBS/Premium">UBS - Premium</option>
                                              <option value="BARX/FXPRO">BARX - FXPRO</option>
                                              <option value="BNPP/GREEN">BNPP - GREEN</option>
                                              <option value="BOAP/SQNS">BOAP - SQNS</option>
                                              <option value="SG/STDA_1">SG - STDA_1</option>
                                              <option value="DB/EU0196733">DB - EU0196733</option>
                                              <option value="VTFX/VFX0">VTFX - VFX0</option>
                                              <option value="CHPT/StreamNY">CHPT - StreamNY</option>
                                              <option value="C241/MS4">C241 - MS4</option>
                                              <option value="CITL/Stream1">CITL - Stream1</option>
                                              <option value="NTFX/stream1">NTFX - stream1</option>
                                              <option value="BNPP/BLUE">BNPP - BLUE</option>
                                              <option value="BNPP/ORANGE">BNPP - ORANGE</option>
                                              <option value="BNPP/RED">BNPP - RED</option>
                                              <option value="BNPP/BROWN">BNPP - BROWN</option>
                                              <option value="BNPP/BLACK">BNPP - BLACK</option>
                                              <option value="BNPP/OLIVE">BNPP - OLIVE</option>
                                              <option value="CFSS/StreamC">CFSS - StreamC</option>
                                              <option value="EFFX/BOAP">EFFX - BOAP</option>
                                              <option value="GFX/GFXDefaultStream">GFX - GFXDefaultStream</option>
                                              <option value="GSFX/ECN">GSFX - ECN</option>
                                              <option value="KMDO/Stream1">KMDO - Stream1</option>
                                              <option value="MSFX/FXIN_4">MSFX - FXIN_4</option>
                                              <option value="SG/STDP_A">SG - STDP_A</option>
                                              <option value="STSB/Institutional">STSB - Institutional</option>
                                              <option value="TRPD/StreamHLR">TRPD - StreamHLR</option>
                                              <option value="TRPDA/StreamLONEPALM">TRPDA - StreamLONEPALM</option>
                                              <option value="TWTH/TWTHStream">TWTH - TWTHStream</option>
                                              <option value="UBS/MS_Standard">UBS - MS_Standard</option>
                                              <option value="VELI/BOAP1">VELI - BOAP1</option>
                                              <option value="VELI/BOAP2">VELI - BOAP2</option>
                                              <option value="VELI/BOAP3">VELI - BOAP3</option>
                                              <option value="VELI/BOAP4">VELI - BOAP4</option>
                                              <option value="VELI/BOAP5">VELI - BOAP5</option>
                                              <option value="VELI/BOAP6">VELI - BOAP6</option>
                                              <option value="VELI/MSFXP1">VELI - MSFXP1</option>
                                              <option value="VELI/MSFXP1">VELI - MSFXP2</option>
                                              <option value="VELI/MSFXP1">VELI - MSFXP3</option>
                                              <option value="VELI/MSFXP1">VELI - MSFXP4</option>
                                              <option value="VELI/MSFXP1">VELI - MSFXP5</option>
                                              <option value="VELI/MSFXP1">VELI - MSFXP6</option>--%>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="chartContainer" class="top-buffer dc-chart"></div>
                    <div id="zoomChart" class="top-buffer"></div>
                    <div class="container dataTablesContainer">
                        <div class="top-buffer">
                            <ul class="nav nav-tabs"  id="blotterTabs">
                                <li class="active"><a href='#blotterTabs-1' data-toggle="tab" data-tablename="ordersTable">Orders</a></li>
                                <li><a href='#blotterTabs-2' data-toggle="tab" data-tablename="tradesTable">Trades</a></li>
                                <li><a href='#blotterTabs-3' data-toggle="tab" data-tablename="benchmarkTable">Benchmark</a></li>
                                <!--<li><a href='#blotterTabs-4' data-toggle="tab" data-tablename="profileTable">Profile</a></li>-->
                                <li><a href='#blotterTabs-5' data-toggle="tab" data-tablename="ratesTable">Rates</a></li>
                            </ul>
                            <div class="tab-content table-tab-content">
                                <div class="tab-pane fade in active" id="blotterTabs-1">
                                    <div id="ordersTableContainer"></div>
                                </div>
                                <div class="tab-pane fade" id="blotterTabs-2">
                                    <div id="tradesTableContainer"></div>
                                </div>
                                <div class="tab-pane fade" id="blotterTabs-3">
                                    <div id="benchmarkTableContainer"></div>
                                </div>
                                <!--<div class="tab-pane fade" id="blotterTabs-4">
                                    <div id="profileTableContainer"></div>
                                </div>-->
                                <div class="tab-pane fade" id="blotterTabs-5">
                                    <div id="ratesTableContainer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade in active" id="emscopetab-2">
                    <div class="setClassLeft">
                        <div class="page-1 page">
                            <div class="order-form text-center">
                                <form class="form-inline" name="orderIdForm" style="margin-top: 120px;">
                                    <h3>Enter Order ID</h3>
                                    <div class="form-group">
                                        <input type="text" id="orderId" name="orderId" class="form-control col-xs-4" />
                                    </div>
                                    <button type="submit" class="btn btn-primary">Go!</button>
                                    <button  type="button" class="btn btn-success btn-order-tree-view">Order Tree View</button>
                                </form>
                            </div>
                        </div>
                        <div class="page-1_1 page display_none">
                            <div class="moduleContainer streamsContainer text-center">
                                <form class="form-inline" role="form" name="getStreamForm">
                                    <div class="form-group">
                                        <div class="input-group date">
                                            <select id="oprovider" name="oprovider" class="form-control multiselect"  data-live-search="true">
                                                <option value="BOANH/BOANHDefaultStream">BOANH - BOANHDefaultStream</option>
                                                <option value="BOANE/BOANEDefaultStream">BOANE - BOANEDefaultStream</option>
                                                <option value="BOANF/BOANFDefaultStream">BOANF - BOANFDefaultStream</option>
                                                <option value="BOANI/BOANIDefaultStream">BOANI - BOANIDefaultStream</option>
                                                <option value="CITIA/Integral1">CITIA - Integral1</option>
                                                <option value="CRSUB/FXIN_C">CRSUB - FXIN_C</option>
                                                <option value="CRSUC/FXIN">CRSUC - FXIN</option>
                                                <option value="GSFXB/Retail">GSFXB - Retail</option>
                                                <option value="GSFXB/Priority">GSFXB - Priority</option>
                                                <option value="HSBCF/IntGloInst1">HSBCF - IntGloInst1</option>
                                                <option value="JPMA/Stream1">JPMA - Stream1</option>
                                                <option value="JPMB/IntegralBlue">JPMB - IntegralBlue</option>
                                                <option value="JPMA/Platinum">JPMA - Platinum</option>
                                                <option value="MHDPA/StreamMC">MHDPA - StreamMC</option>
                                                <option value="MSFXN/FXIN_M_1">MSFXN - FXIN_M_1</option>
                                                <option value="MSFXO/FXIN_11">MSFXO - FXIN_11</option>
                                                <option value="RBSN/integral6">RBSN - integral6</option>
                                                <option value="RBSO/integral0">RBSO - integral0</option>
                                                <option value="RBSN/integral1a">RBSN - integral1a</option>
                                                <option value="SGA/PREA_1">SGA - PREA_1</option>
                                                <option value="UBSA/FXPRO">UBSA - FXPRO</option>
                                                <option value="UBSA/Super">UBSA - Super</option>
                                                <option value="UBSC/Medium">UBSC - Medium</option>
                                                <option value="UBSB/MIG_NTG">UBSB - MIG_NTG</option>
                                                <option value="UBSA/Premium">UBSA - Premium</option>
                                                <option value="BARXB/FXPRO">BARXB - FXPRO</option>
                                                <option value="BNPPC/GREEN">BNPPC - GREEN</option>
                                                <option value="SGA/STDA_1">SGA - STDA_1</option>
                                                <option value="DB15/EU0196733">DB15 - EU0196733</option>
                                                <option value="VTFXA/VFX0">VTFXA - VFX0</option>
                                                <option value="CHPTA/StreamNY">CHPTA - StreamNY</option>
                                                <option value="NTFXA/stream1">NTFXA - stream1</option>
                                                <option value="ADSSA/Stream1">ADSSA - Stream1</option>
                                                <option value="HSFXE/AXICORP">HSFXE - AXICORP</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" id="doGetStream" class="btn btn-primary">Get Stream</button>
                                    <button type="button" id="doAuditEvent" class="btn btn-primary">Trade Response Received from Provider</button>
                                </form>
                            </div>
                            <div class="top-buffer">
                                <button class="zoomOut btn btn-default">Zoom-out 2 seconds</button>&nbsp;&nbsp;
                                <button class="undoZoom btn btn-default disabled">Undo Zoom</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp
                                <button class="vlscape btn btn-primary">View In Landscape >></button>
                            </div>
                        </div>  
                    </div>                  
                    <div class="pieChartContainer moduleContainer col-md-5 col-lg-5 text-center" style="margin-top: 75px;">
                        <div class="confirmedTrades">
                            <h4 class="heading">Confirmed Trades</h4>
                            <div id="tradeCptyConfirmedChartContainer" style="float: right;"></div>
                        </div>
                        <div class="rejectedTrades">
                            <h4 class="heading">Rejected Trades</h4>
                            <div id="tradeCptyRejectedChartContainer" style="float: right;"></div>
                        </div>
                    </div>
                    <div class="clear"></div>                        

                    <div class="page-2 page">   
                        <div id="newOrderChartContainer"></div>
                        <div style="float: right; display: inline-block ! important;">
                            <span>Filter Trades By Stream: </span>
                            <input type="checkbox" id="filterByStreamToggle" name="filterByStreamToggle" data-size="mini">
                        </div> 

                        <div class="orderChartContainer moduleContainer" id="orderChartContainer" style="margin-top: -35px;">
                        </div><div class="clear"></div>
                        <div class="ordzoomChart ordZoomContainer" id="ordzoomChart">
                        </div><div class="clear"></div>
                        <div class="narrativeContainer moduleContainer">
                            <h4 class="heading">Summary</h4>
                            <div class="narrative"></div>
                        </div><div class="clear"></div>
                        <div class="moduleContainer execSummaryContainer">
                            <h4 class="heading">Execution Summary</h4>
                            <div id="execSummary"></div>
                        </div><div class="clear"></div>
                        <div class="orderDetailsContainer moduleContainer row">
                            <h4 class="heading">Order Details</h4>
                            <div class="col-md-6 col-lg-6">
                                <table class="table table-bordered">

                                </table>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <table class="table table-bordered">

                                </table>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="orderTabDTContainer">
                            <div class="top-buffer" style="margin-left:0;">
                                <ul class="nav nav-tabs"  id="orderTabDataTabs">
                                    <li class="active"><a href='#orderTabDataTabs-1' data-toggle="tab" data-tablename="oTTradesTable">Trades</a></li>
                                    <li><a href='#orderTabDataTabs-2' data-toggle="tab" data-tablename="oTBenchmarkTable">Benchmark</a></li>
                                    <li><a href='#orderTabDataTabs-3' data-toggle="tab" data-tablename="oTRatesTable">Rates</a></li>
                                    <li><a href='#orderTabDataTabs-4' data-toggle="tab" data-tablename="oTProfileTable">Stream Profile</a></li>
                                </ul>
                                <div class="tab-content table-tab-content">
                                    <div class="tab-pane fade in active" id="orderTabDataTabs-1">
                                        <div id="oTTradesTableContainer"></div>
                                    </div>
                                    <div class="tab-pane fade" id="orderTabDataTabs-2">
                                        <div id="oTBenchmarkTableContainer"></div>
                                    </div>
                                    <div class="tab-pane fade" id="orderTabDataTabs-3">
                                        <div id="oTRatesTableContainer"></div>
                                    </div>
                                    <div class="tab-pane fade" id="orderTabDataTabs-4">
                                        <div id="oTProfileTableContainer"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Order Tree navigation with expand and collapse
                            @author    Anil    <nl.sainil@gmail.com>  -->
                        <div class="orderNavigateTreeView" style="display:none;margin-top: 62px;">
                            <button><span>O<br/>R<br/>D<br/>E<br/>R<br/><br/>T<br/>R<br/>E<br/>E</span></button>
                            <div></div>
                        </div>
                    </div>
                </div>
                <%-- start third tab --%>
                <div class="tab-pane fade in-active" id="emscopetab-3">

                    <div class="form-holder" style="margin-left: 25px;">
                        <form class="form-inline" role="form" name="getDisplayOrdersForm" method="POST">
                            <div class="row">
                                <div class="form-group col-md-2">
                                    <label for="doCcyPair">Currency Pair</label>
                                    <select class="form-control multiselect" id="doCcyPair" name="doCcyPair" data-live-search="true">
                                        <option value="EUR/USD">EUR/USD</option> <option value="AED/BHD">AED/BHD</option> <option value="AED/CHF">AED/CHF</option> <option value="AED/EUR">AED/EUR</option> <option value="AED/GBP">AED/GBP</option> <option value="AED/JPY">AED/JPY</option> <option value="AED/KWD">AED/KWD</option> <option value="AED/OMR">AED/OMR</option> <option value="AED/QAR">AED/QAR</option> <option value="AED/SAR">AED/SAR</option> <option value="AED/USD">AED/USD</option> <option value="AED/ZAR">AED/ZAR</option> <option value="AUD/AED">AUD/AED</option> <option value="AUD/CAD">AUD/CAD</option> <option value="AUD/CHF">AUD/CHF</option> <option value="AUD/CNH">AUD/CNH</option> <option value="AUD/DKK">AUD/DKK</option> <option value="AUD/EUR">AUD/EUR</option> <option value="AUD/GBP">AUD/GBP</option> <option value="AUD/HKD">AUD/HKD</option> <option value="AUD/IDR">AUD/IDR</option> <option value="AUD/JPY">AUD/JPY</option> <option value="AUD/KWD">AUD/KWD</option> <option value="AUD/MXN">AUD/MXN</option> <option value="AUD/NOK">AUD/NOK</option> <option value="AUD/NZD">AUD/NZD</option> <option value="AUD/RUB">AUD/RUB</option> <option value="AUD/SAR">AUD/SAR</option> <option value="AUD/SEK">AUD/SEK</option> <option value="AUD/SGD">AUD/SGD</option> <option value="AUD/THB">AUD/THB</option> <option value="AUD/USD">AUD/USD</option> <option value="AUD/ZAR">AUD/ZAR</option> <option value="BHD/AED">BHD/AED</option> <option value="BRL/USD">BRL/USD</option> <option value="CAD/AED">CAD/AED</option> <option value="CAD/AUD">CAD/AUD</option> <option value="CAD/CHF">CAD/CHF</option> <option value="CAD/CZK">CAD/CZK</option> <option value="CAD/DKK">CAD/DKK</option> <option value="CAD/EUR">CAD/EUR</option> <option value="CAD/GBP">CAD/GBP</option> <option value="CAD/HKD">CAD/HKD</option> <option value="CAD/JPY">CAD/JPY</option> <option value="CAD/MXN">CAD/MXN</option> <option value="CAD/NOK">CAD/NOK</option> <option value="CAD/PLN">CAD/PLN</option> <option value="CAD/RUB">CAD/RUB</option> <option value="CAD/SAR">CAD/SAR</option> <option value="CAD/SEK">CAD/SEK</option> <option value="CAD/SGD">CAD/SGD</option> <option value="CAD/THB">CAD/THB</option> <option value="CAD/USD">CAD/USD</option> <option value="CAD/ZAR">CAD/ZAR</option> <option value="CHF/AED">CHF/AED</option> <option value="CHF/CAD">CHF/CAD</option> <option value="CHF/CNH">CHF/CNH</option> <option value="CHF/CZK">CHF/CZK</option> <option value="CHF/DKK">CHF/DKK</option> <option value="CHF/EUR">CHF/EUR</option> <option value="CHF/HKD">CHF/HKD</option> <option value="CHF/HUF">CHF/HUF</option> <option value="CHF/JPY">CHF/JPY</option> <option value="CHF/KES">CHF/KES</option> <option value="CHF/MXN">CHF/MXN</option> <option value="CHF/NOK">CHF/NOK</option> <option value="CHF/PLN">CHF/PLN</option> <option value="CHF/RON">CHF/RON</option> <option value="CHF/RUB">CHF/RUB</option> <option value="CHF/SAR">CHF/SAR</option> <option value="CHF/SEK">CHF/SEK</option> <option value="CHF/SGD">CHF/SGD</option> <option value="CHF/THB">CHF/THB</option> <option value="CHF/TRL">CHF/TRL</option> <option value="CHF/TRY">CHF/TRY</option> <option value="CHF/USD">CHF/USD</option> <option value="CHF/ZAR">CHF/ZAR</option> <option value="CNH/JPY">CNH/JPY</option> <option value="CNH/RUB">CNH/RUB</option> <option value="CZK/CHF">CZK/CHF</option> <option value="CZK/JPY">CZK/JPY</option> <option value="CZK/SEK">CZK/SEK</option> <option value="DKK/CHF">DKK/CHF</option> <option value="DKK/CZK">DKK/CZK</option> <option value="DKK/EUR">DKK/EUR</option> <option value="DKK/JPY">DKK/JPY</option> <option value="DKK/NOK">DKK/NOK</option> <option value="DKK/SEK">DKK/SEK</option> <option value="DKK/USD">DKK/USD</option> <option value="EUR/AED">EUR/AED</option> <option value="EUR/AUD">EUR/AUD</option> <option value="EUR/BGN">EUR/BGN</option> <option value="EUR/CAD">EUR/CAD</option> <option value="EUR/CHF">EUR/CHF</option> <option value="EUR/CLP">EUR/CLP</option> <option value="EUR/CNH">EUR/CNH</option> <option value="EUR/CNY">EUR/CNY</option> <option value="EUR/CZK">EUR/CZK</option> <option value="EUR/DKK">EUR/DKK</option> <option value="EUR/GBP">EUR/GBP</option> <option value="EUR/HKD">EUR/HKD</option> <option value="EUR/HRK">EUR/HRK</option> <option value="EUR/HUF">EUR/HUF</option> <option value="EUR/ILS">EUR/ILS</option> <option value="EUR/INR">EUR/INR</option> <option value="EUR/ISK">EUR/ISK</option> <option value="EUR/JPY">EUR/JPY</option> <option value="EUR/KWD">EUR/KWD</option> <option value="EUR/LVL">EUR/LVL</option> <option value="EUR/MXN">EUR/MXN</option> <option value="EUR/NOK">EUR/NOK</option> <option value="EUR/NZD">EUR/NZD</option> <option value="EUR/PLN">EUR/PLN</option> <option value="EUR/RON">EUR/RON</option><option value="EUR/RSD">EUR/RSD</option><option value="EUR/RU0">EUR/RU0</option> <option value="EUR/RUB">EUR/RUB</option><option value="EUR/SAR">EUR/SAR</option><option value="EUR/SEK">EUR/SEK</option> <option value="EUR/SGD">EUR/SGD</option><option value="EUR/THB">EUR/THB</option><option value="EUR/TRL">EUR/TRL</option> <option value="EUR/TRY">EUR/TRY</option><option value="EUR/ZAR">EUR/ZAR</option> <option value="GBP/AED">GBP/AED</option><option value="GBP/AUD">GBP/AUD</option><option value="GBP/BGN">GBP/BGN</option> <option value="GBP/BHD">GBP/BHD</option><option value="GBP/CAD">GBP/CAD</option><option value="GBP/CHF">GBP/CHF</option> <option value="GBP/CLP">GBP/CLP</option><option value="GBP/CZK">GBP/CZK</option><option value="GBP/DKK">GBP/DKK</option> <option value="GBP/EUR">GBP/EUR</option><option value="GBP/HKD">GBP/HKD</option><option value="GBP/HUF">GBP/HUF</option> <option value="GBP/ILS">GBP/ILS</option><option value="GBP/INR">GBP/INR</option><option value="GBP/JPY">GBP/JPY</option> <option value="GBP/MUR">GBP/MUR</option><option value="GBP/MXN">GBP/MXN</option><option value="GBP/NOK">GBP/NOK</option> <option value="GBP/NZD">GBP/NZD</option><option value="GBP/OMR">GBP/OMR</option><option value="GBP/PHP">GBP/PHP</option> <option value="GBP/PLN">GBP/PLN</option><option value="GBP/QAR">GBP/QAR</option><option value="GBP/RUB">GBP/RUB</option> <option value="GBP/SAR">GBP/SAR</option><option value="GBP/SEK">GBP/SEK</option><option value="GBP/SGD">GBP/SGD</option> <option value="GBP/THB">GBP/THB</option><option value="GBP/TRL">GBP/TRL</option><option value="GBP/TRY">GBP/TRY</option> <option value="GBP/TWD">GBP/TWD</option><option value="GBP/USD">GBP/USD</option><option value="GBP/ZAR">GBP/ZAR</option> <option value="HKD/CNH">HKD/CNH</option><option value="HKD/EUR">HKD/EUR</option><option value="HKD/JPY">HKD/JPY</option> <option value="HKD/NOK">HKD/NOK</option><option value="HKD/THB">HKD/THB</option><option value="HKD/USD">HKD/USD</option> <option value="HUF/EUR">HUF/EUR</option><option value="ILS/USD">ILS/USD</option><option value="INR/CHF">INR/CHF</option> <option value="ISK/CHF">ISK/CHF</option><option value="JPY/AED">JPY/AED</option><option value="JPY/BGN">JPY/BGN</option> <option value="JPY/CHF">JPY/CHF</option><option value="JPY/EUR">JPY/EUR</option><option value="JPY/HKD">JPY/HKD</option> <option value="JPY/KRW">JPY/KRW</option><option value="JPY/MYR">JPY/MYR</option><option value="JPY/SGD">JPY/SGD</option> <option value="JPY/TRY">JPY/TRY</option><option value="JPY/USD">JPY/USD</option><option value="KWD/AED">KWD/AED</option> <option value="KWD/BHD">KWD/BHD</option><option value="KWD/CAD">KWD/CAD</option><option value="MXN/JPY">MXN/JPY</option> <option value="MXN/SEK">MXN/SEK</option><option value="MXN/USD">MXN/USD</option><option value="NOK/CAD">NOK/CAD</option> <option value="NOK/CHF">NOK/CHF</option><option value="NOK/CZK">NOK/CZK</option><option value="NOK/DKK">NOK/DKK</option> <option value="NOK/GBP">NOK/GBP</option><option value="NOK/HKD">NOK/HKD</option><option value="NOK/JPY">NOK/JPY</option> <option value="NOK/NZD">NOK/NZD</option><option value="NOK/SEK">NOK/SEK</option><option value="NOK/USD">NOK/USD</option> <option value="NZD/AED">NZD/AED</option><option value="NZD/AUD">NZD/AUD</option><option value="NZD/CAD">NZD/CAD</option> <option value="NZD/CHF">NZD/CHF</option><option value="NZD/DKK">NZD/DKK</option><option value="NZD/GBP">NZD/GBP</option> <option value="NZD/HKD">NZD/HKD</option><option value="NZD/JPY">NZD/JPY</option><option value="NZD/MYR">NZD/MYR</option> <option value="NZD/NOK">NZD/NOK</option><option value="NZD/SAR">NZD/SAR</option><option value="NZD/SEK">NZD/SEK</option> <option value="NZD/SGD">NZD/SGD</option><option value="NZD/USD">NZD/USD</option><option value="OMR/AED">OMR/AED</option> <option value="PLN/CHF">PLN/CHF</option><option value="PLN/CZK">PLN/CZK</option><option value="PLN/DKK">PLN/DKK</option> <option value="PLN/HUF">PLN/HUF</option><option value="PLN/JPY">PLN/JPY</option><option value="PLN/NOK">PLN/NOK</option> <option value="PLN/SEK">PLN/SEK</option><option value="QAR/AED">QAR/AED</option><option value="RUB/BGN">RUB/BGN</option> <option value="RUB/JPY">RUB/JPY</option><option value="SAR/AED">SAR/AED</option><option value="SAR/CHF">SAR/CHF</option> <option value="SAR/JPY">SAR/JPY</option><option value="SAR/SEK">SAR/SEK</option><option value="SAR/USD">SAR/USD</option> <option value="SEK/BGN">SEK/BGN</option><option value="SEK/CHF">SEK/CHF</option><option value="SEK/DKK">SEK/DKK</option> <option value="SEK/EUR">SEK/EUR</option><option value="SEK/GBP">SEK/GBP</option><option value="SEK/JPY">SEK/JPY</option> <option value="SEK/PLN">SEK/PLN</option><option value="SEK/RUB">SEK/RUB</option><option value="SEK/THB">SEK/THB</option> <option value="SEK/TRY">SEK/TRY</option><option value="SEK/USD">SEK/USD</option><option value="SGD/AED">SGD/AED</option> <option value="SGD/AUD">SGD/AUD</option><option value="SGD/CHF">SGD/CHF</option><option value="SGD/CNH">SGD/CNH</option> <option value="SGD/GBP">SGD/GBP</option><option value="SGD/HKD">SGD/HKD</option><option value="SGD/JPY">SGD/JPY</option> <option value="SGD/NOK">SGD/NOK</option><option value="SGD/NZD">SGD/NZD</option><option value="SGD/PLN">SGD/PLN</option> <option value="SGD/THB">SGD/THB</option><option value="SGD/USD">SGD/USD</option><option value="THB/JPY">THB/JPY</option> <option value="THB/USD">THB/USD</option><option value="TRY/JPY">TRY/JPY</option><option value="TRY/PLN">TRY/PLN</option> <option value="TRY/USD">TRY/USD</option><option value="USD/AE0">USD/AE0</option><option value="USD/AE1">USD/AE1</option> <option value="USD/AED">USD/AED</option><option value="USD/ARS">USD/ARS</option><option value="USD/AUD">USD/AUD</option> <option value="USD/BBD">USD/BBD</option><option value="USD/BDT">USD/BDT</option><option value="USD/BGN">USD/BGN</option> <option value="USD/BH1">USD/BH1</option><option value="USD/BHD">USD/BHD</option><option value="USD/BIF">USD/BIF</option> <option value="USD/BND">USD/BND</option><option value="USD/BOB">USD/BOB</option><option value="USD/BRL">USD/BRL</option> <option value="USD/BYR">USD/BYR</option><option value="USD/CAD">USD/CAD</option><option value="USD/CHF">USD/CHF</option> <option value="USD/CLP">USD/CLP</option><option value="USD/CNH">USD/CNH</option><option value="USD/CNY">USD/CNY</option> <option value="USD/COP">USD/COP</option><option value="USD/CRC">USD/CRC</option><option value="USD/CZK">USD/CZK</option> <option value="USD/DJF">USD/DJF</option><option value="USD/DKK">USD/DKK</option><option value="USD/DOP">USD/DOP</option> <option value="USD/EGP">USD/EGP</option><option value="USD/ETB">USD/ETB</option><option value="USD/EUR">USD/EUR</option> <option value="USD/GBP">USD/GBP</option><option value="USD/GMD">USD/GMD</option><option value="USD/GTQ">USD/GTQ</option> <option value="USD/HKD">USD/HKD</option><option value="USD/HNL">USD/HNL</option><option value="USD/HRK">USD/HRK</option> <option value="USD/HTG">USD/HTG</option><option value="USD/HUF">USD/HUF</option><option value="USD/IDR">USD/IDR</option> <option value="USD/ILS">USD/ILS</option><option value="USD/INR">USD/INR</option><option value="USD/ISK">USD/ISK</option> <option value="USD/JMD">USD/JMD</option><option value="USD/JOD">USD/JOD</option><option value="USD/JPY">USD/JPY</option> <option value="USD/KES">USD/KES</option><option value="USD/KRW">USD/KRW</option><option value="USD/KWD">USD/KWD</option> <option value="USD/KZT">USD/KZT</option><option value="USD/LBP">USD/LBP</option><option value="USD/LKR">USD/LKR</option> <option value="USD/LSL">USD/LSL</option><option value="USD/LTL">USD/LTL</option><option value="USD/LVL">USD/LVL</option> <option value="USD/MAD">USD/MAD</option><option value="USD/MRO">USD/MRO</option><option value="USD/MUR">USD/MUR</option> <option value="USD/MWK">USD/MWK</option><option value="USD/MXN">USD/MXN</option><option value="USD/MYR">USD/MYR</option> <option value="USD/NAD">USD/NAD</option><option value="USD/NGN">USD/NGN</option><option value="USD/NIO">USD/NIO</option> <option value="USD/NOK">USD/NOK</option><option value="USD/NPR">USD/NPR</option><option value="USD/NZD">USD/NZD</option> <option value="USD/OM1">USD/OM1</option><option value="USD/OMR">USD/OMR</option><option value="USD/PEN">USD/PEN</option> <option value="USD/PHP">USD/PHP</option><option value="USD/PKR">USD/PKR</option><option value="USD/PLN">USD/PLN</option> <option value="USD/PYG">USD/PYG</option><option value="USD/QA1">USD/QA1</option><option value="USD/QAR">USD/QAR</option> <option value="USD/RON">USD/RON</option><option value="USD/RSD">USD/RSD</option><option value="USD/RU0">USD/RU0</option> <option value="USD/RUB">USD/RUB</option><option value="USD/RUX">USD/RUX</option><option value="USD/SA1">USD/SA1</option> <option value="USD/SAR">USD/SAR</option><option value="USD/SBD">USD/SBD</option><option value="USD/SEK">USD/SEK</option> <option value="USD/SGD">USD/SGD</option><option value="USD/SZL">USD/SZL</option><option value="USD/THB">USD/THB</option> <option value="USD/TND">USD/TND</option><option value="USD/TRL">USD/TRL</option><option value="USD/TRY">USD/TRY</option> <option value="USD/TTD">USD/TTD</option><option value="USD/TWD">USD/TWD</option><option value="USD/TZS">USD/TZS</option> <option value="USD/UGX">USD/UGX</option><option value="USD/UYU">USD/UYU</option><option value="USD/VND">USD/VND</option> <option value="USD/VUV">USD/VUV</option><option value="USD/XAF">USD/XAF</option><option value="USD/ZAR">USD/ZAR</option> <option value="USD/ZMK">USD/ZMK</option><option value="XAG/AUD">XAG/AUD</option><option value="XAG/CAD">XAG/CAD</option> <option value="XAG/CHF">XAG/CHF</option><option value="XAG/EUR">XAG/EUR</option><option value="XAG/GBP">XAG/GBP</option> <option value="XAG/USD">XAG/USD</option><option value="XAU/AUD">XAU/AUD</option><option value="XAU/CAD">XAU/CAD</option> <option value="XAU/CHF">XAU/CHF</option><option value="XAU/EUR">XAU/EUR</option><option value="XAU/GBP">XAU/GBP</option> <option value="XAU/TRY">XAU/TRY</option><option value="XAU/USD">XAU/USD</option><option value="XPD/USD">XPD/USD</option> <option value="XPT/EUR">XPT/EUR</option><option value="XPT/USD">XPT/USD</option><option value="ZAR/CHF">ZAR/CHF</option> <option value="ZAR/EUR">ZAR/EUR</option><option value="ZAR/JPY">ZAR/JPY</option><option value="ZAR/USD">ZAR/USD</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="time">Time</label>
                                    <input type="text" class="form-control datetime-select" readonly id="time" name="time" placeholder="Select Time" required="required">
                                </div>		
                                <div class="col-md-2" style="margin-top: -55px;">
                                    <button type="submit" class="btn btn-primary top-buffer">Get Display Orders At Second</button>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="form-group col-md-2">
                                    <label for="doCcyPair2">Currency Pair</label>
                                    <select class="form-control multiselect" id="doCcyPair2" name="doCcyPair2" data-live-search="true">
                                        <option value="EUR/USD">EUR/USD</option> <option value="AED/BHD">AED/BHD</option> <option value="AED/CHF">AED/CHF</option> <option value="AED/EUR">AED/EUR</option> <option value="AED/GBP">AED/GBP</option> <option value="AED/JPY">AED/JPY</option> <option value="AED/KWD">AED/KWD</option> <option value="AED/OMR">AED/OMR</option> <option value="AED/QAR">AED/QAR</option> <option value="AED/SAR">AED/SAR</option> <option value="AED/USD">AED/USD</option> <option value="AED/ZAR">AED/ZAR</option> <option value="AUD/AED">AUD/AED</option> <option value="AUD/CAD">AUD/CAD</option> <option value="AUD/CHF">AUD/CHF</option> <option value="AUD/CNH">AUD/CNH</option> <option value="AUD/DKK">AUD/DKK</option> <option value="AUD/EUR">AUD/EUR</option> <option value="AUD/GBP">AUD/GBP</option> <option value="AUD/HKD">AUD/HKD</option> <option value="AUD/IDR">AUD/IDR</option> <option value="AUD/JPY">AUD/JPY</option> <option value="AUD/KWD">AUD/KWD</option> <option value="AUD/MXN">AUD/MXN</option> <option value="AUD/NOK">AUD/NOK</option> <option value="AUD/NZD">AUD/NZD</option> <option value="AUD/RUB">AUD/RUB</option> <option value="AUD/SAR">AUD/SAR</option> <option value="AUD/SEK">AUD/SEK</option> <option value="AUD/SGD">AUD/SGD</option> <option value="AUD/THB">AUD/THB</option> <option value="AUD/USD">AUD/USD</option> <option value="AUD/ZAR">AUD/ZAR</option> <option value="BHD/AED">BHD/AED</option> <option value="BRL/USD">BRL/USD</option> <option value="CAD/AED">CAD/AED</option> <option value="CAD/AUD">CAD/AUD</option> <option value="CAD/CHF">CAD/CHF</option> <option value="CAD/CZK">CAD/CZK</option> <option value="CAD/DKK">CAD/DKK</option> <option value="CAD/EUR">CAD/EUR</option> <option value="CAD/GBP">CAD/GBP</option> <option value="CAD/HKD">CAD/HKD</option> <option value="CAD/JPY">CAD/JPY</option> <option value="CAD/MXN">CAD/MXN</option> <option value="CAD/NOK">CAD/NOK</option> <option value="CAD/PLN">CAD/PLN</option> <option value="CAD/RUB">CAD/RUB</option> <option value="CAD/SAR">CAD/SAR</option> <option value="CAD/SEK">CAD/SEK</option> <option value="CAD/SGD">CAD/SGD</option> <option value="CAD/THB">CAD/THB</option> <option value="CAD/USD">CAD/USD</option> <option value="CAD/ZAR">CAD/ZAR</option> <option value="CHF/AED">CHF/AED</option> <option value="CHF/CAD">CHF/CAD</option> <option value="CHF/CNH">CHF/CNH</option> <option value="CHF/CZK">CHF/CZK</option> <option value="CHF/DKK">CHF/DKK</option> <option value="CHF/EUR">CHF/EUR</option> <option value="CHF/HKD">CHF/HKD</option> <option value="CHF/HUF">CHF/HUF</option> <option value="CHF/JPY">CHF/JPY</option> <option value="CHF/KES">CHF/KES</option> <option value="CHF/MXN">CHF/MXN</option> <option value="CHF/NOK">CHF/NOK</option> <option value="CHF/PLN">CHF/PLN</option> <option value="CHF/RON">CHF/RON</option> <option value="CHF/RUB">CHF/RUB</option> <option value="CHF/SAR">CHF/SAR</option> <option value="CHF/SEK">CHF/SEK</option> <option value="CHF/SGD">CHF/SGD</option> <option value="CHF/THB">CHF/THB</option> <option value="CHF/TRL">CHF/TRL</option> <option value="CHF/TRY">CHF/TRY</option> <option value="CHF/USD">CHF/USD</option> <option value="CHF/ZAR">CHF/ZAR</option> <option value="CNH/JPY">CNH/JPY</option> <option value="CNH/RUB">CNH/RUB</option> <option value="CZK/CHF">CZK/CHF</option> <option value="CZK/JPY">CZK/JPY</option> <option value="CZK/SEK">CZK/SEK</option> <option value="DKK/CHF">DKK/CHF</option> <option value="DKK/CZK">DKK/CZK</option> <option value="DKK/EUR">DKK/EUR</option> <option value="DKK/JPY">DKK/JPY</option> <option value="DKK/NOK">DKK/NOK</option> <option value="DKK/SEK">DKK/SEK</option> <option value="DKK/USD">DKK/USD</option> <option value="EUR/AED">EUR/AED</option> <option value="EUR/AUD">EUR/AUD</option> <option value="EUR/BGN">EUR/BGN</option> <option value="EUR/CAD">EUR/CAD</option> <option value="EUR/CHF">EUR/CHF</option> <option value="EUR/CLP">EUR/CLP</option> <option value="EUR/CNH">EUR/CNH</option> <option value="EUR/CNY">EUR/CNY</option> <option value="EUR/CZK">EUR/CZK</option> <option value="EUR/DKK">EUR/DKK</option> <option value="EUR/GBP">EUR/GBP</option> <option value="EUR/HKD">EUR/HKD</option> <option value="EUR/HRK">EUR/HRK</option> <option value="EUR/HUF">EUR/HUF</option> <option value="EUR/ILS">EUR/ILS</option> <option value="EUR/INR">EUR/INR</option> <option value="EUR/ISK">EUR/ISK</option> <option value="EUR/JPY">EUR/JPY</option> <option value="EUR/KWD">EUR/KWD</option> <option value="EUR/LVL">EUR/LVL</option> <option value="EUR/MXN">EUR/MXN</option> <option value="EUR/NOK">EUR/NOK</option> <option value="EUR/NZD">EUR/NZD</option> <option value="EUR/PLN">EUR/PLN</option> <option value="EUR/RON">EUR/RON</option><option value="EUR/RSD">EUR/RSD</option><option value="EUR/RU0">EUR/RU0</option> <option value="EUR/RUB">EUR/RUB</option><option value="EUR/SAR">EUR/SAR</option><option value="EUR/SEK">EUR/SEK</option> <option value="EUR/SGD">EUR/SGD</option><option value="EUR/THB">EUR/THB</option><option value="EUR/TRL">EUR/TRL</option> <option value="EUR/TRY">EUR/TRY</option><option value="EUR/ZAR">EUR/ZAR</option> <option value="GBP/AED">GBP/AED</option><option value="GBP/AUD">GBP/AUD</option><option value="GBP/BGN">GBP/BGN</option> <option value="GBP/BHD">GBP/BHD</option><option value="GBP/CAD">GBP/CAD</option><option value="GBP/CHF">GBP/CHF</option> <option value="GBP/CLP">GBP/CLP</option><option value="GBP/CZK">GBP/CZK</option><option value="GBP/DKK">GBP/DKK</option> <option value="GBP/EUR">GBP/EUR</option><option value="GBP/HKD">GBP/HKD</option><option value="GBP/HUF">GBP/HUF</option> <option value="GBP/ILS">GBP/ILS</option><option value="GBP/INR">GBP/INR</option><option value="GBP/JPY">GBP/JPY</option> <option value="GBP/MUR">GBP/MUR</option><option value="GBP/MXN">GBP/MXN</option><option value="GBP/NOK">GBP/NOK</option> <option value="GBP/NZD">GBP/NZD</option><option value="GBP/OMR">GBP/OMR</option><option value="GBP/PHP">GBP/PHP</option> <option value="GBP/PLN">GBP/PLN</option><option value="GBP/QAR">GBP/QAR</option><option value="GBP/RUB">GBP/RUB</option> <option value="GBP/SAR">GBP/SAR</option><option value="GBP/SEK">GBP/SEK</option><option value="GBP/SGD">GBP/SGD</option> <option value="GBP/THB">GBP/THB</option><option value="GBP/TRL">GBP/TRL</option><option value="GBP/TRY">GBP/TRY</option> <option value="GBP/TWD">GBP/TWD</option><option value="GBP/USD">GBP/USD</option><option value="GBP/ZAR">GBP/ZAR</option> <option value="HKD/CNH">HKD/CNH</option><option value="HKD/EUR">HKD/EUR</option><option value="HKD/JPY">HKD/JPY</option> <option value="HKD/NOK">HKD/NOK</option><option value="HKD/THB">HKD/THB</option><option value="HKD/USD">HKD/USD</option> <option value="HUF/EUR">HUF/EUR</option><option value="ILS/USD">ILS/USD</option><option value="INR/CHF">INR/CHF</option> <option value="ISK/CHF">ISK/CHF</option><option value="JPY/AED">JPY/AED</option><option value="JPY/BGN">JPY/BGN</option> <option value="JPY/CHF">JPY/CHF</option><option value="JPY/EUR">JPY/EUR</option><option value="JPY/HKD">JPY/HKD</option> <option value="JPY/KRW">JPY/KRW</option><option value="JPY/MYR">JPY/MYR</option><option value="JPY/SGD">JPY/SGD</option> <option value="JPY/TRY">JPY/TRY</option><option value="JPY/USD">JPY/USD</option><option value="KWD/AED">KWD/AED</option> <option value="KWD/BHD">KWD/BHD</option><option value="KWD/CAD">KWD/CAD</option><option value="MXN/JPY">MXN/JPY</option> <option value="MXN/SEK">MXN/SEK</option><option value="MXN/USD">MXN/USD</option><option value="NOK/CAD">NOK/CAD</option> <option value="NOK/CHF">NOK/CHF</option><option value="NOK/CZK">NOK/CZK</option><option value="NOK/DKK">NOK/DKK</option> <option value="NOK/GBP">NOK/GBP</option><option value="NOK/HKD">NOK/HKD</option><option value="NOK/JPY">NOK/JPY</option> <option value="NOK/NZD">NOK/NZD</option><option value="NOK/SEK">NOK/SEK</option><option value="NOK/USD">NOK/USD</option> <option value="NZD/AED">NZD/AED</option><option value="NZD/AUD">NZD/AUD</option><option value="NZD/CAD">NZD/CAD</option> <option value="NZD/CHF">NZD/CHF</option><option value="NZD/DKK">NZD/DKK</option><option value="NZD/GBP">NZD/GBP</option> <option value="NZD/HKD">NZD/HKD</option><option value="NZD/JPY">NZD/JPY</option><option value="NZD/MYR">NZD/MYR</option> <option value="NZD/NOK">NZD/NOK</option><option value="NZD/SAR">NZD/SAR</option><option value="NZD/SEK">NZD/SEK</option> <option value="NZD/SGD">NZD/SGD</option><option value="NZD/USD">NZD/USD</option><option value="OMR/AED">OMR/AED</option> <option value="PLN/CHF">PLN/CHF</option><option value="PLN/CZK">PLN/CZK</option><option value="PLN/DKK">PLN/DKK</option> <option value="PLN/HUF">PLN/HUF</option><option value="PLN/JPY">PLN/JPY</option><option value="PLN/NOK">PLN/NOK</option> <option value="PLN/SEK">PLN/SEK</option><option value="QAR/AED">QAR/AED</option><option value="RUB/BGN">RUB/BGN</option> <option value="RUB/JPY">RUB/JPY</option><option value="SAR/AED">SAR/AED</option><option value="SAR/CHF">SAR/CHF</option> <option value="SAR/JPY">SAR/JPY</option><option value="SAR/SEK">SAR/SEK</option><option value="SAR/USD">SAR/USD</option> <option value="SEK/BGN">SEK/BGN</option><option value="SEK/CHF">SEK/CHF</option><option value="SEK/DKK">SEK/DKK</option> <option value="SEK/EUR">SEK/EUR</option><option value="SEK/GBP">SEK/GBP</option><option value="SEK/JPY">SEK/JPY</option> <option value="SEK/PLN">SEK/PLN</option><option value="SEK/RUB">SEK/RUB</option><option value="SEK/THB">SEK/THB</option> <option value="SEK/TRY">SEK/TRY</option><option value="SEK/USD">SEK/USD</option><option value="SGD/AED">SGD/AED</option> <option value="SGD/AUD">SGD/AUD</option><option value="SGD/CHF">SGD/CHF</option><option value="SGD/CNH">SGD/CNH</option> <option value="SGD/GBP">SGD/GBP</option><option value="SGD/HKD">SGD/HKD</option><option value="SGD/JPY">SGD/JPY</option> <option value="SGD/NOK">SGD/NOK</option><option value="SGD/NZD">SGD/NZD</option><option value="SGD/PLN">SGD/PLN</option> <option value="SGD/THB">SGD/THB</option><option value="SGD/USD">SGD/USD</option><option value="THB/JPY">THB/JPY</option> <option value="THB/USD">THB/USD</option><option value="TRY/JPY">TRY/JPY</option><option value="TRY/PLN">TRY/PLN</option> <option value="TRY/USD">TRY/USD</option><option value="USD/AE0">USD/AE0</option><option value="USD/AE1">USD/AE1</option> <option value="USD/AED">USD/AED</option><option value="USD/ARS">USD/ARS</option><option value="USD/AUD">USD/AUD</option> <option value="USD/BBD">USD/BBD</option><option value="USD/BDT">USD/BDT</option><option value="USD/BGN">USD/BGN</option> <option value="USD/BH1">USD/BH1</option><option value="USD/BHD">USD/BHD</option><option value="USD/BIF">USD/BIF</option> <option value="USD/BND">USD/BND</option><option value="USD/BOB">USD/BOB</option><option value="USD/BRL">USD/BRL</option> <option value="USD/BYR">USD/BYR</option><option value="USD/CAD">USD/CAD</option><option value="USD/CHF">USD/CHF</option> <option value="USD/CLP">USD/CLP</option><option value="USD/CNH">USD/CNH</option><option value="USD/CNY">USD/CNY</option> <option value="USD/COP">USD/COP</option><option value="USD/CRC">USD/CRC</option><option value="USD/CZK">USD/CZK</option> <option value="USD/DJF">USD/DJF</option><option value="USD/DKK">USD/DKK</option><option value="USD/DOP">USD/DOP</option> <option value="USD/EGP">USD/EGP</option><option value="USD/ETB">USD/ETB</option><option value="USD/EUR">USD/EUR</option> <option value="USD/GBP">USD/GBP</option><option value="USD/GMD">USD/GMD</option><option value="USD/GTQ">USD/GTQ</option> <option value="USD/HKD">USD/HKD</option><option value="USD/HNL">USD/HNL</option><option value="USD/HRK">USD/HRK</option> <option value="USD/HTG">USD/HTG</option><option value="USD/HUF">USD/HUF</option><option value="USD/IDR">USD/IDR</option> <option value="USD/ILS">USD/ILS</option><option value="USD/INR">USD/INR</option><option value="USD/ISK">USD/ISK</option> <option value="USD/JMD">USD/JMD</option><option value="USD/JOD">USD/JOD</option><option value="USD/JPY">USD/JPY</option> <option value="USD/KES">USD/KES</option><option value="USD/KRW">USD/KRW</option><option value="USD/KWD">USD/KWD</option> <option value="USD/KZT">USD/KZT</option><option value="USD/LBP">USD/LBP</option><option value="USD/LKR">USD/LKR</option> <option value="USD/LSL">USD/LSL</option><option value="USD/LTL">USD/LTL</option><option value="USD/LVL">USD/LVL</option> <option value="USD/MAD">USD/MAD</option><option value="USD/MRO">USD/MRO</option><option value="USD/MUR">USD/MUR</option> <option value="USD/MWK">USD/MWK</option><option value="USD/MXN">USD/MXN</option><option value="USD/MYR">USD/MYR</option> <option value="USD/NAD">USD/NAD</option><option value="USD/NGN">USD/NGN</option><option value="USD/NIO">USD/NIO</option> <option value="USD/NOK">USD/NOK</option><option value="USD/NPR">USD/NPR</option><option value="USD/NZD">USD/NZD</option> <option value="USD/OM1">USD/OM1</option><option value="USD/OMR">USD/OMR</option><option value="USD/PEN">USD/PEN</option> <option value="USD/PHP">USD/PHP</option><option value="USD/PKR">USD/PKR</option><option value="USD/PLN">USD/PLN</option> <option value="USD/PYG">USD/PYG</option><option value="USD/QA1">USD/QA1</option><option value="USD/QAR">USD/QAR</option> <option value="USD/RON">USD/RON</option><option value="USD/RSD">USD/RSD</option><option value="USD/RU0">USD/RU0</option> <option value="USD/RUB">USD/RUB</option><option value="USD/RUX">USD/RUX</option><option value="USD/SA1">USD/SA1</option> <option value="USD/SAR">USD/SAR</option><option value="USD/SBD">USD/SBD</option><option value="USD/SEK">USD/SEK</option> <option value="USD/SGD">USD/SGD</option><option value="USD/SZL">USD/SZL</option><option value="USD/THB">USD/THB</option> <option value="USD/TND">USD/TND</option><option value="USD/TRL">USD/TRL</option><option value="USD/TRY">USD/TRY</option> <option value="USD/TTD">USD/TTD</option><option value="USD/TWD">USD/TWD</option><option value="USD/TZS">USD/TZS</option> <option value="USD/UGX">USD/UGX</option><option value="USD/UYU">USD/UYU</option><option value="USD/VND">USD/VND</option> <option value="USD/VUV">USD/VUV</option><option value="USD/XAF">USD/XAF</option><option value="USD/ZAR">USD/ZAR</option> <option value="USD/ZMK">USD/ZMK</option><option value="XAG/AUD">XAG/AUD</option><option value="XAG/CAD">XAG/CAD</option> <option value="XAG/CHF">XAG/CHF</option><option value="XAG/EUR">XAG/EUR</option><option value="XAG/GBP">XAG/GBP</option> <option value="XAG/USD">XAG/USD</option><option value="XAU/AUD">XAU/AUD</option><option value="XAU/CAD">XAU/CAD</option> <option value="XAU/CHF">XAU/CHF</option><option value="XAU/EUR">XAU/EUR</option><option value="XAU/GBP">XAU/GBP</option> <option value="XAU/TRY">XAU/TRY</option><option value="XAU/USD">XAU/USD</option><option value="XPD/USD">XPD/USD</option> <option value="XPT/EUR">XPT/EUR</option><option value="XPT/USD">XPT/USD</option><option value="ZAR/CHF">ZAR/CHF</option> <option value="ZAR/EUR">ZAR/EUR</option><option value="ZAR/JPY">ZAR/JPY</option><option value="ZAR/USD">ZAR/USD</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="timeinmillis">At Specific Time</label>
                                    <input type="text" id="timeinmillis" name="timeinmillis" class="form-control col-xs-4" placeholder="Select Specific Time"/>
                                </div>
                                <div class="col-md-2" style="margin-top: -55px;">
                                    <button type="button" id="doDetailsBtn" name="doDetailsBtn" class="btn btn-primary top-buffer">Get Display Orders At Specific Time</button>
                                </div>
                            </div>
                        </form>		
                    </div>


                    <div class="doDataTablesContainer" style="margin-left: 25px;">
                        <div>
                            <ul class="nav nav-tabs"  id="blotterDOTabs">
                                <li><a href='#blotterDOTabs-5' data-toggle="tab" data-tablename="displayOrderRatesTable">Display Order Snapshot</a></li>
                            </ul>
                            <div class="tab-content table-tab-content">
                                <div class="tab-pane fade in active" id="blotterDOTabs-5">
                                    <div id="displayOrderRatesTableContainer"></div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <%-- end third tab --%>

                <div class="tab-pane fade in-active" id="emscopetab-4">

                    <div class="form-holder">

                        <form class="form-inline" role="form" name="getOrderCollectionForm" method="POST">
                            <div class="row">
                                <div class="form-group col-md-2">
                                    <a href="javascript:ccyPairSelectall();">select all</a>
                                    <a href="javascript:ccyPairUnselectall();">unselect all</a>
                                    <label for="doCcyPair">Currency Pair</label>
                                    <br>
                                    <select multiple="multiple" id="searchCcyPair" <%--class="form-control multiselect" MULTIPLE id="searchCcyPair" name="doCcyPair" data-live-search="true"--%>>
                                        <%-- <option value="EUR/USD">EUR/USD</option> <option value="AED/BHD">AED/BHD</option> <option value="AED/CHF">AED/CHF</option>
                                    <option value="AED/EUR">AED/EUR</option> <option value="AED/GBP">AED/GBP</option> <option value="AED/JPY">AED/JPY</option>
                                    <option value="AED/KWD">AED/KWD</option> <option value="AED/OMR">AED/OMR</option> <option value="AED/QAR">AED/QAR</option>
                                    <option value="AED/SAR">AED/SAR</option> <option value="AED/USD">AED/USD</option> <option value="AED/ZAR">AED/ZAR</option>
                                    <option value="AUD/AED">AUD/AED</option> <option value="AUD/CAD">AUD/CAD</option> <option value="AUD/CHF">AUD/CHF</option>
                                    <option value="AUD/CNH">AUD/CNH</option> <option value="AUD/DKK">AUD/DKK</option> <option value="AUD/EUR">AUD/EUR</option>
                                    <option value="AUD/GBP">AUD/GBP</option> <option value="AUD/HKD">AUD/HKD</option> <option value="AUD/IDR">AUD/IDR</option>
                                    <option value="AUD/JPY">AUD/JPY</option> <option value="AUD/KWD">AUD/KWD</option> <option value="AUD/MXN">AUD/MXN</option>
                                    <option value="AUD/NOK">AUD/NOK</option> <option value="AUD/NZD">AUD/NZD</option> <option value="AUD/RUB">AUD/RUB</option>
                                    <option value="AUD/SAR">AUD/SAR</option> <option value="AUD/SEK">AUD/SEK</option> <option value="AUD/SGD">AUD/SGD</option>
                                    <option value="AUD/THB">AUD/THB</option> <option value="AUD/USD">AUD/USD</option> <option value="AUD/ZAR">AUD/ZAR</option>
                                    <option value="BHD/AED">BHD/AED</option> <option value="BRL/USD">BRL/USD</option>
                                    <option value="CAD/AED">CAD/AED</option> <option value="CAD/AUD">CAD/AUD</option>
                                    <option value="CAD/CHF">CAD/CHF</option> <option value="CAD/CZK">CAD/CZK</option> <option value="CAD/DKK">CAD/DKK</option>
                                    <option value="CAD/EUR">CAD/EUR</option> <option value="CAD/GBP">CAD/GBP</option> <option value="CAD/HKD">CAD/HKD</option>
                                    <option value="CAD/JPY">CAD/JPY</option> <option value="CAD/MXN">CAD/MXN</option> <option value="CAD/NOK">CAD/NOK</option>
                                    <option value="CAD/PLN">CAD/PLN</option> <option value="CAD/RUB">CAD/RUB</option> <option value="CAD/SAR">CAD/SAR</option>
                                    <option value="CAD/SEK">CAD/SEK</option> <option value="CAD/SGD">CAD/SGD</option> <option value="CAD/THB">CAD/THB</option>
                                    <option value="CAD/USD">CAD/USD</option> <option value="CAD/ZAR">CAD/ZAR</option> <option value="CHF/AED">CHF/AED</option>
                                    <option value="CHF/CAD">CHF/CAD</option> <option value="CHF/CNH">CHF/CNH</option> <option value="CHF/CZK">CHF/CZK</option>
                                    <option value="CHF/DKK">CHF/DKK</option> <option value="CHF/EUR">CHF/EUR</option> <option value="CHF/HKD">CHF/HKD</option>
                                    <option value="CHF/HUF">CHF/HUF</option> <option value="CHF/JPY">CHF/JPY</option> <option value="CHF/KES">CHF/KES</option>
                                    <option value="CHF/MXN">CHF/MXN</option> <option value="CHF/NOK">CHF/NOK</option> <option value="CHF/PLN">CHF/PLN</option>
                                    <option value="CHF/RON">CHF/RON</option> <option value="CHF/RUB">CHF/RUB</option> <option value="CHF/SAR">CHF/SAR</option>
                                    <option value="CHF/SEK">CHF/SEK</option> <option value="CHF/SGD">CHF/SGD</option> <option value="CHF/THB">CHF/THB</option>
                                    <option value="CHF/TRL">CHF/TRL</option> <option value="CHF/TRY">CHF/TRY</option> <option value="CHF/USD">CHF/USD</option>
                                    <option value="CHF/ZAR">CHF/ZAR</option> <option value="CNH/JPY">CNH/JPY</option>
                                    <option value="CNH/RUB">CNH/RUB</option> <option value="CZK/CHF">CZK/CHF</option> <option value="CZK/JPY">CZK/JPY</option>--%>
                                        <%--<option value="CZK/SEK">CZK/SEK</option> <option value="DKK/CHF">DKK/CHF</option> <option value="DKK/CZK">DKK/CZK</option> <option value="DKK/EUR">DKK/EUR</option> <option value="DKK/JPY">DKK/JPY</option> <option value="DKK/NOK">DKK/NOK</option> <option value="DKK/SEK">DKK/SEK</option> <option value="DKK/USD">DKK/USD</option> <option value="EUR/AED">EUR/AED</option> <option value="EUR/AUD">EUR/AUD</option> <option value="EUR/BGN">EUR/BGN</option> <option value="EUR/CAD">EUR/CAD</option> <option value="EUR/CHF">EUR/CHF</option> <option value="EUR/CLP">EUR/CLP</option> <option value="EUR/CNH">EUR/CNH</option> <option value="EUR/CNY">EUR/CNY</option> <option value="EUR/CZK">EUR/CZK</option> <option value="EUR/DKK">EUR/DKK</option> <option value="EUR/GBP">EUR/GBP</option> <option value="EUR/HKD">EUR/HKD</option> <option value="EUR/HRK">EUR/HRK</option> <option value="EUR/HUF">EUR/HUF</option> <option value="EUR/ILS">EUR/ILS</option> <option value="EUR/INR">EUR/INR</option> <option value="EUR/ISK">EUR/ISK</option> <option value="EUR/JPY">EUR/JPY</option> <option value="EUR/KWD">EUR/KWD</option> <option value="EUR/LVL">EUR/LVL</option> <option value="EUR/MXN">EUR/MXN</option> <option value="EUR/NOK">EUR/NOK</option> <option value="EUR/NZD">EUR/NZD</option> <option value="EUR/PLN">EUR/PLN</option> <option value="EUR/RON">EUR/RON</option><option value="EUR/RSD">EUR/RSD</option><option value="EUR/RU0">EUR/RU0</option> <option value="EUR/RUB">EUR/RUB</option><option value="EUR/SAR">EUR/SAR</option><option value="EUR/SEK">EUR/SEK</option> <option value="EUR/SGD">EUR/SGD</option><option value="EUR/THB">EUR/THB</option><option value="EUR/TRL">EUR/TRL</option> <option value="EUR/TRY">EUR/TRY</option><option value="EUR/ZAR">EUR/ZAR</option> <option value="GBP/AED">GBP/AED</option><option value="GBP/AUD">GBP/AUD</option><option value="GBP/BGN">GBP/BGN</option> <option value="GBP/BHD">GBP/BHD</option><option value="GBP/CAD">GBP/CAD</option><option value="GBP/CHF">GBP/CHF</option> <option value="GBP/CLP">GBP/CLP</option><option value="GBP/CZK">GBP/CZK</option><option value="GBP/DKK">GBP/DKK</option> <option value="GBP/EUR">GBP/EUR</option><option value="GBP/HKD">GBP/HKD</option><option value="GBP/HUF">GBP/HUF</option> <option value="GBP/ILS">GBP/ILS</option><option value="GBP/INR">GBP/INR</option><option value="GBP/JPY">GBP/JPY</option> <option value="GBP/MUR">GBP/MUR</option><option value="GBP/MXN">GBP/MXN</option><option value="GBP/NOK">GBP/NOK</option> <option value="GBP/NZD">GBP/NZD</option><option value="GBP/OMR">GBP/OMR</option><option value="GBP/PHP">GBP/PHP</option> <option value="GBP/PLN">GBP/PLN</option><option value="GBP/QAR">GBP/QAR</option><option value="GBP/RUB">GBP/RUB</option> <option value="GBP/SAR">GBP/SAR</option><option value="GBP/SEK">GBP/SEK</option><option value="GBP/SGD">GBP/SGD</option> <option value="GBP/THB">GBP/THB</option><option value="GBP/TRL">GBP/TRL</option><option value="GBP/TRY">GBP/TRY</option> <option value="GBP/TWD">GBP/TWD</option><option value="GBP/USD">GBP/USD</option><option value="GBP/ZAR">GBP/ZAR</option> <option value="HKD/CNH">HKD/CNH</option><option value="HKD/EUR">HKD/EUR</option><option value="HKD/JPY">HKD/JPY</option> <option value="HKD/NOK">HKD/NOK</option><option value="HKD/THB">HKD/THB</option><option value="HKD/USD">HKD/USD</option> <option value="HUF/EUR">HUF/EUR</option><option value="ILS/USD">ILS/USD</option><option value="INR/CHF">INR/CHF</option> <option value="ISK/CHF">ISK/CHF</option><option value="JPY/AED">JPY/AED</option><option value="JPY/BGN">JPY/BGN</option> <option value="JPY/CHF">JPY/CHF</option><option value="JPY/EUR">JPY/EUR</option><option value="JPY/HKD">JPY/HKD</option> <option value="JPY/KRW">JPY/KRW</option><option value="JPY/MYR">JPY/MYR</option><option value="JPY/SGD">JPY/SGD</option> <option value="JPY/TRY">JPY/TRY</option><option value="JPY/USD">JPY/USD</option><option value="KWD/AED">KWD/AED</option> <option value="KWD/BHD">KWD/BHD</option><option value="KWD/CAD">KWD/CAD</option><option value="MXN/JPY">MXN/JPY</option> <option value="MXN/SEK">MXN/SEK</option><option value="MXN/USD">MXN/USD</option><option value="NOK/CAD">NOK/CAD</option> <option value="NOK/CHF">NOK/CHF</option><option value="NOK/CZK">NOK/CZK</option><option value="NOK/DKK">NOK/DKK</option> <option value="NOK/GBP">NOK/GBP</option><option value="NOK/HKD">NOK/HKD</option><option value="NOK/JPY">NOK/JPY</option> <option value="NOK/NZD">NOK/NZD</option><option value="NOK/SEK">NOK/SEK</option><option value="NOK/USD">NOK/USD</option> <option value="NZD/AED">NZD/AED</option><option value="NZD/AUD">NZD/AUD</option><option value="NZD/CAD">NZD/CAD</option> <option value="NZD/CHF">NZD/CHF</option><option value="NZD/DKK">NZD/DKK</option><option value="NZD/GBP">NZD/GBP</option> <option value="NZD/HKD">NZD/HKD</option><option value="NZD/JPY">NZD/JPY</option><option value="NZD/MYR">NZD/MYR</option> <option value="NZD/NOK">NZD/NOK</option><option value="NZD/SAR">NZD/SAR</option><option value="NZD/SEK">NZD/SEK</option> <option value="NZD/SGD">NZD/SGD</option><option value="NZD/USD">NZD/USD</option><option value="OMR/AED">OMR/AED</option> <option value="PLN/CHF">PLN/CHF</option><option value="PLN/CZK">PLN/CZK</option><option value="PLN/DKK">PLN/DKK</option> <option value="PLN/HUF">PLN/HUF</option><option value="PLN/JPY">PLN/JPY</option><option value="PLN/NOK">PLN/NOK</option> <option value="PLN/SEK">PLN/SEK</option><option value="QAR/AED">QAR/AED</option><option value="RUB/BGN">RUB/BGN</option> <option value="RUB/JPY">RUB/JPY</option><option value="SAR/AED">SAR/AED</option><option value="SAR/CHF">SAR/CHF</option> <option value="SAR/JPY">SAR/JPY</option><option value="SAR/SEK">SAR/SEK</option><option value="SAR/USD">SAR/USD</option> <option value="SEK/BGN">SEK/BGN</option><option value="SEK/CHF">SEK/CHF</option><option value="SEK/DKK">SEK/DKK</option> <option value="SEK/EUR">SEK/EUR</option><option value="SEK/GBP">SEK/GBP</option><option value="SEK/JPY">SEK/JPY</option> <option value="SEK/PLN">SEK/PLN</option><option value="SEK/RUB">SEK/RUB</option><option value="SEK/THB">SEK/THB</option> <option value="SEK/TRY">SEK/TRY</option><option value="SEK/USD">SEK/USD</option><option value="SGD/AED">SGD/AED</option> <option value="SGD/AUD">SGD/AUD</option><option value="SGD/CHF">SGD/CHF</option><option value="SGD/CNH">SGD/CNH</option> <option value="SGD/GBP">SGD/GBP</option><option value="SGD/HKD">SGD/HKD</option><option value="SGD/JPY">SGD/JPY</option> <option value="SGD/NOK">SGD/NOK</option><option value="SGD/NZD">SGD/NZD</option><option value="SGD/PLN">SGD/PLN</option> <option value="SGD/THB">SGD/THB</option><option value="SGD/USD">SGD/USD</option><option value="THB/JPY">THB/JPY</option> <option value="THB/USD">THB/USD</option><option value="TRY/JPY">TRY/JPY</option><option value="TRY/PLN">TRY/PLN</option> <option value="TRY/USD">TRY/USD</option><option value="USD/AE0">USD/AE0</option><option value="USD/AE1">USD/AE1</option> <option value="USD/AED">USD/AED</option><option value="USD/ARS">USD/ARS</option><option value="USD/AUD">USD/AUD</option> <option value="USD/BBD">USD/BBD</option><option value="USD/BDT">USD/BDT</option><option value="USD/BGN">USD/BGN</option> <option value="USD/BH1">USD/BH1</option><option value="USD/BHD">USD/BHD</option><option value="USD/BIF">USD/BIF</option> <option value="USD/BND">USD/BND</option><option value="USD/BOB">USD/BOB</option><option value="USD/BRL">USD/BRL</option> <option value="USD/BYR">USD/BYR</option><option value="USD/CAD">USD/CAD</option><option value="USD/CHF">USD/CHF</option> <option value="USD/CLP">USD/CLP</option><option value="USD/CNH">USD/CNH</option><option value="USD/CNY">USD/CNY</option> <option value="USD/COP">USD/COP</option><option value="USD/CRC">USD/CRC</option><option value="USD/CZK">USD/CZK</option> <option value="USD/DJF">USD/DJF</option><option value="USD/DKK">USD/DKK</option><option value="USD/DOP">USD/DOP</option> <option value="USD/EGP">USD/EGP</option><option value="USD/ETB">USD/ETB</option><option value="USD/EUR">USD/EUR</option> <option value="USD/GBP">USD/GBP</option><option value="USD/GMD">USD/GMD</option><option value="USD/GTQ">USD/GTQ</option> <option value="USD/HKD">USD/HKD</option><option value="USD/HNL">USD/HNL</option><option value="USD/HRK">USD/HRK</option> <option value="USD/HTG">USD/HTG</option><option value="USD/HUF">USD/HUF</option><option value="USD/IDR">USD/IDR</option> <option value="USD/ILS">USD/ILS</option><option value="USD/INR">USD/INR</option><option value="USD/ISK">USD/ISK</option> <option value="USD/JMD">USD/JMD</option><option value="USD/JOD">USD/JOD</option><option value="USD/JPY">USD/JPY</option> <option value="USD/KES">USD/KES</option><option value="USD/KRW">USD/KRW</option><option value="USD/KWD">USD/KWD</option> <option value="USD/KZT">USD/KZT</option><option value="USD/LBP">USD/LBP</option><option value="USD/LKR">USD/LKR</option> <option value="USD/LSL">USD/LSL</option><option value="USD/LTL">USD/LTL</option><option value="USD/LVL">USD/LVL</option> <option value="USD/MAD">USD/MAD</option><option value="USD/MRO">USD/MRO</option><option value="USD/MUR">USD/MUR</option> <option value="USD/MWK">USD/MWK</option><option value="USD/MXN">USD/MXN</option><option value="USD/MYR">USD/MYR</option> <option value="USD/NAD">USD/NAD</option><option value="USD/NGN">USD/NGN</option><option value="USD/NIO">USD/NIO</option> <option value="USD/NOK">USD/NOK</option><option value="USD/NPR">USD/NPR</option><option value="USD/NZD">USD/NZD</option> <option value="USD/OM1">USD/OM1</option><option value="USD/OMR">USD/OMR</option><option value="USD/PEN">USD/PEN</option> <option value="USD/PHP">USD/PHP</option><option value="USD/PKR">USD/PKR</option><option value="USD/PLN">USD/PLN</option> <option value="USD/PYG">USD/PYG</option><option value="USD/QA1">USD/QA1</option><option value="USD/QAR">USD/QAR</option> <option value="USD/RON">USD/RON</option><option value="USD/RSD">USD/RSD</option><option value="USD/RU0">USD/RU0</option> <option value="USD/RUB">USD/RUB</option><option value="USD/RUX">USD/RUX</option><option value="USD/SA1">USD/SA1</option> <option value="USD/SAR">USD/SAR</option><option value="USD/SBD">USD/SBD</option><option value="USD/SEK">USD/SEK</option> <option value="USD/SGD">USD/SGD</option><option value="USD/SZL">USD/SZL</option><option value="USD/THB">USD/THB</option> <option value="USD/TND">USD/TND</option><option value="USD/TRL">USD/TRL</option><option value="USD/TRY">USD/TRY</option> <option value="USD/TTD">USD/TTD</option><option value="USD/TWD">USD/TWD</option><option value="USD/TZS">USD/TZS</option> <option value="USD/UGX">USD/UGX</option><option value="USD/UYU">USD/UYU</option><option value="USD/VND">USD/VND</option> <option value="USD/VUV">USD/VUV</option><option value="USD/XAF">USD/XAF</option><option value="USD/ZAR">USD/ZAR</option> <option value="USD/ZMK">USD/ZMK</option><option value="XAG/AUD">XAG/AUD</option><option value="XAG/CAD">XAG/CAD</option> <option value="XAG/CHF">XAG/CHF</option><option value="XAG/EUR">XAG/EUR</option><option value="XAG/GBP">XAG/GBP</option> <option value="XAG/USD">XAG/USD</option><option value="XAU/AUD">XAU/AUD</option><option value="XAU/CAD">XAU/CAD</option> <option value="XAU/CHF">XAU/CHF</option><option value="XAU/EUR">XAU/EUR</option><option value="XAU/GBP">XAU/GBP</option> <option value="XAU/TRY">XAU/TRY</option><option value="XAU/USD">XAU/USD</option><option value="XPD/USD">XPD/USD</option> <option value="XPT/EUR">XPT/EUR</option><option value="XPT/USD">XPT/USD</option><option value="ZAR/CHF">ZAR/CHF</option> <option value="ZAR/EUR">ZAR/EUR</option><option value="ZAR/JPY">ZAR/JPY</option><option value="ZAR/USD">ZAR/USD</option>
                                        --%>                                </select>
                                    <br>
                                    <label for="searchDate">Date</label>
                                    <br>
                                    <select multiple="multiple" id="searchDate"<%-- multiple="multiple"--%> <%--class="form-control multiselect" MULTIPLE id="searchDate" name="doSearchDate" data-live-search="true"--%>>
                                        <%--<option value="8/1/2014">8/1/2014</option> <option value="7/31/2014">7/31/2014</option>
                                    <option value="7/30/2014">7/30/2014</option>
                                    <option value="7/29/2014">7/29/2014</option> <option value="7/28/2014">7/28/2014</option> <option value="7/27/2014">7/27/2014</option>
                                    <option value="7/26/2014">7/26/2014</option> <option value="7/25/2014">7/25/2014</option> <option value="7/24/2014">7/24/2014</option>
                                    <option value="7/23/2014">7/23/2014</option> <option value="7/22/2014">7/22/2014</option>
                                    <option value="7/21/2014">7/21/2014</option> <option value="7/20/2014">7/20/2014</option>--%>
                                    </select>
                                    <br>
                                    <a href="javascript:orderDateSelectall();">select all</a>
                                    <a href="javascript:orderDateUnselectall();">unselect all</a>
                                </div>
                                <div class="form-group col-md-2">
                                    <a href="javascript:takerOrgSelectall();">select all</a>
                                    <a href="javascript:takerOrgUnselectall();">unselect all</a>
                                    <label for="searchTakerOrg">takerOrg</label>
                                    <br>
                                    <select multiple="multiple" id="searchTakerOrg" <%--class="form-control multiselect" MULTIPLE id="searchTakerOrg" name="doSearchTakerOrg" data-live-search="true->"--%>>
                                        <%-- <option value="ISYAT">ISYAT</option> <option value="MIGB">MIGB</option> <option value="INTL">INTL</option> <option value="FXPRO">FXPRO</option>
                                    <option value="RBIV">RBIV</option> <option value="AXICORP">AXICORP</option> <option value="CITIP">CITIP</option>
                                    <option value="SWED">SWED</option> <option value="PSBF">PSBF</option> <option value="EDGE">EDGE</option>
                                    <option value="ErsteBank">ErsteBank</option> <option value="VAF">VAF</option> <option value="DIBRE">DIBRE</option> <option value="NBAD">NBAD</option>--%>
                                    </select>
                                    <br>
                                    <label for="searchBuySell">Buy/Sell</label>
                                    <br>
                                    <select multiple="multiple" id="searchBuySell" <%--class="form-control multiselect" MULTIPLE id="searchBuySell" name="doSearchBuySell" data-live-search="true"--%>>
                                        <option value="Buy">Buy</option> <option value="Sell">Sell</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="searchStatus">Status</label>
                                    <br>
                                    <select multiple="multiple" id="searchStatus" <%--class="form-control multiselect" MULTIPLE id="searchStatus" name="doSearchDate" data-live-search="true"--%>>
                                        <option value="C">C</option> <option value="F">F</option>
                                        <option value="I">I</option>
                                        <option value="J">J</option> <option value="P">P</option> <option value="R">R</option>
                                    </select>
                                    <br>
                                    <br>
                                    <label for="searchOrigOrg">origOrg</label>
                                    <br>
                                    <select  multiple="multiple" id="searchOrigOrg" <%--class="form-control multiselect" MULTIPLE id="searchOrigOrg" name="doSearchOrigOrg" data-live-search="true"--%>>
                                        <%--  <option value="19815LCGIX">19815LCGIX</option> <option value="19817LCGGUI">19817LCGGUI</option> <option value="227389WFNA">227389WFNA</option>
                                   <option value="249032WFNA">249032WFNA</option>
                                   <option value="261493WFNA">261493WFNA</option> <option value="ALGOGMTL">ALGOGMTL</option> <option value="ALIORPT">ALIORPT</option>
                                   <option value="APICust12">APICust12</option> <option value="BBGPSBF">BBGPSBF</option> <option value="BBVANY">BBVANY</option>
                                   <option value="BELV">BELV</option> <option value="BGAZBLTC">BGAZBLTC</option> <option value="DIBRE">DIBRE</option> <option value="NBAD">NBAD</option>--%>
                                    </select>
                                    <a href="javascript:origOrgSelectall();">select all</a>
                                    <a href="javascript:origOrgUnselectall();">unselect all</a>
                                </div>
                                <div class="form-group col-md-2">

                                    <label for="searchOrderType">Order Type</label>
                                    <select id="searchOrderType" multiple="multiple" <%--class="form-control multiselect" MULTIPLE id="searchOrderType" name="doSearchOrderType" data-live-search="true"--%>>
                                        <option value="LIMIT">LIMIT</option> <option value="MARKET">MARKET</option>
                                        <option value="LIMIT/OCO">LIMIT/OCO</option>
                                        <option value="LIMIT/OUO (Absolute)">LIMIT/OUO (Absolute)</option> <option value="LIMIT/OUO (Proportional)">LIMIT/OUO (Proportional)</option>
                                        <option value="PQ">PQ</option>
                                        <option value="RFQ">RFQ</option>
                                        <option value="STOP">RFQ</option>
                                        <option value="STOP/OCO">STOP/OCO</option>
                                        <option value="STOP/OTO">STOP/OTO</option>
                                        <option value="STOP/OUO (Absolute)">STOP/OUO (Absolute)</option>
                                        <option value="STOP/OUO (Proportional)">STOP/OUO (Proportional)</option>
                                        <option value="STOPLIMIT">STOPLIMIT</option>
                                        <option value="STOPMARKETRANGE">STOPMARKETRANGE</option>
                                    </select>
                                    <br>
                                    <label for="searchChannel">Channel</label>
                                    <select multiple="multiple" id="searchChannel" <%--class="form-control multiselect" MULTIPLE id="searchChannel" name="doSearchChannel" data-live-search="true"--%>>
                                        <option value="BA/ESP">BA/ESP</option> <option value="BA/ESP/FM">BA/ESP/FM</option> <option value="BA/RFS">BA/RFS</option>
                                        <option value="DNET/ENA/FM">DNET/ENA/FM</option>
                                        <option value="DNET/EPA/FM">DNET/EPA/FM</option>
                                        <option value="DNET/ESP/FXID/DLP">DNET/ESP/FXID/DLP</option>
                                        <option value="DNET/ESP/FXID/FMA">DNET/ESP/FXID/FMA</option>
                                        <option value="DNET/ESP/FXID/FXB">DNET/ESP/FXID/FXB</option>
                                        <option value="DNET/ESP/FXID/OP">DNET/ESP/FXID/OP</option>
                                        <option value="DNET/ESP/SDP">DNET/ESP/SDP</option>
                                        <option value="DNET/INFXSEF/FB">DNET/INFXSEF/FB</option>
                                        <option value="DNET/OB/FM">DNET/OB/FM</option>
                                        <option value="DNET/OS">DNET/OS</option>
                                        <option value="DNET/PD">DNET/PD</option>


                                        <option value="DNET/PM">DNET/PM</option> <option value="DNET/PP">DNET/PP</option> <option value="DNET/PT">DNET/PT</option>
                                        <option value="DNET/RFS/PD">DNET/RFS/PD</option>
                                        <option value="DNET/RFS/PP">DNET/RFS/PP</option> <option value="DNET/RFS/SDP">DNET/RFS/SDP</option>

                                        <option value="FIX/ESP">FIX/ESP</option>
                                        <option value="FIX/Order">FIX/Order</option> <option value="FIX/Order/FXGO">FIX/Order/FXGO</option>
                                        <option value="FIX/Order/HostedMT4">FIX/Order/HostedMT4</option>
                                        <option value="FIX/Order/INTMT4">FIX/Order/INTMT4</option> <option value="FIX/Order/MT4">FIX/Order/MT4</option>
                                        <option value="FIX/RFS">FIX/RFS</option>
                                        <option value="FIX/RFS/360T">FIX/RFS/360T</option>


                                        <option value="FIX/RFS/FXALL">FIX/RFS/FXALL</option>
                                        <option value="FIX/RFS/FXGO">FIX/Order/MT4</option>
                                        <option value="FIX/RFS">FIX/RFS</option>
                                        <option value="FIX/RFS/360T">FIX/RFS/360T</option>


                                        <option value="HTML/ESP/FXB">HTML/ESP/FXB</option>
                                        <option value="HTML/ESP/FXFB">HTML/ESP/FXFB</option>
                                        <option value="HTML/ESP/FXID/DLP">HTML/ESP/FXID/DLP</option>
                                        <option value="HTML/ESP/FXID/OP">HTML/ESP/FXID/OP</option>

                                        <option value="HTML/ESP/FXL">HTML/ESP/FXL</option>
                                        <option value="HTML/ESP/IFX">HTML/ESP/IFX</option>
                                        <option value="HTML/ESP/OP">HTML/ESP/OP</option>
                                        <option value="HTML/ESP/PB">HTML/ESP/PB</option>

                                        <option value="HTML/RFS/FB">HTML/RFS/FB</option>
                                        <option value="HTML/RFS/IFX">HTML/RFS/IFX</option>
                                        <option value="HTML/RFS/TOB">HTML/RFS/TOB</option>
                                        <option value="iOS/ESP/FB">iOS/ESP/FB</option>

                                        <option value="iOS/ESP/FXB">iOS/ESP/FXB</option>
                                        <option value="PNET/ENA/FM">PNET/ENA/FM</option>
                                        <option value="PNET/EPA/FM">PNET/EPA/FM</option>
                                        <option value="PNET/OB/FM">PNET/OB/FM</option>


                                        <option value="PNET/OP/FM">PNET/OP/FM</option>
                                        <option value="PNET/OS">PNET/OS</option>
                                        <option value="PNET/PP">PNET/PP</option>
                                        <option value="PNET/PT">PNET/PT</option>
                                        <option value="PNET/RFS/PP">PNET/RFS/PP</option>

                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="searchTif">TIF</label>
                                    <br>
                                    <select multiple="multiple" id="searchTif" <%--class="form-control multiselect" MULTIPLE id="searchTif" name="dosearchTif" data-live-search="true"--%>>
                                        <option value="TIF">TIF</option> <option value="DAY">DAY</option>
                                        <option value="FOK">FOK</option>
                                        <option value="GTC">GTC</option> <option value="GTD">GTD</option> <option value="IOC">IOC</option>
                                    </select>
                                    <br>
                                    <label for="searchOrderBy">Order By Columns</label>
                                    <br>
                                    <select multiple="multiple" id="searchOrderBy"<%--class="form-control multiselect" MULTIPLE id="searchOrderBy" name="dosearchOrderBy" data-live-search="true"--%>>
                                        <option value="CCYPAIR">Currency Pair</option>
                                        <option value="ORG">Taker Org</option>
                                        <option value="STATUS">Status</option>
                                        <option value="ORDERTYPE">Order Type</option>
                                        <option value="TIMEINFORCE">TIF</option>
                                        <option value="CREATED">Order Date</option>
                                        <option value="BUYSELL">Buy/Sell</option>
                                        <option value="ORIGINATINGORG">Originating Org</option>
                                        <option value="CHANNEL">Channel</option>
                                        <option value="ORDERSTATE">Order State</option>
                                        <option value="ORDERAMTUSD">Order Amount USD</option>
                                    </select>

                                </div>

                                <div class="form-group col-md-2">
                                    <button type="submit" class="btn btn-primary top-buffer">Get Orders</button>
                                    <button type="submit" id="doOrderSummarySreach" name="doOrderSummarySreach" class="btn btn-primary top-buffer">Get Order Summary View</button>
                                </div>
                                <%-- <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary top-buffer">Get Orders</button>
                                </div>--%>
                            </div>
                        </form>
                    </div>

                    <div id="rowtext" >
                    </div>
                    <div class="container doDataTablesContainer" id="orderCollectionTableContainer1" style="visibility: hidden;">
                        <div class="top-buffer">
                            <ul class="nav nav-tabs"  id="blotterOrderTabs">
                                <li><a href='#blotterDOTabs-5' data-toggle="tab" data-tablename="orderCollectionTable">Order Set</a></li>
                            </ul>
                            <div class="tab-content table-tab-content">
                                <div class="tab-pane fade in active" id="blotterOrderTabs-5">
                                    <div id="orderCollectionTableContainer"></div>
                                </div>
                            </div>
                            <div id="buttondiv" style="visibility: hidden;">
                                <span>
                                    <input type="button" value="<<" onclick="javascript:prev();" name="Previous"/>
                                </span>
                                <span >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <span>
                                    <input type="button" value=">>" onclick="javascript:next();" name="Next"/>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="container doDataTablesContainer" style="visibility: hidden;">
                        <div class="top-buffer">
                            <ul class="nav nav-tabs"  id="orderStatisticsTabs">
                                <li><a href='#blotterDOTabs-5' data-toggle="tab" data-tablename="orderStatisticsTable">Order Statistics</a></li>
                            </ul>
                            <div class="tab-content table-tab-content">
                                <div class="tab-pane fade in active" id="blotterOrderTabs-6">
                                    <div id="orderStatisticsContainer"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container doDataTablesContainer" id="orderStatisticsCCyTableContainer" style="visibility: hidden;">

                        <div class="top-buffer">
                            <ul class="nav nav-tabs"  id="orderStatisticsCCyTabs">
                                <li><a href='#blotterDOTabs-5' data-toggle="tab" data-tablename="orderStatisticsCCyTable">Volume-Ccy Pair (Top 5) </a></li>
                            </ul>
                            <div class="tab-content table-tab-content">
                                <div class="tab-pane fade in active" id="blotterOrderTabs-7">
                                    <div id="orderStatisticsCCyContainer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container doDataTablesContainer" id="orderStatisticsCptyTableContainer" style="visibility: hidden;">
                        <div class="top-buffer">
                            <ul class="nav nav-tabs"  id="orderStatisticsCptyTabs">
                                <li><a href='#blotterDOTabs-5' data-toggle="tab" data-tablename="orderStatisticsCptyTable">Volume-Cpty  (Top 5)</a></li>
                            </ul>
                            <div class="tab-content table-tab-content">
                                <div class="tab-pane fade in active" id="blotterOrderTabs-8">
                                    <div id="orderStatisticsCptyContainer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <%-- end 5 tab --%>

                <%-- <div class="tab-pane fade in-active" id="emscopetab-5">
                     <div class="container doDataTablesContainer">
                         <div class="top-buffer">
                             <ul class="nav nav-tabs"  id="tradeStatisticsTabs">
                                 <li><a href='#blotterDOTabs-5' data-toggle="tab" data-tablename="tradeStatisticsTable">Trade Statistics</a></li>
                             </ul>
                             <div class="tab-content table-tab-content">
                                 <div class="tab-pane fade in active" id="blotterOrderTabs-6">
                                     <div id="tradeStatisticsContainer"></div>
                                 </div>
                             </div>
                         </div>
                     </div>

                <div class="tab-pane fade in-active" id="emscopetab-5">
                    <div class="container doDataTablesContainer">
                        <div class="top-buffer">
                            <ul class="nav nav-tabs"  id="tradeStatisticsCCyTabs">
                                <li><a href='#blotterDOTabs-5' data-toggle="tab" data-tablename="tradeStatisticsCCyTable">Volume-Ccy Pair (Top 5) </a></li>
                            </ul>
                            <div class="tab-content table-tab-content">
                                <div class="tab-pane fade in active" id="blotterOrderTabs-7">
                                    <div id="tradeStatisticsCCyContainer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="container doDataTablesContainer">
                    <div class="top-buffer">
                        <ul class="nav nav-tabs"  id="tradeStatisticsCptyTabs">
                            <li><a href='#blotterDOTabs-5' data-toggle="tab" data-tablename="tradeStatisticsCptyTable">Volume-Cpty  (Top 5)</a></li>
                        </ul>
                        <div class="tab-content table-tab-content">
                            <div class="tab-pane fade in active" id="blotterOrderTabs-8">
                                <div id="tradeStatisticsCptyContainer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>
                <%-- end third tab --%>
                <%-- start 6 tab --%>
                <div class="tab-pane fade" id="emscopetab-6">
                    <div class="setClassLeft">
                        <div class="page-1 page">
                            <div class="order-form text-center">
                                <form class="form-inline" name="orderIdForm" style="margin-top: 120px;">
                                    <h3>Enter Order ID</h3>
                                    <div class="form-group">
                                        <input type="text" id="orderId" name="orderId" class="form-control col-xs-4" />
                                    </div>
                                    <button type="submit" class="btn btn-primary">Go!</button>
                                    <button  type="button" class="btn btn-success btn-order-tree-view">Order Tree View</button>
                                </form>
                            </div>
                        </div>
                        <div class="page-1_1 page display_none">
                            <div class="moduleContainer streamsContainer text-center">
                                <form class="form-inline" role="form" name="getStreamForm">
                                    <div class="form-group">
                                        <div class="input-group date">
                                            <select id="oprovider" name="oprovider" class="form-control multiselect"  data-live-search="true">
                                                <option value="BOANH/BOANHDefaultStream">BOANH - BOANHDefaultStream</option>
                                                <option value="BOANE/BOANEDefaultStream">BOANE - BOANEDefaultStream</option>
                                                <option value="BOANF/BOANFDefaultStream">BOANF - BOANFDefaultStream</option>
                                                <option value="BOANI/BOANIDefaultStream">BOANI - BOANIDefaultStream</option>
                                                <option value="CITIA/Integral1">CITIA - Integral1</option>
                                                <option value="CRSUB/FXIN_C">CRSUB - FXIN_C</option>
                                                <option value="CRSUC/FXIN">CRSUC - FXIN</option>
                                                <option value="GSFXB/Retail">GSFXB - Retail</option>
                                                <option value="GSFXB/Priority">GSFXB - Priority</option>
                                                <option value="HSBCF/IntGloInst1">HSBCF - IntGloInst1</option>
                                                <option value="JPMA/Stream1">JPMA - Stream1</option>
                                                <option value="JPMB/IntegralBlue">JPMB - IntegralBlue</option>
                                                <option value="JPMA/Platinum">JPMA - Platinum</option>
                                                <option value="MHDPA/StreamMC">MHDPA - StreamMC</option>
                                                <option value="MSFXN/FXIN_M_1">MSFXN - FXIN_M_1</option>
                                                <option value="MSFXO/FXIN_11">MSFXO - FXIN_11</option>
                                                <option value="RBSN/integral6">RBSN - integral6</option>
                                                <option value="RBSO/integral0">RBSO - integral0</option>
                                                <option value="RBSN/integral1a">RBSN - integral1a</option>
                                                <option value="SGA/PREA_1">SGA - PREA_1</option>
                                                <option value="UBSA/FXPRO">UBSA - FXPRO</option>
                                                <option value="UBSA/Super">UBSA - Super</option>
                                                <option value="UBSC/Medium">UBSC - Medium</option>
                                                <option value="UBSB/MIG_NTG">UBSB - MIG_NTG</option>
                                                <option value="UBSA/Premium">UBSA - Premium</option>
                                                <option value="BARXB/FXPRO">BARXB - FXPRO</option>
                                                <option value="BNPPC/GREEN">BNPPC - GREEN</option>
                                                <option value="SGA/STDA_1">SGA - STDA_1</option>
                                                <option value="DB15/EU0196733">DB15 - EU0196733</option>
                                                <option value="VTFXA/VFX0">VTFXA - VFX0</option>
                                                <option value="CHPTA/StreamNY">CHPTA - StreamNY</option>
                                                <option value="NTFXA/stream1">NTFXA - stream1</option>
                                                <option value="ADSSA/Stream1">ADSSA - Stream1</option>
                                                <option value="HSFXE/AXICORP">HSFXE - AXICORP</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" id="doGetStream" class="btn btn-primary">Get Stream</button>
                                    <button type="button" id="doAuditEvent" class="btn btn-primary">Trade Response Received from Provider</button>
                                </form>
                            </div>
                            <div class="top-buffer">
                                <button class="zoomOut btn btn-default">Zoom-out 2 seconds</button>&nbsp;&nbsp;
                                <button class="undoZoom btn btn-default disabled">Undo Zoom</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp
                                <button class="vlscape btn btn-primary">View In Landscape >></button>
                            </div>
                        </div>  
                    </div>                  
                    <div class="pieChartContainer moduleContainer col-md-5 col-lg-5 text-center" style="margin-top: 75px;">
                        <div class="confirmedTrades">
                            <h4 class="heading">Confirmed Trades</h4>
                            <div id="tradeCptyConfirmedChartContainer" style="float: right;"></div>
                        </div>
                        <div class="rejectedTrades">
                            <h4 class="heading">Rejected Trades</h4>
                            <div id="tradeCptyRejectedChartContainer" style="float: right;"></div>
                        </div>
                    </div>
                    <div class="clear"></div>                        

                    <div class="page-2 page">   
                        <div style="float: right; display: inline-block ! important;">
                            <span>Filter Trades By Stream: </span>
                            <input type="checkbox" id="filterByStreamToggle" name="filterByStreamToggle" data-size="mini">
                        </div>                     
                        <div class="orderChartContainer moduleContainer" id="orderChartContainer" style="margin-top: -35px;">
                        </div><div class="clear"></div>
                        <div class="ordzoomChart ordZoomContainer" id="ordzoomChart">
                        </div><div class="clear"></div>
                        <div class="narrativeContainer moduleContainer">
                            <h4 class="heading">Summary</h4>
                            <div class="narrative"></div>
                        </div><div class="clear"></div>
                        <div class="moduleContainer execSummaryContainer">
                            <h4 class="heading">Execution Summary</h4>
                            <div id="execSummary"></div>
                        </div><div class="clear"></div>
                        <div class="orderDetailsContainer moduleContainer row">
                            <h4 class="heading">Order Details</h4>
                            <div class="col-md-6 col-lg-6">
                                <table class="table table-bordered">

                                </table>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <table class="table table-bordered">

                                </table>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="orderTabDTContainer">
                            <div class="top-buffer" style="margin-left:0;">
                                <ul class="nav nav-tabs"  id="orderTabDataTabs">
                                    <li class="active"><a href='#orderTabDataTabs-1' data-toggle="tab" data-tablename="oTTradesTable">Trades</a></li>
                                    <li><a href='#orderTabDataTabs-2' data-toggle="tab" data-tablename="oTBenchmarkTable">Benchmark</a></li>
                                    <li><a href='#orderTabDataTabs-3' data-toggle="tab" data-tablename="oTRatesTable">Rates</a></li>
                                    <li><a href='#orderTabDataTabs-4' data-toggle="tab" data-tablename="oTProfileTable">Stream Profile</a></li>
                                </ul>
                                <div class="tab-content table-tab-content">
                                    <div class="tab-pane fade in active" id="orderTabDataTabs-1">
                                        <div id="oTTradesTableContainer"></div>
                                    </div>
                                    <div class="tab-pane fade" id="orderTabDataTabs-2">
                                        <div id="oTBenchmarkTableContainer"></div>
                                    </div>
                                    <div class="tab-pane fade" id="orderTabDataTabs-3">
                                        <div id="oTRatesTableContainer"></div>
                                    </div>
                                    <div class="tab-pane fade" id="orderTabDataTabs-4">
                                        <div id="oTProfileTableContainer"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Order Tree navigation with expand and collapse
                            @author    Anil    <nl.sainil@gmail.com>  -->
                        <div class="orderNavigateTreeView" style="display:none;margin-top: 62px;">
                            <button><span>O<br/>R<br/>D<br/>E<br/>R<br/><br/>T<br/>R<br/>E<br/>E</span></button>
                            <div></div>
                        </div>
                    </div>
                </div>
                <%-- end 6 tab --%>
            </div>

        </div>
        <div class="perf-console">
            <h6 class="pc-header">
                <span>Performance Console</span>
                <div class="pc-close">
                    X
                </div>
            </h6>
            <div class="messages">

            </div>
        </div>

        <%--<div class="modal" id="myModal">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">\D7</button>
                    <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                    Content for the dialog / modal goes here.
                    </div>
                    </div>
                    </div>
        </div>--%>



        <!-- jQuery JS -->
        <script src="scripts/jquery/1.10.2/jquery.min.js"></script>
        <script src="scripts/bootstrap/plugins/jquery.async.js"></script>
        <!-- Bootstrap JS -->
        <script src="scripts/bootstrap/3.0.2/bootstrap.min.js"></script>

        <!-- Other Libraries -->
        <script src="scripts/bootstrap/plugins/moment.min.js"></script>
        <script src="scripts/bootstrap/plugins/daterangepicker.js"></script>
        <script src="scripts/bootstrap/plugins/datetimepicker.js"></script>
        <script src="scripts/bootstrap/plugins/datatables.js"></script>
        <!--<script src="scripts/bootstrap/plugins/dimple.js"></script>-->
        <script src="scripts/bootstrap/plugins/d3.js"></script>
        <script src="scripts/bootstrap/plugins/crossfilter.js"></script>
        <script src="scripts/bootstrap/plugins/dc.js"></script>
        <script src="scripts/bootstrap/plugins/2D.js"></script>
        <script src="scripts/bootstrap/plugins/intersection.js"></script>
        <script src="scripts/bootstrap/plugins/jquery.blockUI.js"></script>
        <script src="scripts/bootstrap/plugins/bootstrap-select.js"></script>
        <script src="scripts/bootstrap/plugins/dataTables.tableTools.js"></script>
        <script src="scripts/bootstrap/plugins/bootstrap-switch.min.js"></script>

        <!-- Integral Libraries -->
        <script src="scripts/integral/main.js?t=<%=timestamp%>"></script>
        <script type="text/javascript">
                                        $(function () {
                                            setProviders();
                                            setOrganizations();
                                            setOrderDates();
                                            setCurrencyPairs();
                                            setTakerOrg();
                                            setOrigCpty();

                                        });
                                        function setProviders() {
                                            IEM.Utils.syncajaxRequest('../rest/framework/streams', handleproviderResponse,
                                                    function () {
                                                        alert('Retrieving providers & Streams Failed. Try Again!');
                                                    },
                                                    "Getting Providers&Streams",
                                                    'Getting Providers&Streams'
                                                    );
                                        }

                                        function handleproviderResponse(response) {
                                            var providers = '';
                                            var psArr = [];
                                            for (var i = 0; i < response.length; i++) {
                                                providers += '<option value="' + response[i].provider + '/' + response[i].stream + '">' + response[i].provider + ' - ' + response[i].stream + '</option>';
                                                psArr.push(response[i].provider + ' - ' + response[i].stream);
                                            }
                                            $('#provider').html(providers).selectpicker('refresh');
                                            $('#oprovider').html(providers).selectpicker('refresh');
                                            IEM.Mapping.setMap('ProviderStream', psArr);
                                        }

                                        function setOrganizations() {
                                            IEM.Utils.syncajaxRequest('../rest/framework/organizations', handleOrganizationsResponse,
                                                    function () {
                                                        alert('Retrieving Organizations List Failed. Try Again!');
                                                    },
                                                    "Getting Organizations List",
                                                    'Getting Organizations List'
                                                    );
                                        }

                                        function setOrderDates() {
                                            IEM.Utils.syncajaxRequest('../rest/framework/orderdates', handleOrderDateResponse,
                                                    function () {
                                                        alert('Retrieving Order Dates List Failed. Try Again!');
                                                    },
                                                    "Getting Order Dates List",
                                                    'Getting Order Dates List'
                                                    );
                                        }

                                        function setCurrencyPairs() {
                                            IEM.Utils.syncajaxRequest('../rest/framework/currencypairs', handleCurrencyPairResponse,
                                                    function () {
                                                        alert('Retrieving Currency Pair List Failed. Try Again!');
                                                    },
                                                    "Getting Currency Pair  List",
                                                    'Getting Currency Pair List'
                                                    );
                                        }

                                        function setTakerOrg() {
                                            IEM.Utils.syncajaxRequest('../rest/framework/takerorgs', handleTakerOrgResponse,
                                                    function () {
                                                        alert('Retrieving Taker Org List Failed. Try Again!');
                                                    },
                                                    "Getting Taker Org List",
                                                    'Getting Taker Org List'
                                                    );
                                        }

                                        function setOrigCpty() {
                                            IEM.Utils.syncajaxRequest('../rest/framework/origcptys', handleOrigCptyResponse,
                                                    function () {
                                                        alert('Retrieving Original Counter Party List Failed. Try Again!');
                                                    },
                                                    "Getting Original Counter Party List",
                                                    'Getting Original Counter Party List'
                                                    );
                                        }

                                        function handleOrganizationsResponse(response) {
                                            var organizations = '';
                                            var organizationsArr = [];
                                            for (var i = 0; i < response.length; i++) {
                                                organizations += '<option value="' + response[i] + '">' + response[i] + '</option>';
                                                organizationsArr.push(response[i]);
                                            }
                                            $('#customer').html(organizations).selectpicker('refresh');
                                            IEM.Mapping.setMap('OrganizationsList', organizationsArr);
                                        }

                                        function handleOrderDateResponse(response) {
                                            var orderdates = '';
                                            for (var i = 0; i < response.length; i++) {
                                                orderdates += '<option value="' + response[i] + '">' + response[i] + '</option>';
                                            }
                                            //$('#searchDate').html(orderdates).selectpicker('refresh');
                                            $('#searchDate').append(orderdates);
                                            // IEM.Mapping.setMap('OrganizationsList',organizationsArr);
                                        }

                                        function handleCurrencyPairResponse(response) {
                                            var currencypairs = '';
                                            for (var i = 0; i < response.length; i++) {
                                                currencypairs += '<option value="' + response[i] + '">' + response[i] + '</option>';
                                            }
                                            $('#searchCcyPair').append(currencypairs);
                                            //$('#searchCcyPair').html(currencypairs).selectpicker('refresh');
                                            //IEM.Mapping.setMap('OrganizationsList',organizationsArr);
                                        }

                                        function handleTakerOrgResponse(response) {
                                            var takerorgs = '';
                                            for (var i = 0; i < response.length; i++) {
                                                takerorgs += '<option value="' + response[i] + '">' + response[i] + '</option>';
                                            }
                                            $('#searchTakerOrg').append(takerorgs);
                                            //$('#searchTakerOrg').html(takerorgs).selectpicker('refresh');
                                            //  IEM.Mapping.setMap('OrganizationsList',organizationsArr);
                                        }

                                        function handleOrigCptyResponse(response) {
                                            var origCpty = '';
                                            for (var i = 0; i < response.length; i++) {
                                                origCpty += '<option value="' + response[i] + '">' + response[i] + '</option>';
                                            }
                                            $('#searchOrigOrg').append(origCpty);
                                            //$('#searchOrigOrg').html(origCpty).selectpicker('refresh');

                                            /* $('#searchOrigOrg').multiselect({
                                             includeSelectAllOption: true
                                             });*/
                                            //  IEM.Mapping.setMap('OrganizationsList',organizationsArr);
                                        }

        </script>
        <!-- Tooltip Libraries -->
        <script src="scripts/integral/tipsy.js?t=<%=timestamp%>"></script>

        <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">

        <script src="scripts/integral/emscope.js?t=<%=timestamp%>"></script>
        <script src="scripts/integral/map.js?t=<%=timestamp%>"></script>
        <script src="scripts/integral/tradeanalysis_refined.js?t=<%=timestamp%>"></script>
        <script src="scripts/integral/orderTab_5.js?t=<%=timestamp%>"></script>
        <script src="scripts/integral/orderTabNew.js?t=<%=timestamp%>"></script>
        <script src="scripts/integral/utils.js?t=<%=timestamp%>"></script>
        <script src="scripts/integral/user.js?t=<%=timestamp%>"></script>
        <script src="scripts/integral/displayorder.js?t=<%=timestamp%>"></script>
        <script src="scripts/integral/ordercollection.js?t=<%=timestamp%>"></script>
        <script src="scripts/integral/tradesummary.js?t=<%=timestamp%>"></script>
        <!-- This JS is used to enable users to start a topic related QnA-->
        <script src="scripts/integral/contextual_qna.js?t=<%=timestamp%>"></script>

        <!--Modal is used to show if loading time is greater then 5 seconds -->
        <div class="modal fade" id="loadingTimeModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Warning</h4>
                    </div>
                    <div class="modal-body text-center">
                        <p>Request is taking more time to load, <br/>would you like to refresh your page or continue</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default refreshModal">Refresh</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Continue</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <!--Modal is used to show order tree view -->
        <div class="modal fade TreeChartContainer3" id="orderTreeViewModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-titlTreeChartContainere">Order Tree View</h4>
                    </div>
                    <div class="modal-body text-center orderNavigateTreeView3" style="overflow: auto">
                        <div></div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!--Modal is used to enable users to start a topic related QnA -->
        <div class="modal fade" id="contextualHelpModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">

                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </body>
</html>

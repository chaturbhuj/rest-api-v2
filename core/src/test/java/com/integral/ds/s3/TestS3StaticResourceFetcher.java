package com.integral.ds.s3;

import com.integral.ds.dao.s3.S3StaticResourceFetcher;
import org.junit.Test;

/**
 * Tests code for s3 static resource fetcher.
 * @author Rahul Bhattacharjee
 */
public class TestS3StaticResourceFetcher {

    //@Test
    public void testOrganizationFetcher() {
        S3StaticResourceFetcher staticResourceFetcher = new S3StaticResourceFetcher();
        staticResourceFetcher.init();
        System.out.println(staticResourceFetcher.getCompleteOrganizationList());
    }
}

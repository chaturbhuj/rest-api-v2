package com.integral.ds.s3;

import com.integral.ds.emscope.rates.BrokerRateObject;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertNotNull;

/**
 * Tests S3 broker rates reader.
 *
 * @author Rahul Bhattacharjee
 */
public class TestS3BrokerRatesReader {

    //@Test
    public void testBrokerRatesReader() throws Exception {
        String broker = "DemoCITIFMA";
        String stream = "DemoCITIFMA";
        String ccyPair = "EURUSD";

        String startTimestamp = "03.12.2014 11:55:00.000";
        String endTimestamp =   "03.12.2014 12:05:00.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        S3BrokerRatesReader brokerRatesReader = new S3BrokerRatesReader();
        List<BrokerRateObject> brokerRates = brokerRatesReader.getRates(broker,stream,startTime,endTime,ccyPair);

        assertNotNull(brokerRates);
        for(BrokerRateObject rate : brokerRates) {
            System.out.println(new Date(rate.getTimeStamp()));
        }
    }
}

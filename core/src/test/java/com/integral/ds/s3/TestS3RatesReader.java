package com.integral.ds.s3;

import com.integral.ds.emscope.quote.QuoteTransformer;
import com.integral.ds.emscope.quote.impl.ProfileQuoteTransformer;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.util.DataQueryUtils;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

/**
 * Tests the S3RatesReader.
 *
 * @author Rahul Bhattacharjee
 */
public class TestS3RatesReader {

    //@Test
    public void testS3RatesReaderForSingleCurrency() throws Exception {
        String startTimestamp = "05.08.2014 12:39:00.000";
        String endTimestamp =   "05.08.2014 12:40:00.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        String provider = "DB";
        String stream = "EU0308852";
        String ccypair = "EURUSD";

        long start = System.currentTimeMillis();

        S3RatesReader ratesReader = new S3RatesReader();
        List<RatesObject> ratesObjectList = ratesReader.getRates(provider, stream, startTime, endTime, ccypair);

        System.out.println("Time taken " + (System.currentTimeMillis() - start));

        System.out.println("Size " + ratesObjectList.size());

        for(RatesObject ratesObject : ratesObjectList) {
            System.out.println(new Date(ratesObject.getTmstmp()));
            //System.out.println(ratesObject.toString());
        }
    }

    //@Test
    public void testS3RatesReaderForSingleCurrencyExotic() throws Exception {
        String startTimestamp = "05.27.2014 14:58:45.185";
        String endTimestamp =   "05.27.2014 15:06:45.185";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        String provider = "JPM";
        String stream = "Blue";
        String ccypair = "CHFJPY";

        long start = System.currentTimeMillis();

        S3RatesReader ratesReader = new S3RatesReader();
        List<RatesObject> ratesObjectList = ratesReader.getRates(provider, stream, startTime, endTime, ccypair);

        System.out.println("Time taken " + (System.currentTimeMillis() - start));

        System.out.println("Size " + ratesObjectList.size());

        for(RatesObject ratesObject : ratesObjectList) {
            System.out.println(new Date(ratesObject.getTmstmp()));
        }
    }

    //@Test
    public void testS3RatesReaderSamplingTRPD() throws Exception {
        String startTimestamp = "04.15.2014 12:00:00.000";
        String endTimestamp = "04.15.2014 12:00:00.999";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        String provider = "HSBC";
        String stream = "IntGloInst1";
        String ccypair = "EURUSD";

        long start = System.currentTimeMillis();

        S3RatesReader ratesReader = new S3RatesReader();
        List<RatesObject> ratesObjectList = ratesReader.getRates(provider, stream, startTime, endTime, ccypair);

        System.out.println("Time taken " + (System.currentTimeMillis() - start));

        Quote quote = new Quote();

        int count = 0;
        for(RatesObject ratesObject : ratesObjectList) {
            float offer = ratesObject.getAsk_price().floatValue();
            float bid = ratesObject.getBid_price().floatValue();

            System.out.println(DataQueryUtils.toString(ratesObject));

            quote.addRate(DataQueryUtils.getRestRateObject(ratesObject));

            if((offer - bid) < 0.0f) {
                if(offer == 0.0 || bid == 0.0) {
                    continue;
                }
                System.out.println(DataQueryUtils.toString(ratesObject));
                count++;
            }
        }

        QuoteTransformer transformer = new ProfileQuoteTransformer();
        Quote q = transformer.transform(quote);
        for(RestRatesObject rate : q.getRates()) {
            System.out.println(DataQueryUtils.toString(rate));
        }

        System.out.println("Size " + ratesObjectList.size());
        System.out.println("Count " + count);
    }

    //@Test
    public void testS3RatesReaderSamplingVTFX() throws Exception {
        String startTimestamp = "04.16.2014 08:30:00.000";
        String endTimestamp = "04.16.2014 08:30:01.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        String provider = "VTFX";
        String stream = "VFX0";
        String ccypair = "GBPUSD";

        long start = System.currentTimeMillis();

        S3RatesReader ratesReader = new S3RatesReader();
        List<RatesObject> ratesObjectList = ratesReader.getRates(provider, stream, startTime, endTime, ccypair);

        System.out.println("Time taken " + (System.currentTimeMillis() - start));
        int count = 0;
        Quote quote = new Quote();

        for(RatesObject ratesObject : ratesObjectList) {
            BigDecimal askSize = ratesObject.getAsk_size();
            BigDecimal askPrice = ratesObject.getAsk_price();
            BigDecimal bidPrice = ratesObject.getBid_price();
            BigDecimal bidSize = ratesObject.getBid_size();
            String status = ratesObject.getStatus();
            int tier = ratesObject.getLvl();
            long timeStamp = ratesObject.getTmstmp();

            System.out.println("Adding rate offer=" + askPrice +" offer size="+askSize+" ,bid price=" + bidPrice +" ,bidSize="+bidSize
            +" status=" +status+ " tier="+tier+ " ,timestamp="+dateFormat.format(new Date(timeStamp)));

            RestRatesObject rate = new RestRatesObject();
            rate.setAskSize(askSize);
            rate.setAskPrice(askPrice);
            rate.setBidPrice(bidPrice);
            rate.setBidSize(bidSize);
            rate.setStatus(status);
            rate.setTmstmp(timeStamp);
            rate.setCcyPair(ratesObject.getCcyp());
            rate.setTier(tier);
            rate.setGuid(ratesObject.getGuid());

            quote.addRate(rate);
        }

        QuoteTransformer transformer = new ProfileQuoteTransformer();

        Quote q = transformer.transform(quote);

        List<RestRatesObject> restRates = q.getRates();
        for(RestRatesObject r : restRates) {
            System.out.println("Offer " + r.getAsk_price() + " Bid " + r.getBid_price() + " level " + r.getLvl() + " spread " + r.getAsk_price().subtract(r.getBid_price()));

        }

        System.out.println("Size " + ratesObjectList.size());
        System.out.println("Count " + count);
    }

    //@Test
    public void testS3RatesReaderForMultipleCurrency() throws Exception {
        String startTimestamp = "01.22.2014 23:59:00.000";
        String endTimestamp =   "01.23.2014 00:02:00.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        String provider = "CITI";
        String stream = "Integral1";
        String ccypairFirst = "EURZAR";
        String ccypairSecond = "EURNOK";

        List<String> currencyPairs = new ArrayList<String>();
        currencyPairs.add(ccypairFirst);
        currencyPairs.add(ccypairSecond);

        S3RatesReader ratesReader = new S3RatesReader();
        Map<String,List<RatesObject>> ratesObjectMap = ratesReader.getRates(provider, stream, startTime, endTime, currencyPairs);

        System.out.println("Size of " + ccypairFirst + " " + ratesObjectMap.get(ccypairFirst).size());
        System.out.println("Size of " + ccypairSecond + " " + ratesObjectMap.get(ccypairSecond).size());

    }

    private static class RateObjectFilter implements Filter<RatesObject> {
        private String ccyPair;

        public RateObjectFilter(String ccypair) {
            this.ccyPair = ccypair;
        }

        @Override
        public boolean shouldFilter(RatesObject obj) {
            return !ccyPair.equalsIgnoreCase(obj.getCcyp());
        }
    }

}

package com.integral.ds.s3;

import com.integral.ds.emscope.rates.BrokerRateObject;
import com.integral.ds.emscope.rates.LPStream;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * @author Rahul Bhattacharjee
 */
public class TestBrokerRateObjectTransformer {

    //@Test
    public void testBrokerRateTransformer() {
        RecordTransformer<BrokerRateObject> transformer = new BrokerRateObjectTransformer();
        String record = "DemoCITIFMA,DemoCITIFMA,EURUSD,03.12.2014 12:00:00.245,B110-FXIN_4|P830-StreamC|B390-IntGloInst2|P820-MSFXP1|B140-MS_Standard|B260-ECN|CITIP-Matrix|P750-VFX0|P491-StreamNY|B371-GREEN|F890-CITIP|B031-FXIN_B|B930-Institutional|B370-BLUE,FBTA,MQ,Full,G-3c369d32-144b629daf5-DemoCITIFMA-8ce0d2,1.38691,6241000.0,B110+,1.38697,2773000.0,B930";

        BrokerRateObject rateObject = transformer.transform(record);

        assertNotNull(rateObject);
        assertEquals(rateObject.getBroker(),"DemoCITIFMA");
        assertEquals(rateObject.getBrokerStream(),"DemoCITIFMA");
        assertEquals(rateObject.getCcyPair(),"EURUSD");
        assertEquals(rateObject.getLpStreams().size(),14);

        LPStream lpStream = rateObject.getLpStreams().get(0);
        assertEquals(lpStream.getProvider(),"B110");
        assertEquals(lpStream.getStream(),"FXIN_4");
    }
}

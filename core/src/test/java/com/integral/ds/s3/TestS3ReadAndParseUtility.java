package com.integral.ds.s3;

import com.integral.ds.dao.s3.S3ReadAndParseUtility;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.util.List;

/**
 * Tests the S3ReadAndParseUtility.
 *
 * @author Rahul Bhattacharjee
 */
public class TestS3ReadAndParseUtility {

    //@Test
    public void testReadAndParseUtility() {
        String bucket = "integral-rates";
        String key = "config/AvailableStreams.txt";
        List<Pair<String,String>> response = S3ReadAndParseUtility.downloadAndParseS3File(bucket,key,
                new RecordTransformer<Pair<String,String>>() {
                    @Override
                    public Pair<String, String> transform(String input) {
                        if(StringUtils.isNotBlank(input)) {
                            Pair<String,String> pair = new Pair<String,String>();
                            String [] splits = input.split(",");
                            pair.setFirst(splits[0]);
                            pair.setSecond(splits[1]);
                        }
                        return null;
                    }
        });
        System.out.println("Size of response => " + response.size());
    }


    private static class Pair<U,V> {
        private U first;
        private V second;

        private U getFirst() {
            return first;
        }

        private void setFirst(U first) {
            this.first = first;
        }

        private V getSecond() {
            return second;
        }

        private void setSecond(V second) {
            this.second = second;
        }
    }
}

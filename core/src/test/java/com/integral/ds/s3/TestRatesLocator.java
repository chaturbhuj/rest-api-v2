package com.integral.ds.s3;

import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RatesObject;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Tests the rates locator utility.
 *
 * @author Rahul Bhattacharjee
 */
public class TestRatesLocator {

    //@Test
    public void testRatesLocator() throws Exception {
        String startTimestamp = "01.22.2014 08:01:00.000";
        String endTimestamp =   "01.22.2014 012:40:00.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        String provider = "CITI";
        String stream = "Integral1";
        String ccypair = "EURUSD";

        long start = System.currentTimeMillis();

        RatesLocator ratesLocator = new RatesLocator(provider,stream,ccypair,startTime,endTime);

        System.out.println("Time taken to create rates locator " + (System.currentTimeMillis() - start));

        Map<Long,List<RatesObject>> rates = ratesLocator.getSubRates(startTime, endTime);

        System.out.println("Full view " + rates.size());

        startTimestamp = "01.22.2014 10:10:00.000";
        endTimestamp =   "01.22.2014 10:30:00.000";

        long startSubQuery = System.currentTimeMillis();

        rates = ratesLocator.getSubRates(dateFormat.parse(startTimestamp), dateFormat.parse(endTimestamp));

        System.out.println("Time taken to fetch submap " + (System.currentTimeMillis() - startSubQuery));
        System.out.println("End view " + rates.size());

        startTimestamp = "01.22.2014 10:00:00.000";
        endTimestamp =   "01.22.2014 10:42:00.000";

        startSubQuery = System.currentTimeMillis();

        rates = ratesLocator.getSubRates(dateFormat.parse(startTimestamp), dateFormat.parse(endTimestamp));

        System.out.println("Time taken to fetch submap 2 " + (System.currentTimeMillis() - startSubQuery));
        System.out.println("End view " + rates.size());
    }

    //@Test
    public void testSubRates() throws Exception {
        String startTimestamp = "01.20.2014 10:01:00.000";
        String endTimestamp =   "01.21.2014 10:01:00.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        System.out.println(startTime.getTime());
        System.out.print(endTime.getTime());

        String provider = "CITI";
        String stream = "Integral1";
        String ccypair = "EURUSD";

        long t1 = System.currentTimeMillis();
        RatesLocator ratesLocator = new RatesLocator(provider,stream,ccypair,startTime,endTime);

        List<Quote> quotes = ratesLocator.getSamples(startTime,endTime,30);
        //http://localhost:75/emscope/rest/quote/sample/CITI/Integral1/EURUSD/1390240860000/1390327260000/10

        System.out.println("Time " + (System.currentTimeMillis()- t1));
        System.out.println("Size of quotes " + quotes.size());
        for(Quote quote : quotes) {
            System.out.println(quote.getRates().size());
        }
    }

    //@Test
    public void testSubRatesWithNewDateFormat() throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        /*
        String startTimestamp = "03.28.2014 20:32:52.049";
        String endTimestamp =   "03.28.2014 20:57:55.376";
        String provider = "BOAN";
        String stream = "BandC";
        String ccypair = "NZDUSD";
        */
        String startTimestamp = "03.28.2014 20:58:58.276";
        String endTimestamp =   "03.28.2014 20:59:19.569";
        String provider = "DB";
        String stream = "EU0196733";
        String ccypair = "EURJPY";

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        long t1 = System.currentTimeMillis();
        RatesLocator ratesLocator = new RatesLocator(provider,stream,ccypair,startTime,endTime);

        List<Quote> quotes = ratesLocator.getSamples(startTime,endTime,30);
        //http://localhost:75/emscope/rest/quote/sample/CITI/Integral1/EURUSD/1390240860000/1390327260000/10
        System.out.println("Time " + (System.currentTimeMillis()- t1));
        System.out.println("Size of quotes " + quotes.size());
        for(Quote quote : quotes) {
            System.out.println(quote.getRates().size());
        }
    }
}

package com.integral.ds.s3;

import com.integral.ds.dao.s3.S3ProviderStreamConfigFetcherImpl;
import com.integral.ds.dto.ProviderStream;
import org.junit.Test;

import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class TestS3StreamReaderDao {

    //@Test
    public void testS3StreamReaderDao(){
        S3ProviderStreamConfigFetcherImpl streamReaderDao = new S3ProviderStreamConfigFetcherImpl();
        streamReaderDao.init(null);

        List<ProviderStream> streamList = streamReaderDao.getProviderStreams();
        System.out.println("Streams list size " + streamList);
    }

    //@Test
    public void testS3FileUploader() {
        S3StreamReader reader = new S3StreamReader();
        String bucketName = "execution-profile";
        String s3Key = "logs/test.zip";
        String localFilePath = "C:\\Users\\bhattacharjeer\\Desktop\\epa\\file_1403572791003.csv";
        reader.uploadLocalFile(bucketName,s3Key,localFilePath);
    }
}

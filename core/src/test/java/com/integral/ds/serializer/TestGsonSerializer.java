package com.integral.ds.serializer;

import org.junit.Test;

/**
 * Tests ignore serialization annotation.
 * @author Rahul Bhattacharjee
 */
public class TestGsonSerializer {

    //@Test
    public void testDoNotSerializerAnnotation() {
        TestClass test = new TestClass();
        test.setAge(35);
        test.setName("Rahul");

        Serializer serializer = new SerializerFactory().getSerializer(SerializerFactory.JSON);
        String serializedContent = serializer.serialize(test);

        System.out.println("Serializer " + serializedContent);
    }

    class TestClass {
        private String name;

        @DoNotSerialize
        private int age;

        int getAge() {
            return age;
        }

        void setAge(int age) {
            this.age = age;
        }

        String getName() {
            return name;
        }

        void setName(String name) {
            this.name = name;
        }
    }
}

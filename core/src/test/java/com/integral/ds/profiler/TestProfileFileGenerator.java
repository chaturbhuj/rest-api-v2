package com.integral.ds.profiler;

import com.integral.ds.dto.ProviderStream;
import org.junit.Test;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

/**
 * @author Rahul Bhattacharjee
 */
public class TestProfileFileGenerator {

    //@Test
    public void generateProfileFile() throws Exception {
        String startTimestamp = "05.13.2014 16:01:00.000";
        String endTimestamp = "05.13.2014 16:55:00.000";
        List<ProviderStream> providerStreams = getProviderStreams();
        String ccyPair = "AUDUSD";
        int samples = 3000;

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        ExecutorService executorService = getExecutor();
        List<Future<ProfileGroup>> futures = new ArrayList<Future<ProfileGroup>>();
        for(ProviderStream providerStream : providerStreams) {
            ProfileFetcherTask task = new ProfileFetcherTask(providerStream.getProvider(),providerStream.getStream(),ccyPair,startTime,endTime,samples);
            futures.add(executorService.submit(task));
        }

        List<ProfileGroup> profileGroups = getProfileGroups(futures);
        executorService.shutdown();

        String fileName = "C:\\Users\\bhattacharjeer\\Desktop\\out\\sample-big.tqd";
        File file = new File(fileName);
        try {
            ProfileFileGenerator fileGenerator = new ProfileFileGenerator();
            fileGenerator.write(file,profileGroups);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Generated Profile.");
    }

    private ExecutorService getExecutor() {
        int noOfThreads = Runtime.getRuntime().availableProcessors()+1;
        ExecutorService service = Executors.newFixedThreadPool(noOfThreads,new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
        });
        return service;
    }

    /**
     *
     * @param futures
     * @return
     */
    private List<ProfileGroup> getProfileGroups(List<Future<ProfileGroup>> futures) {
        List<ProfileGroup> profileGroups = new ArrayList<ProfileGroup>();
        for(Future<ProfileGroup> groupFuture : futures) {
            try {
                profileGroups.add(groupFuture.get());
            } catch (Exception e) {
                //todo put logger.
                e.printStackTrace();
            }
        }
        return profileGroups;
    }

    public List<ProviderStream> getProviderStreams() {
        List<ProviderStream> providerStreams = new ArrayList<ProviderStream>();
        String serializedString = "BOAN:BandD,JPM:Gold,UBS:Super,BOAN:BandF,CITI:Integral1";
        String [] splits = serializedString.split(",");
        for(String split : splits) {
            String [] parts = split.split(":");
            String provider = parts[0];
            String stream = parts[1];
            providerStreams.add(new ProviderStream(provider,stream));
        }
        return providerStreams;
    }
}

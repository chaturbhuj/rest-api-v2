package com.integral.ds.profiler;

import com.integral.ds.emscope.profile.ProfileRecord;
import com.integral.ds.s3.S3ProfileReader;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Test case for testing profile service.
 *
 * @author Rahul Bhattacharjee
 */
public class TestProfileService {

    //@Test
    public void testProfileService() throws Exception {
        S3ProfileReader profileReader = new S3ProfileReader();
        String provider = "MHDP";
        String stream = "StreamMC";
        String ccyPair = "AUDUSD";

        String startTimestamp = "01.22.2014 08:00:00.000";
        String endTimestamp =   "01.22.2014 09:00:00.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date start = dateFormat.parse(startTimestamp);
        Date end = dateFormat.parse(endTimestamp);

        List<ProfileRecord> profiles = profileReader.getProfileRecords(provider, stream, ccyPair, start, end);
        System.out.println("Profile fetched " + profiles.size());

        NavigableMap<Long,ProfileRecord> map = new TreeMap<Long,ProfileRecord>();
        for(ProfileRecord record : profiles) {
            long timeStamp = record.getTime();
            map.put(timeStamp,record);
        }
        System.out.println("Start " + new Date(map.firstKey()));
        System.out.println("End " + new Date(map.lastKey()));
    }
}

package com.integral.ds.profiler;

import com.hazelcast.impl.Util;
import com.integral.ds.util.ResourceUtils;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class ProfileFileGenerator {

    private FileOutputStream fout = null;

    public void write(File file,List<ProfileGroup> profileGroups) {
        createFile(file);
        try {
            writeHeader();
            writeTimeHeader(profileGroups.get(0));
            for(ProfileGroup profileGroup : profileGroups) {
                writeProfileGroup(profileGroup);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    private void writeTimeHeader(ProfileGroup profileGroup) {
        List<String> timestamps = new ArrayList<String>();
        for(ProfileTuple profileTuple : profileGroup.getProfileTuples()) {
            String formattedTime = getFormatterTime(profileTuple.getDateTime());
            timestamps.add(formattedTime);
        }
        String timeSeries = StringUtils.join(timestamps,",");
        try {
            fout.write(timeSeries.getBytes());
            fout.write("\n".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getFormatterTime(Date dateTime) {
        // format : 1/1/2014 22:00:01
        //SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/y HH:mm:ss");
        return dateFormat.format(dateTime);
    }

    private void writeProfileGroup(ProfileGroup profileGroup) {
        try {
            fout.write(profileGroup.toContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeHeader() {
        ResourceUtils resourceUtils = new ResourceUtils();
        InputStream in = resourceUtils.getInputStream("tqdfile.hdr");
        try {
            Util.copyStream(in,fout);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void createFile(File file) {
        try {
            fout = new FileOutputStream(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void close() {
        if(fout != null) {
            try {
                fout.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

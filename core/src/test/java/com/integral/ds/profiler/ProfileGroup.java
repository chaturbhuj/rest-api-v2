package com.integral.ds.profiler;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class ProfileGroup {

    private List<ProfileTuple> profileTupleList = new ArrayList<ProfileTuple>();

    public void addProfileTuple(ProfileTuple tuple) {
        profileTupleList.add(tuple);
    }

    public boolean isEmpty() {
        return profileTupleList.isEmpty();
    }

    public List<ProfileTuple> getProfileTuples(){
        return this.profileTupleList;
    }

    public byte [] toContent() {
        List<String> fields = new ArrayList<String>();
        fields.add(getGroupIdentifier());
        for(ProfileTuple tuple : getProfileTuples()) {
            fields.add(tuple.getMidPriceMean().toString());
            fields.add(tuple.getMaxAskPrice().toString());
            fields.add(tuple.getMaxBidPrice().toString());
        }
        String joinedString = StringUtils.join(fields,",");
        joinedString += "\n";
        return joinedString.getBytes();
    }

    private String getGroupIdentifier() {
        if(profileTupleList.isEmpty()) {
            return "";
        }
        ProfileTuple tuple = profileTupleList.get(0);
        return tuple.getProvider() + "-" + tuple.getStream();
    }

    public int groupSize() {
        int size = 0;
        if(!isEmpty()) {
            getProfileTuples().size();
        }
        return size;
    }

    @Override
    public String toString() {
        return "ProfileGroup{" +
                "profileTupleList=" + profileTupleList +
                '}';
    }
}

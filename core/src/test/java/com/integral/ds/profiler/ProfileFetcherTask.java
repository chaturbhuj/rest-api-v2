package com.integral.ds.profiler;

import com.integral.ds.emscope.profile.ProfileRecord;
import com.integral.ds.s3.RecordTransformer;
import com.integral.ds.s3.S3StreamReader;
import com.integral.ds.s3.impl.ProfileRecordTransformer;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;

/**
 * Profile fetcher task.
 *
 * @author Rahul Bhattacharjee
 */
public class ProfileFetcherTask implements Callable<ProfileGroup> {

    private static final Logger LOGGER = Logger.getLogger(ProfileFetcherTask.class);

    private static final String S3_BASE_PATH = "ProfileData/";

    private String provider;
    private String stream;
    private String ccyPair;
    private Date start;
    private Date end;
    private int samples;

    public ProfileFetcherTask(String provider, String stream, String ccyPair, Date start, Date end, int samples) {
        this.provider = provider;
        this.stream = stream;
        this.ccyPair = ccyPair;
        this.start = start;
        this.end = end;
        this.samples = samples;
    }

    @Override
    public ProfileGroup call() throws Exception {
        Map<Long,ProfileRecord> profileMap = getProfileCache();
        ProfileGroup profileGroup = new ProfileGroup();
        long timeRange = (end.getTime() - start.getTime());
        long gapBetweenSamples = timeRange/samples;
        long startTime = start.getTime();
        int count = 0;

        while(count < samples) {
            ProfileTuple tuple = getProfileTuple(profileMap,startTime);
            profileGroup.addProfileTuple(tuple);
            startTime += gapBetweenSamples;
            count++;
        }
        return profileGroup;
    }

    private ProfileTuple getProfileTuple(Map<Long, ProfileRecord> profileMap, long startTime) {
        ProfileTuple profileTuple = null;
        try {
            long normalizedTime = normalize(startTime);
            ProfileRecord record = profileMap.get(normalizedTime);

            if(record == null) {
                System.out.println("Record for time " + new Date(normalizedTime) + " is null for provider = " + provider + " stream " + stream);
                System.exit(1);
            }

            profileTuple = transformProfileRecord(record);
        } catch (Exception e) {
            // todo
            e.printStackTrace();
            LOGGER.error("Exception while fetching profile tuple.",e);
        }
        return profileTuple;
    }

    private long normalize(long startTime) {
        Calendar calender = Calendar.getInstance();
        calender.setTime(new Date(startTime));
        calender.set(Calendar.MILLISECOND,0);
        return calender.getTime().getTime();
    }

    private Map<Long, ProfileRecord> getProfileCache() throws Exception {
        long start = System.currentTimeMillis();
        Map<Long,ProfileRecord> profileCache = new TreeMap<Long,ProfileRecord>();
        S3StreamReader streamReader = new S3StreamReader();
        String filePath = getFilePath();

        RecordTransformer<ProfileRecord> profileRecordTransformer = new ProfileRecordTransformer();
        InputStream inputStream = null;
        InputStream zipInputStream = null;
        BufferedReader reader = null;
        ProfileRecord profileRecord = null;

        try {

            inputStream = streamReader.getInputStream(filePath);
            zipInputStream = new GZIPInputStream(inputStream);
            reader = new BufferedReader(new InputStreamReader(zipInputStream));

            String line = "";
            while(line != null) {
                line = reader.readLine();
                if(StringUtils.isBlank(line)) {
                    continue;
                }
                profileRecord = profileRecordTransformer.transform(line);
                Long time = profileRecord.getTime();
                profileCache.put(time,profileRecord);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Path " + filePath);
            LOGGER.error("Exception while fetching profile.",e);
        } finally {
            if(reader != null)
                reader.close();
            if(zipInputStream != null)
                zipInputStream.close();
            if(inputStream != null)
                inputStream.close();
        }
        System.out.println("Time taken to create profile cache for " + provider + " is " + (System.currentTimeMillis() - start));
        return profileCache;
    }

    private ProfileTuple transformProfileRecord(ProfileRecord record) {
        ProfileTuple tuple = new ProfileTuple();
        tuple.setProvider(record.getProvider());
        tuple.setStream(record.getStream());
        tuple.setDateTime(new Date(record.getTime()));
        tuple.setMidPriceMean(record.getMidPriceMean());
        tuple.setMaxAskPrice(record.getMaxAskPrice());
        tuple.setMaxBidPrice(record.getMaxBidPrice());
        return tuple;
    }

    private String getFilePath() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(S3_BASE_PATH);
        SimpleDateFormat dateFormatForDate = new SimpleDateFormat("y-MM-dd");
        String dateString = dateFormatForDate.format(start);
        stringBuilder.append(dateString);
        stringBuilder.append("/");
        SimpleDateFormat dateFormatForHour = new SimpleDateFormat("HH");
        String hourString = dateFormatForHour.format(start);
        stringBuilder.append(hourString);
        stringBuilder.append("/");
        String fileName = ccyPair + "_" + dateString + "_" +provider + "_" + stream + "_" + hourString + ".gz";
        stringBuilder.append(fileName);
        return stringBuilder.toString();
    }
}

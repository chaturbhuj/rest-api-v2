package com.integral.ds.profiler;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public class ProfileTuple {

    private String provider;
    private String stream;
    private Date dateTime;
    private BigDecimal midPriceMean;
    private BigDecimal maxBidPrice;
    private BigDecimal maxAskPrice;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public BigDecimal getMidPriceMean() {
        return midPriceMean;
    }

    public void setMidPriceMean(BigDecimal midPriceMean) {
        this.midPriceMean = midPriceMean;
    }

    public BigDecimal getMaxBidPrice() {
        return maxBidPrice;
    }

    public void setMaxBidPrice(BigDecimal maxBidPrice) {
        this.maxBidPrice = maxBidPrice;
    }

    public BigDecimal getMaxAskPrice() {
        return maxAskPrice;
    }

    public void setMaxAskPrice(BigDecimal maxAskPrice) {
        this.maxAskPrice = maxAskPrice;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "ProfileTuple{" +
                "provider='" + provider + '\'' +
                ", stream='" + stream + '\'' +
                ", dateTime=" + dateTime +
                ", midPriceMean=" + midPriceMean +
                ", maxBidPrice=" + maxBidPrice +
                ", maxAskPrice=" + maxAskPrice +
                '}';
    }
}

package com.integral.ds.emscope.quote;

import com.integral.ds.emscope.quote.impl.RateQuoteServiceCompleteDownload;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Tests the rates quote service which downloads the complte file.
 *
 * @author Rahul Bhattacharjee
 */
public class TestQuoteService {

    private RatesQuoteService quoteService = new RateQuoteServiceCompleteDownload();

    //@Test
    public void testQuoteAtTime()  throws Exception {
        //String point = "01.22.2014 02:55:01.796"; // exact match
        String point = "01.23.2014 02:55:04.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        Date time = dateFormat.parse(point);

        String provider = "BOAN";
        String stream = "BandC";
        String ccypair = "AUDCAD";

        Quote quote = quoteService.getQuoteAtTime(provider,stream,ccypair,time);
        System.out.println("Quote " + quote);

        System.out.println("Quote time " + dateFormat.format(quote.getQuoteTime()));
        for(RestRatesObject rate : quote.getRates()) {
            System.out.println("   " + dateFormat.format(rate.getTmstmp()));
        }

    }

    //@Test
    public void testVWAPRate()  throws Exception {
        String point = "01.23.2014 02:55:04.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        Date time = dateFormat.parse(point);

        String provider = "BOAN";
        String stream = "BandC";
        String ccypair = "AUDCAD";

        RestRatesObject rate = quoteService.getQuoteAtTimeForVolume(provider,stream,ccypair,time,50000l);
        System.out.println("Rate " + rate);

        System.out.println("Rate time" + dateFormat.format(rate.getTmstmp()));
        System.out.println("Rate Bid price " + rate.getBid_price());
        System.out.println("Rate Bid volume " + rate.getBid_size());
        System.out.println("Rate Offer price " + rate.getAsk_price());
        System.out.println("Rate Offer volume " + rate.getAsk_size());
    }

    //@Test
    public void testQuoteSamplesWithOneBucket() throws Exception {
        String startTimestamp = "02.12.2014 02:10:01.796";
        String endTimestamp = "02.12.2014 07:40:01.796";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        Date start = dateFormat.parse(startTimestamp);
        Date end = dateFormat.parse(endTimestamp);

        String provider = "BOAN";
        String stream = "BandC";
        String ccypair = "AUDUSD";

        List<Quote> quotes = quoteService.getSampledQuotes(provider,stream,ccypair,start,end,150);
        System.out.println("Quote list size " + quotes.size());

        for(Quote quote : quotes) {
            System.out.println("Quote time " + quote.getQuoteTime());
        }
    }

    //@Test
    public void testQuoteSamplesWithMultipleBuckets() throws Exception {
        String startTimestamp = "02.12.2014 02:00:01.796";
        String endTimestamp = "02.12.2014 03:00:01.796";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        Date start = dateFormat.parse(startTimestamp);
        Date end = dateFormat.parse(endTimestamp);

        String provider = "BOAN";
        String stream = "BandC";
        String ccypair = "AUDUSD";

        List<Quote> quotes = quoteService.getSampledQuotes(provider,stream,ccypair,start,end,10);
        System.out.println("Quote list size " + quotes.size());

        for(Quote quote : quotes) {
            System.out.println("Quote time " + dateFormat.format(quote.getQuoteTime()));
            for(RestRatesObject rate : quote.getRates()) {
                System.out.println("   " + dateFormat.format(rate.getTmstmp()));
            }
        }
    }

    //@Test
    public void testQuoteSamplesWithQuoteServiceFromFactory() throws Exception {
        String startTimestamp = "02.12.2014 02:00:01.796";
        String endTimestamp = "02.12.2014 08:00:01.796";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        Date start = dateFormat.parse(startTimestamp);
        Date end = dateFormat.parse(endTimestamp);

        String provider = "BOAN";
        String stream = "BandC";
        String ccypair = "AUDUSD";

        int noOfSamples = 100;

        RatesQuoteService quoteService = QuoteServiceFactory.getRateQuoteService(start,end,noOfSamples);

        System.out.println("Service Class " + quoteService.getClass().getName());

        List<Quote> quotes = quoteService.getSampledQuotes(provider,stream,ccypair,start,end,noOfSamples);

        System.out.println("Quote list size " + quotes.size());

        for(Quote quote : quotes) {
            System.out.println("Quote time " + dateFormat.format(quote.getQuoteTime()));
            for(RestRatesObject rate : quote.getRates()) {
                System.out.println("   " + dateFormat.format(rate.getTmstmp()));
            }
        }
    }
}

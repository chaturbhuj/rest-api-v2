package com.integral.ds.emscope.quote;

import com.integral.ds.emscope.quote.impl.RateQuoteServiceCompleteDownload;
import com.integral.ds.emscope.quote.impl.RateQuoteServiceDenseSamples;
import com.integral.ds.emscope.quote.impl.RateQuoteServiceWithSelectiveDownload;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class TestQuoteServiceForDenseSamples {

    private RatesQuoteService quoteService = new RateQuoteServiceDenseSamples();

    //@Test
    public void testQuoteSamplesWithOneBucket() throws Exception {
        String startTimestamp = "05.14.2014 00:00:00.000";
        String endTimestamp = "05.15.2014 00:00:00.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        Date start = dateFormat.parse(startTimestamp);
        Date end = dateFormat.parse(endTimestamp);

        String provider = "BOAN";
        String stream = "RetailA";
        String ccypair = "EURUSD";

        long startTime = System.currentTimeMillis();

        List<Quote> quotes = quoteService.getSampledQuotes(provider,stream,ccypair,start,end,1000);
        System.out.println("Quote list size " + quotes.size());

        for(Quote quote : quotes) {
            printQuote(quote);
            System.out.println("==");
        }

        System.out.println("Time taken " + (System.currentTimeMillis()-startTime)/(1000) + " sec");
        System.out.println("Size is " + quotes.size());
    }

    private void printQuote(Quote quote) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        System.out.println("Sample time " + dateFormat.format(quote.getSampleTime()));
        List<RestRatesObject> rates = quote.getRates();
        for(RestRatesObject rate : rates) {
            System.out.println(dateFormat.format(new Date(rate.getTmstmp())));
        }
    }

    //@Test
    public void testQuoteSamplesWithMultipleBuckets() throws Exception {
        String startTimestamp = "04.03.2014 02:00:01.796";
        String endTimestamp = "04.03.2014 22:00:01.796";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        Date start = dateFormat.parse(startTimestamp);
        Date end = dateFormat.parse(endTimestamp);

        /*
        String provider = "BARX";
        String stream = "TIER3";
        String ccypair = "AUDUSD";
        */

        String provider = "BOAN";
        String stream = "BandC";
        String ccypair = "AUDUSD";

        long start1 = System.currentTimeMillis();

        List<Quote> quotes = quoteService.getSampledQuotes(provider,stream,ccypair,start,end,78);

        for(Quote quote : quotes) {
            System.out.println("Quote time " + dateFormat.format(quote.getQuoteTime()));
            for(RestRatesObject rate : quote.getRates()) {
                System.out.println("   " + dateFormat.format(rate.getTmstmp()));
            }
        }

        System.out.println("Time taken  " + (System.currentTimeMillis()- start1));
        System.out.println("Quotes size " + quotes.size());
    }
}

package com.integral.ds.emscope.graph;

import com.integral.ds.dao.OrdersDAO;
import com.integral.ds.dto.OrderInfo;
import com.integral.ds.dto.OrderNode;
import com.integral.ds.emscope.graph.impl.NavigationServiceUsingOrderTableImpl;
import org.junit.Test;

import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the navigation service table based implementation.
 *
 * @author Rahul Bhattacharjee
 */
public class TestNavigationServiceTableImpl {

    //@Test
    public void testNavigationService() {
        OrdersDAO ordersDAO = mock(OrdersDAO.class);

        OrderInfo info = mock(OrderInfo.class);
        when(info.getOrderId()).thenReturn("123456");
        when(info.getCoveredOrderId()).thenReturn("98765");
        when(ordersDAO.getOrder("123456")).thenReturn(info);

        OrderInfo parentInfo = mock(OrderInfo.class);
        when(parentInfo.getOrderId()).thenReturn("98765");
        when(parentInfo.getCoveredOrderId()).thenReturn(null);
        when(ordersDAO.getOrder("98765")).thenReturn(parentInfo);

        when(ordersDAO.getParentOrder("123456")).thenReturn(parentInfo);
        when(ordersDAO.getCoveringOrders("98765")).thenReturn(Collections.singletonList(info));
        when(ordersDAO.getCoveringOrders("123456")).thenReturn(Collections.EMPTY_LIST);

        NavigationService<OrderNode> navigationService = new NavigationServiceUsingOrderTableImpl();
        ((NavigationServiceUsingOrderTableImpl)navigationService).setOrdersDAO(ordersDAO);

        OrderNode parentOrder = navigationService.getCompleteTree("123456");
        System.out.println(parentOrder);
    }

    //@Test
    public void testNavigationServiceStartingWithParent() {
        OrdersDAO ordersDAO = mock(OrdersDAO.class);

        OrderInfo info = mock(OrderInfo.class);
        when(info.getOrderId()).thenReturn("123456");
        when(info.getCoveredOrderId()).thenReturn("98765");
        when(ordersDAO.getOrder("123456")).thenReturn(info);

        OrderInfo parentInfo = mock(OrderInfo.class);
        when(parentInfo.getOrderId()).thenReturn("98765");
        when(parentInfo.getCoveredOrderId()).thenReturn(null);
        when(ordersDAO.getOrder("98765")).thenReturn(parentInfo);

        when(ordersDAO.getParentOrder("123456")).thenReturn(parentInfo);
        when(ordersDAO.getCoveringOrders("98765")).thenReturn(Collections.singletonList(info));
        when(ordersDAO.getCoveringOrders("123456")).thenReturn(Collections.EMPTY_LIST);

        NavigationService<OrderNode> navigationService = new NavigationServiceUsingOrderTableImpl();
        ((NavigationServiceUsingOrderTableImpl)navigationService).setOrdersDAO(ordersDAO);

        OrderNode parentOrder = navigationService.getCompleteTree("98765");
        System.out.println(parentOrder);
    }

}

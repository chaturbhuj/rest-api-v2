package com.integral.ds.emscope.orders;

import com.integral.ds.dto.Order;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests narrative generator.
 *
 * @author Rahul Bhattacharjee
 */
public class TestNarrativeGenerator {

    //@Test
    public void testNarrativeGenerationWithMarketRange() {
        Order order = getBaseOrder();
        when(order.getMarketRange()).thenReturn(new BigDecimal(0.0002));

        NarrativeGenerator narrativeGenerator = new NarrativeGenerator();
        String narrative = narrativeGenerator.getNarrative(order);

        System.out.println("Narrative " + narrative);
    }

    //@Test
    public void testNarrativeGenerationWithoutMarketRange() {
        Order order = getBaseOrder();
        NarrativeGenerator narrativeGenerator = new NarrativeGenerator();
        String narrative = narrativeGenerator.getNarrative(order);

        System.out.println("Narrative " + narrative);
    }

    private Order getBaseOrder() {
        Order order = mock(Order.class);
        when(order.getOrderID()).thenReturn("123456");
        when(order.getOrg()).thenReturn("Alior");
        when((order.getDealer())).thenReturn("BOAP");
        when((order.getOrderType())).thenReturn("MKT");
        when(order.getBuySell()).thenReturn("Buy");
        when(order.getCcyPair()).thenReturn("EURUSD");
        when(order.getOrderAmt()).thenReturn(1234578L);
        when(order.getOrderRate()).thenReturn("1.2469");
        when(order.getTimeInForce()).thenReturn("GTD");
        when(order.getCreated()).thenReturn(new Date().getTime());
        when(order.getDealt()).thenReturn("EURUSD");
        when(order.getOrderStatus()).thenReturn("X");
        return order;
    }
}

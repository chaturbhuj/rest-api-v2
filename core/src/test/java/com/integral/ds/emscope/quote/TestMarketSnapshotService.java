package com.integral.ds.emscope.quote;

import com.integral.ds.dao.OrdersWithTradesDAO;
import com.integral.ds.emscope.orderswithtrade.OrdersWithTradesAppDataContext;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class TestMarketSnapshotService {

    //@Test
    public void testMarketSnapshotService() {
        String orderId = "511590502";
        BasicDataSource dataSource = getBasicDataSource();
        try {
            OrdersWithTradesDAO tradesDAO = new OrdersWithTradesDAO(dataSource);
            List<String> tradeIds = tradesDAO.getTradeIdsForOrder(orderId);

            System.out.println("Trade Ids " + tradeIds);

            OrdersWithTradesAppDataContext ordersWithTradesAppDataContext = new OrdersWithTradesAppDataContext();
            ordersWithTradesAppDataContext.setOrdersWithTradesDAO(tradesDAO);

            List<String> resultJsons = new ArrayList<String>();

            for(String tradeId : tradeIds) {
                String result = ordersWithTradesAppDataContext.getMarketSnapShotForTrade(tradeId);
                System.out.println("Result " + result);
                resultJsons.add(result);
            }
            String finalJsonString = createJson(resultJsons);
            System.out.println("Final json string " + finalJsonString);
        } finally {
            if(dataSource != null) {
                try {
                    dataSource.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String createJson(List<String> resultJsons) {
        if(resultJsons.isEmpty()) {
            return "";
        }

        String json = StringUtils.join(resultJsons,",");
        json = "[" + json + "]";
        return json;
    }

    private String getJsonMarketSnapshot(OrdersWithTradesAppDataContext dataContext,String tradeId) {
        return dataContext.getMarketSnapShotForTrade(tradeId);
    }

    public BasicDataSource getBasicDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUsername("emscope");
        dataSource.setPassword("Integral4309");
        dataSource.setUrl("jdbc:postgresql://eastot.chgyeswrwlad.us-east-1.rds.amazonaws.com:5432/eastOT");
        return dataSource;
    }
}

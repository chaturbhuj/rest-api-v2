package com.integral.ds.emscope.quote;

import com.integral.ds.emscope.quote.impl.RateQuoteServiceCompleteDownload;
import com.integral.ds.emscope.quote.impl.RateQuoteServiceWithPrevailingRate;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.springframework.test.util.AssertionErrors.assertTrue;

/**
 * @author Rahul Bhattacharjee
 */
public class TestQuoteServiceFactory {

    //@Test
    public void testQuoteServiceFactoryForParseSamples() throws Exception {
        String startTimestamp = "01.22.2014 02:55:00.000";
        String endTimestamp =   "01.22.2014 03:55:00.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date start = dateFormat.parse(startTimestamp);
        Date end = dateFormat.parse(endTimestamp);

        int samples = 2;

        RatesQuoteService quoteService = QuoteServiceFactory.getRateQuoteService(start,end,samples);
        assertTrue("Quote service is not instance of RateQuoteServiceCompletedDownload",quoteService instanceof RateQuoteServiceCompleteDownload);
    }

    //@Test
    public void testQuoteServiceFactoryForFinerSamples() throws Exception {
        String startTimestamp = "01.22.2014 02:55:00.000";
        String endTimestamp =   "01.22.2014 03:55:00.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date start = dateFormat.parse(startTimestamp);
        Date end = dateFormat.parse(endTimestamp);

        int samples = 10;

        RatesQuoteService quoteService = QuoteServiceFactory.getRateQuoteService(start,end,samples);
        assertTrue("Quote service is not instance of RateQuoteServiceCompletedDownload",quoteService instanceof RateQuoteServiceWithPrevailingRate);
    }
}

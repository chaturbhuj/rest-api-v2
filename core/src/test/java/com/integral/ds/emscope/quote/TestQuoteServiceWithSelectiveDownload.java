package com.integral.ds.emscope.quote;

import com.integral.ds.emscope.quote.impl.RateQuoteServiceCompleteDownload;
import com.integral.ds.emscope.quote.impl.RateQuoteServiceWithSelectiveDownload;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class TestQuoteServiceWithSelectiveDownload {

    private RatesQuoteService quoteService = new RateQuoteServiceWithSelectiveDownload();

    //@Test
    public void testQuoteAtTime()  throws Exception {
        String point = "01.22.2014 02:55:01.796";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        Date time = dateFormat.parse(point);

        String provider = "BOAN";
        String stream = "BandC";
        String ccypair = "AUDCAD";

        Quote quote = quoteService.getQuoteAtTime(provider,stream,ccypair,time);
        System.out.println("Quote " + quote);
    }

    //@Test
    public void testQuoteSamplesWithOneBucket() throws Exception {
        String startTimestamp = "02.12.2014 02:10:01.796";
        String endTimestamp = "02.13.2014 02:10:01.796";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        Date start = dateFormat.parse(startTimestamp);
        Date end = dateFormat.parse(endTimestamp);

        String provider = "BOAN";
        String stream = "BandC";
        String ccypair = "AUDUSD";

        long startTime = System.currentTimeMillis();

        List<Quote> quotes = quoteService.getSampledQuotes(provider,stream,ccypair,start,end,24);
        System.out.println("Quote list size " + quotes.size());

        for(Quote quote : quotes) {
            System.out.println("Quote time " + quote.getQuoteTime());
        }

        System.out.println("Time taken " + (System.currentTimeMillis()-startTime)/(1000) + " sec");
        System.out.println("Size is " + quotes.size());
    }

    //@Test
    public void testQuoteSamplesWithMultipleBuckets() throws Exception {
        String startTimestamp = "04.03.2014 02:00:01.796";
        String endTimestamp = "04.03.2014 22:00:01.796";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        Date start = dateFormat.parse(startTimestamp);
        Date end = dateFormat.parse(endTimestamp);

        /*
        String provider = "BARX";
        String stream = "TIER3";
        String ccypair = "AUDUSD";
        */

        String provider = "BOAN";
        String stream = "BandC";
        String ccypair = "AUDUSD";

        long start1 = System.currentTimeMillis();

        List<Quote> quotes = quoteService.getSampledQuotes(provider,stream,ccypair,start,end,78);

        for(Quote quote : quotes) {
            System.out.println("Quote time " + dateFormat.format(quote.getQuoteTime()));
            for(RestRatesObject rate : quote.getRates()) {
                System.out.println("   " + dateFormat.format(rate.getTmstmp()));
            }
        }

        System.out.println("Time taken  " + (System.currentTimeMillis()- start1));
        System.out.println("Quotes size " + quotes.size());
    }
}

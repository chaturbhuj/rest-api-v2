package com.integral.ds.emscope.quote;

import com.integral.ds.dto.ProviderStream;
import com.integral.ds.emscope.quote.impl.AggregatedQuoteServiceImpl;
import com.integral.ds.model.AggregatedBookTuple;
import com.integral.ds.model.QuoteBook;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Aggregated Quote Service.
 *
 * @author Rahul Bhattacharjee
 */
public class TestAggregatedQuoteService {

    //@Test
    public void testAggregatedQuoteService() throws Exception {
        init();
        try {
            createBook();
        }finally {
            close();
        }
    }

    private void createBook() throws Exception {
        String startTimestamp = "02.12.2014 02:10:01.796";
        String endTimestamp = "02.12.2014 02:20:01.796";

        //String provider = "BOAN,BNPP,CITI,HSBC";
        //String stream = "BandC,GREEN,Integral1,IntGloInst1";

        String provider = "BOAN";
        String stream = "BandC";

        String ccyPair = "AUDUSD";
        int samples = 10;
        int bookSize = 10;

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        Date start = dateFormat.parse(startTimestamp);
        Date end = dateFormat.parse(endTimestamp);

        AggregatedQuoteService quoteService = new AggregatedQuoteServiceImpl();

        List<ProviderStream> providerStreamList = getProviderStreamList(getParsedContent(provider), getParsedContent(stream));

        List<QuoteBook> quoteBooks = quoteService.getAggregatedBook(providerStreamList,ccyPair,start,end,samples,bookSize);

        for(QuoteBook book : quoteBooks) {
            log("===START of Book===");
            printBook(book);
            log("===END of Book===");
        }
    }

    private void printBook(QuoteBook book) {
        List<AggregatedBookTuple> tuples = book.getTuples();
        for(AggregatedBookTuple tuple : tuples) {
            log(tuple.toString());
        }
    }

    private List<String> getParsedContent(String streamString) {
        if(streamString == null || StringUtils.isBlank(streamString)) {
            return Collections.EMPTY_LIST;
        }
        List<String> streamList = new ArrayList<String>();
        String[] streams = streamString.split(",");
        for(String stream : streams) {
            streamList.add(stream);
        }
        return streamList;
    }

    private List<ProviderStream> getProviderStreamList(List<String> provider,List<String> stream) {
        if(provider.isEmpty() || stream.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        List<ProviderStream> providerStreams = new ArrayList<ProviderStream>();
        for(int index = 0 ; index < provider.size() ; index++) {
            String providerName = provider.get(index);
            String streamName = stream.get(index);
            providerStreams.add(new ProviderStream(providerName,streamName));
        }
        return providerStreams;
    }

    private PrintWriter writer;
    private String fileName = "C:\\Users\\bhattacharjeer\\Desktop\\Report\\rate_book.txt";
    private void init() {
        try {
            File file = new File(fileName);
            writer = new PrintWriter(file);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void log(String msg) {
        //System.out.println(msg);
        writer.write(msg+"\n");
    }

    private void close() {
        if(writer != null){
            writer.close();
        }
    }
}

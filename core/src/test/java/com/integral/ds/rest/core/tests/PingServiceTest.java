package com.integral.ds.rest.core.tests;

import com.integral.ds.rest.core.RestRunnerUtil;
import com.integral.ds.rest.core.VerifyResponse;
import org.junit.Test;

import java.io.InputStream;

/**
 * Tests the Ping REST service.
 *
 * @author Rahul
 */
public class PingServiceTest {

    //@Test
    public void testPingService() {
        RestRunnerUtil runner = new RestRunnerUtil();
        int noOfThreads = 5;
        int noOfRequestsToFire = 100;

        runner.run("http://localhost:8080/emscope/rest/ping/test", noOfThreads, noOfRequestsToFire, new VerifyResponse() {
            @Override
            public boolean verifyResponse(InputStream in) {
                if(in == null) {
                    return false;
                } else {
                    return true;
                }
            }
        });

    }

}
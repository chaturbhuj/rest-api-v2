package com.integral.ds.rest.core;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Capture stats from the REST run.
 *
 * @author Rahul
 */
public class Stats {

    private AtomicInteger success = new AtomicInteger();
    private AtomicInteger failures = new AtomicInteger();

    public void success() {
        success.incrementAndGet();
    }

    public void failure() {
        failures.incrementAndGet();
    }

    @Override
    public String toString() {
        return "Stats{" +
                "success=" + success +
                ", failures=" + failures +
                '}';
    }
}

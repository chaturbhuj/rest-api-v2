package com.integral.ds.rest.core;

import java.io.InputStream;

/**
 * Implementation of this interface can be used for verifying responses from REST call.
 *
 * @author Rahul Bhattacharjee
 */
public interface VerifyResponse {
    /**
     * Returns true , if response as expected or returns false.
     *
     * @param in
     * @return
     */
    public boolean verifyResponse(InputStream in);

}

package com.integral.ds.rest.core.tests;

import com.integral.ds.dto.Message;
import com.owlike.genson.Genson;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.test.framework.JerseyTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Ping service jersey test.
 *
 * @author Rahul Bhattacharjee
 */
public class PingServiceJerseyTest extends JerseyTest {

    public PingServiceJerseyTest() throws Exception {
        super("com.integral.ds.resources");
    }

    //@Test
    public void testPingService() {
        final String input = "test";
        WebResource webResource = resource();
        String responseMsg = webResource.path("ping/" + input).get(String.class);

        //System.out.println("Response  => " + responseMsg);

        Genson genson = new Genson();
        try {
            Message message = genson.deserialize(responseMsg,Message.class);
            assertEquals(message.getMessage(),input);
        }catch (Exception e) {
            fail();
        }
    }
}

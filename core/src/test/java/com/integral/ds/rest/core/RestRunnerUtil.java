package com.integral.ds.rest.core;

import java.io.InputStream;

import java.net.URL;
import java.net.URLConnection;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Tool to invoke REST service endpoint.This can be used to fire multiple requests using multiple threads.
 *
 * @author Rahul Bhattacharjee
 */
public class RestRunnerUtil {

    public void run(String endPoint , int noOfThreads , int numberOfRequests , VerifyResponse response) {
        if(noOfThreads <=0) {
            noOfThreads = 1;
        }
        Stats stats = new Stats();
        ExecutorService executorService = Executors.newFixedThreadPool(noOfThreads);

        long start = System.currentTimeMillis();

        for(int i = 0 ; i < numberOfRequests ; i++) {
            executorService.submit(new RestRunner(endPoint,response,stats));
        }

        executorService.shutdown();
        try {
            executorService.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long endTime = System.currentTimeMillis();

        System.out.println("Run stats : " + stats);
        System.out.println("Time taken (ms): " + (endTime-start));
    }

    private static class RestRunner implements Runnable {
        String endPoint;
        VerifyResponse verifyResponse;
        Stats stats;

        RestRunner(String endPoint,VerifyResponse verifyResponse ,Stats stats) {
            this.endPoint = endPoint;
            this.verifyResponse = verifyResponse;
            this.stats = stats;
        }

        @Override
        public void run() {
            try {
                URL url = new URL(endPoint);

                URLConnection urlConnection = url.openConnection();
                urlConnection.setDoOutput(true);

                InputStream in = urlConnection.getInputStream();
                boolean responseStatus = verifyResponse.verifyResponse(in);

                if(responseStatus) {
                    stats.success();
                } else {
                    stats.failure();
                }

            } catch (Exception e) {
                e.printStackTrace();
                stats.failure();
            }
        }
    }
}

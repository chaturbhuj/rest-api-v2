package com.integral.ds.dao;

import com.integral.ds.emscope.trades.TradeAppDataContext;
import com.integral.ds.resources.TradesCollectionResource;
import org.apache.commons.dbcp.BasicDataSource;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
//import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Rahul Bhattacharjee
 */
public class BaseDaoTest {

    private String url = "jdbc:postgresql://dev2.chgyeswrwlad.us-east-1.rds.amazonaws.com:5432/eastOT";
    private String userName = "fxbenchmarkadmin";
    private String password = "Integral4309";

    //@Test
    public void testTradesDao() {
        BasicDataSource dataSource = getDataSource();
        Connection connection = null;
        try {

            try
            {
                connection = dataSource.getConnection() ;
                String sqlString = "select array_to_json(array_agg(row_to_json(t)))" +
                        "  from (select CREATED as CREATEDTIMESTAMP, CCYPAIR, BUYSELL, STATUS AS OrderStatus, ORG,"
                        + "ORDERTYPE, ORDERID, PERSISTENT, CLIENTORDERID, LASTEVENT,"
                        + "TENOR, ACCOUNT, DEALER, DEALT, TIMEINFORCE, ORDERRATE, ORDERAMT, FILLEDAMT, " +
                        " FILLEDPERCENT As FILLEDPCT, FILLRATE, PI, ORDERAMTUSD AS OrdAmtUSD, " +
                        " FILLEDAMTUSD FILLAMTUSD, PIPNL AS PPNL, "   +
                        " valuedate as VALUEDATE, businessdate as ORDERDATE,MARKETRANGE as MARKETRANGE,expirytime as \"Expiry Time\"," +
                        " visibility as Visibility,server as Server,servername as \"Server Name\"," +
                        "servermanaged as \"Server Managed\"," +
                        "fixSessionId as \"FIX Session\"," +
                        "CptyId as \"Cover Cpty Cover\",originatingorg as \"Originating Org\",ordertype as \"Order Type\",coverExecMethod as \"Cover Execution Method\"," +
                        "channel as Channel,pricedisplay as \"Price Display\",executionstrategy as \"Execution Strategy\"," +
                        "fixingDate as \"Fixing Date\",preferredProviders as \"Preferred Proviers\"," +
                        "customSpreads as \"Custom Spot Spreads\",portfolioTID as \"Portfolio ID\",executionInstruction as \"Execution Instruction\"," +
                        "executiontype as \"Execution Type\",minfillqty as \"Min Fill Qty\",showamt as \"Show Amount\",coveredcpty as \"Covered cpty\"," +
                        "coveredcptyuser as \"Covered Cpty User\"" +
                        ",originatinguser as \"Originating User\"   " +
                        "from ORDER_MASTER where ORDERID = ?) t";
                PreparedStatement statementUpdate=connection.prepareStatement( sqlString );
                statementUpdate.setString(1,"527802390");

                ResultSet result=statementUpdate.executeQuery();
                while (result.next())
                {
                    String json = result.getString( 1 );
                    System.out.println(json);
                    try
                    {
                        final JSONArray obj = new JSONArray(json);
                      //  Object mkr=obj.get( "marketRange" );
                        System.out.println("object :");
                    }
                    catch ( JSONException e )
                    {
                        e.printStackTrace();
                    }
                }
            }
            catch ( SQLException e )
            {
                e.printStackTrace();
            }
            finally
            {
                if (connection != null)
                {
                    try
                    {
                        connection.close();
                    }
                    catch ( SQLException e )
                    {
                        e.printStackTrace();
                    }
                }
            }
            /*TradeDAO tradeDAO = new TradeDAO(dataSource);
            TradeAppDataContext tradeAppDataContext = new TradeAppDataContext();
            tradeAppDataContext.setTradeDAO(tradeDAO);

            TradesCollectionResource tradesCollectionResource = new TradesCollectionResource();
            tradesCollectionResource.setContext(tradeAppDataContext);

            String tradeId = "FXI820622822";

            String trades = tradesCollectionResource.getTrades(tradeId);

            System.out.println("Trades " + trades);*/
        }finally {
            try {
                dataSource.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private BasicDataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl(url);
        dataSource.setUsername(userName);
        dataSource.setPassword(password);
        return dataSource;
    }

    public static void main( String[] args )
    {
        BaseDaoTest test = new BaseDaoTest();
        test.testTradesDao();
    }
}

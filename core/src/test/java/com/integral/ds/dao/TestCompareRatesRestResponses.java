package com.integral.ds.dao;

import com.integral.ds.dao.s3.S3FilesRatesDao;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.emscope.rates.RestRatesObject;
import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Test;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Rahul Bhattacharjee
 */
public class TestCompareRatesRestResponses {

    private static boolean isLoggerEnabled = true;

    private String [] provider_stream_list = {"BOAN,BandC","BOAN,BandF","BOAN,BandD","BOAN,BandG","CITI,Integral1","CRSU,FXIN_C"
    ,"CRSU,FXIN","GSFX,Retail","GSFX,Priority","HSBC,IntGloInst1","JPM,Gold","JPM,Blue","JPM,Platinum","MHDP,StreamMC",
    "MSFX,FXIN_M_1","MSFX,FXIN_11","RBS,integralfull","RBS,integralset0","RBS,integralset1a","SG,PREA_1","UBS,FXPRO","UBS,Super"
    ,"UBS,Medium","UBS,MIG_NTG","UBS,Premium","BARX,FXPRO","BNPP,GREEN","SG,STDA_1","DB,EU0196733","VTFX,VFX0","CHPT,StreamNY",
    "NTFX,stream1","ADSS,Stream1","HSFX,AXICORP"};

    //private String [] ccyPairs = {"AUDUSD","EURGBP","EURCHF","NZDUSD","USDCAD","USDJPY"};
    private String [] ccyPairs = {"AUDUSD","EURGBP","EURCHF"};

    private static FileOutputStream writer = null;

    private static String fileName = "C:\\\\Users\\\\bhattacharjeer\\\\Desktop\\\\RatesAudit\\\\rates_log.txt";

    static {
        try {
            File file = new File(fileName);
            writer = new FileOutputStream(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void log(String msg) {
        if(isLoggerEnabled) {
            System.out.println(msg);
            try {
                writer.write(msg.getBytes());
                writer.write("\n".getBytes());
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //@Test
    public void testMonthsDataForAllProvider() {
        for(String providerString : provider_stream_list) {
            String [] splits = providerString.split(",");
            for(int month = 1 ; month < 2 ; month++) {
                String provider = splits[0];
                String stream = splits[1];
                log("Provider " + provider + " stream " + stream);
                runForEntireMonth(provider,stream,month);
            }
        }
    }

    private void runForEntireMonth(String provider, String stream, int month) {
        for(int day = 1 ; day < 30 ; day++) {
            runForDay(provider, stream, month, day);
        }
    }

    private void runForDay(String provider, String stream, int month, int day) {
        int [] ignore = {1,2,8,9,15,16,22,23};
        for(int ignoreDay  : ignore) {
            log("Ignoring day " + day);
            if(day == ignoreDay){
                return;
            }
        }

        for(int hour = 0 ; hour < 24 ; hour = hour + 18) {
            String start = getStart(month,day,hour);
            String end = getEnd(month, day, hour);
            for(String ccyPair : ccyPairs) {
                log("Running for start " + start + " end " + end + " hour " + hour);
                runForTimeRange(provider,stream,ccyPair, start,end);
            }
        }
    }

    private void runForTimeRange(String provider, String stream, String ccypair, String start, String end) {
        try {

            log("Running for provider " + provider + " stream " + stream + " ccypair " + ccypair + " start " + start + " end " + end);

            BasicDataSource dataSource = new BasicDataSource();
            dataSource.setDriverClassName("org.postgresql.Driver");
            dataSource.setUrl("jdbc:postgresql://dw-1.czmcljkbj3gl.us-east-1.redshift.amazonaws.com:5439/portalwarehouse");
            dataSource.setUsername("fxbenchmarkadmin");
            dataSource.setPassword("Integral4309");

            //RatesDAO redshiftRatesDao = null;
            S3FilesRatesDao s3RatesDao = new S3FilesRatesDao();

            SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

            Date startTime = dateFormat.parse(start);
            Date endTime = dateFormat.parse(end);

            //List<RatesObject> ratesFromRedshift = redshiftRatesDao.getRates(provider, stream, ccypair, startTime, endTime);
            //Set<ComparableRateObject> comparableRateObjectsOne = getNormalizedRates(ratesFromRedshift);
            
            List<RatesObject> ratesFromRedshift = null;
            Set<ComparableRateObject> comparableRateObjectsOne = null;
            
            List<RestRatesObject> ratesFromS3 = s3RatesDao.getRates(provider, stream, ccypair, startTime, endTime);
            Set<ComparableRateObject> comparableRateObjectsTwo = getNormalizedRestRatesObject(ratesFromS3);

            Set<ComparableRateObject> result = difference(comparableRateObjectsOne, comparableRateObjectsTwo);

            log("Size of diff set " + result.size());
            log("Size redshift " + comparableRateObjectsOne.size());
            log("Size s3 " + comparableRateObjectsTwo.size());
            //print(result);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getEnd(int month, int day, int hour) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(prependZeroIfLessThanTen(month));
        stringBuilder.append(".");
        stringBuilder.append(prependZeroIfLessThanTen(day));
        stringBuilder.append(".");
        stringBuilder.append("2014 ");
        stringBuilder.append(hour);
        stringBuilder.append(":59:00.000");
        return stringBuilder.toString();
    }

    private String prependZeroIfLessThanTen(int day) {
        String dayString = day + "";
        if(day < 10) {
            dayString = "0" + dayString;
        }
        return dayString;
    }

    private String getStart(int month, int day, int hour) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(prependZeroIfLessThanTen(month));
        stringBuilder.append(".");
        stringBuilder.append(prependZeroIfLessThanTen(day));
        stringBuilder.append(".");
        stringBuilder.append("2014 ");
        stringBuilder.append(hour);
        stringBuilder.append(":00:00.000");
        return stringBuilder.toString();
    }

    private void getRateObjectForTimestamp(Set<ComparableRateObject> comparableRateObjects, long timestamp) {
        for(ComparableRateObject rateObject : comparableRateObjects) {
            if(timestamp == rateObject.getTmstmp()) {
                log(rateObject.toString());
            }
        }
    }

    private void getRateObjectForGuid(Set<ComparableRateObject> comparableRateObjects, String guid) {
        for(ComparableRateObject rateObject : comparableRateObjects) {
            if(guid.equalsIgnoreCase(rateObject.getGuid())) {
                log(rateObject.toString());
            }
        }
    }

    private void print(Set<ComparableRateObject> set) {
        for(ComparableRateObject rateObject : set) {
            log(rateObject.toString());
        }
    }

    private Set<ComparableRateObject> difference(Set<ComparableRateObject> one , Set<ComparableRateObject> two) {
        int length1 = one.size();
        int length2 = two.size();

        if((length1-length2) > 0) {
            log("Redshift has more records by " + (length1 - length2));
            one.removeAll(two);
            return one;
        } else {
            log("S3 has more records by " + (length2 - length1));
            two.removeAll(one);
            return two;
        }
    }

    private Set<ComparableRateObject> getNormalizedRates(List<RatesObject> ratesObjects) {
        Set<ComparableRateObject> returnSet = new HashSet<>();
        RecordTransformer<ComparableRateObject,RatesObject> recordTransformer = new RateObjectTransformer();
        for(RatesObject ratesObject : ratesObjects) {
            returnSet.add(recordTransformer.transform(ratesObject));
        }
        return returnSet;
    }

    private Set<ComparableRateObject> getNormalizedRestRatesObject(List<RestRatesObject> ratesObjects) {
        Set<ComparableRateObject> returnSet = new HashSet<>();
        RecordTransformer<ComparableRateObject,RestRatesObject> recordTransformer = new RestRatesObjectTransformer();
        for(RestRatesObject ratesObject : ratesObjects) {
            returnSet.add(recordTransformer.transform(ratesObject));
        }
        return returnSet;
    }

    private static class ComparableRateObject {
        private Long tmstmp;
        private float bidPrice;
        private float bidSize;
        private float askPrice;
        private float askSize;
        private String status;
        private String guid;
        private Integer tier;

        private Long getTmstmp() {
            return tmstmp;
        }

        private void setTmstmp(Long tmstmp) {
            this.tmstmp = tmstmp;
        }

        private float getBidPrice() {
            return bidPrice;
        }

        private void setBidPrice(float bidPrice) {
            this.bidPrice = bidPrice;
        }

        private float getBidSize() {
            return bidSize;
        }

        private void setBidSize(float bidSize) {
            this.bidSize = bidSize;
        }

        private float getAskPrice() {
            return askPrice;
        }

        private void setAskPrice(float askPrice) {
            this.askPrice = askPrice;
        }

        private float getAskSize() {
            return askSize;
        }

        private void setAskSize(float askSize) {
            this.askSize = askSize;
        }

        private String getStatus() {
            return status;
        }

        private void setStatus(String status) {
            this.status = status;
        }

        private String getGuid() {
            return guid;
        }

        private void setGuid(String guid) {
            this.guid = guid;
        }

        private Integer getTier() {
            return tier;
        }

        private void setTier(Integer tier) {
            this.tier = tier;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ComparableRateObject that = (ComparableRateObject) o;

            if (Float.compare(that.askPrice, askPrice) != 0) return false;
            if (Float.compare(that.askSize, askSize) != 0) return false;
            if (Float.compare(that.bidPrice, bidPrice) != 0) return false;
            if (Float.compare(that.bidSize, bidSize) != 0) return false;
            if (!guid.equals(that.guid)) return false;
            if (!status.equals(that.status)) return false;
            if (!tier.equals(that.tier)) return false;
            if (!tmstmp.equals(that.tmstmp)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = tmstmp.hashCode();
            result = 31 * result + (bidPrice != +0.0f ? Float.floatToIntBits(bidPrice) : 0);
            result = 31 * result + (bidSize != +0.0f ? Float.floatToIntBits(bidSize) : 0);
            result = 31 * result + (askPrice != +0.0f ? Float.floatToIntBits(askPrice) : 0);
            result = 31 * result + (askSize != +0.0f ? Float.floatToIntBits(askSize) : 0);
            result = 31 * result + status.hashCode();
            result = 31 * result + guid.hashCode();
            result = 31 * result + tier.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return "ComparableRateObject{" +
                    "tmstmp=" + tmstmp +
                    ", bidPrice=" + bidPrice +
                    ", bidSize=" + bidSize +
                    ", askPrice=" + askPrice +
                    ", askSize=" + askSize +
                    ", status='" + status + '\'' +
                    ", guid='" + guid + '\'' +
                    ", tier=" + tier +
                    '}';
        }
    }

    private static interface RecordTransformer<T,R> {
        public T transform(R result);
    }

    private static class RateObjectTransformer implements RecordTransformer<ComparableRateObject,RatesObject> {
        private static final long MILL_OFFSET = 28800000L;

        @Override
        public ComparableRateObject transform(RatesObject result) {
            ComparableRateObject rateObject = new ComparableRateObject();
            rateObject.setTmstmp(result.getTmstmp() + MILL_OFFSET);
            rateObject.setBidSize(result.getBid_size().floatValue());
            rateObject.setBidPrice(result.getBid_price().floatValue());
            rateObject.setAskPrice(result.getAsk_price().floatValue());
            rateObject.setAskSize(result.getAsk_size().floatValue());
            rateObject.setStatus(result.getStatus());
            rateObject.setTier(result.getLvl());
            rateObject.setGuid(result.getGuid());
            return rateObject;
        }
    }

    private static class RestRatesObjectTransformer implements RecordTransformer<ComparableRateObject,RestRatesObject> {
        @Override
        public ComparableRateObject transform(RestRatesObject result) {
            ComparableRateObject rateObject = new ComparableRateObject();
            rateObject.setTmstmp(result.getTmstmp());
            rateObject.setBidSize(result.getBid_size().floatValue());
            rateObject.setBidPrice(result.getBid_price().floatValue());
            rateObject.setAskPrice(result.getAsk_price().floatValue());
            rateObject.setAskSize(result.getAsk_size().floatValue());
            rateObject.setStatus(result.getStatus());
            rateObject.setTier(result.getLvl());
            rateObject.setGuid(result.getGuid());
            return rateObject;
        }
    }
}


package com.integral.ds.dao;

import com.integral.ds.dto.UserInfo;
import com.integral.ds.epa.dao.EPADao;
import com.integral.ds.epa.dao.EpaStatus;
import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class TestAuthDao extends BaseDatasourceSupportTest {

    //@Test
    public void testAuthDaoFetchAllUser() {
        BasicDataSource dataSource = getDataSource();
        AuthDAO authDAO = new AuthDAO(dataSource);

        try {
            List<UserInfo> users = authDAO.getAllUsers();
            System.out.println(users.size());
        } finally {
            try {
                dataSource.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            super.close(dataSource);
        }
    }

    //@Test
    public void testAuthDaoFetchSingleUser() {
        BasicDataSource dataSource = getDataSource();
        AuthDAO authDAO = new AuthDAO(dataSource);

        try {
            UserInfo user = authDAO.getUserInfoForUser("rahul");
            System.out.println(user);
        } finally {
            try {
                dataSource.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            super.close(dataSource);
        }
    }
}

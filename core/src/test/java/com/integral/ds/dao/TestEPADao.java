package com.integral.ds.dao;

import com.integral.ds.epa.dao.EPADao;
import com.integral.ds.epa.dao.EpaInfo;
import com.integral.ds.epa.dao.EpaStatus;
import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Test;

import java.sql.SQLException;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class TestEPADao extends BaseDatasourceSupportTest {

    //@Test
    public void testUpdateRecord() {
        BasicDataSource dataSource = getDataSource();
        EPADao epaDao = new EPADao(dataSource);

        try {
            epaDao.updateStatus(1403651054780l, EpaStatus.PROCESS_FAILURE,"Failed to process.");
        } finally {
            try {
                dataSource.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            super.close(dataSource);
        }
    }

    //@Test
    public void testUpdateEmail() {
        BasicDataSource dataSource = getDataSource();
        EPADao epaDao = new EPADao(dataSource);

        try {
            epaDao.updateEmailOfCustomer(1403651054780l,"rahul@integral.com");
        } finally {
            try {
                dataSource.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            super.close(dataSource);
        }
    }

    //@Test
    public void testGetAllEpaInfos() {
        BasicDataSource dataSource = getDataSource();
        EPADao epaDao = new EPADao(dataSource);

        try {
            List<EpaInfo> infos = epaDao.getCompletedRequests();
            for(EpaInfo info : infos) {
                System.out.println(info);
            }
        } finally {
            try {
                dataSource.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            super.close(dataSource);
        }
    }
}

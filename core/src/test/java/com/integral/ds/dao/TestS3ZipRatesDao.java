package com.integral.ds.dao;

import com.integral.ds.dao.s3.S3FilesRatesDao;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.emscope.rates.RestRatesObject;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Tests rates dao.Rates dao using S3 zip files.
 */
public class TestS3ZipRatesDao {

    //@Test
    public void testS3MultipleS3FilesRatesDao() throws Exception {
        String startTimestamp = "01.22.2014 02:55:01.795";
        String endTimestamp =   "01.22.2014 02:55:01.797";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        S3FilesRatesDao ratesDao = new S3FilesRatesDao();

        String provider = "BOAN";
        String stream = "BandC";
        String ccypair = "AUDCAD";

        long start = System.nanoTime();

        List<RestRatesObject> ratesObjects = ratesDao.getRates(provider,stream,ccypair,startTime,endTime);
        System.out.println("Size of the return list => " + ratesObjects.size());

        for(RestRatesObject ratesObject : ratesObjects) {
            long time = ratesObject.getTmstmp();
            Date date = new Date(time);
            System.out.println(ratesObject.getLvl());
            System.out.println(dateFormat.format(date));
        }

        long end = System.nanoTime();
        System.out.println("Time taken " + ((end - start)/1000000000.0) + " sec.");
    }

    //@Test
    public void testS3SingleS3FilesRatesDao() throws Exception {
        String startTimestamp = "2014-04-10 00:08:19.000";
        String endTimestamp =   "2014-04-10 00:08:20.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("y-MM-dd HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        S3FilesRatesDao ratesDao = new S3FilesRatesDao();

        String provider = "CFSS";
        String stream = "StreamC";
        String ccypair = "NZDUSD";

        List<RestRatesObject> ratesObjects = ratesDao.getRates(provider,stream,ccypair,startTime,endTime);

        System.out.println("Size of the return list => " + ratesObjects.size());

        for(RestRatesObject ratesObject : ratesObjects) {
            long time = ratesObject.getTmstmp();
            Date date = new Date(time);
            System.out.println("Rate = > " + dateFormat.format(date));
        }
    }

    //@Test
    public void testS3MultipleS3FilesRatesDaoForExoticCurrencyPairs() throws Exception {
        String startTimestamp = "01.22.2014 23:58:00.000";
        String endTimestamp =   "01.23.2014 00:02:00.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        S3FilesRatesDao ratesDao = new S3FilesRatesDao();

        String provider = "CITI";
        String stream = "Integral1";
        String ccypair = "NOKJPY";

        long start = System.nanoTime();

        List<RestRatesObject> ratesObjects = ratesDao.getRates(provider,stream,ccypair,startTime,endTime);
        System.out.println("Size of the return list => " + ratesObjects.size());

        for(RestRatesObject ratesObject : ratesObjects) {
            long time = ratesObject.getTmstmp();
            Date date = new Date(time);
            System.out.println("date " + date);
            System.out.println("ccypair " + ratesObject.getCcyPair());
        }

        long end = System.nanoTime();
        System.out.println("Time taken " + ((end - start)/1000000000.0) + " sec.");
    }
}

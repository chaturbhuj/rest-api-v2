package com.integral.ds.dao;

import com.integral.ds.s3.S3StreamReader;
import org.junit.Test;

import java.io.InputStream;
import java.util.Random;

/**
 * @author Rahul Bhattacharjee
 */
public class TestS3DirectoryResponse {

    private static Random random = new Random(new Object().hashCode());

    //@Test
    public void testDirectoryListing() throws Exception {
        S3StreamReader s3StreamReader = new S3StreamReader();

        String path = getRandomPath();

        System.out.println("Path => " + path);

        long start = System.nanoTime();
        InputStream in = s3StreamReader.getInputStream(path);
        if(in != null) {
            System.out.println("Input stream hashcode => " + in.hashCode());
            System.out.println("Input stream bytes available => " + in.available());
        }
        long end = System.nanoTime();
        System.out.println("Time taken to get to the stream => " + ((end-start)/1000000));
    }

    private static String getRandomPath() {
        String basePath = "ymdpsc/2014/2014-03/";
        String date = getDate();
        String provider = getProvider();
        String stream = getStream();
        String currencyPair = getCurrencyPair();
        String fileName = getFileName();

        String path = basePath + date + "/" + provider + "/" + stream + "/" + currencyPair + "/" + fileName;

        // below path goes against the original s3 bucket.
        path = "processed/2014/2014-01/2014-01-22/BOAN/BandC/AUDCAD/processedLog_2014-01-22-02.zip";

        //path = "ymdpsc/2014/2014-02/2014-02-05/PRVD-10/STRM-2/AUDUSD/processedLog_2014-01-22-02.zip";

        //path = "ymdpsc-6months/2014/2014-03/2014-03-08/PRVD-19/STRM-2/USDCAD/processedLog_17.zip";

        return path;
    }

    private static String getDate() {
        int day = random.nextInt(22);
        if(day == 0) {
            return getDate();
        }
        String dayString = "2014-03-";
        String onlyDay = null;
        if(day < 10) {
            onlyDay = "0" + day;
        } else {
            onlyDay = day+"";
        }
        return dayString + onlyDay;
    }

    public static String getProvider() {
        int num = random.nextInt(50);
        if(num == 0) {
            return getProvider();
        }
        return "PRVD-" + num;
    }

    private static String getStream() {
        int num = random.nextInt(2);
        if(num == 0) {
            return getStream();
        }
        return "STRM-" + num;
    }

    private static String getCurrencyPair() {
        String [] ccPair = "AUDUSD,EURUSD,GBPUSD".split(",");
        int index = random.nextInt(ccPair.length-1);
        return ccPair[index];
    }

    private static String getFileName() {
        String file = "processedLog_";
        int num = random.nextInt(20);
        return file + num + ".zip";
    }
}

package com.integral.ds.dao;

import com.integral.ds.dto.OrderInfo;
import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

/**
 * Tests Order Dao.
 *
 * @author Rahul Bhattacharjee
 */
public class TestOrderDao extends BaseDatasourceSupportTest {

    //@Test
    public void testGetOrder() {
        BasicDataSource dataSource = getDataSource();
        OrdersDAO ordersDAO = new OrdersDAO(dataSource);

        try {
            OrderInfo order = ordersDAO.getOrder("580254609");
            System.out.println(order);
        } finally {
            super.close(dataSource);
        }
    }

    //@Test
    public void testParentOrderWithNoParent() {
        BasicDataSource dataSource = getDataSource();
        OrdersDAO ordersDAO = new OrdersDAO(dataSource);

        try {
            OrderInfo order = ordersDAO.getParentOrder("578908565");
            System.out.println(order);
        } finally {
            super.close(dataSource);
        }
    }

    //@Test
    public void testParentOrderWithParent() {
        BasicDataSource dataSource = getDataSource();
        OrdersDAO ordersDAO = new OrdersDAO(dataSource);

        try {
            OrderInfo order = ordersDAO.getParentOrder("580254609");
            System.out.println(order);
        } finally {
            super.close(dataSource);
        }
    }

    //@Test
    public void testGetCoveringOrders() {
        BasicDataSource dataSource = getDataSource();
        OrdersDAO ordersDAO = new OrdersDAO(dataSource);

        try {
            List<OrderInfo> coveringOrders = ordersDAO.getCoveringOrders("580254609");
            System.out.println(coveringOrders);
        } finally {
            super.close(dataSource);
        }
    }
}

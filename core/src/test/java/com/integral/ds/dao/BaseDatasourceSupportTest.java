package com.integral.ds.dao;

import org.apache.commons.dbcp.BasicDataSource;

import java.sql.SQLException;

/**
 * To be extended by DAO tests for connection,datasource.
 * @author Rahul Bhattacharjee
 */
public class BaseDatasourceSupportTest {

    private String url = "jdbc:postgresql://eastot.chgyeswrwlad.us-east-1.rds.amazonaws.com:5432/eastOT";
    private String userName = "emscope";
    private String password = "Integral4309";


    protected BasicDataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl(url);
        dataSource.setUsername(userName);
        dataSource.setPassword(password);
        return dataSource;
    }

    protected void close(BasicDataSource dataSource) {
        if(dataSource != null) {
            try {
                dataSource.close();
            } catch (SQLException e) {
                // ok to eat.
            }
        }
    }
}

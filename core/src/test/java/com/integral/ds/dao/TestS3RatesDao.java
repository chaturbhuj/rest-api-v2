package com.integral.ds.dao;

import com.integral.ds.dao.s3.S3RatesDao;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.emscope.rates.RestRatesObject;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * S3 indexed files rates implementation.
 *
 * @author Rahul Bhattacharjee
 */
public class TestS3RatesDao {

    //@Test
    public void testS3FilesRatesDao() {
        try {
            S3RatesDao ratesDao = new S3RatesDao();
            String provider = "BOAN";
            String stream = "BandD";
            String ccyPair = "EURUSD";

            String startTimestamp = "02.24.2014 15:01:00.000";
            String endTimestamp = "02.24.2014 15:02:02.067";

            SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

            Date startTime = dateFormat.parse(startTimestamp);
            Date endTime = dateFormat.parse(endTimestamp);

            long startTime1 = System.currentTimeMillis();

            List<RestRatesObject> ratesObjectList = ratesDao.getRatesFromIndexedS3(provider,stream,ccyPair,startTime,endTime);

            long endTime1 = System.currentTimeMillis();

            System.out.println("Time taken " + (endTime1-startTime1));

            System.out.println("Size of rate object list => " + ratesObjectList.size());

            System.out.println("Start time " + startTimestamp);
            System.out.println("End time " + endTimestamp);

        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}

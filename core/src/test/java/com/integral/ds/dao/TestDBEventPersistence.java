package com.integral.ds.dao;

import com.integral.ds.event.Event;
import com.integral.ds.event.EventAggregationBean;
import com.integral.ds.event.EventPersistence;
import com.integral.ds.event.EventType;
import com.integral.ds.event.impl.DBEventPersistenceImpl;
import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Test;

import java.util.Date;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class TestDBEventPersistence extends BaseDatasourceSupportTest {

    //@Test
    public void testInsertEvent() {
        BasicDataSource dataSource = getDataSource();
        try {
            EventPersistence eventPersistence = new DBEventPersistenceImpl(dataSource);
            Event event = new Event();
            event.setEventDate(new Date());
            event.setEventName(EventType.EventName.LANDSCAPE_SERVICE);
            event.setType(EventType.SERVICE);
            event.setUserName("rahul");
            eventPersistence.insertEvent(event);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            super.close(dataSource);
        }
    }

    //@Test
    public void testReporterQuery() {
        BasicDataSource dataSource = getDataSource();
        try {
            EventPersistence eventPersistence = new DBEventPersistenceImpl(getDataSource());
            List<EventAggregationBean> events = eventPersistence.getCurrentAggregationReport();

            for(EventAggregationBean event : events) {
                System.out.println("Event " + event);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            super.close(dataSource);
        }
    }
}

package com.integral.ds.dao;

import com.integral.ds.dto.TradeInfo;
import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Test;

import java.util.List;

/**
 * Test class for trade master dao.
 *
 * @author Rahul Bhattacharjee
 */
public class TestTradeMasterDao extends BaseDatasourceSupportTest {

    //@Test
    public void testGetTradesForOrder() {
        BasicDataSource dataSource = getDataSource();
        TradeMasterDao tradeMasterDao = new TradeMasterDao(dataSource);

        try {
            List<TradeInfo> tradeInfos = tradeMasterDao.getTradesForOrder("578908565");
            System.out.println(tradeInfos.size());
        } finally {
            super.close(dataSource);
        }
    }

    //@Test
    public void testGetTrade() {
        BasicDataSource dataSource = getDataSource();
        TradeMasterDao tradeMasterDao = new TradeMasterDao(dataSource);

        try {
            TradeInfo tradeInfo = tradeMasterDao.getTrade("FXI991269053");
            System.out.println(tradeInfo);
        } finally {
            super.close(dataSource);
        }
    }

    //@Test
    public void testGetParentTrade() {
        BasicDataSource dataSource = getDataSource();
        TradeMasterDao tradeMasterDao = new TradeMasterDao(dataSource);

        try {
            TradeInfo tradeInfo = tradeMasterDao.getParentTrade("FXI991269053");
            System.out.println(tradeInfo);
        } finally {
            super.close(dataSource);
        }
    }

    //@Test
    public void testGetCoveringTrades() {
        BasicDataSource dataSource = getDataSource();
        TradeMasterDao tradeMasterDao = new TradeMasterDao(dataSource);

        try {
            List<TradeInfo> tradeInfos = tradeMasterDao.getCoveringTrades("FXI991269053");
            for(TradeInfo info : tradeInfos) {
                System.out.println(info);
            }
        } finally {
            super.close(dataSource);
        }
    }
}

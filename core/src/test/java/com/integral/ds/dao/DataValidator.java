package com.integral.ds.dao;

import org.apache.commons.dbcp.BasicDataSource;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by kasi on 5/20/2014.
 */
public class DataValidator
{
    //select count(orderid) as numberOfOrders, org from ORDER_MASTER where created between '19-May-2014' AND '20-May-2014'
    //group by org
    static String url = "db.url=jdbc:postgresql://eastot.chgyeswrwlad.us-east-1.rds.amazonaws.com:5432/eastOT";
    private static String userName = "emscope";
    private static String password = "Integral4309";
    public static void main (String [] args)
    {
        String fromDate = args[0];
        String toDate = args[1];
        String errorFileName = args[2];
        String url= "https://report3.fxinside.net/birt/jsp/reportApi.jsp?type=ordersVolume&groupBy1=org&fromDate="
                +fromDate+"&toDate="+toDate+"&format=csv";
        Map<String,Integer> misMap = getUrlContents( url );
        final String sqlString = "select count(orderid) as numberOfOrders, org from ORDER_MASTER where created between '"+fromDate+"' AND '"
                +toDate+"'"
                + " group by org";
        DateFormat firstFormat = new SimpleDateFormat();
        DateFormat secondFormat = new SimpleDateFormat();
        TimeZone gmtTime = TimeZone.getTimeZone("GMT");
        TimeZone secondTime = TimeZone.getTimeZone( "EST" );
        firstFormat.setTimeZone(gmtTime );

        // get the data from DB.
        try
        {
            firstFormat.parse( fromDate );
            FileWriter in = new FileWriter( new File(errorFileName ));
            BufferedWriter br = new BufferedWriter( in );
            Connection conn = getDataSource().getConnection();
            PreparedStatement statementUpdate=conn.prepareStatement( sqlString );
            ResultSet result=statementUpdate.executeQuery();

            while(result.next())
            {
                String org = result.getString( 2);
                int no_of_orders = result.getInt( 1 ) ;

                // check if the value matches.

                Integer misOrders = misMap.get(org);
                if (misOrders == null)
                {
                    br.write("Org : "+org+" present in PosGreSql not in MIS");
                    br.newLine();
                }
                else
                {
                    if (misOrders != no_of_orders)
                    {
                        br.write("Order count mismatch for Org :"+org);
                        br.newLine();
                    }
                }
            }

        }
        catch ( SQLException e )
        {
            e.printStackTrace();
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }
        catch ( ParseException e )
        {
            e.printStackTrace();
        }

    }

    private static Map getUrlContents(String theUrl)
    {
        StringBuilder content = new StringBuilder();
        Map<String,Integer> misMap = new HashMap<String,Integer>();

        // many of these calls can throw exceptions, so i've just
        // wrapped them all in one try/catch statement.
        try
        {
            // create a url object
            URL url = new URL(theUrl);

            // create a urlconnection object
            URLConnection urlConnection = url.openConnection();

            // wrap the urlconnection in a bufferedreader
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            String line;
            // skip the header

            bufferedReader.readLine();
            // read from the urlconnection via the bufferedreader
            while ((line = bufferedReader.readLine()) != null)
            {
                String [] data = line.split( "," );
                if (data.length > 2)
                {
                    misMap.put( data[0], Integer.parseInt( data[1] ));
                }

            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return misMap;
    }

    private static Connection connectToDatabase()
    {
        Connection conn = null;
        try
        {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://eastot.chgyeswrwlad.us-east-1.rds.amazonaws.com:5439/eastOT";
            conn = DriverManager.getConnection( url, "emscope", "Integral4309" );
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
            System.exit(1);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            System.exit(2);
        }
        return conn;
    }
    private static BasicDataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl(url);
        dataSource.setUsername(userName);
        dataSource.setPassword(password);
        return dataSource;
    }

    public void ConvertTimeZone() {

            Date date = new Date();
            DateFormat firstFormat = new SimpleDateFormat();
            DateFormat secondFormat = new SimpleDateFormat();
            TimeZone firstTime = TimeZone.getTimeZone("GMT");
            TimeZone secondTime = TimeZone.getTimeZone( "EST" );
            firstFormat.setTimeZone(firstTime);
            secondFormat.setTimeZone(secondTime);
            System.out.println("-->"+"GMT"+": " + firstFormat.format(date));
            System.out.println("-->"+"EST"+": " + secondFormat.format(date));
        }

}

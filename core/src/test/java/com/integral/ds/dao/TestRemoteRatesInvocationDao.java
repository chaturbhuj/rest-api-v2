package com.integral.ds.dao;

import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.rmi.LoadBalancer;
import com.integral.ds.rmi.RemoteRatesInvocationDao;
import com.integral.ds.rmi.impl.SimpleRoundRobinLoadBalancer;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class TestRemoteRatesInvocationDao {

    //@Test
    public void testRemoteRatesInvocationDao() throws Exception {
        LoadBalancer balancer = new SimpleRoundRobinLoadBalancer();
        RemoteRatesInvocationDao  ratesDao = new RemoteRatesInvocationDao();

        // inject round robin balancer.
        ratesDao.setBalancer(balancer);

        String startTimestamp = "01.22.2014 02:55:00.000";
        String endTimestamp =   "01.22.2014 03:56:55.067";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        String provider = "BOAN";
        String stream = "BandC";
        String ccypair = "AUDCAD";

        long start = System.nanoTime();
        List<RestRatesObject> ratesObjects = ratesDao.getRates(provider,stream,ccypair,startTime,endTime);
        long start1 = System.nanoTime();
        System.out.println("Time " + (start1-start)/1000000000.0);

        List<RestRatesObject> ratesObjects1 = ratesDao.getRates(provider,stream,ccypair,startTime,endTime);
        long start2 = System.nanoTime();
        System.out.println("Time " + (start2-start1)/1000000000.0);

        List<RestRatesObject> ratesObjects2 = ratesDao.getRates(provider,stream,ccypair,startTime,endTime);
        long start3 = System.nanoTime();
        System.out.println("Time " + (start3-start2)/1000000000.0);

        List<RestRatesObject> ratesObjects3 = ratesDao.getRates(provider,stream,ccypair,startTime,endTime);
        long start4 = System.nanoTime();
        System.out.println("Time " + (start4-start3)/1000000000.0);

        List<RestRatesObject> ratesObjects4 = ratesDao.getRates(provider,stream,ccypair,startTime,endTime);
        long start5 = System.nanoTime();
        System.out.println("Time " + (start5-start4)/1000000000.0);

        //System.out.println("Size of the return list => " + ratesObjects.size());

        /*
        for(RestRatesObject ratesObject : ratesObjects) {
            long time = ratesObject.getTmstmp();
            System.out.println("Rate object time : " + time);
        }
        */

        long end = System.nanoTime();
        System.out.println("Time taken " + ((end - start)/1000000000.0) + " sec.");
    }
}

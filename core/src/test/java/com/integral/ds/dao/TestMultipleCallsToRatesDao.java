package com.integral.ds.dao;

import com.integral.ds.s3.S3StreamReader;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Tests multiple calls to RatesDoa.
 *
 * @author Rahul Bhattacharjee
 */
public class TestMultipleCallsToRatesDao {


    //@Test
    public void testMultipleCallsToRatesDao() throws Exception {

        String []  files = {"processed/2014/2014-01/2014-01-22/BOAN/BandC/GBPUSD/processedLog_2014-01-22-03.zip","processed/2014/2014-01/2014-01-22/BOAN/BandC/AUDCAD/processedLog_2014-01-22-03.zip",
                "processed/2014/2014-01/2014-01-22/BOAN/BandC/AUDCHF/processedLog_2014-01-22-03.zip","processed/2014/2014-01/2014-01-22/BOAN/BandC/EURHUF/processedLog_2014-01-22-03.zip",
                "processed/2014/2014-01/2014-01-22/BOAN/BandC/EURGBP/processedLog_2014-01-22-03.zip","processed/2014/2014-01/2014-01-22/BOAN/BandC/EURZAR/processedLog_2014-01-22-03.zip",
                "processed/2014/2014-01/2014-01-22/BOAN/BandC/EURSEK/processedLog_2014-01-22-03.zip","processed/2014/2014-01/2014-01-22/BOAN/BandC/EURDKK/processedLog_2014-01-22-03.zip",
                "processed/2014/2014-01/2014-01-22/BOAN/BandC/EURJPY/processedLog_2014-01-22-03.zip","processed/2014/2014-01/2014-01-22/BOAN/BandC/GBPPLN/processedLog_2014-01-22-03.zip"};

        long start = System.currentTimeMillis();

        ExecutorService executorService = Executors.newFixedThreadPool(10);
        List<Future> futures = new ArrayList<>();
        Future future = null;

        for(String fileName : files) {
            future = executorService.submit(new ZipFileFetcher(fileName));
            futures.add(future);
        }

        for(Future currentFuture : futures) {
            currentFuture.get();
        }

        System.out.println("Time diff " + (System.currentTimeMillis() - start));
        executorService.shutdown();
    }


    private static class ZipFileFetcher implements Runnable {

        private ZipFileFetcher(String fileName) {
            this.fileName = fileName;
        }

        private String fileName;
        private int minute = 33;

        @Override
        public void run() {
            S3StreamReader streamReader = new S3StreamReader();

            InputStream inputStream = null;
            ZipInputStream zis = null;
            BufferedReader buff = null;
            InputStreamReader reader = null;

            try {
                inputStream = streamReader.getInputStream(fileName);

                zis = new ZipInputStream(new BufferedInputStream(inputStream));

                ZipEntry entry;

                String filter = "_" + minute + ".csv";

                while((entry = zis.getNextEntry()) != null) {
                    String entryName = entry.getName();

                    if(entryName.contains(filter)) {
                        reader = new InputStreamReader(zis);
                        buff = new BufferedReader(reader);
                        String line = "";

                        int count = 0;

                        while(line !=null) {
                            if(count == 1){
                                break;
                            }
                            line = buff.readLine();
                            System.out.println("Line => " + line);
                            count++;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if(inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if(zis != null) {
                    try {
                        zis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if(buff != null) {
                    try {
                        buff.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}

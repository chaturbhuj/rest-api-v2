package com.integral.ds.util;

import com.integral.ds.dto.OrderNode;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Tests util methods of graph utility.
 *
 * @author Rahul Bhattacharjee
 */
public class TestGraphUtil {

    //@Test
    public void testGetOrderObjectGraph() {
        OrderNode node = GraphUtil.getOrderObjectGraphFromMap(getMap());
        System.out.print("Parent node " + node);
    }

    private Map<Integer,OrderNode> getMap() {
        OrderNode node1 = new OrderNode();
        node1.setId(1);
        node1.setOrderId("parent_order_id");

        List<Integer> links1 = new ArrayList<Integer>();
        links1.add(2);
        links1.add(3);
        node1.setLinkIds(links1);

        OrderNode node2 = new OrderNode();
        node2.setId(2);
        node2.setOrderId("child_order_1");


        OrderNode node3 = new OrderNode();
        node3.setId(3);
        node3.setOrderId("child_order_2");

        Map<Integer,OrderNode> configMap = new HashMap<Integer,OrderNode>();
        configMap.put(1,node1);
        configMap.put(2,node2);
        configMap.put(3,node3);

        return configMap;
    }
}

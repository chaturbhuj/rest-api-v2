package com.integral.ds.util;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * Tests Date query utils.
 *
 * @author Rahul Bhattacharjee
 */
public class TestDataQueryUtils {

    //@Test
    public void testDataWithLastAvailability() throws Exception {
        String startTimestamp = "03.06.2014 13:40:00.000";
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        String provider = "VTFX";
        String stream  = "VFX0";
        String currencyPair = "GBPUSD";
        Date statDate = dateFormat.parse(startTimestamp);

        String date = DataQueryUtils.getNearestAvailableRates(provider,stream,currencyPair,statDate);

        System.out.println("Old date " + statDate);
        System.out.println("New date " + date);
    }

    //@Test
    public void testDataWithLastAvailability1() throws Exception {
        String startTimestamp = "03.20.2014 22:40:00.000";
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        String provider = "BOAP";
        String stream  = "SQNS";
        String currencyPair = "GBPUSD";
        Date statDate = dateFormat.parse(startTimestamp);

        String date = DataQueryUtils.getNearestAvailableRates(provider,stream,currencyPair,statDate);

        System.out.println("Old date " + statDate);
        System.out.println("New date " + date);
    }

    //@Test
    public void testGetMinutesAvailableInHour() throws Exception {

        String startTimestamp = "03.06.2014 13:40:00.000";
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        String provider = "VTFX";
        String stream  = "VFX0";
        String currencyPair = "GBPUSD";
        Date statDate = dateFormat.parse(startTimestamp);

        int hourBefore=0 ;
        Set<Integer> minutesAvailable = DataQueryUtils.getMinutesAvailableInHour(provider,stream,currencyPair,statDate,hourBefore);

        System.out.println(minutesAvailable.size());
        System.out.println("Content " + minutesAvailable);

        int val = DataQueryUtils.getLastAvailableMinute(statDate, minutesAvailable);
        System.out.println("Last " + val);
        String newDateString = DataQueryUtils.getDate(statDate,hourBefore,val);

        System.out.println("input date String " + startTimestamp);
        System.out.println("nearest available date String " + newDateString);
    }

    //@Test
    public void testFileSplit() {
        String fileName = "processedLog_2014-01-22-18_1.csv";
        int lastIndexOfDot = fileName.lastIndexOf(".");
        int lastIndexOfUnderbar = fileName.lastIndexOf("_");

        String choppedValue = fileName.substring(lastIndexOfUnderbar+1,lastIndexOfDot);
        System.out.println(choppedValue);
    }
}

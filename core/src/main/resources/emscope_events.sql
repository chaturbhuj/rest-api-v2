CREATE TABLE emscope_events
(
    event_date          DATE,
    event_name          VARCHAR(50),
    user                VARCHAR(50),
    event_type          VARCHAR(50)
)

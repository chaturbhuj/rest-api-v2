package com.integral.ds.boot;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 * User: vchawla
 * Date: 9/15/11
 * Time: 7:36 PM
 * Documentation:
 */

@Configuration
public class AppConfig {

    private
    @Value("${local.host}")
    String localHost;
    private
    @Value("${local.port}")
    String localPort;
    private
    @Value("${local.protocol}")
    String localProtocol;


    private
    @Value("${app.context.name}")
    String appContext;


    private
    @Value("${streams}")
    String streams;

    public
    @Bean
    String applicationURL() {
        return buildApplicationURL(appContext);
    }

    private String buildApplicationURL(String applicationContext) {
//        final int region = resolveEnvironment();
        StringBuilder builder = new StringBuilder();
        builder.append(localProtocol);
        builder.append("://");
        builder.append(localHost);
        builder.append(":").append(localPort);
        builder.append("/");
        builder.append(applicationContext);
        return builder.toString();
    }

    public
    @Bean
    List<String> streams() {
        return Arrays.asList(streams, ",");
    }


}
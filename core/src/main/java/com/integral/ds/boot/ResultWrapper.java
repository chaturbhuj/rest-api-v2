package com.integral.ds.boot;

/**
 * Jsp aren't good with collections.So this wrapper is for rescue.
 * @author Rahul Bhattacharjee
 * @param <T>
 */
public class ResultWrapper<T> {
    private T object;

    public T getBean(){
        return object;
    }

    public void setBean(T bean) {
        this.object = bean;
    }
}

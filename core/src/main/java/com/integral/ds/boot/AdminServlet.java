package com.integral.ds.boot;

import com.integral.ds.event.EventAggregationBean;
import com.integral.ds.event.EventPersistence;
import com.integral.ds.util.PropertyReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class AdminServlet extends HttpServlet {

    private static final String ADMIN_VIEW = "/web1/admin.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(PropertyReader.getBooleanPropertyValue(PropertyReader.KEY_SERVICE_AUDIT_ENABLED)){
            EventPersistence context = (EventPersistence) getServletContext().getAttribute("eventPersistence");
            List<EventAggregationBean> reportBeans = context.getCurrentAggregationReport();

            ResultWrapper<List<EventAggregationBean>> result = new ResultWrapper<List<EventAggregationBean>>();
            result.setBean(reportBeans);
            request.setAttribute("RESULT",result);
            getServletContext().getRequestDispatcher(ADMIN_VIEW).forward(request,response);
        } else {
            response.getWriter().write("ERROR :Admin portal not enabled in this environment. Contact administrator.");
        }
    }
}

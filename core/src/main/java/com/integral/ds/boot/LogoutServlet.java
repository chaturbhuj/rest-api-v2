package com.integral.ds.boot;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.owlike.genson.Genson;
import org.apache.log4j.Logger;

public class LogoutServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(LogoutServlet.class);
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest aRequest, HttpServletResponse aResponse)
			throws ServletException, IOException {
		
		HttpSession session = aRequest.getSession(false);
		String message = "";
		boolean isError = false;
		if (session == null)	{
			isError = true;
			message = "Cannot Logout as No User Logged In.";
		}
		else	{
			isError = false;
			session.invalidate();
			message = "User Logged Out Successfully.";
		}
		try	{
			Genson genson = new Genson ();
			aResponse.setContentType("application/json");
			PrintWriter out = aResponse.getWriter();
			Map <String, Object> returnMessage = new HashMap<>();
			returnMessage.put("IsError", isError);
			returnMessage.put("Message", message);
			out.write(genson.serialize(returnMessage));
			out.flush();
		}
		catch (Exception exception){
            LOGGER.error("Exception while logging out.",exception);
        }
	}
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}
}

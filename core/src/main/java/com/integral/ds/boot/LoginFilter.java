package com.integral.ds.boot;

import org.apache.log4j.Logger;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class LoginFilter implements javax.servlet.Filter {

    private static final Logger LOGGER = Logger.getLogger(LoginFilter.class);

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		String homePageUrl = request.getContextPath() + "/web1/home1.jsp" ;
		
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("UserName") == null) {
            LOGGER.info("Redirecting to login page.");
			arg2.doFilter(arg0, arg1);
		} else	{
			response.sendRedirect(homePageUrl);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

    @Override
    public void destroy() {
    }
}

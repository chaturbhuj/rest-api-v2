package com.integral.ds.boot;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.integral.ds.emscope.auth.AuthAppDataContext;
import com.integral.ds.event.Event;
import com.integral.ds.event.EventManager;
import com.integral.ds.event.EventType;
import com.owlike.genson.Genson;
import com.owlike.genson.TransformationException;
import org.apache.log4j.Logger;

public class LoginServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(LoginServlet.class);

    private static final long serialVersionUID = 4438710188375237803L;

    public void doGet(HttpServletRequest aRequest, HttpServletResponse aResponse)
			throws IOException, ServletException {
		doPost(aRequest, aResponse);
		
	}

	public void doPost(HttpServletRequest aRequest,
			HttpServletResponse aResponse) throws IOException, ServletException {
		
		HttpSession session = aRequest.getSession(false);
		if (session == null || session.getAttribute("UserName") == null)	{
			AuthAppDataContext context = (AuthAppDataContext) getServletContext().getAttribute("authAppDataContext");
			String userName = aRequest.getParameter("username");
			String password = aRequest.getParameter("password");
			boolean isLoginValid = context.isLoginValid(userName, password);
			if (isLoginValid){
				session = aRequest.getSession();
				session.setAttribute("UserName", userName);
				generateResponse(false, "User Login Successful", aResponse);
                submitSuccessfulLoginEvent(userName);
			}
			else	{
				generateResponse(true, "User ID or Password is Incorrect. Please Try Again", aResponse);
                submitUnsuccessfulLoginEvent(userName);
			}
		}
		else	{
			generateResponse(false, "User Already Logged In", aResponse);
		}
	}

    private void submitUnsuccessfulLoginEvent(String userName) {
        EventManager eventManager = (EventManager) getServletContext().getAttribute("eventManager");
        Event event = new Event();
        event.setEventDate(new Date());
        event.setEventName(EventType.EventName.UNSUCCESSFUL_LOGIN);
        event.setType(EventType.AUTHENTICATION);
        event.setUserName(userName);
        eventManager.submitEvent(event);
    }

    private void submitSuccessfulLoginEvent(String userName) {
        EventManager eventManager = (EventManager) getServletContext().getAttribute("eventManager");
        Event event = new Event();
        event.setEventDate(new Date());
        event.setEventName(EventType.EventName.SUCCESSFUL_LOGIN);
        event.setType(EventType.AUTHENTICATION);
        event.setUserName(userName);
        eventManager.submitEvent(event);
    }

    private void generateResponse (boolean isError, String errorMessage, HttpServletResponse aResponse)	{
		try	{
			aResponse.setContentType("application/json");
			Genson genson = new Genson();
			PrintWriter out = aResponse.getWriter();
			Map <String, Object> returnMessage = new HashMap<>();
			returnMessage.put("isError", isError);
			returnMessage.put("message", errorMessage);
			out.write(genson.serialize(returnMessage));
			out.flush();
		}
		catch (TransformationException | IOException exception ){
            LOGGER.error("Exception while generating response.",exception);
		}
	}
}

package com.integral.ds.boot;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RequestFilter implements Filter {

	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain arg2) throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) req;
		HttpSession session = request.getSession(false);
		HttpServletResponse response = (HttpServletResponse) resp;
		String loginUrl = request.getContextPath() + "/web1/login.jsp" ;

        String pathRequested = getFullURL(request);

        if(session == null || session.getAttribute("UserName") == null) {
            loginUrl = loginUrl + "?path=" + URLEncoder.encode(pathRequested);
			response.sendRedirect(loginUrl);
		} else	{
			arg2.doFilter(req, resp);
		}
	}

    public static String getFullURL(HttpServletRequest request) {
        StringBuffer requestURL = request.getRequestURL();
        String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}
}

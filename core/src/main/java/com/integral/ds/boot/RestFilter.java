package com.integral.ds.boot;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.integral.ds.emscope.auth.AuthAppDataContext;
import com.integral.ds.event.Event;
import com.integral.ds.event.EventManager;
import com.integral.ds.event.EventType;
import com.integral.ds.event.PathToEventConverter;
import org.apache.log4j.Logger;

import com.owlike.genson.Genson;
import com.owlike.genson.TransformationException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;

public class RestFilter implements Filter {

	private static final Logger LOGGER = Logger.getLogger(RestFilter.class);

    private EventManager eventManager;
    private PathToEventConverter eventConverter;

    @Override
    public void init(FilterConfig config) throws ServletException {
        eventManager = (EventManager) config.getServletContext().getAttribute("eventManager");
        eventConverter = (PathToEventConverter) config.getServletContext().getAttribute("eventConverter");
    }

    @Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain arg2) throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) req;
		HttpSession session = request.getSession(false);

        if(isAuthRequired(request.getPathInfo())) {
            if(session == null || session.getAttribute("UserName") == null) {
                generateResponse(true, "You are not logged in", resp);
            } else	{
                try	{
                    Object userName = session.getAttribute("UserName");
                    if (userName != null){
                        String path = request.getPathInfo();
                        LOGGER.info("User: " + userName.toString() + " has Requested " + path);
                        submitServiceEvent((String) userName,path);
                    }
                }
                catch (Exception exception)	{
                    LOGGER.error("Exception while doing rest filteration.",exception);
                }
            }
        }
        arg2.doFilter(req, resp);
	}

    private void submitServiceEvent(String userName, String path) {
        Event event = new Event();
        event.setEventDate(new Date());
        event.setType(EventType.SERVICE);
        event.setUserName(userName);
        EventType.EventName eventName = eventConverter.getEventNameFromPath(path);

        switch (eventName) {
            case LANDSCAPE_SERVICE:
                event.setEventName(eventName);
                eventManager.submitEvent(event);
                break;

            case ORDER_SERVICE:
                event.setEventName(eventName);
                eventManager.submitEvent(event);
                break;
            case NONE:
                break;
            default:
        }
    }

    private boolean isAuthRequired(String url) {
        if(url != null && url.contains("noauth")) {
            return false;
        }
        return true;
    }
	
	private void generateResponse (boolean isError, String errorMessage, ServletResponse aResponse)	{
		try	{
			aResponse.setContentType("application/json");
			Genson genson = new Genson();
			PrintWriter out = aResponse.getWriter();
			Map <String, Object> returnMessage = new HashMap<>();
			returnMessage.put("isError", isError);
			returnMessage.put("message", errorMessage);
			out.write(genson.serialize(returnMessage));
			out.flush();
		}
		catch (TransformationException | IOException exception ){
            LOGGER.error("Exception while generating error message.",exception);
		}
	}

    @Override
    public void destroy() {}
}

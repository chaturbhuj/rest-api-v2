package com.integral.ds.dto;

/**
 * @author Rahul Bhattacharjee
 */
public class OrderInfo {

    private String orderId;
    private String coveredOrderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCoveredOrderId() {
        return coveredOrderId;
    }

    public void setCoveredOrderId(String coveredOrderId) {
        this.coveredOrderId = coveredOrderId;
    }

    @Override
    public String toString() {
        return "OrderInfo{" +
                "orderId='" + orderId + '\'' +
                ", coveredOrderId='" + coveredOrderId + '\'' +
                '}';
    }
}

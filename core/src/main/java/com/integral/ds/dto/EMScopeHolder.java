package com.integral.ds.dto;

import java.util.List;

public class EMScopeHolder {

	private List<Order> orderList;
	private List<Trade> tradeList;
	
	public List<Order> getOrderList() {
		return orderList;
	}
	public void setOrderList(List<Order> orderList) {
		this.orderList = orderList;
	}
	public List<Trade> getTradeList() {
		return tradeList;
	}
	public void setTradeList(List<Trade> tradeList) {
		this.tradeList = tradeList;
	}

    @Override
    public String toString() {
        return "EMScopeHolder{" +
                "orderListSize=" + orderList.size() +
                ", tradeListSize=" + tradeList.size() +
                '}';
    }
}

package com.integral.ds.dto;

/**
 * Trade info bean for representing a row of trades master table.
 *
 * @author Rahul Bhattacharjee
 */
public class TradeInfo {

    private String tradeId;
    private String orderId;
    private String makerOrg;
    private String coveredtradeid;

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMakerOrg() {
        return makerOrg;
    }

    public void setMakerOrg(String makerOrg) {
        this.makerOrg = makerOrg;
    }

    public String getCoveredtradeid() {
        return coveredtradeid;
    }

    public void setCoveredtradeid(String coveredtradeid) {
        this.coveredtradeid = coveredtradeid;
    }

    @Override
    public String toString() {
        return "TradeInfo{" +
                "tradeId='" + tradeId + '\'' +
                ", orderId='" + orderId + '\'' +
                ", makerOrg='" + makerOrg + '\'' +
                ", coveredtradeid='" + coveredtradeid + '\'' +
                '}';
    }
}

package com.integral.ds.dto;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class BenchmarkObject implements Comparable<BenchmarkObject> {
	private static final String SEPERATOR = ",";
	private String ccyPair;
	private BigDecimal timeStamp;
	private Integer tier;
	private BigDecimal meanMidPrice;
	private BigDecimal medianMidPrice;
	private BigDecimal midPriceRange;
	private Integer lpCount;
	private BigDecimal midPriceMedianLP;
	

	private static SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MMM-yy");
	private static SimpleDateFormat timeFormater = new SimpleDateFormat("HH:mm:ss");
	
	static	{
		dateFormater.setTimeZone(TimeZone.getTimeZone("GMT"));
		timeFormater.setTimeZone(TimeZone.getTimeZone("GMT"));
	}

	

	public String getCcyPair() {
		return ccyPair;
	}
	public void setCcyPair(String ccyPair) {
		this.ccyPair = ccyPair;
	}
	public BigDecimal getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(BigDecimal timeStamp) {
		this.timeStamp = timeStamp;
	}
	public Integer getTier() {
		return tier;
	}
	public void setTier(Integer tier) {
		this.tier = tier;
	}
	public BigDecimal getMeanMidPrice() {
		return meanMidPrice;
	}
	public void setMeanMidPrice(BigDecimal meanMidPrice) {
		this.meanMidPrice = meanMidPrice;
	}
	public BigDecimal getMedianMidPrice() {
		return medianMidPrice;
	}
	public void setMedianMidPrice(BigDecimal medianMidPrice) {
		this.medianMidPrice = medianMidPrice;
	}
	public BigDecimal getMidPriceRange() {
		return midPriceRange;
	}
	public void setMidPriceRange(BigDecimal midPriceRange) {
		this.midPriceRange = midPriceRange;
	}
	public Integer getLpCount() {
		return lpCount;
	}
	public void setLpCount(Integer lpCount) {
		this.lpCount = lpCount;
	}
	public BigDecimal getMidPriceMedianLP() {
		return midPriceMedianLP;
	}
	public void setMidPriceMedianLP(BigDecimal midPriceMedianLP) {
		this.midPriceMedianLP = midPriceMedianLP;
	}

	public static final String getHeader()	{
		return 
				"Currency Pair" + SEPERATOR 
				+ "DATE" + SEPERATOR 
				+ "TIME" + SEPERATOR 
				+ "Mid Mean Price" + SEPERATOR 
				+ "RANGE" + SEPERATOR
				+ "MID Median Price"; 
	}

	@Override
	public String toString() {
		return 
			ccyPair + SEPERATOR 
			+ dateFormater.format(timeStamp) + SEPERATOR
			+ timeFormater.format(timeStamp) + SEPERATOR
			+ meanMidPrice + SEPERATOR
			+ midPriceRange + SEPERATOR
			+ medianMidPrice + SEPERATOR ;
	}	
	
	@Override
	public int compareTo(BenchmarkObject o) {
		return this.timeStamp.compareTo(o.getTimeStamp()); 
	}
	
	
	public static void main(String[] args) {
		long time = 1384765200000l;
		Date d = new Date(time);
		System.out.println(d);
	}
}

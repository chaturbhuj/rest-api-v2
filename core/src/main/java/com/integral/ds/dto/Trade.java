package com.integral.ds.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.xml.bind.annotation.XmlRootElement;

import com.integral.ds.model.IntegralObject;
import com.integral.ds.representation.Representation;
import com.integral.ds.util.PropertyReader;

/**
 * Created with IntelliJ IDEA. User: vchawla Date: 10/10/13 Time: 4:39 PM To
 * change this template use File | Settings | File Templates.
 */
@XmlRootElement
public class Trade extends IntegralObject implements Comparable<Trade>{

    private String ORG;
    private Timestamp EXECTIMESTAMP;
    private String CCYPAIR;
    private String MKRTKR;
    private String TYPE;
    private String BUYSELL;
    private String DEALER;
    private String ORDERID;
    private String TRADEID;
    private String CPTYACCOUNT;
    private String CPTY;
    private Long FIXINGDATE;
    private String ACCOUNT;
    private String DEALT;
    private String PBACCOUNT;
    private Long TRADEDATE;
    private BigDecimal SPOTR;
    private BigDecimal BASEAMT;
    private BigDecimal RATE;
    private String SDACCOUNT;
    private String MAKERREFID;
    private String WORKFLOW;
    private BigDecimal FWDPOINTS;
    private BigDecimal USDRATE;
    private String PRIMEBROKER;
    private String TRDCOMMENT;
    private String COVEREDTRADEID;
    private String TRADESTATUS;
    private Long VALUEDATE;
    private String TENOR;
    private BigDecimal USDAMT;
    private String STREAM;
    private String SDUSER;
    private BigDecimal TERMAMT;
    private String VIRTUALSERVER;
    private String TAKERREFID;
    private String tickStream;
    private String tickCpty;
    private String STATUS;
    private String rateLog;
    private String maskLP;
    private String rateEvent;

    public Timestamp getEXECTIMESTAMP() {
		return EXECTIMESTAMP;
	}

	public void setEXECTIMESTAMP(Timestamp eXECTIMESTAMP) {
		EXECTIMESTAMP = eXECTIMESTAMP;
	}

	@Representation(names = {"COMPLETE", "SUMMARY"})
    public String getTickStream() {
		return tickStream;
	}

    @Representation(names = {"COMPLETE", "SUMMARY"})
	public String getTickCpty() {
		return tickCpty;
	}

	
	public void setTickStream(String tickStream) {
		this.tickStream = tickStream;
	}

	public void setTickCpty(String tickCpty) {
		this.tickCpty = tickCpty;
	}

	
	@Representation(names = {"COMPLETE", "SUMMARY"})
    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public long getEXECTIME() {
        return EXECTIMESTAMP.getTime();
    }
    


    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getCCYPAIR() {
        return CCYPAIR;
    }

    public void setCCYPAIR(String CCYPAIR) {
        this.CCYPAIR = CCYPAIR;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getMKRTKR() {
        return MKRTKR;
    }

    public void setMKRTKR(String MKRTKR) {
        this.MKRTKR = MKRTKR;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getBUYSELL() {
        return BUYSELL;
    }

    public void setBUYSELL(String BUYSELL) {
        this.BUYSELL = BUYSELL;
    }

    @Representation
    public String getDEALER() {
        return DEALER;
    }

    public void setDEALER(String DEALER) {
        this.DEALER = DEALER;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getORDERID() {
        return ORDERID;
    }

    public void setORDERID(String ORDERID) {
        this.ORDERID = ORDERID;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getTRADEID() {
        return TRADEID;
    }

    public void setTRADEID(String TRADEID) {
        this.TRADEID = TRADEID;
    }

    @Representation
    public String getCPTYACCOUNT() {
        return CPTYACCOUNT;
    }

    public void setCPTYACCOUNT(String CPTYACCOUNT) {
        this.CPTYACCOUNT = CPTYACCOUNT;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getCPTY() {
        return CPTY;
    }

    public void setCPTY(String CPTY) {
        this.CPTY = CPTY;
    }

    @Representation
    public Long getFIXINGDATE() {
        return FIXINGDATE;
    }

    public void setFIXINGDATE(Long FIXINGDATE) {
        this.FIXINGDATE = FIXINGDATE;
    }

    @Representation
    public String getACCOUNT() {
        return ACCOUNT;
    }

    public void setACCOUNT(String ACCOUNT) {
        this.ACCOUNT = ACCOUNT;
    }

    @Representation
    public String getDEALT() {
        return DEALT;
    }

    public void setDEALT(String DEALT) {
        this.DEALT = DEALT;
    }

    @Representation
    public String getPBACCOUNT() {
        return PBACCOUNT;
    }

    public void setPBACCOUNT(String PBACCOUNT) {
        this.PBACCOUNT = PBACCOUNT;
    }

    public Long getTRADEDATE() {
        return TRADEDATE;
    }

    public void setTRADEDATE(Long TRADEDATE) {
        this.TRADEDATE = TRADEDATE;
    }

    @Representation
    public BigDecimal getSPOTR() {
        return SPOTR;
    }

    public void setSPOTR(BigDecimal SPOTR) {
        this.SPOTR = SPOTR;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getBASEAMT() {
        return BASEAMT;
    }

    public void setBASEAMT(BigDecimal BASEAMT) {
        this.BASEAMT = BASEAMT;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getRATE() {
        return RATE;
    }

    public void setRATE(BigDecimal RATE) {
        this.RATE = RATE;
    }

    @Representation
    public String getSDACCOUNT() {
        return SDACCOUNT;
    }

    public void setSDACCOUNT(String SDACCOUNT) {
        this.SDACCOUNT = SDACCOUNT;
    }

    @Representation
    public String getMAKERREFID() {
        return MAKERREFID;
    }

    public void setMAKERREFID(String MAKERREFID) {
        this.MAKERREFID = MAKERREFID;
    }

    @Representation
    public String getWORKFLOW() {
        return WORKFLOW;
    }

    public void setWORKFLOW(String WORKFLOW) {
        this.WORKFLOW = WORKFLOW;
    }

    @Representation
    public BigDecimal getFWDPOINTS() {
        return FWDPOINTS;
    }

    public void setFWDPOINTS(BigDecimal FWDPOINTS) {
        this.FWDPOINTS = FWDPOINTS;
    }

    @Representation
    public BigDecimal getUSDRATE() {
        return USDRATE;
    }

    public void setUSDRATE(BigDecimal USDRATE) {
        this.USDRATE = USDRATE;
    }

    @Representation
    public String getPRIMEBROKER() {
        return PRIMEBROKER;
    }

    public void setPRIMEBROKER(String PRIMEBROKER) {
        this.PRIMEBROKER = PRIMEBROKER;
    }

    @Representation
    public String getTRDCOMMENT() {
        return TRDCOMMENT;
    }

    public void setTRDCOMMENT(String TRDCOMMENT) {
        this.TRDCOMMENT = TRDCOMMENT;
    }

    @Representation
    public String getCOVEREDTRADEID() {
        return COVEREDTRADEID;
    }

    public void setCOVEREDTRADEID(String COVEREDTRADEID) {
        this.COVEREDTRADEID = COVEREDTRADEID;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getTRADESTATUS() {
        return TRADESTATUS;
    }

    public void setTRADESTATUS(String TRADESTATUS) {
        this.TRADESTATUS = TRADESTATUS;
    }

    @Representation
    public Long getVALUEDATE() {
        return VALUEDATE;
    }

    public void setVALUEDATE(Long VALUEDATE) {
        this.VALUEDATE = VALUEDATE;
    }

    @Representation
    public String getTENOR() {
        return TENOR;
    }

    public void setTENOR(String TENOR) {
        this.TENOR = TENOR;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getUSDAMT() {
        return USDAMT;
    }

    public void setUSDAMT(BigDecimal USDAMT) {
        this.USDAMT = USDAMT;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getSTREAM() {
        return STREAM;
    }

    public void setSTREAM(String STREAM) {
        this.STREAM = STREAM;
    }

    @Representation
    public String getSDUSER() {
        return SDUSER;
    }

    public void setSDUSER(String SDUSER) {
        this.SDUSER = SDUSER;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getTERMAMT() {
        return TERMAMT;
    }

    public void setTERMAMT(BigDecimal TERMAMT) {
        this.TERMAMT = TERMAMT;
    }

    @Representation
    public String getVIRTUALSERVER() {
        return VIRTUALSERVER;
    }

    public void setVIRTUALSERVER(String VIRTUALSERVER) {
        this.VIRTUALSERVER = VIRTUALSERVER;
    }

    @Representation
    public String getTAKERREFID() {
        return TAKERREFID;
    }

    public void setTAKERREFID(String TAKERREFID) {
        this.TAKERREFID = TAKERREFID;
    }

    @Override
    public String getObjectIdentifier() {
        return getTRADEID();
    }

    @Override
    //@Representation(neverExclude = true)
    public String getThisUri() {
        return "trades/" + getObjectIdentifier();
    }

	@Override
	public int compareTo(Trade o) {
		return this.EXECTIMESTAMP.compareTo(o.EXECTIMESTAMP);
	}
	
	@Representation(names = {"COMPLETE", "SUMMARY"})
	public String getSTATUS() {
		return STATUS;
	}
	
	public void setSTATUS(String status) {
		 this.STATUS = status;
	}

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getMaskLP()
    {
        if ( PropertyReader.getBooleanPropertyValue(PropertyReader.KEY_IS_DEMO_MODE))
        {
            return maskLP;
        }
        else
        {
            return STREAM;
        }
    }

    public void setMaskLP( String maskLP )
    {
        this.maskLP = maskLP;
    }
}
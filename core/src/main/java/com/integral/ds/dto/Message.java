package com.integral.ds.dto;

/**
 * @author Rahul Bhattacharjee
 */
public class Message    {

    private String message;
    private String timestampString;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimestampString() {
        return timestampString;
    }

    public void setTimestampString(String timestampString) {
        this.timestampString = timestampString;
    }

    @Override
    public String toString() {
        return "Message{" +
                "message='" + message + '\'' +
                ", timestampString='" + timestampString + '\'' +
                '}';
    }
}

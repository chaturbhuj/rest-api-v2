package com.integral.ds.dto;

import com.integral.ds.serializer.DoNotSerialize;

import java.util.ArrayList;
import java.util.List;

/**
 * OrderNode representing a node in the tree.
 *
 * @author Rahul Bhattacharjee
 */
public class OrderNode {

    private String orderId;
    private List<OrderNode> childOrderList = new ArrayList<OrderNode>();
    private boolean youAreHere;

    @DoNotSerialize
    private int id;

    @DoNotSerialize
    private List<Integer> linkIds = new ArrayList<Integer>();

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<OrderNode> getChildOrderList() {
        return childOrderList;
    }

    public void addChildOrderList(OrderNode childOrder) {
        this.childOrderList.add(childOrder);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Integer> getLinkIds() {
        return linkIds;
    }

    public void setLinkIds(List<Integer> linkIds) {
        this.linkIds = linkIds;
    }

    public boolean isYouAreHere() {
        return youAreHere;
    }

    public void setYouAreHere(boolean youAreHere) {
        this.youAreHere = youAreHere;
    }

    public void setChildOrderList(List<OrderNode> childOrderList) {
        this.childOrderList = childOrderList;
    }

    @Override
    public String toString() {
        return "OrderNode{" +
                "youAreHere=" + youAreHere +
                ", childOrderList=" + childOrderList +
                ", orderId='" + orderId + '\'' +
                '}';
    }
}

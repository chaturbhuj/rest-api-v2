package com.integral.ds.dto;

/**
 * Quote type - MQ,MT
 * @author Rahul Bhattacharjee
 */
public enum QuoteType {
    MQ,MT,NA
}

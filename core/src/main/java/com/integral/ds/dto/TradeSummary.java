package com.integral.ds.dto;

import java.sql.Timestamp;

import com.integral.ds.representation.Representation;
import com.sun.grizzly.util.buf.TimeStamp;

public class TradeSummary {

	private String TRADEID;
	
	private Timestamp EXECTIMESTAMP;

	private String RATE;
	private String BASEAMT;
	private String STATUS;
	private String ORDERID;
	
	@Representation (names = {"COMPLETE", "SUMMARY"})
	public String getORDERID() {
		return ORDERID;
	}

	public void setORDERID(String oRDERID) {
		ORDERID = oRDERID;
	}

	@Representation (names = {"COMPLETE", "SUMMARY"})
	public String getTRADEID() {
		return TRADEID;
	}
	
	@Representation (names = {"COMPLETE", "SUMMARY"})
	public long getEXECTIME() {
		return EXECTIMESTAMP.getTime();
	}
	
	@Representation (names = {"COMPLETE", "SUMMARY"})
	public String getRATE() {
		return RATE;
	}
	
	@Representation (names = {"COMPLETE", "SUMMARY"})
	public String getBASEAMT() {
		return BASEAMT;
	}
	
	@Representation (names = {"COMPLETE", "SUMMARY"})
	public String getTRADESTATUS() {
		return STATUS;
	}
	public void setTRADEID(String tRADEID) {
		TRADEID = tRADEID;
	}
	
	public void setRATE(String rATE) {
		RATE = rATE;
	}
	public void setBASEAMT(String bASEAMT) {
		BASEAMT = bASEAMT;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	
	public Timestamp getEXECTIMESTAMP() {
		return EXECTIMESTAMP;
	}

	public void setEXECTIMESTAMP(Timestamp eXECTIMESTAMP) {
		EXECTIMESTAMP = eXECTIMESTAMP;
	}

}

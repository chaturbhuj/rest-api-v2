package com.integral.ds.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Profile bean.
 *
 * @author Rahul Bhattacharjee
 */
public class Profile {

    private String provider;
    private String stream;
    private Date dateTime;
    private BigDecimal midPriceMean;
    private BigDecimal maxBidPrice;
    private BigDecimal maxAskPrice;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public BigDecimal getMidPriceMean() {
        return midPriceMean;
    }

    public void setMidPriceMean(BigDecimal midPriceMean) {
        this.midPriceMean = midPriceMean;
    }

    public BigDecimal getMaxBidPrice() {
        return maxBidPrice;
    }

    public void setMaxBidPrice(BigDecimal maxBidPrice) {
        this.maxBidPrice = maxBidPrice;
    }

    public BigDecimal getMaxAskPrice() {
        return maxAskPrice;
    }

    public void setMaxAskPrice(BigDecimal maxAskPrice) {
        this.maxAskPrice = maxAskPrice;
    }
}

package com.integral.ds.dto;

public class OrdersWithTradeObject {

	private String orderID;
	private long orderCreatedTime;
	private String orderRate;
	private String orderAmt;
	private String tradeID;
	private long TradeExecTime;
	private String tradeRate;
	private String tradeAmt;
	private String tradeStatus;
	
	public String getTradeStatus() {
		return tradeStatus;
	}
	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}
	public String getOrderID() {
		return orderID;
	}
	public long getOrderCreatedTime() {
		return orderCreatedTime;
	}
	public String getOrderRate() {
		return orderRate;
	}
	public String getOrderAmt() {
		return orderAmt;
	}
	public String getTradeID() {
		return tradeID;
	}
	public long getTradeExecTime() {
		return TradeExecTime;
	}
	public String getTradeRate() {
		return tradeRate;
	}
	public String getTradeAmt() {
		return tradeAmt;
	}
	
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	public void setOrderCreatedTime(long orderCreatedTime) {
		this.orderCreatedTime = orderCreatedTime;
	}
	public void setOrderRate(String orderRate) {
		this.orderRate = orderRate;
	}
	public void setOrderAmt(String orderAmt) {
		this.orderAmt = orderAmt;
	}
	public void setTradeID(String tradeID) {
		this.tradeID = tradeID;
	}
	public void setTradeExecTime(long tradeExecTime) {
		TradeExecTime = tradeExecTime;
	}
	public void setTradeRate(String tradeRate) {
		this.tradeRate = tradeRate;
	}
	public void setTradeAmt(String tradeAmt) {
		this.tradeAmt = tradeAmt;
	}
	
	
	
}

package com.integral.ds.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Graph node.
 *
 * @author Rahul Bhattacharjee
 */
public class GraphNode {

    public enum  GraphNodeType {ORDER,LP};

    private GraphNodeType type;
    private String id;
    private List<GraphNode> childGraphNodes = new ArrayList<GraphNode>();
    private boolean youAreHere;

    public GraphNodeType getType() {
        return type;
    }

    public void setType(GraphNodeType type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<GraphNode> getChildGraphNodes() {
        return childGraphNodes;
    }

    public void addChildGraphNode(GraphNode childGraphNode) {
        this.childGraphNodes.add(childGraphNode);
    }

    public boolean isYouAreHere() {
        return youAreHere;
    }

    public void setYouAreHere(boolean youAreHere) {
        this.youAreHere = youAreHere;
    }
}

package com.integral.ds.dto;

import java.util.Date;

public class RestCallAudit {

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRestCall() {
		return restCall;
	}
	public void setRestCall(String restCall) {
		this.restCall = restCall;
	}
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	private String userName;
	private String restCall;
	private Date timeStamp;
	private String hostName;
	
	
}

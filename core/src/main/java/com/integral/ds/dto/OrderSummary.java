package com.integral.ds.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.integral.ds.representation.Representation;

public class OrderSummary {
	private String orderID;
	private Timestamp createdTimestamp;
	
	private String orderRate;
	private String orderAmt;
	private List<TradeSummary> tradeList = new ArrayList<>();
	

	@Representation (names = {"COMPLETE", "SUMMARY"})
	public String getOrderID() {
		return orderID;
	}
	
	@Representation (names = {"COMPLETE", "SUMMARY"})
	public long getCreated() {
		if (createdTimestamp ==null)
			return 0;
		return createdTimestamp.getTime();
	}
	
	@Representation (names = {"COMPLETE", "SUMMARY"})
	public String getOrderRate() {
		return orderRate;
	}
	
	@Representation (names = {"COMPLETE", "SUMMARY"})
	public String getOrderAmt() {
		return orderAmt;
	}
	public List<TradeSummary> getTradeList() {
		return tradeList;
	}
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public void setOrderRate(String orderRate) {
		this.orderRate = orderRate;
	}
	public void setOrderAmt(String orderAmt) {
		this.orderAmt = orderAmt;
	}
	public void setTradeList(List<TradeSummary> tradeList) {
		this.tradeList = tradeList;
	}
	
	public Timestamp getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	
	
}

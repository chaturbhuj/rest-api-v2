package com.integral.ds.dto;

/**
 * Bean for provider , stream pair.
 *
 * @author Rahul Bhattacharjee
 */
public class ProviderStream implements Comparable<ProviderStream> {

    private String provider;
    private String stream;
    private QuoteType quoteType;

    public ProviderStream() {}

    public ProviderStream(String provider, String stream) {
        this.provider = provider;
        this.stream = stream;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public QuoteType getQuoteType() {
        return quoteType;
    }

    public void setQuoteType(QuoteType quoteType) {
        this.quoteType = quoteType;
    }

    @Override
    public int compareTo(ProviderStream o) {
        int compVal = provider.compareTo(o.getProvider());
        if(compVal != 0) {
            return compVal;
        } else {
            return stream.compareTo(o.getStream());
        }
    }

    @Override
    public String toString() {
        return "ProviderStream{" +
                "provider='" + provider + '\'' +
                ", stream='" + stream + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProviderStream that = (ProviderStream) o;

        if (provider != null ? !provider.equals(that.provider) : that.provider != null) return false;
        if (stream != null ? !stream.equals(that.stream) : that.stream != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = provider != null ? provider.hashCode() : 0;
        result = 31 * result + (stream != null ? stream.hashCode() : 0);
        return result;
    }
}

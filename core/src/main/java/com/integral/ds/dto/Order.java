package com.integral.ds.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

import com.integral.ds.model.IntegralObject;
import com.integral.ds.representation.Representation;




/**
 * Created with IntelliJ IDEA.
 * User: vchawla
 * Date: 10/7/13
 * Time: 8:16 PM
 * To change this template use File | Settings | File Templates.)
 */

@XmlRootElement
public class Order extends IntegralObject{
    private String org;
    private String ccyPair;
    private String buySell;
    
    private String orderType;
    private String orderID;
    private String persistent;
    private String clientOrderId;
    private String tenor;
    private String account;
    private String dealer;
    private String dealt;
    private String timeInForce;
    private String orderRate;
    private Long orderAmt;
    private BigDecimal filledAmt;;
    private BigDecimal	filledPct;
    private String fillRate;
    private BigDecimal PI;
    private BigDecimal ordAmtUSD;
    private BigDecimal fillAmtUSD;
    private BigDecimal ppnl;
    
	private Timestamp createdTimestamp;
    private String narrative;
    private long lastEvent;
    private String orderStatus;
    
    private String status;
    private BigDecimal marketRange;

    private String side;
    private String visibility;
    private BigDecimal numTrades;

    private BigDecimal confirmedTrades;
    private BigDecimal rejectedTrades;
    private BigDecimal failedTrades;
    private BigDecimal totalOrderDuration;
    private BigDecimal totalTradeDuration;
    private BigDecimal numSweeps;

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getVisibility()
    {
        return visibility;
    }

    public void setVisibility( String visibility )
    {
        this.visibility = visibility;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getNumTrades()
    {
        return numTrades;
    }

    public void setNumTrades( BigDecimal numTrades )
    {
        this.numTrades = numTrades;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getConfirmedTrades()
    {
        return confirmedTrades;
    }

    public void setConfirmedTrades( BigDecimal confirmedTrades )
    {
        this.confirmedTrades = confirmedTrades;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getRejectedTrades()
    {
        return rejectedTrades;
    }

    public void setRejectedTrades( BigDecimal rejectedTrades )
    {
        this.rejectedTrades = rejectedTrades;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getFailedTrades()
    {
        return failedTrades;
    }

    public void setFailedTrades( BigDecimal failedTrades )
    {
        this.failedTrades = failedTrades;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getTotalOrderDuration()
    {
        return totalOrderDuration;
    }

    public void setTotalOrderDuration( BigDecimal totalOrderDuration )
    {
        this.totalOrderDuration = totalOrderDuration;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getTotalTradeDuration()
    {
        return totalTradeDuration;
    }

    public void setTotalTradeDuration( BigDecimal totalTradeDuration )
    {
        this.totalTradeDuration = totalTradeDuration;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getNumSweeps()
    {
        return numSweeps;
    }

    public void setNumSweeps( BigDecimal numSweeps )
    {
        this.numSweeps = numSweeps;
    }




    @Representation(names = {"COMPLETE", "SUMMARY"})
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getLastEvent() {
		return lastEvent;
	}

	public void setLastEvent(long lastEvent) {
		this.lastEvent = lastEvent;
	}
	private Collection<Trade> trades;


    public Order() {
    }

    @Representation (names = {"COMPLETE"})
    public String getNarrative() {
		return narrative;
	}

	public void setNarrative(String narrative) {
		this.narrative = narrative;
	}
	
    @Representation (names = {"COMPLETE", "SUMMARY"})
    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }
    
    @Representation (neverExclude = true)
    public long getCreated() {
        return createdTimestamp.getTime();
    }

    
    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getCcyPair() {
        return ccyPair;
    }

    public void setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getBuySell() {
        return buySell;
    }

    public void setBuySell(String buySell) {
        this.buySell = buySell;
    }



    @Representation (names = {"COMPLETE", "SUMMARY"})
    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    @Representation(neverExclude = true)
    public String getOrderID() {
        return orderID;
    }


    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    @Representation
    public String getPersistent() {
        return persistent;
    }

    public void setPersistent(String persistent) {
        this.persistent = persistent;
    }

    @Representation
    public String getClientOrderId() {
        return clientOrderId;
    }

    public void setClientOrderId(String clientOrderId) {
        this.clientOrderId = clientOrderId;
    }

    public String getTenor() {
        if (tenor == null || tenor.equals("null"))
    		return "";
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    @Representation
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Representation
    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    @Representation
    public String getDealt() {
        return dealt;
    }

    public void setDealt(String dealt) {
        this.dealt = dealt;
    }

    @Representation
    public String getTimeInForce() {
        return timeInForce;
    }

    public void setTimeInForce(String timeInForce) {
        this.timeInForce = timeInForce;
    }

    @Representation (names = {"COMPLETE", "SUMMARY"})
    public String getOrderRate() {
        return formatOrderRate(orderRate);
    }

    public void setOrderRate(String orderRate) {
        this.orderRate = orderRate;
    }

    @Representation (names = {"COMPLETE", "SUMMARY"})
    public Long getOrderAmt() {
        return orderAmt;
    }

    public void setOrderAmt(Long orderAmt) {
        this.orderAmt = orderAmt;
    }

    @Representation (names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getFilledAmt() {
        return filledAmt;
    }

    public void setFilledAmt(BigDecimal filledAmt) {
        this.filledAmt = filledAmt;
    }

    @Representation (names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getFilledPct() {
        return filledPct;
    }

    public void setFilledPct(BigDecimal filledPct) {
        this.filledPct = filledPct;
    }

    @Representation (names = {"COMPLETE", "SUMMARY"})
    public String getFillRate() {
        return fillRate;
    }

    public void setFillRate(String fillRate) {
        this.fillRate = fillRate;
    }

    @Representation
    public BigDecimal getPI() {
        return PI;
    }

    public void setPI(BigDecimal PI) {
        this.PI = PI;
    }

    @Representation
    public BigDecimal getOrdAmtUSD() {
        return ordAmtUSD;
    }

    public void setOrdAmtUSD(BigDecimal orderAmtUSD) {
        this.ordAmtUSD = orderAmtUSD;
    }

    @Representation
    public BigDecimal getFillAmtUSD() {
        return fillAmtUSD;
    }

    public void setFillAmtUSD(BigDecimal fillAmtUSD) {
        this.fillAmtUSD = fillAmtUSD;
    }

    @Representation
    public BigDecimal getPpnl() {
        return ppnl;
    }

    public void setPpnl(BigDecimal ppnl) {
        this.ppnl = ppnl;
    }

    @Representation (names = {"COMPLETE", "SUMMARY"})
    public Collection<Trade> getTrades() {
        return trades;
    }

    public void setTrades(Collection<Trade> trades) {
        this.trades = trades;
    }

    @Override
    public String getObjectIdentifier() {

        return orderID;  //To change body of implemented methods use File | Settings | File Templates.
    }
    @Override
   // @Representation  (neverExclude = true)
    public String getThisUri() {
        return "orders/" + getObjectIdentifier();
    }

    @Representation (names = {"COMPLETE", "SUMMARY"})
	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	public String formatOrderRate(String orderRate)	{
		DecimalFormat df = new DecimalFormat("#.########");
		try	{
			
			Double doubleRate = Double.parseDouble(orderRate);
			return df.format(doubleRate);
		}
		catch (Exception exception){
		}
		
		try	{
			int index = orderRate.indexOf("from");
			int startIndex = 0;
			if (index == -1)	{
				startIndex = 10;
			}
			else	{
				startIndex = index + 5;
			}
			
			String someString = df.format(Double.parseDouble(orderRate.substring(startIndex)));
			String startingString = orderRate.substring(0, startIndex);
			
			return startingString + someString;
		}
		catch (Exception exception)	{
					}
		return orderRate;
	}

	public Timestamp getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

    @Representation (names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getMarketRange() {
        // get the pips factor
        /*double pipsFactor = OrderUtils.getPipsForCP(ccyPair  ) ;
        if (pipsFactor > 0.0 )
        {
            //return BigDecimal.valueOf( marketRange.doubleValue()/pipsFactor );
            return BigDecimal.valueOf(marketRange.doubleValue());
        }*/
        return marketRange;
    }

    public void setMarketRange(BigDecimal marketRange) {
        this.marketRange = marketRange;
    }

    public String getSide()
    {
        return side;
    }

    public void setSide( String side )
    {
        this.side = side;
    }
}

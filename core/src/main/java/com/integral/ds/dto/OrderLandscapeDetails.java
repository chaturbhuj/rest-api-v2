package com.integral.ds.dto;

import java.util.List;

public class OrderLandscapeDetails {
	private List<OrderSummary> orderSummary = null;
	private List<TradeSummary> tradeSummary = null;
	
	public List<OrderSummary> getOrderSummary() {
		return orderSummary;
	}
	public void setOrderSummary(List<OrderSummary> orderSummary) {
		this.orderSummary = orderSummary;
	}
	public List<TradeSummary> getTradeSummary() {
		return tradeSummary;
	}
	public void setTradeSummary(List<TradeSummary> tradeSummary) {
		this.tradeSummary = tradeSummary;
	}
}

package com.integral.ds.dto;

/**
 * S3 object representation.
 *
 * @author Rahul Bhattacharjee
 */
public class S3ObjectIdentifier {

    private String bucket;
    private String key;
    private String fileName;

    public S3ObjectIdentifier() {}

    public S3ObjectIdentifier(String bucket, String key, String fileName) {
        this.bucket = bucket;
        this.key = key;
        this.fileName = fileName;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        return "S3ObjectIdentifier{" +
                "bucket='" + bucket + '\'' +
                ", key='" + key + '\'' +
                ", fileName='" + fileName + '\'' +
                '}';
    }
}

package com.integral.ds.model;

import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.util.ResourceUtils;

import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class CompleteQuoteSample {

    private String provider;
    private String stream;
    private List<Quote> quotes;

    public CompleteQuoteSample(String provider, String stream, List<Quote> quotes) {
        this.provider = provider;
        this.stream = stream;
        this.quotes = quotes;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public List<RatesObject> getRatesObjectListForIndex(int index) {
        return ResourceUtils.transformRateObject(quotes.get(index).getRates(), provider, stream);
    }
}

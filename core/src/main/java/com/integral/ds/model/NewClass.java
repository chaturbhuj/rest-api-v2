package com.integral.ds.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.integral.ds.representation.Representation;

/**
 * Created with IntelliJ IDEA. User: vchawla Date: 10/10/13 Time: 4:39 PM To
 * change this template use File | Settings | File Templates.
 */
@XmlRootElement
public class NewClass extends IntegralObject {

    private String ORG;
    private String EXECTIME;
    private String CCYPAIR;
    private String MKRTKR;
    private String TYPE;
    private String BUYSELL;
    private String DEALER;
    private String ORDERID;
    private String TRADEID;
    private String CPTYACCOUNT;
    private String CPTY;
    private Timestamp FIXINGDATE;
    private String ACCOUNT;
    private String DEALT;
    private String PBACCOUNT;
    private Timestamp TRADEDATE;
    private BigDecimal SPOTR;
    private BigDecimal BASEAMT;
    private BigDecimal RATE;
    private String SDACCOUNT;
    private String MAKERREFID;
    private String WORKFLOW;
    private BigDecimal FWDPOINTS;
    private BigDecimal USDRATE;
    private String PRIMEBROKER;
    private String TRDCOMMENT;
    private String COVEREDTRADEID;
    private String STATUS;
    private Timestamp VALUEDATE;
    private String TENOR;
    private BigDecimal USDAMT;
    private String STREAM;
    private String SDUSER;
    private BigDecimal TERMAMT;
    private String VIRTUALSERVER;
    private String TAKERREFID;

    public NewClass() {
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getEXECTIME() {
        String fmtTime = null;

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS z");
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS z").parse(EXECTIME);
            Calendar timeCal = Calendar.getInstance();
            Timestamp ts = new Timestamp(date.getTime());
            timeCal.setTime(ts);
            timeCal.add(Calendar.HOUR, 3);
            fmtTime = formatter.format(timeCal.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fmtTime != null) {
            return fmtTime;
        } else {
            return EXECTIME;
        }

    }

    public void setEXECTIME(String EXECTIME) {
        this.EXECTIME = EXECTIME;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getCCYPAIR() {
        return CCYPAIR;
    }

    public void setCCYPAIR(String CCYPAIR) {
        this.CCYPAIR = CCYPAIR;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getMKRTKR() {
        return MKRTKR;
    }

    public void setMKRTKR(String MKRTKR) {
        this.MKRTKR = MKRTKR;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getBUYSELL() {
        return BUYSELL;
    }

    public void setBUYSELL(String BUYSELL) {
        this.BUYSELL = BUYSELL;
    }

    @Representation
    public String getDEALER() {
        return DEALER;
    }

    public void setDEALER(String DEALER) {
        this.DEALER = DEALER;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getORDERID() {
        return ORDERID;
    }

    public void setORDERID(String ORDERID) {
        this.ORDERID = ORDERID;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public String getTRADEID() {
        return TRADEID;
    }

    public void setTRADEID(String TRADEID) {
        this.TRADEID = TRADEID;
    }

    @Representation
    public String getCPTYACCOUNT() {
        return CPTYACCOUNT;
    }

    public void setCPTYACCOUNT(String CPTYACCOUNT) {
        this.CPTYACCOUNT = CPTYACCOUNT;
    }

    @Representation
    public String getCPTY() {
        return CPTY;
    }

    public void setCPTY(String CPTY) {
        this.CPTY = CPTY;
    }

    @Representation
    public Timestamp getFIXINGDATE() {
        return FIXINGDATE;
    }

    public void setFIXINGDATE(Timestamp FIXINGDATE) {
        this.FIXINGDATE = FIXINGDATE;
    }

    @Representation
    public String getACCOUNT() {
        return ACCOUNT;
    }

    public void setACCOUNT(String ACCOUNT) {
        this.ACCOUNT = ACCOUNT;
    }

    @Representation
    public String getDEALT() {
        return DEALT;
    }

    public void setDEALT(String DEALT) {
        this.DEALT = DEALT;
    }

    @Representation
    public String getPBACCOUNT() {
        return PBACCOUNT;
    }

    public void setPBACCOUNT(String PBACCOUNT) {
        this.PBACCOUNT = PBACCOUNT;
    }

    public Timestamp getTRADEDATE() {
        return TRADEDATE;
    }

    public void setTRADEDATE(Timestamp TRADEDATE) {
        this.TRADEDATE = TRADEDATE;
    }

    @Representation
    public BigDecimal getSPOTR() {
        return SPOTR;
    }

    public void setSPOTR(BigDecimal SPOTR) {
        this.SPOTR = SPOTR;
    }

    @Representation
    public BigDecimal getBASEAMT() {
        return BASEAMT;
    }

    public void setBASEAMT(BigDecimal BASEAMT) {
        this.BASEAMT = BASEAMT;
    }

    @Representation(names = {"COMPLETE", "SUMMARY"})
    public BigDecimal getRATE() {
        return RATE;
    }

    public void setRATE(BigDecimal RATE) {
        this.RATE = RATE;
    }

    @Representation
    public String getSDACCOUNT() {
        return SDACCOUNT;
    }

    public void setSDACCOUNT(String SDACCOUNT) {
        this.SDACCOUNT = SDACCOUNT;
    }

    @Representation
    public String getMAKERREFID() {
        return MAKERREFID;
    }

    public void setMAKERREFID(String MAKERREFID) {
        this.MAKERREFID = MAKERREFID;
    }

    @Representation
    public String getWORKFLOW() {
        return WORKFLOW;
    }

    public void setWORKFLOW(String WORKFLOW) {
        this.WORKFLOW = WORKFLOW;
    }

    @Representation
    public BigDecimal getFWDPOINTS() {
        return FWDPOINTS;
    }

    public void setFWDPOINTS(BigDecimal FWDPOINTS) {
        this.FWDPOINTS = FWDPOINTS;
    }

    @Representation
    public BigDecimal getUSDRATE() {
        return USDRATE;
    }

    public void setUSDRATE(BigDecimal USDRATE) {
        this.USDRATE = USDRATE;
    }

    @Representation
    public String getPRIMEBROKER() {
        return PRIMEBROKER;
    }

    public void setPRIMEBROKER(String PRIMEBROKER) {
        this.PRIMEBROKER = PRIMEBROKER;
    }

    @Representation
    public String getTRDCOMMENT() {
        return TRDCOMMENT;
    }

    public void setTRDCOMMENT(String TRDCOMMENT) {
        this.TRDCOMMENT = TRDCOMMENT;
    }

    @Representation
    public String getCOVEREDTRADEID() {
        return COVEREDTRADEID;
    }

    public void setCOVEREDTRADEID(String COVEREDTRADEID) {
        this.COVEREDTRADEID = COVEREDTRADEID;
    }

    @Representation
    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    @Representation
    public Timestamp getVALUEDATE() {
        return VALUEDATE;
    }

    public void setVALUEDATE(Timestamp VALUEDATE) {
        this.VALUEDATE = VALUEDATE;
    }

    @Representation
    public String getTENOR() {
        return TENOR;
    }

    public void setTENOR(String TENOR) {
        this.TENOR = TENOR;
    }

    @Representation
    public BigDecimal getUSDAMT() {
        return USDAMT;
    }

    public void setUSDAMT(BigDecimal USDAMT) {
        this.USDAMT = USDAMT;
    }

    @Representation
    public String getSTREAM() {
        return STREAM;
    }

    public void setSTREAM(String STREAM) {
        this.STREAM = STREAM;
    }

    @Representation
    public String getSDUSER() {
        return SDUSER;
    }

    public void setSDUSER(String SDUSER) {
        this.SDUSER = SDUSER;
    }

    @Representation
    public BigDecimal getTERMAMT() {
        return TERMAMT;
    }

    public void setTERMAMT(BigDecimal TERMAMT) {
        this.TERMAMT = TERMAMT;
    }

    @Representation
    public String getVIRTUALSERVER() {
        return VIRTUALSERVER;
    }

    public void setVIRTUALSERVER(String VIRTUALSERVER) {
        this.VIRTUALSERVER = VIRTUALSERVER;
    }

    @Representation
    public String getTAKERREFID() {
        return TAKERREFID;
    }

    public void setTAKERREFID(String TAKERREFID) {
        this.TAKERREFID = TAKERREFID;
    }

    @Override
    public String getObjectIdentifier() {
        return getTRADEID();
    }

    @Override
    @Representation(neverExclude = true)
    public String getThisUri() {
        return "trades/" + getObjectIdentifier();
    }
}
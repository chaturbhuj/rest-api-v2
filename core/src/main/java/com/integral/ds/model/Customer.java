package com.integral.ds.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created with IntelliJ IDEA.
 * User: vchawla
 * Date: 10/10/13
 * Time: 5:01 PM
 * To change this template use File | Settings | File Templates.
 */

@XmlRootElement
public class Customer {



    private String name;


    public Customer() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String orders() {
        return "/name/orders";
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Customer{");
        sb.append("name='").append(name).append('\'');

        sb.append('}');
        return sb.toString();
    }
}

package com.integral.ds.model;

import java.util.Collection;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.integral.ds.representation.Representation;


public class LiquidityProviderOrderSummary extends IntegralObject
{
	
	protected String provider;
	protected String currencyPair;
	protected Integer ordersCount;
	protected Double ordersAmount;
	protected SimpleSingleton singleton;
	protected Collection<SimpleSingleton> collectionOfSingleton;
	public LiquidityProviderOrderSummary(){ this(null); }
	public LiquidityProviderOrderSummary(String provider) { this(provider, null); }
	public LiquidityProviderOrderSummary(String provider, String currencyPair) { this(provider, currencyPair, null); } 
	public LiquidityProviderOrderSummary(String provider, String currencyPair, Integer ordersCount) { this(provider, currencyPair, ordersCount, null); } 
	public LiquidityProviderOrderSummary(String provider, String currencyPair, Integer ordersCount, Double ordersAmount) {this(provider, currencyPair, ordersCount, ordersAmount, null); }
	public LiquidityProviderOrderSummary(String provider, String currencyPair, Integer ordersCount, Double ordersAmount, SimpleSingleton singleton) {this(provider, currencyPair, ordersCount, ordersAmount, singleton, null); }
	
	public LiquidityProviderOrderSummary(String provider, String currencyPair, Integer ordersCount, Double ordersAmount, SimpleSingleton singleton, Collection<SimpleSingleton> collectionOfSingleton)
	{
		this.provider = provider;
		this.currencyPair = currencyPair;
		this.ordersCount = ordersCount;
		this.ordersAmount = ordersAmount;
		this.singleton = singleton;
		this.collectionOfSingleton = collectionOfSingleton;
	}

	@Override
	public String getObjectIdentifier() {
		return this.provider;
	}

	@Representation
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	
	@Representation(names= { "COMPLETE", "SUMMARY"})
	public String getCurrencyPair() {
		return currencyPair;
	}
	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair;
	}
	
	@Representation(names= { "COMPLETE", "SUMMARY"})
	public Integer getOrdersCount() {
		return ordersCount;
	}
	public void setOrdersCount(Integer ordersCount) {
		this.ordersCount = ordersCount;
	}
	
	@Representation
	public Double getOrdersAmount() {
		return ordersAmount;
	}
	public void setOrdersAmount(Double ordersAmount) {
		this.ordersAmount = ordersAmount;
	}

	//@Representation
	public String getOrdersURL()
	{
		return String.format("/providers/%s/orders", this.provider);
	}
	
	@Representation(names= { "COMPLETE", "SUMMARY"})
	public SimpleSingleton getSingleton() {
		return singleton;
	}
	public void setCollectionOfSingleton(Collection<SimpleSingleton> collectionOfSingleton) {
		this.collectionOfSingleton= collectionOfSingleton;
	}
	
	@Representation(names= { "COMPLETE", "SUMMARY"})
	public Collection<SimpleSingleton> getCollectionOfSingleton() {
		return collectionOfSingleton;
	}
	public void setSingleton(SimpleSingleton singleton) {
		this.singleton = singleton;
	}
	@Override
	public String toString() {
		return String.format("LP: P:%s CP:%s OC:%s OA:%s", this.provider, this.currencyPair, this.ordersCount, this.ordersAmount);
	}
	
	/**
	 * Returns this instance object as a JSON representation
	 * @return
	 */
	public JSONObject toJson() 
	{
		///TODO: 	Place this method in a superclass that can 
		///			perform this action automatically via reflection					
		try 
		{
			return new JSONObject().
			put("provider", this.getProvider()).
			put("currencyPair", this.getCurrencyPair()).
			put("ordersCount", this.getOrdersCount()).
			put("ordersAmount", this.getOrdersAmount()).
			put("ordersURL", this.getOrdersURL());
		} 
		catch (JSONException je) 
		{
			return null;
		}
    }

}

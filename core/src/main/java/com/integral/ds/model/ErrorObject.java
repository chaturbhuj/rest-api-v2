package com.integral.ds.model;

/**
 * Error object.Serialize this and return the json to browser to show to the user.
 *
 * @author Rahul Bhattacharjee
 */
public class ErrorObject {

    private String errorMessage;
    private ErrorType type;

    public enum ErrorType {error,warning,info}

    public ErrorObject() {
    }

    public ErrorObject(String errorMessage) {
        this.errorMessage = errorMessage;
        setType(ErrorType.error);
    }

    public ErrorObject(String errorMessage, ErrorType type) {
        this.errorMessage = errorMessage;
        this.type = type;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ErrorType getType() {
        return type;
    }

    public void setType(ErrorType type) {
        this.type = type;
    }
}

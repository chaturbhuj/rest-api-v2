package com.integral.ds.model.data;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.integral.ds.model.IntegralObject;
import com.integral.ds.representation.Representation;
import com.integral.ds.representation.RepresentationModel;
import com.integral.ds.util.DataQueryUtils;

/**
 * Version 10/18/2013-9PM
 * @author Awah
 *
 */
public class DataQuery {

    public DataQuery(){/*Default Constructor*/}
    /**
     * Constructor based on a JSON Object that has an implementation of the query
     * @param query
     */

    protected Collection<String> returnFields = new ArrayList<String>();
    protected Collection<DataQueryFilter> filters  = new ArrayList<DataQueryFilter>();
    protected Collection<DataQueryAggregation> returnAggregations  = new ArrayList<DataQueryAggregation>();
    protected String rootUri="";
    public String getRootUri(){return this.rootUri;}
    public void setRootUri(String rootUri){this.rootUri=rootUri;}
    /**
     * A list of return fields associated with this data query.
     * Note: this attribute is mutually exclusive with the
     * returnAggregations attribute, meaning that if values are
     * specified for this attribute, then the returnAggregations
     * attribute should be empty (or it would be ignored) and
     * vice-versa
     * @return
     */
    public Collection<String> getReturnFields ()
    {
        return this.returnFields;
    }

    /**
     * A list of filters associated with this data query
     * @return
     */
    public Collection<DataQueryFilter> getFilters ()
    {
        return this.filters;
    }

    /**
     * A list of aggregations to be associated with this
     * data query instance
     * Note: this attribute is mutually exclusive with the
     * returnAggregations attribute, meaning that if values are
     * specified for this attribute, then the returnAggregations
     * attribute should be empty (or it would be ignored) and
     * vice-versa

     * @return
     */
    public Collection<DataQueryAggregation> getReturnAggregations ()
    {
        return this.returnAggregations ;
    }

    /**
     * Applies the various query elements (filter, aggregation, sorting, etc) 
     * against a given collection of objects (source parameter), and returns
     * a transformed collection of objections after the query elements have
     * been applied. This overload of the apply method will attempt to infer
     * the return fields from Representaion annotations on the element of 
     * the passed in source collection; failing that it will use the returnFields
     * collection that is part of this instance of DataQuery
     * @param source
     * @return
     */
    public Collection<?> apply (Collection<?> source)
    {
        return apply(source, (Collection<String> )null, true);
    }
    /**
     * Applies the various query elements (filter, aggregation, sorting, etc) 
     * against a given collection of objects (source parameter), and returns
     * a transformed collection of objections after the query elements have
     * been applied. This overload of the apply method will attempt to infer
     * the return fields from Representaion annotations on the element of 
     * the passed in source collection; failing that it will use the returnFields
     * collection that is part of this instance of DataQuery
     * @param source
     * @return
     */
    public Collection<?> apply (Collection<?> source, boolean explodeReferenceTypes)
    {
        return apply(source, (Collection<String>)null, explodeReferenceTypes);
    }

    /**
     * Applies the various query elements (filter, aggregation, sorting, etc) 
     * against a given collection of objects (source parameter), and returns
     * a transformed collection of objections after the query elements have
     * been applied
     * @param source
     * @return
     */
    public Collection<?> apply(Collection<?> source, RepresentationModel model, boolean explodeReferenceTypes)
    {
        return apply(source, DataQueryUtils.BuildReturnFields(source, model), explodeReferenceTypes);
    }

    /**
     * Applies the various query elements (filter, aggregation, sorting, etc) 
     * against a given collection of objects (source parameter), and returns
     * a transformed collection of objections after the query elements have
     * been applied
     * @param source
     * @return
     */
    public Collection<?> apply (Collection<?> source, Collection<String> overrideReturnFields, boolean explodeReferenceTypes)
    {
        if(source!=null)
        {
            source=applyFilters(source, this.getFilters());
            source=applyAggregations(source, this.getReturnAggregations());
            boolean overridePassedInReturnFieldsIsNullOrEmpty = (overrideReturnFields==null || (overrideReturnFields!=null && overrideReturnFields.size()==0));
            source = applyPrunings(source, overridePassedInReturnFieldsIsNullOrEmpty? this.getReturnFields() : overrideReturnFields, explodeReferenceTypes);

        }
        return source;
    }

    /**
     * Reshapes every object in a passed in collection and prunes it down to
     * a new object 
     * @param source
     * @param fieldsToReturn
     * @return
     */
    private <T > Collection<?> applyPrunings(Collection<T> source, Collection<String> fieldsToReturn, boolean explodeReferenceTypes)
    {

        if( fieldsToReturn!=null && fieldsToReturn.size()>0 && source!=null && source.size()>0)
        {
            Collection<HashMap<String, Object>> retVal = new ArrayList<HashMap<String, Object>>();
            Class<?> reflectedType= null;
	    	/*
			 * Iterate through every element in the collection, and for each 
			 * element in the collection build a new map (Key/Value pair) of
			 * each return field being returned where the key of the map is
			 * the name of the field being returned and the value of the map
			 * is the actual attribute value of the field being returned on
			 * the instance object within the collection
			 */
            for(T element : source)
            {

                if(reflectedType==null) { reflectedType= element.getClass(); }
                if(reflectedType!=null) /*NPE Safety... no matter what*/
                {
                    retVal.add(applyPruning(element, reflectedType, fieldsToReturn, explodeReferenceTypes));
                }
            }
            return retVal;

        }
        ///If no return fields are specified, return the source collection un-touched
        return source;
    }

    public <T> HashMap<String, Object> applyPruning(T element, Class<?> reflectedType, Collection<String> fieldsToReturn, boolean explodeReferenceTypes)
    {
        HashMap<String, Object> map = new HashMap<String, Object>();

        for(String field : fieldsToReturn)
        {
            try
            {

                ///Assuming there is a proper getter for this "return field"
                java.lang.reflect.Method reflectedMethod = reflectedType.getMethod("get"+ field);
                if(reflectedMethod !=null)
                {
                    if(DataQueryUtils.isPrimitiveType(reflectedMethod.getReturnType()))
                    {
                        map.put(field, reflectedMethod.invoke(element, null));
                    }
                    else if(explodeReferenceTypes)
                    {
                        if(DataQueryUtils.isCollectionType(reflectedMethod.getReturnType()))
                        {
                            try
                            {
                                Collection<?> referenceObjectValues = (Collection<?>)reflectedMethod.invoke(element, (Object[])null);

                                //referenceObjectCollection.add(referenceObjectValues);
                                Collection<?> explodedReferenceObjectInstance =applyPrunings(referenceObjectValues, DataQueryUtils.BuildReturnFields(referenceObjectValues, RepresentationModel.COMPLETE), explodeReferenceTypes);
                                map.put(field, explodedReferenceObjectInstance);
                            }
                            catch(Exception ex)
                            {
                                ex.printStackTrace();
                            }
                        }
                        else
                        {
                            Object referenceObjectValue = reflectedMethod.invoke(element, null);
                            HashMap<String, Object> explodedReferenceObjectInstance =null;
                            if(referenceObjectValue!=null)
                            {
                                Class<?> refereceObjectType = referenceObjectValue.getClass();
                                explodedReferenceObjectInstance =applyPruning(referenceObjectValue, refereceObjectType, DataQueryUtils.BuildReturnFields(refereceObjectType, RepresentationModel.COMPLETE), explodeReferenceTypes);
                            }
                            map.put(field, explodedReferenceObjectInstance);
                        }
                    }
                    else
                    {
                        if (reflectedMethod.isAnnotationPresent(Representation.class) && element instanceof IntegralObject)
                        {
                            Object reflectedMethodValue = reflectedMethod.invoke(element, null);
                            IntegralObject ioElement = (IntegralObject)element;
                            Representation rep = reflectedMethod.getAnnotation(Representation.class);
                            String referenceUri=(rep.referenceUriFormat()!=null && rep.referenceUriFormat().length()>0)?rep.referenceUriFormat():reflectedMethod.getName().replace("get","");
                            if(IntegralObject.class.isAssignableFrom( reflectedMethod.getReturnType()))
                            {
                                Object objectId="";
                                if(reflectedMethodValue!=null)
                                {
                                    ///If we are dealing with an integral object then we can at least
                                    ///extract the object identifier and use that in the formatstring
                                    ///for the reference URI. However if it isn't an IntegralObject
                                    ///and say for example a collection, then the referenceURIFormat
                                    ///annotation parameter should do on its own
                                   objectId=((IntegralObject)reflectedMethodValue).getObjectIdentifier();
                                }
                                referenceUri= String.format(referenceUri+"/%s", objectId);

                            }


                            //map.put(field,  reflectedMethodValue!=null?String.format("%s/%s", ioElement.getThisUri() , referenceUri):null);
//                            map.put(field,  String.format("%s/%s/%s",this.getRootUri(), ioElement.getThisUri() , referenceUri));

                            map.put(field,  String.format("/%s/%s",ioElement.getThisUri() , referenceUri));

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
        return map;
    }
    /**
     * Applies the query filters to a given collection (source) of data
     * @param source
     * @param filters
     * @return
     * @throws SecurityException
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    public static <T> Collection<T> applyFilters(Collection<T> source, Collection<DataQueryFilter> filters)
    {
        if(filters==null || (filters!=null && filters.size()==0))
        {
            ///If no filters were supplied, then we should return
            ///the original collection that was sent in.
            return source;
        }
        ///For performance measurement
        long startTime = System.currentTimeMillis();

        ArrayList<T> retainer = new ArrayList<T>();
        if(source!=null && filters!=null && filters.size()>0)
        {
            Class<?> reflectedType= null;
            for(T element : source)
            {
                boolean retain= true;
                for(DataQueryFilter filter :  filters)
                {
                    if(reflectedType==null){reflectedType= element.getClass();}

                    try {
                        java.lang.reflect.Method reflectedMethod = reflectedType.getMethod("get"+ filter.getField());
                        if(!filter.evaluate( reflectedMethod.invoke(element, null).toString()))
                        {
                            retain =false;
                            break;
                        }

                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    }
                }
                if(retain) {  retainer.add(element); }
            }
            long stopTime = System.currentTimeMillis();
            long elapsedTime = stopTime - startTime;
            System.out.println(">Filtering took: " + elapsedTime +" ms.");
            System.out.println(">retainer size " + retainer.size() +" elements.");
        }
        return retainer;
    }

    public static Collection<?> applyAggregations(Collection<?> source, Collection<DataQueryAggregation> aggregations)
    {
        ///TOOD: There is a lot to do here..
		/*
    	Collection<?> retVal = null;
    	if(source!=null && aggregations!=null && aggregations.size()>0)
    	{
    		
    	}
    	return retVal;
    	*/
        return source;
    }


}

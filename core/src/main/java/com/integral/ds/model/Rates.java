package com.integral.ds.model;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Rates implements Comparable<Rates> {
	
	private String prvdr;
	private String strm;
	private String ccyp;
	private BigDecimal tmstmp;
	private int lvl;
	private String status;
	private BigDecimal bid_price;
	private BigDecimal bid_size;
	private BigDecimal ask_price;
	private BigDecimal ask_size;
	private String guid;
	private String quoteId;
	private String effTime;
	private String srvr;
	private String adptr;
	private String valueDate;
	 
	public String getPrvdr() {
		return prvdr;
	}
	public void setPrvdr(String prvdr) {
		this.prvdr = prvdr;
	}
	public String getStrm() {
		return strm;
	}
	public void setStrm(String strm) {
		this.strm = strm;
	}
	public String getCcyp() {
		return ccyp;
	}
	public void setCcyp(String ccyp) {
		this.ccyp = ccyp;
	}
	public BigDecimal getTmstmp() {
		return tmstmp;
	}
	public void setTmstmp(BigDecimal tmstmp) {
		this.tmstmp = tmstmp;
	}
	public int getLvl() {
		return lvl;
	}
	public void setLvl(int lvl) {
		this.lvl = lvl;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getBid_price() {
		return bid_price;
	}
	public void setBid_price(BigDecimal bid_price) {
		this.bid_price = bid_price;
	}
	public BigDecimal getBid_size() {
		return bid_size;
	}
	public void setBid_size(BigDecimal bid_size) {
		this.bid_size = bid_size;
	}
	public BigDecimal getAsk_price() {
		return ask_price;
	}
	public void setAsk_price(BigDecimal ask_price) {
		this.ask_price = ask_price;
	}
	public BigDecimal getAsk_size() {
		return ask_size;
	}
	public void setAsk_size(BigDecimal ask_size) {
		this.ask_size = ask_size;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(String quoteId) {
		this.quoteId = quoteId;
	}
	public String getEffTime() {
		return effTime;
	}
	public void setEffTime(String timestamp) {
		this.effTime = timestamp;
	}
	public String getSrvr() {
		return srvr;
	}
	public void setSrvr(String srvr) {
		this.srvr = srvr;
	}
	public String getAdptr() {
		return adptr;
	}
	public void setAdptr(String adptr) {
		this.adptr = adptr;
	}
	public String getValueDate() {
		return valueDate;
	}
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}
	
	@Override
	public int compareTo(Rates o) {
		return this.tmstmp.compareTo(o.tmstmp);
	}
	
}
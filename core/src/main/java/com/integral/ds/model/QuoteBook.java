package com.integral.ds.model;

import java.util.List;

/**
 * Quote book.Collection of AggregatedBookTuple.
 *
 * @author Rahul Bhattacharjee
 */
public class QuoteBook {

    private List<AggregatedBookTuple> tuples;

    public QuoteBook(List<AggregatedBookTuple> tuples) {
        this.tuples = tuples;
    }

    public List<AggregatedBookTuple> getTuples() {
        return tuples;
    }

    public void setTuples(List<AggregatedBookTuple> tuples) {
        this.tuples = tuples;
    }
}

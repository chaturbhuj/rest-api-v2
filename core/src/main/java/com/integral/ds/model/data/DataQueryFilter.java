package com.integral.ds.model.data;

import java.math.BigDecimal;

/**
 * Class has all fields and method required to perform
 * in memory boolean evaluation of a given filter
 * specified by the getField() getOperand() and getValue()
 * attributes respectfully of this class against a passed
 * in value.
 * @author Awah
 *
 */
public class DataQueryFilter 
{

	
	private final String EQUAL ="==";
	private final String ATLEAST =">=";
	private final String ATMOST ="<=";
	private final String LESSTHAN ="<";
	private final String MORETHAN =">";
	private final String NOTEQUAL ="!=";
	private final String LIKE ="%";
	private final String NOTLIKE ="!%";
	
	protected String field;
	protected String operand;
	protected String value;
	
	public DataQueryFilter()
	{
		
	}
	public DataQueryFilter(String field, String operand, String value)
	{
		this.field = field;
		this.operand = operand;
		this.value = value;
	}
	
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getOperand() {
		return operand;
	}
	public void setOperand(String operand) {
		this.operand = operand;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Evaluates the filter based on the value of the passed in compareValue
	 * @param invoke
	 * @return
	 */
	public boolean evaluate(String compareValue) {
		boolean retVal= false;

		switch(this.getOperand())
		{
		case NOTEQUAL:
			//System.out.println("Evaluating Not equal to");
			if(value==null && compareValue==null){return false;}
			if(value != null )
			{
				retVal = !value.equals(compareValue);
			}
			break;

		case ATLEAST:
			//System.out.println("Evaluating At least");
			try
			{
				if(value!=null && compareValue!=null)
				{
					BigDecimal dec1 = new BigDecimal(compareValue);
					BigDecimal dec2 = new BigDecimal(value);
					
					retVal = dec1.compareTo(dec2) >= 0;
					//if(retVal) { System.out.println("To true for compareValue of " + compareValue + " from " + value); }
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				retVal = false;
			}
			break;
			

		case ATMOST:
			//System.out.println("Evaluating At most");
			try
			{
				if(value!=null && compareValue!=null)
				{
					BigDecimal dec1 = new BigDecimal(compareValue);
					BigDecimal dec2 = new BigDecimal(value);
					
					retVal = dec1.compareTo(dec2) <= 0;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				retVal = false;
			}
			break;

		case LESSTHAN:
			//System.out.println("Evaluating less than");
			try
			{
				if(value!=null && compareValue!=null)
				{
					BigDecimal dec1 = new BigDecimal(value);
					BigDecimal dec2 = new BigDecimal(compareValue);
					
					retVal = dec1.compareTo(dec2) < 0;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				retVal = false;
			}
			break;
		case MORETHAN:
			//System.out.println("Evaluating more than");
			try
			{
				if(value!=null && compareValue!=null)
				{
					BigDecimal dec1 = new BigDecimal(value);
					BigDecimal dec2 = new BigDecimal(compareValue);
					
					retVal = dec1.compareTo(dec2) > 0;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				retVal = false;
			}
			break;
		default:	///EQUALS
			//System.out.println("Evaluating eqauls");
			if(value==null && compareValue==null){return true;}
			if(value != null )
			{
				retVal = value.equals(compareValue);
				//if(retVal) { System.out.println("To true for compareValue of " + compareValue + " from " + value); }
			}
		}
		return retVal;
	}
}

package com.integral.ds.model.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.integral.ds.boot.AppConfig;
import com.integral.ds.dto.Order;
import com.integral.ds.dto.Trade;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.model.Customer;
import com.integral.ds.model.LiquidityProvider;
import com.integral.ds.model.LiquidityProviderOrderSummary;
import com.integral.ds.staticdata.ProviderKey;
import com.integral.ds.staticdata.ProviderValue;
import com.integral.ds.staticdata.StaticDataManager;

/**
 * This class represents the data access layer,
 * and is desigend to be a wrapper (in essence)
 * creating a black box for data related operations
 *
 *
 */

@Repository
public class AppDataContext {

    @Autowired
    private ProvidersDao providersDao;

    @Autowired
    private JdbcDao dao;

    @Autowired
    private AppConfig appConfig;

	public Collection<Object> executeQuery(Collection<DataQueryFilter> filters, Collection<DataQueryAggregation> aggregations, Collection<String> returnFields)
	{
		///TODO: Implementation required
		return null;
	}
	
	/*****************************************************************
	 * 
	 * A couple of canned DAL methods for the purposes of covering 
	 * sprint level use cases
	 * 
	 ****************************************************************/
	///
	public Collection<LiquidityProvider> getProviders(Map<String, Object> params, DataQuery queryElements)
	{
		///TODO: 	For now the returned collection is hard-coded, but should soon
		///			have logic to perform actual data look up from data source with 
		///			the application of any of the queryElements such as return fields 
		///			and filters
//		Collection<LiquidityProvider>  retVal = new ArrayList<LiquidityProvider>();
	    Collection<LiquidityProvider> providers = dao.getProviders(params) ;
//
//
//  	retVal.add( new LiquidityProvider("1", "Liquidity Provider 1"));
//		retVal.add( new LiquidityProvider("2", "Liquidity Provider 2"));
//		retVal.add( new LiquidityProvider("3", "Liquidity Provider 3"));
//		retVal.add( new LiquidityProvider("4", "Liquidity Provider 4"));
//		retVal.add( new LiquidityProvider("5", "Liquidity Provider 5"));
		return providers;
	}

	public Collection<LiquidityProviderOrderSummary> getProvidersOrderSummary(Map<String,String> params,DataQuery queryElements)
	{

        Collection<LiquidityProviderOrderSummary> summaries = dao.getOrdersSummary(params);



//		retVal.add( new LiquidityProviderOrderSummary("1", 20.0, 50, 3500000.0));
//		retVal.add( new LiquidityProviderOrderSummary("2", 50.0, 900, 5481522.0));
//		retVal.add( new LiquidityProviderOrderSummary("3", 80.0, 80, 554122.0));
//		retVal.add( new LiquidityProviderOrderSummary("4", 30.0, 70, 752500.0));
//		retVal.add( new LiquidityProviderOrderSummary("ABC", 100.0, 1, 78050.0));

		return summaries;
	}

    public List<Map<String,Object>> getOrdersCountByCurrencyPair(Map<String,String> params, DataQuery dataQuery){

        return dao.countOrdersByCurrencyPair(params);

    }

    public List<String> getCurrencyPairs(Map<String,String> params,DataQuery queryElements){
        return dao.getCurrencyPairs(params);
    }

    public List<Order> getOrders(Map<String,Object> params, DataQuery query) {

       return dao.getOrders(params);
    }

    public Collection<RatesObject> getRates(Map<String,Object> params, DataQuery dataQuery) {
        if (params.containsKey(JdbcDao.ORDER_ID_PARAMETER) )
            return dao.getRatesAroundOrder(params);
        else return dao.getRateAroundTrade(params);
    }

    public Collection<Order> getOrdersForOrg(Map<String, Object> params, DataQuery dataQuery) {
        return dao.getOrdersByOrg(params);
    }

//    public JSONObject getOrdersForOrg(Map<String, Object> params, DataQuery dataQuery) {
//        Collection<Order> orders =  dao.getOrdersByOrg(params);
//        Collection<?> values =  dataQuery.apply(orders, DataQueryUtils.BuildReturnFields(Order.class, RepresentationModel.COMPLETE), false);
//        JSONObject ret = new JSONObject();
//        ret.put("thisURL", appConfig.applicationURL());
//        return ret;
//    }

    public Collection<Trade> getTradesByOrderId(Map<String, Object> params, DataQuery dataQuery) {
        return dao.getTradesByOrderId(params);
    }

     public Collection<Trade> getTradesByOrderIds(Map<String, Object> params, DataQuery dataQuery) {
        return dao.getTradesByOrderIds(params);
    }

     public Collection<Trade> getTradesByOrderIdsAndTimeStamp(Map<String, Object> params, DataQuery dataQuery) {
         List <Trade> tradeList = (List<Trade>) dao.getTradesByOrderIdsandTimeStamp(params);
         List<Trade> returnList = new ArrayList<>();
         Map<String, String> tradeIDStreamMap = new HashMap<String, String>(); 
         for (Trade trade: tradeList)	{
        	 String tradeID = trade.getTRADEID();
        	 String makerTakerFlag = trade.getMKRTKR();
        	 String cpty = trade.getCPTY();
        	 String stream = trade.getSTREAM();
        	 if (makerTakerFlag.equals("Maker"))	{
         		tradeIDStreamMap.put(trade.getTRADEID(), trade.getSTREAM());
        	 }
         	 else	{
         		returnList.add(trade);	
         		}
         	}
        	
         Map<ProviderKey, ProviderValue> staticDataManager = StaticDataManager.getInstance().getProviderMappingForTrades();
         for (Trade trade: returnList)	{
        	 String tradeID = trade.getTRADEID();
        	 String newStream = tradeIDStreamMap.get(tradeID);
        	 trade.setSTREAM(newStream);
        	 
        	 ProviderKey key = new ProviderKey(trade.getCPTY(), trade.getSTREAM());
        	 ProviderValue value = staticDataManager.get(key);
        	 if (value == null){
        		 trade.setTickCpty("NA");
            	 trade.setTickStream("NA");
        	 }
        	 else	{
        		 trade.setTickCpty(value.getTickcpty());
            	 trade.setTickStream(value.getTickstream()); 
        	 }
        	 
        	 
         }
         
         
         return returnList; 
     }
     
    public Collection<Customer> getCustomers(Map<String,Object> params, DataQuery dataQuery){

        return dao.getCustomers(params);
    }

    public Collection<Order> getOrdersForIds(Map<String, Object> params, DataQuery dataQuery) {
        return dao.getOrdersByIds(params);
    }

    public Collection<Trade> getTradesForIds(Map<String, Object> params, DataQuery dataQuery) {

        return dao.getTradesByIds(params);
    }


    public Collection<String> getStreams(Map<String,Object> params, DataQuery dataquery){

        return null;
    }

    public Collection<Trade> getTradesForMultipleOrders(Map<String,Object> params, DataQuery dataQuery){
        return dao.getTradesByOrderIds(params);
    }
    public Collection<Trade> getTrades(Map<String,Object> params, DataQuery dataQuery){
        return dao.getTrades(params);
    }
     public Collection<Order> getOrderKeys(Map<String,Object> params, DataQuery dataQuery){
        return dao.getOrderKeys(params);
    }
     

    
    
    public List<RatesObject> getRateList(Map<String, Object> params)	{
    	return dao.getRates(params);
    }

}

package com.integral.ds.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

@XmlRootElement
public class LiquidityProvider 
{
	
	public LiquidityProvider(){ this(null); }
	public LiquidityProvider(String id) { this(id, null); }
	public LiquidityProvider(String id, String name)
	{
		this.id = id;
		this.name = name;
	}
	protected String id;
	protected String name;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getOrdersURL()
	{
		return String.format("/providers/%s/orders", this.id);
	}
	
	/**
	 * Returns this instance object as a JSON representation
	 * @return
	 */
	public JSONObject toJson() 
	{
		///TODO: 	Place this method in a superclass that can 
		///			perform this action automatically via reflection					
		try 
		{
			return new JSONObject().
			put("id", this.getId()).
			put("name", this.getName()).
			put("ordersURL", this.getOrdersURL());
		} 
		catch (JSONException je) 
		{
			return null;
		}
    }
}

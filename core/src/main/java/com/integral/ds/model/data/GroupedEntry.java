package com.integral.ds.model.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

public class GroupedEntry {

	protected Collection<String> keySet = new ArrayList<String>();
	protected Collection<?> source ;
	public GroupedEntry(Collection<String> keySet , Collection<?> source)
	{
		this.keySet = keySet;
		this.source = source;
	}
	
	public int Count(String field){ return 0;}
	public BigDecimal Average(String field){return new BigDecimal(0);}
	public BigDecimal Max(String field){return new BigDecimal(0);}
	public BigDecimal Min(String field){return new BigDecimal(0);}
	public BigDecimal Sum(String field){return new BigDecimal(0);}

}

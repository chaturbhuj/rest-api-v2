package com.integral.ds.model.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class  Groupable<T> {

	protected Groupable (){}
	
	private HashSet<String> keys = new HashSet<String>();
	private Collection<GroupedEntry> groupedEntries = new ArrayList<GroupedEntry>();
	private Collection<T> dataSource=null;
	
	protected void setKeys(HashSet<String> keys) {this.keys = keys;}
	protected void setDataSource(Collection<T> dataSource) {  this.dataSource=dataSource;  }
	public static <T> Groupable<T> GroupBy(Collection<T> dataSource, String groupField)
	{
		Groupable<T> groupable = new Groupable<T> ();
		groupable.setKeys (new HashSet<String>(DataUtils.Select(dataSource, groupField)));
		groupable.setDataSource(dataSource); 
		return groupable;
		
	}
	
	
	
}

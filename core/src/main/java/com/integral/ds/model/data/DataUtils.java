package com.integral.ds.model.data;

import java.util.ArrayList;
import java.util.Collection;

public class DataUtils {
	public static <T> Collection<String> Select (final Collection<T> source, String shapeField)
	{
		Class reflectedType=null;
		ArrayList<String> retVal= new ArrayList<String>();
		for(T element : source)
		{
			try {

				if(reflectedType==null){reflectedType=  element.getClass();}
				java.lang.reflect.Method reflectedMethod = reflectedType.getMethod("get"+ shapeField );
				retVal.add(reflectedMethod.invoke(element, null).toString());
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}

		}
		return retVal;
	}
}

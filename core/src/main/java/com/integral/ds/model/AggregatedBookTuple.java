package com.integral.ds.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Bean representing aggregated books tuple.
 *
 * @author Rahul Bhattacharjee
 */
public class AggregatedBookTuple {

    private Date bidDate;
    private String bidProvider;
    private String bidStream;
    private BigDecimal bidSize;
    private int bidTier;
    private BigDecimal bidRate;
    private BigDecimal offerRate;
    private int offerTier;
    private BigDecimal offerSize;
    private String offerProvider;
    private String offerStream;
    private Date offerDate;

    public Date getBidDate() {
        return bidDate;
    }

    public AggregatedBookTuple setBidDate(Date bidDate) {
        this.bidDate = bidDate;
        return this;
    }

    public String getBidProvider() {
        return bidProvider;
    }

    public AggregatedBookTuple setBidProvider(String bidProvider) {
        this.bidProvider = bidProvider;
        return this;
    }

    public String getBidStream() {
        return bidStream;
    }

    public AggregatedBookTuple setBidStream(String bidStream) {
        this.bidStream = bidStream;
        return this;
    }

    public BigDecimal getBidSize() {
        return bidSize;
    }

    public AggregatedBookTuple setBidSize(BigDecimal bidSize) {
        this.bidSize = bidSize;
        return this;
    }

    public int getBidTier() {
        return bidTier;
    }

    public AggregatedBookTuple setBidTier(int bidTier) {
        this.bidTier = bidTier;
        return this;
    }

    public BigDecimal getBidRate() {
        return bidRate;
    }

    public AggregatedBookTuple setBidRate(BigDecimal bidRate) {
        this.bidRate = bidRate;
        return this;
    }

    public BigDecimal getOfferRate() {
        return offerRate;
    }

    public AggregatedBookTuple setOfferRate(BigDecimal offerRate) {
        this.offerRate = offerRate;
        return this;
    }

    public int getOfferTier() {
        return offerTier;
    }

    public AggregatedBookTuple setOfferTier(int offerTier) {
        this.offerTier = offerTier;
        return this;
    }

    public BigDecimal getOfferSize() {
        return offerSize;
    }

    public AggregatedBookTuple setOfferSize(BigDecimal offerSize) {
        this.offerSize = offerSize;
        return this;
    }

    public String getOfferProvider() {
        return offerProvider;
    }

    public AggregatedBookTuple setOfferProvider(String offerProvider) {
        this.offerProvider = offerProvider;
        return this;
    }

    public String getOfferStream() {
        return offerStream;
    }

    public AggregatedBookTuple setOfferStream(String offerStream) {
        this.offerStream = offerStream;
        return this;
    }

    public Date getOfferDate() {
        return offerDate;
    }

    public void setOfferDate(Date offerDate) {
        this.offerDate = offerDate;
    }

    @Override
    public String toString() {
        return "AggregatedBookTuple{" +
                "bidDate=" + bidDate +
                ", bidProvider='" + bidProvider + '\'' +
                ", bidStream='" + bidStream + '\'' +
                ", bidSize=" + bidSize +
                ", bidTier=" + bidTier +
                ", bidRate=" + bidRate +
                ", offerRate=" + offerRate +
                ", offerTier=" + offerTier +
                ", offerSize=" + offerSize +
                ", offerProvider='" + offerProvider + '\'' +
                ", offerStream='" + offerStream + '\'' +
                ", offerDate=" + offerDate +
                '}';
    }
}

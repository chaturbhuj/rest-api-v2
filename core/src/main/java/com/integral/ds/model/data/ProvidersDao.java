package com.integral.ds.model.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.springframework.stereotype.Repository;

import com.integral.ds.model.LiquidityProvider;
import com.integral.ds.model.Rates;


@Repository
public class ProvidersDao {
	Connection con=null;
	ResultSet rs=null;
	List<Rates> rateSet;
	String url = "jdbc:phoenix:master2.hadoop:2181";
	String driverClassName="com.salesforce.phoenix.jdbc.PhoenixDriver";
	PreparedStatement ptmt;

	public ProvidersDao(){
		//ptmt = getStmt();
	}
	private PreparedStatement getStmt()
	{
		Connection conn = null;
		PreparedStatement stmt =null;
		try {
			Class.forName(driverClassName);

			String querystring="select DISTINCT ORG from ORDTRADE";
			Properties props = new Properties();

			conn = DriverManager.getConnection(url);

			stmt = conn.prepareStatement(querystring);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stmt;
	}


    public Collection<LiquidityProvider> findProviders() {
        Collection<LiquidityProvider> providers = new ArrayList<LiquidityProvider>();
        if (ptmt != null) {
//            providers = null;

            try {
                ResultSet rs = ptmt.executeQuery();
                while (rs.next()) {
                    LiquidityProvider t = new LiquidityProvider(rs.getString(1), rs.getString(1));
                    providers.add(t);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        return providers;
    }
}
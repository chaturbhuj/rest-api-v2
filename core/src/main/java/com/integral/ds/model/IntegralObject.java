package com.integral.ds.model;

import com.integral.ds.representation.Representation;


/**
 * Baseline class for all Integral Data Objects
 * @author Awah
 *
 */
public abstract class IntegralObject {

	/**
	 * This method should be implemented by class extenders and should return the unique
	 * identifier for the resource which will in turn will be used as part of the ResourceURI
	 * via the getThisURI() method.
	 * @return
	 */
	public abstract String getObjectIdentifier(); 
	
	/**
	 * Returns the Resource Name used by the getThisUri() attribute of this object
	 * Note to Class extenders: This method can be overriden to provide a different
	 * resource
	 * @return
	 */
	
	public String getObjectResourceName() 
	{
		return this.getClass().getSimpleName();
	}
	@Representation(neverExclude=true)
	public String getThisUri()
	{
		return String.format("%s/%s", this.getObjectResourceName(), this.getObjectIdentifier());
	}
}

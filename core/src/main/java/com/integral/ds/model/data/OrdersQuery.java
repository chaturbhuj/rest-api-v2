package com.integral.ds.model.data;

public class OrdersQuery {



	public static final String TRADES_BY_ORDER_IDS_TIMESTAMP_QUERY2 = "select td3.stream, td3.orderid as orderid, org,  "
			+ "extract(EPOCH from exectime)::INT8 * 1000 + extract(MILLISECONDS from exectime) exectime, status, "
			+ "ccypair,  mkrtkr, td3.type as type, td3.buysell as buysell, td3.tradeid as tradeid, td3.cpty as cpty, stream, baseAmt, termAmt, usdAmt "
			+ "td3.baseamt as baseamt,td3.rate as rate from TRADES_MIS TD3"
			+ " where orderid  in (:ORDER_IDS) and exectime > :EXECTIME";

	public static final String TRADES_BY_ORDER_IDS_TIMESTAMP_QUERY = "select td3.stream, td3.orderid as orderid, org, baseAmt, termAmt, usdAmt, "
			+ " exectime, status, "
			+ "ccypair,  mkrtkr, td3.type as type, td3.buysell as buysell, td3.tradeid as tradeid, td3.cpty as cpty, stream, "
			+ "td3.baseamt as baseamt,td3.rate as rate from TRADES_MIS TD3"
			+ " where  orderid  in (:ORDER_IDS)";

	public static final String ORDERS_BY_ID_QUERY = "select extract(EPOCH from CREATED)::INT8 * 1000 + extract(MILLISECONDS from created) CREATED, CCYPAIR, BUYSELL, STATUS, ORG,"
			+ "ORDERTYPE, ORDERID, PERSISTENT, CLIENTORDERID, extract(EPOCH from LASTEVENT)::INT8 * 1000 + extract(MILLISECONDS from LASTEVENT) LASTEVENT,"
			+ "TENOR, ACCOUNT, DEALER, DEALT, TIMEINFORCE, ORDERRATE, ORDERAMT, FILLEDAMT, FILLEDPERCENT As FILLEDPCT, FILLRATE, PI, ORDERAMTUSD AS OrdAmtUSD, FILLEDAMTUSD FILLAMTUSD, PIPNL AS PPNL "
			+ "from ORDERS_MIS where ORDERID = :ORDERID";

	public static final String ORDERS_BY_ORG_QUERY = "select extract(EPOCH from CREATED)::INT8 * 1000 + extract(MILLISECONDS from created) CREATED, CCYPAIR, BUYSELL, STATUS, ORDERTYPE, ORG, ORDERID, PERSISTENT, CLIENTORDERID, "
			+ "TENOR, ACCOUNT, DEALER, DEALT, TIMEINFORCE, ORDERRATE, ORDERAMT, FILLEDAMT, FILLEDPERCENT AS FILLEDPCT, FILLRATE, PI, ORDERAMTUSD AS ORDAMTUSD, FILLEDAMTUSD AS FILLAMTUSD, PIPNL AS PPNL"
			+ " from ORDERS_MIS where ORG = :ORG  and CCYPAIR = :CCYPAIR"
			+ " and CREATED > :FROM and CREATED <  :TO and ordertype = :ORDERTYPE and orderamt > :MINAMOUNT";

	public static void main(String[] args) {
		System.out.println(ORDERS_BY_ID_QUERY);
	}
}

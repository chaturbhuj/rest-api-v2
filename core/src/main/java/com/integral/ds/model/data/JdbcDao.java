package com.integral.ds.model.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.integral.ds.dao.RatesQuery;
import com.integral.ds.dto.Order;
import com.integral.ds.dto.Trade;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.model.Customer;
import com.integral.ds.model.LiquidityProvider;
import com.integral.ds.model.LiquidityProviderOrderSummary;

/**
 * Created with IntelliJ IDEA.
 * User: vchawla
 * Date: 9/26/13
 * Time: 12:32 PM
 * To change this template use File | Settings | File Templates.
 */

@Repository
public class JdbcDao {


    private static final Log log = LogFactory.getLog(JdbcDao.class);

    public final static String LP_PARAMETER = "LP";
    public final static String FROM_DATE_PARAMETER = "FROM";
    public final static String TO_DATE_PARAMETER = "TO";
    public static final String RATE_ORIGIN_DATE = "ORIGIN";
    public static final String RATE_SEARCH_BOUND = "BOUND";
    public static final String ORG_PARAMETER = "ORG";
    public static final String CCYPAIR_PARAMETER = "CCYPAIR";
    public static final String ORDER_ID_PARAMETER = "ORDER_ID";
    public static final String ORDER_IDS_PARAMETER = "ORDER_IDS";
    public static final String TRADE_IDS_PARAMETER = "TRADE_IDS";
    public static final String LOWER_BOUND_PARAMETER = "LOWER_BOUND";
    public static final String UPPER_BOUND_PARAMETER = "UPPER_BOUND";
    public static final String STREAM_PARAMETER = "STREAM";
    public static final String CREATED_PARAMETER="CREATED";
    public static final String EXECTIME_PARAMETER="EXECTIME";
    public static final String MKRTKR_PARAMETER="MKRTKR";
    
    private NamedParameterJdbcTemplate jdbcTemplate;


    private String PROVIDERS_QUERY = "select CPTY as NAME from TRADES GROUP BY CPTY";

    private String CURRENCYPAIRS_QUERY = "select CCYPAIR from ORDERS GROUP BY CCYPAIR" ;

    

    
    
    


    private String ORDERSBYCCYPAIR_QUERY = "select CCYPAIR, count(1) as ORDERS from ORDTRADE where ORG = :LP " +
            "and TO_NUMBER(created) > :FROM  " +
            "group by CCYPAIR";


    private String ORDERSUMMARY_QUERY = "select ORG, CCYPAIR, count(1) as ORDERS, SUM(ORDERAMTUSD) as TOTAL  from ORDTRADE  \n" +
            "where TO_NUMBER(created) > :FROM and TO_NUMBER(created) <  :TO " +
            "group by ORG,CCYPAIR ";



//    private String ORDERS_QUERY = "select * from ORDERS where ORG = :ORG and CCYPAIR = :CCYPAIR and " +
//            "TO_NUMBER(created)ATE > to_date(:FROM, 'yyyy-MM-dd HH:mm:ss.SSS z')";

    //    private String TRADES_BY_ORDER_QUERY = "select * from TRADES2 where orderid in (:ORDER_ID)";


    private String TRADES_BY_ORDER_QUERY = "select /* +INDEX(ndx_trade_orderid) */ orderid, org, to_number(exectime) as exectime,ccypair, MKRTKR, TYPE, BUYSELL, TRADEID, CPTY, BASEAMT from TRADES where orderid  = :ORDER_ID";
//    private String TRADES_ALL_BY_ORDER_QUERY = "select * from Trades2 where ORG = :ORG and exectime = to_date";
    private String CUSTOMERS_QUERY = "select ORG as name from ORDERS GROUP BY ORG";
    private String RATES_BY_ORDER_QUERY = "select * from TICKDATALP ";

  //  private String ORDERS_BY_ID_QUERY = "select * from ORDERS where ORDERID in (:ORDER_IDS)" ;
   




    private String RATES_AROUND_TRADE_QUERY = "select * from tickdatalp where prvdr = :ORG and strm = :STREAM and CCYP = :CCYPAIR and " +
                                                "tmstmp > :LOWER_BOUND and tmstmp < :UPPER_BOUND";


    

    private String ORDER_KEYS_QUERY = "select OD.orderid as orderid, org, ccypair, to_number(created) as created, buysell from ORDERS_TEMP "+
                            "where orderid = :ORDER_ID";

    @Resource(name="dataSource")
    
    public void setDataSource(DataSource dataSource) {
    	this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }


    public List<String> getCurrencyPairs(Map<String, String> params) {
        List<String> retVal = new ArrayList<String>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(CURRENCYPAIRS_QUERY, params);
        for (Map row : rows) {
            String pair = (String) row.get("CCYPAIR");
            retVal.add(pair);
        }
        return retVal;
    }

    public List<Map<String, Object>> countOrdersByCurrencyPair(String provider, Long from, Long to) {
        Map parm = new HashMap();
        parm.put("LP", provider);
        parm.put("FROM", from);
        parm.put("TO", to);
        return this.jdbcTemplate.queryForList(ORDERSBYCCYPAIR_QUERY, parm);

    }
    public List<Map<String, Object>> countOrdersByCurrencyPair(Map<String, String> params) {
//        jdbcTemplate.q
        return this.jdbcTemplate.queryForList(ORDERSBYCCYPAIR_QUERY, params);
    }
    public List<LiquidityProviderOrderSummary> getOrdersSummary(Map<String, String> params) {
        List<LiquidityProviderOrderSummary> retVal = new ArrayList<>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(ORDERSUMMARY_QUERY, params);

        for (Map<String, Object> row : rows) {
            LiquidityProviderOrderSummary val = new LiquidityProviderOrderSummary();
            val.setProvider((String) row.get("ORG"));
//            val.setOrdersCount((Long) row.get("ORDERS"));
//            val.setOrdersAmount((BigDecimal) row.get("TOTAL"));
            val.setCurrencyPair((String) row.get("CCYPAIR"));
            retVal.add(val);
        }
        return retVal;
    }


    public List<Order> getOrders(Map<String, Object> params) {
        //log.error("query: "+ ORDERS_QUERY);
        List<Order> orders1 = jdbcTemplate.query(OrdersQuery.ORDERS_BY_ORG_QUERY, params, new BeanPropertyRowMapper(Order.class));
        return orders1;
    }


    public Collection<Order> getOrdersByOrg(Map<String, Object> params) {
        Collection<Order> orders = jdbcTemplate.query(OrdersQuery.ORDERS_BY_ORG_QUERY,params, new BeanPropertyRowMapper<>(Order.class));
        return orders;
    }

    public Collection<Trade> getTradesByOrderId(Map<String, Object> params) {
        Collection<Trade> trades = jdbcTemplate.query(TRADES_BY_ORDER_QUERY, params, new BeanPropertyRowMapper<>(Trade.class));
        return trades;
    }

    public Collection<Customer> getCustomers(Map<String, Object> params) {
        Collection<Customer> customers = jdbcTemplate.query(CUSTOMERS_QUERY, params, new BeanPropertyRowMapper<>(Customer.class));
        return customers;
    }


    public Collection<LiquidityProvider> getProviders(Map<String, Object> params) {
        Collection<LiquidityProvider> providers = jdbcTemplate.query(PROVIDERS_QUERY, params, new BeanPropertyRowMapper<>(LiquidityProvider.class));
        return providers;
    }

    public Collection<RatesObject> getRatesAroundOrder(Map<String, Object> params) {

        return jdbcTemplate.query(RATES_BY_ORDER_QUERY, params, new BeanPropertyRowMapper<>(RatesObject.class));
    }


    public Collection<Order> getOrdersByIds(Map<String,Object> params){
        return jdbcTemplate.query(OrdersQuery.ORDERS_BY_ID_QUERY, params, new BeanPropertyRowMapper<>(Order.class));
    }

    public Collection<Trade> getTradesByIds(Map<String, Object> params) {
        return null;
    	//return jdbcTemplate.query(OrdersQuery.TRADES_REDSHIFT_QUERY, params, new BeanPropertyRowMapper<>(Trade.class));
    }

    public Collection<RatesObject> getRateAroundTrade(Map<String, Object> params) {
        return jdbcTemplate.query(RATES_AROUND_TRADE_QUERY, params, new BeanPropertyRowMapper<>(RatesObject.class));
    }

    public Collection<Trade> getTradesByOrderIds(Map<String, Object> params) {
        return null;
    	//return jdbcTemplate.query(OrdersQuery.TRADES_BY_ORDER_IDS_QUERY, params, new BeanPropertyRowMapper<>(Trade.class));
    }
    public Collection<Trade> getTrades(Map<String, Object> params) {
        //return jdbcTemplate.query(OrdersQuery.TRADES_QUERY, params, new BeanPropertyRowMapper<>(Trade.class));
    	return null;
    }
    public Collection<Order> getOrderKeys(Map<String, Object> params) {
        return jdbcTemplate.query(ORDER_KEYS_QUERY, params, new BeanPropertyRowMapper<>(Order.class));
    }
    
    
    
    public Collection<Trade> getTradesByOrderIdsandTimeStamp(Map<String, Object> params) {
        return jdbcTemplate.query(OrdersQuery.TRADES_BY_ORDER_IDS_TIMESTAMP_QUERY, params, new BeanPropertyRowMapper<>(Trade.class));
    }
    
    public List<RatesObject> getRates(Map<String, Object> params) {
        return jdbcTemplate.query(RatesQuery.RATESDS_QUERY, params, new BeanPropertyRowMapper<>(RatesObject.class));
    }

}

package com.integral.ds.model.data;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import com.google.gson.Gson;
import com.integral.ds.representation.AnnotatedClass;
import com.integral.ds.representation.Contained;
import com.integral.ds.representation.ModelEntity;
import com.integral.ds.representation.Representation;
import com.integral.ds.representation.RepresentationModel;

/**
 * Created with IntelliJ IDEA.
 * User: vchawla
 * Date: 10/14/13
 * Time: 2:28 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ResponseBuilder {

    @Autowired
    Gson gson;

    public Map<String, Object> buildResponse(Class cl, Object o, RepresentationModel r) {
        Field[] fields = cl.getDeclaredFields();
        Map<String,Object> ret = new HashMap<String,Object>();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Representation.class)) {
                Representation rep = field.getAnnotation(Representation.class);

                List<String> names = Arrays.asList(rep.names());

//                if (names.contains(RepresentationModel.FOREIGN)){
//                    ret.put(field.getName(), buildForeignReferenceRepresentation(field, RepresentationModel.SUMMARY));
//                }
                if (names.contains(r.getName())) {
                    org.springframework.util.ReflectionUtils.makeAccessible(field);
                    System.out.println(field.getName());
                    Object c = ReflectionUtils.getField(field, o);
                    System.out.println(c.getClass());
                    if (c.getClass().isAnnotationPresent(ModelEntity.class)) {
                        Contained child = (Contained)c;
                        ret.put("child", buildResponse(child.getClass(),child, RepresentationModel.SUMMARY));
                    } else {
                        ret.put(field.getName(),c);

                    }
                }

            }


        }
//        System.out.println(ret.entrySet());
        return ret;
    }

    private Map<String, Object> buildForeignReferenceRepresentation(Field field, RepresentationModel r) {


        Map<String,Object> ret = new HashMap<String,Object>();
        ret = buildResponse(field.getClass(),field, RepresentationModel.SUMMARY);

        return ret;



    }


    public static void main(String[] args) {
        AnnotatedClass ac = new AnnotatedClass();
        ac.setAttr1("1");
        ac.setAttr2("2");

        Contained c = new Contained();
        c.setContainedName("Contained");
        ac.setC(c);
        ResponseBuilder rb = new ResponseBuilder();
        Map<String, Object> map = rb.buildResponse(ac.getClass(), ac, RepresentationModel.COMPLETE);
        JSONObject obj = new JSONObject(map);

        ac.getAttrs().add("3");
        ac.getAttrs().add("4");




        System.out.println(obj.toString()) ;

    }

}

package com.integral.ds.model;

import com.integral.ds.representation.Representation;

public class SimpleSingleton extends IntegralObject{

	protected String name="";
	
	@Representation
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public SimpleSingleton(String name){this.name=name;}
	
	@Override
	public String getObjectIdentifier() 
	{
		
		return this.name;
	}
}

package com.integral.ds.emscope.quote.impl;

import com.integral.ds.dao.s3.S3FilesRatesDao;
import com.integral.ds.emscope.quote.QuoteTransformer;
import com.integral.ds.emscope.quote.RatesQuoteService;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.util.BidOfferCalculator;
import com.integral.ds.util.DataQueryUtils;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author Rahul Bhattacharjee
 */
public abstract class AbstractRateQuoteService implements RatesQuoteService {

    protected final Logger LOGGER = Logger.getLogger(getClass());

    protected QuoteTransformer quoteTransformer = new ProfileQuoteTransformerIgnoreOneSidedRates();
    protected BidOfferCalculator calculator = new BidOfferCalculator();

    @Override
    public Quote getQuoteAtTime(String provider, String stream, String ccyPair, Date point) {
        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("Executing get quote in time for provider " + provider + " stream " + stream + " ccy pair " + ccyPair
                    +" time " + point);
        }

        Quote quote = new Quote();
        quote.setSampleTime(point);
        Date start = addMinutesToDate(point, -1);
        Date end = addMillisecondsToDate(point,+1);
        S3FilesRatesDao ratesDao = new S3FilesRatesDao();
        List<RestRatesObject> rates = ratesDao.getRates(provider,stream,ccyPair,start,end);

        NavigableMap<Long,List<RestRatesObject>> subRateMap = new TreeMap<Long,List<RestRatesObject>>();

        for(RestRatesObject ratesObject : rates) {
            long time = ratesObject.getTmstmp();
            List<RestRatesObject> rateList = subRateMap.get(time);
            if(rateList == null) {
                rateList = new ArrayList<RestRatesObject>();
                subRateMap.put(time,rateList);
            }
            rateList.add(ratesObject);
        }
        Long prevailingQuote = subRateMap.floorKey(point.getTime());
        List<RestRatesObject> value = subRateMap.get(prevailingQuote);
        quote.addRates(value);
        DataQueryUtils.getNormalizedQuote(quote);
        return quote;
    }

    @Override
    public RestRatesObject getQuoteAtTimeForVolume(String provider, String stream, String ccyPair, Date point, long volume) {
        Quote quote = getQuoteAtTime(provider,stream,ccyPair,point);

        BigDecimal bidPrice = calculator.getVWAPBidAtVolume(quote,volume);
        BigDecimal offerPrice = calculator.getVWAPOfferAtVolume(quote,volume);

        RestRatesObject rate = new RestRatesObject();
        BigDecimal vol = new BigDecimal(volume);

        if(offerPrice != null) {
            rate.setAskPrice(offerPrice);
            rate.setAskSize(vol);
        }

        if(bidPrice != null) {
            rate.setBidPrice(bidPrice);
            rate.setBidSize(vol);
        }

        rate.setCcyPair(ccyPair);
        rate.setTier(1);
        rate.setStatus("Active");
        rate.setTmstmp(DataQueryUtils.getTimeWithoutMills(quote.getQuoteTime().getTime()));
        return rate;
    }

    protected Date addMinutesToDate(Date date,int minutes) {
        return addToDate(date, Calendar.MINUTE,minutes);
    }

    protected Date addMillisecondsToDate(Date date,int mills) {
        return addToDate(date, Calendar.MILLISECOND,mills);
    }

    protected Date addToDate(Date date,int unit,int number) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(unit,number);
        return calendar.getTime();
    }

    protected List<Quote> profileQuotes(List<Quote> quotes) {
        List<Quote> returnQuotes = new ArrayList<Quote>();
        for(Quote quote : quotes) {
            Quote profiledQuote = profileQuote(quote);
            if(!profiledQuote.isEmpty()) {
                returnQuotes.add(profiledQuote);
            }
        }
        return returnQuotes;
    }

    private Quote profileQuote(Quote quote) {
        return quoteTransformer.transform(quote);
    }
}

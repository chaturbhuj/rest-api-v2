package com.integral.ds.emscope.orders;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.integral.ds.dto.Order;
import com.integral.ds.s3.S3StreamReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

public class NarrativeGenerator {

    private static final Logger LOGGER = Logger.getLogger(NarrativeGenerator.class);
	
	private static final String MESSAGE = 
			"{orgName} Order ID: {orderID}~"
			+ "The {orgName}:{dealer} placed a/an {orderType} order "
			+ "to {buySell} {ccyp} @ {rate} for an amount of {orderAmt} {ccy}.~"
			+ "Time in Force: {timeInForce} The order was placed at {created}, GMT~";

	private static final String STATUS_A  = " It is still an Active order";
	private static final String STATUS_F = " It was completely filled @ {fillRate}.";
	private static final String STATUS_P = " It was partially filled @ {fillRate} for an amount of {filledAmt}. ";
	private static final String STATUS_X = " It was cancelled. The partially filled amount was {filledAmt} @ {fillRate}.";
	private static final String STATUS_Z = " It got expired at {lastEvent}. The partially filled amount was {filledAmt} @ {fillRate}.";
	private static final String STATUS_D = " It got declined and the order was completed at {lastEvent}";

	public String getNarrative (Order order)	{
		String returnMessage = "";

        try	{
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			DecimalFormat amountDf = new DecimalFormat("###,###,###,###.###");

            returnMessage = MESSAGE.replace("{orgName}", order.getOrg());
			returnMessage = returnMessage.replace("{orderID}", order.getOrderID());
			returnMessage = returnMessage.replace("{dealer}", order.getDealer());
			returnMessage = returnMessage.replace("{orderType}", order.getOrderType());
			returnMessage = returnMessage.replace("{buySell}", order.getBuySell());
			returnMessage = returnMessage.replace("{ccyp}", order.getCcyPair());

            if (order.getMarketRange() != null && order.getMarketRange().doubleValue() > 0.0) {
                String rateMessage = String.format( "Range %s from %s", formatOrderRate(order.getMarketRange().doubleValue()+ ""), formatOrderRate(order.getOrderRate()));
                returnMessage = returnMessage.replace("{rate}", rateMessage );
            } else {
                returnMessage = returnMessage.replace( "{rate}", formatOrderRate( order.getOrderRate() ) );
            }

            returnMessage = returnMessage.replace("{orderAmt}", amountDf.format(order.getOrderAmt()));
			returnMessage = returnMessage.replace("{timeInForce}", order.getTimeInForce());
			returnMessage = returnMessage.replace("{created}", sdf.format(new Date(order.getCreated())));
			returnMessage = returnMessage.replace("{ccy}", order.getDealt());

            returnMessage = appendOrderStatusToMessage(OrderType.valueOf(order.getOrderStatus()),returnMessage);

			if ( order.getFillRate()  != null)
				returnMessage = returnMessage.replace("{fillRate}", order.getFillRate() + "");
			
			if ( order.getPpnl()  != null)
				returnMessage = returnMessage.replace("{ppnl}", order.getPpnl().floatValue() + "");
			
			if ( order.getFilledAmt()  != null)
				returnMessage = returnMessage.replace("{filledAmt}", order.getFilledAmt().longValue() + "");
			
			if ( order.getLastEvent()  != 0l)
				returnMessage = returnMessage.replace("{lastEvent}", sdf.format(new Date(order.getLastEvent())));

            LOGGER.debug("Returning narrative message for order " + order + " , message " + returnMessage);
        }
		catch (Exception exception)	{
            LOGGER.error("Exception while generating narrative for order " + order , exception);
		}
		return returnMessage;
	}

    private String appendOrderStatusToMessage(OrderType orderType, String returnMessage) {
        switch (orderType) {
            case A: returnMessage += STATUS_A; break;
            case F: returnMessage += STATUS_F; break;
            case P: returnMessage += STATUS_P; break;
            case X: returnMessage += STATUS_X; break;
            case Z: returnMessage += STATUS_Z; break;
            case D: returnMessage += STATUS_D; break;
            default:
        }
        return returnMessage;
    }

    public String formatOrderRate(String orderRate)	{
		DecimalFormat df = new DecimalFormat("#.#####");
		try	{
			Double doubleRate = Double.parseDouble(orderRate);
            return getFormattedDouble(doubleRate);
		} catch (Exception exception){
            LOGGER.error("Ignoring exception while formatting order rate.",exception);
        }
		try	{
			int index = orderRate.indexOf("from");
			int startIndex = 0;
			if (index == -1) {
				startIndex = 10;
			} else {
				startIndex = index + 5;
			}
			String someString = df.format(Double.parseDouble(orderRate.substring(startIndex)));
			String startingString = orderRate.substring(0, startIndex);
			return startingString + someString;
		} catch (Exception exception)	{
            LOGGER.error("Ignoring exception while formatting order rate.",exception);
        }
		return "";
	}

    private String getFormattedDouble(double value) throws Exception {
        DecimalFormat decimalFormat = new DecimalFormat("#.#####");
        try	{
            return decimalFormat.format(value);
        } catch (Exception exception){
            LOGGER.error("Ignoring exception while formatting double " + value,exception);
        }
        return "";
    }
}

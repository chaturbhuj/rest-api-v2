package com.integral.ds.emscope.report;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.integral.ds.commons.reporting.model.RateGranulizationReportResult;
import com.integral.ds.commons.reporting.rategranulization.RateGranulizationReportService;

public class RateGranulizationReportAppDataContext {

	private static final Logger LOGGER = Logger
			.getLogger(RateGranulizationReportAppDataContext.class);

	private final RateGranulizationReportService _rateGranulizationReportService;

	public RateGranulizationReportAppDataContext(
			RateGranulizationReportService rateGranulizationReportService_) {
		_rateGranulizationReportService = rateGranulizationReportService_;
	}

	public boolean isGranulizedRateAvailable(String prvdr, String strm,
			String ccyPair, Date date, int hour) {
		LOGGER.log(Level.INFO, "Received request for : " + prvdr + " " + strm
				+ " " + ccyPair + " " + hour + " " + date);
		RateGranulizationReportResult result =  _rateGranulizationReportService.isGranulizatedDataAvailable(
				prvdr, strm, ccyPair, date, hour);
		return result.isGranulizedRateAvailable(hour);
	}
	
	public Map<Integer,Boolean> getGranulizedRateAvailabilityForDate(String prvdr, String strm,
			String ccyPair, Date date) {
		LOGGER.log(Level.INFO, "Received request for : " + prvdr + " " + strm
				+ " " + ccyPair + " " + date);
		RateGranulizationReportResult result =  _rateGranulizationReportService.isGranulizatedDataAvailable(
				prvdr, strm, ccyPair, date);
		return result.getAvailabilityResult();
	}
	
/*	public static void main(String[] args) {
		RateGranulizationReportAppDataContext context = new RateGranulizationReportAppDataContext(new RateGranulizationReportServiceImpl());
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.set(Calendar.DAY_OF_MONTH, 22);
		System.out.println(context.isGranulizedRateAvailable("BOAN1", "BandC", "EURUSD", cal.getTime(), 10));
	}*/
}

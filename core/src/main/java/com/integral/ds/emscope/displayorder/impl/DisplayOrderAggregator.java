package com.integral.ds.emscope.displayorder.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.TreeMap;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.integral.ds.commons.aggregator.QuoteBook;
import com.integral.ds.commons.model.IDisplayOrderRate;
import com.integral.ds.commons.model.IRate;
import com.integral.ds.commons.model.Quote;
import com.integral.ds.commons.model.Rate;
import com.integral.ds.commons.model.Side;
import com.integral.ds.emscope.displayorder.IDisplayOrderAggregator;

public class DisplayOrderAggregator implements IDisplayOrderAggregator {

	private final static Logger log = LogManager
			.getLogger(DisplayOrderAggregator.class);

	private static final String BID_SIDE = "BidSide-";

	private static final String OFFER_SIDE = "-OfferSide-";

	private final String DO_PROVIDER = "DO_Provider";

	private final String DO_STREAM = "DO_Stream";

	private final SimpleDateFormat _sdf = new SimpleDateFormat(
			"MM.dd.yyyy HH:mm:ss.SSS");
	
	private final Calendar calendar = Calendar.getInstance(TimeZone
			.getTimeZone("GMT"));

	private List<IRate> removeZeroRates(List<IRate> rates, Side side) {
		List<IRate> cleanedUpRates = new ArrayList<>();
		for (IRate rate : rates) {
			if (side.isBid() ? rate.getBidPrice() != 0
					: rate.getOfferPrice() != 0)
				cleanedUpRates.add(rate);
		}

		return cleanedUpRates;
	}

	private IRate getInActiveRateDetails(String ccyPair, long tmstmp) {
		return new Rate(DO_PROVIDER, DO_STREAM, 0, ccyPair, tmstmp, "InActive",
				0.0D, 0, 0.0D, 0, "");
	}

	private IRate getBidDetails(IRate bidRate, int lvl, long tmstmp) {
		IDisplayOrderRate displayOrderRate = (IDisplayOrderRate) bidRate;

		StringBuilder sb = new StringBuilder();
		sb.append(BID_SIDE).append(getGUID(displayOrderRate, Side.BID))
				.append(OFFER_SIDE);

		return new Rate(DO_PROVIDER, DO_STREAM, lvl, bidRate.getCcyPair(),
				tmstmp, "Active", bidRate.getBidPrice(), bidRate.getBidSize(),
				0.0D, 0, sb.toString());

	}

	private IRate getOfferDetails(IRate offerRate, int lvl, long tmstmp) {
		IDisplayOrderRate displayOrderRate = (IDisplayOrderRate) offerRate;

		StringBuilder sb = new StringBuilder();
		sb.append(BID_SIDE).append(OFFER_SIDE)
				.append(getGUID(displayOrderRate, Side.OFFER));

		return new Rate(DO_PROVIDER, DO_STREAM, lvl, offerRate.getCcyPair(),
				tmstmp, "Active", 0.0D, 0, offerRate.getOfferPrice(),
				offerRate.getOfferSize(), sb.toString());
	}

	private String getGUID(IDisplayOrderRate displayOrderRate, Side side) {
		StringBuilder guidBuilder = new StringBuilder();

		if (side.isBid()) {
			guidBuilder.append(displayOrderRate.getGuid()).append("-OrderId-")
					.append(displayOrderRate.getBidSideOrderId())
					.append("-Server-").append(displayOrderRate.getServer())
					.append("-Adapter-").append(displayOrderRate.getAdapter())
					.append("-Tmstmp-").append(displayOrderRate.getTmstmp());
		} else {
			guidBuilder.append(displayOrderRate.getGuid()).append("-OrderId-")
					.append(displayOrderRate.getOfferSideOrderId())
					.append("-Server-").append(displayOrderRate.getServer())
					.append("-Adapter-").append(displayOrderRate.getAdapter())
					.append("-Tmstmp-").append(displayOrderRate.getTmstmp());
		}

		return guidBuilder.toString();
	}

	private IRate getBidOfferDetails(IRate bidRate, IRate offerRate, int lvl,
			long tmstmp) {

		IDisplayOrderRate displayOrderOfferRate = (IDisplayOrderRate) offerRate;

		IDisplayOrderRate displayOrderBidRate = (IDisplayOrderRate) bidRate;

		StringBuilder sb = new StringBuilder();
		sb.append(BID_SIDE).append(getGUID(displayOrderBidRate, Side.BID))
				.append(OFFER_SIDE)
				.append(getGUID(displayOrderOfferRate, Side.OFFER));

		return new Rate(DO_PROVIDER, DO_STREAM, lvl, bidRate.getCcyPair(),
				tmstmp, "Active", bidRate.getBidPrice(), bidRate.getBidSize(),
				offerRate.getOfferPrice(), offerRate.getOfferSize(),
				sb.toString());

	}

	public Map<Long, List<Long>> getSampledTimestamps(List<Long> timestampList) {

		Map<Long, List<Long>> sampledTimestampList = new TreeMap<>();
		if (timestampList == null || timestampList.isEmpty())
			return sampledTimestampList;

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		cal.setTimeInMillis(timestampList.get(0)); // setting the first
													// timestamp

		cal.set(Calendar.MILLISECOND, 0);
		cal.add(Calendar.SECOND, 1);
		Long nextTimestamp = cal.getTimeInMillis();

		Long prevailingTimestampForLastSecond = timestampList.get(0);
		for (Long tmstmp : timestampList) {
			if (tmstmp > nextTimestamp) {
				List<Long> prevailingTimestampFor = new ArrayList<Long>();
				prevailingTimestampFor.add(cal.getTimeInMillis());
				sampledTimestampList.put(prevailingTimestampForLastSecond,
						prevailingTimestampFor);
				long timestampDiffInSeconds = ((tmstmp - nextTimestamp) / 1000);

				cal.add(Calendar.SECOND, 1);

				for (int i = 0; i < timestampDiffInSeconds; i++) {
					sampledTimestampList.get(prevailingTimestampForLastSecond)
							.add(cal.getTimeInMillis());
					cal.add(Calendar.SECOND, 1);
				}
				nextTimestamp = cal.getTimeInMillis();
			}
			prevailingTimestampForLastSecond = tmstmp;
		}
		List<Long> prevailingTimestampFor = new ArrayList<Long>();
		prevailingTimestampFor.add(nextTimestamp);

		sampledTimestampList.put(prevailingTimestampForLastSecond,
				prevailingTimestampFor);

		return sampledTimestampList;
	}

	private class SortingKey implements Comparable<SortingKey> {
		private Long _tmstmp;

		private int _tier;

		public SortingKey(Long _tmstmp, int _tier) {
			this._tmstmp = _tmstmp;
			this._tier = _tier;
		}

		@Override
		public int hashCode() {
			return HashCodeBuilder.reflectionHashCode(this);
		}

		@Override
		public boolean equals(Object obj) {
			return EqualsBuilder.reflectionEquals(this, obj);
		}

		@Override
		public int compareTo(SortingKey o) {
			if (this._tmstmp == o._tmstmp && this._tier == o._tier)
				return 0;

			if (this._tmstmp > o._tmstmp || this._tmstmp == o._tmstmp
					&& this._tier > o._tier)
				return 1;
			else
				return -1;

		}
	}

	@Override
	public List<IRate> aggregateDisplayOrders(
			Map<String, Quote> displayOrderMap_, String ccyPair_,
			Date timestamp_) {

		List<IRate> quoteList = new ArrayList<>();

		List<IRate> output = new ArrayList<>();

		for (Entry<String, Quote> quotesForEachProvider : displayOrderMap_
				.entrySet()) {

			if (quotesForEachProvider != null
					&& quotesForEachProvider.getValue().isActive()) {

				quoteList.addAll(quotesForEachProvider.getValue().getRates());
			}
		}

		QuoteBook quoteBook = new QuoteBook(quoteList, quoteList);
		quoteBook.sort();
		List<IRate> bidRates = removeZeroRates(quoteBook.getBidRates(),
				Side.BID);
		List<IRate> offerRates = removeZeroRates(quoteBook.getOfferRates(),
				Side.OFFER);

		int bidSideDepth = bidRates.size();
		int offerSideDepth = offerRates.size();
		int counter = bidSideDepth < offerSideDepth ? bidSideDepth
				: offerSideDepth;

		// all rates inactive
		if (bidSideDepth == 0 && offerSideDepth == 0) {
			output.add(getInActiveRateDetails(ccyPair_,
					timestamp_.getTime()));
		} else {
			for (int i = 0; i < counter; i++) {
				output.add(getBidOfferDetails(bidRates.get(i),
						offerRates.get(i), i + 1, timestamp_.getTime()));
			}

			if (bidSideDepth > offerSideDepth) {
				for (int i = counter; i < bidSideDepth; i++) {
					output.add(getBidDetails(bidRates.get(i), i + 1,
							timestamp_.getTime()));
				}
			} else {
				for (int i = counter; i < offerSideDepth; i++) {
					output.add(getOfferDetails(offerRates.get(i), i + 1,
							timestamp_.getTime()));
				}
			}
		}
		
		return output;
	}

}

package com.integral.ds.emscope.quote;

import com.integral.ds.emscope.quote.impl.RateQuoteServiceCompleteDownload;
import com.integral.ds.emscope.quote.impl.RateQuoteServiceWithPrevailingRate;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Factory to dispense implementation of quote service based on the number of quote samples requested for.
 *
 * 1. If sparse samples are requested then we would return RateQuoteServiceWithPrevailingRate
 * 2. If a large number of samples are requested for a small duration of time , then RateQuoteServiceCompleteDownload would
 *    be returned.
 *
 * @author Rahul Bhattacharjee
 */
public class QuoteServiceFactory {

    protected final Logger LOGGER = Logger.getLogger(QuoteServiceFactory.class);

    private static final int SAMPLES_PER_HOUR_THRESHOLD  = 5;

    private final static RatesQuoteService QUOTE_SERVICE_SPARSE_DATA = new RateQuoteServiceWithPrevailingRate();
    private final static RatesQuoteService QUOTE_SERVICE_FINER_SAMPLES = new RateQuoteServiceCompleteDownload();

    public static RatesQuoteService getDefaultRateQuoteService()
    {
    	return QUOTE_SERVICE_SPARSE_DATA;
    }
    /**
     * Factory to give appropriate instance/implementation of rate quote service.
     *
     * @param start
     * @param end
     * @param samples
     * @return
     */
    public static RatesQuoteService getRateQuoteService(Date start ,Date end ,int samples) {
        RatesQuoteService quoteService = null;
        long endTime = end.getTime();
        long startTime = start.getTime();

        float duration = (endTime-startTime);
        if(duration<0) {
            throw new IllegalArgumentException("Start time should be before end time");
        }

        float samplesPerMillisecond = samples/duration;

        int samplesPerHour = (int) (samplesPerMillisecond * 1000 * 60 * 60);
        if(samplesPerHour >= SAMPLES_PER_HOUR_THRESHOLD) {
            quoteService = QUOTE_SERVICE_FINER_SAMPLES;
        } else {
            quoteService = QUOTE_SERVICE_SPARSE_DATA;
        }
        return quoteService;
    }
}

package com.integral.ds.emscope.profile;

public class RatesProfileQuery {

	public static final String  RATES_PROFILE = 
			"select prvdr as provider, "
			+ "strm as stream, ccyp as currencyPair, to_number(tmstmp) as timeStamp, tier as tier, hasdata as hasData, twoSidedTickCnt, "
			+ "midprcMean as midPriceMean, sprdAvg as spreadAverage, to_number(lastTickTmstmp) as lastTickTimestamp, lastTickAge as lastTickAge, bidSizeAvg, askSizeAvg,  bidPrcMax, bidPrcMin, askPrcMax, askPrcMin "
			+ " from SECOND_CUBE_TEST where prvdr = :PVDR and strm = :STRM and ccyp = :CCYP and tier = :TIER and tmstmp >= to_date(:FROMTIME) "
			+ "and  tmstmp <= to_date(:TOTIME)";	
	
}

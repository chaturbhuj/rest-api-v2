package com.integral.ds.emscope.rates;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.integral.ds.emscope.RestServiceRuntimeException;
import com.integral.ds.model.ErrorObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.integral.ds.staticdata.StaticDataManager;
import com.owlike.genson.Genson;

@Path("/rates1")
@Component
public class RatesCollectionResource1 {

    private static final Logger LOGGER = Logger.getLogger(RatesCollectionResource1.class);
    private static final String UNEXPECTED_ERROR = "Unexpected error in EMScope.";

    private final static boolean S3_DAO = true;
    static {
        System.out.println("USING S3 Implementation of rates dao ? : " + S3_DAO);
    }

	@Autowired
	private RatesAppDataContext ratesAppDataContext;

	@GET
	@Path("{prvdr}/{stream}/{ccyp}/{fromtime}/{totime}")
	@Produces(MediaType.TEXT_PLAIN)
	public String findRange(@PathParam("prvdr") String provider,@PathParam("stream") String stream,
			@PathParam("ccyp") String ccyPair, @PathParam("fromtime") long fromtime, @PathParam("totime") long toTime) {

        try {
            Genson genson = new Genson();
            Date startTime = new Date(fromtime);
            Date endTime = new Date(toTime);
            Object response = null;

            if(S3_DAO) {
                try {
                    List<RestRatesObject> listOfRates = ratesAppDataContext.getRatesByTimeStampFromS3Files(provider, ccyPair, stream, startTime,endTime , true);
                    response = listOfRates;
                } catch (Exception e) {
                    LOGGER.error("Exception while serializing rates object list.",e);
                    if(e instanceof RestServiceRuntimeException) {
                        response = ((RestServiceRuntimeException)e).getErrorObject();
                    } else {
                        ErrorObject errorObject = new ErrorObject();
                        errorObject.setType(ErrorObject.ErrorType.error);
                        errorObject.setErrorMessage(UNEXPECTED_ERROR + " Message => " + e.getMessage());
                        response = errorObject;
                    }
                }
            } else {
               List<RatesObject> listOfRates = ratesAppDataContext.getRatesByTimeStamp(provider, ccyPair, stream, new Date(fromtime), new Date(toTime), true);
                try {
                    response = listOfRates;
                } catch (Exception e) {
                    LOGGER.error("Exception while serializing rates object list.",e);
                }
            }
            assert response != null;
            return genson.serialize(response);
        }catch (Exception e) {
            LOGGER.error(UNEXPECTED_ERROR,e);
        }
        return UNEXPECTED_ERROR;
	}

	@Path("/ccypairs")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getCurrencyPairs()	{
		
		Set <String> listOfCurrencyPairs = StaticDataManager.getInstance().getListOfCurrencyPairs();
		Genson genson = new Genson();
		try	{
			String returnValue = genson.serialize(listOfCurrencyPairs);
			return returnValue;
		}
		catch (Exception exception)	{
			
		}
		return "";
	}
	
	@Path("/providers")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getCurrencyProvder()	{
		Set <String> listOfCurrencyPairs = StaticDataManager.getInstance().getProviderList();
		Genson genson = new Genson();
		try	{
			String returnValue = genson.serialize(listOfCurrencyPairs);
			return returnValue;
		}
		catch (Exception exception)	{
			
		}
		return "";
	}
	
	@Path("/streams/{provider}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getStreams(@PathParam("provider") String prvdr)	{
		
		List <String> listOfCurrencyPairs = StaticDataManager.getInstance().getStreamList(prvdr);
		Genson genson = new Genson();
		try	{
			String returnValue = genson.serialize(listOfCurrencyPairs);
			return returnValue;
		}
		catch (Exception exception)	{
			
		}
		return "";
	}
}

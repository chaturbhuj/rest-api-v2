package com.integral.ds.emscope.displayorder.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.google.common.base.Optional;
import com.integral.ds.commons.converter.IConverter;
import com.integral.ds.commons.model.IDisplayOrderRate;
import com.integral.ds.commons.model.IRate;
import com.integral.ds.commons.model.Quote;
import com.integral.ds.commons.util.Pair;
import com.integral.ds.emscope.displayorder.DisplayOrderService;
import com.integral.ds.emscope.displayorder.IDisplayOrderAggregator;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.s3.S3Utils;

/**
 * Default implementation class for {@link DisplayOrderService}
 * 
 * @author chawlas
 *
 */
public class DisplayOrderServiceImpl implements DisplayOrderService {

	private final static Logger log = LogManager
			.getLogger(DisplayOrderServiceImpl.class);

	private final String OA_RATES_LOCATION_IN_S3 = "integral-rates";

	private final SimpleDateFormat inputDateFormat = new SimpleDateFormat(
			"yyyy-MM-dd");


	private final IDisplayOrderAggregator _displayOrderAggregator;


	private final IConverter<String, IDisplayOrderRate> _stringToDisplayOrderRateConverter;


	private final IConverter<IRate, RestRatesObject> _rateToRestRatesObjectConverter;

	public DisplayOrderServiceImpl(
			IDisplayOrderAggregator displayOrderAggregator_,
			IConverter<String, IDisplayOrderRate> stringToDisplayOrderRateConverter_,
			IConverter<IRate, RestRatesObject> rateToRestRatesObjectConverter_) {
		_displayOrderAggregator = displayOrderAggregator_;
		_stringToDisplayOrderRateConverter = stringToDisplayOrderRateConverter_;
		_rateToRestRatesObjectConverter = rateToRestRatesObjectConverter_;
	}

	
	private boolean isLineBeforeOrAtTimestamp(String line, int minute,
			int second, int millisecond) {
		String[] lineArr = line.replaceAll("\\s+", " ").trim().split(" ");
		if (lineArr.length > 12) {
			try {
				String[] timestamp = lineArr[6].split(":");
				int currentMinute = Integer.parseInt(timestamp[1]);
				int currentSecond = Integer.parseInt(timestamp[2]);
				int currentMillis = Integer.parseInt(timestamp[3]);
				if (currentMinute < minute
						|| (currentMinute == minute && currentSecond < second)
						|| (currentMinute == minute && currentSecond == second && currentMillis <= millisecond)) {
					return true;
				} else
					return false;
			} catch (Exception e) {
				return false;
			}
		} else
			return false;
	}

		private Pair<String, String> getBrokerAndCcyPair(String line) {
		String[] lineArr = line.replaceAll("\\s+", " ").trim().split(" ");
		if (lineArr.length >= 12) {
			return new Pair<String, String>(lineArr[9], lineArr[10]
					+ lineArr[11]);
		} else
			return new Pair<String, String>("", "");

	}

	private boolean isStatusActive(String line) {
		return !line.contains("InActive");
	}

	private IDisplayOrderRate lineToRateConverter(String line, String server,
			String adapter) {
		IDisplayOrderRate dor = _stringToDisplayOrderRateConverter
				.convert(line);
		dor.setServerAndAdapter(server, adapter);
		return dor;
	}

	@Override
	public List<RestRatesObject> getPrevailingDisplayOrderRates(
			final String ccyPair_, final Date timestamp_) {

		log.info("DisplayOrderServiceImpl: " + ccyPair_ + " " + timestamp_);

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.setTime(timestamp_);
		String year = String.valueOf(cal.get(Calendar.YEAR));
		int month = cal.get(Calendar.MONTH) + 1;
		String year_month = year + "-" + (month < 10 ? "0" : "") + month;
		List<String> files = S3Utils.getListOfFiles(
				OA_RATES_LOCATION_IN_S3,
				"raw/" + year + "/" + year_month + "/"
						+ inputDateFormat.format(timestamp_) + "/OA-RATES");

		Map<String, String> serverAdapter = new HashMap<String, String>();

		List<Pair<String, String>> filesLocations = new ArrayList<>();

		final int hour = cal.get(Calendar.HOUR_OF_DAY);
		final int minute = cal.get(Calendar.MINUTE);
		final int second = cal.get(Calendar.SECOND);
		final int millisecond = cal.get(Calendar.MILLISECOND);

		for (String fileDetails : files) {
			String[] keyDetails = fileDetails.split("\\/");
			String keyName = keyDetails[keyDetails.length - 1];
			String[] keyNameSplitter = keyName.split("\\.");
			String[] dateHourSplitter = keyNameSplitter[keyNameSplitter.length - 2]
					.split("\\-");

			if (Integer.parseInt(dateHourSplitter[dateHourSplitter.length - 1]) == hour) {

				filesLocations.add(new Pair<String, String>(
						keyDetails[keyDetails.length - 3] + ":"
								+ keyDetails[keyDetails.length - 2],
						fileDetails));

				serverAdapter.put(keyDetails[keyDetails.length - 3],
						keyDetails[keyDetails.length - 2]);
			}

		}

		final Map<String, Quote> allBrokersToPrevailingQuoteMap = new ConcurrentHashMap<>();

		final CountDownLatch latch = new CountDownLatch(filesLocations.size());

		ExecutorService exec = Executors.newFixedThreadPool(filesLocations
				.size() + 1);

		// start the aggregator Thread

		Future<List<IRate>> aggregationOutput = exec
				.submit(new Callable<List<IRate>>() {

					@Override
					public List<IRate> call() {
						List<IRate> ratesToReturn = new ArrayList<>();
						try {
							latch.await();

							ratesToReturn.addAll(_displayOrderAggregator
									.aggregateDisplayOrders(
											allBrokersToPrevailingQuoteMap,
											ccyPair_, timestamp_));

						} catch (InterruptedException e) {
							log.error(e);
						}
						return ratesToReturn;
					}
				});

		for (final Pair<String, String> fileLocation : filesLocations) {

			// start a new thread to read the file
			exec.execute(new Runnable() {

				@Override
				public void run() {

					String[] servAdap = fileLocation.getFirst().split(":");
					final String server = servAdap[0];
					final String adapter = servAdap[1];
					BufferedReader reader = null;
					Optional<File> file = null;
					try {
						Thread.sleep(100);

						Thread.currentThread().setName(fileLocation.getFirst());

						 file = S3Utils.downloadFile(
								OA_RATES_LOCATION_IN_S3,
								fileLocation.getSecond());

						if (file.isPresent()) {
							reader = new BufferedReader(new InputStreamReader(
									new GZIPInputStream(new FileInputStream(
											file.get()))));
						}

					} catch (Exception e) {
						log.error(e);
						latch.countDown();
					}

					String line = "";

					try {
						long currentTmstmp = 0;
						Map<String, Quote> brokerToQuoteMap = new HashMap<String, Quote>();

						boolean hasCalledLatchCountdown = false;

						while (reader != null && (line = reader.readLine()) != null) {
							// if the line is empty or the currency pair
							// doesn't match, don't process the line
							if (StringUtils.isEmpty(line)) {
								continue;
							}

							Pair<String, String> brokerAndCcyPair = getBrokerAndCcyPair(line);
							if (!brokerAndCcyPair.getSecond().equals(ccyPair_)) {
								continue;
							}

							String broker = brokerAndCcyPair.getFirst();

							// Edge case: if the previous second is 59
							// second and previous minute is 59 minute
							// then there will be no entry for last hour
							// data
							if (isLineBeforeOrAtTimestamp(line, minute, second,
									millisecond)) {

								// for Inactive Status, remove the entry
								// from prevailingActiveQuoteForBroker
								if (!isStatusActive(line)) {
									brokerToQuoteMap.remove(broker);
								} else {
									IDisplayOrderRate rate = lineToRateConverter(
											line, server, adapter);

									// For a broker: for an existing quote,
									// add the rate else create a new quote
									// and
									// add the rate
									if (rate.getTmstmp() == currentTmstmp
											&& brokerToQuoteMap
													.containsKey(broker)) {
										brokerToQuoteMap.get(broker).getRates()
												.add(rate);
									} else {
										Quote quote = new Quote(rate
												.getTmstmp(), brokerAndCcyPair);
										quote.getRates().add(rate);
										brokerToQuoteMap.put(broker, quote);
										currentTmstmp = rate.getTmstmp();
									}
								}
							} else {
								if (brokerToQuoteMap != null
										&& !brokerToQuoteMap.isEmpty()) {
									allBrokersToPrevailingQuoteMap
											.putAll(brokerToQuoteMap);
								}
								hasCalledLatchCountdown = true;
								latch.countDown();
								break;
							}
						}
						if (!hasCalledLatchCountdown) {
							hasCalledLatchCountdown = true;
							latch.countDown();
						}
					} catch (Exception ex) {
						latch.countDown();
						log.error(ex);
					}
					finally
					{
						if(reader != null)
							try {
								reader.close();
							} catch (IOException e) {
								log.error(e);
							}
						
						if(file.isPresent())
						{
							log.debug("removing local file "+file.get().getAbsolutePath());
							file.get().delete();
							
						}
					}
				}
			});
		}

		exec.shutdown();
		try {
			exec.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
			log.error(e);
		}

		try {
			log.debug(" return rates :");

			List<IRate> rates = aggregationOutput.get();
			List<RestRatesObject> restRatesObject = new LinkedList<RestRatesObject>();
			for (IRate rate : rates) {
				restRatesObject.add(_rateToRestRatesObjectConverter
						.convert(rate));
			}
			return restRatesObject;
		} catch (InterruptedException | ExecutionException e) {
			log.error(e);
		}

		return Collections.<RestRatesObject> emptyList();
	}

	/*
	 * public static void main(String[] args) { long startTmstmp =
	 * System.currentTimeMillis(); String ccyPair = "EURUSD";
	 * 
	 * DisplayOrderService _displayOrderService = new
	 * DisplayOrderServiceImpl(new DisplayOrderAggregator(), new
	 * StringToDisplayOrderRateConverter());
	 * 
	 * List<IRate> actualList = _displayOrderService
	 * .getPrevailingDisplayOrderRates(ccyPair, getDateFor(2014, 7, 01, 02, 10,
	 * 02, 100));
	 * 
	 * System.out.println(actualList);
	 * 
	 * System.out.println(System.currentTimeMillis() - startTmstmp); }
	 */

	private static Date getDateFor(int year, int month, int day, int hour,
			int minute, int second, int milli) {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, day);
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.SECOND, second);
		cal.set(Calendar.MILLISECOND, milli);
		return cal.getTime();
	}

}

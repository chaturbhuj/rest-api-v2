package com.integral.ds.emscope.benchmark;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.integral.ds.dto.BenchmarkObject;
import com.owlike.genson.Genson;

@Path("/benchmarks1")
@Component
@Scope("request")
public class BenchmarkCollectionResource1 {

    private static final Log LOGGER = LogFactory.getLog(BenchmarkCollectionResource1.class);

	//private long ONE_DAY = 3600 * 24 * 1000l;
	@Autowired
	BenchmarkAppDataContext context = null;
	
	
	@Path("/ccypair/{ccyPair}/fromtime/{fromTime}/totime/{toTime}/tier/{tier}/")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
	public String getBenchmarkRates(
			@PathParam("ccyPair") String ccyPair, 
			@PathParam("fromTime") long fromTime, 
			@PathParam("toTime") long toTime,
			@PathParam("tier") Integer tier)	{
		
		List <BenchmarkObject> benchmarkObjectList = context.getBenchMarkRates(ccyPair, new Date(fromTime), new Date(toTime), tier);
		Collections.sort(benchmarkObjectList);
		Genson genson = new Genson();
		try	{
			return genson.serialize(benchmarkObjectList);
		}
		catch (Exception exception)	{
            LOGGER.error("Exception while getting benchmark rates.",exception);
		}
		return "";
	}
	
	
	
	
	@Path("sample/{count}/ccypair/{ccyPair}/fromtime/{fromTime}/totime/{toTime}/tier/{tier}/")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
	public String getSampleBenchmarkRates(
			@PathParam("ccyPair") String ccyPair, 
			@PathParam("fromTime") long fromTime, 
			@PathParam("toTime") long toTime,
			@PathParam("tier") Integer tier,
			@PathParam("count") Integer count)	{
		List <BenchmarkObject> benchmarkObjectList = context.getBenchMarkSampleRates(ccyPair, new Date(fromTime), new Date(toTime), tier, count);
		Collections.sort(benchmarkObjectList);
		Genson genson = new Genson();
		try	{
			return genson.serialize(benchmarkObjectList);
		}
		catch (Exception exception)	{
            LOGGER.error("Exception while getting benchmark rates.",exception);
		}
		return "";	
	}
}

package com.integral.ds.emscope.trades;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Map;

public class TradeGrouping implements Comparable<TradeGrouping> {
	
	private static final String CONFIRMED_TRADE = "C";
	private static final String REJECTED_TRADE = "R";
	private static final String FAILEd_TRADE = "F";
	private static final String CANCELLED_TRADE = "X";
	private DecimalFormat df = new DecimalFormat("#.#####");
	
	private String providerName;
	
	public String getProviderName() {
		return providerName;
	}


	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	private int confirmedTradeCount;
	private double comfirmedAmount;
	
	private int rejectedTradesCount;
	private double rejectedTradesAmount;
	
	private int failedTradesCount;
	private double failedTradesAmount;
	
	private int cancelledTradeCount;
	private double cancelledTradeAmount;
	
	private double totalAmount;
	
	
	public String getWeightedAvgRate() {
		if (comfirmedAmount == 0)
			return "NA";
		return df.format(totalAmount/comfirmedAmount);
	}
	
	
	public int getConfirmedTradeCount() {
		return confirmedTradeCount;
	}


	public String getComfirmedAmount() {
		return df.format(comfirmedAmount);
	}


	public int getRejectedTradesCount() {
		return rejectedTradesCount;
	}


	public String getRejectedTradesAmount() {
		return df.format(rejectedTradesAmount);
	}


	public int getFailedTradesCount() {
		return failedTradesCount;
	}


	public String getFailedTradesAmount() {
		return df.format(failedTradesAmount);
	}


	public int getCancelledTradeCount() {
		return cancelledTradeCount;
	}


	public String getCancelledTradeAmount() {
		return df.format(cancelledTradeAmount);
	}


	public void processTrade(Map tradeMap){
		String status = (String)tradeMap.get("TRADESTATUS");
		BigDecimal amount = (BigDecimal)tradeMap.get("BASEAMT");
		BigDecimal rate = (BigDecimal) tradeMap.get("RATE");
		if (status.equals("C") )	{
			confirmedTradeCount ++;
			comfirmedAmount= comfirmedAmount + amount.doubleValue();
			
			double value = (amount.doubleValue() * rate.doubleValue());
			totalAmount = totalAmount + value;
			
		}
		else if (status.equals(REJECTED_TRADE))	{
			rejectedTradesCount ++;
			rejectedTradesAmount = rejectedTradesAmount + amount.doubleValue();
		}

		else if (status.equals(FAILEd_TRADE))	{
			failedTradesCount ++;
			failedTradesAmount = failedTradesAmount + amount.doubleValue();
		}
		
		else if (status.equals(CANCELLED_TRADE))	{
			cancelledTradeAmount ++;
			cancelledTradeAmount = cancelledTradeAmount + amount.doubleValue();
		}
	}
	
	@Override
	public int compareTo(TradeGrouping o) {
		return new Double(o.getComfirmedAmount()).compareTo(new Double(this.getComfirmedAmount()));
	}

}

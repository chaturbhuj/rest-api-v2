package com.integral.ds.emscope.displayorder.impl;

import java.math.BigDecimal;

import com.integral.ds.commons.converter.IConverter;
import com.integral.ds.commons.model.IRate;
import com.integral.ds.emscope.rates.RestRatesObject;

public class IRateToRestRatesObjectConverter implements
		IConverter<IRate, RestRatesObject> {

	@Override
	public RestRatesObject convert(IRate from) {

        RestRatesObject ratesObject = new RestRatesObject();
        try {
            ratesObject.setTmstmp(from.getTmstmp());
            ratesObject.setTier(from.getLevel());
            ratesObject.setStatus(from.getStatus());
            ratesObject.setBidPrice(new BigDecimal(from.getBidPrice()));
            ratesObject.setBidSize(new BigDecimal(from.getBidSize()));
            ratesObject.setAskPrice(new BigDecimal(from.getOfferPrice()));
            ratesObject.setAskSize(new BigDecimal(from.getOfferSize()));
            ratesObject.setGuid(from.getGuid());
            ratesObject.setCcyPair(from.getCcyPair());
            ratesObject.setMetaInfo("");
        } catch (Exception e) {
            throw new IllegalArgumentException("Exception while transforming IRate into Rate object.",e);
        }
        return ratesObject;
	}

}

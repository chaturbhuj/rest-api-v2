package com.integral.ds.emscope.displayorder;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.integral.ds.commons.model.IRate;
import com.integral.ds.commons.model.Quote;

public interface IDisplayOrderAggregator {

	List<IRate> aggregateDisplayOrders(Map<String, Quote> displayOrderMap_, String ccyPair_, Date timestamp_);
}

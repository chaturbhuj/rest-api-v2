package com.integral.ds.emscope.benchmark;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.integral.ds.dao.BenchmarkDAO;
import com.integral.ds.dto.BenchmarkObject;

public class BenchmarkAppDataContext {

	private BenchmarkDAO benchmarkDAO;

	public void setBenchmarkDAO (BenchmarkDAO benchmarkDAO)	{
		this.benchmarkDAO = benchmarkDAO;
	}
	
	public List<BenchmarkObject> getBenchMarkRates(String currencyPair, Date fromTime, Date toTime, int tier)	{
    	return benchmarkDAO.getBenchmrarkRates(currencyPair, fromTime, toTime, tier);
    }
    
    public List<BenchmarkObject> getBenchMarkSampleRates(String currencyPair, Date fromTime, Date toTime, int tier, int count)	{
    	List <Date> dateList = getTimeIntervals(fromTime.getTime(), toTime.getTime(), count);
    	return benchmarkDAO.getBenchmrarkSampleRates(currencyPair, fromTime, toTime, tier, dateList);
    }

	private List<Date> getTimeIntervals(long fromTime, long toTime, int count)	{
		List<Date> bigDecimalList = new ArrayList<>();
		long lStartTime = fromTime / 1000;
		long lEndTime = toTime / 1000;
		long timeDifference = lEndTime - lStartTime;
		float interval = timeDifference / (float)count;
		for (int index = 0; index < count; index++)	{
			long offset = new Float((index + 1) * interval).longValue(); 
			long timeIndex = lStartTime + offset;
			Date date = new Date(timeIndex * 1000);
			bigDecimalList.add(date);
		}
		return bigDecimalList;
	}

    public List<BenchmarkObject> getBenchmarkTest(Date from) {
        long longTime = from.getTime();
        longTime -= 16400000;
        Date transformedDate = new Date(longTime);
        return benchmarkDAO.getBenchmarkForDate(transformedDate);
    }
}

package com.integral.ds.emscope.graph.impl;

import com.integral.ds.dto.GraphNode;
import com.integral.ds.dto.OrderNode;
import com.integral.ds.emscope.graph.NavigationService;

import java.util.List;

/**
 * Neo4j implementation of navigation service.
 *
 * @author Rahul Bhattacharjee
 */
public class NavigationServiceNeo4JImpl implements NavigationService<OrderNode> {

    @Override
    public OrderNode getCompleteTree(String orderId) {
        return null;
    }
}

package com.integral.ds.emscope.quote;

import com.integral.ds.dto.ProviderStream;
import com.integral.ds.model.AggregatedBookTuple;
import com.integral.ds.model.QuoteBook;

import java.util.Date;
import java.util.List;

/**
 * Aggregated quote book.
 *
 * @author Rahul Bhattacharjee
 */
public interface AggregatedQuoteService {

    public List<QuoteBook> getAggregatedBook(List<ProviderStream> providerStreams,String ccyPair,
                                                       Date start,Date end,int noOfSamples,int bookDepth);
}

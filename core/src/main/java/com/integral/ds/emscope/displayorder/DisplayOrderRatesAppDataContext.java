package com.integral.ds.emscope.displayorder;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.integral.ds.commons.model.IRate;
import com.integral.ds.dto.BenchmarkObject;
import com.integral.ds.emscope.benchmark.BenchmarkAppDataContext;
import com.integral.ds.emscope.quote.RatesQuoteService;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.model.ErrorObject;
import com.integral.ds.util.DataQueryUtils;

public class DisplayOrderRatesAppDataContext {

	private static final Logger LOGGER = Logger
			.getLogger(DisplayOrderRatesAppDataContext.class);

	private static final String provider = "DO_Provider";
	private static final String stream = "DO_Stream";

	DecimalFormat df = new DecimalFormat("#.##");      
	
	DecimalFormat jpydf = new DecimalFormat("#.####");
	
	private RatesQuoteService _rateQuoteService;

	private BenchmarkAppDataContext _benchmarkAppDataContext;

	private DisplayOrderService _displayOrderService;
	
	public DisplayOrderRatesAppDataContext(RatesQuoteService rateQuoteService_,
			BenchmarkAppDataContext benchmarkAppDataContext_, DisplayOrderService displayOrderService_) {

		_rateQuoteService = rateQuoteService_;
		_benchmarkAppDataContext = benchmarkAppDataContext_;
		_displayOrderService = displayOrderService_;
	}

	public List<RestRatesObject> getDisplayOrderRatesByTimeStampFromS3Files(
			String ccyPair, Date time) {
		List<RestRatesObject> finalRateList = new ArrayList<>();

		LOGGER.info("Received request : " + ccyPair + " : " + time);
		Quote quote = null;
		try {
			quote = _rateQuoteService.getQuoteAtTime(provider, stream, ccyPair,
					time);
		} catch (Exception e) {
			LOGGER.error("Exception while fetching rate files in S3.", e);
			return Collections.<RestRatesObject> emptyList();
			// throw new
			// RestServiceRuntimeException(getErrorObject(RATES_NOT_AVAILABLE_IN_DATASTORE,
			// provider, stream, ccyPair, time, time,
			// ErrorObject.ErrorType.error),"No rates for this period in datastore.");
		}

		if (quote == null || quote.isEmpty()) {
			return Collections.<RestRatesObject> emptyList();
		}

		Map<Integer, List<RestRatesObject>> ratesGroupedByTier = new HashMap<Integer, List<RestRatesObject>>();
		for (RestRatesObject rateItem : quote.getRates()) {
			List<RestRatesObject> rateListForTier = ratesGroupedByTier
					.get(rateItem.getLvl());
			if (rateListForTier == null) {
				rateListForTier = new ArrayList<RestRatesObject>();
				ratesGroupedByTier.put(rateItem.getLvl(), rateListForTier);
			}
			rateListForTier.add(rateItem);
		}

		Set<Integer> tierSrt = new TreeSet<Integer>(ratesGroupedByTier.keySet());

		for (Integer tier : tierSrt) {
			List<RestRatesObject> ratesForTier = ratesGroupedByTier.get(tier);
			Collections.sort(ratesForTier, Collections.reverseOrder());
			for (RestRatesObject rate : ratesForTier) {

				Date timeStamp = new Date(rate.getTmstmp().longValue());
				if (timeStamp.compareTo(time) > 0) {
					finalRateList.add(rate);
				} else {
					rate.setTmstmp(time.getTime());
					finalRateList.add(rate);
					break;
				}
			}

		}
		Collections.sort(finalRateList);
		return finalRateList;
	}

	public List<RestRatesObject> getDisplayOrderAndBenchmarkRatesByTimeStamp(
			String ccyPair, Date time) {
		List<RestRatesObject> displayOrderRates = getDisplayOrderRatesByTimeStampFromS3Files(
				ccyPair, time);

		
		List<BenchmarkObject> benchmarkRates = _benchmarkAppDataContext
				.getBenchMarkRates(ccyPair, time, time, 1);
		if (benchmarkRates.size() == 0) {
			return displayOrderRates;
		}

		BenchmarkObject benchmarkRate = benchmarkRates.get(0);
		double benchmarkPrice = benchmarkRate.getMeanMidPrice().doubleValue();

		LOGGER.log(Level.INFO, "Benchmark Rate is : " + benchmarkPrice);

		RestRatesObject tobDisplayOrderRate = displayOrderRates.get(0);
		double tobAskPrice = tobDisplayOrderRate.getAsk_price() != null ? tobDisplayOrderRate
				.getAsk_price().doubleValue() : 0;
		double tobBidPrice = tobDisplayOrderRate.getBid_price() != null ? tobDisplayOrderRate
				.getBid_price().doubleValue() : 0;

		if (benchmarkPrice > tobBidPrice && benchmarkPrice < tobAskPrice) {
			RestRatesObject benchmarkRestRatesObject = new RestRatesObject();
			benchmarkRestRatesObject
					.setAskPrice(new BigDecimal(benchmarkPrice));
			benchmarkRestRatesObject
					.setBidPrice(new BigDecimal(benchmarkPrice));
			benchmarkRestRatesObject.setGuid("BENCHMARK");
			benchmarkRestRatesObject.setTmstmp(time.getTime());

			double bidDifference = benchmarkPrice - tobBidPrice;
			double offerDifference = tobAskPrice - benchmarkPrice;
			benchmarkRestRatesObject
					.setMetaInfo("BidDifference-"
							+ pipFactor(bidDifference,
									tobDisplayOrderRate.getCcyPair())
							+ ":AskDifference-"
							+ pipFactor(offerDifference,
									tobDisplayOrderRate.getCcyPair()) + ":");
			displayOrderRates.add(0, benchmarkRestRatesObject);

		} else {
			boolean isBenchmarkOnBidSide = benchmarkPrice <= tobBidPrice;

			int benchmarkRestRatePosition = 0;
			double difference = 0.0;

			for (RestRatesObject restRatesObject : displayOrderRates) {
				if (isBenchmarkOnBidSide) {
					double currentBidPrice = restRatesObject.getBid_price() != null ? restRatesObject
							.getBid_price().doubleValue() : 0.0;
					if (benchmarkPrice > currentBidPrice) {
						difference = benchmarkPrice - currentBidPrice;
						break;
					}
				} else {
					double currentAskPrice = restRatesObject.getAsk_price() != null ? restRatesObject
							.getAsk_price().doubleValue() : 0.0;

					if (benchmarkPrice < currentAskPrice) {
						difference =  currentAskPrice - benchmarkPrice;
						break;
					}
				}
				benchmarkRestRatePosition++;
			}

			RestRatesObject benchmarkRestRatesObject = new RestRatesObject();
			if (isBenchmarkOnBidSide) {
				benchmarkRestRatesObject.setBidPrice(new BigDecimal(
						benchmarkPrice));
				benchmarkRestRatesObject.setMetaInfo("BidDifference-"
						+ pipFactor(difference,tobDisplayOrderRate.getCcyPair()) + ":");
			} else {
				benchmarkRestRatesObject.setAskPrice(new BigDecimal(
						benchmarkPrice));
				benchmarkRestRatesObject.setMetaInfo("AskDifference-"
						+ pipFactor(difference,tobDisplayOrderRate.getCcyPair()) + ":");
			}

			benchmarkRestRatesObject.setGuid("BENCHMARK");
			benchmarkRestRatesObject.setTmstmp(time.getTime());
			displayOrderRates.add(benchmarkRestRatePosition,
					benchmarkRestRatesObject);

		}

		return displayOrderRates;

	}

	public List<RestRatesObject> getInteractiveDisplayOrderAndBenchmarkRatesByTimestamp(String ccyPair, Date time)
	{
		List<RestRatesObject> displayOrderRates = _displayOrderService.getPrevailingDisplayOrderRates(ccyPair, time);
		
		
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.setTime(time);
		cal.set(Calendar.MILLISECOND,0);
		
		List<BenchmarkObject> benchmarkRates = _benchmarkAppDataContext
				.getBenchMarkRates(ccyPair, cal.getTime(), cal.getTime(), 1);
		if (benchmarkRates.size() == 0) {
			return displayOrderRates;
		}

		BenchmarkObject benchmarkRate = benchmarkRates.get(0);
		double benchmarkPrice = benchmarkRate.getMeanMidPrice().doubleValue();

		LOGGER.log(Level.INFO, "Benchmark Rate is : " + benchmarkPrice);

		RestRatesObject tobDisplayOrderRate = displayOrderRates.get(0);
		double tobAskPrice = tobDisplayOrderRate.getAsk_price() != null ? tobDisplayOrderRate
				.getAsk_price().doubleValue() : 0;
		double tobBidPrice = tobDisplayOrderRate.getBid_price() != null ? tobDisplayOrderRate
				.getBid_price().doubleValue() : 0;

		if (benchmarkPrice > tobBidPrice && benchmarkPrice < tobAskPrice) {
			RestRatesObject benchmarkRestRatesObject = new RestRatesObject();
			benchmarkRestRatesObject
					.setAskPrice(new BigDecimal(benchmarkPrice));
			benchmarkRestRatesObject
					.setBidPrice(new BigDecimal(benchmarkPrice));
			benchmarkRestRatesObject.setGuid("BENCHMARK");
			benchmarkRestRatesObject.setTmstmp(cal.getTimeInMillis());

			double bidDifference = benchmarkPrice - tobBidPrice;
			double offerDifference = tobAskPrice - benchmarkPrice;
			benchmarkRestRatesObject
					.setMetaInfo("BidDifference-"
							+ pipFactor(bidDifference,
									tobDisplayOrderRate.getCcyPair())
							+ ":AskDifference-"
							+ pipFactor(offerDifference,
									tobDisplayOrderRate.getCcyPair()) + ":");
			displayOrderRates.add(0, benchmarkRestRatesObject);

		} else {
			boolean isBenchmarkOnBidSide = benchmarkPrice <= tobBidPrice;

			int benchmarkRestRatePosition = 0;
			double difference = 0.0;

			for (RestRatesObject restRatesObject : displayOrderRates) {
				if (isBenchmarkOnBidSide) {
					double currentBidPrice = restRatesObject.getBid_price() != null ? restRatesObject
							.getBid_price().doubleValue() : 0.0;
					if (benchmarkPrice > currentBidPrice) {
						difference = benchmarkPrice - currentBidPrice;
						break;
					}
				} else {
					double currentAskPrice = restRatesObject.getAsk_price() != null ? restRatesObject
							.getAsk_price().doubleValue() : 0.0;

					if (benchmarkPrice < currentAskPrice) {
						difference =  currentAskPrice - benchmarkPrice;
						break;
					}
				}
				benchmarkRestRatePosition++;
			}

			RestRatesObject benchmarkRestRatesObject = new RestRatesObject();
			if (isBenchmarkOnBidSide) {
				benchmarkRestRatesObject.setBidPrice(new BigDecimal(
						benchmarkPrice));
				benchmarkRestRatesObject.setMetaInfo("BidDifference-"
						+ pipFactor(difference,tobDisplayOrderRate.getCcyPair()) + ":");
			} else {
				benchmarkRestRatesObject.setAskPrice(new BigDecimal(
						benchmarkPrice));
				benchmarkRestRatesObject.setMetaInfo("AskDifference-"
						+ pipFactor(difference,tobDisplayOrderRate.getCcyPair()) + ":");
			}

			benchmarkRestRatesObject.setGuid("BENCHMARK");
			benchmarkRestRatesObject.setTmstmp(cal.getTimeInMillis());
			displayOrderRates.add(benchmarkRestRatePosition,
					benchmarkRestRatesObject);

		}

		return displayOrderRates;
	}
	private ErrorObject getErrorObject(String message, String provider,
			String stream, String ccyPair, Date startTime, Date endTime,
			ErrorObject.ErrorType type) {
		String date = DataQueryUtils.getNearestAvailableRates(provider, stream,
				ccyPair, startTime);

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(message);
		stringBuilder.append("Stream : " + stream);
		stringBuilder.append(" ,Provider : " + provider + " ,Start time : "
				+ startTime);
		stringBuilder.append(" ,End time : " + endTime + " ,currency pair : "
				+ ccyPair + ".");
		if (date != null) {
			stringBuilder.append("Last available rate at " + date);
		}
		ErrorObject errorObject = new ErrorObject();
		errorObject.setErrorMessage(stringBuilder.toString());
		errorObject.setType(type);
		return errorObject;
	}

	private double pipFactor(double difference, String ccyPair) {
		if(ccyPair.contains("JPY"))
		{
			return Double.valueOf(jpydf.format(difference * 100));
		}
		else
		{
			return Double.valueOf(df.format(difference * 10000));	
		}
		
	}
}

package com.integral.ds.emscope.orders;

/**
 * @author Rahul Bhattacharjee
 */
public enum OrderType {

    A("Active Order"),F("Completely filled order"),P("Partially filled order"),X("Cancelled order"),Z("Expired order"),D("declined order");

    private String message;

    OrderType(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

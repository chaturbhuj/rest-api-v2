package com.integral.ds.emscope.quote.impl;

import com.integral.ds.dto.ProviderStream;
import com.integral.ds.emscope.quote.AggregatedQuoteService;
import com.integral.ds.emscope.quote.impl.task.SampleQuoteFetcherTask;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.model.AggregatedBookTuple;
import com.integral.ds.model.CompleteQuoteSample;
import com.integral.ds.model.QuoteBook;
import com.integral.ds.util.BidOfferCalculator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

/**
 * AggregatedQuoteService to be used when duration is less. < 6m
 *
 * @author Rahul Bhattacharjee
 */
public class AggregatedQuoteServiceImpl implements AggregatedQuoteService {

    protected final Logger LOGGER = Logger.getLogger(AggregatedQuoteServiceImpl.class);

    private BidOfferCalculator CALCULATOR = new BidOfferCalculator();
    private ExecutorService executor;

    public AggregatedQuoteServiceImpl() {
        this(Runtime.getRuntime().availableProcessors()+1);
    }

    public AggregatedQuoteServiceImpl(int noOfThreads) {
        executor = Executors.newFixedThreadPool(noOfThreads, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
        });
    }

    @Override
    public List<QuoteBook> getAggregatedBook(List<ProviderStream> providerStreams, String ccyPair, Date start, Date end, int noOfSamples, int depthOfBook) {
        List<QuoteBook> result = new ArrayList<QuoteBook>();
        List<Future<CompleteQuoteSample>> futures = new ArrayList<Future<CompleteQuoteSample>>();

        for(ProviderStream providerStream : providerStreams) {
            futures.add(executor.submit(new SampleQuoteFetcherTask(providerStream,ccyPair,start,end,noOfSamples)));
        }

        List<CompleteQuoteSample> completeQuoteSamples = getListOfQuoteListForProviderStreams(futures);
        for(int index = 0; index< noOfSamples; index++) {
            List<RatesObject> rates = getRatesForIndex(completeQuoteSamples,index);
            List<AggregatedBookTuple> tuples = CALCULATOR.getAggregatedBook(rates,depthOfBook);
            result.add(new QuoteBook(tuples));
        }
        return result;
    }

    private List<RatesObject> getRatesForIndex(List<CompleteQuoteSample> completeQuoteSamples, int index) {
        List<RatesObject> result = new ArrayList<>();
        for(CompleteQuoteSample completeQuoteSample : completeQuoteSamples) {
            result.addAll(completeQuoteSample.getRatesObjectListForIndex(index));
        }
        return result;
    }

    private List<CompleteQuoteSample> getListOfQuoteListForProviderStreams(List<Future<CompleteQuoteSample>> futures) {
        List<CompleteQuoteSample> result = new ArrayList<CompleteQuoteSample>();

        for(Future<CompleteQuoteSample> future : futures) {
            try {
                CompleteQuoteSample sample = future.get();
                result.add(sample);
            } catch (Exception e) {
                e.printStackTrace();
                LOGGER.error("Exception while getting result from future.",e);
            }
        }
        return result;
    }
}

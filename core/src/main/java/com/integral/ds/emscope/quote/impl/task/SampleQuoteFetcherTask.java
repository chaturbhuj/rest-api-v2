package com.integral.ds.emscope.quote.impl.task;

import com.integral.ds.dto.ProviderStream;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.model.CompleteQuoteSample;
import com.integral.ds.s3.RatesLocator;
import com.integral.ds.util.DataQueryUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Quote fetcher task for asyn fetching of quote samples.
 *
 * @author Rahul Bhattacharjee
 */
public class SampleQuoteFetcherTask implements Callable<CompleteQuoteSample> {

    protected final Logger LOGGER = Logger.getLogger(SampleQuoteFetcherTask.class);

    private static final long PRE_SAMPLE_WINDOW = 2 * 60 * 1000; // 2 min
    private static final long POST_SAMPLE_WINDOW = 2 * 60 * 1000; // 2 min

    private ProviderStream providerStream;
    private Date start;
    private Date end;
    private String ccyPair;
    private int noOfSamples;
    private RatesLocator ratesLocator;

    public SampleQuoteFetcherTask(ProviderStream providerStream ,String ccyPair, Date start,Date end,int noOfSamples) {
        this.providerStream = providerStream;
        this.start = start;
        this.end = end;
        this.ccyPair = ccyPair;
        this.noOfSamples = noOfSamples;
    }

    @Override
    public CompleteQuoteSample call() throws Exception {
        long samplePreTime = start.getTime() - PRE_SAMPLE_WINDOW;
        long samplePostTime = end.getTime() + POST_SAMPLE_WINDOW;
        ratesLocator = new RatesLocator(providerStream.getProvider(),providerStream.getStream(),ccyPair,new Date(samplePreTime),new Date(samplePostTime));

        long noOfMills = (end.getTime() - start.getTime());
        long gapInMills = noOfMills/noOfSamples;
        long startingTime = start.getTime();
        int count = 0;

        List<Quote> quoteList = new ArrayList<Quote>();

        while(count < noOfSamples) {
            Quote quote = getQuote(startingTime);
            quoteList.add(quote);
            startingTime += gapInMills;
            count++;
        }
        CompleteQuoteSample quoteSample = new CompleteQuoteSample(providerStream.getProvider(),providerStream.getStream(),quoteList);
        return quoteSample;
    }

    private Quote getQuote(long sampleTime) {
        Quote quote = new Quote();
        Long prevailingRateTime = ratesLocator.getPrevailingRateTime(new Date(sampleTime));
        if(prevailingRateTime != null) {
            List<RatesObject> rates = ratesLocator.getRate(prevailingRateTime);
            for(RatesObject ratesObject : rates) {
                quote.addRate(DataQueryUtils.getRestRateObject(ratesObject));
            }
        }
        return quote;
    }
}

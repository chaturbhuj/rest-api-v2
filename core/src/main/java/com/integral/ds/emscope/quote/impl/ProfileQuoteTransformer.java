package com.integral.ds.emscope.quote.impl;

import com.integral.ds.emscope.quote.QuoteTransformer;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Runs profile algo. (average of fields) for the given quote.
 *
 * @author Rahul Bhattacharjee
 */
public class ProfileQuoteTransformer implements QuoteTransformer {

    @Override
    public Quote transform(Quote quote) {
        Quote profilesQuote = new Quote();
        profilesQuote.setSampleTime(quote.getSampleTime());

        List<RestRatesObject> rates = quote.getRates();

        Map<Integer,List<RestRatesObject>> ratesMap = getAggregatedTierMap(rates);

        for(Map.Entry<Integer,List<RestRatesObject>> entry : ratesMap.entrySet()) {
            RestRatesObject ratesObject = getRateForMapEntry(entry);
            if(ratesObject != null) {
                profilesQuote.addRate(ratesObject);
            }
        }
        return profilesQuote;
    }

    private Map<Integer,List<RestRatesObject>> getAggregatedTierMap(List<RestRatesObject> rates) {
        Map<Integer,List<RestRatesObject>> ratesMap = new HashMap<Integer,List<RestRatesObject>>();
        for(RestRatesObject rate : rates) {
            int level = rate.getLvl();
            List<RestRatesObject> rateList = ratesMap.get(level);
            if(rateList == null) {
                rateList = new ArrayList<RestRatesObject>();
                ratesMap.put(level,rateList);
            }
            rateList.add(rate);
        }
        return ratesMap;
    }

    protected RestRatesObject getRateForMapEntry(Map.Entry<Integer,List<RestRatesObject>> entry) {
        Integer level = entry.getKey();
        List<RestRatesObject> rateList = entry.getValue();

        int bidCount = 0;
        int askCount = 0;

        String ccyPair = null;
        long timeStamp = 0;

        float askPrice = 0;
        float askVol = 0;
        float bidPrice = 0;
        float bidVolume = 0;

        for(RestRatesObject ratesObject : rateList) {
            if(!ratesObject.getStatus().equalsIgnoreCase("Active")) {
                continue;
            }
            ccyPair = ratesObject.getCcyPair();
            timeStamp = ratesObject.getTmstmp();

            float askPriceToConsider = ratesObject.getAsk_price().floatValue();

            if(askPriceToConsider != 0.0f) {
                askPrice += ratesObject.getAsk_price().floatValue();
                askVol   += ratesObject.getAsk_size().floatValue();
                askCount++;
            }

            float bidPriceToConsider = ratesObject.getBid_price().floatValue();

            if(bidPriceToConsider != 0.0f) {
                bidPrice += ratesObject.getBid_price().floatValue();
                bidVolume+= ratesObject.getBid_size().floatValue();
                bidCount++;
            }
        }

        if(askCount != 0 || bidCount != 0) {
            RestRatesObject profileRate = new RestRatesObject();
            profileRate.setStatus("Active");
            profileRate.setCcyPair(ccyPair);

            if(askCount != 0) {
                profileRate.setAskSize(new BigDecimal(askVol/askCount));
                profileRate.setAskPrice(new BigDecimal(askPrice/askCount));
            } else {
                profileRate.setAskSize(new BigDecimal(0));
                profileRate.setAskPrice(new BigDecimal(0));
            }

            if(bidCount !=0) {
                profileRate.setBidPrice(new BigDecimal(bidPrice/bidCount));
                profileRate.setBidSize(new BigDecimal(bidVolume/bidCount));
            } else {
                profileRate.setBidPrice(new BigDecimal(0));
                profileRate.setBidSize(new BigDecimal(0));
            }

            profileRate.setTier(level);
            profileRate.setTmstmp(timeStamp);
            return profileRate;
        }
        return null;
    }

    private long getRateTimestamp(List<RestRatesObject> rateList) {
        return 0;
    }
}

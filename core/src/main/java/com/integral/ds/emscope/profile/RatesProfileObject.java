package com.integral.ds.emscope.profile;

import java.math.BigDecimal;

public class RatesProfileObject implements Comparable<RatesProfileObject> {
	
	



	private String provider;
	private String stream;
	private String currencyPair;
	private BigDecimal timeStamp;
	private String tier;
	private String hasData;
	private int twoSidedTickCnt;
	private BigDecimal midPriceMean;
	private BigDecimal spreadAverage;
	private BigDecimal lastTickTimestamp;
	private long lastTickAge;
	private BigDecimal bidSizeAvg; 
	private BigDecimal askSizeAvg;  
	private BigDecimal bidPrcMax;
	private BigDecimal bidPrcMin; 
	private BigDecimal askPrcMax; 
	private BigDecimal askPrcMin;
	
	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}


	public String getStream() {
		return stream;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}

	public String getCurrencyPair() {
		return currencyPair;
	}

	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair;
	}

	public BigDecimal getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(BigDecimal timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getTier() {
		return tier;
	}

	public void setTier(String tier) {
		this.tier = tier;
	}

	public String getHasData() {
		return hasData;
	}

	public void setHasData(String hasData) {
		this.hasData = hasData;
	}

	public int getTwoSidedTickCnt() {
		return twoSidedTickCnt;
	}

	public void setTwoSidedTickCnt(int twoSidedTickCnt) {
		this.twoSidedTickCnt = twoSidedTickCnt;
	}

	public BigDecimal getMidPriceMean() {
		return midPriceMean;
	}




	public void setMidPriceMean(BigDecimal midPriceMean) {
		this.midPriceMean = midPriceMean;
	}




	public BigDecimal getSpreadAverage() {
		return spreadAverage;
	}




	public void setSpreadAverage(BigDecimal spreadAverage) {
		this.spreadAverage = spreadAverage;
	}




	public BigDecimal getLastTickTimestamp() {
		return lastTickTimestamp;
	}




	public void setLastTickTimestamp(BigDecimal lastTickTimestamp) {
		this.lastTickTimestamp = lastTickTimestamp;
	}




	public long getLastTickAge() {
		return lastTickAge;
	}




	public void setLastTickAge(long lastTickAge) {
		this.lastTickAge = lastTickAge;
	}




	public BigDecimal getBidSizeAvg() {
		return bidSizeAvg;
	}




	public void setBidSizeAvg(BigDecimal bidSizeAvg) {
		this.bidSizeAvg = bidSizeAvg;
	}




	public BigDecimal getAskSizeAvg() {
		return askSizeAvg;
	}




	public void setAskSizeAvg(BigDecimal askSizeAvg) {
		this.askSizeAvg = askSizeAvg;
	}




	public BigDecimal getBidPrcMax() {
		return bidPrcMax;
	}




	public void setBidPrcMax(BigDecimal bidPrcMax) {
		this.bidPrcMax = bidPrcMax;
	}




	public BigDecimal getBidPrcMin() {
		return bidPrcMin;
	}




	public void setBidPrcMin(BigDecimal bidPrcMin) {
		this.bidPrcMin = bidPrcMin;
	}




	public BigDecimal getAskPrcMax() {
		return askPrcMax;
	}




	public void setAskPrcMax(BigDecimal askPrcMax) {
		this.askPrcMax = askPrcMax;
	}




	public BigDecimal getAskPrcMin() {
		return askPrcMin;
	}




	public void setAskPrcMin(BigDecimal askPrcMin) {
		this.askPrcMin = askPrcMin;
	}


	
	
	
		
	@Override
	public int compareTo(RatesProfileObject o) {
		return this.timeStamp.compareTo(o.timeStamp);
	}
	
}

package com.integral.ds.emscope.graph.impl;

import com.integral.ds.dao.OrdersDAO;
import com.integral.ds.dto.GraphNode;
import com.integral.ds.dto.OrderInfo;
import com.integral.ds.dto.OrderNode;
import com.integral.ds.emscope.graph.NavigationService;

import java.util.List;

/**
 * Postgres implementation of navigation service which would user trade_master and order_master to create
 * the navigational links.
 *
 * @author Rahul Bhattacharjee
 */
public class NavigationServiceUsingOrderTableImpl implements NavigationService<OrderNode> {

    private OrdersDAO ordersDAO;

    @Override
    public OrderNode getCompleteTree(final String orderId) {
        OrderInfo parentOrder = getOriginatingOrder(orderId);
        OrderNode parentNode = createNode(parentOrder);
        setYouAreHereFlag(parentNode,orderId);

        List<OrderInfo> coveredOrders = ordersDAO.getCoveringOrders(parentOrder.getOrderId());
        for(OrderInfo coveredOrder : coveredOrders) {
            OrderNode childNode = getChildNode(coveredOrder,orderId);
            parentNode.addChildOrderList(childNode);
        }
        return parentNode;
    }

    private OrderNode getChildNode(OrderInfo order, String orderId) {
        OrderNode orderNode = createNode(order);
        setYouAreHereFlag(orderNode,orderId);
        List<OrderInfo> coveredOrders = ordersDAO.getCoveringOrders(order.getOrderId());
        for(OrderInfo coveredOrder : coveredOrders) {
            OrderNode childNode = getChildNode(coveredOrder,orderId);
            orderNode.addChildOrderList(childNode);
        }
        return orderNode;
    }

    private OrderNode createNode(OrderInfo parentOrder) {
        OrderNode resultOrder = new OrderNode();
        resultOrder.setOrderId(parentOrder.getOrderId());
        return resultOrder;
    }

    private OrderInfo getOriginatingOrder(String orderId) {
        OrderInfo order = ordersDAO.getOrder(orderId);
        if(order == null) {
            throw new IllegalArgumentException("Order missing in DS order repository.Order id " + orderId);
        }
        OrderInfo parentOrder = ordersDAO.getParentOrder(order.getOrderId());

        while(parentOrder != null) {
            order = parentOrder;
            parentOrder = ordersDAO.getParentOrder(parentOrder.getOrderId());
        }
        return order;
    }

    private void setYouAreHereFlag(OrderNode parentNode, String orderId) {
        String nodeOrderId = parentNode.getOrderId();
        if(nodeOrderId.equalsIgnoreCase(orderId)) {
            parentNode.setYouAreHere(true);
        }
    }

    public void setOrdersDAO(OrdersDAO ordersDAO) {
        this.ordersDAO = ordersDAO;
    }
}

package com.integral.ds.emscope.rates;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.integral.ds.dao.s3.S3FilesRatesDao;
import com.integral.ds.emscope.RestServiceRuntimeException;
import com.integral.ds.model.ErrorObject;
import com.integral.ds.util.DataQueryUtils;

public class RatesAppDataContext {

    private static final Logger LOGGER = Logger.getLogger(RatesAppDataContext.class);

    private static final long LOOK_BACK_PERIOD = 2 * 60 *1000l;
    private static final String RATES_NOT_AVAILABLE = "Tick Data not available for the requested time frame.";
    private static final String RATES_NOT_AVAILABLE_IN_DATASTORE = "Stream Data not available for the time frame in the data store.";

    
    private S3FilesRatesDao s3RatesDao;
	
	public RatesAppDataContext(S3FilesRatesDao s3FilesRatesDao) {
		
        this.s3RatesDao = s3FilesRatesDao;
	}
	
	public List<RatesObject> getRatesByTimeStamp(String provider, String ccyPair, String stream, Date startTime, Date endTime, boolean backFill)	{
		Date lookBackStartDate = new Date(startTime.getTime() - LOOK_BACK_PERIOD);
		List<RatesObject> listOfRates = null;
		List<RatesObject> finalRateList = new ArrayList<>();
		
		//listOfRates = ratesDao.getRates(provider, stream, ccyPair, lookBackStartDate, endTime);

		//First Group Rates by Tier 
		Map<Integer, List<RatesObject>> ratesGroupedByTier = new HashMap<Integer, List<RatesObject>>();
		for (RatesObject rateItem: listOfRates){
			List <RatesObject> rateListForTier = ratesGroupedByTier.get(rateItem.getLvl());
			if (rateListForTier == null)	{
				rateListForTier = new ArrayList<RatesObject>();
				ratesGroupedByTier.put(rateItem.getLvl(), rateListForTier);
			}
			rateListForTier.add(rateItem);
		}
		
		//For Each Tier Find the Grouping.
		
		Set<Integer> tierSrt = ratesGroupedByTier.keySet();
		for (Integer tier: tierSrt){
			List <RatesObject> ratesForTier = ratesGroupedByTier.get(tier);
			Collections.sort(ratesForTier, Collections.reverseOrder());
			boolean startTimePopulated = false;
			for (RatesObject rate: ratesForTier){
				
				Date timeStamp = new Date(rate.getTmstmp().longValue());
				if (timeStamp.compareTo(startTime) > 0){
					finalRateList.add(rate);
				}
				else	{
					rate.setTmstmp(startTime.getTime());
					finalRateList.add(rate);
					startTimePopulated = true;
					break;
				}
			}
		
		}
		
		Collections.sort(finalRateList);
		
		//Merge All the Groups Back.
		
		return finalRateList;
	}

    public List<RestRatesObject> getRatesByTimeStampFromS3Files(String provider, String ccyPair, String stream,
                                                                Date startTime, Date endTime, boolean backFill)	{
        Date lookBackStartDate = new Date(startTime.getTime() - LOOK_BACK_PERIOD);
        List<RestRatesObject> listOfRates = null;
        List<RestRatesObject> finalRateList = new ArrayList<>();

        try {
            listOfRates = s3RatesDao.getRates(provider, stream, ccyPair, lookBackStartDate, endTime);
        } catch (Exception e) {
            LOGGER.error("Exception while fetching rate files in S3.",e);
            throw new RestServiceRuntimeException(getErrorObject(RATES_NOT_AVAILABLE_IN_DATASTORE, provider, stream, ccyPair, startTime, endTime, ErrorObject.ErrorType.error),"No rates for this period in datastore.");
        }

        if(listOfRates.isEmpty()) {
            throw new RestServiceRuntimeException(getErrorObject(RATES_NOT_AVAILABLE ,
                    provider, stream, ccyPair, startTime, endTime, ErrorObject.ErrorType.warning),"No rates for this period.");
        }

        Map<Integer, List<RestRatesObject>> ratesGroupedByTier = new HashMap<Integer, List<RestRatesObject>>();
        for (RestRatesObject rateItem: listOfRates){
            List <RestRatesObject> rateListForTier = ratesGroupedByTier.get(rateItem.getLvl());
            if (rateListForTier == null)	{
                rateListForTier = new ArrayList<RestRatesObject>();
                ratesGroupedByTier.put(rateItem.getLvl(), rateListForTier);
            }
            rateListForTier.add(rateItem);
        }

        Set<Integer> tierSrt = new TreeSet<Integer>(ratesGroupedByTier.keySet());
        
        for (Integer tier: tierSrt){
            List <RestRatesObject> ratesForTier = ratesGroupedByTier.get(tier);
            Collections.sort(ratesForTier, Collections.reverseOrder());
            for (RestRatesObject rate: ratesForTier){

                Date timeStamp = new Date(rate.getTmstmp().longValue());
                if (timeStamp.compareTo(startTime) > 0){
                    finalRateList.add(rate);
                }
                else	{
                    rate.setTmstmp(startTime.getTime());
                    finalRateList.add(rate);
                    break;
                }
            }

        }
        Collections.sort(finalRateList);
        return finalRateList;
    }

    private ErrorObject getErrorObject(String message, String provider, String stream, String ccyPair, Date startTime, Date endTime, ErrorObject.ErrorType type) {
        String date = DataQueryUtils.getNearestAvailableRates(provider,stream,ccyPair,startTime);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(message);
        stringBuilder.append("Stream : " + stream);
        stringBuilder.append(" ,Provider : " + provider + " ,Start time : " + startTime);
        stringBuilder.append(" ,End time : " + endTime + " ,currency pair : " + ccyPair + ".");
        if(date != null) {
            stringBuilder.append("Last available rate at " + date);
        }
        ErrorObject errorObject = new ErrorObject();
        errorObject.setErrorMessage(stringBuilder.toString());
        errorObject.setType(type);
        return errorObject;
    }
}

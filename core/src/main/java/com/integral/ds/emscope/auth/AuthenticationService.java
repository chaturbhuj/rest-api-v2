package com.integral.ds.emscope.auth;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;
import com.integral.ds.dao.AuthDAO;
import com.integral.ds.dto.UserInfo;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Service for auth of EMScope application.
 *
 * @author Rahul Bhattacharjee
 */
public class AuthenticationService {

    private static final Logger LOGGER = Logger.getLogger(AuthenticationService.class);

    private AuthDAO authDAO;

    private LoadingCache<String,UserInfo> USER_IDENTITY_CACHE = null;

    public void init() {
        USER_IDENTITY_CACHE = CacheBuilder.newBuilder()
                .maximumSize(100)
                .build(new UserIdentityFetcher(authDAO));
        USER_IDENTITY_CACHE.asMap().putAll(getInitialEntries());
    }

    private Map<String,UserInfo> getInitialEntries() {
        Map<String,UserInfo> userInfoMap = new HashMap<String,UserInfo>();
        List<UserInfo> userInfos = authDAO.getAllUsers();
        for(UserInfo userInfo : userInfos) {
            userInfoMap.put(userInfo.getId(),userInfo);
        }
        return userInfoMap;
    }

    public boolean isValidLogin(String userName, String password) {
        if(userName == null || password == null) {
            return false;
        }
        try {
            UserInfo userInfo = USER_IDENTITY_CACHE.get(userName);
            return password.equals(userInfo.getPassword());
        } catch (ExecutionException e) {
            LOGGER.error("Exception while loading cache for user " + userName);
        }
        return false;
    }

    public void setAuthDAO(AuthDAO authDAO) {
        this.authDAO = authDAO;
    }
}

package com.integral.ds.emscope.marketsnapshot;

import com.integral.ds.dao.OrdersWithTradesDAO;
import com.integral.ds.emscope.orderswithtrade.OrdersWithTradesAppDataContext;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Market snapshot service for a trade or for all trades for an order.
 *
 * @author Rahul Bhattacharjee
 */
public class MarketSnapshotService {

    private static final Log LOGGER = LogFactory.getLog(MarketSnapshotService.class);

    @Autowired
    private OrdersWithTradesDAO ordersWithTradesDAO;

    @Autowired
    private OrdersWithTradesAppDataContext ordersWithTradesAppDataContext;

    public String getSerializedMarketSnapshotForTradeId(String tradeId) {
        String result = null;
        try {
            result = ordersWithTradesAppDataContext.getMarketSnapShotForTrade(tradeId);
        } catch (Exception e) {
            LOGGER.error("Exception while fetching serialized market snapshot.",e);
            throw new IllegalArgumentException("Exception while fetching market snapshot.",e);
        }
        return result;
    }

    public String getSerializedMarketSnapshotForOrderId(String orderId) {
        List<String> tradeIds = ordersWithTradesDAO.getTradeIdsForOrder(orderId);
        List<String> jsonResponses = new ArrayList<String>();
        for(String tradeId : tradeIds) {
            String serializedMarketSnapshot = getSerializedMarketSnapshotForTradeId(tradeId);
            jsonResponses.add(serializedMarketSnapshot);
        }
        return joinSerializedTrades(jsonResponses);
    }

    private String joinSerializedTrades(List<String> serializedTrades) {
        if(serializedTrades == null || serializedTrades.isEmpty()) {
            return "";
        }
        String json = StringUtils.join(serializedTrades, ",");
        json = "[" + json + "]";
        return json;
    }

}

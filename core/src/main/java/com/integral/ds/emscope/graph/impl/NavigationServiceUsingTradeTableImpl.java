package com.integral.ds.emscope.graph.impl;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.integral.ds.dao.OrdersDAO;
import com.integral.ds.dao.TradeMasterDao;
import com.integral.ds.dto.GraphNode;
import com.integral.ds.dto.OrderInfo;
import com.integral.ds.dto.TradeInfo;
import com.integral.ds.emscope.graph.NavigationService;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Navigation service using trades master table.
 *
 * @author Rahul Bhattacharjee
 */
public class NavigationServiceUsingTradeTableImpl implements NavigationService<GraphNode> {

    private static final Logger LOGGER = Logger.getLogger(NavigationServiceUsingTradeTableImpl.class);

    //TODO : move to application level configuration.
    private static final int CACHE_SIZE = 50;

    private TradeMasterDao tradeMasterDao;
    private OrdersDAO ordersDAO;

    private LoadingCache<String,GraphNode> ORDER_GRAPH_CACHE = null;

    public void initCache() {
        ORDER_GRAPH_CACHE = CacheBuilder.newBuilder().maximumSize(CACHE_SIZE).build(new GraphCacheLoader(this));
    }

    @Override
    public GraphNode getCompleteTree(String orderId) {
        try {
            return ORDER_GRAPH_CACHE.get(orderId);
        } catch (ExecutionException e) {
            LOGGER.error("Exception while creating order graph for order id " + orderId,e);
            throw new IllegalArgumentException("Exception while creating Order Graph for order id " + orderId , e);
        }
    }

    private GraphNode constructGraphForOrder(String orderId) {
        OrderInfo topOrder = getOriginatingOrder(orderId);
        GraphNode result = createGraphNode(topOrder.getOrderId(), GraphNode.GraphNodeType.ORDER);
        setYouAreHereFlag(result,orderId);

        List<TradeInfo> tradeInfos = tradeMasterDao.getTradesForOrder(topOrder.getOrderId());
        for(TradeInfo tradeInfo : tradeInfos) {
            result.addChildGraphNode(getChildNode(tradeInfo,orderId));
        }
        return result;
    }

    private GraphNode getChildNode(TradeInfo childTradeInfo, String orderId) {
        List<TradeInfo> childTrades = tradeMasterDao.getCoveringTrades(childTradeInfo.getTradeId());

        if(childTrades.isEmpty()) {
            GraphNode result = createGraphNode(childTradeInfo.getMakerOrg(), GraphNode.GraphNodeType.LP);
            setYouAreHereFlag(result,orderId);
            return result;
        }
        GraphNode result = createGraphNode(childTradeInfo.getOrderId(), GraphNode.GraphNodeType.ORDER);
        setYouAreHereFlag(result,orderId);

        for(TradeInfo child : childTrades) {
            result.addChildGraphNode(getChildNode(child, orderId));
        }
        return result;
    }

    private void setYouAreHereFlag(GraphNode result, String orderId) {
        if(orderId.equalsIgnoreCase(result.getId())) {
            result.setYouAreHere(true);
        }
    }

    private GraphNode createGraphNode(String id , GraphNode.GraphNodeType type) {
        GraphNode node = new GraphNode();
        node.setId(id);
        node.setType(type);
        return node;
    }

    private OrderInfo getOriginatingOrder(String orderId) {
        OrderInfo order = ordersDAO.getOrder(orderId);
        if(order == null) {
            throw new IllegalArgumentException("Order missing in DS order repository.Order id " + orderId);
        }
        OrderInfo parentOrder = ordersDAO.getParentOrder(order.getOrderId());

        while(parentOrder != null) {
            order = parentOrder;
            parentOrder = ordersDAO.getParentOrder(parentOrder.getOrderId());
        }
        return order;
    }

    /**
     * Inner class for loading cache element during a cache miss.
     */
    private static class GraphCacheLoader extends CacheLoader<String,GraphNode> {
        private NavigationServiceUsingTradeTableImpl parent;
        GraphCacheLoader(NavigationServiceUsingTradeTableImpl parent){
            this.parent = parent;
        }
        @Override
        public GraphNode load(String orderId) throws Exception {
            return parent.constructGraphForOrder(orderId);
        }
    }

    public void setTradeMasterDao(TradeMasterDao tradeMasterDao) {
        this.tradeMasterDao = tradeMasterDao;
    }

    public void setOrdersDAO(OrdersDAO ordersDAO) {
        this.ordersDAO = ordersDAO;
    }
}

package com.integral.ds.emscope.restaudit;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.integral.ds.dao.RestCallAuditDAO;
import com.integral.ds.dto.RestCallAudit;

public class RestAuditLogger {
	
	private ExecutorService threadLogger = null;
	private RestCallAuditDAO restCallAuditDAO = null;
	
	public RestAuditLogger (RestCallAuditDAO restCallAuditDAO)	{
		threadLogger = Executors.newFixedThreadPool(1);
		this.restCallAuditDAO = restCallAuditDAO;
	}
	
	public void logRecord (final RestCallAudit restCallAudit)	{
		
		Runnable r = new Runnable() {
			@Override
			public void run() {
				restCallAuditDAO.insertRestCall(restCallAudit);
				
			}
		};
		threadLogger.submit(r);
	}
	
}


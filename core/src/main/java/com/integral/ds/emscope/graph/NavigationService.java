package com.integral.ds.emscope.graph;

import com.integral.ds.dto.GraphNode;
import com.integral.ds.dto.OrderNode;

import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public interface NavigationService<T> {

    /**
     * Lists the nodes for representing the complete order navigation structure.
     * @param orderId
     * @return
     */
    T getCompleteTree(String orderId);
}

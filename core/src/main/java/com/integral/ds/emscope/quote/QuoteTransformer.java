package com.integral.ds.emscope.quote;

import com.integral.ds.emscope.rates.Quote;

/**
 * Transforms a quote to another based on certain criteria.
 *
 * @author Rahul Bhattacharjee
 */
public interface QuoteTransformer {
    Quote transform(Quote quote);
}

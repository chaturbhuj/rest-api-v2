package com.integral.ds.emscope.auth;

public class AuthAppDataContext {

    private AuthenticationService authenticationService;

	public boolean isLoginValid (String userName, String password)	{
        return authenticationService.isValidLogin(userName,password);
	}

    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }
}

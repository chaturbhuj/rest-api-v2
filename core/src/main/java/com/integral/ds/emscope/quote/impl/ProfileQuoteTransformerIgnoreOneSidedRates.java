package com.integral.ds.emscope.quote.impl;

import com.integral.ds.emscope.quote.QuoteTransformer;
import com.integral.ds.emscope.rates.RestRatesObject;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Profile quote transformer , ignore rates which are one sided.
 *
 * @author Rahul Bhattacharjee
 */
public class ProfileQuoteTransformerIgnoreOneSidedRates extends ProfileQuoteTransformer implements QuoteTransformer {

    @Override
    protected RestRatesObject getRateForMapEntry(Map.Entry<Integer, List<RestRatesObject>> entry) {
        Integer level = entry.getKey();
        List<RestRatesObject> rateList = entry.getValue();

        int count = 0;
        String ccyPair = null;
        long timeStamp = 0;

        float askPrice = 0;
        float askVol = 0;
        float bidPrice = 0;
        float bidVolume = 0;

        for(RestRatesObject ratesObject : rateList) {
            if(isIgnorable(ratesObject)) {
                continue;
            }
            ccyPair = ratesObject.getCcyPair();
            timeStamp = ratesObject.getTmstmp();

            float askPriceToConsider = ratesObject.getAsk_price().floatValue();
            float askVolToConsider = ratesObject.getAsk_size().floatValue();
            float bidPriceToConsider = ratesObject.getBid_price().floatValue();
            float bidVolToConsider = ratesObject.getBid_size().floatValue();

            askPrice += askPriceToConsider;
            askVol += askVolToConsider;

            bidPrice += bidPriceToConsider;
            bidVolume += bidVolToConsider;
            count++;
        }

        if(count != 0) {
            RestRatesObject profileRate = new RestRatesObject();
            profileRate.setStatus("Active");
            profileRate.setCcyPair(ccyPair);
            profileRate.setAskSize(new BigDecimal(askVol/count));
            profileRate.setAskPrice(new BigDecimal(askPrice/count));
            profileRate.setBidPrice(new BigDecimal(bidPrice/count));
            profileRate.setBidSize(new BigDecimal(bidVolume/count));
            profileRate.setTier(level);
            profileRate.setTmstmp(timeStamp);
            return profileRate;
        }
        return null;
    }

    /**
     * Ignore rate if this is one sided or is inactive rate.Process all the remaining rates.
     *
     * @param ratesObject
     * @return
     */
    private boolean isIgnorable(RestRatesObject ratesObject) {
        boolean active = ratesObject.getStatus().equalsIgnoreCase("Active");
        if(!active) {
            return true;
        }
        float askPriceToConsider = ratesObject.getAsk_price().floatValue();
        float bidPriceToConsider = ratesObject.getBid_price().floatValue();

        if(askPriceToConsider == 0.0f || bidPriceToConsider == 0.0f) {
            return true;
        }
        return false;
    }
}

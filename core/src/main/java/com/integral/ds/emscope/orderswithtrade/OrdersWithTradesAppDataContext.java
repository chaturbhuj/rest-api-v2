package com.integral.ds.emscope.orderswithtrade;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.owlike.genson.Genson;
import com.owlike.genson.TransformationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.integral.ds.dao.OrdersWithTradesDAO;
import com.integral.ds.dto.EMScopeHolder;
import com.integral.ds.dto.Order;
import com.integral.ds.dto.OrderLandscapeDetails;
import com.integral.ds.dto.OrderSummary;
import com.integral.ds.dto.Trade;
import com.integral.ds.dto.TradeSummary;
import com.integral.ds.emscope.orders.NarrativeGenerator;
import com.integral.ds.emscope.trades.TradeGrouping;
import com.integral.ds.model.data.DataQuery;
import com.integral.ds.representation.RepresentationModel;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;


public class OrdersWithTradesAppDataContext
{

    private static final Log log = LogFactory.getLog( OrdersWithTradesAppDataContext.class );

    private OrdersWithTradesDAO ordersWithTradesDAO;

    public static final long DAY_1 = 3600l * 24 * 1000;
    final static int STATUS_COUNTER = 0;
    final static int DEALTAMOUNT_COUNTER = 1;
    final static int NAME_COUNTER = 2;
    final static int TIER_COUNTER = 3;
    final static int RATE_COUNTER = 4;
    final static int SPOTRATE_COUNTER = 5;
    final static int FWDPOINTS_COUNTER = 6;
    final static int BIDOFFERMODE_COUNTER = 7;
    final static int VALUEDATE_COUNTER = 8;
    final static int PROVIDER_COUNTER = 9;
    final static int TIMESTAMP_COUNTER = 10;
    final static int SORTORDER_COUNTER = 11;
    final static char TILDA = '~';
    final static char PIPE = '|';
    final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat( "yyyy-MM-dd" );
    final static SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.SSS" );

    final static int SORT_ORDER_POS = 0;
    final static int EVT_CLSF_POS = 1;
    final static int EVT_TIME_POS = 2;
    final static int HOST_NAME_POS = 3;
    final static int QUOTE_GUID_POS = 4;

    public void setOrdersWithTradesDAO( OrdersWithTradesDAO ordersWithTradesDAO )
    {
        this.ordersWithTradesDAO = ordersWithTradesDAO;
    }

    @SuppressWarnings( {"rawtypes", "unchecked"} )
    public Map getOrderDetailsForOrderID( String userGroup, String orderID )
    {
        Map returnMap = new HashMap<>();
        Order order = ordersWithTradesDAO.getOrderByOrderID( orderID );
        if ( order == null )
        {
            return returnMap;
        }
        List<Trade> tradeList = ordersWithTradesDAO.getTradesByOrderIDAndMonth( orderID, new Date( order.getCreated() ) );
        DataQuery orderFormatter = new DataQuery();
        DataQuery tradeFormatter = new DataQuery();
        List<Order> orderList = new ArrayList<>();
        orderList.add( order );
        Collection formattedOrders = orderFormatter.apply( orderList, RepresentationModel.COMPLETE, false );
        Collection formattedTrades = tradeFormatter.apply( tradeList, RepresentationModel.SUMMARY, false );
        NarrativeGenerator narrativeGenerator = new NarrativeGenerator();
        String narrative = narrativeGenerator.getNarrative( order );
        String rateEvents =  getTradeEventsForOrder( orderID );
        Map<String, TradeGrouping> executionSummary = generateExecutionSummary( formattedTrades );
        returnMap.put( "ORDERDETAILS", formattedOrders );
        returnMap.put( "TRADEDETAILS", formattedTrades );
        returnMap.put( "ORDERNARRATIVE", narrative );
        returnMap.put( "EXECUTIONSUMMARY", executionSummary );
        returnMap.put( "TRADERATEEVENTS",rateEvents) ;
        return returnMap;
    }

    public String getExtendedOrderDetailsForOrderID( String userGroup, String orderID )
    {
        return ordersWithTradesDAO.getExtendedOrderDetailsForOrderID( orderID );
    }

    public OrderLandscapeDetails getOrderSummaryWithTrades( String userGroup, String orgName, String ccyPair, String orderType, long minAmount, Date fromTime, Date toTime )
    {
        String newccyPair = ccyPair.substring( 0, 3 ) + "/" + ccyPair.substring( 3 );
        List<OrderSummary> orderSummList = ordersWithTradesDAO.getOrderSummaryForOrg( userGroup, orgName, newccyPair, orderType, minAmount, fromTime, toTime );
        List<TradeSummary> tradeSummaryList = ordersWithTradesDAO.getTradesSummaryForOrg( userGroup, orgName, newccyPair, orderType, minAmount, fromTime, toTime );

        Set<String> orderIDs = new HashSet<>();

        for ( OrderSummary orderSummary : orderSummList )
        {
            orderIDs.add( orderSummary.getOrderID() );
        }

        Iterator<TradeSummary> tradeIterator = tradeSummaryList.iterator();
        while ( tradeIterator.hasNext() )
        {
            if ( !( orderIDs.contains( tradeIterator.next().getORDERID() ) ) )
            {
                tradeIterator.remove();
            }
        }

        OrderLandscapeDetails orderLandscapeDetails = new OrderLandscapeDetails();
        orderLandscapeDetails.setOrderSummary( orderSummList );
        orderLandscapeDetails.setTradeSummary( tradeSummaryList );
        return orderLandscapeDetails;
    }

    public EMScopeHolder getOrderAndTradeDetailsForOrg( String userGroup, String orgName, String ccyPair, String orderType, long minAmount, Date fromTime, Date toTime )
    {
        String newccyPair = ccyPair.substring( 0, 3 ) + "/" + ccyPair.substring( 3 );
        return ordersWithTradesDAO.getOrderAndTradeDetailsForOrg( userGroup, orgName, newccyPair, orderType, minAmount, fromTime, toTime );
    }

    public OrderLandscapeDetails getTradesSummaryWithOrders( String orgName, String ccyPair, String orderType, long minAmount, Date fromTime, Date toTime )
    {
        String newccyPair = ccyPair.substring( 0, 3 ) + "/" + ccyPair.substring( 3 );

        List<TradeSummary> tradeSummaryList = ordersWithTradesDAO.getTradesInRange( orgName, newccyPair, orderType, minAmount, fromTime, toTime );
        List<String> orderIds = getOrderIdList( tradeSummaryList );

        log.info( "Order ids from trades " + orderIds );
        List<OrderSummary> orderSummList = ordersWithTradesDAO.getOrdersForTrades( "", orderIds );
        log.info( "Size of orders summary list " + orderSummList.size() );

        OrderLandscapeDetails orderLandscapeDetails = new OrderLandscapeDetails();
        orderLandscapeDetails.setOrderSummary( orderSummList );
        orderLandscapeDetails.setTradeSummary( tradeSummaryList );
        return orderLandscapeDetails;
    }

    public EMScopeHolder getTradesAndOrdersDetailsForOrg( String userGroup, String orgName, String ccyPair, String orderType, long minAmount, Date fromTime, Date toTime )
    {
        String newccyPair = ccyPair.substring( 0, 3 ) + "/" + ccyPair.substring( 3 );
        return ordersWithTradesDAO.getTradeAndOrdersDetailsForOrg( userGroup, orgName, newccyPair, orderType, minAmount, fromTime, toTime );
    }

    @SuppressWarnings( {"rawtypes", "unchecked"} )
    private Map<String, TradeGrouping> generateExecutionSummary( Collection formattedTrades )
    {

        List<Map> formattedTradeList = ( List<Map> ) formattedTrades;
        Map<String, TradeGrouping> amountByCptyMap = new HashMap<>();

        for ( Map map : formattedTradeList )
        {
            String org = ( String ) map.get( "CPTY" );
            TradeGrouping cptySumary = amountByCptyMap.get( org );
            if ( cptySumary == null )
            {
                cptySumary = new TradeGrouping();
                cptySumary.setProviderName( org );
                amountByCptyMap.put( org, cptySumary );
            }
            cptySumary.processTrade( map );
        }
        return amountByCptyMap;
    }

    private List<String> getOrderIdList( List<TradeSummary> tradeSummaryList )
    {
        List<String> orderIds = new ArrayList<String>();
        for ( TradeSummary tradeSummary : tradeSummaryList )
        {
            orderIds.add( tradeSummary.getORDERID() );
        }
        return orderIds;
    }


    public Map<String, List<Map<String, Object>>> getCoveredTrades( String tradeID )
    {
        Map<String, List<Map<String, Object>>> returnMap = new HashMap<>();
        List<Trade> coveredTrades = ordersWithTradesDAO.getCoveredTrades( tradeID );
        List<Trade> coveringTrades = ordersWithTradesDAO.getCoveringTrades( tradeID );
        DataQuery dq = new DataQuery();
        returnMap.put( "COVEREDTRADES", ( List<Map<String, Object>> ) dq.apply( coveredTrades, RepresentationModel.SUMMARY, false ) );
        returnMap.put( "COVERINGTRADES", ( List<Map<String, Object>> ) dq.apply( coveringTrades, RepresentationModel.SUMMARY, false ) );
        return returnMap;
    }

    public String getCoveredOrders( String orderID )
    {
        return ordersWithTradesDAO.getCoveredOrders( orderID );
    }

    public String getOriginatingOrder( String orderID )
    {
        return ordersWithTradesDAO.getOriginatingOrder( orderID );
    }

    public String getCoveringOrders( String orderID )
    {
        return ordersWithTradesDAO.getCoveringOrders( orderID );
    }

    public String getMarketSnapShotForTrade(String tradeID)
    {
        // get the market snapshot for the trade

        String rawMktData = ordersWithTradesDAO.getMarketSnapShot( tradeID ) ;

        String retString = " ";
        if (rawMktData != null && rawMktData.trim().length() >  0 )
        {
            // parse the data only if it is not empty

            JsonObjectBuilder objectBuilder = Json.createObjectBuilder()
                    .add( "TradeID", tradeID );
            populateMktSnapShot(rawMktData,objectBuilder);
            StringWriter stWriter = new StringWriter();
            JsonWriter jsonWriter = Json.createWriter(stWriter);
            jsonWriter.writeObject(objectBuilder.build());
            jsonWriter.close();
            retString= stWriter.toString();
            log.info(" Market snapshot for Trade ID : "+retString);
        }
        return retString;
    }


    public String getRateEventsForTrade(String tradeID)
    {
        // get the market snapshot for the trade

        String rawEventData = ordersWithTradesDAO.getRateEvents( tradeID ) ;

        String retString = " ";
        if (rawEventData != null && rawEventData.trim().length() >  0 )
        {
            // parse the data only if it is not empty

            JsonObjectBuilder objectBuilder = Json.createObjectBuilder()
                    .add( "TradeID", tradeID );
            createGeneratedEvents(rawEventData,objectBuilder);
            StringWriter stWriter = new StringWriter();
            JsonWriter jsonWriter = Json.createWriter(stWriter);
            jsonWriter.writeObject(objectBuilder.build());
            jsonWriter.close();
            retString= stWriter.toString();
            log.info(" Rate Events for Trade ID : "+retString);
        }
        return retString;
    }


    private   void populateMktSnapShot( String record, JsonObjectBuilder objectBuilder )
    {
        StringBuilder builder = new StringBuilder();
        int counter = STATUS_COUNTER;

        char[] chars = record.toCharArray();
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        JsonObjectBuilder snapBuilder = Json.createObjectBuilder();
        for ( char ch : chars )
        {
            switch ( ch )
            {
                case TILDA:
                    String str = builder.toString();
                    switch ( counter )
                    {
                        case STATUS_COUNTER:
                            char [] status =  {builder.charAt( 0 )};
                            snapBuilder.add( "status", new String(status) );
                            break;
                        case DEALTAMOUNT_COUNTER:
                            snapBuilder.add( "dealtAmount", Double.parseDouble( str ) );
                            break;
                        case NAME_COUNTER:
                            snapBuilder.add( "name", str );
                            break;
                        case TIER_COUNTER:
                            snapBuilder.add( "tier", Integer.parseInt( str ) );
                            break;
                        case RATE_COUNTER:
                            snapBuilder.add( "rate", Double.parseDouble( str ) );
                            break;
                        case SPOTRATE_COUNTER:
                            snapBuilder.add( "spotRate", Double.parseDouble( str ) );
                            break;
                        case FWDPOINTS_COUNTER:
                            snapBuilder.add( "fwdPoints", Double.parseDouble( str ) );
                            break;
                        case BIDOFFERMODE_COUNTER:
                            snapBuilder.add( "bidOfferMode", Integer.parseInt( str ) == 0 ? "BID" : "OFFER" );
                            break;
                        case VALUEDATE_COUNTER:
                            snapBuilder.add( "valueDate", DATE_FORMAT.format( new java.sql.Date( Long.parseLong( str ) ) ) );
                            break;
                        case PROVIDER_COUNTER:
                            snapBuilder.add( "provider", str );
                            break;
                        case TIMESTAMP_COUNTER:
                            if ( !"-1".equals( str ) )
                            {
                                snapBuilder.add( "timestamp", TIMESTAMP_FORMAT.format( new Timestamp( Long.parseLong( str ) ) ) );
                            }
                            break;
                        case SORTORDER_COUNTER:
                            snapBuilder.add( "sortOrder", Integer.parseInt( str ) );
                            break;
                    }
                    counter++;
                    builder = new StringBuilder();
                    break;
                case PIPE:
                    counter = STATUS_COUNTER;
                    arrayBuilder.add( snapBuilder );
                    snapBuilder = Json.createObjectBuilder();
                    break;
                default:
                    builder.append( ch );
            }
        }
        objectBuilder.add( "MarketSnapShot", arrayBuilder );
    }

    private  void populateMktSnapShotForOrder( String record, String date, JsonObjectBuilder objectBuilder )
    {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        JsonObjectBuilder snapBuilder = Json.createObjectBuilder();
        //14:24:59:910;JPM1;2000000.0;1;1.36444;1.36444;1;500000.0;DB3;14:24:59:926|14:24:59:040;MSFX;500000.0;2;1.36443;1.36445;2;1500000.0;DB3;14:24:59:926|
        // Timestamp					Provider	Bid Size		Tier	Bid Rate	Offer Rate	Tier	Offer Size	Provider	Timestamp
        // 2014/07/09 20:49:32 710 GMT	SUCD		1,000,000.00	1	7.8361	7.8389	1	996,000.00	SUCD	2014/07/09 20:49:32 710 GMT
        String [] rows = record.split("\\|");

        for (String row:rows)
        {

            String [] cols = row.split( ";" );
            if (cols.length >=5 )
            {
                snapBuilder.add( "BidTimestamp", cols[0].trim().length() > 0 ?date+" " +cols[0]:"" );
                snapBuilder.add( "BidProvider", cols[1] );
                snapBuilder.add( "BidSize", cols[2] );
                snapBuilder.add( "BidTier", cols[3] );
                snapBuilder.add( "BidRate", cols[4] );
                if ((cols.length >=6 )  && (cols.length <8))
                {
                    continue;
                }
                if ( cols.length >= 10 )
                {
                    snapBuilder.add( "OfferRate", cols[5] );
                    snapBuilder.add( "OfferTier", cols[6] );
                    snapBuilder.add( "OfferSize", cols[7] );
                    snapBuilder.add( "OfferProvider", cols[8] );
                    snapBuilder.add( "OfferTimestamp", date+" " +cols[9] );
                }
                else
                {
                    snapBuilder.add( "OfferRate", "" );
                    snapBuilder.add( "OfferTier", "" );
                    snapBuilder.add( "OfferSize", "" );
                    snapBuilder.add( "OfferProvider", "" );
                    snapBuilder.add( "OfferTimestamp", "" );
                }

                arrayBuilder.add( snapBuilder );
                snapBuilder = Json.createObjectBuilder();
            }
        }
        objectBuilder.add( "MarketSnapShot", arrayBuilder );
    }



    protected  void createGeneratedEvents(String eventString,JsonObjectBuilder objectBuilder )
    {
        StringBuilder temp = new StringBuilder(140);
        //StringBuilder record = new StringBuilder(256);
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        JsonObjectBuilder snapBuilder = Json.createObjectBuilder();

        int position = SORT_ORDER_POS;
        for ( int i = 0 ; i < eventString.length() ; i++ )
        {
            char ch = eventString.charAt(i);
            switch ( ch )
            {
                case TILDA :
                    String str = temp.toString();
                    switch ( position )
                    {
                        case SORT_ORDER_POS :
                            snapBuilder.add("sortOrder",str);

                            break;
                        case EVT_CLSF_POS :
                            snapBuilder.add("EventClassification",str);
                            break;
                        case EVT_TIME_POS :
                            snapBuilder.add("EventTime",TIMESTAMP_FORMAT.format( new Timestamp( Long.parseLong( str ) ) ));
                            break;
                        case HOST_NAME_POS :
                            snapBuilder.add("hostname",str);
                            break;
                        case QUOTE_GUID_POS :
                            snapBuilder.add("QuoteGuid",str);
                            break;
                    }
                    position++;
                    temp = new StringBuilder(40);
                    break;
                case PIPE :
                    arrayBuilder.add( snapBuilder );
                    snapBuilder = Json.createObjectBuilder();

                    position = SORT_ORDER_POS;
                    break;
                default :
                    temp.append(ch);
            }
        }
        objectBuilder.add( "RateEvents", arrayBuilder );

    }

    public String getMarketSnapShotForOrder( String orderID )
    {
        // get the market snapshot for the trade

        String rawMktDataWithDate = ordersWithTradesDAO.getMarketSnapShotForOrder( orderID ) ;

        String retString = " ";
        if (rawMktDataWithDate != null && rawMktDataWithDate.trim().length() >  0 )
        {
            // parse the data only if it is not empty

            JsonObjectBuilder objectBuilder = Json.createObjectBuilder()
                    .add( "orderID", orderID );
            String [] data = rawMktDataWithDate.split( "~" );
            String rawMktData=data[1];
            if (rawMktData.equals( "null" ))
            {
                return retString;
            }
            populateMktSnapShotForOrder(rawMktData,data[0],objectBuilder);
            StringWriter stWriter = new StringWriter();
            JsonWriter jsonWriter = Json.createWriter(stWriter);
            jsonWriter.writeObject(objectBuilder.build());
            jsonWriter.close();
            retString= stWriter.toString();
            log.info(" Market snapshot for orderID : "+retString);
        }
        return retString;
    }

    public String generateOrderCollectionQuery(String takerorg, String ccypair,long from, long to,String orderType,
                                String status, String channel, String tif, String origorg , String buysell,int offset)
    {
        String subChannel = channel.equals( "none" ) ? channel : channel.replace( '-','/' ) ;

        String[] ccyList = ccypair.split( "," );
        String finalCcys="";
        if(!ccyList[0].equals( "none" ))
        {
            for ( int i = 0; i < ccyList.length; i++ )
            {
                finalCcys = finalCcys + ccyList[i].substring( 0, 3 ) + "/" + ccyList[i].substring( 3 );
                if ( i != ( ccyList.length - 1 ) )
                {
                    finalCcys = finalCcys + ",";
                }
            }
        }
        else
        {
            finalCcys="none";
        }

        String[] subchannelList = subChannel.split( "," );
        String finalchannel="";
        if(!subchannelList[0].equals( "none" ))
        {
            for ( int i = 0; i < subchannelList.length; i++ )
            {
                finalchannel = finalchannel + subchannelList[i].replace( '-','/' );
                if ( i != ( subchannelList.length - 1 ) )
                {
                    finalchannel = finalchannel + ",";
                }
            }
        }
        else
        {
            finalchannel="none";
        }
       // String newccyPair = ccypair.equals("none")? ccypair:ccypair.substring( 0, 3 ) + "/" + ccypair.substring( 3 );
        Date fromDate = new Date(from);
        Date toDate = new Date(to);
        String orderQuery = "select array_to_json(array_agg(row_to_json(t)))"  +
                "                      from (SELECT CREATED,ORDERID, ORDERSTATE, ORG, CCYPAIR, BUYSELL, ORDERAMTUSD, ORDERAMT, FILLEDAMT, ORDERTYPE, TIMEINFORCE, CHANNEL" +
                " FROM ORDER_MASTER " +
                " WHERE CREATED >= '"+fromDate+  "' AND CREATED <=  '"+toDate +"' AND "+
                (!takerorg.equals( "none" ) ? "ORG IN ('" +takerorg+"')":"ORG = ORG " )   + " AND  "   +
                (!finalCcys.equals( "none" ) ? " CCYPAIR IN ('" +finalCcys +"')":"CCYPAIR=CCYPAIR") + " AND " +
                (!orderType.equals( "none" ) ? "ORDERTYPE IN ('"+orderType+"')":"ORDERTYPE=ORDERTYPE") +" AND " +
                (!status.equals( "none" ) ? "STATUS IN ('" +status +"')":"STATUS = STATUS") + "  AND  "   +
                (!subChannel.equals( "none" )? "CHANNEL IN ('" + finalchannel + "')":"CHANNEL = CHANNEL") + " AND " +
                (!tif.equals( "none" ) ? "TIMEINFORCE IN ('" +tif + "')": "TIMEINFORCE = TIMEINFORCE") + " AND " +
                (!origorg.equals("none") ?  "ORIGINATINGORG IN ('"+origorg +"')" :  "ORIGINATINGORG = ORIGINATINGORG")  + " AND  " +
                (!buysell.equals( "none" ) ? "BUYSELL IN ('"+buysell+"')" : "BUYSELL = BUYSELL ") +" ORDER BY CREATED " + "OFFSET " +offset+" LIMIT 10)t";

        String jsonData = ordersWithTradesDAO.getOrderCollection( orderQuery );

        return jsonData;
    }

    public String generateOrderCollectionQuery( String takerorg, String ccypair, String timeRange, String orderType,
                                                String status, String channel, String tif, String origorg, String buysell, int offset, String orderby, String groupby,
                                                boolean fireRowCount )
    {
        // split the time range into respective time zones.



        String subChannel = channel.equals( "none" ) ? channel : channel.replace( '-','/' ) ;

        String[] ccyList = ccypair.split( "," );
        String finalCcys="";
        if(!ccyList[0].equals( "none" ))
        {
            for ( int i = 0; i < ccyList.length; i++ )
            {
                finalCcys = finalCcys +"'"+ ccyList[i].substring( 0, 3 ) + "/" + ccyList[i].substring( 3 )+"'";
                if ( i != ( ccyList.length - 1 ) )
                {
                    finalCcys = finalCcys + ",";
                }
            }
        }
        else
        {
            finalCcys="none";
        }

        String[] subchannelList = subChannel.split( "," );
        String finalchannel="";
        if(!subchannelList[0].equals( "none" ))
        {
            for ( int i = 0; i < subchannelList.length; i++ )
            {
                finalchannel = finalchannel +"'"+ subchannelList[i].replace( '-','/' )+"'";
                if ( i != ( subchannelList.length - 1 ) )
                {
                    finalchannel = finalchannel + ",";
                }
            }
        }
        else
        {
            finalchannel="none";
        }

        String[] subbuysellList = buysell.split( "," );
        String finalbuysell="";
        if(!subbuysellList[0].equals( "none" ))
        {
            for ( int i = 0; i < subbuysellList.length; i++ )
            {
                finalbuysell = finalbuysell +"'"+ subbuysellList[i]+"'";
                if ( i != ( subbuysellList.length - 1 ) )
                {
                    finalbuysell = finalbuysell + ",";
                }
            }
        }
        else
        {
            finalbuysell="none";
        }

        String[] subtakerorgList = takerorg.split( "," );
        String finaltakerorg="";
        if(!subtakerorgList[0].equals( "none" ))
        {
            for ( int i = 0; i < subtakerorgList.length; i++ )
            {
                finaltakerorg = finaltakerorg +"'"+ subtakerorgList[i]+"'";
                if ( i != ( subtakerorgList.length - 1 ) )
                {
                    finaltakerorg = finaltakerorg + ",";
                }
            }
        }
        else
        {
            finaltakerorg="none";
        }


        String[] substatusList = status.split( "," );
        String finalstatus="";
        if(!substatusList[0].equals( "none" ))
        {
            for ( int i = 0; i < substatusList.length; i++ )
            {
                finalstatus = finalstatus +"'"+ substatusList[i]+"'";
                if ( i != ( substatusList.length - 1 ) )
                {
                    finalstatus = finalstatus + ",";
                }
            }
        }
        else
        {
            finalstatus="none";
        }


        String[] suborderTypeList = orderType.split( "," );
        String finalorderType="";
        if(!suborderTypeList[0].equals( "none" ))
        {
            for ( int i = 0; i < suborderTypeList.length; i++ )
            {
                finalorderType = finalorderType +"'"+ suborderTypeList[i].replace( '-','/' )+"'";
                if ( i != ( suborderTypeList.length - 1 ) )
                {
                    finalorderType = finalorderType + ",";
                }
            }
        }
        else
        {
            finalorderType="none";
        }


        String[] subtifList = tif.split( "," );
        String finaltif="";
        if(!subtifList[0].equals( "none" ))
        {
            for ( int i = 0; i < subtifList.length; i++ )
            {
                finaltif = finaltif +"'"+ subtifList[i]+"'";
                if ( i != ( subtifList.length - 1 ) )
                {
                    finaltif = finaltif + ",";
                }
            }
        }
        else
        {
            finaltif="none";
        }


        String[] suborigorgList = origorg.split( "," );
        String finalorigorg="";
        if(!suborigorgList[0].equals( "none" ))
        {
            for ( int i = 0; i < suborigorgList.length; i++ )
            {
                finalorigorg = finalorigorg +"'"+ suborigorgList[i]+"'";
                if ( i != ( suborigorgList.length - 1 ) )
                {
                    finalorigorg = finalorigorg + ",";
                }
            }
        }
        else
        {
            finalorigorg="none";
        }


        String []dates=timeRange.split(",");

        String orderQuery = "select array_to_json(array_agg(row_to_json(t)))"  +
                "                      from ( ";
        String rowCount = "select count(*) rowcount from (" ;
        for (int t=0;t <dates.length;t++)
        {
            String [] longTimes = dates[t].split( ":" );
            Date fromDate = new Date(Long.parseLong( longTimes[0]));
            Date toDate = new Date(Long.parseLong( longTimes[1]));


             String innerQuery=       "SELECT CREATED,ORDERID, ORDERSTATE, ORG, CCYPAIR, BUYSELL, ORDERAMTUSD, ORDERAMT, FILLEDAMT, ORDERTYPE, TIMEINFORCE, CHANNEL" +
                          " FROM ORDER_MASTER " +
                        " WHERE CREATED >= '"+fromDate+  "' AND CREATED <=  '"+toDate +"' AND "+
                        (!takerorg.equals( "none" ) ? "ORG IN (" +finaltakerorg+")":"ORG = ORG " )   + " AND  "   +
                        (!finalCcys.equals( "none" ) ? " CCYPAIR IN (" +finalCcys +")":"CCYPAIR=CCYPAIR") + " AND " +
                        (!orderType.equals( "none" ) ? "ORDERTYPE IN ("+finalorderType+")":"ORDERTYPE=ORDERTYPE") +" AND " +
                        (!status.equals( "none" ) ? "STATUS IN (" +finalstatus +")":"STATUS = STATUS") + "  AND  "   +
                        (!subChannel.equals( "none" )? "CHANNEL IN (" + finalchannel + ")":"CHANNEL = CHANNEL") + " AND " +
                        (!tif.equals( "none" ) ? "TIMEINFORCE IN (" +finaltif + ")": "TIMEINFORCE = TIMEINFORCE") + " AND " +
                        (!origorg.equals("none") ?  "ORIGINATINGORG IN ("+finalorigorg +")" :  "ORIGINATINGORG = ORIGINATINGORG")  + " AND  " +
                        (!buysell.equals( "none" ) ? "BUYSELL IN ("+finalbuysell+")" : "BUYSELL = BUYSELL ");

                        if ( t != ( dates.length - 1 ) )
                        {
                            orderQuery += " UNION " ;
                        }
            orderQuery += innerQuery;
            rowCount += innerQuery;

        }
        orderQuery += " ORDER BY " + (!orderby.equals("none")? orderby +" DESC " : "CREATED ") + "  OFFSET " +offset+" LIMIT 50)t";
        // String newccyPair = ccypair.equals("none")? ccypair:ccypair.substring( 0, 3 ) + "/" + ccypair.substring( 3 );

        /*String orderQuery = "select array_to_json(array_agg(row_to_json(t)))"  +
                "                      from (SELECT CREATED,ORDERID, ORDERSTATE, ORG, CCYPAIR, BUYSELL, ORDERAMTUSD, ORDERAMT, FILLEDAMT, ORDERTYPE, TIMEINFORCE, CHANNEL" +
                " FROM ORDER_MASTER " +
                " WHERE CREATED >= '"+fromDate+  "' AND CREATED <=  '"+toDate +"' AND "+
                (!takerorg.equals( "none" ) ? "ORG IN ('" +takerorg+"')":"ORG = ORG " )   + " AND  "   +
                (!finalCcys.equals( "none" ) ? " CCYPAIR IN ('" +finalCcys +"')":"CCYPAIR=CCYPAIR") + " AND " +
                (!orderType.equals( "none" ) ? "ORDERTYPE IN ('"+orderType+"')":"ORDERTYPE=ORDERTYPE") +" AND " +
                (!status.equals( "none" ) ? "STATUS IN ('" +status +"')":"STATUS = STATUS") + "  AND  "   +
                (!subChannel.equals( "none" )? "CHANNEL IN ('" + finalchannel + "')":"CHANNEL = CHANNEL") + " AND " +
                (!tif.equals( "none" ) ? "TIMEINFORCE IN ('" +tif + "')": "TIMEINFORCE = TIMEINFORCE") + " AND " +
                (!origorg.equals("none") ?  "ORIGINATINGORG IN ('"+origorg +"')" :  "ORIGINATINGORG = ORIGINATINGORG")  + " AND  " +
                (!buysell.equals( "none" ) ? "BUYSELL IN ('"+buysell+"')" : "BUYSELL = BUYSELL ") +" ORDER BY CREATED " + "OFFSET " +offset+" LIMIT 10)t";*/
        rowCount +=  ") t";
        int rowRum = 0;
        Map<String,String> jsonMap = new HashMap<String,String>(2);
        if (fireRowCount)
        {
             rowRum = ordersWithTradesDAO.getRowCount( rowCount );
            jsonMap.put("rowCount", String.valueOf( rowRum ) );
        }


        String jsonData = ordersWithTradesDAO.getOrderCollection( orderQuery );
        jsonMap.put("orderSet",jsonData);

        Genson genson = new Genson();
        String returnJSON ="[]";
        try
        {
            returnJSON= genson.serialize(jsonMap);
        }
        catch ( TransformationException e )
        {
            log.warn( "Exception serializing map",e);
        }
        catch ( IOException e )
        {
            log.warn( "Exception serializing map", e );
        }
        if (fireRowCount)
        {
            return returnJSON;
        }
        else
        {
            return jsonData;
        }
    }

    public List<String> getTradeEventsForOrders(String orderId) {
        return ordersWithTradesDAO.getTradeEventsForOrder(orderId);
    }

    public String getTradeEventsForOrder(String orderId) {

        Genson genson = new Genson();
        String tradesJSON ="[]";
        Map<String,String> tradeEventsMap = new HashMap<String,String>();
        // get the list of trade ids for the order.
        List <String> tradeIds = ordersWithTradesDAO.getTradeIDSForOrder(orderId);

        for (String id:tradeIds)
        {
            tradeEventsMap.put(id,getRateEventsForTrade(id));
        }
        try
        {
            tradesJSON= genson.serialize(tradeEventsMap);
        }
        catch ( TransformationException e )
        {
            log.warn( "Exception serializing map",e);
        }
        catch ( IOException e )
        {
            log.warn( "Exception serializing map", e );
        }
        return tradesJSON;
    }
}

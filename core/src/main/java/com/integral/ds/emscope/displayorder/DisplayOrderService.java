package com.integral.ds.emscope.displayorder;

import java.util.Date;
import java.util.List;

import com.integral.ds.emscope.rates.RestRatesObject;

/**
 * Service to request details of Display order
 * 
 * @author chawlas
 *
 */
public interface DisplayOrderService {

	/**
	 * Fetches the prevailing display orders rates as a book
	 * 
	 * @param ccyPair the currency pair for which to fetch the changes
	 * @param timestamp the timestamp for which to fetch the prevailing display order rate
	 * @return
	 */
	List<RestRatesObject> getPrevailingDisplayOrderRates(String ccyPair_, Date timestamp_);
	
}

package com.integral.ds.emscope.quote;

import java.util.Date;
import java.util.List;

import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;

/**
 * Quote service for forming various quotes and doing computation on quotes.
 *
 * @author Rahul Bhattacharjee
 */
public interface RatesQuoteService {

    public Quote getQuoteAtTime(String provider , String stream , String ccyPair ,Date point);

    public RestRatesObject getQuoteAtTimeForVolume(String provider , String stream , String ccyPair ,Date point,long volume);

    public List<Quote> getSampledQuotes(String provider,String stream,String ccyPair,
                                        Date start,Date end,int noOfSamples);
}

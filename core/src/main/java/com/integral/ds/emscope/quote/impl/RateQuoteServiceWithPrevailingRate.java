package com.integral.ds.emscope.quote.impl;

import com.integral.ds.emscope.quote.RatesQuoteService;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.s3.RatesLocator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Rate quote service ,it will use prevailing rate for all its calculations.
 *
 * @author Rahul Bhattacharjee
 */
public class RateQuoteServiceWithPrevailingRate extends AbstractRateQuoteService implements RatesQuoteService {

    private static final long PRE_SAMPLE_WINDOW = 10 * 60 * 1000; // 10 min
    private static final long POST_SAMPLE_WINDOW = 2 * 60 * 1000; // 2 min

    @Override
    public List<Quote> getSampledQuotes(String provider, String stream, String ccyPair, Date start, Date end, final int noOfSamples) {
        List<Quote> quotes = new ArrayList<Quote>();
        long noOfMills = (end.getTime() - start.getTime());
        long gapInMills = noOfMills/noOfSamples;
        long startingTime = start.getTime();
        int count = 0;

        while(count < noOfSamples) {
            Quote quote = getQuote(provider, stream, ccyPair, startingTime);
            if(quote != null) {
                quotes.add(quote);
            }
            startingTime += gapInMills;
            count++;
        }
        quotes = profileQuotes(quotes);
        return quotes;
    }

    private Quote getQuote(String provider, String stream ,String ccyPair, long sampleTime) {
        long startTime = sampleTime - PRE_SAMPLE_WINDOW;
        long endTime = sampleTime + POST_SAMPLE_WINDOW;
        Date sampleDate = new Date(sampleTime);
        sampleDate = getRoundedSampleDate(sampleDate);
        Date start = new Date(startTime);
        Date end = new Date(endTime);

        RatesLocator ratesLocator = new RatesLocator(provider,stream,ccyPair,start,end);
        Long prevailingRateTime = ratesLocator.getPrevailingRateTime(sampleDate);
        Quote quote = null;

        if(prevailingRateTime != null) {
            quote = ratesLocator.getQuoteForSecond(prevailingRateTime);
            if(!quote.isEmpty()) {
                quote.setSampleTime(sampleDate);
                return quote;
            }
        }
        return null;
    }

    private Date getRoundedSampleDate(Date sampleDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(sampleDate);
        calendar.set(Calendar.MILLISECOND,999);
        return calendar.getTime();
    }
}

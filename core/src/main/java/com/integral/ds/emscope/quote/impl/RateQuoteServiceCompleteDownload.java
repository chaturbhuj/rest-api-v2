package com.integral.ds.emscope.quote.impl;

import com.integral.ds.emscope.quote.RatesQuoteService;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.s3.RatesLocator;
import com.integral.ds.util.DataQueryUtils;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * @author Rahul Bhattacharjee
 */
public class RateQuoteServiceCompleteDownload extends AbstractRateQuoteService implements RatesQuoteService {

    private static final Logger LOGGER = Logger.getLogger(RateQuoteServiceCompleteDownload.class);
    private static final int MAX_WINDOW_WIDTH = 1;

    @Override
    public List<Quote> getSampledQuotes(String provider,String stream,String ccyPair,
                                        Date start,Date end,int noOfSamples) {
        List<Quote> quotes = new ArrayList<>();

        if(isWithinBounds(start,end)) {
            quotes.addAll(getSampledQuotesForSubRange(provider,stream,ccyPair,start,end,noOfSamples));
        }else {
            int numberOfBuckets = (int)(end.getTime()-start.getTime())/(MAX_WINDOW_WIDTH*60*60*1000);
            int numberOfSamplesPerBucket = noOfSamples/numberOfBuckets;
            Date startDate = start;
            Date endDate = addHours(start,MAX_WINDOW_WIDTH);

            while((endDate.getTime() <= end.getTime())) {
                if(LOGGER.isDebugEnabled()){
                    LOGGER.debug("Start " + startDate + " end " + endDate + " samples per bucket " + numberOfSamplesPerBucket);
                }
                quotes.addAll(getSampledQuotesForSubRange(provider,stream,ccyPair,startDate,endDate,numberOfSamplesPerBucket));
                startDate = endDate;
                endDate = addHours(endDate,MAX_WINDOW_WIDTH);
            }
        }
        quotes = profileQuotes(quotes);
        DataQueryUtils.getNormalizedQuotes(quotes);
        return quotes;
    }

    private Date addHours(Date subStart, int maxWindowWidth) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(subStart);
        calendar.add(Calendar.HOUR,maxWindowWidth);
        return calendar.getTime();
    }

    private List<Quote> getSampledQuotesForSubRange(String provider,String stream,String ccyPair,
                                                    Date start,Date end,int noOfSamples) {

        RatesLocator ratesLocator = null;
        try {
            ratesLocator = new RatesLocator(provider,stream,ccyPair,start,end);
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }

        List<Quote> subList = ratesLocator.getSamples(start, end, noOfSamples);
        return subList;
    }

    private boolean isWithinBounds(Date start, Date end) {
        long mills = (end.getTime() - start.getTime());
        if(mills < MAX_WINDOW_WIDTH*60*60*1000) {
            return true;
        }
        return false;
    }
}

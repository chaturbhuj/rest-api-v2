package com.integral.ds.emscope.quote.impl;

import com.integral.ds.emscope.quote.RatesQuoteService;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.s3.RatesLocator;
import com.integral.ds.util.DataQueryUtils;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Rate Quote samples for dense samples.
 *
 * @author Rahul Bhattacharjee
 */
public class RateQuoteServiceDenseSamples extends AbstractRateQuoteService implements RatesQuoteService {

    private static final Logger LOGGER = Logger.getLogger(RateQuoteServiceDenseSamples.class);

    private static final int HOUR = 1 * 60 * 60 * 1000;
    private static final int PRECAUTION_GAP = 5 * 60 * 1000;

    @Override
    public List<Quote> getSampledQuotes(String provider, String stream, String ccyPair, Date start, Date end, int noOfSamples) {
        List<Quote> quotes = new ArrayList<Quote>();
        List<Date> sampleTimes = getSampleTimes(start, end, noOfSamples);
        Map<Long,List<Date>> samplesGroupedByHour = getSamplesGroupedByHour(sampleTimes);

        Iterator<Map.Entry<Long,List<Date>>> iterator = samplesGroupedByHour.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry<Long,List<Date>> entry = iterator.next();
            Long startDateEpoch = entry.getKey();
            List<Quote> quoteForHour = getQuoteForHour(startDateEpoch,entry.getValue(),provider,stream,ccyPair);
            quotes.addAll(quoteForHour);
        }
        return quotes;
    }

    private List<Quote> getQuoteForHour(Long startDateEpoch, List<Date> sampleTimes, String provider, String stream, String ccyPair) {
        List<Quote> quotes = new ArrayList<Quote>();
        Long endDateEpoch = startDateEpoch.longValue() + HOUR;
        Date start = new Date(startDateEpoch-PRECAUTION_GAP);
        Date end = new Date(endDateEpoch+PRECAUTION_GAP);
        RatesLocator ratesLocator = new RatesLocator(provider,stream,ccyPair,start,end);
        for(Date sampleTime : sampleTimes) {
            Quote quote = getQuote(ratesLocator,sampleTime);
            if(quote != null) {
                quotes.add(quote);
            }
        }
        return quotes;
    }

    private Quote getQuote(RatesLocator ratesLocator, Date sampleTime) {
        Long prevailingRateTime = ratesLocator.getPrevailingRateTime(sampleTime);
        Quote quote = null;

        if(prevailingRateTime != null) {
            quote = ratesLocator.getQuoteForSecond(prevailingRateTime);
            if(!quote.isEmpty()) {
                quote.setSampleTime(sampleTime);
                return quote;
            }
        }
        return quote;
    }

    private Map<Long, List<Date>> getSamplesGroupedByHour(List<Date> sampleTimes) {
        Map<Long,List<Date>> sampleGroups = new TreeMap<Long,List<Date>>();
        for(Date sampleTime : sampleTimes) {
            Date keyDate = getKeyDate(sampleTime);
            List<Date> values = sampleGroups.get(keyDate.getTime());
            if(values == null) {
                values = new ArrayList<Date>();
                sampleGroups.put(keyDate.getTime(),values);
            }
            values.add(sampleTime);
        }
        return sampleGroups;
    }

    private Date getKeyDate(Date sampleTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(sampleTime);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND,0);
        return calendar.getTime();
    }

    private List<Date> getSampleTimes(Date start, Date end, int noOfSamples) {
        List<Date> dates = new ArrayList<Date>();
        long duration = (end.getTime() - start.getTime());
        long gap = duration/noOfSamples;
        int count = 0;
        long startTime = start.getTime();
        while(count < noOfSamples) {
            dates.add(new Date(startTime));
            startTime += gap;
            count++;
        }
        return dates;
    }
}

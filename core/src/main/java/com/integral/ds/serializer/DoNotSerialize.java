package com.integral.ds.serializer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation to mark fields to be ignored while gson serialization.
 *
 * @author Rahul Bhattacharjee
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface DoNotSerialize {}

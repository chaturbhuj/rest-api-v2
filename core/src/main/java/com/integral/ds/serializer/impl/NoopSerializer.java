package com.integral.ds.serializer.impl;

import com.integral.ds.serializer.Serializer;

/**
 * Simply does toString on the response object.
 * @author Rahul Bhattacharjee
 */
public class NoopSerializer implements Serializer {

    @Override
    public String serialize(Object response) {
        if(response == null) {
            return "Response null";
        }
        return response.toString();
    }
}

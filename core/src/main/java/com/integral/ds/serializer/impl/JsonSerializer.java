package com.integral.ds.serializer.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.integral.ds.serializer.CustomExclusionStrategy;
import com.integral.ds.serializer.Serializer;
import com.owlike.genson.Genson;
import org.apache.log4j.Logger;

/**
 * @author Rahul Bhattacharjee
 */
public class JsonSerializer implements Serializer {

    private static final Logger LOGGER = Logger.getLogger(JsonSerializer.class);

    // private static final Genson GENSON = new Genson();
    // changing the json serializer from GENSON to Google's Gson.

    private static final Gson GENSON = new GsonBuilder().setExclusionStrategies(new CustomExclusionStrategy()).create();

    public String serialize(Object response) {
        try {
            return GENSON.toJson(response);
        } catch (Exception e) {
            LOGGER.error("Exception while serializing response.",e);
        }
        return "Error";
    }
}

package com.integral.ds.serializer;

import com.integral.ds.serializer.impl.JsonSerializer;
import com.integral.ds.serializer.impl.NoopSerializer;
import com.integral.ds.serializer.impl.XMLSerializer;

/**
 * @author Rahul Bhattacharjee
 */
public class SerializerFactory {

    private static Serializer jsonSerializer = new JsonSerializer();
    private static Serializer xmlSerializer = new XMLSerializer();
    private static Serializer noopSerializer = new NoopSerializer();

    public static final String JSON = "json";
    public static final String XML = "xml";


    public Serializer getSerializer(String type) {
        switch (type) {
            case "json": return jsonSerializer;
            case "xml":  return xmlSerializer;
            default:     return noopSerializer;
        }
    }
}

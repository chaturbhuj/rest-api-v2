package com.integral.ds.serializer;

/**
 * Serializer interface.
 */
public interface Serializer {
    String serialize(Object response);
}

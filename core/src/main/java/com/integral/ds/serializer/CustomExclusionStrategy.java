package com.integral.ds.serializer;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * GSON exclusion strategy.Excludes all fields marked with annotation @DoNotSerialize
 *
 * @author Rahul Bhattacharjee
 */
public class CustomExclusionStrategy implements ExclusionStrategy {

    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        return checkForExcludeAnnotation(fieldAttributes.getAnnotations());
    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        Annotation [] annotations = aClass.getAnnotations();
        List<Annotation> annotationList = new ArrayList<Annotation>();
        for(Annotation annotation : annotations) {
            annotationList.add(annotation);
        }
        return checkForExcludeAnnotation(annotationList);
    }

    private boolean checkForExcludeAnnotation(Collection<Annotation> annotations) {
        for(Annotation annotation : annotations) {
            if(annotation instanceof DoNotSerialize) {
                return true;
            }
        }
        return false;
    }
}

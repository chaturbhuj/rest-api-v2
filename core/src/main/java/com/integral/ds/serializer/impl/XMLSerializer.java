package com.integral.ds.serializer.impl;

import com.integral.ds.serializer.Serializer;
import com.thoughtworks.xstream.XStream;

/**
 * Serializes object into XML.
 *
 * @author Rahul Bhattacharjee
 */
public class XMLSerializer implements Serializer {

    private XStream xStream = new XStream();

    @Override
    public String serialize(Object response) {
        return xStream.toXML(response);
    }
}

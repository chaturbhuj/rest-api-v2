package com.integral.ds.epa.dao;

import java.util.HashMap;
import java.util.Map;

/**
 * EPA status.
 *
 * @author Rahul Bhattacharjee
 */
public enum EpaStatus {

    INPROGRESS(1),SUCCESS(2),PROCESS_FAILURE(3),VALIDATION_FAILURE(4),SENT_SUCCESS(5),SENT_WITH_ERROR(6);

    private int id;
    private static Map<Integer,EpaStatus> statusMap = new HashMap<Integer,EpaStatus>();

    static {
        init();
    }

    private static void init() {
        EpaStatus [] values = values();
        for(EpaStatus status : values) {
            statusMap.put(status.getId(),status);
        }
    }

    EpaStatus(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static EpaStatus getStatusForId(int code) {
        return statusMap.get(code);
    }
}

package com.integral.ds.epa.core;

import com.integral.ds.s3.S3StreamReader;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Download file , streaming from S3.
 *
 * @author Rahul Bhattacharjee
 */
public class EPADownloadServlet extends DaoSupportServlet {

    private static final Logger LOGGER = Logger.getLogger(EPADownloadServlet.class);

    private static final String BUCKET_NAME = "execution-profile";
    private static final String REQUEST_PARAM_KEY = "requestid";
    private static final String CONTENT_TYPE = "application/zip, application/octet-stream";
    private static final String CONTENT_DISPOSITION_KEY = "Content-Disposition";
    private static final String CONTENT_DISPOSITION = "attachment; filename=\"epa_results.zip\"";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            resp.setContentType(CONTENT_TYPE);
            resp.setHeader(CONTENT_DISPOSITION_KEY, CONTENT_DISPOSITION);

            String requestIdString = req.getParameter(REQUEST_PARAM_KEY);
            String outputFile = getEpaDao().getOutputFileForRequestId(Long.parseLong(requestIdString));
            outputStream = resp.getOutputStream();
            inputStream = getInputStream(outputFile);
            IOUtils.copy(inputStream, outputStream);
        } catch (Exception e) {
            LOGGER.error("Exception while downloading files from S3.",e);
        } finally {
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(outputStream);
        }
    }

    private InputStream getInputStream(String fileName) {
        fileName = fileName.substring(fileName.indexOf("external_trades"));
        S3StreamReader streamReader = new S3StreamReader();
        return streamReader.getInputStream(BUCKET_NAME,fileName);
    }
}

package com.integral.ds.epa.core.auth;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * Authentication filter.
 *
 * @author Rahul Bhattacharjee
 */
public class AuthenticationFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(AuthenticationFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession(false);
        if(session == null) {
            String contextPath = request.getContextPath();
            String pathRequested = getFullURL(request);

            LOGGER.debug("Requested path => " + pathRequested);

            response.sendRedirect(contextPath + "/auth/login.html?path=" + URLEncoder.encode(pathRequested));

            return;
        } else {
            String userName = (String) session.getAttribute("UserName");
            LOGGER.debug("User : " + userName + " requested for " + request.getRequestURI());
        }
        filterChain.doFilter(request,response);
    }

    public static String getFullURL(HttpServletRequest request) {
        StringBuffer requestURL = request.getRequestURL();
        String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }

    @Override
    public void destroy() {

    }
}

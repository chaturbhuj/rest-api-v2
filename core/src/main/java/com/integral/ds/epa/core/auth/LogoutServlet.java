package com.integral.ds.epa.core.auth;

import com.integral.ds.epa.core.DaoSupportServlet;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Invalidates the active session.
 *
 * @author Rahul Bhattacharjee
 */
public class LogoutServlet extends DaoSupportServlet {

    private static final Logger LOGGER = Logger.getLogger(LogoutServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if(session != null) {
            LOGGER.debug("Invalidating current session");
            session.invalidate();
        }
        String contextPath = req.getContextPath();
        resp.sendRedirect(contextPath + "/auth/login.html");
    }
}

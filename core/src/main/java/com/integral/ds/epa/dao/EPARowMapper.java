package com.integral.ds.epa.dao;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Maps a row of external_trade_profile table to a bean of type EpaInfo.
 *
 * @author Rahul Bhattacharjee
 */
public class EPARowMapper implements RowMapper<EpaInfo> {

    @Override
    public EpaInfo mapRow(ResultSet resultSet, int count) throws SQLException {
        EpaInfo result = new EpaInfo();
        result.setRequestId(resultSet.getLong(1));
        result.setDate(resultSet.getDate(2));
        result.setInputFile(resultSet.getString(3));
        result.setOutputFile(resultSet.getString(4));
        result.setStatus(EpaStatus.getStatusForId(resultSet.getInt(5)));
        result.setEmails(getEmails(resultSet.getString(6)));
        result.setComment(resultSet.getString(7));
        return result;
    }

    private List<String> getEmails(String email) {
        if(StringUtils.isNotBlank(email)) {
            List<String> result = new ArrayList<String>();
            String [] emails = email.split(",");
            for(String currentEmail : emails) {
                result.add(currentEmail);
            }
            return result;
        }
        return null;
    }
}

package com.integral.ds.epa.dao;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * EPA dao to interact with the external_trade_profile table.
 *
 * @author Rahul Bhattacharjee
 */
public class EPADao {

    private static final Logger LOGGER = Logger.getLogger(EPADao.class);

    private static final String OUTPUT_FILE_FOR_REQUEST_ID = "SELECT * FROM EXTERNAL_TRADE_PROFILE WHERE REQUESTID = ?";

    private static final String UPDATE_RECORD = "UPDATE EXTERNAL_TRADE_PROFILE SET STATUS = ? , COMMENT = ? WHERE REQUESTID = ?";

    private static final String UPDATE_EMAIL_RECORD = "UPDATE EXTERNAL_TRADE_PROFILE SET TOEMAIL = ? WHERE REQUESTID = ?";

    private static final String LIST_SUCCESSFUL_RECORDS = "SELECT * FROM EXTERNAL_TRADE_PROFILE WHERE STATUS IN (2,3,4)";

    private NamedParameterJdbcTemplate jdbcTemplate;

    private BasicDataSource dataSource;

    public EPADao(BasicDataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public void updateStatus(long request, EpaStatus validationFailure, String comment) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(UPDATE_RECORD);
            statement.setInt(1, validationFailure.getId());
            statement.setString(2,comment);
            statement.setLong(3, request);
            statement.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("Exception while updating status.",e);
            throw new IllegalArgumentException(e);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error("Exception while closing statement.",e);
                }
            }
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Exception while closing connection.",e);
                }
            }
        }
    }

    public String getOutputFileForRequestId(long requestId) {
        Connection connection = null;
        PreparedStatement statement = null;
        String outputFile = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(OUTPUT_FILE_FOR_REQUEST_ID);
            statement.setLong(1, requestId);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                outputFile = resultSet.getString("outputfile");
            }
        } catch (Exception e) {
            LOGGER.error("Exception while getting output file for request id " + requestId ,e);
            throw new IllegalArgumentException(e);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error("Exception while closing statement.",e);
                }
            }
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Exception while closing connection.",e);
                }
            }
        }
        return outputFile;
    }

    public void updateEmailOfCustomer(long requestId, String emails) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(UPDATE_EMAIL_RECORD);
            statement.setString(1,emails);
            statement.setLong(2, requestId);
            statement.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("Exception while updating email of customer.",e);
            throw new IllegalArgumentException(e);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error("Exception while closing statement.",e);
                }
            }
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Exception while closing connection.",e);
                }
            }
        }
    }

    public List<EpaInfo> getCompletedRequests() {
        return jdbcTemplate.query(LIST_SUCCESSFUL_RECORDS, new EPARowMapper());
    }
}

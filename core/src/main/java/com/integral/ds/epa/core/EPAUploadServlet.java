package com.integral.ds.epa.core;

import com.integral.ds.epa.bean.InputParams;
import com.integral.ds.epa.remote.RemoteEpaJobSubmitter;
import com.integral.ds.s3.S3StreamReader;
import com.integral.ds.util.EmailUtil;
import com.integral.ds.util.MailMessage;
import com.integral.ds.util.MailMessageFactory;
import com.integral.ds.util.PropertyReader;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.mail.MessagingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Collects date from the UI for processing.
 *
 * @author Rahul Bhattacharjee
 */
public class EPAUploadServlet extends DaoSupportServlet {

    private static final Logger LOGGER = Logger.getLogger(EPAUploadServlet.class);

    private static final String TMP_DIR = PropertyReader.getPropertyValue(PropertyReader.KEY_TEMP_EPA_DIRECTORY);

    private static final RemoteEpaJobSubmitter REST_INVOKER = new RemoteEpaJobSubmitter();
    private static ExecutorService EXECUTOR_SERVICE = null;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        initResultPollerThread();
    }

    private void initResultPollerThread() {
        if(PropertyReader.getBooleanPropertyValue(PropertyReader.KEY_ENABLE_DOWNLOAD_THREAD)) {
            EXECUTOR_SERVICE = Executors.newSingleThreadExecutor(new ThreadFactory() {
                @Override
                public Thread newThread(Runnable r) {
                    Thread t = new Thread(r,"Background-Response-Pooling-Thread");
                    t.setDaemon(true);
                    return t;
                }
            });
            EXECUTOR_SERVICE.submit(new ResponseGeneratorTask(getEpaDao()));
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            File repository = new File(TMP_DIR);
            repository.mkdirs();
            factory.setRepository(repository);

            ServletFileUpload upload = new ServletFileUpload(factory);
            List<FileItem> items = upload.parseRequest(request);
            InputParams params = getParams(items);
            response.getWriter().write("Request submitted for processing. You will get a confirmation email shortly.");
            processRequest(params);
        } catch (Exception e) {
            LOGGER.error("Exception while processing request.",e);
            response.getWriter().write("Exception while processing request " + e.getMessage());
        }
    }

    private void processRequest(InputParams params) throws Exception {
        zipInputFile(params);
        uploadZippedFileToS3(params);
        long requestId =  REST_INVOKER.invoke(params);
        sendResponse(params,requestId);
    }

    private void sendResponse(InputParams params, long requestId) {
        if(requestId == RemoteEpaJobSubmitter.REMOTE_SERVICE_INVOCATION_ERROR) {
            emailForException(params);
        } else {
            params.setRequestId(requestId);
            getEpaDao().updateEmailOfCustomer(requestId, StringUtils.join(params.getEmails(), ","));
            emailForAcceptance(params);
        }
    }

    private void emailForException(InputParams params) {
        sendEmail(params,"Failed to process the request.", getFailureMessage(params));
    }

    private void emailForAcceptance(InputParams params) {
        sendEmail(params,"Request accepted for processing",getAcceptanceMessage(params));
    }

    private void sendEmail(InputParams params,String messageSubject, String messageBody) {
        if(params.getEmails() != null) {
            EmailUtil util = new EmailUtil();
            MailMessage message = MailMessageFactory.getEmptyMailMessage();
            message.setMailContents(messageBody);
            message.setMailSubject(messageSubject);
            message.setToMailIds(params.getEmails());
            try {
                util.sendMail(message);
            } catch (MessagingException e) {
                LOGGER.error("Exception while sending email.",e);
            }
        }
    }

    private String getFailureMessage(InputParams params) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Hello, \n\n");
        stringBuilder.append("Failed to process your request.REQUEST ID " + params.getRequestId() + "\n");
        stringBuilder.append("\n Thanks, \n").append("EPA Team");
        return stringBuilder.toString();
    }

    private String getAcceptanceMessage(InputParams params) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Hello, \n\n");
        stringBuilder.append("Your request has been accepted for processing , REQUEST ID " + params.getRequestId() + "\n");
        stringBuilder.append("\n Thanks, \n").append("EPA Team");
        return stringBuilder.toString();
    }

    private void uploadZippedFileToS3(InputParams params) {
        S3StreamReader writer = new S3StreamReader();
        writer.uploadLocalFile("execution-profile","external_trades/" + params.getCustomerName()+"/"+params.getS3FileName()+".zip",params.getLocalZippedFile());
    }

    private void zipInputFile(InputParams params) throws Exception {
        ZipOutputStream out = null;
        FileInputStream in = null;
        FileOutputStream fout = null;
        try {
            String fileName = TMP_DIR + File.separator + params.getS3FileName() + ".zip";
            params.setLocalZippedFile(fileName);
            File file = new File(fileName);
            in = new FileInputStream(params.getUploadedFile());
            fout = new FileOutputStream(file);
            out = new ZipOutputStream(fout);
            ZipEntry zipEntry = new ZipEntry(params.getS3FileName()+".csv");
            out.putNextEntry(zipEntry);
            IOUtils.copy(in, out);
        } finally {
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(fout);
        }
    }

    private InputParams getParams(List<FileItem> items) {
        InputParams inputParams = new InputParams();
        Iterator<FileItem> itemIterator = items.iterator();
        while(itemIterator.hasNext()) {
            FileItem item = itemIterator.next();
            String fieldName = item.getFieldName();
            if(item.isFormField()) {
                if("customer".equalsIgnoreCase(fieldName)) {
                    inputParams.setCustomerName(item.getString());
                } else if("email".equalsIgnoreCase(fieldName)){
                    inputParams.setEmails(getEmailList(item.getString()));
                }
            } else {
                long epoch = new Date().getTime();
                String suffix = "_" + epoch + ".csv";
                String fileName = TMP_DIR + File.separator + fieldName + suffix;
                String s3Key = fieldName+"_"+epoch;
                File tempFile = new File(fileName);
                try {
                    item.write(tempFile);
                } catch (Exception e) {
                    LOGGER.error("Exception while parsing form input fields.",e);
                }
                inputParams.setUploadedFile(tempFile);
                inputParams.setS3FileName(s3Key);
            }
        }
        return inputParams;
    }

    private List<String> getEmailList(String string) {
        List<String> emails = new ArrayList<String>();
        String [] emailStrings = string.split(",");
        for(String email : emailStrings) {
            emails.add(email);
        }
        return emails;
    }

}

package com.integral.ds.epa.dao;

import java.util.Date;
import java.util.List;

/**
 * Bean representing a row in external_trade_profile.
 *
 * @author Rahul Bhattacharjee
 */
public class EpaInfo {

    private long requestId;
    private Date date;
    private String inputFile;
    private String outputFile;
    private EpaStatus status;
    private List<String> emails;
    private String comment;

    public EpaInfo() {
    }

    public long getRequestId() {
        return requestId;
    }

    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getInputFile() {
        return inputFile;
    }

    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    public String getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
    }

    public EpaStatus getStatus() {
        return status;
    }

    public void setStatus(EpaStatus status) {
        this.status = status;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "EpaInfo{" +
                "requestId=" + requestId +
                ", date=" + date +
                ", inputFile='" + inputFile + '\'' +
                ", outputFile='" + outputFile + '\'' +
                ", status=" + status +
                ", emails=" + emails +
                ", comment='" + comment + '\'' +
                '}';
    }
}

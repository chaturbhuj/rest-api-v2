package com.integral.ds.epa.bean;

import java.io.File;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class InputParams {

    private String customerName;
    private String s3FileName;
    private List<String> emails;
    private File uploadedFile;
    private String s3KeyName;
    private String localZippedFile;
    private long requestId;

    public InputParams() {
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public File getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(File uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public String getS3FileName() {
        return s3FileName;
    }

    public void setS3FileName(String s3FileName) {
        this.s3FileName = s3FileName;
    }

    public String getS3KeyName() {
        return s3KeyName;
    }

    public void setS3KeyName(String s3KeyName) {
        this.s3KeyName = s3KeyName;
    }

    public String getLocalZippedFile() {
        return localZippedFile;
    }

    public void setLocalZippedFile(String localZippedFile) {
        this.localZippedFile = localZippedFile;
    }

    public long getRequestId() {
        return requestId;
    }

    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }
}

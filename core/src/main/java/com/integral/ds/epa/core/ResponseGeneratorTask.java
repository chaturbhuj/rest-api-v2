package com.integral.ds.epa.core;

import com.integral.ds.epa.dao.EPADao;
import com.integral.ds.epa.dao.EpaInfo;
import com.integral.ds.epa.dao.EpaStatus;
import com.integral.ds.util.EmailUtil;
import com.integral.ds.util.MailMessage;
import com.integral.ds.util.MailMessageFactory;
import com.integral.ds.util.PropertyReader;
import org.apache.log4j.Logger;

import javax.mail.MessagingException;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * This async. task that checks for completed epa jobs and sends emails to the requestor.
 *
 *
 * @author Rahul Bhattacharjee
 */
public class ResponseGeneratorTask implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(ResponseGeneratorTask.class);

    private static final long DELAY = 10;
    private static final TimeUnit unit = TimeUnit.SECONDS;

    private static final String BASE_URL = PropertyReader.getPropertyValue(PropertyReader.KEY_DOWNLOAD_SERVLET_BASE);

    private static final EmailUtil EMAIL_UTIL = new EmailUtil();

    private EPADao dao;

    public ResponseGeneratorTask(EPADao dao) {
        this.dao = dao;
    }

    @Override
    public void run() {
        while(true) {
            try{
                checkForNewRecords();
                sleepForSometime();
            } catch (Exception e) {
                LOGGER.error("Exception while pooling for completed requests.",e);
            }
        }
    }

    private void checkForNewRecords() {
        LOGGER.info("Pooling for new records.");
        List<EpaInfo> completedProcess = dao.getCompletedRequests();
        LOGGER.info("Size of completed records => " + completedProcess.size());
        for(EpaInfo completedTask : completedProcess) {
            LOGGER.info("Info of completed process => " + completedProcess);
            sendNotification(completedTask);
        }
    }

    private void sendNotification(EpaInfo completedTask) {
        EpaStatus status = completedTask.getStatus();
        switch(status) {
            case SUCCESS: sendSuccessNotification(completedTask);
                          break;
            case VALIDATION_FAILURE: sendFailureNotification(completedTask);
                                     break;
            case PROCESS_FAILURE: sendFailureNotification(completedTask);
        }
    }

    private void sendSuccessNotification(EpaInfo completedTask) {
        List<String> toEmails = completedTask.getEmails();
        if(toEmails != null && !toEmails.isEmpty()) {
            MailMessage message = MailMessageFactory.getEmptyMailMessage();
            message.setToMailIds(toEmails);
            message.setMailContents(getSuccessMailContent(completedTask));
            message.setMailSubject(getSubjectForSuccess(completedTask));
            try {
                EMAIL_UTIL.sendMail(message);
            } catch (MessagingException e) {
                LOGGER.error("Exception while sending notification.",e);
            }
        }
        dao.updateStatus(completedTask.getRequestId(), EpaStatus.SENT_SUCCESS,"Response email sent to client.");
    }

    private void sendFailureNotification(EpaInfo completedTask) {
        List<String> toEmails = completedTask.getEmails();
        if(toEmails != null && !toEmails.isEmpty()) {
            MailMessage message = MailMessageFactory.getEmptyMailMessage();
            message.setToMailIds(toEmails);
            message.setMailContents(getFailureMailContent(completedTask));
            message.setMailSubject(getSubjectForFailure(completedTask));
            try {
                EMAIL_UTIL.sendMail(message);
            } catch (MessagingException e) {
                LOGGER.error("Exception while sending notification.",e);
            }
        }
        dao.updateStatus(completedTask.getRequestId(), EpaStatus.SENT_WITH_ERROR,"Response email sent to client.");
    }

    private String getSubjectForSuccess(EpaInfo completedTask) {
        return "EPA completed and available for download . Request ID : " + completedTask.getRequestId();
    }

    private String getSubjectForFailure(EpaInfo completedTask) {
        return "EPA process failed for Request ID : " + completedTask.getRequestId();
    }

    private String getSuccessMailContent(EpaInfo completedTask) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Hi, \n Your EPA report for request " + completedTask.getRequestId() + " is available for download.\n");
        stringBuilder.append("Download link \n");
        stringBuilder.append(BASE_URL + "?requestid=" + completedTask.getRequestId() + "\n");
        stringBuilder.append("Thanks,\n");
        stringBuilder.append("EPA Team.");
        return stringBuilder.toString();
    }

    private String getFailureMailContent(EpaInfo completedTask) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Hi, \n Your EPA report for request " + completedTask.getRequestId() + " has failed for the following reason.\n");
        stringBuilder.append(completedTask.getComment()+ "\n");
        stringBuilder.append("\nThanks,\n");
        stringBuilder.append("EPA Team.");
        return stringBuilder.toString();
    }

    private void sleepForSometime() {
        LOGGER.debug("Delay while pooling.");
        try {
            unit.sleep(DELAY);
        } catch (InterruptedException e) {
            throw new IllegalArgumentException("Interrupted exception encountered.",e);
        }
    }
}

package com.integral.ds.epa.core.auth;

import com.integral.ds.emscope.auth.AuthAppDataContext;
import com.integral.ds.epa.core.DaoSupportServlet;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import sun.swing.StringUIClientPropertyKey;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URLDecoder;

/**
 * Login servlet for EPA.
 *
 * @author Rahul Bhattacharjee
 */
public class LoginServlet extends DaoSupportServlet {

    private static final Logger LOGGER = Logger.getLogger(LoginServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("username");
        String password = req.getParameter("password");

        String contextPath = req.getContextPath();
        LOGGER.debug("Username " + userName);
        LOGGER.debug("Password " + password);

        AuthAppDataContext context = getAppDataContext();
        boolean isLoginValid = context.isLoginValid(userName, password);

        if(isLoginValid) {
            HttpSession session = req.getSession();
            session.setAttribute("UserName", userName);

            String landingPage = req.getParameter("landing_url");

            LOGGER.debug("Landing page requested is " + landingPage);

            if(StringUtils.isNotBlank(landingPage)) {
                landingPage = URLDecoder.decode(landingPage);
                resp.sendRedirect(landingPage);
            } else {
                resp.sendRedirect(contextPath + "/web/home.html");
            }

        } else {
            resp.getWriter().write("Invalid Login.");
        }
    }
}



package com.integral.ds.epa.remote;

import com.integral.ds.epa.bean.InputParams;
import com.integral.ds.util.PropertyReader;

import org.apache.log4j.Logger;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Invokes rest endpoint for starting off EPA job. No exception is throws from this method. -1 is returned in case of
 * remote exception.
 *
 * @author Rahul Bhattacharjee
 */
public class RemoteEpaJobSubmitter {

    private static final Logger LOGGER = Logger.getLogger(RemoteEpaJobSubmitter.class);

    private static final String REST_URL = PropertyReader.getPropertyValue(PropertyReader.KEY_REMOTE_REST_ENDPOINT);
    private static final String GET_METHOD = "GET";

    public static final long REMOTE_SERVICE_INVOCATION_ERROR = -1;

    public long invoke(InputParams params) throws Exception {
        long requestId = REMOTE_SERVICE_INVOCATION_ERROR;
        try {
            String restUrl = getRestUrl(params);
            URL url = new URL(restUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(GET_METHOD);

            InputStream inputStream = connection.getInputStream();
            byte [] buf = new byte[inputStream.available()];
            inputStream.read(buf);

            String responseIdString = new String(buf);
            LOGGER.debug("Request id sent is " + responseIdString);
            requestId = Long.parseLong(responseIdString);
        } catch (Exception e) {
            LOGGER.error("Exception while communicating with remote "
                         + "EPA rest API.Returning request ID " + requestId + 
                         ".", e);
        }
        return requestId;
    }

    private String getRestUrl(InputParams params) {
        String restUrl = null;
        try {
            String customerName = params.getCustomerName();
            String s3FileName = params.getS3FileName();
            restUrl = REST_URL + "/" + URLEncoder.encode(customerName, "utf-8") + "/" + s3FileName;
            LOGGER.debug("Generated RestURL : " + restUrl);
        } catch(Exception e) {
            LOGGER.error("Exception while creating remote URL", e);
            throw new IllegalArgumentException(e);
        }

        return restUrl;
    }
}


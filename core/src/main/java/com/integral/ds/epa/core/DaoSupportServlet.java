package com.integral.ds.epa.core;

import com.integral.ds.emscope.auth.AuthAppDataContext;
import com.integral.ds.epa.dao.EPADao;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/**
 * Base servlet for epa servlets. It will have spring , dao support.
 *
 * @author Rahul Bhattacharjee
 */
public class DaoSupportServlet extends HttpServlet {

    private EPADao epaDao;
    private AuthAppDataContext authAppDataContext;
    private static final String DAO_KEY = "epaDao";
    private static final String AUTH_CONTEXT = "authAppDataContext";

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().
                getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
        epaDao = (EPADao) context.getBean(DAO_KEY);
        authAppDataContext = (AuthAppDataContext) context.getBean(AUTH_CONTEXT);
    }

    protected EPADao getEpaDao() {
        return this.epaDao;
    }

    protected AuthAppDataContext getAppDataContext() {
        return authAppDataContext;
    }
}

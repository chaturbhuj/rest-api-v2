package com.integral.ds.epa.core;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Startup servlet , for initializing the logging sub system etc.
 * @author Rahul Bhattacharjee
 */
public class StartupServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(StartupServlet.class);

    private static final String LOG_CONFIG_NAME = "log4jConfigLocation";

    @Override
    public void init(ServletConfig config) throws ServletException {
        initLogger(config.getServletContext());
    }

    private void initLogger(ServletContext context) throws ServletException{
        InputStream in = null;
        try {
            String logFileLocation = context.getInitParameter(LOG_CONFIG_NAME);
            in = context.getResourceAsStream(logFileLocation);
            Properties properties = new Properties();
            properties.load(in);
            PropertyConfigurator.configure(properties);

            LOGGER.info("Logger initialized for ETL_EPA.");

        } catch (Exception e) {
            throw new ServletException("Exception while initializing application.",e);
        } finally {
            if(in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

package com.integral.ds.event;

import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public class EventAggregationBean {

    private Date eventDate;
    private String userName;
    private String eventType;
    private String eventName;
    private int count;

    public EventAggregationBean() {
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "EventAggregationBean{" +
                "eventDate=" + eventDate +
                ", userName='" + userName + '\'' +
                ", eventType='" + eventType + '\'' +
                ", eventName='" + eventName + '\'' +
                ", count=" + count +
                '}';
    }
}

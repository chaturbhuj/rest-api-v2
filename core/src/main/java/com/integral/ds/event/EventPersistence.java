package com.integral.ds.event;

import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public interface EventPersistence {

    public void insertEvent(Event event);

    public List<EventAggregationBean> getCurrentAggregationReport();
}

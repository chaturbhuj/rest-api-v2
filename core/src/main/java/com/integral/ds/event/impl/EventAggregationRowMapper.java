package com.integral.ds.event.impl;

import com.integral.ds.event.EventAggregationBean;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public class EventAggregationRowMapper implements RowMapper<EventAggregationBean>{

    @Override
    public EventAggregationBean mapRow(ResultSet resultSet, int i) throws SQLException {
        EventAggregationBean eventAggregationBean = new EventAggregationBean();
        Date date = resultSet.getDate(1);
        String userName = resultSet.getString(2);
        String eventType = resultSet.getString(3);
        String eventName = resultSet.getString(4);
        int count = resultSet.getInt(5);

        eventAggregationBean.setCount(count);
        eventAggregationBean.setEventDate(date);
        eventAggregationBean.setEventName(eventName);
        eventAggregationBean.setEventType(eventType);
        eventAggregationBean.setUserName(userName);
        return eventAggregationBean;
    }
}

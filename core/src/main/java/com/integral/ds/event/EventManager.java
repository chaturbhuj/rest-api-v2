package com.integral.ds.event;

import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public interface EventManager {

    public void init();
    /**
     *  Submits a event.
     */
    public void submitEvent(Event event);

    /**
     * Returns the list of entries for reporting.
     * @return
     */
    public List<EventAggregationBean> getCurrentEventReport();
}

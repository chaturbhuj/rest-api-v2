package com.integral.ds.event.impl;

import com.integral.ds.event.Event;
import com.integral.ds.event.EventPersistence;
import org.apache.log4j.Logger;

/**
 * @author Rahul Bhattahcharjee
 */
public class EventSubmitTask implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(EventSubmitTask.class);

    private Event event;
    private EventPersistence eventPersistence;

    public EventSubmitTask(Event event, EventPersistence eventPersistence) {
        this.event = event;
        this.eventPersistence = eventPersistence;
    }

    @Override
    public void run() {
        try {
            eventPersistence.insertEvent(event);
        } catch (Exception e) {
            LOGGER.error("Exception while inserting event.",e);
        }
    }
}

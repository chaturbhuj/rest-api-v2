package com.integral.ds.event.impl;

import com.integral.ds.event.Event;
import com.integral.ds.event.EventAggregationBean;
import com.integral.ds.event.EventManager;
import com.integral.ds.event.EventPersistence;
import com.integral.ds.util.PropertyReader;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * @author Rahul Bhattacharjee
 */
public class EventManagerImpl implements EventManager {

    private EventPersistence eventPersistence;
    private ExecutorService service;
    private boolean isEnabled = false;

    public EventManagerImpl() {}

    @Override
    public void init() {
        isEnabled = PropertyReader.getBooleanPropertyValue(PropertyReader.KEY_SERVICE_AUDIT_ENABLED);
        if(isEnabled){
            startAsynWrokerThread();
        }
    }

    private void startAsynWrokerThread() {
        service = Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r);
                thread.setDaemon(true);
                return thread;
            }
        });
    }

    @Override
    public void submitEvent(Event event) {
        if(isEnabled) {
            service.submit(new EventSubmitTask(event,eventPersistence));
        }
    }

    @Override
    public List<EventAggregationBean> getCurrentEventReport() {
        return eventPersistence.getCurrentAggregationReport();
    }

    public void setEventPersistence(EventPersistence eventPersistence) {
        this.eventPersistence = eventPersistence;
    }
}

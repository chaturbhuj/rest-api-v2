package com.integral.ds.event;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * EventType enumeration.
 *
 * @author Rahul Bhattacharjee
 */
public enum EventType {

    AUTHENTICATION("AUTHENTICATION",1,EnumSet.of(EventName.SUCCESSFUL_LOGIN,EventName.UNSUCCESSFUL_LOGIN)),
    SERVICE("SERVICE",2,EnumSet.of(EventName.ORDER_SERVICE,EventName.LANDSCAPE_SERVICE));

    public enum EventName {SUCCESSFUL_LOGIN,UNSUCCESSFUL_LOGIN,ORDER_SERVICE,LANDSCAPE_SERVICE,NONE};

    private String eventType;
    private int index;
    private EnumSet<EventName> supportedEvents;

    private static Map<Integer,String> staticMap = new HashMap<Integer,String>();

    static {
        loadMap();
    }

    EventType(String eventType,int index,EnumSet<EventName> supportedEvents) {
        this.eventType = eventType;
        this.index = index;
        this.supportedEvents = supportedEvents;
    }

    public String getEventType() {
        return eventType;
    }

    public int getIndex() {
        return index;
    }

    public String getEventTypeFromIndex(int index){
        return staticMap.get(index);
    }

    private static void loadMap() {
        EventType [] values = values();
        for(EventType type : values) {
            staticMap.put(type.getIndex(),type.getEventType());
        }
    }
}

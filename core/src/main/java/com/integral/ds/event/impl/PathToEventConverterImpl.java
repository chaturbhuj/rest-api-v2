package com.integral.ds.event.impl;

import com.integral.ds.event.EventType;
import com.integral.ds.event.PathToEventConverter;

/**
 * @author Rahul Bhattacharjee
 */
public class PathToEventConverterImpl implements PathToEventConverter {

    @Override
    public EventType.EventName getEventNameFromPath(String path) {
        EventType.EventName eventName = EventType.EventName.NONE;
        if(path == null) {
            eventName = EventType.EventName.NONE;
        } else if (path.contains("orderswithtrades/orderid")) {
            eventName = EventType.EventName.ORDER_SERVICE;
        } else if(path.contains("orderswithtrades/full1")) {
            eventName = EventType.EventName.LANDSCAPE_SERVICE;
        }
        return eventName;
    }
}

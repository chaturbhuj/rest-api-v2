package com.integral.ds.event;

/**
 * @author Rahul Bhattacharjee
 */
public interface PathToEventConverter {

    public EventType.EventName getEventNameFromPath(String path);

}

package com.integral.ds.event;

import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public class Event {

    private Date eventDate;
    private EventType.EventName eventName;
    private EventType type;
    private String userName;

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public EventType.EventName getEventName() {
        return eventName;
    }

    public void setEventName(EventType.EventName eventName) {
        this.eventName = eventName;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "Event{" +
                "eventDate=" + eventDate +
                ", eventName='" + eventName + '\'' +
                ", type=" + type +
                ", userName='" + userName + '\'' +
                '}';
    }
}

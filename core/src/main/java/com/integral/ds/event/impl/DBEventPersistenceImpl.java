package com.integral.ds.event.impl;

import com.integral.ds.event.Event;
import com.integral.ds.event.EventAggregationBean;
import com.integral.ds.event.EventPersistence;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * Dao to interact with the event table.
 *
 * @author Rahul Bhattacharjee
 */
public class DBEventPersistenceImpl implements EventPersistence {

    private static final Logger LOGGER = Logger.getLogger(DBEventPersistenceImpl.class);

    private static final String INSERT_EVENT = "INSERT INTO EMSCOPE_EVENTS (EVENT_DATE,EVENT_NAME,USER_NAME,EVENT_TYPE) VALUES" +
            " (?,?,?,?)";

    // query for fetching only last two weeks of data.
    private static final String REPORT_QUERY = "SELECT EVENT_DATE,USER_NAME,EVENT_TYPE,EVENT_NAME , COUNT(*) AS COUNT FROM EMSCOPE_EVENTS " +
            "WHERE EVENT_DATE >= (CURRENT_DATE - 14) GROUP BY EVENT_DATE,USER_NAME,EVENT_TYPE,EVENT_NAME ORDER BY EVENT_DATE DESC , USER_NAME";

    private NamedParameterJdbcTemplate jdbcTemplate;
    private BasicDataSource dataSource;

    public DBEventPersistenceImpl(BasicDataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public void insertEvent(Event event){
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(INSERT_EVENT);
            statement.setDate(1,new Date(event.getEventDate().getTime()));
            statement.setString(2, event.getEventName().name());
            statement.setString(3, event.getUserName());
            statement.setString(4,event.getType().getEventType());
            statement.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("Exception while inserting event.Event => " + event.toString() ,e);
            throw new IllegalArgumentException(e);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error("Exception while closing statement.", e);
                }
            }
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Exception while closing connection.",e);
                }
            }
        }
    }

    public List<EventAggregationBean> getCurrentAggregationReport() {
        return jdbcTemplate.query(REPORT_QUERY, new EventAggregationRowMapper());
    }
}

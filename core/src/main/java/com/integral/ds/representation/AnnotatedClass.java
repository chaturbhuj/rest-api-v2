package com.integral.ds.representation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vchawla
 * Date: 10/14/13
 * Time: 2:23 PM
 * To change this template use File | Settings | File Templates.
 */


@ModelEntity(name = "")
public class AnnotatedClass {


    @Representation(names={"FOREIGN"})
    private Contained c;

    @Representation(names={"SUMMARY","COMPLETE"})
    private List<String> attrs = new ArrayList<>();



    @Representation(names={"COMPLETE","SUMMARY"})
    private String attr2;


    public String getAttr1() {
        return attr1;
    }


    @Representation(names={"COMPLETE","SUMMARY"})
    private String attr1;

    public void setAttr1(String attr1) {
        this.attr1 = attr1;
    }


    public String getAttr2() {
        return attr2;
    }

    public void setAttr2(String attr2) {
        this.attr2 = attr2;
    }

    public Contained getC() {
        return c;
    }

    public void setC(Contained c) {
        this.c = c;
    }

    public List<String> getAttrs() {
        return attrs;
    }

    public void setAttrs(List<String> attrs) {
        this.attrs = attrs;
    }
}

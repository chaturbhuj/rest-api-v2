package com.integral.ds.representation;

/**
 * Created with IntelliJ IDEA.
 * User: vchawla
 * Date: 10/14/13
 * Time: 4:28 PM
 * To change this template use File | Settings | File Templates.
 */

@ModelEntity (name = "contained")
public class Contained {

    @Representation(names={"COMPLETE","SUMMARY"})
    public String containedName;

    public String getContainedName() {
        return containedName;
    }

    public void setContainedName(String containedName) {
        this.containedName = containedName;
    }
}

package com.integral.ds.representation;

/**
 * Created with IntelliJ IDEA.
 * User: vchawla
 * Date: 10/15/13
 * Time: 1:48 PM
 * To change this template use File | Settings | File Templates.
 */
public enum RepresentationModel {

    COMPLETE("COMPLETE"), SUMMARY("SUMMARY");


    private String name;



    private RepresentationModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}

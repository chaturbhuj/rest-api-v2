package com.integral.ds.representation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created with IntelliJ IDEA.
 * User: vchawla
 * Date: 10/14/13
 * Time: 2:13 PM
 * To change this template use File | Settings | File Templates.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Representation {


    public String[] names() default "COMPLETE";
    public boolean neverExclude() default false;
	public String referenceUriFormat() default "";

}

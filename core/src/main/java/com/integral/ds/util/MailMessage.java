package com.integral.ds.util;

import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class MailMessage {

    private List<String> toMailIds;
    private List<String> ccMailIds;
    private List<String> attachments;
    private List<String> bccMailIds;
    private String mailSubject;
    private String mailContents;

    public MailMessage() {
    }

    public List<String> getToMailIds() {
        return toMailIds;
    }

    public void setToMailIds(List<String> toMailIds) {
        this.toMailIds = toMailIds;
    }

    public List<String> getCcMailIds() {
        return ccMailIds;
    }

    public void setCcMailIds(List<String> ccMailIds) {
        this.ccMailIds = ccMailIds;
    }

    public List<String> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<String> attachments) {
        this.attachments = attachments;
    }

    public String getMailSubject() {
        return mailSubject;
    }

    public void setMailSubject(String mailSubject) {
        this.mailSubject = mailSubject;
    }

    public String getMailContents() {
        return mailContents;
    }

    public void setMailContents(String mailContents) {
        this.mailContents = mailContents;
    }

    public List<String> getBccMailIds() {
        return bccMailIds;
    }

    public void setBccMailIds(List<String> bccMailIds) {
        this.bccMailIds = bccMailIds;
    }
}

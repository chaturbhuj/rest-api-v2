package com.integral.ds.util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;

/**
 * Emscope configuration resolver.
 *
 * @author Rahul Bhattacharjee
 */
public class EmscopeConfigurationResolver {

    private static final Logger LOGGER = Logger.getLogger(EmscopeConfigurationResolver.class);

    private static final String CONFIG_NAME = "config.properties";
    private static final String DEFAULT_ENVIRONMENT_PREFIX = "dev";
    private static final String EMSCOPE_ENVIRONMENT = "EMSCOPE_ENV";

    public Properties getConfiguration() {
        ResourceUtils resourceUtils = new ResourceUtils();
        Properties config = new Properties();

        InputStream in = resourceUtils.getInputStream(CONFIG_NAME);
        try {
            if(in != null) {
                normalizeConfigurationForEnvironment(in,config);
            }
        } catch (Exception e) {
            LOGGER.error("Exception while getting configuration.",e);
        } finally {
            resourceUtils.close(in);
        }
        return config;
    }

    private void normalizeConfigurationForEnvironment(InputStream in, Properties config) throws Exception {
        Properties originalConfig = new Properties();
        originalConfig.load(in);

        String environment = getEnvironmentPrefix();

        Iterator keyIterator = originalConfig.keySet().iterator();
        while (keyIterator.hasNext()) {
            String key = (String) keyIterator.next();
            String val = (String) originalConfig.get(key);

            if(key.startsWith(environment)) {
                key = key.substring(environment.length()+1);
            }
            config.put(key,val);
        }
    }

    private String getEnvironmentPrefix() {
        String environment = System.getenv(EMSCOPE_ENVIRONMENT);
        if(StringUtils.isBlank(environment)){
            environment = DEFAULT_ENVIRONMENT_PREFIX;
        }
        return environment;
    }
}

package com.integral.ds.util;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Gives mail message with the bcc list set.
 *
 * @author Rahul Bhattacharjee
 */
public class MailMessageFactory {

    private static List<String> bccEmailList;

    static {
        loadBccList();
    }

    private static void loadBccList() {
        String emailString = PropertyReader.getPropertyValue(PropertyReader.KEY_BCC_EMAIL_LIST);
        if(StringUtils.isNotBlank(emailString)) {
            bccEmailList = new ArrayList<String>();
            String [] emails = emailString.split(",");
            for(String email : emails ) {
                bccEmailList.add(email);
            }
        }
    }

    public static MailMessage getEmptyMailMessage() {
        MailMessage message = new MailMessage();
        if(bccEmailList != null) {
            message.setBccMailIds(bccEmailList);
        }
        return message;
    }
}

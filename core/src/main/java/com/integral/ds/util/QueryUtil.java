package com.integral.ds.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.integral.ds.dao.AuthDAO;
import com.integral.ds.staticdata.StaticDataManager;

/**
 * @author Rahul Bhattacharjee
 */
public class QueryUtil {

    public static final String RATE_TABLE_IDENTIFIER = "#RATE#";
    public static final String ORDER_TABLE_IDENTIFIER = "#ORDER#";
    public static final String BENCHMARK_TABLE_IDENTIFIER = "#BENCHMARK#";
    public static final String PROFILE_TABLE_IDENTIFIER = "#PROFILE#";
    public static final String TRADE_TABLE_IDENTIFIER = "#TRADE#";
    public static final String AUTH_TABLE_IDENTIFIER = "#AUTH#";
    
    private static StaticDataManager DATA_MANAGER = StaticDataManager.getInstance();

    private static Map<String,String> INPUT_QUERY_MAP = new HashMap<String,String>();
    private static Map<String,String> QUERY_MAP = new HashMap<String,String>();

    static {
        INPUT_QUERY_MAP.put("TEST_RATE_QUERY","select * from #RATE#");
        INPUT_QUERY_MAP.put("TEST_RATE_ORDER_QUERY","select * from #RATE# , #BENCHMARK#");
        INPUT_QUERY_MAP.put("TEST_BENCHMARK_QUERY","select * from #BENCHMARK# where tmstmp >= :TIMESTAMP limit 10;");
        
        INPUT_QUERY_MAP.put("USER_INFO_QUERY", AuthDAO.USER_LOGIN_QUERY);

        normalizeQueries();
    }

    private static void normalizeQueries() {
        Set<String> keySet = INPUT_QUERY_MAP.keySet();

        String ratesTable = DATA_MANAGER.getApplicationProperty(Constants.RATE_TABLE);
        String tradeTable = DATA_MANAGER.getApplicationProperty(Constants.TRADES_TABLE);
        String orderTable = DATA_MANAGER.getApplicationProperty(Constants.ORDERS_TABLE);
        String profileTable = DATA_MANAGER.getApplicationProperty(Constants.PROFILE_TABLE);
        String benchmarkTable = DATA_MANAGER.getApplicationProperty(Constants.BENCHMARK_TABLE);
        String authTable = DATA_MANAGER.getApplicationProperty(Constants.AUTH_TABLE);

        for(String key : keySet) {
            String query = INPUT_QUERY_MAP.get(key);
            query = query.replaceAll(RATE_TABLE_IDENTIFIER,ratesTable);
            query = query.replaceAll(TRADE_TABLE_IDENTIFIER,tradeTable);
            query = query.replaceAll(ORDER_TABLE_IDENTIFIER,orderTable);
            query = query.replaceAll(PROFILE_TABLE_IDENTIFIER,profileTable);
            query = query.replaceAll(BENCHMARK_TABLE_IDENTIFIER,benchmarkTable);
            query = query.replaceAll(AUTH_TABLE_IDENTIFIER,authTable);
            QUERY_MAP.put(key,query);
        }
    }

    public String getNamedQuery(String queryKey) {
        String query = QUERY_MAP.get(queryKey);
        if(query == null) {
            throw new IllegalArgumentException("Query for key doesn't exist.Key => " +queryKey);
        }
        return query;
    }
}

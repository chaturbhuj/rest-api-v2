package com.integral.ds.util;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.representation.Representation;
import com.integral.ds.representation.RepresentationModel;
import com.integral.ds.s3.S3StreamReader;
import org.apache.log4j.Logger;

public class DataQueryUtils {

    private static final long MILLS_IN_DAY = 24 * 60 * 60 * 1000;

    private static final Logger LOGGER = Logger.getLogger(DataQueryUtils.class);

    private static final String LISTED_CURRENCY = "AUDUSD,EURCHF,EURGBP,EURJPY,EURUSD,GBPJPY,GBPUSD,NZDUSD,USDCAD,USDCHF,USDJPY";
    private static final String S3_PROFILE_BASE_PATH = "ProfileData/";

    private static Set<String> listedCcyPair;

    private static S3StreamReader s3StreamReader = new S3StreamReader();

    static {
        loadListedCcyPairs();
    }

    private static void loadListedCcyPairs() {
        Set<String> ccyPairSet = new HashSet<String>();
        String [] ccypairs = LISTED_CURRENCY.split(",");
        for(String ccyPair : ccypairs) {
            ccyPairSet.add(ccyPair);
        }
        listedCcyPair = Collections.unmodifiableSet(ccyPairSet);
    }

    private static boolean isListedCurrency(String ccyPair) {
        return listedCcyPair.contains(ccyPair);
    }

    public static Collection<String> BuildReturnFields(Collection<?> collection, RepresentationModel model)
	{
		Collection<String>  retVal = new ArrayList<String>();
		if(collection!=null && collection.size()>0)
		{
			return BuildReturnFields(collection.iterator().next(), model);
		}
		return retVal;
	}

    /**
	 * Builds and returns a list of fields that are part of the representation model
	 * specified by the model paramter on the passed in instance object
	 * @param instance
	 * @param model
	 * @return
	 */
	public static Collection<String> BuildReturnFields (Object instance, RepresentationModel model)
	{
		if(instance!=null)
		{
			return BuildReturnFields (instance.getClass(), model);
		}
		return null;
	}
	public static Collection<String> BuildReturnFields (Class<?> reflectedType, RepresentationModel model)
	{
		/*
		 * We only need the reflected type once and do not need to
		 * call getClass for each object iterated in the collection
		 * presumably each element in the collection is of the same
		 * data type
		 */
		Collection<Method> annotatedAttributeGetters = new ArrayList<Method>();
	    Collection<String> retVal = new ArrayList<String>();
	    Collection<String> neverExcludesReturnFields = new ArrayList<String>();
	    Collection<String> allOtherReturnFields = new ArrayList<String>();
		/*
		 * Build a list of getters that have the Representation annotation 
		 */
		for (Method method : reflectedType.getMethods()) 
		{
            if (method.isAnnotationPresent(Representation.class)) 
            {
                Representation rep = method.getAnnotation(Representation.class);
            	List<String> models = Arrays.asList(rep.names());
        		if (rep.neverExclude()) 
        		{
        			neverExcludesReturnFields.add(method.getName().replace("get", ""));
        		}
        		else if(models.contains(model.getName())|| models.contains(model.toString()))
        		{
        			allOtherReturnFields.add(method.getName().replace("get", ""));
        		}
            }
		}
		///Sort the never excludes at the top before everything else
		retVal.addAll(neverExcludesReturnFields);
		retVal.addAll(allOtherReturnFields);
		return retVal;
	}

	 private static final Set<Class<?>> BASIC_TYPES = getBasicTypes();
	 private static final Set<Class<?>> COLLECTION_TYPES = getCollectionTypes();

	 private static Set<Class<?>> getBasicTypes()
	    {
	        Set<Class<?>> retVal = new HashSet<Class<?>>();
	        retVal.add(Boolean.class);
	        retVal.add(Character.class);
	        retVal.add(Byte.class);
	        retVal.add(Short.class);
	        retVal.add(Integer.class);
	        retVal.add(Long.class);
	        retVal.add(Float.class);
	        retVal.add(Double.class);
	        retVal.add(Void.class);
	        retVal.add(String.class);
            retVal.add(Date.class);
            retVal.add(Timestamp.class);
            retVal.add(BigDecimal.class);
	        return retVal;
	    }

	 private static Set<Class<?>> getCollectionTypes()
	    {
	        Set<Class<?>> retVal = new HashSet<Class<?>>();
	        retVal.add(Vector.class);
	        retVal.add(Iterable.class);
	        retVal.add(List.class);
	        retVal.add(ArrayList.class);
	        retVal.add(Collection.class);
	        retVal.add(Arrays.class);
	        return retVal;
	    }

	public static boolean isPrimitiveType(Class<?> reflectedType)
	{
		if(reflectedType!=null && reflectedType.isPrimitive()){return true;}
		if(BASIC_TYPES.contains(reflectedType)){return true;}
		return false;
	
	}
	public static boolean isCollectionType(Class<?> reflectedType) {
		if(reflectedType!=null && reflectedType.isArray()){return true;}
		if(COLLECTION_TYPES.contains(reflectedType)){return true;}
		return false;
		
	}

    public static String getS3FilePath(String year,String yearMonth,String date,String provider,String stream
                           ,String ccyPair , String fromFileSelectorSuffix) {

        String relativePathOfFirstFile = null;
        if(isListedCurrency(ccyPair)){
            relativePathOfFirstFile = "processed/"+ year +"/"+ yearMonth + "/"+ date +"/"+ provider +"/"+ stream
                +"/"+ ccyPair +"/processedLog_"+ fromFileSelectorSuffix+".zip";
        } else {
            relativePathOfFirstFile = "processed/"+ year +"/"+ yearMonth + "/"+ date +"/"+ provider +"/"+ stream
                    +"/"+ "OTHER" +"/processedLog_"+ fromFileSelectorSuffix+".zip";
        }
        return relativePathOfFirstFile;
    }

    public static List<S3FileInfo> getS3FileList(Date fromTime ,Date toTime , String provider ,String stream ,String ccyPair) {
        List<S3FileInfo> s3FileInfos = new ArrayList<S3FileInfo>();

        SimpleDateFormat simpleYearFormat = new SimpleDateFormat("y");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("y-MM-dd");
        SimpleDateFormat simpleDateMonthFormat = new SimpleDateFormat("y-MM");
        SimpleDateFormat simpleFormatForFile = new SimpleDateFormat("y-MM-dd-HH");

        String fromYear = simpleYearFormat.format(fromTime);
        String fromYearMonth = simpleDateMonthFormat.format(fromTime);
        String fromDate = simpleDateFormat.format(fromTime);
        String fromFileSelectorSuffix = simpleFormatForFile.format(fromTime);
        String relativePathOfFirstFile = getS3FilePath(fromYear,fromYearMonth,fromDate,provider,stream,ccyPair,fromFileSelectorSuffix);

        String toYear = simpleYearFormat.format(toTime);
        String toYearMonth = simpleDateMonthFormat.format(toTime);
        String toDate = simpleDateFormat.format(toTime);
        String toFileSelectorSuffix = simpleFormatForFile.format(toTime);
        String relativePathOfSecondFile = getS3FilePath(toYear,toYearMonth,toDate,provider,stream,ccyPair,toFileSelectorSuffix);

        if(relativePathOfFirstFile.equalsIgnoreCase(relativePathOfSecondFile)) {
            S3FileInfo fileInfo = new S3FileInfo();
            fileInfo.setFilePath(relativePathOfFirstFile);
            fileInfo.setFromTime(fromTime);
            fileInfo.setToTime(toTime);
            fileInfo.setStartMinute(getMinute(fromTime));
            fileInfo.setEndMinute(59);
            fileInfo.setListedCurrency(isListedCurrency(ccyPair));
            s3FileInfos.add(fileInfo);
        } else {
            long from = fromTime.getTime();
            long to = toTime.getTime();
            long duration = (to - from);

            if(duration > MILLS_IN_DAY) {
                throw new IllegalArgumentException("Too big a query range to handle , fromTime => "
                        + fromDate + " , toTime => " + toTime);
            }

            S3FileInfo startFileInfo = new S3FileInfo();
            startFileInfo.setFilePath(relativePathOfFirstFile);
            startFileInfo.setFromTime(fromTime);
            startFileInfo.setToTime(toTime);
            startFileInfo.setStartMinute(getMinute(fromTime));
            startFileInfo.setEndMinute(59);
            startFileInfo.setListedCurrency(isListedCurrency(ccyPair));
            s3FileInfos.add(startFileInfo);

            S3FileInfo endFileInfo = new S3FileInfo();
            endFileInfo.setFilePath(relativePathOfSecondFile);
            endFileInfo.setFromTime(fromTime);
            endFileInfo.setToTime(toTime);
            endFileInfo.setStartMinute(0);
            endFileInfo.setEndMinute(getMinute(toTime));
            endFileInfo.setListedCurrency(isListedCurrency(ccyPair));
            s3FileInfos.add(endFileInfo);
        }
        return s3FileInfos;
    }

    private static String getFileNameForDate(Date inputDate,String provider,String stream,String ccyPair) {
        SimpleDateFormat simpleYearFormat = new SimpleDateFormat("y");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("y-MM-dd");
        SimpleDateFormat simpleDateMonthFormat = new SimpleDateFormat("y-MM");
        SimpleDateFormat simpleFormatForFile = new SimpleDateFormat("y-MM-dd-HH");

        String year = simpleYearFormat.format(inputDate);
        String yearMonth = simpleDateMonthFormat.format(inputDate);
        String date = simpleDateFormat.format(inputDate);
        String fileSelectorSuffix = simpleFormatForFile.format(inputDate);

        return getS3FilePath(year,yearMonth,date,provider,stream,ccyPair,fileSelectorSuffix);
    }

    private static Date addHourToDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR,1);
        return calendar.getTime();
    }

    private static int getStartMinute(Date fromTime, Date presentTime)
    {
        if (getHour(presentTime) == getHour(fromTime)) {
            return getMinute(fromTime);
        }

        return 0;
    }

    private static int getEndMinute(Date toTime, Date presentTime)
    {
        if (getHour(presentTime) == getHour(toTime)) {
            return getMinute(toTime);
        }

        return 59;
    }

    public static List<S3FileInfo> getS3FileListForLongerDuration(Date fromTime ,Date toTime , String provider ,String stream ,String ccyPair) {
        List<S3FileInfo> s3FileInfos = new ArrayList<S3FileInfo>();

        Date startDate = fromTime;
        while(startDate.getTime() < toTime.getTime()) {
            String fileName = getFileNameForDate(startDate, provider, stream, ccyPair);
            S3FileInfo fileInfo = new S3FileInfo();
            fileInfo.setStartMinute(getStartMinute(fromTime, startDate));
            fileInfo.setEndMinute(getEndMinute(toTime, startDate));
            fileInfo.setFromTime(fromTime);
            fileInfo.setToTime(toTime);
            fileInfo.setFilePath(fileName);
            s3FileInfos.add(fileInfo);
            startDate = addHourToDate(startDate);
        }

        if(getHour(startDate) == getHour(toTime)) {
            String fileName = getFileNameForDate(startDate, provider, stream, ccyPair);
            S3FileInfo fileInfo = new S3FileInfo();
            fileInfo.setStartMinute(0);
            fileInfo.setEndMinute(getMinute(toTime));
            fileInfo.setFromTime(fromTime);
            fileInfo.setToTime(toTime);
            fileInfo.setFilePath(fileName);
            s3FileInfos.add(fileInfo);
        }
        return s3FileInfos;
    }

    private static int getHour(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.HOUR);
    }

    private static int getMinute(Date date) {
        SimpleDateFormat minuteExtractor = new SimpleDateFormat("m");
        try {
            return Integer.parseInt(minuteExtractor.format(date));
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * This will find the last possible time where the rates are available on a best effort basis.It might return null as well.
     */
    public static String getNearestAvailableRates(String provider, String stream, String ccyPair, Date startTime) {
        final int MAX_HOURS_TO_TRY = 120; // This code would try for MAX_HOURS_TO_TRY number of times.
        try {
            for(int hourBefore = 0 ; hourBefore < MAX_HOURS_TO_TRY ; hourBefore++) {
                Set<Integer> availableMinutesInHour = getMinutesAvailableInHour(provider, stream, ccyPair, startTime , hourBefore);
                if(!availableMinutesInHour.isEmpty()) {
                    int lastAvailableMinute = getLastAvailableMinute(startTime, availableMinutesInHour);
                    return getDate(startTime,hourBefore,lastAvailableMinute);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception while finding the last available rate.",e);
        }
        return null;
    }

    private static int chopMinuteFromFilename(String fileName) {
        int lastIndexOfDot = fileName.lastIndexOf(".");
        int lastIndexOfUnderbar = fileName.lastIndexOf("_");
        String minute = fileName.substring(lastIndexOfUnderbar+1,lastIndexOfDot);
        return Integer.parseInt(minute);
    }

    /**
     * Returns the time after reducing by number of hours before and the given minute in that hour.
     */
    public static String getDate(Date time, int hourBefore, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        calendar.add(Calendar.HOUR,-hourBefore);
        calendar.set(Calendar.MINUTE,minute);
        return calendar.getTime().toString();
    }

    private static Date getDateWithHoursBack(Date data, int hoursToMoveBack) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data);
        calendar.add(Calendar.HOUR,-hoursToMoveBack);
        return calendar.getTime();
    }

    /**
     * Get the last available minute from the end. eg. 59.
     */
    public static int getLastAvailableMinute(Date startTime, Set<Integer> availableMinutesInHour) {
        int minuteFromDate = startTime.getMinutes();
        int lastMinute = -1;
        for(int minute = minuteFromDate; minute > 0 ; minute--) {
            if(availableMinutesInHour.contains(minute)) {
                lastMinute = minute;
                break;
            }
        }
        return lastMinute;
    }

    /**
     * Get minutes available in an hour , which is hourBefore from start time.
     */
    public static Set<Integer> getMinutesAvailableInHour(String provider, String stream, String ccyPair, Date startTime, int hourBefore) throws Exception {
        Date modifiedDate = getDateWithHoursBack(startTime, hourBefore);
        List<DataQueryUtils.S3FileInfo> listOfS3Files = DataQueryUtils.getS3FileList(modifiedDate,modifiedDate,provider,stream,ccyPair);

        DataQueryUtils.S3FileInfo fileInfo = listOfS3Files.get(0);
        InputStream inputStream = null;
        ZipInputStream zipInputStream = null;
        Set<Integer> minutesAvailable = new HashSet<Integer>();

        try {
            inputStream = s3StreamReader.getInputStream(fileInfo.getFilePath());
            zipInputStream = new ZipInputStream(inputStream);

            ZipEntry entry = null;
            while((entry = zipInputStream.getNextEntry()) != null) {
                String fileName = entry.getName();
                minutesAvailable.add(chopMinuteFromFilename(fileName));
            }
        } catch (Exception e) {
            LOGGER.error("Exception while getting minutes available.",e);
        } finally {
            if(inputStream != null) {
                inputStream.close();
            }
            if(zipInputStream != null) {
                zipInputStream.close();
            }
        }
        return minutesAvailable;
    }

    public static List<S3FileInfo> getS3FileInfoForProfile(String provider, String stream, String ccyPair,Date start, Date end) {
        List<S3FileInfo> result = new ArrayList<S3FileInfo>();
        String firstFile = getProfileFile(provider,stream,ccyPair,start);
        String lastFile = getProfileFile(provider,stream,ccyPair,end);
        result.add(new S3FileInfo(firstFile));

        if(firstFile.equalsIgnoreCase(lastFile))  {
            return result;
        }

        while(!firstFile.equalsIgnoreCase(lastFile)) {
            try {
                firstFile = nextFile(firstFile);
                result.add(new S3FileInfo(firstFile));
            }catch (Exception e) {
                LOGGER.error("Error while creating next fileName, lastFileName " + lastFile,e);
            }
        }
        return result;
    }

    private static String nextFile(String fileName) throws Exception {
        String [] splits = fileName.split("/");
        String dateString = splits[1];
        String hour = splits[2];
        String file = splits[3];

        String [] fileSplits = file.split("_");
        String ccyPair = fileSplits[0];
        String provider = fileSplits[2];
        String stream = fileSplits[3];

        SimpleDateFormat dateFormatForDate = new SimpleDateFormat("y-MM-dd HH");
        String dateWithHour = dateString + " " + hour;
        Date fileDate = dateFormatForDate.parse(dateWithHour);
        Date nextFileDate = addHourToDate(fileDate);
        return getProfileFile(provider,stream,ccyPair,nextFileDate);
    }

    private static String getProfileFile(String provider, String stream, String ccyPair, Date date) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(S3_PROFILE_BASE_PATH);
        SimpleDateFormat dateFormatForDate = new SimpleDateFormat("y-MM-dd");
        String dateString = dateFormatForDate.format(date);
        stringBuilder.append(dateString);
        stringBuilder.append("/");
        SimpleDateFormat dateFormatForHour = new SimpleDateFormat("HH");
        String hourString = dateFormatForHour.format(date);
        stringBuilder.append(hourString);
        stringBuilder.append("/");
        String fileName = ccyPair + "_" + dateString + "_" +provider + "_" + stream + "_" + hourString + ".gz";
        stringBuilder.append(fileName);
        return stringBuilder.toString();
    }

    public static class S3FileInfo {

        private String filePath;
        private Date fromTime;
        private Date toTime;
        private int startMinute;
        private int endMinute;
        private boolean isListedCurrency = true;

        public S3FileInfo() {
        }

        public S3FileInfo(String filePath) {
            this.filePath = filePath;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }

        public Date getFromTime() {
            return fromTime;
        }

        public void setFromTime(Date fromTime) {
            this.fromTime = fromTime;
        }

        public Date getToTime() {
            return toTime;
        }

        public void setToTime(Date toTime) {
            this.toTime = toTime;
        }

        public int getStartMinute() {
            return startMinute;
        }

        public void setStartMinute(int startMinute) {
            this.startMinute = startMinute;
        }

        public int getEndMinute() {
            return endMinute;
        }

        public void setEndMinute(int endMinute) {
            this.endMinute = endMinute;
        }

        public boolean isListedCurrency() {
            return this.isListedCurrency;
        }

        public void setListedCurrency(boolean listedCurrency) {
            isListedCurrency = listedCurrency;
        }

        @Override
        public String toString() {
            return "S3FileInfo{" +
                    "filePath='" + filePath + '\'' +
                    ", fromTime=" + fromTime +
                    ", toTime=" + toTime +
                    ", startMinute=" + startMinute +
                    ", endMinute=" + endMinute +
                    ", isListedCurrency=" + isListedCurrency +
                    '}';
        }
    }

    public static void getNormalizedQuote(Quote quote) {
        if(!quote.isEmpty()) {
            RestRatesObject ratesObject = quote.getRates().get(0);
            Long timestamp = ratesObject.getTmstmp();
            Long timeStampWithMillsChopped = getTimeWithoutMills(timestamp);
            for(RestRatesObject rate : quote.getRates()) {
                rate.setTmstmp(timeStampWithMillsChopped);
            }
        }
    }

    public static void getNormalizedQuotes(List<Quote> quotes) {
        for(Quote quote : quotes) {
            getNormalizedQuote(quote);
        }
    }

    public static Long getTimeWithoutMills(Long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(timestamp));
        calendar.set(Calendar.MILLISECOND,0);
        return calendar.getTime().getTime();
    }

    public static RestRatesObject getRestRateObject(RatesObject rate) {
        RestRatesObject ratesObject = new RestRatesObject();
        ratesObject.setAskSize(rate.getAsk_size());
        ratesObject.setAskPrice(rate.getAsk_price());
        ratesObject.setBidSize(rate.getBid_size());
        ratesObject.setBidPrice(rate.getBid_price());
        ratesObject.setTmstmp(rate.getTmstmp());
        ratesObject.setTier(rate.getLvl());
        ratesObject.setCcyPair(rate.getCcyp());
        ratesObject.setStatus(rate.getStatus());
        return ratesObject;
    }

    public static String toString(RatesObject ratesObject) {
        String provide = ratesObject.getPrvdr();
        String strm = ratesObject.getStrm();
        String status = ratesObject.getStatus();
        String timestamp = new Date(ratesObject.getTmstmp()).toString();
        float offer = ratesObject.getAsk_price().floatValue();
        float bid = ratesObject.getBid_price().floatValue();

        return  "Provider " + provide + " ,Stream " + strm + " ,status "+ status +
                " ,timestamp " + timestamp + " ,Offer " + offer + " ,Bid " + bid;
    }

    public static String toString(RestRatesObject ratesObject) {
        String status = ratesObject.getStatus();
        String timestamp = new Date(ratesObject.getTmstmp()).toString();
        float offer = ratesObject.getAsk_price().floatValue();
        float bid = ratesObject.getBid_price().floatValue();

        return  "Status "+ status +
                " ,timestamp " + timestamp + " ,Offer " + offer + " ,Bid " + bid;
    }

    public static String getFormattedDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        return dateFormat.format(date);
    }
}

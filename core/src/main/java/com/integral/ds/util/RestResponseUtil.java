package com.integral.ds.util;

import com.integral.ds.model.ErrorObject;

import javax.servlet.http.HttpServletRequest;


/**
 * @author Rahul Bhattacharjee
 */
public class RestResponseUtil {

    public static final String RESPONSE_OBJECT_KEY = "response";
    public static final String SERIALIZER_TYPE = "serializer";

    private static final String DUMMY_RESPONSE = "";

    public static String setJsonResponse(HttpServletRequest request, Object response) {
        return setResponse(request, response,"json");
    }

    public static String setXmlResponse(HttpServletRequest request, Object response) {
        return setResponse(request, response,"xml");
    }

    public static String setResponse(HttpServletRequest request , Object response , String formatterType) {
        request.setAttribute(RESPONSE_OBJECT_KEY,response);
        request.setAttribute(SERIALIZER_TYPE,formatterType);
        return DUMMY_RESPONSE;
    }

    public static ErrorObject getErrorObject(Exception e) {
        ErrorObject errorObject = new ErrorObject();
        errorObject.setErrorMessage("Exception encountered while processing request : " + e.getMessage());
        errorObject.setType(ErrorObject.ErrorType.error);
        return errorObject;
    }

}

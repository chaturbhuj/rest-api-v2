package com.integral.ds.util;

import com.integral.ds.dto.OrderNode;

import java.util.*;

/**
 * Graph traversal related utility methods.
 *
 * @author Rahul Bhattacharjee
 */
public class GraphUtil {

    /**
     * Create a order graph from id=>order map supplied.
     *
     * @param idToOrderNodeMap
     * @return
     */
    public static OrderNode getOrderObjectGraphFromMap(Map<Integer,OrderNode> idToOrderNodeMap) {
        int parentId = getParentId(idToOrderNodeMap);
        OrderNode parentOrder = createNode(parentId,idToOrderNodeMap);
        return parentOrder;
    }

    /**
     * Recursive call to create the complete node graph.
     *
     * @param nodeId
     * @param idToOrderNodeMap
     * @return
     */
    private static OrderNode createNode(int nodeId, Map<Integer, OrderNode> idToOrderNodeMap) {
        OrderNode node = idToOrderNodeMap.get(nodeId);
        List<Integer> links = node.getLinkIds();
        List<OrderNode> linkNodes = new ArrayList<OrderNode>();
        for(Integer link : links) {
            linkNodes.add(createNode(link, idToOrderNodeMap));
        }
        node.setChildOrderList(linkNodes);
        return node;
    }

    private static Integer getParentId(Map<Integer, OrderNode> idToOrderNodeMap) {
        Set<Integer> keySet = idToOrderNodeMap.keySet();
        Set<Integer> superSet = new HashSet<Integer>();
        for(Integer key : keySet) {
            superSet.add(key);
        }
        Set<Integer> linkSet = new HashSet<Integer>();

        for(OrderNode orderNode : idToOrderNodeMap.values()) {
            List<Integer> links = orderNode.getLinkIds();
            for(Integer linkId : links) {
                linkSet.add(linkId);
            }
        }
        superSet.removeAll(linkSet);
        int size = superSet.size();
        if(size > 1) {
            System.out.println("WAARNING : GRAPH INCOMPLETE.");
        }
        return superSet.iterator().next();
    }
}

package com.integral.ds.util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Util to read configuration properties.
 *
 * @author Rahul Bhattacharjee
 */
public class PropertyReader {

    private static final Logger LOGGER = Logger.getLogger(PropertyReader.class);

    private static final String CONFIG_FILENAME = "config.properties";

    public static final String KEY_EMAIL_HOSTNAME = "epa.email.hostname";
    public static final String KEY_TEMP_EPA_DIRECTORY = "epa.temp.directory";
    public static final String KEY_REMOTE_REST_ENDPOINT = "epa.rest.endpoint";
    public static final String KEY_DOWNLOAD_SERVLET_BASE = "epa.download.servlet.base";
    public static final String KEY_BCC_EMAIL_LIST = "epa.bcc.email.list";
    public static final String KEY_ENABLE_DOWNLOAD_THREAD = "epa.enable.download.thread";
    public static final String KEY_PROFILE_SUPPORTED_CCYPAIR = "profile.supported.ccyypairs";
    public static final String KEY_SERVICE_AUDIT_ENABLED = "emscope.service.audit.enable";
    public static final String KEY_IS_DEMO_MODE = "app.demomode";

    private static Properties properties = new Properties();

    static {
        loadConfig();
    }

    private static void loadConfig() {
        InputStream inputStream = getStream();
        if(inputStream != null) {
            try {
                properties.load(inputStream);
            } catch (IOException e) {
                LOGGER.error("Exception while loading properties.",e);
            }
        }
    }

    public static String getPropertyValue(String key) {
        return properties.getProperty(key);
    }

    public static boolean getBooleanPropertyValue(String key) {
        String value = getPropertyValue(key);
        boolean returnValue = false;
        if( StringUtils.isNotBlank( value )) {
            returnValue = Boolean.parseBoolean(value);
        }
        return returnValue;
    }

    public static InputStream getStream() {
        InputStream inputStream = null;
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        if(classLoader != null) {
            inputStream = classLoader.getResourceAsStream(CONFIG_FILENAME);
            if(inputStream == null) {
                inputStream = PropertyReader.class.getResourceAsStream(CONFIG_FILENAME);
            }
        }
        return inputStream;
    }

    public static void setProperty(String key,String value) {
        properties.put(key,value);
    }
}

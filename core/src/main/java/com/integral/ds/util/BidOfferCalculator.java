package com.integral.ds.util;

import com.integral.ds.emscope.quote.QuoteTransformer;
import com.integral.ds.emscope.quote.impl.ProfileQuoteTransformerIgnoreOneSidedRates;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.model.AggregatedBookTuple;

import java.math.BigDecimal;
import java.util.*;

/**
 * Various calculations based on bad and offer tiers.
 *
 * @author Rahul Bhattacharjee
 */
public class BidOfferCalculator {

    private QuoteTransformer transformer = new ProfileQuoteTransformerIgnoreOneSidedRates();

    public BigDecimal getVWAPBidAtVolume(Quote quote,long volume) {
        quote = transformer.transform(quote);
        List<RestRatesObject> rates = quote.getRates();
        sortBids(rates);

        double collectedSize = 0;
        double volumeWeightedPrice = 0;
        boolean volumeFulfilled = false;

        for (RestRatesObject rate : rates) {
            long availableSize = rate.getBid_size().longValue();
            double availablePrice = rate.getBid_price().doubleValue();
            double remainingSize = volume - collectedSize;
            if (availableSize >= remainingSize) {
                collectedSize += remainingSize;
                volumeWeightedPrice += remainingSize*availablePrice;
                volumeFulfilled = true;
                break;
            }
            collectedSize += availableSize;
            volumeWeightedPrice += availableSize*availablePrice;
        }
        if(volumeFulfilled) {
            double vwapBidPrice = volumeWeightedPrice/collectedSize;
            return new BigDecimal(vwapBidPrice);
        }
        return null;
    }

    public BigDecimal getVWAPOfferAtVolume(Quote quote, long volume) {
        quote = transformer.transform(quote);
        List<RestRatesObject> rates = quote.getRates();
        sortOffer(rates);

        double collectedSize = 0;
        double volumeWeightedPrice = 0;
        boolean volumeFulfilled = false;

        for (RestRatesObject rate : rates) {
            long availableSize = rate.getAsk_size().longValue();
            double availablePrice = rate.getAsk_price().doubleValue();
            double remainingSize = volume - collectedSize;
            if (availableSize >= remainingSize) {
                collectedSize += remainingSize;
                volumeWeightedPrice += remainingSize*availablePrice;
                volumeFulfilled = true;
                break;
            }
            collectedSize += availableSize;
            volumeWeightedPrice += availableSize*availablePrice;
        }
        if(volumeFulfilled) {
            double vwapAskPrice = volumeWeightedPrice/collectedSize;
            return new BigDecimal(vwapAskPrice);
        }
        return null;
    }

    /**
     * isBid - true for bid and false for offer.
     *
     * One sided rates and inactive rates.
     *
     * @param rates
     * @param isBid
     * @return
     */
    public List<RestRatesObject> getFilteredRates(List<RestRatesObject> rates, boolean isBid) {
        List<RestRatesObject> result = new ArrayList<RestRatesObject>();
        for(RestRatesObject rate : rates) {
            if(isBid) {
                if(includeBidTier(rate)) {
                    result.add(rate);
                }
            } else {
                if(includeOfferTier(rate)) {
                    result.add(rate);
                }
            }
        }
        return result;
    }

    public List<AggregatedBookTuple> getAggregatedBook(List<RatesObject> rates,int bookDepth) {
        if(rates.isEmpty()) {
            return Collections.EMPTY_LIST;
        }

        List<RatesObject> bid = new ArrayList<RatesObject>();
        List<RatesObject> offer = new ArrayList<RatesObject>();
        for(RatesObject rate : rates) {
            if(includeBidTier(rate)){
                bid.add(rate);
            }
            if(includeOfferTier(rate)){
                offer.add(rate);
            }
        }

        Collections.sort(bid,FULL_RATE_OBJECT_BID_SORTER);
        Collections.sort(offer,FULL_RATE_OBJECT_OFFER_SORTER);

        List<AggregatedBookTuple> result = new ArrayList<AggregatedBookTuple>();
        int size = Math.min(bookDepth , Math.min(bid.size(),offer.size()));

        for(int index = 0; index < size ; index++) {
            RatesObject currentBid = bid.get(index);
            RatesObject currentOffer = offer.get(index);
            result.add(getTuple(currentBid,currentOffer));
        }
        return result;
    }

    public boolean includeBidTier(RatesObject ratesObject) {
        float rate = ratesObject.getBid_price().floatValue();
        float volume = ratesObject.getBid_size().floatValue();
        if(rate == 0.0f && volume == 0.0f) {
            return false;
        }
        return true;
    }

    public boolean includeOfferTier(RatesObject ratesObject) {
        float rate = ratesObject.getAsk_price().floatValue();
        float volume = ratesObject.getAsk_size().floatValue();
        if(rate == 0.0f && volume == 0.0f) {
            return false;
        }
        return true;
    }

    public boolean includeBidTier(RestRatesObject ratesObject) {
        float rate = ratesObject.getBid_price().floatValue();
        float volume = ratesObject.getBid_size().floatValue();
        if(rate == 0.0f && volume == 0.0f) {
            return false;
        }
        return true;
    }

    public boolean includeOfferTier(RestRatesObject ratesObject) {
        float rate = ratesObject.getAsk_price().floatValue();
        float volume = ratesObject.getAsk_size().floatValue();
        if(rate == 0.0f && volume == 0.0f) {
            return false;
        }
        return true;
    }


    public AggregatedBookTuple getTuple(RatesObject currentBid, RatesObject currentOffer) {
        AggregatedBookTuple tuple = new AggregatedBookTuple();
        tuple.setBidDate(new Date(currentBid.getTmstmp()));
        tuple.setBidProvider(currentBid.getPrvdr());
        tuple.setBidStream(currentBid.getStrm());
        tuple.setBidSize(currentBid.getBid_size());
        tuple.setBidTier(currentBid.getLvl());
        tuple.setBidRate(currentBid.getBid_price());
        tuple.setOfferRate(currentOffer.getAsk_price());
        tuple.setOfferTier(currentOffer.getLvl());
        tuple.setOfferSize(currentOffer.getAsk_size());
        tuple.setOfferProvider(currentOffer.getPrvdr());
        tuple.setOfferStream(currentOffer.getStrm());
        tuple.setOfferDate(new Date(currentOffer.getTmstmp()));
        return tuple;
    }

    public void sortBids(List<RestRatesObject> rates) {
        Collections.sort(rates, BID_SORTER);
    }

    public void sortOffer(List<RestRatesObject> rates) {
        Collections.sort(rates,OFFER_SORTER);
    }

    /**
     * COMPARATORS FOR SORTING.
     */

    public static Comparator<RestRatesObject> BID_SORTER = new Comparator<RestRatesObject>() {
        @Override
        public int compare(RestRatesObject o1, RestRatesObject o2) {
            if (o1.getBid_price().floatValue()  == o2.getBid_price().floatValue()) {
                return 0;
            }
            if (o1.getBid_price().floatValue() > o2.getBid_price().floatValue()) {
                return -1;
            }
            return 1;
        }
    };

    public static Comparator<RestRatesObject> OFFER_SORTER = new Comparator<RestRatesObject>() {
        @Override
        public int compare(RestRatesObject o1, RestRatesObject o2) {
            if (o1.getAsk_price().floatValue() == o2.getAsk_price().floatValue()) {
                return 0;
            }

            if (o1.getAsk_price().floatValue() > o2.getAsk_price().floatValue()) {
                return 1;
            }
            return -1;
        }
    };

    // comparator for full rate object.

    public static Comparator<RatesObject> FULL_RATE_OBJECT_BID_SORTER = new Comparator<RatesObject>() {
        @Override
        public int compare(RatesObject o1, RatesObject o2) {
            if (o1.getBid_price().floatValue()  == o2.getBid_price().floatValue()) {
                return 0;
            }
            if (o1.getBid_price().floatValue() > o2.getBid_price().floatValue()) {
                return -1;
            }
            return 1;
        }
    };

    public static Comparator<RatesObject> FULL_RATE_OBJECT_OFFER_SORTER = new Comparator<RatesObject>() {
        @Override
        public int compare(RatesObject o1, RatesObject o2) {
            if (o1.getAsk_price().floatValue() == o2.getAsk_price().floatValue()) {
                return 0;
            }

            if (o1.getAsk_price().floatValue() > o2.getAsk_price().floatValue()) {
                return 1;
            }
            return -1;
        }
    };
}

package com.integral.ds.util;

import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * @author Ashok
 */
public class EmailUtil {

    private static final Logger LOGGER = Logger.getLogger(EmailUtil.class);

    public EmailUtil() {}

    public void sendMail(MailMessage mailMessage) throws MessagingException
    {
        Properties properties = System.getProperties();
        String mailServer = PropertyReader.getPropertyValue(PropertyReader.KEY_EMAIL_HOSTNAME);

        LOGGER.debug("Using mail server " + mailServer);
        properties.setProperty("mail.smtp.host", mailServer);

        Session session = Session.getDefaultInstance(properties);

        MimeMessage message = new MimeMessage(session);
        for (String toM : mailMessage.getToMailIds()) {
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(
                    toM));
        }

        // Set To: Whom to cc this mail.
        if (mailMessage.getCcMailIds() != null) {
            for (String ccM : mailMessage.getCcMailIds()) {
                message.addRecipient(Message.RecipientType.CC,
                        new InternetAddress(ccM));
            }
        }

        if(mailMessage.getBccMailIds() != null) {
            for(String bccEmail : mailMessage.getBccMailIds()) {
                message.addRecipient(Message.RecipientType.BCC,new InternetAddress(bccEmail));
            }
        }
        // Set Subject: header field
        message.setSubject(mailMessage.getMailSubject());
        // Create the message part
        BodyPart messageBodyPart = new MimeBodyPart();

        // Main contents part.
        messageBodyPart.setText(mailMessage.getMailContents());
        Multipart multipart = new MimeMultipart();

        multipart.addBodyPart(messageBodyPart);

        // Attachments if any.
        if (mailMessage.getAttachments() != null) {
            for (String a : mailMessage.getAttachments()) {
                File f = new File(a);
                messageBodyPart = new MimeBodyPart();

                DataSource source = new FileDataSource(a);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(f.getName());
                multipart.addBodyPart(messageBodyPart);
            }
        }

        // Send the complete message parts
        message.setContent(multipart);

        // Send message
        Transport.send(message);
    }

    public static void main(String[] args)
    {
        String fromMail = "rahul@integral.com";
        List<String> toMailIds = new ArrayList<String>();
        toMailIds.add("rahul@integral.com");

        List<String>ccMailIds = new ArrayList<String>();
        String mailSubject = "TEST: Subject";
        String mailContents = "TEST: Contents";

        EmailUtil myMailer = new EmailUtil();

        MailMessage message = new MailMessage();
        message.setCcMailIds(ccMailIds);
        message.setMailContents(mailContents);
        message.setMailSubject(mailSubject);
        message.setToMailIds(toMailIds);

        try {
            myMailer.sendMail(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}

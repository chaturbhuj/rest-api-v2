package com.integral.ds.util;

/**
 * Key constants for application config file.
 *
 * @author Rahul Bhattacharjee
 */
public class Constants {

    public static final String BENCHMARK_TABLE = "table.benchmark";
    public static final String PROFILE_TABLE = "table.profile";
    public static final String RATE_TABLE = "table.rate";
    public static final String ORDERS_TABLE = "table.orders";
    public static final String TRADES_TABLE = "table.trades";
    public static final String AUTH_TABLE = "table.auth";
}

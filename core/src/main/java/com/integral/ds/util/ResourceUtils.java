package com.integral.ds.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.emscope.rates.RestRatesObject;
import org.apache.log4j.Logger;

public class ResourceUtils {

    private static final Logger LOGGER = Logger.getLogger(ResourceUtils.class);

    /**
	 * Returns the url for the given http request context
	 * @return
	 */
	public static String getContextUrl()
	{
		//TODO: Implementation of getting context url
		return null;
	}

    public InputStream getInputStream(String resourceName) {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream(resourceName);
        if(in == null) {
            throw new IllegalArgumentException(resourceName + " resource not available.");
        }
        return in;
    }

    public void close(InputStream in) {
        if(in != null) {
            try {
                in.close();
            } catch (IOException e) {
                LOGGER.error("Exception while closing inputstream.",e);
            }
        }
    }

    public static List<RatesObject> transformRateObject(List<RestRatesObject> rates,String provider,String stream) {
        if(rates == null || rates.isEmpty()) {
            return Collections.EMPTY_LIST;
        }

        List<RatesObject> result = new ArrayList<RatesObject>();
        for(RestRatesObject rate : rates) {
            RatesObject rateObject = new RatesObject();
            rateObject.setTmstmp(rate.getTmstmp());
            rateObject.setTier(rate.getLvl());
            rateObject.setAskPrice(rate.getAsk_price());
            rateObject.setAskSize(rate.getAsk_size());
            rateObject.setBidPrice(rate.getBid_price());
            rateObject.setBidSize(rate.getBid_size());
            rateObject.setGuid(rate.getGuid());
            rateObject.setCcyPair(rate.getCcyPair());
            rateObject.setProvider(provider);
            rateObject.setStream(stream);
            rateObject.setStatus(rate.getStatus());
            result.add(rateObject);
        }
        return result;
    }
}

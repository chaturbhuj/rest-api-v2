package com.integral.ds.dao.s3;

import com.integral.ds.dao.ProviderStreamConfigFetcher;
import com.integral.ds.dto.ProviderStream;
import com.integral.ds.s3.RecordTransformer;
import com.integral.ds.s3.S3StreamReader;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Dao to read streams from S3.
 *
 * @author Rahul Bhattacharjee
 */
public class S3ProviderStreamConfigFetcherImpl implements ProviderStreamConfigFetcher {

    private static final String BUCKET_NAME = "integral-rates";
    private static final String S3_STATIC_CONFIG_FILE = "config/AvailableStreams.txt";
    private static final String S3_DYNAMIC_CONFIG_FILE = "config/AdhocStreams.txt";

    private List<ProviderStream> staticProviderStreams = null;
    private Properties config = null;

    @Override
    public void init(Properties properties) {
        this.config = properties;
        staticProviderStreams = loadStreamConfig(S3_STATIC_CONFIG_FILE);
    }

    // no arg init for spring.
    public void init(){
        init(null);
    }

    @Override
    public List<ProviderStream> getProviderStreams() {
        List<ProviderStream> dynamicStreams = loadStreamConfig(S3_DYNAMIC_CONFIG_FILE);
        List<ProviderStream> listOfStreams = new ArrayList<ProviderStream>();
        listOfStreams.addAll(staticProviderStreams);
        listOfStreams.addAll(dynamicStreams);

        Set<ProviderStream> providerStreamsSet = new HashSet<>();
        providerStreamsSet.addAll(listOfStreams);
        listOfStreams.clear();
        listOfStreams.addAll(providerStreamsSet);
        Collections.sort(listOfStreams);
        return listOfStreams;
    }

    private List<ProviderStream> loadStreamConfig(String fileName) {
        return S3ReadAndParseUtility.downloadAndParseS3File(BUCKET_NAME,fileName,
                new RecordTransformer<ProviderStream>() {
                        @Override
                        public ProviderStream transform(String input) {
                            String [] splits = input.split(",");
                            String provider = splits[0];
                            String stream = splits[1];
                            return new ProviderStream(provider,stream);
                        }
        });
    }
}

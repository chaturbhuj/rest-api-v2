package com.integral.ds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.integral.ds.dto.Trade;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

public class TradeDAO {

private NamedParameterJdbcTemplate jdbcTemplate = null;
private BasicDataSource basicDataSource = null;
    private final Log log = LogFactory.getLog( TradeDAO.class );
	public TradeDAO (BasicDataSource ds)	{
        basicDataSource=ds;
		jdbcTemplate = new NamedParameterJdbcTemplate(ds);
	}
    
    public static final String QUOTES = "\"";
    public static final String COLON = ":";
    public static final String SPACE = " ";
    public static final String COMMA = ",";
    public static final String PIPE = "|";
    public static final String SEMICOLON = ";";
    public static final String UNDERSCORE = "_";

    private static final String TRADES_AS_JSON = "select array_to_json(array_agg(row_to_json(t)))" +
            "    from (select td3.stream, td3.orderid as orderid, td3.org, baseAmt, termAmt, td3.usdAmt, exectime as exectimestamp, "
            + "td3.status as TRADESTATUS, td3.ccypair,  td3.mkrtkr, td3.type as type, td3.buysell as buysell, td3.tradeid as tradeid, td3.cpty as cpty, td3.stream, " +
            "td3.spotrate SPOTRATE, td3.cpty COUNTERPARTY, td3.cptyaccount COUNTERPARTYACCOUNT, td3.dealer DEALER, td3.origcpty ORIGINATINGCPTY, td3.origcptyusr ORIGINATINGCPTYUSER," +
            " td3.trdcomment TRADECOMMENT, td3.virtualserver, td3.valuedate, td3.tradedate, td3.takerrefid, td3.makerrefid, td3.matchedrate, td3.baseamt, " +
            "td3.termamt, td3.dealt, td3.workflow, td3.stream, td3.maker_org, td3.tenor, td3.fixingdate, td3.sdaccount, td3.sduser, td3.pbaccount, td3.primebroker,"
            + "td3.baseamt as baseamt,td3.rate as rate, " +
            "td3.assettype as \"Asset Type\","+
            "td3.externaldealid as \"External Deal ID\","+
            "td3.makerorderid as \"External Order ID\","+
            "td3.cptyborg as \"Counter Party(FI)\","+
            "td3.servername as Server,"+
            "td3.fresherquoteused as \"Fresher Quote Used\","+
            "td3.nextratercvd as \"Next Rate Received\","+
            "td3.stdrejectreason as \"Standard Reason\","+
            "td3.ishistoricalmatch as \"Historic Rate\","+
            "td3.nextrate as \"Next Rate\","+
            "td3.clientTag as \"Client Tag\","+
            "td3.CptyALEI as \"Organization LEI\","+
            "td3.upi as UPI,"+
            "td3.usi as USI ,"+
            "td3.portfoliotid as \"Portfolio ID\","+
            "td3.cptyblei as \"Provider LEI\","+
            "td3.tradeclassification as Classification,"+
            "td3.nettradeiD as \"Net Trade ID\","+
            "O.channel as \"Channel\","+
            "O.ordertype as \"Order Type\","+
            "O.dealer as \"Dealer\" "+
            "from TRADE_MASTER TD3, ORDER_MASTER O"
            + " where " +
            " TD3.TRADEID = ? AND TD3.ORDERID = ? AND  TD3.ORDERID = O.ORDERID) t";

    private static final String TRADES_AS_JSON_WITH_EPA = "select array_to_json(array_agg(row_to_json(t)))" +
            "    from (select td3.stream, td3.orderid as orderid, td3.org, baseAmt, termAmt, td3.usdAmt, exectime as exectimestamp, "
            + "td3.status as TRADESTATUS, td3.ccypair,  td3.mkrtkr, td3.type as type, td3.buysell as buysell, td3.tradeid as tradeid, td3.cpty as cpty, td3.stream, " +
            "td3.spotrate SPOTRATE, td3.cpty COUNTERPARTY, td3.cptyaccount COUNTERPARTYACCOUNT, td3.dealer DEALER, td3.origcpty ORIGINATINGCPTY, td3.origcptyusr ORIGINATINGCPTYUSER," +
            " td3.trdcomment TRADECOMMENT, td3.virtualserver, td3.valuedate, td3.tradedate, td3.takerrefid, td3.makerrefid, td3.matchedrate, td3.baseamt, " +
            "td3.termamt, td3.dealt, td3.workflow, td3.stream, td3.maker_org, td3.tenor, td3.fixingdate, td3.sdaccount, td3.sduser, td3.pbaccount, td3.primebroker,"
            + "td3.baseamt as baseamt,td3.rate as rate, " +
            "td3.assettype as \"Asset Type\","+
            "td3.externaldealid as \"External Deal ID\","+
            "td3.makerorderid as \"External Order ID\","+
            "td3.cptyborg as \"Counter Party(FI)\","+
            "td3.servername as Server,"+
            "td3.fresherquoteused as \"Fresher Quote Used\","+
            "td3.nextratercvd as \"Next Rate Received\","+
            "td3.stdrejectreason as \"Standard Reason\","+
            "td3.ishistoricalmatch as \"Historic Rate\","+
            "td3.nextrate as \"Next Rate\","+
            "td3.clientTag as \"Client Tag\","+
            "td3.CptyALEI as \"Organization LEI\","+
            "td3.upi as UPI,"+
            "td3.usi as USI ,"+
            "td3.portfoliotid as \"Portfolio ID\","+
            "td3.cptyblei as \"Provider LEI\","+
            "td3.tradeclassification as Classification,"+
            "td3.nettradeiD as \"Net Trade ID\","+
            "O.channel as \"Channel\","+
            "O.ordertype as \"Order Type\","+
            "O.dealer as \"Dealer\", "+
            "tradeEpa.tradeid as \"epaTradeid\", tradeEpa.ChangeInPrice  , tradeEpa.CPriceImpr, tradeEpa.PPriceImpr, tradeEpa.NumberOfEventsMissed, tradeEpa.RateEventLifeTime_ms, tradeEpa.RateValueLifeTime_ms, tradeEpa.RateBucket, tradeEpa.RateStatus,"+
            "tradeEpa.resiliencyFactor, tradeEpa.isResilient, tradeEpa.UpdatesCountAtSameMs, tradeEpa.edgeTradingMarginMs, tradeEpa.staleRateMarginMs, tradeEpa.isEdgeTrading_50, tradeEpa.isEdgeTrading_10,"+
            "tradeEpa.CWin2Secs, tradeEpa.CWin4Secs, tradeEpa.QuotedSpread, tradeEpa.EffectiveSpread, tradeEpa.SpreadAfter2Secs, tradeEpa.SpreadAfter4Secs, tradeEpa.SpreadAfter10Secs, tradeEpa.SpreadAfter20Secs,"+
            "tradeEpa.SpreadAfter30Secs, tradeEpa.SpreadAfter60Secs, tradeEpa.TradeRateHitCount, tradeEpa.TradeThrough, tradeEpa.TradeRateGUID, tradeEpa.EMSRateGUID, tradeEpa.isEMSEPAGUIDSame, tradeEpa.EPAErrorCode,"+
            "tradeEpa.IsRetailTrade, tradeEpa.EMS_EPARateArrivalCode, tradeEpa.ResponseTime, tradeEpa.PriceDiscoveryTime, tradeEpa.InsideSpread, tradeEpa.IsMktSnapParsed, tradeEpa.IsAuditLogParsed, tradeEpa.costUSD,"+
            "tradeEpa.QuotedSpreadPIP, tradeEpa.EffectiveSpreadPIP, tradeEpa.SpreadAfter2SecsPIP, tradeEpa.SpreadAfter4SecsPIP, tradeEpa.SpreadAfter10SecsPIP, tradeEpa.SpreadAfter20SecsPIP, tradeEpa.SpreadAfter30SecsPIP,"+
            "tradeEpa.SpreadAfter60SecsPIP, tradeEpa.InsideSpreadCostUSD, tradeEpa.InsideSpreadPIP, tradeEpa.TradeMatchRound, tradeEpa.NextBestPrice, tradeEpa.NextBestPrvdr, tradeEpa.SlippageWRTNextBestPrice, tradeEpa.NextBestPriceMissedByMS,"+
            "tradeEpa.TimeElapsedSinceEMSRate, tradeEpa.TimeElapsedSincePrevRate, tradeEpa.EventsMissedAfterEMSRate, tradeEpa.PostEMSRateMissedEventsAt,"+
            "tradeEpa.EMSMatchingDuration, tradeEpa.ServerAcceptanceDuration, tradeEpa.ServerResponseDuration, tradeEpa.TradeSizeBucket,"+
            "tradeEpa.RatesRelatedErrorCode, tradeEpa.SnapshotRelatedErrorCode, tradeEpa.RateAuditRelatedErrorCode, tradeEpa.TierAcceptancePattern,tradeEpa.isTierAcceptanceConsistent,"+
            "tradeEpa.TakerIsBroker, tradeEpa.MakerIsBroker, tradeEpa.TradesCount2SecPast,"+
            "tradeEpa.TradesCount250MsPast, tradeEpa.TradesCountWithLastQuote, tradeEpa.TradesVolume2SecPast,"+
            "tradeEpa.TradesVolumeWithLastQuote, tradeEpa.LiquidityWithLastQuote, tradeEpa.FxBAtStartOfSec,"+
            "tradeEpa.FxBAtStartOfSecSpread, tradeEpa.FxBAtStartOfSecSpreadPIP, tradeEpa.FxBAtEndOfSec,"+
            "tradeEpa.FxBAtEndOfSecSpread, tradeEpa.FxBAtEndOfSecSpreadPIP "+

            " from TRADE_MASTER TD3, internal_trade_profile_master tradeEpa, ORDER_MASTER O "
            + " where " +
            "tradeEpa.TRADEID = ? AND tradeEpa.runid=? AND tradeEpa.ORDERID = ?AND TD3.TRADEID = ? AND TD3.ORDERID = ? AND  TD3.ORDERID = O.ORDERID) t";


	private static String maxRunIDQuery="select max(runid) from internal_trade_profile_master tradeEpa where TRADEID = ?";

    // query for returning top n cps, currently 5,
    private static String topStatsQuery = "select array_to_json(array_agg(row_to_json(t))) from (select type,sum(total),sum(totalamt) from trade_type_statistics group by type) t";

    private static String topTypeQuery = "select array_to_json(array_agg(row_to_json(t))) from (select status,sum(total),sum(sum) from trade_status_statistics group by status) t";

    private static String topnCcyPairs = "select array_to_json(array_agg(row_to_json(t))) from (SELECT  ccypair,sum(total) numTrades,sum(sum) usdAmt, sum(sum)/sum(total) avgTrdSize" +
            "  from trade_ccypair_statistics  group by ccypair order by usdamt desc limit 5) t";

    private static String topnOrgs = "select array_to_json(array_agg(row_to_json(t))) from ( SELECT  org ,sum(total) numTrades,sum(sum) usdAmt, sum(sum)/sum(total) avgTrdSize  " +
            "from trade_ccypair_statistics  group by org order by usdamt desc limit 5) t";


    public String getTradeDetails (String tradeID,String orderID)	{

        String jsonString=" ";
        Timestamp runId=null;

        Connection connection = null;
        try
        {
            connection = basicDataSource.getConnection();
            PreparedStatement runIdStatement = connection.prepareStatement(maxRunIDQuery  );
            runIdStatement.setString(1,tradeID);
            ResultSet runIDResult=runIdStatement.executeQuery();
            while (runIDResult.next())
            {
                runId = runIDResult.getTimestamp( 1 );
            }
            runIDResult.close();
            if (runId == null)
            {
                PreparedStatement jsonQueryWithEpa = connection.prepareStatement( TRADES_AS_JSON );
                jsonQueryWithEpa.setString( 1, tradeID );
                jsonQueryWithEpa.setString( 2, orderID );
                ResultSet result = jsonQueryWithEpa.executeQuery();
                while ( result.next() )
                {
                    jsonString = result.getString( 1 );
                }
                result.close();
            }
            else
            {
                PreparedStatement jsonQueryWithEpa = connection.prepareStatement( TRADES_AS_JSON_WITH_EPA );
                jsonQueryWithEpa.setString( 1, tradeID );
                jsonQueryWithEpa.setTimestamp( 2, runId );
                jsonQueryWithEpa.setString( 3, orderID );
                jsonQueryWithEpa.setString( 4, tradeID );
                jsonQueryWithEpa.setString( 5, orderID );
                ResultSet result = jsonQueryWithEpa.executeQuery();
                while ( result.next() )
                {
                    jsonString = result.getString( 1 );
                }
                result.close();
            }
        }
        catch ( Exception ex )
        {
            log.error( "SQL Exception occured while trying to get JSON for trade ID: "+tradeID,ex );
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch ( SQLException e )
                {
                    log.error( "SQL Exception occured while trying to close connection: "+tradeID,e );
                }
            }
        }
        return jsonString;
       /* Map parms = new HashMap<>();
		parms.put("TRADEID", tradeID);
		List <Trade> tradeList = (List<Trade>) jdbcTemplate.query(TRADES_BY_TRADEID, parms, new BeanPropertyRowMapper <> (Trade.class));
		return tradeList;*/
	}

    public String getTradeStatsStatistics (String timerange, String org )
    {
        Connection connection = null;
        //StringBuilder jsonSB = new StringBuilder();
        String jsonString=" ";


        try
        {
            connection = basicDataSource.getConnection();
            PreparedStatement topCcyPairsStatement = connection.prepareStatement( topStatsQuery );
            ResultSet result = topCcyPairsStatement.executeQuery();
            while ( result.next() )
            {
                jsonString = result.getString( 1 );
            }
        }
        catch ( Exception ex )
        {
            log.error( "SQL Exception occured while trying to get JSON for timerange: "+timerange,ex );
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch ( SQLException e )
                {
                    log.error( "SQL Exception occured while trying to close connection: "+timerange,e );
                }
            }
        }
        return jsonString;
    }

    public String getTopnCCyPairs( String timerange, String org )
    {
        Connection connection = null;
        StringBuilder jsonSB = new StringBuilder();
        String jsonString=" ";
        try
        {
            connection = basicDataSource.getConnection();
            PreparedStatement topCcyPairsStatement = connection.prepareStatement( topnCcyPairs );
            ResultSet result = topCcyPairsStatement.executeQuery();
            while ( result.next() )
            {
                jsonString = result.getString( 1 );
            }

           /* jsonSB.append("{");
            jsonSB.append(QUOTES).append("rows").append(QUOTES).append(COLON);
            jsonSB.append("[");
            DecimalFormat floatFmt = new java.text.DecimalFormat("###,###,###,###,###,##0.00");
            DecimalFormat intFmt = new java.text.DecimalFormat("###,###,###,###,###,##0.##");
            int rowNum = 1;
            while ( result.next() )
            {
                String cp = result.getString( 1 );
                String numTrades = intFmt.format( result.getInt( 2 ) );
                String avgTrdSize = floatFmt.format( result.getDouble( 3 ) );
                String usdAmt =  floatFmt.format( result.getDouble( 4 ) );


                jsonSB.append("{");
                jsonSB.append(QUOTES).append("Col1").append(QUOTES).append(COLON).append(QUOTES).append(cp).append(QUOTES);
                jsonSB.append(COMMA);
                jsonSB.append(QUOTES).append("Col2").append(QUOTES).append(COLON).append(QUOTES).append(numTrades).append(QUOTES);
                jsonSB.append(COMMA);
                jsonSB.append(QUOTES).append("Col3").append(QUOTES).append(COLON).append(QUOTES).append(usdAmt).append(QUOTES);
                jsonSB.append(COMMA);
                jsonSB.append(QUOTES).append("Col4").append( QUOTES ).append(COLON).append(QUOTES).append(avgTrdSize).append(QUOTES);
                jsonSB.append("}");

                if (rowNum < 5)
                {
                    jsonSB.append( COMMA );
                    rowNum ++;
                }
            }
            jsonSB.append("]");
            jsonSB.append("}");*/

            return jsonString;
        }
        catch ( Exception ex )
        {
            log.error( "SQL Exception occured while trying to get JSON for timerange: "+timerange,ex );
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch ( SQLException e )
                {
                    log.error( "SQL Exception occured while trying to close connection: "+timerange,e );
                }
            }
        }
        return jsonSB.toString();
    }

    public String getTopnOrgs( String timerange, String org )
    {
        Connection connection = null;
        StringBuilder jsonSB = new StringBuilder();
        String jsonString=" ";

        try
        {
            connection = basicDataSource.getConnection();
            PreparedStatement topCcyPairsStatement = connection.prepareStatement( topnOrgs );
            ResultSet result = topCcyPairsStatement.executeQuery();
            while ( result.next() )
            {
                jsonString = result.getString( 1 );
            }
           /* jsonSB.append("{");
            jsonSB.append(QUOTES).append("rows").append(QUOTES).append(COLON);
            jsonSB.append("[");
            DecimalFormat floatFmt = new java.text.DecimalFormat("###,###,###,###,###,##0.00");
            DecimalFormat intFmt = new java.text.DecimalFormat("###,###,###,###,###,##0.##");
            int rowNum = 1;
            while ( result.next() )
            {
                String organization = result.getString( 1 );
                String numTrades = intFmt.format( result.getInt( 2 ) );
                String avgTrdSize = floatFmt.format( result.getDouble( 3 ) );
                String usdAmt =  floatFmt.format( result.getDouble( 4 ) );


                jsonSB.append("{");
                jsonSB.append(QUOTES).append("Col1").append(QUOTES).append(COLON).append(QUOTES).append(organization).append(QUOTES);
                jsonSB.append(COMMA);
                jsonSB.append(QUOTES).append("Col2").append(QUOTES).append(COLON).append(QUOTES).append(numTrades).append(QUOTES);
                jsonSB.append(COMMA);
                jsonSB.append(QUOTES).append("Col3").append(QUOTES).append(COLON).append(QUOTES).append(usdAmt).append(QUOTES);
                jsonSB.append(COMMA);
                jsonSB.append(QUOTES).append("Col4").append( QUOTES ).append(COLON).append(QUOTES).append(avgTrdSize).append(QUOTES);
                jsonSB.append("}");

                if (rowNum < 5)
                {
                    jsonSB.append( COMMA );
                    rowNum ++;
                }
            }
            jsonSB.append("]");
            jsonSB.append("}");*/

            return jsonString;
        }
        catch ( Exception ex )
        {
            log.error( "SQL Exception occured while trying to get JSON for timerange: "+timerange,ex );
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch ( SQLException e )
                {
                    log.error( "SQL Exception occured while trying to close connection: "+timerange,e );
                }
            }
        }
        return jsonSB.toString();
    }

    public String getTradeTypeStatistics( String timerange, String org )
    {
        Connection connection = null;
        //StringBuilder jsonSB = new StringBuilder();
        String jsonString=" ";


        try
        {
            connection = basicDataSource.getConnection();
            PreparedStatement topCcyPairsStatement = connection.prepareStatement( topTypeQuery );
            ResultSet result = topCcyPairsStatement.executeQuery();
            while ( result.next() )
            {
                jsonString = result.getString( 1 );
            }
        }
        catch ( Exception ex )
        {
            log.error( "SQL Exception occured while trying to get JSON for timerange: "+timerange,ex );
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch ( SQLException e )
                {
                    log.error( "SQL Exception occured while trying to close connection: "+timerange,e );
                }
            }
        }
        return jsonString;
    }
}

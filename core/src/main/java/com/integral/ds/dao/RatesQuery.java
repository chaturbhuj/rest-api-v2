package com.integral.ds.dao;

public class RatesQuery {

	public static final String RATES_QUERY=
			"select prvdr,strm,ccyp,tmstmp,lvl,status,bid_price,bid_size, ask_price, " +
			"ask_size, guid, quoteid, rteffectivetime, srvr, adptr, value_date from tickdatalp where prvdr = ? and strm = ? and ccyp = ?  and tmstmp > ?" +
			" and tmstmp < ?";
	
	public static final String RATESDS_QUERY=
			"select prvdr as provider,strm as stream, ccypair, extract(EPOCH from TMSTMP)::INT8 * 1000 + extract(MILLISECONDS from TMSTMP) AS TMSTMP ,"
			+ "lvl as tier,status as status, bid_price as bidprice,bid_size as bidSize, ask_price as askPrice, " +
			"ask_size as askSize, guid, quoteid, 0 AS rteffectivetime, srvr as server, adptr as adapter, 0 AS valuedate from tickdata where strm in ( :STREAM ) and ccypair = :CCYP  and tmstmp > :FROMTIME" +
			" and tmstmp < :TOTIME and lvl in ( :TIER )";
	
	public static void main(String[] args) {
		System.out.println(RATES_QUERY);
	}
}
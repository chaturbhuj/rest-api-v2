package com.integral.ds.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.integral.ds.dto.Order;
import com.integral.ds.dto.Trade;

public class OrderAndTradesCallBackHandler implements RowCallbackHandler  {

	BeanPropertyRowMapper<Trade> tradeRowMapper = new BeanPropertyRowMapper<Trade>(Trade.class);
	BeanPropertyRowMapper<Order> orderRowMapper = new BeanPropertyRowMapper<Order>(Order.class);
	List<Order> orderList = new ArrayList<>();
	Order order = null;
	Map <String, Trade> makerTradeRecords = new HashMap<String, Trade>();
	Map <String, Trade> takerTradeRecords = new HashMap<String, Trade>();
	List<Trade> tradeList = new ArrayList<Trade> ();
	int count = 0;
	
	
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		if (count == 0){
			order = orderRowMapper.mapRow(rs, count);
			String buySell = rs.getString("OrderBuySell");
			order.setBuySell(buySell);
			order.setFillRate(rs.getString("FillRate"));
			order.setOrderID(rs.getString("OrderID"));
			if (order.getOrderStatus().equals("F") && order.getFilledPct().longValue() < 100){
				order.setOrderStatus("P");
			}
			
		}
		String tradeID = rs.getString("TradeID");
		if (!(tradeID == null || tradeID.equals("")))	{
			Trade trade = tradeRowMapper.mapRow(rs, count);
			
			String makerStream = rs.getString("Maker_Stream");
			if (makerStream == null || makerStream.equals(""))	{
				trade.setTickCpty("");
				trade.setTickCpty("");
			}
			else	{
				trade.setTickCpty(trade.getORG());
				trade.setTickStream(makerStream);
			}
			
			if (trade.getMKRTKR().equals("Taker")){
				takerTradeRecords.put(trade.getTRADEID(), trade);
			}
			else	{
				makerTradeRecords.put(trade.getTRADEID(), trade);
			}
		}
		count ++;
	}
	
	public void postProcess ()	{
		if (count == 0)
			return;
		orderList.add(order);
		//Set<String> takerTradeIDSet = takerTradeRecords.keySet();
		//Set<String> makerTradeIDSet = makerTradeRecords.keySet();
		Set<String> finalSet = new HashSet<String>();
		finalSet.addAll(takerTradeRecords.keySet());
		finalSet.addAll(makerTradeRecords.keySet());
		
		List<Trade> tradeList = new ArrayList<>();
		order.setTrades(tradeList);
		
		for (String tradeID: finalSet ){
			Trade makerTradeRecord = makerTradeRecords.get(tradeID);
			Trade takerTradeRecord = takerTradeRecords.get(tradeID);
			//If there is a Taker Record and No Maker Record, this means that this is a  Order. Here we 
			//return the Maker Record.
			if (takerTradeRecord == null)	{
				//makerTradeRecord.setTickCpty("N/A");
				//makerTradeRecord.setTickStream("N/A");
				tradeList.add(makerTradeRecord);
				
			}
			else	{
				if (makerTradeRecord == null)	{
					//takerTradeRecord.setTickCpty("N/A");
					//takerTradeRecord.setTickStream("N/A");
				}
				else	{
					//takerTradeRecord.setTickCpty(makerTradeRecord.getORG());
					//takerTradeRecord.setTickStream(makerTradeRecord.getSTREAM());
				}
				tradeList.add(takerTradeRecord);
			}
			
		}
	}
	
	public List<Order> getOrder ()	{
		return orderList;
	}
}

package com.integral.ds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.integral.ds.dto.EMScopeHolder;
import com.integral.ds.dto.Order;
import com.integral.ds.dto.OrderSummary;
import com.integral.ds.dto.Trade;
import com.integral.ds.dto.TradeSummary;
import com.integral.ds.emscope.orderswithtrade.OrdersWithTradesQuerys;

public class OrdersWithTradesDAO {

	private static final Log logger = LogFactory.getLog(OrdersWithTradesDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate = null;
    private final Log log = LogFactory.getLog( OrdersWithTradesDAO.class );
    private BasicDataSource datasource = null;
	private NamedParameterJdbcTemplate getJDBCTemplate (String userGroup)	{
		return jdbcTemplate;
	}

	public OrdersWithTradesDAO (BasicDataSource ds)	{
		jdbcTemplate = new NamedParameterJdbcTemplate(ds);
        datasource=ds;
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Order> getOrderByOrderID (String userGroup, String orderID)	{
		HashMap params = new HashMap();
		params.put("ORDERID", orderID);
		OrderAndTradesCallBackHandler callbackHandler = new OrderAndTradesCallBackHandler();
		logger.info("Starting Query for Order: " + orderID);
		long time = System.currentTimeMillis();
		getJDBCTemplate(userGroup).query(OrdersWithTradesQuerys.Y_GET_SINGLE_ORDER_AND_TRADE, params, callbackHandler);
		long endTime = System.currentTimeMillis();
		logger.info("End Query for Order: " + orderID + " it Took " + (endTime - time) + " ms");
		callbackHandler.postProcess();
		return callbackHandler.getOrder();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<TradeSummary> getTradesSummaryForOrg (String userGroup, String orgName, String ccyPair, String orderType, long minAmount, Date fromTime, Date toTime)	{
		long startTime = Calendar.getInstance().getTimeInMillis();
		HashMap params = new HashMap();
		params.put("CCYPAIR", ccyPair);
		params.put("MINAMOUNT", minAmount);
		params.put("FROM", fromTime);
		params.put("TO", toTime);
		if (orgName.equalsIgnoreCase("ALL"))	{
			params.put("ORG", "%");
		}
		else	{
			params.put("ORG", orgName);
		}
		if (orderType.equals("ALL"))	{
			params.put("ORDERTYPE", "%");
		}
		else	{
			params.put("ORDERTYPE", orderType);
		}
		
		String query = getTradesModifiedQuery(OrdersWithTradesQuerys.Y_TRADES_SUMMARY_QUERY, fromTime);
		List<TradeSummary> tradeSummary =  getJDBCTemplate(userGroup).query(query, params, new BeanPropertyRowMapper(TradeSummary.class));
		long endTime = Calendar.getInstance().getTimeInMillis();
		logger.info("Trade Summary Query Took: " + (endTime - startTime) + "ms");
		return tradeSummary;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<OrderSummary> getOrderSummaryForOrg(String userGroup, String orgName, String ccyPair, String orderType, long minAmount, Date fromTime, Date toTime)	{
		long startTime = Calendar.getInstance().getTimeInMillis();
		HashMap params = new HashMap();
		params.put("CCYPAIR", ccyPair);
		params.put("MINAMOUNT", minAmount);
		params.put("FROM", fromTime);
		params.put("TO", toTime);
		if (orgName.equalsIgnoreCase("ALL"))	{
			params.put("ORG", "%");
		}
		else	{
			params.put("ORG", orgName);
		}
		if (orderType.equals("ALL"))	{
			params.put("ORDERTYPE", "%");
		}
		else	{
			params.put("ORDERTYPE", orderType);
		}
		List<OrderSummary> orderSummaryList =  getJDBCTemplate(userGroup).query(OrdersWithTradesQuerys.Y_ORDERS_SUMMARY_QUERY, params, new BeanPropertyRowMapper(OrderSummary.class));
		long endTime = Calendar.getInstance().getTimeInMillis();
		logger.info("Order Summary Query Took: " + (endTime - startTime) + "ms");
		return orderSummaryList;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public EMScopeHolder getOrderAndTradeDetailsForOrg (String userGroup, String orgName, String ccyPair, String orderType, long minAmount, Date fromTime, Date toTime)	{
		long startTime = Calendar.getInstance().getTimeInMillis();
		HashMap params = new HashMap();
		params.put("CCYPAIR", ccyPair);
		params.put("MINAMOUNT", minAmount);
		params.put("FROM", fromTime);
		params.put("TO", toTime);
		if (orgName.equalsIgnoreCase("ALL"))	{
			params.put("ORG", "%");
		}
		else	{
			params.put("ORG", orgName);
		}
		if (orderType.equals("ALL"))	{
			params.put("ORDERTYPE", "%");
		}
		else	{
			params.put("ORDERTYPE", orderType);
		}
		
		String query = getTradesModifiedQuery(OrdersWithTradesQuerys.GET_FULL_ORDER_AND_TRADE, fromTime);
		EMScopeCallBackHandler callBackHandler = new EMScopeCallBackHandler();
		getJDBCTemplate(userGroup).query(query, params, callBackHandler);
		callBackHandler.postProcess();
		long endTime = Calendar.getInstance().getTimeInMillis();
		logger.info("Order and Trade Details Query Took: " + (endTime - startTime) + "ms");
		return callBackHandler.getResults();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
    public List<TradeSummary> getTradesInRange(String orgName, String newccyPair, String orderType, long minAmount, Date fromTime, Date toTime) {
        String query = "select  T.TRADEID AS TRADEID, ExecTime as EXECTIMESTAMP, T.rate as Rate, ORDERID, T.baseamt as baseamt, t.ccypair as CcyPair, t.status as Status from {trades_table_name} t where T.EXECTIME > :FROM and T.EXECTIME < :TO and T.CCYPAIR = :CCYPAIR and T.CPTY = :ORG;";
        String modifiedQuery = getTradesModifiedQuery(query, fromTime);
        

        HashMap params = new HashMap();
        params.put("ORG", orgName);
        params.put("CCYPAIR", newccyPair);
        params.put("ORDERTYPE", orderType);
        params.put("MINAMOUNT", minAmount);
        params.put("FROM", fromTime);
        params.put("TO", toTime);
        //params.put("TYPE", "Maker");

        logger.info("Running TradesInRange query " + query);
        logger.info("Running TradesInRange parameters org " + orgName + " ccypair " + newccyPair + " order type " + orderType
            + " minamount " + minAmount + " from data " + fromTime + " to date " + toTime);

        List<TradeSummary> tradeSummary =  getJDBCTemplate("").query(modifiedQuery, params, new BeanPropertyRowMapper(TradeSummary.class));

        logger.info("Trade summary list size " + tradeSummary.size());
        return tradeSummary;
    }

	@SuppressWarnings({ "rawtypes", "unchecked" })
    public List<OrderSummary> getOrdersForTrades(String userGroup,List<String> orderIds) {
        String query = "select created as createdTimestamp, O.ORDERID ORDERID, O.ORDERRATE AS ORDERRATE, O.ORDERAMT AS ORDERAMT from ORDER_MASTER O where O.orderid in (:IDS)";
        HashMap params = new HashMap();

        params.put("IDS", orderIds);

        logger.info("Get orders for trades " + query);

        if(orderIds.size() == 0) {
            return Collections.EMPTY_LIST;
        }

        List<OrderSummary> orderSummaryList =  getJDBCTemplate(userGroup).query(query, params, new BeanPropertyRowMapper(OrderSummary.class));
        logger.info("Size of order summary list returned " + orderSummaryList.size());
        return orderSummaryList;
    }

	@SuppressWarnings({ "rawtypes", "unchecked" })
    public EMScopeHolder getTradeAndOrdersDetailsForOrg(String userGroup, String orgName, String newccyPair, String orderType, long minAmount, Date fromTime, Date toTime) {
        //String query = "select CREATED,O.CCYPAIR, O.BUYSELL as OrderBuySell, O.STATUS as ORDERSTATUS, O.ORG AS ORG, ORDERTYPE, O.ORDERID, PERSISTENT, CLIENTORDERID, extract(EPOCH from LASTEVENT)::INT8 * 1000 + extract(MILLISECONDS from LASTEVENT) LASTEVENT, O.TENOR, O.ACCOUNT, O.DEALER, O.DEALT, TIMEINFORCE, ORDERRATE, ORDERAMT, FILLEDAMT, FILLEDPERCENT As FILLEDPCT, FILLRATE, PI, ORDERAMTUSD AS OrdAmtUSD, FILLEDAMTUSD FILLAMTUSD, PIPNL AS PPNL,td3.stream as stream, td3.orderid as orderid, baseAmt,termAmt, usdAmt, exectime, TD3.status as tradestatus,mkrtkr, td3.type as type, td3.buysell as buysell, td3.tradeid as tradeid, td3.cpty as cpty, td3.baseamt as baseamt,td3.rate as rate from ORDERS O, TRADES4 TD3 where TD3.ORDERID = O.ORDERID AND TD3.EXECTIME > :FROM AND TD3.EXECTIME < :TO AND TD3.CCYPAIR = :CCYPAIR and O.CCYPAIR = :CCYPAIR and O.ordertype = :ORDERTYPE and O.orderamt > :MINAMOUNT and TD3.mkrtkr = :TYPE and TD3.org = :ORG";
        String query = "select CREATED as createdTimestamp, O.CCYPAIR, (case when O.BUYSELL='S' then 'Buy' else 'Sell' end) as OrderBuySell, O.STATUS as ORDERSTATUS, O.ORG AS ORG, O.ORDERTYPE, O.ORDERID, PERSISTENT, CLIENTORDERID, "
        		+ "extract(EPOCH from LASTEVENT)::INT8 * 1000 + extract(MILLISECONDS from LASTEVENT) LASTEVENT, O.TENOR, O.ACCOUNT, O.DEALER, O.DEALT, TIMEINFORCE, ORDERRATE, ORDERAMT, FILLEDAMT, (o.filledamt/o.orderamt)*100 As FILLEDPCT, "
        		+ "FILLRATE, PI, ORDERAMTUSD AS OrdAmtUSD, FILLEDAMTUSD FILLAMTUSD, PIPNL AS PPNL,td3.stream as stream, td3.orderid as orderid, baseAmt,termAmt, usdAmt, exectime AS EXECTIMESTAMP, Td3.stream as Maker_Stream,"
        		+ " TD3.status as tradestatus,mkrtkr, td3.type as type,(case when td3.buysell='S' then 'B' else 'S' end) as buysell, td3.tradeid as tradeid, td3.cpty as cpty, td3.baseamt "
        		+ "as baseamt,td3.rate as rate from ORDER_MASTER O, {trades_table_name} TD3 where TD3.ORDERID = O.ORDERID AND TD3.EXECTIME > :FROM AND TD3.EXECTIME < :TO "
        		+ "AND TD3.CCYPAIR = :CCYPAIR and O.CCYPAIR = :CCYPAIR and O.ordertype = :ORDERTYPE and O.orderamt > :MINAMOUNT and  TD3.cpty = :ORG";
        
        String modifiedQuery = getTradesModifiedQuery(query, fromTime);
        HashMap params = new HashMap();
        params.put("ORG", orgName);
        params.put("CCYPAIR", newccyPair);
        params.put("ORDERTYPE", orderType);
        params.put("MINAMOUNT", minAmount);
        params.put("FROM", fromTime);
        params.put("TO", toTime);
      //  params.put("TYPE", "Maker");

        logger.info("GetTradeAndOrdersDetailsForOrg query " + modifiedQuery);
        logger.info("Parameters org " + orgName + " ccypair " + newccyPair + " orderType " + orderType + " minAmount " + minAmount
            + " from time " + fromTime + " to Date " + toTime);

        EMScopeCallBackHandler callBackHandler = new EMScopeCallBackHandler();
        getJDBCTemplate(userGroup).query(modifiedQuery, params, callBackHandler);
        callBackHandler.postProcess();
        EMScopeHolder holder = callBackHandler.getResults();

        logger.info("Result Holder "+ holder);

        return holder;
    }
	
	@SuppressWarnings({"unchecked", "rawtypes" })
	public Order getOrderByOrderID (String orderID)	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ORDERID", orderID);
		List<Order> orderList = jdbcTemplate.query(OrdersWithTradesQuerys.ORDERS_BY_ID_QUERY, params, new BeanPropertyRowMapper(Order.class));
	    if (orderList == null || orderList.isEmpty())	{
	    	return null;
	    }
	    return orderList.get(0);
	 }
	
	@SuppressWarnings({"unchecked", "rawtypes" })
	public List<Trade> getTradesByOrderIDAndMonth (String orderID, Date orderCreatedTime)	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ORDERID", orderID);
		
		String query = getTradesModifiedQuery(OrdersWithTradesQuerys.TRADES_BY_ORDER_IDS, orderCreatedTime);
		
		List<Trade> tradeList = jdbcTemplate.query(query, params, new BeanPropertyRowMapper(Trade.class));
		Map<String, Trade> newTradeMap = new HashMap<>();
		for (Trade trade: tradeList){
			newTradeMap.put(trade.getTRADEID(), trade);
		}
		Set<String> tradeIDs = newTradeMap.keySet();
		List newTradeList = new ArrayList<>();
		for (String tradeID : tradeIDs)	{
			newTradeList.add(newTradeMap.get(tradeID));
		}
		
		return newTradeList;
	}

	public List<Trade> getCoveredTrades(String tradeID)	{
		Map <String, Object> params = new HashMap<>();
		params.put("TRADEID", tradeID);
		List<Trade> tradeList = jdbcTemplate.query(getTradesModifiedQuery(OrdersWithTradesQuerys.COVEREDTRADE_QI, null), params, new BeanPropertyRowMapper(Trade.class));
		return tradeList;
	}
	
	public List<Trade> getCoveringTrades(String tradeID)	{
		Map <String, Object> params = new HashMap<>();
		params.put("TRADEID", tradeID);
		List<Trade> tradeList = jdbcTemplate.query(getTradesModifiedQuery(OrdersWithTradesQuerys.COVEREDTRADE_Q2, null), params, new BeanPropertyRowMapper(Trade.class));
		return tradeList;
	}
	
	public String getTradesModifiedQuery (String originalQuery, Date date)	{
		
		/*SimpleDateFormat monthFormatter = new SimpleDateFormat("MMM");
		SimpleDateFormat yearFormatter = new SimpleDateFormat("yyyy");
		String month = monthFormatter.format(date);
		String year = yearFormatter.format(date);
		
		String modifiedQuery = "";
		if (month.equalsIgnoreCase("Mar") && year.equals("2014"))	{
			modifiedQuery = originalQuery.replace("{trades_table_name}", "TRADES_03_2014");
		}
		else if (month.equalsIgnoreCase("Feb") && year.equals("2014"))	{
			modifiedQuery = originalQuery.replace("{trades_table_name}", "TRADES_02_2014");
		}
		else {
			modifiedQuery = originalQuery.replace("{trades_table_name}", "TRADES_TAKER");
		}*/
		String modifiedQuery = originalQuery.replace("{trades_table_name}", "TRADE_MASTER");
		return modifiedQuery;
	}

    public String getExtendedOrderDetailsForOrderID( String orderID )
    {
        String jsonString=" ";
        Connection connection = null;
        try
        {
            connection = datasource.getConnection();
            PreparedStatement jsonQuery=connection.prepareStatement( OrdersWithTradesQuerys.GET_SINGLE_ORDER_JSON_DETAILS );
            jsonQuery.setString(1,orderID);

            ResultSet result=jsonQuery.executeQuery();
            while (result.next())
            {
                jsonString = result.getString( 1 );
            }
        }
        catch ( Exception ex )
        {
            log.error( "SQL Exception occured while trying to get JSON for Order ID: "+orderID,ex );
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch ( SQLException e )
                {
                    log.error( "SQL Exception occured while trying to close connection: "+orderID,e );
                }
            }
        }
        return jsonString;
    }

    public String getMarketSnapShot(String tradeID)
    {
        String jsonString=" ";
        Connection connection = null;
        try
        {
            connection = datasource.getConnection();
            PreparedStatement jsonQuery=connection.prepareStatement( OrdersWithTradesQuerys.MARKETSNAPSHOT_TRADE_Q );
            jsonQuery.setString(1,tradeID);

            ResultSet result=jsonQuery.executeQuery();
            while (result.next())
            {
                jsonString = result.getString( 1 );
            }
        }
        catch ( Exception ex )
        {
            log.error( "SQL Exception occured while trying to get Market Snapshot for Trade ID: "+tradeID,ex );
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch ( SQLException e )
                {
                    log.error( "SQL Exception occured while trying to close connection: "+tradeID,e );
                }
            }
        }
        log.info("Raw Market snapshot for Trade ID : "+tradeID);
        return jsonString;
    }

    public String getRateEvents(String tradeID)
    {
        String jsonString=" ";
        Connection connection = null;
        try
        {
            connection = datasource.getConnection();
            PreparedStatement jsonQuery=connection.prepareStatement( OrdersWithTradesQuerys.RATEEVENTS_TRADE_Q );
            jsonQuery.setString(1,tradeID);

            ResultSet result=jsonQuery.executeQuery();
            while (result.next())
            {
                jsonString = result.getString( 1 );
            }
        }
        catch ( Exception ex )
        {
            log.error( "SQL Exception occured while trying to get Market Snapshot for Trade ID: "+tradeID,ex );
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch ( SQLException e )
                {
                    log.error( "SQL Exception occured while trying to close connection: "+tradeID,e );
                }
            }
        }
        log.info("Raw Market snapshot for Trade ID : "+tradeID);
        return jsonString;
    }

    public String getCoveredOrders(String orderID)
    {
        String jsonString=" ";
        Connection connection = null;
        try
        {
            connection = datasource.getConnection();
            PreparedStatement jsonQuery=connection.prepareStatement( OrdersWithTradesQuerys.COVEREDORDER_Q );
            jsonQuery.setString(1,orderID);

            ResultSet result=jsonQuery.executeQuery();
            while (result.next())
            {
                jsonString = result.getString( 1 );
            }
            result.close();
        }
        catch ( Exception ex )
        {
            log.error( "SQL Exception occured while trying to get Covered Orders for Order ID: "+orderID,ex );
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch ( SQLException e )
                {
                    log.error( "SQL Exception occured while trying to close connection: " + orderID, e );
                }
            }
        }
        return jsonString;
    }

    public String getOriginatingOrder(String orderID)
    {
        String jsonString=" ";
        Connection connection = null;
        try
        {
            connection = datasource.getConnection();
            PreparedStatement jsonQuery=connection.prepareStatement( OrdersWithTradesQuerys.ORIGINATINORDER_Q );
            jsonQuery.setString(1,orderID);
            jsonQuery.setString(2,orderID);

            ResultSet result=jsonQuery.executeQuery();
            while (result.next())
            {
                jsonString = result.getString( 1 );
            }
            result.close();
        }
        catch ( Exception ex )
        {
            log.error( "SQL Exception occured while trying to get Originating Order for order ID: "+orderID,ex );
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch ( SQLException e )
                {
                    log.error( "SQL Exception occured while trying to close connection: "+orderID,e );
                }
            }
        }
        return jsonString;
    }

    public String getCoveringOrders(String orderID)
    {
        String jsonString=" ";
        Connection connection = null;
        try
        {
            connection = datasource.getConnection();
            PreparedStatement jsonQuery=connection.prepareStatement( OrdersWithTradesQuerys.COVERINGORDER_Q );
            jsonQuery.setString(1,orderID);
            jsonQuery.setString(2,orderID);

            ResultSet result=jsonQuery.executeQuery();
            while (result.next())
            {
                jsonString = result.getString( 1 );
            }
            result.close();
        }
        catch ( Exception ex )
        {
            log.error( "SQL Exception occured while trying to get Covering Orders for Order ID : "+orderID,ex );
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch ( SQLException e )
                {
                    log.error( "SQL Exception occured while trying to close connection: "+orderID,e );
                }
            }
        }
        return jsonString;
    }

    public String getMarketSnapShotForOrder( String orderID )
    {
        String jsonString=" ";
        Connection connection = null;
        try
        {
            connection = datasource.getConnection();
            PreparedStatement jsonQuery=connection.prepareStatement( OrdersWithTradesQuerys.MARKETSNAPSHOT_ORDER_Q );
            jsonQuery.setString(1,orderID);

            ResultSet result=jsonQuery.executeQuery();
            while (result.next())
            {
                Date created = result.getDate( 1 );
                jsonString = created.toString()+"~"+result.getString( 2 );
            }
        }
        catch ( Exception ex )
        {
            log.error( "SQL Exception occured while trying to get Market Snapshot for order ID: "+orderID,ex );
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch ( SQLException e )
                {
                    log.error( "SQL Exception occured while trying to close connection: "+orderID,e );
                }
            }
        }
        log.info("Raw Market snapshot for order ID : "+orderID);
        return jsonString;
    }

    public List<String> getTradeIdsForOrder(String orderId) {
        List<String> result = new ArrayList<String>();
        Connection connection = null;
        try {
            connection = datasource.getConnection();
            PreparedStatement tradeIdFetcher = connection.prepareStatement(OrdersWithTradesQuerys.TRADES_FOR_ORDER_Q);
            tradeIdFetcher.setString(1,orderId);
            ResultSet resultSet = tradeIdFetcher.executeQuery();

            while (resultSet.next()) {
                String tradeId = resultSet.getString(1);
                result.add(tradeId);
            }
        } catch (Exception e) {
            log.error("Exception while fetching trade ids for order , order id " + orderId ,e);
        } finally {
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    log.error("Exception while closing the connection object.",e);
                }
            }

        }
        return result;
    }

    public List<String> getTradeEventsForOrder(String orderId) {
        List<String> result = new ArrayList<String>();
        Connection connection = null;
        try {
            connection = datasource.getConnection();
            PreparedStatement tradeIdFetcher = connection.prepareStatement(OrdersWithTradesQuerys.TRADE_EVENT_Q);
            tradeIdFetcher.setString(1,orderId);
            ResultSet resultSet = tradeIdFetcher.executeQuery();

            // build a map of trade id with the  JSON String
            // and then wire it out
            Map<String,String> jsonMap = new HashMap<String,String>();

            while (resultSet.next()) {
                String tradeId = resultSet.getString(1);


                /*String status =  resultSet.getString(3);
                String rateLog = resultSet.getString(2);
                String rate= String.valueOf( resultSet.getDouble( 4 ) );
                String rateEvent="";

                int idx2 = rateLog.indexOf( "RateAggregatedByServer" );
                if (idx2 >0)
                {
                    int idx3 = rateLog.indexOf( "|", idx2 );
                    rateEvent = rateLog.substring( idx2, idx3 );
                    addEventToResult( result, tradeId, rate, rateEvent );
                }

                int idx = rateLog.indexOf( "TradeRequestSentToProvider" );
                if (idx >0 )
                {
                    int idx1 = rateLog.indexOf( "|", idx );
                    rateEvent = rateLog.substring( idx, idx1 );
                    addEventToResult( result, tradeId, rate, rateEvent );
                }

                if (status.equalsIgnoreCase( "C" ))
                {
                    int idx4 = rateLog.indexOf( "TradeVerifiedReceivedFromProvider" );
                    int idx5=rateLog.indexOf( "|",idx4);
                    rateEvent=rateLog.substring( idx4, idx5);
                    addEventToResult( result, tradeId, rate, rateEvent );
                }
                else if (status.equalsIgnoreCase( "R" ))
                {
                    int idx6 = rateLog.indexOf( "TradeRejectReceivedFromProvider" );
                    int idx7=rateLog.indexOf( "|",idx6);
                    rateEvent=rateLog.substring( idx6, idx7);
                    addEventToResult( result, tradeId, rate, rateEvent );
                }
               *//* else
                {*//*

               // }*/



            }
        } catch (Exception e) {
            log.error("Exception while fetching trade ids for order , order id " + orderId ,e);
        } finally {
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    log.error("Exception while closing the connection object.",e);
                }
            }

        }
        return result;
    }


    public List<String> getTradeIDSForOrder(String orderId) {
        List<String> result = new ArrayList<String>();
        Connection connection = null;
        try {
            connection = datasource.getConnection();
            PreparedStatement tradeIdFetcher = connection.prepareStatement(OrdersWithTradesQuerys.TRADE_EVENT_Q);
            tradeIdFetcher.setString(1,orderId);
            ResultSet resultSet = tradeIdFetcher.executeQuery();

            while (resultSet.next()) {
                String tradeId = resultSet.getString(1);
                result.add(tradeId);
            }
        } catch (Exception e) {
            log.error("Exception while fetching trade ids for order , order id " + orderId ,e);
        } finally {
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    log.error("Exception while closing the connection object.",e);
                }
            }

        }
        return result;
    }

    public String getOrderCollection( String orderQuery )
    {
        String jsonString=" ";
        Connection connection = null;
        try
        {
            connection = datasource.getConnection();
            PreparedStatement jsonQuery=connection.prepareStatement( orderQuery);


            ResultSet result=jsonQuery.executeQuery();
            while (result.next())
            {
                jsonString = result.getString( 1 );
            }
        }
        catch ( Exception ex )
        {
            log.error( "SQL Exception occured while trying to get JSON for order query: "+orderQuery,ex );
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch ( SQLException e )
                {
                    log.error( "SQL Exception occured while trying to close connection: "+orderQuery,e );
                }
            }
        }
        return jsonString;
    }
    private void addEventToResult( List<String> result, String tradeId, String rate, String rateEvent )
    {
        String [] toks = rateEvent.split( "~" );
        result.add(tradeId+":"+toks[1]+":"+rate+":"+toks[0]);
    }

    public int getRowCount( String rowCount )
    {
        int rowCountRes=0;
        Connection connection = null;
        try
        {
            connection = datasource.getConnection();
            PreparedStatement jsonQuery=connection.prepareStatement( rowCount);


            ResultSet result=jsonQuery.executeQuery();
            while (result.next())
            {
                rowCountRes = result.getInt( 1 );
            }
        }
        catch ( Exception ex )
        {
            log.error( "SQL Exception occured while trying to get JSON for order query: "+rowCount,ex );
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch ( SQLException e )
                {
                    log.error( "SQL Exception occured while trying to close connection: "+rowCount,e );
                }
            }
        }
        return rowCountRes;
    }
}

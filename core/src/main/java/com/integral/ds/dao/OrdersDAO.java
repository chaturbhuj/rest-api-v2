package com.integral.ds.dao;

import com.integral.ds.dto.OrderInfo;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * OrdersDao for finding orders from order_master table.
 *
 * @author Rahul Bhattacharjee
 */
public class OrdersDAO {

    public static String GET_ORDER_QUERY = "select ORDERID, COVEREDORDERID from ORDER_MASTER WHERE ORDERID = :orderid";
    public static String GET_COVERING_ORDER_QUERY = "select ORDERID, COVEREDORDERID from ORDER_MASTER WHERE COVEREDORDERID = :orderid";

    private NamedParameterJdbcTemplate jdbcTemplate = null;

    public OrdersDAO(DataSource dataSource)	{
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public OrderInfo getOrder(String orderId){
        Map<String, String> parameters = new HashMap<>();
        parameters.put("orderid",orderId);
        List <OrderInfo> orderInfoList = jdbcTemplate.query(GET_ORDER_QUERY, parameters, new OrderRowMapper());
        if(orderInfoList != null && !orderInfoList.isEmpty()) {
            return orderInfoList.get(0);
        }
        return null;
    }

    public OrderInfo getParentOrder(String orderId) {
        OrderInfo order = getOrder(orderId);
        if(order == null) {
            return null;
        }
        String coveredOrderId = order.getCoveredOrderId();
        if(coveredOrderId != null || !orderId.equalsIgnoreCase(coveredOrderId)) {
            return getOrder(coveredOrderId);
        }
        return null;
    }

    public List<OrderInfo> getCoveringOrders(String orderId) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("orderid",orderId);
        List <OrderInfo> orderInfoList = jdbcTemplate.query(GET_COVERING_ORDER_QUERY, parameters, new OrderRowMapper());
        return orderInfoList;
    }
}

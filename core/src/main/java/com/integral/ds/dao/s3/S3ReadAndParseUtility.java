package com.integral.ds.dao.s3;

import com.integral.ds.s3.RecordTransformer;
import com.integral.ds.s3.S3StreamReader;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * S3 file read and parse utility.
 *
 * @author Rahul Bhattacharjee
 */
public class S3ReadAndParseUtility {

    private static final Logger LOGGER = Logger.getLogger(S3ReadAndParseUtility.class);

    /**
     * Utility method to parse a file in S3 and parse it using the provided parser and return a list of parsed objects.
     * Blank line from the file are ignored.
     *
     * @param bucket - Bucket where the file is located.
     * @param key - Key against which the file is stored in S3.
     * @param parser - Implementation of ContentParser SPI , which would be used for parsing the content of the S3 file.
     * @param <T>
     * @return - Returns a list of object of type T.
     */
    public static <T> List<T> downloadAndParseS3File(String bucket,String key ,RecordTransformer<T> parser) {
        List<T> result = new ArrayList<T>();
        S3StreamReader streamReader = new S3StreamReader();
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            inputStream = streamReader.getInputStream(bucket,key);
            if(inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                while(line != null) {
                    line = bufferedReader.readLine();
                    if(StringUtils.isNotBlank(line)) {
                        T parsedValue = parser.transform(line);
                        if(parsedValue != null) {
                            result.add(parsedValue);
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception while fetching/parsing file from S3.Filename " + key);
        } finally {
            close(bufferedReader,inputStream);
        }
        return result;
    }

    private static void close(BufferedReader bufferedReader,InputStream inputStream) {
        try {
            if(bufferedReader != null) {
                bufferedReader.close();
            }
            if(inputStream != null) {
                inputStream.close();
            }
        } catch (Exception e){
            LOGGER.error("Exception while closing s3 stream.",e);
        }
    }
}

package com.integral.ds.dao;

import com.integral.ds.dto.OrderInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Row mapper for converting JDBC row to java pojo/bean.
 *
 * @author Rahul Bhattacharjee
 */
public class OrderRowMapper implements RowMapper<OrderInfo> {

    @Override
    public OrderInfo mapRow(ResultSet resultSet, int index) throws SQLException {
        OrderInfo order = new OrderInfo();
        order.setOrderId(resultSet.getString("orderid"));
        order.setCoveredOrderId(resultSet.getString("coveredorderid"));
        return order;
    }
}

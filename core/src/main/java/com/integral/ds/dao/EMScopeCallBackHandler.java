package com.integral.ds.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.integral.ds.dto.EMScopeHolder;
import com.integral.ds.dto.Order;
import com.integral.ds.dto.Trade;

public class EMScopeCallBackHandler implements RowCallbackHandler {

	private static final Logger logger = Logger.getLogger(EMScopeCallBackHandler.class);
	
	private List<Order> orderList = new ArrayList<> ();
	private List<Trade> tradeList = new ArrayList<> ();
	private Set<String> orderIDList = new HashSet<>();
	private BeanPropertyRowMapper<Trade> tradeRowMapper = new BeanPropertyRowMapper<Trade>(Trade.class);
	private BeanPropertyRowMapper<Order> orderRowMapper = new BeanPropertyRowMapper<Order>(Order.class);
	private Map <String, Trade> makerTradeRecords = new HashMap<String, Trade>();
	private Map <String, Trade> takerTradeRecords = new HashMap<String, Trade>();
	int count = 0;
	EMScopeHolder emScopeHolder = new EMScopeHolder();
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		try	{
			
			Order order = orderRowMapper.mapRow(rs, count);
			String buySell = rs.getString("OrderBuySell");
			order.setBuySell(buySell); 
			if (! (orderIDList.contains(order.getOrderID())))	{
				orderList.add(order);
				orderIDList.add(order.getOrderID());
			}
			String tradeID = rs.getString("TradeID");
			
			if (!(tradeID == null || tradeID.equals("")))	{
				Trade trade = tradeRowMapper.mapRow(rs, count);
				if (trade.getMKRTKR().equals("Taker")){
					takerTradeRecords.put(trade.getTRADEID(), trade);
					
				}
				else	{
					makerTradeRecords.put(trade.getTRADEID(), trade);
					//trade.setEXECTIME(new BigDecimal(trade.getEXECTIME().longValue() + (8 * 60L * 1000)));
				}
				
				String makerStream = rs.getString("Maker_Stream");
				if (makerStream == null || makerStream.equals(""))	{
					trade.setTickCpty("");
					trade.setTickCpty("");
				}
				else	{
					trade.setTickCpty(trade.getORG());
					trade.setTickStream(makerStream);
				}
				
			}
		}
		catch (Exception exception){
			logger.error("Exception: " + exception.getMessage());
		}
		count ++;
	}
		
	public void postProcess ()	{
		emScopeHolder.setOrderList(orderList);
		emScopeHolder.setTradeList(tradeList);
		Set<String> finalSet = new HashSet<String>();
		finalSet.addAll(takerTradeRecords.keySet());
		finalSet.addAll(makerTradeRecords.keySet());
		
		for (String tradeID: finalSet ){
			Trade makerTradeRecord = makerTradeRecords.get(tradeID);
			Trade takerTradeRecord = takerTradeRecords.get(tradeID);
			//If there is a Taker Record and No Maker Record, this means that this is a " " Order. Here we 
			//return the Maker Record.
			
			if (takerTradeRecord == null)	{
				//makerTradeRecord.setTickCpty("N/A");
				//makerTradeRecord.setTickStream("N/A");
				tradeList.add(makerTradeRecord);
				
			}
			else	{
				if (makerTradeRecord == null)	{
					//takerTradeRecord.setTickCpty("N/A");
					//takerTradeRecord.setTickStream("N/A");
				}
				else	{
					//takerTradeRecord.setTickCpty(makerTradeRecord.getORG());
					//takerTradeRecord.setTickStream(makerTradeRecord.getSTREAM());
				}
				tradeList.add(takerTradeRecord);
			}
			
		}
	}
	
	public EMScopeHolder getResults ()	{
		return emScopeHolder;
	}
}
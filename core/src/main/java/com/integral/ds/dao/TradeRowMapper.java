package com.integral.ds.dao;

import com.integral.ds.dto.TradeInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Trade row mapper.
 *
 * @author Rahul Bhattacharjee
 */
public class TradeRowMapper implements RowMapper<TradeInfo>  {

    @Override
    public TradeInfo mapRow(ResultSet resultSet, int i) throws SQLException {
        TradeInfo result = new TradeInfo();
        result.setMakerOrg(resultSet.getString("maker_org"));
        result.setOrderId(resultSet.getString("orderid"));
        result.setTradeId(resultSet.getString("tradeid"));
        result.setCoveredtradeid(resultSet.getString("coveredtradeid"));
        return result;
    }
}

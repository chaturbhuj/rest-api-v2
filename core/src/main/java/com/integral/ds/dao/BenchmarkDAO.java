package com.integral.ds.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.integral.ds.dto.BenchmarkObject;
import com.integral.ds.emscope.profile.RatesProfileObject;
import com.integral.ds.emscope.profile.RatesProfileQuery;
import com.integral.ds.util.QueryUtil;

public class BenchmarkDAO {

    private static final Logger logger = Logger.getLogger(BenchmarkDAO.class);

	public static final String BENCHMARK_RATE = "SELECT CCYPair as ccyPair, extract(EPOCH from TMSTMP )::INT8 * 1000 as timestamp, "
			+ "TIER as tier, MIDPRICEMEAN as meanMidPrice,  "
			+ "MIDPRICEMEDIAN as medianMidPrice, MIDPRICERANGE as midPriceRange, "
			+ "NUMLP as lpCount, 0 as midPriceMedianLP from BenchmarkRateNew" 
			+ " where TMSTMP >= :FROMTIME and TMSTMP <= :TOTIME and CCYPAIR = :CCYP and TIER = :TIER";
	
	public static final String BENCHMARK_SAMPLE = BENCHMARK_RATE + " AND TMSTMP IN (:TMSTMPLIST)";
	
	

	private NamedParameterJdbcTemplate jdbcTemplate = null;
	private DataSource dataSource = null;
	
	
	public BenchmarkDAO ()	{
		
	}
	
	public BenchmarkDAO (DataSource dataSource)	{
		this.dataSource = dataSource;
		jdbcTemplate = new NamedParameterJdbcTemplate(this.dataSource);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<BenchmarkObject> getBenchmrarkRates(String currencyPair, Date fromTime, Date toTime, int tier)	{
		Map params = new HashMap <> ();
		params.put("FROMTIME", fromTime);
		params.put("TOTIME", toTime);
		params.put("TIER", tier);
		params.put("CCYP", currencyPair);
		long time = System.currentTimeMillis();
		List<BenchmarkObject> listOfBenchmarkDetails = jdbcTemplate.query(BENCHMARK_RATE, params, new BeanPropertyRowMapper(BenchmarkObject.class));
		long endTime = System.currentTimeMillis();
		logger.info("Benchmark Rates Query Took " + (endTime - time) + " ms");
		return listOfBenchmarkDetails;
	}
    
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<BenchmarkObject> getBenchmrarkSampleRates (String currencyPair, Date fromTime, Date toTime, int tier, List<Date> dateList)	{
    	Map params = new HashMap <> ();
    	params.put("FROMTIME", fromTime);
		params.put("TOTIME", toTime);
		params.put("TIER", tier);
		params.put("CCYP", currencyPair);
		params.put("TMSTMPLIST", dateList);
		long time = System.currentTimeMillis();
    	List<BenchmarkObject> listOfBenchmarkDetails = jdbcTemplate.query(BENCHMARK_SAMPLE, params, new BeanPropertyRowMapper(BenchmarkObject.class));
    	long endTime = System.currentTimeMillis();
		logger.info("Benchmark Sample Rates Query Took " + (endTime - time) + " ms");
		return listOfBenchmarkDetails;
	}

    public List<RatesProfileObject> getBenchmrarkDetails(Map<String, Object> params)	{
		List<RatesProfileObject> listOfBenchmarkDetails = jdbcTemplate.query(RatesProfileQuery.RATES_PROFILE, params, new BeanPropertyRowMapper(RatesProfileObject.class));
		return listOfBenchmarkDetails;
	}

    public List<BenchmarkObject> getBenchmarkForDate(Date date)	{
        Map params = new HashMap <> ();
        params.put("TIMESTAMP", date);

        QueryUtil queryUtil = new QueryUtil();
        String query = queryUtil.getNamedQuery("TEST_BENCHMARK_QUERY");

        logger.debug("Executing query : " + query);

        List<BenchmarkObject> listOfBenchmarkDetails = jdbcTemplate.query(query, params, new BeanPropertyRowMapper(BenchmarkObject.class));
        return listOfBenchmarkDetails;
    }

    public static void main(String[] args) {
    	System.out.println(BENCHMARK_RATE + BENCHMARK_SAMPLE);
    } 
}

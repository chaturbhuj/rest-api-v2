package com.integral.ds.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.integral.ds.dto.UserInfo;

public class AuthDAO {
	
	public static String USER_LOGIN_QUERY = "select ID, FIRSTNAME, LASTNAME, PASSWORD, USERROLE from USERINFO";
    public static String USER_DETAILS_FOR_USER = "select ID, FIRSTNAME, LASTNAME, PASSWORD, USERROLE from USERINFO WHERE ID = :username";

	private NamedParameterJdbcTemplate jdbcTemplate = null;
	
	public AuthDAO (DataSource dataSource)	{
		jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	public List<UserInfo> getAllUsers()	{
		Map<String, String> params = new HashMap<>();
		List <UserInfo> userInfoList = jdbcTemplate.query(USER_LOGIN_QUERY, params, new BeanPropertyRowMapper(UserInfo.class));
		return userInfoList;
	}

    public UserInfo getUserInfoForUser(String userName) {
        Map<String, String> params = new HashMap<>();
        params.put("username",userName);
        List <UserInfo> userInfoList = jdbcTemplate.query(USER_DETAILS_FOR_USER, params, new BeanPropertyRowMapper(UserInfo.class));
        if(userInfoList != null && !userInfoList.isEmpty()) {
            return userInfoList.get(0);
        }
        return null;
    }
}


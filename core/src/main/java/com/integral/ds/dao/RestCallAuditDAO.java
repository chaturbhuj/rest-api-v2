package com.integral.ds.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.integral.ds.dto.RestCallAudit;

public class RestCallAuditDAO {

	private static final String INSERT_REST_CALl = "insert into AUDITRESTCALL VALUES (:HOSTNAME, :USERNAME, :RESTCALL, :TIME)";
	
	private NamedParameterJdbcTemplate jdbcTemplate = null;
	
	public RestCallAuditDAO (DataSource dataSource)	{
		jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	public void insertRestCall(RestCallAudit restAudit){
		Map <String, Object> params = new HashMap<>();
		params.put("HOSTNAME", restAudit.getHostName());
		params.put("USERNAME", restAudit.getUserName());
		params.put("RESTCALL", restAudit.getRestCall());
		params.put("TIME", restAudit.getTimeStamp());
		jdbcTemplate.update(INSERT_REST_CALl, params);
	}
}

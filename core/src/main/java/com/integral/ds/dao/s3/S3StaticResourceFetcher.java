package com.integral.ds.dao.s3;

import com.integral.ds.s3.S3StreamReader;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Fetches static resources from S3 files, parse and return appropriate collections.
 *
 * @author Rahul Bhattacharjee
 */
public class S3StaticResourceFetcher {

    private static final Logger LOGGER = Logger.getLogger(S3StaticResourceFetcher.class);

    private static final String BUCKET_NAME = "integral-configuration";
    private static final String ORGANIZATION_FILENAME = "Commons/Organizations.txt";

    private List<String> organizations;

    public void init() {
        loadOrganizations();
    }

    public List<String> getCompleteOrganizationList() {
        return this.organizations;
    }

    private void loadOrganizations() {
        if(organizations == null) {
            organizations = new ArrayList<String>();
            S3StreamReader streamReader = new S3StreamReader();
            InputStream inputStream = null;
            BufferedReader bufferedReader = null;
            try {
                inputStream = streamReader.getInputStream(BUCKET_NAME,ORGANIZATION_FILENAME);
                if(inputStream != null) {
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    String line = "";
                    while(line != null) {
                        line = bufferedReader.readLine();
                        if(StringUtils.isNotBlank(line)) {
                            organizations.add(line);
                        }
                    }
                }
            } catch (Exception e) {
                LOGGER.error("Exception while fetching/parsing file from S3.Filename " + ORGANIZATION_FILENAME);
            } finally {
                close(bufferedReader,inputStream);
            }
            Collections.sort(organizations);
        }
    }

    private void close(BufferedReader bufferedReader,InputStream inputStream) {
        try {
            if(bufferedReader != null) {
                bufferedReader.close();
            }
            if(inputStream != null) {
                inputStream.close();
            }
        } catch (Exception e){
            LOGGER.error("Exception while closing s3 stream.",e);
        }
    }
}

package com.integral.ds.dao.s3;

import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.s3.Filter;
import com.integral.ds.s3.RecordTransformer;
import com.integral.ds.s3.RestRateObjectTransformer;
import com.integral.ds.s3.S3StreamReader;
import com.integral.ds.s3.impl.RateCurrencyPairFilter;
import com.integral.ds.s3.impl.RateObjectFilter;
import com.integral.ds.s3.impl.RatesIterationBreakFilter;
import com.integral.ds.util.DataQueryUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * S3 rates dao. It reads zip files from S3 and then parses those to get the required rates.
 *
 * @author Rahul Bhattacharjee
 */
public class S3FilesRatesDao {

    private static final Logger LOGGER = Logger.getLogger(S3FilesRatesDao.class);

    private S3StreamReader s3StreamReader = new S3StreamReader();

    private static ExecutorService executor = Executors.newCachedThreadPool(new ThreadFactory() {
        @Override
        public Thread newThread(Runnable runnable) {
            Thread daemonThread = new Thread(runnable);
            daemonThread.setDaemon(true);
            return daemonThread;
        }
    });

    public List<RestRatesObject> getRates(String provider, String stream, String ccyPair, Date fromTime, Date endTime) {

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("Rates DAO parameters " + provider + " stream " + stream + " ccypair " + ccyPair
                    + " start " + fromTime + " end " + endTime);
        }

        List<RestRatesObject> ratesObjectList = new ArrayList<RestRatesObject>();
        List<DataQueryUtils.S3FileInfo> listOfS3Files = DataQueryUtils.getS3FileList(fromTime,endTime,provider,stream,ccyPair);

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("File info list " + listOfS3Files);
        }

        for(DataQueryUtils.S3FileInfo fileInfo : listOfS3Files) {
            populateListWithRatesObject(fileInfo,ccyPair,ratesObjectList);
        }
        return ratesObjectList;
    }

    public void populateListWithRatesObject(DataQueryUtils.S3FileInfo fileInfo ,String ccyPair, List<RestRatesObject> ratesObjectList) {
        String filePath = fileInfo.getFilePath();
        Date fromTime = fileInfo.getFromTime();
        Date toTime = fileInfo.getToTime();

        Filter<RestRatesObject> ratesObjectFilter = new RateObjectFilter(fromTime,toTime);
        Filter<RestRatesObject> ratesBreakFilter = new RatesIterationBreakFilter(toTime);
        Filter<RestRatesObject> ccyPairFilter = new RateCurrencyPairFilter(ccyPair);

        RecordTransformer<RestRatesObject> ratesTransformer = new RestRateObjectTransformer();

        InputStream inputStream = null;
        ZipInputStream zipStream = null;
        BufferedReader reader = null;
        BufferedInputStream bufferedInputStream = null;

        try {
            inputStream = s3StreamReader.getInputStream(filePath);
            bufferedInputStream = new BufferedInputStream(inputStream);
            zipStream = new ZipInputStream(bufferedInputStream);

            List<String> filters = getFilterList(fileInfo.getStartMinute(),fileInfo.getEndMinute());
            ZipEntry entry = null;

            while((entry = zipStream.getNextEntry()) != null) {
                if(entryContains(entry.getName(),filters)) {
                    reader = new BufferedReader(new InputStreamReader(zipStream));
                    String record = "";

                    while(record != null) {
                        record = reader.readLine();
                        if(StringUtils.isBlank(record)){
                            continue;
                        }
                        RestRatesObject ratesObject = ratesTransformer.transform(record);
                        if(ratesBreakFilter.shouldFilter(ratesObject)) {
                            break;
                        } else {
                            if(!ccyPairFilter.shouldFilter(ratesObject)) {
                                if(!ratesObjectFilter.shouldFilter(ratesObject)){
                                    ratesObjectList.add(ratesObject);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception while fetching rates objects from S3.",e);
            throw new IllegalArgumentException("Exception while fetching rates from S3.",e);
        } finally {
            submitCloseEvent(inputStream,bufferedInputStream, zipStream,reader);
        }
    }

    private void submitCloseEvent(InputStream inputStream, BufferedInputStream bufferedInputStream, ZipInputStream zipInputStream, BufferedReader reader) {
        executor.submit(new StreamCloseEvent(inputStream,zipInputStream,reader,bufferedInputStream));
    }

    public static List<String> getFilterList(int start , int end) {
        // do collections operation contains.
        List<String> filters = new ArrayList<>();
        for(int i = start ; i <= end ; i++) {
            filters.add("_" + i + ".csv");
        }
        return filters;
    }

    public static boolean entryContains(String entryName , List<String> filters) {
        for(String filter : filters) {
            if(entryName.contains(filter)) {
                return true;
            }
        }
        return false;
    }

    /**
     * A runnable event to close the stream.
     */
    private static class StreamCloseEvent implements Runnable {

        private InputStream inputStream;
        private ZipInputStream zipInputStream;
        private BufferedReader reader;
        private BufferedInputStream bufferedInputStream;

        private StreamCloseEvent(InputStream inputStream,ZipInputStream zipInputStream,BufferedReader reader,
                                 BufferedInputStream bufferedInputStream) {
            this.inputStream = inputStream;
            this.zipInputStream = zipInputStream;
            this.reader = reader;
            this.bufferedInputStream = bufferedInputStream;
        }

        @Override
        public void run() {
            // ok to  eat exception here.
            try {
                if(reader != null) {
                    reader.close();
                }
            }catch (Exception e) {
                LOGGER.error("Exception while closing reader.",e);
            }
            try {
                if(zipInputStream != null) {
                    zipInputStream.close();
                }
            } catch (Exception e) {
                LOGGER.error("Exception while closing zip inputstream",e);
            }
            try {
               if(bufferedInputStream != null) {
                   bufferedInputStream.close();
               }
            } catch (Exception e) {
                LOGGER.error("Exception while closing  buffered inputstream",e);
            }
            try {
                if(inputStream != null) {
                    inputStream.close();
                }
            } catch (Exception e) {
                LOGGER.error("Exception while closing input stream." ,e);
            }
        }
    }
}

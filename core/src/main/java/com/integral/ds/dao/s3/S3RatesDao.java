package com.integral.ds.dao.s3;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.s3.*;
import com.integral.ds.s3.impl.RatesIterationBreakFilter;
import com.integral.ds.s3.impl.RateObjectFilter;
import com.integral.ds.s3.impl.RateRecordTransformer;
import com.integral.ds.util.DataQueryUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Rates Dao using S3 indexed files.
 *
 * @author Rahul Bhattacharjee
 */
public class S3RatesDao {

    private static final Logger LOGGER = Logger.getLogger(S3RatesDao.class);

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    public S3RatesDao() {
    }

    /**
     * Returns the list of RateObject , S3 is the source of the data.
     *
     * @param provider
     * @param stream
     * @param ccyPair
     * @param fromTime
     * @param endTime
     * @return
     */
    public List<RestRatesObject> getRatesFromS3(String provider, String stream, String ccyPair, Date fromTime, Date endTime) {
        List<RestRatesObject> ratesObjectList = new ArrayList<RestRatesObject>();
        Filter<RestRatesObject> ratesObjectFilter = new RateObjectFilter(fromTime,endTime);
        RecordTransformer<RestRatesObject> ratesTransformer = new RestRateObjectTransformer();

        InputStream inputStream = null;
        BufferedReader reader = null;
        try {
            S3StreamReader s3StreamReader = new S3StreamReader();
            String filePath = getS3FilePath(fromTime , provider , stream,ccyPair);

            inputStream = s3StreamReader.getGzipInputStream(filePath);
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String record = "";

            while(record != null) {
                record = reader.readLine();
                if(StringUtils.isBlank(record)){
                    continue;
                }
                RestRatesObject ratesObject = ratesTransformer.transform(record);
                if(!ratesObjectFilter.shouldFilter(ratesObject)){
                    ratesObjectList.add(ratesObject);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception while fetching rates objects from S3.",e);
            throw new IllegalArgumentException("Exception while fetching rates from S3.",e);
        } finally {
            submitCloseEvent(reader,inputStream);
        }
        return ratesObjectList;
    }

    /**
     * Method to read rates from indexed S3 files.
     *
     * @param provider
     * @param stream
     * @param ccyPair
     * @param fromTime
     * @param endTime
     * @return
     */
    public List<RestRatesObject> getRatesFromIndexedS3(String provider, String stream, String ccyPair, Date fromTime, Date endTime) {
        List<RestRatesObject> ratesObjectList = new ArrayList<RestRatesObject>();

        Filter<RestRatesObject> ratesObjectFilter = new RateObjectFilter(fromTime,endTime);
        Filter<RestRatesObject> ratesBreakFilter = new RatesIterationBreakFilter(endTime);

        RecordTransformer<RestRatesObject> ratesTransformer = new RestRateObjectTransformer();

        InputStream inputStream = null;
        BufferedReader reader = null;

        try {
            S3StreamReader s3StreamReader = new S3StreamReader();
            String filePath = getS3FilePath(fromTime , provider , stream ,ccyPair);

            int minute = Integer.parseInt(getMinute(fromTime));
            inputStream = s3StreamReader.getSeekableStreamForMinute(filePath, minute);
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String record = "";

            while(record != null) {
                record = reader.readLine();
                if(StringUtils.isBlank(record)){
                    continue;
                }
                RestRatesObject ratesObject = ratesTransformer.transform(record);
                if(ratesBreakFilter.shouldFilter(ratesObject)) {
                    break;
                } else {
                    if(!ratesObjectFilter.shouldFilter(ratesObject)){
                        ratesObjectList.add(ratesObject);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception while fetching rates objects from S3.",e);
            throw new IllegalArgumentException("Exception while fetching rates from S3.",e);
        } finally {
            submitCloseEvent(reader,inputStream);
        }
        return ratesObjectList;
    }

    public void populateListWithRatesObject(DataQueryUtils.S3FileInfo fileInfo ,List<RestRatesObject> ratesObjectList) {
        String filePath = fileInfo.getFilePath();
        Date fromTime = fileInfo.getFromTime();
        Date toTime = fileInfo.getToTime();
        int startMinute = fileInfo.getStartMinute();

        Filter <RestRatesObject> ratesObjectFilter = new RateObjectFilter(fromTime,toTime);
        Filter<RestRatesObject> ratesBreakFilter = new RatesIterationBreakFilter(toTime);

        RecordTransformer<RestRatesObject> ratesTransformer = new RestRateObjectTransformer();

        InputStream inputStream = null;
        BufferedReader reader = null;
        try {
            S3StreamReader s3StreamReader = new S3StreamReader();
            inputStream = s3StreamReader.getSeekableStreamForMinute(filePath, startMinute);
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String record = "";

            while(record != null) {
                record = reader.readLine();
                if(StringUtils.isBlank(record)){
                    continue;
                }
                RestRatesObject ratesObject = ratesTransformer.transform(record);
                if(ratesBreakFilter.shouldFilter(ratesObject)) {
                    break;
                } else {
                    if(!ratesObjectFilter.shouldFilter(ratesObject)){
                        ratesObjectList.add(ratesObject);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception while fetching rates objects from S3.",e);
            throw new IllegalArgumentException("Exception while fetching rates from S3.",e);
        } finally {
            submitCloseEvent(reader,inputStream);
        }
    }

    public List<RestRatesObject> getRatesFromIndexedS3Boundary(String provider, String stream, String ccyPair, Date fromTime, Date endTime) {
        List<RestRatesObject> ratesObjectList = new ArrayList<RestRatesObject>();
        List<DataQueryUtils.S3FileInfo> listOfS3Files = DataQueryUtils.getS3FileList(fromTime,endTime,provider,stream,ccyPair);

        for(DataQueryUtils.S3FileInfo fileInfo : listOfS3Files) {
            populateListWithRatesObject(fileInfo,ratesObjectList);
        }
        return ratesObjectList;
    }

    /**
     *
     * Creates S3 file location using the parameters.
     *
     * @param fromDate
     * @param provider
     * @param stream
     * @return
     */
    private String getS3FilePath(Date fromDate, String provider , String stream ,String ccyPair) {
        SimpleDateFormat simpleYearFormat = new SimpleDateFormat("y");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("y-MM-dd");
        SimpleDateFormat simpleDateMonthFormat = new SimpleDateFormat("y-MM");
        SimpleDateFormat simpleFormatForFile = new SimpleDateFormat("y-MM-dd-HH");

        String year = simpleYearFormat.format(fromDate);
        String yearMonth = simpleDateMonthFormat.format(fromDate);
        String date = simpleDateFormat.format(fromDate);
        String fileSelectorTail = simpleFormatForFile.format(fromDate);
        String relativePath = "processed/"+ year +"/"+ yearMonth + "/"+ date +"/"+ provider +"/"+ stream
                +"/"+ ccyPair +"/processedLog_"+ fileSelectorTail+".gz";
        return relativePath;
    }

    private String getMinute(Date date) {
        SimpleDateFormat minuteExtractor = new SimpleDateFormat("m");
        return minuteExtractor.format(date);
    }

    private void submitCloseEvent(BufferedReader reader, InputStream inputStream) {
        executorService.submit(new StreamCloseEvent(reader,inputStream));
    }

    /**
     * A runnable event to close the stream.
     */
    private static class StreamCloseEvent implements Runnable {

        private BufferedReader reader;
        private InputStream inputStream;

        private StreamCloseEvent(BufferedReader reader, InputStream inputStream) {
            this.reader = reader;
            this.inputStream = inputStream;
        }

        @Override
        public void run() {
            // ok to  eat exception here.
            try {
                reader.close();
            }catch (Exception e) {
                LOGGER.error("Exception while closing reader.",e);
            }
            try {
                inputStream.close();
            } catch (Exception e) {
                LOGGER.error("Exception while closing input stream." ,e);
            }
        }
    }
}

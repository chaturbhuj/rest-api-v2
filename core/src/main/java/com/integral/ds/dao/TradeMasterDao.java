package com.integral.ds.dao;

import com.integral.ds.dto.TradeInfo;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Trade master dao.
 *
 * @author Rahul Bhattacharjee
 */
public class TradeMasterDao {

    private NamedParameterJdbcTemplate jdbcTemplate = null;

    private static final String GET_TRADE_FOR_TRADE_ID = "select orderid,tradeid,maker_org,coveredtradeid from trade_master where tradeid = :tradeid";
    private static final String GET_TRADE_FOR_ORDER_ID = "select orderid,tradeid,maker_org,coveredtradeid from trade_master where orderid = :orderid";
    private static final String GET_CHILD_TRADES_FOR_TRADE_ID = "select orderid,tradeid,maker_org,coveredtradeid from trade_master where coveredtradeid = :tradeid";

    public TradeMasterDao(DataSource dataSource)	{
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<TradeInfo> getTradesForOrder(String orderId) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("orderid",orderId);
        List <TradeInfo> tradeInfoList = jdbcTemplate.query(GET_TRADE_FOR_ORDER_ID, parameters, new TradeRowMapper());
        return tradeInfoList;
    }

    public TradeInfo getTrade(String tradeId) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("tradeid",tradeId);
        List <TradeInfo> tradeInfoList = jdbcTemplate.query(GET_TRADE_FOR_TRADE_ID, parameters, new TradeRowMapper());
        if(tradeInfoList != null && !tradeInfoList.isEmpty()) {
            return tradeInfoList.get(0);
        }
        return null;
    }

    public TradeInfo getParentTrade(String tradeId) {
        TradeInfo trade = getTrade(tradeId);
        if(trade == null) {
            return null;
        }
        String coveredTradeId = trade.getCoveredtradeid();
        if(coveredTradeId != null || !tradeId.equalsIgnoreCase(coveredTradeId)) {
            return getTrade(coveredTradeId);
        }
        return null;
    }

    public List<TradeInfo> getCoveringTrades(String tradeId) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("tradeid",tradeId);
        List <TradeInfo> tradeInfoList = jdbcTemplate.query(GET_CHILD_TRADES_FOR_TRADE_ID, parameters, new TradeRowMapper());
        return tradeInfoList;
    }

}

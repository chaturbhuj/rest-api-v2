package com.integral.ds.dao;

import com.integral.ds.dto.ProviderStream;

import java.util.List;
import java.util.Properties;

/**
 * @author Rahul Bhattacharjee
 */
public interface ProviderStreamConfigFetcher {

    public static final Object CONFIG_FILENAME_KEY = "CONFIG_FILENAME";

    /**
     * Initializes the instance.
     */
    public void init(Properties config);

    /**
     * Returns the cached list of provider stream instance.
     * @return
     */
    public List<ProviderStream> getProviderStreams();
}

package com.integral.ds.mbean;

/**
 * Interface to expose administrative functionality of emscope.
 *
 * The following web link explains as how to enable JMX in tomcat running in EC2 instance.
 *
 * http://glassonionblog.wordpress.com/2013/04/25/jmx-tomcat-firewall-ec2-jconsole/
 *
 * @author Rahul Bhattacharjee
 */
public interface IEmscopeManagementBean {
    /**
     * Returns if this emscope instance is running in demo mode.
     * @return
     */
    public String isDemoMode();

    /**
     * Sets the demo mode for this instance of emscope.
     *
     * @param mode
     */
    public void setDemoMode(String mode);
}

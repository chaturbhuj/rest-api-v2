package com.integral.ds.mbean.impl;

import com.integral.ds.mbean.IEmscopeManagementBean;
import com.integral.ds.util.PropertyReader;
import org.apache.log4j.Logger;

/**
 * Impl of Emscope management bean.
 *
 * @author Rahul Bhattacharjee
 */
public class EmscopeManagementBean implements IEmscopeManagementBean {

    private static final Logger LOGGER = Logger.getLogger(EmscopeManagementBean.class);

    @Override
    public String isDemoMode() {
        return PropertyReader.getPropertyValue(PropertyReader.KEY_IS_DEMO_MODE);
    }

    @Override
    public void setDemoMode(String mode) {
        boolean value = Boolean.parseBoolean(mode);
        PropertyReader.setProperty(PropertyReader.KEY_IS_DEMO_MODE, Boolean.toString(value));
        LOGGER.debug("Property value changed to " + value);
    }
}

package com.integral.ds.resources;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.integral.ds.emscope.RestServiceRuntimeException;
import com.integral.ds.emscope.displayorder.DisplayOrderRatesAppDataContext;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.model.ErrorObject;
import com.integral.ds.util.RestResponseUtil;

@Path("/displayorderrates")
@Component
@Scope("request")
public class DisplayOrderRatesCollectionResource {

    private static final Logger LOGGER = Logger.getLogger(DisplayOrderRatesCollectionResource.class);
    private static final String UNEXPECTED_ERROR = "Unexpected error in EMScope.";

    @Autowired
	private DisplayOrderRatesAppDataContext displayOrderRatesAppDataContext;

    
	@GET
	@Path("{ccyp}/{time}")
	@Produces(MediaType.TEXT_PLAIN)
	public String findRange(@Context HttpServletRequest request,
			@PathParam("ccyp") String ccyPair, @PathParam("time") long time_) {

        try {
            Date time = new Date(time_);
            Object response = null;

                try {
                    List<RestRatesObject> listOfRates = displayOrderRatesAppDataContext.getDisplayOrderAndBenchmarkRatesByTimeStamp(ccyPair, time);
                    response = listOfRates;
                } catch (Exception e) {
                    LOGGER.error("Exception while serializing rates object list.",e);
                    if(e instanceof RestServiceRuntimeException) {
                        response = ((RestServiceRuntimeException)e).getErrorObject();
                    } else {
                        ErrorObject errorObject = new ErrorObject();
                        errorObject.setType(ErrorObject.ErrorType.error);
                        errorObject.setErrorMessage(UNEXPECTED_ERROR + " Message => " + e.getMessage());
                        response = errorObject;
                    }
                }
            assert response != null;
            return RestResponseUtil.setJsonResponse(request, response);
        }catch (Exception e) {
            LOGGER.error(UNEXPECTED_ERROR,e);
        }
        return UNEXPECTED_ERROR;
	}

	
	@GET
	@Path("at/{ccyp}/{time}")
	@Produces(MediaType.TEXT_PLAIN)
	public String findInterativeRange(@Context HttpServletRequest request,
			@PathParam("ccyp") String ccyPair, @PathParam("time") long time_) {

        try {
            Date time = new Date(time_);
            Object response = null;

                try {
                    List<RestRatesObject> listOfRates = displayOrderRatesAppDataContext.getInteractiveDisplayOrderAndBenchmarkRatesByTimestamp(ccyPair, time);
                    response = listOfRates;
                } catch (Exception e) {
                    LOGGER.error("Exception while serializing rates object list.",e);
                    if(e instanceof RestServiceRuntimeException) {
                        response = ((RestServiceRuntimeException)e).getErrorObject();
                    } else {
                        ErrorObject errorObject = new ErrorObject();
                        errorObject.setType(ErrorObject.ErrorType.error);
                        errorObject.setErrorMessage(UNEXPECTED_ERROR + " Message => " + e.getMessage());
                        response = errorObject;
                    }
                }
            assert response != null;
            return RestResponseUtil.setJsonResponse(request, response);
        }catch (Exception e) {
            LOGGER.error(UNEXPECTED_ERROR,e);
        }
        return UNEXPECTED_ERROR;
	}

}

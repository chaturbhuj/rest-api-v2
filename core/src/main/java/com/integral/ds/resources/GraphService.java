package com.integral.ds.resources;

import com.integral.ds.dto.OrderNode;
import com.integral.ds.emscope.graph.NavigationService;
import com.integral.ds.util.GraphUtil;
import com.integral.ds.util.RestResponseUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.*;

/**
 * Service for graph related functions.
 *
 * ex : - REQ - http://localhost:75/emscope/rest/graph/navigate/580254609
 * RESP - {"orderId":"578908565","childOrderList":[{"orderId":"580254609","childOrderList":[{"orderId":"580404661","childOrderList":[],"youAreHere":false}],
 * "youAreHere":false}],"youAreHere":true}
 *
 * @author Rahul Bhattacharjee
 */
@Path("/graph")
@Component
public class GraphService {

    private  final Log LOGGER = LogFactory.getLog(GraphService.class);

    @Autowired
    private NavigationService NAVIGATION_SERVICE;

    @GET
    @Path("/navigate/{orderid}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getOriginal(@Context HttpServletRequest request,@PathParam("orderid") String orderid) {
        Object response = null;
        try {
            response = NAVIGATION_SERVICE.getCompleteTree(orderid);
        }catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Exception while creating order navigation path.",e);
            response = RestResponseUtil.getErrorObject(e);
        }
        return RestResponseUtil.setJsonResponse(request,response);
    }

    @GET
    @Path("/navigate_static/{orderid}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getStreams(@Context HttpServletRequest request,@PathParam("orderid") String orderid) {
        Map<Integer,OrderNode> metadata = new HashMap<Integer,OrderNode>();

        OrderNode one = new OrderNode();
        one.setId(1);
        one.setOrderId("111");
        List<Integer> linksForOne = new ArrayList<Integer>();
        linksForOne.add(2);
        linksForOne.add(3);
        one.setLinkIds(linksForOne);

        metadata.put(1,one);

        OrderNode two = new OrderNode();
        two.setId(2);
        two.setOrderId("222");

        metadata.put(2,two);

        OrderNode three = new OrderNode();
        three.setYouAreHere(true);
        three.setId(3);
        three.setOrderId("333");
        List<Integer> linksForThree = new ArrayList<Integer>();
        linksForThree.add(4);
        linksForThree.add(5);
        three.setLinkIds(linksForThree);

        metadata.put(3,three);

        OrderNode four = new OrderNode();
        four.setId(4);
        four.setOrderId("444");

        metadata.put(4,four);

        OrderNode five = new OrderNode();
        five.setId(5);
        five.setOrderId("555");

        metadata.put(5,five);

        OrderNode parent = GraphUtil.getOrderObjectGraphFromMap(metadata);
        return RestResponseUtil.setJsonResponse(request,parent);
    }
}



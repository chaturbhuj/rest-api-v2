package com.integral.ds.resources.delete;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.integral.ds.model.data.AppDataContext;
import com.integral.ds.util.ResourceUtils;

/**
 * Created with IntelliJ IDEA.
 * User: vchawla
 * Date: 9/26/13
 * Time: 9:26 PM
 * To change this template use File | Settings | File Templates.
 */
@Path("/currencypairs")
@Component
@Scope("request")
public class CurrencyPairsResource {

    @Autowired
    AppDataContext appDataContext;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject getCurrencyPairs()
    {

        try
        {
            return new JSONObject().
                    put("thisURL", ResourceUtils.getContextUrl()).
                    put("currencyPairs", appDataContext.getCurrencyPairs(null,null));
        }
        catch (JSONException je)
        {
            return null;
        }
    }
}

package com.integral.ds.resources;

import com.integral.ds.dao.s3.S3ProviderStreamConfigFetcherImpl;
import com.integral.ds.dao.s3.S3ReadAndParseUtility;
import com.integral.ds.dao.s3.S3StaticResourceFetcher;
import com.integral.ds.s3.RecordTransformer;
import com.integral.ds.util.RestResponseUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * General purpose services for emscope application.
 *
 * @author Rahul Bhattacharjee
 */
@Path("/framework")
@Component
public class FrameworkService {

    private static final String BUCKET_NAME = "integral-rates";
    private static final String S3_STATIC_CONFIG_FILE_FOR_OC_CURRENCY_PAIRS = "config/OC-CCYPAIRS.txt";
    private static final String S3_DYNAMIC_CONFIG_FILE_FOR_OC_ORIG_ORGS = "config/OC-ORIG-ORG.txt";
    private static final String S3_DYNAMIC_CONFIG_FILE_FOR_OC_TAKER_ORGS = "config/OC-TAKER_ORGS.txt";
    private static final String S3_DYNAMIC_CONFIG_FILE_FOR_OC_DATES = "config/OC-DATES.txt";

    private List<String> OC_CURRENCY_PAIRS = null;
    private List<String> OC_ORIG_ORGS = null;
    private List<String> OC_TAKER_ORGS = null;
    private List<String> OC_DATES = null;

    @Autowired
    private S3ProviderStreamConfigFetcherImpl streamReaderDao;
    @Autowired
    private S3StaticResourceFetcher staticResourceReaderDao;

    //Service to return list of provider-stream pairs.
    @GET
    @Path("/streams")
    @Produces(MediaType.TEXT_PLAIN)
    public String getStreams(@Context HttpServletRequest request) {
        Object response = streamReaderDao.getProviderStreams();
        return RestResponseUtil.setJsonResponse(request, response);
    }

    @GET
    @Path("/organizations")
    @Produces(MediaType.TEXT_PLAIN)
    public void findOrgs(@Context HttpServletRequest request) {

        Object response = staticResourceReaderDao.getCompleteOrganizationList();
        RestResponseUtil.setJsonResponse(request, response);
    }

    @GET
    @Path("/orderdates")
    @Produces(MediaType.TEXT_PLAIN)
    public void findOrderDates(@Context HttpServletRequest request) {

        /*List<String> orderDates = new ArrayList<>(  );
        orderDates.add("8/1/2014");
        orderDates.add("7/30/2014");
        orderDates.add("7/29/2014");
        orderDates.add("7/28/2014") ;
        orderDates.add("7/27/2014") ;
        orderDates.add("7/26/2014") ;
        orderDates.add("7/25/2014") ;
        orderDates.add("7/24/2014") ;
        orderDates.add("7/23/2014") ;
        orderDates.add("7/22/2014") ;
        orderDates.add("7/21/2014") ;
        orderDates.add("7/20/2014") ;
        //  Object response = staticResourceReaderDao.getCompleteOrganizationList();
        RestResponseUtil.setJsonResponse(request, orderDates);*/
        if(OC_DATES == null) {
            OC_DATES = S3ReadAndParseUtility.downloadAndParseS3File(BUCKET_NAME, S3_DYNAMIC_CONFIG_FILE_FOR_OC_DATES,
                    new LineByLineRecordReader());
            Collections.sort( OC_DATES );
        }
        RestResponseUtil.setJsonResponse(request, OC_DATES);

    }

    @GET
    @Path("/origcptys")
    @Produces(MediaType.TEXT_PLAIN)
    public void findOrigCptys(@Context HttpServletRequest request) {
        if(OC_ORIG_ORGS == null) {
            OC_ORIG_ORGS = S3ReadAndParseUtility.downloadAndParseS3File(BUCKET_NAME, S3_DYNAMIC_CONFIG_FILE_FOR_OC_ORIG_ORGS,
                    new LineByLineRecordReader());
            Collections.sort( OC_ORIG_ORGS );
        }
        RestResponseUtil.setJsonResponse(request, OC_ORIG_ORGS);
    }

    @GET
    @Path("/currencypairs")
    @Produces(MediaType.TEXT_PLAIN)
    public void findCurrencyPairs(@Context HttpServletRequest request) {
        if(OC_CURRENCY_PAIRS == null) {
            OC_CURRENCY_PAIRS = S3ReadAndParseUtility.downloadAndParseS3File(BUCKET_NAME, S3_STATIC_CONFIG_FILE_FOR_OC_CURRENCY_PAIRS,
                    new LineByLineRecordReader());
            Collections.sort(OC_CURRENCY_PAIRS) ;
        }
        RestResponseUtil.setJsonResponse(request,OC_CURRENCY_PAIRS);
    }

    @GET
    @Path("/takerorgs")
    @Produces(MediaType.TEXT_PLAIN)
    public void findTakerOrgs(@Context HttpServletRequest request) {
        if(OC_TAKER_ORGS == null) {
            OC_TAKER_ORGS = S3ReadAndParseUtility.downloadAndParseS3File(BUCKET_NAME, S3_DYNAMIC_CONFIG_FILE_FOR_OC_TAKER_ORGS,
                    new LineByLineRecordReader());
            Collections.sort(OC_TAKER_ORGS);
        }
        RestResponseUtil.setJsonResponse(request, OC_TAKER_ORGS);
    }

    /**
     * Record transformer implementation which returns whatever it gets as input.Checks for null and blank.
     */
    private static class LineByLineRecordReader implements RecordTransformer<String> {
        @Override
        public String transform(String record) {
            if(StringUtils.isNotBlank(record)){
                return record;
            }
            return null;
        }
    }
}

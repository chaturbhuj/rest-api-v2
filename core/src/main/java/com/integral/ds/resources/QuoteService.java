package com.integral.ds.resources;

import com.integral.ds.emscope.quote.QuoteServiceFactory;
import com.integral.ds.emscope.quote.RatesQuoteService;
import com.integral.ds.emscope.quote.impl.RateQuoteServiceCompleteDownload;
import com.integral.ds.model.ErrorObject;
import com.integral.ds.util.RestResponseUtil;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Quote service for getting various definitions of quotes.
 *
 * @author Rahul Bhattacharjee
 */
@Path("/noauth/quote")
@Component
@Scope("request")
public class QuoteService {

    private static final Logger LOGGER = Logger.getLogger(QuoteService.class);

    private RatesQuoteService quoteService = new RateQuoteServiceCompleteDownload();

    @Path("/find/{provider}/{stream}/{ccypair}/{time}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getQuoteAtPoint(@Context HttpServletRequest request,
                                @PathParam("provider") String provider,
                                @PathParam("stream") String stream,
                                @PathParam("ccypair") String ccypair,
                                @PathParam("time") long time) {

        Date point = new Date(time);
        Object response = null;

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("Executing point at quote service for parameters , provider " + provider + " stream " + stream
            +" ccy pair " + ccypair + " time " + point);
        }
        try {

            response = quoteService.getQuoteAtTime(provider,stream,ccypair,point);
        } catch (Exception e) {
            LOGGER.error("Exception while getting quote.",e);
            response = new ErrorObject(e.getMessage());
        }
        return RestResponseUtil.setJsonResponse(request, response);
    }

    @Path("/find/string/{provider}/{stream}/{ccypair}/{time}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getQuoteAtPointWithDateString(@Context HttpServletRequest request,
                                @PathParam("provider") String provider,
                                @PathParam("stream") String stream,
                                @PathParam("ccypair") String ccypair,
                                @PathParam("time") String time) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y-HH:mm:ss.SSS");
        Object response = null;

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("Executing point at quote service for parameters , provider " + provider + " stream " + stream
                    +" ccy pair " + ccypair + " time " + time);
        }

        try {
            Date point = dateFormat.parse(time);
            response = quoteService.getQuoteAtTime(provider,stream,ccypair,point);
        } catch (Exception e) {
            LOGGER.error("Exception while getting quote.",e);
            response = new ErrorObject(e.getMessage());
        }
        return RestResponseUtil.setJsonResponse(request, response);
    }

    @Path("/sample/{provider}/{stream}/{ccypair}/{from}/{to}/{sample}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getQuoteSample(@Context HttpServletRequest request,
                                @PathParam("provider") String provider,
                                @PathParam("stream") String stream,
                                @PathParam("ccypair") String ccypair,
                                @PathParam("from") long from,
                                @PathParam("to") long to,
                                @PathParam("sample") int sample) {

        Date fromDate = new Date(from);
        Date toDate = new Date(to);
        Object response = null;

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("Executing get quote sample for parameters , provider " + provider + " stream " + stream
            + " ccy pair " + ccypair + " from " + from + " to " + to + " sample " + sample);
        }

        RatesQuoteService contextualQuoteService = getQuoteService(fromDate,toDate,sample);
        try {
            response = contextualQuoteService.getSampledQuotes(provider,stream,ccypair,fromDate,toDate,sample);
        } catch (Exception e) {
            LOGGER.error("Exception while getting samples rates.",e);
            response = new ErrorObject(e.getMessage());
        }
        return RestResponseUtil.setJsonResponse(request, response);
    }

    @Path("/sample/string/{provider}/{stream}/{ccypair}/{from}/{to}/{sample}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getQuoteSample(@Context HttpServletRequest request,
                               @PathParam("provider") String provider,
                               @PathParam("stream") String stream,
                               @PathParam("ccypair") String ccypair,
                               @PathParam("from") String from,
                               @PathParam("to") String to,
                               @PathParam("sample") int sample) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y-HH:mm:ss.SSS");
        Object response = null;

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("Executing get quote sample for parameters , provider " + provider + " stream " + stream
                    + " ccy pair " + ccypair + " from " + from + " to " + to + " sample " + sample);
        }

        try {
            Date fromDate = dateFormat.parse(from);
            Date toDate = dateFormat.parse(to);
            RatesQuoteService contextualQuoteService = getQuoteService(fromDate,toDate,sample);

            response = contextualQuoteService.getSampledQuotes(provider,stream,ccypair,fromDate,toDate,sample);
        } catch (Exception e) {
            LOGGER.error("Exception while getting samples rates.",e);
            response = new ErrorObject(e.getMessage());
        }
        return RestResponseUtil.setJsonResponse(request, response);
    }

    @Path("/vwap/{provider}/{stream}/{ccypair}/{time}/{volume}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getPriceAtSizeRate(@Context HttpServletRequest request,
                               @PathParam("provider") String provider,
                               @PathParam("stream") String stream,
                               @PathParam("ccypair") String ccypair,
                               @PathParam("time") String time,
                               @PathParam("volume") int volume) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y-HH:mm:ss.SSS");
        Object response = null;

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("Executing vwap price at size service with parameters , provider " + provider + " stream " + stream
                    + " ccy pair " + ccypair + " time " + time + " for volume " + volume);
        }

        try {
            Date point = dateFormat.parse(time);
            response = quoteService.getQuoteAtTimeForVolume(provider,stream,ccypair,point,volume);
        } catch (Exception e) {
            LOGGER.error("Exception while getting samples rates.",e);
            response = new ErrorObject(e.getMessage());
        }
        return RestResponseUtil.setJsonResponse(request, response);
    }

    /**
     *
     * Returns the appropriate implementation of RatesQuoteService using QuoteServiceFactory.
     *
     * @param start
     * @param end
     * @param noOfSamples
     * @return
     */
    private RatesQuoteService getQuoteService(Date start , Date end , int noOfSamples) {
        return QuoteServiceFactory.getRateQuoteService(start,end,noOfSamples);
    }
}

package com.integral.ds.resources;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.integral.ds.util.RestResponseUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.integral.ds.dto.EMScopeHolder;
import com.integral.ds.dto.OrderLandscapeDetails;
import com.integral.ds.emscope.orderswithtrade.OrdersWithTradesAppDataContext;
import com.integral.ds.model.data.DataQuery;
import com.integral.ds.representation.RepresentationModel;
import com.owlike.genson.Genson;

/**
 * Created with IntelliJ IDEA. User: vchawla Date: 10/7/13 Time: 7:24 PM To
 * change this template use File | Settings | File Templates.
 */
@Path("/orderswithtrades")
@Component
@Scope("request")
public class OrdersWithTradeCollectionResource {

	private  final Log log = LogFactory.getLog(OrdersWithTradeCollectionResource.class);
	
	private OrdersWithTradesAppDataContext appDataContext;
	
	@Resource(name="ordersWithTradesContext")
    public void setAppDataContext(OrdersWithTradesAppDataContext appDataContext) {
		this.appDataContext = appDataContext;
		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
	}


	@Path("orderid/{orderid}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getOrdersWithTrades(@Context HttpServletRequest request, @PathParam("orderid") String orderID) {
    	Map returnMap = appDataContext.getOrderDetailsForOrderID(((String)request.getSession().getAttribute("UserName")), orderID);
    	Genson genson = new Genson();
    	try	{
    		return genson.serialize(returnMap);
    	}
    	catch (Exception exception){
            log.error("Exception while getting orders with trades.",exception);
    	}
    	return "";
	}

    @Path("orderdetails/{orderid}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getOrdersWithDetails(@Context HttpServletRequest request, @PathParam("orderid") String orderID) {
        String jsonString = " ";

        try
        {
             jsonString = appDataContext.getExtendedOrderDetailsForOrderID( ( ( String ) request.getSession().getAttribute( "UserName" ) ), orderID );
        }
        catch (Exception exception)
        {
            log.error("Exception while getting detailed orders: "+orderID,exception);
        }
        return jsonString;
    }
	@Path("landscape/org/{org}/ccypair/{ccypair}/from/{from}/to/{to}/ordertype/{orderType}/minamount/{minAmount}/type/{type}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getOrdersForOrg(@Context HttpServletRequest request, @PathParam("org") String org, @PathParam("ccypair") String ccyPair,
            @PathParam("from") long fromTime, @PathParam("to") long toTime,
            @PathParam("orderType") String orderType, @PathParam("minAmount") long minAmount, @PathParam("type") String type) {

        if(isMaker(type)) {
            return getTradesOrdersForLandscape(request,org,ccyPair,orderType,minAmount,fromTime,toTime);
        } else {
            return getOrdersTradesForLandscape(request,org,ccyPair,orderType,minAmount,fromTime,toTime);
        }
    }

    
	@Path("full1/org/{org}/ccypair/{ccypair}/from/{from}/to/{to}/ordertype/{orderType}/minamount/{minAmount}/type/{type}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getFullOrdersDetailsForOrg(@Context HttpServletRequest request, @PathParam("org") String org, @PathParam("ccypair") String ccyPair,
            @PathParam("from") long fromTime, @PathParam("to") long toTime,
            @PathParam("orderType") String orderType, @PathParam("minAmount") long minAmount,@PathParam("type") String type) {

        if(isMaker(type)) {
            return getTradesOrdersForFullTableDetails(request,org,ccyPair,orderType,minAmount,fromTime,toTime);
        } else {
            return getOrdersTradesForFullTableDetails(request,org,ccyPair,orderType,minAmount,fromTime,toTime);
        }
    }
	
	
	@Path("coveredTrades/tradeID/{tradeID}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getCoveredTrades (@PathParam("tradeID") String tradeID) {
		try	{
			Genson genson = new Genson();
			Map returnMap = appDataContext.getCoveredTrades(tradeID);
			return genson.serialize(returnMap);
		}
		catch (Exception exception){
			log.error(exception.getMessage());
		}
		return "";
    }

    @Path("coveredOrders/orderID/{orderID}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getCoveredOrders (@PathParam("orderID") String orderID) {
        String jsonString=" ";
        try	{

            jsonString = appDataContext.getCoveredOrders(orderID);
            if (jsonString == null || jsonString.trim().length() <1)
            {
                jsonString="[]";
            }
        }
        catch (Exception exception){
            log.error(exception.getMessage());
        }
        return jsonString;
    }

    @Path("coveringOrders/orderID/{orderID}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getCoveringOrders (@PathParam("orderID") String orderID) {
        String jsonString=" ";
        try	{
            jsonString = appDataContext.getCoveringOrders(orderID);
            if (jsonString == null || jsonString.trim().length() <1)
            {
                jsonString="[]";
            }
        }
        catch (Exception exception){
            log.error(exception.getMessage());
        }
        return jsonString;
    }


    @Path("originatingOrder/orderID/{orderID}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getOriginatingOrder (@PathParam("orderID") String orderID) {
        String jsonString=" ";
        try	{

            jsonString = appDataContext.getOriginatingOrder(orderID);
            if (jsonString == null || jsonString.trim().length() <1)
            {
                jsonString="[]";
            }
            log.info("Originating order JSON "+jsonString);
        }
        catch (Exception exception){
            log.error(exception.getMessage());
        }
        return jsonString;
    }

    @Path("marketSnapShot/tradeID/{tradeID}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getMarketSnapshotForTrade (@PathParam("tradeID") String tradeID) {
        String jsonString=" ";
        try	{

            jsonString = appDataContext.getMarketSnapShotForTrade(tradeID);
            if (jsonString == null || jsonString.trim().length() <1)
            {
                jsonString="[]";
            }
        }
        catch (Exception exception){
            log.error(exception.getMessage());
        }
        return jsonString;
    }

    @Path("marketSnapShot/orderID/{orderID}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getMarketSnapshotForOrder (@PathParam("orderID") String orderID) {
        String jsonString=" ";
        try	{

            jsonString = appDataContext.getMarketSnapShotForOrder(orderID);
            if (jsonString == null || jsonString.trim().length() <1)
            {
                jsonString="[]";
            }
        }
        catch (Exception exception){
            log.error(exception.getMessage());
        }
        return jsonString;
    }
    @Path("rateEvents/tradeID/{tradeID}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getRateEventsForTrade (@PathParam("tradeID") String tradeID) {
        String jsonString=" ";
        try	{

            jsonString = appDataContext.getRateEventsForTrade(tradeID);
            if (jsonString == null || jsonString.trim().length() <1)
            {
                jsonString="[]";
            }
        }
        catch (Exception exception){
            log.error(exception.getMessage());
        }
        return jsonString;
    }

    @Path("ordercollection/{takerorg}/{ccypair}/{from}/{to}/{orderType}/{status}/{channel}/{tif}/{origorg}/{buysell}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getOrderCollection(@Context HttpServletRequest request, @PathParam("takerorg") String takerorg,@PathParam("ccypair") String ccyPair,
                                  @PathParam("from") long fromTime, @PathParam("to") long toTime,
                                  @PathParam("orderType") String orderType,
                                  @PathParam("status") String status,
                                  @PathParam("channel") String channel,
                                  @PathParam("tif") String tif,
                                  @PathParam("origorg") String origorg,
                                  @PathParam("buysell") String buysell) {

        // create the dynamic query based on the parameters.
        request.getSession().setAttribute( "takerorg", takerorg );
        request.getSession().setAttribute( "ccypair", ccyPair );
        request.getSession().setAttribute( "fromTime", fromTime );
        request.getSession().setAttribute( "toTime", toTime );
        request.getSession().setAttribute( "orderType", orderType );
        request.getSession().setAttribute( "status", status );
        request.getSession().setAttribute( "channel", channel );
        request.getSession().setAttribute( "tif", tif );
        request.getSession().setAttribute( "origorg", origorg );
        request.getSession().setAttribute( "buysell", buysell );

        int offset =0;
        request.getSession().setAttribute( "offset", offset );
        String jsonData= appDataContext.generateOrderCollectionQuery(  takerorg,  ccyPair, fromTime,  toTime, orderType,
                 status,  channel,  tif,  origorg ,  buysell,offset );
        return jsonData;
    }

    @Path("ordercollection/{from}/{to}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getOrderCollection(@Context HttpServletRequest request,
                                     @PathParam("from") long fromTime, @PathParam("to") long toTime
                                     ) {

        // create the dynamic query based on the parameters.
        request.getSession().setAttribute( "fromTime", fromTime );
        request.getSession().setAttribute( "toTime", toTime );
        int offset =0;
        request.getSession().setAttribute( "offset", 0 );
        String orderQuery= appDataContext.generateOrderCollectionQuery(  "none",  "none", fromTime,  toTime, "none",
                "none",  "none",  "none",  "none" ,  "none",offset );
        return orderQuery;
    }

    @Path("ordercollection/{takerorg}/{ccypair}/{timeRange}/{orderType}/{status}/{channel}/{tif}/{origorg}/{buysell}/{orderby}/{groupby}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getOrderCollection(@Context HttpServletRequest request, @PathParam("takerorg") String takerorg,@PathParam("ccypair") String ccyPair,
                                     @PathParam("timeRange") String timeRange,
                                     @PathParam("orderType") String orderType,
                                     @PathParam("status") String status,
                                     @PathParam("channel") String channel,
                                     @PathParam("tif") String tif,
                                     @PathParam("origorg") String origorg,
                                     @PathParam("buysell") String buysell,
                                     @PathParam("orderby") String orderby,
                                     @PathParam("groupby") String groupby) {

        // create the dynamic query based on the parameters.
        request.getSession().setAttribute( "takerorg", takerorg );
        request.getSession().setAttribute( "ccypair", ccyPair );
        request.getSession().setAttribute( "timeRange", timeRange );

        request.getSession().setAttribute( "orderType", orderType );
        request.getSession().setAttribute( "status", status );
        request.getSession().setAttribute( "channel", channel );
        request.getSession().setAttribute( "tif", tif );
        request.getSession().setAttribute( "origorg", origorg );
        request.getSession().setAttribute( "buysell", buysell );
        request.getSession().setAttribute( "orderby", orderby );
        request.getSession().setAttribute( "groupby", groupby );

        int offset =0;
        request.getSession().setAttribute( "offset", offset );
        String jsonData= appDataContext.generateOrderCollectionQuery(  takerorg,  ccyPair, timeRange, orderType,
                status,  channel,  tif,  origorg ,  buysell,offset,orderby,groupby,true );
        return jsonData;
    }

    @Path("next")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getNextOrderCollection(@Context HttpServletRequest request

    ) {

        // create the dynamic query based on the parameters.
        String takerorg= ( String ) request.getSession().getAttribute( "takerorg");
        String ccyPair= ( String ) request.getSession().getAttribute( "ccypair" );
        Object timeRange = request.getSession().getAttribute( "timeRange" );
        Object fromTimeObj = null;
        Object toTimeObj = null;
        if (timeRange == null)
        {
             fromTimeObj = request.getSession().getAttribute( "fromTime" );
             toTimeObj = request.getSession().getAttribute( "toTime" );
        }
        String orderType = (String)request.getSession().getAttribute( "orderType" );
        String status=(String)request.getSession().getAttribute( "status" );
        String channel=(String)request.getSession().getAttribute( "channel" );
        String tif =(String)request.getSession().getAttribute( "tif" );
        String origorg= ( String ) request.getSession().getAttribute( "origorg" );
        String buysell= ( String ) request.getSession().getAttribute( "buysell" );
        String orderby = ( String ) request.getSession().getAttribute( "orderby" );
        String groupby = ( String ) request.getSession().getAttribute( "groupby" );

        int offset = (int) request.getSession().getAttribute( "offset") + 50;
        String jsonData ="{}";
                request.getSession().setAttribute( "offset", offset);
        if (timeRange == null)
        {
             jsonData = appDataContext.generateOrderCollectionQuery( takerorg, ccyPair, ( long ) fromTimeObj, ( long ) toTimeObj, orderType,
                    status, channel, tif, origorg, buysell, offset );
        }
        else
        {
             jsonData= appDataContext.generateOrderCollectionQuery(  takerorg,  ccyPair, ( String ) timeRange, orderType,
                    status,  channel,  tif,  origorg ,  buysell,offset, orderby, groupby,false );
        }
        return jsonData;
    }


    @Path("prev")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getPrevOrderCollection(@Context HttpServletRequest request

    ) {

        // create the dynamic query based on the parameters.
        String takerorg= ( String ) request.getSession().getAttribute( "takerorg");
        String ccyPair= ( String ) request.getSession().getAttribute( "ccypair" );
        Object timeRange = request.getSession().getAttribute( "timeRange" );
        Object fromTimeObj = null;
        Object toTimeObj = null;
        if (timeRange == null)
        {
            fromTimeObj = request.getSession().getAttribute( "fromTime" );
            toTimeObj = request.getSession().getAttribute( "toTime" );
        }
       /* Object fromTimeObj = request.getSession().getAttribute( "fromTime");
        Object toTimeObj = request.getSession().getAttribute( "toTime" );*/
        String orderType = (String)request.getSession().getAttribute( "orderType" );
        String status=(String)request.getSession().getAttribute( "status" );
        String channel=(String)request.getSession().getAttribute( "channel" );
        String tif =(String)request.getSession().getAttribute( "tif" );
        String origorg= ( String ) request.getSession().getAttribute( "origorg" );
        String buysell= ( String ) request.getSession().getAttribute( "buysell" );
        String orderby = ( String ) request.getSession().getAttribute( "orderby" );
        String groupby = ( String ) request.getSession().getAttribute( "groupby" );
        int offset = (int) request.getSession().getAttribute( "offset");
        offset = offset > 10? offset - 50: 0;
        request.getSession().setAttribute( "offset", offset);
        String jsonData ="{}";
        if (timeRange == null)
        {
            jsonData = appDataContext.generateOrderCollectionQuery( takerorg, ccyPair, ( long ) fromTimeObj, ( long ) toTimeObj, orderType,
                    status, channel, tif, origorg, buysell, offset );
        }
        else
        {
            jsonData= appDataContext.generateOrderCollectionQuery(  takerorg,  ccyPair, ( String ) timeRange, orderType,
                    status,  channel,  tif,  origorg ,  buysell,offset, orderby, groupby,false );
        }
        return jsonData;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private String getTradesOrdersForFullTableDetails(HttpServletRequest request, String org, String ccyPair, String orderType, long minAmount, long fromTime, long toTime) {
       
        EMScopeHolder emScopeHolder = appDataContext.getTradesAndOrdersDetailsForOrg(((String)request.getSession().getAttribute("UserName")), org, ccyPair, orderType, minAmount, new Date(fromTime), new Date(toTime));

        Genson genson = new Genson();
        DataQuery dq = new DataQuery();
        List<Map> orderDetails =(List<Map>) dq.apply(emScopeHolder.getOrderList(), RepresentationModel.SUMMARY, false);
        List<Map> tradeDetails =(List<Map>) dq.apply(emScopeHolder.getTradeList(), RepresentationModel.SUMMARY, false);

        Map returnMap = new HashMap<>();
        returnMap.put("OrderSummary", orderDetails);
        returnMap.put("TradeSummary", tradeDetails);

        try	{
            return genson.serialize(returnMap);
        }
        catch (Exception exception)	{
            log.error("Exception while getting trades orders for trades orders for full table details.",exception);
        }
        return "";
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private String getOrdersTradesForFullTableDetails (HttpServletRequest request,String org, String ccyPair,String orderType,long minAmount,long fromTime,long toTime) {
        
        EMScopeHolder emScopeHolder = appDataContext.getOrderAndTradeDetailsForOrg(((String)request.getSession().getAttribute("UserName")), org, ccyPair, orderType, minAmount, new Date(fromTime), new Date(toTime));

        Genson genson = new Genson();
        DataQuery dq = new DataQuery();
        List<Map> orderDetails =(List<Map>) dq.apply(emScopeHolder.getOrderList(), RepresentationModel.SUMMARY, false);
        List<Map> tradeDetails =(List<Map>) dq.apply(emScopeHolder.getTradeList(), RepresentationModel.SUMMARY, false);

        Map returnMap = new HashMap<>();
        returnMap.put("OrderSummary", orderDetails);
        returnMap.put("TradeSummary", tradeDetails);

        try	{
            return genson.serialize(returnMap);
        }
        catch (Exception exception)	{
            log.error("Exception while getting orders trades for full table details.",exception);
        }
        return "";
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private String getTradesOrdersForLandscape(HttpServletRequest request, String org, String ccyPair, String orderType, long minAmount, long fromTime, long toTime) {
        
        OrderLandscapeDetails orderSummaryList = appDataContext.getTradesSummaryWithOrders(org, ccyPair, orderType, minAmount, new Date(fromTime), new Date(toTime));
        Genson genson = new Genson();
        DataQuery dq = new DataQuery();
        List<Map> orderDetails =(List<Map>) dq.apply(orderSummaryList.getOrderSummary(), RepresentationModel.SUMMARY, false);
        List<Map> tradeDetails =(List<Map>) dq.apply(orderSummaryList.getTradeSummary(), RepresentationModel.SUMMARY, false);

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("OrderSummary", orderDetails);
        returnMap.put("TradeSummary", tradeDetails);

        try	{
            return genson.serialize(returnMap);
        }
        catch (Exception exception)	{
            log.error("Exception while getting trades , orders for landscape.",exception);
        }
        return "";
    }

    
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String getOrdersTradesForLandscape(HttpServletRequest request , String org ,String ccyPair,String orderType,
                                               long minAmount , long fromTime, long toTime) {
       // Map<String, List<Map>> map = new HashMap();
        OrderLandscapeDetails orderSummaryList = appDataContext.getOrderSummaryWithTrades(((String)request.getSession().getAttribute("UserName")), org, ccyPair, orderType,
                minAmount, new Date(fromTime), new Date(toTime));
        Genson genson = new Genson();
        DataQuery dq = new DataQuery();
        List<Map> orderDetails =(List<Map>) dq.apply(orderSummaryList.getOrderSummary(), RepresentationModel.SUMMARY, false);
        List<Map> tradeDetails =(List<Map>) dq.apply(orderSummaryList.getTradeSummary(), RepresentationModel.SUMMARY, false);

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("OrderSummary", orderDetails);
        returnMap.put("TradeSummary", tradeDetails);

        try	{
            return genson.serialize(returnMap);
        }
        catch (Exception exception)	{
            log.error("Exception while getting orders trades for landscape.",exception);
        }
        return "";
    }

    private boolean isMaker(String type) {
        if(type == null){
            return false;
        } else {
            return "Maker".equalsIgnoreCase(type);
        }
    }

    @Path("tradeevents/{orderid}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getTradeEventsForOrder(@Context HttpServletRequest request ,@PathParam("orderid") String orderId){
        List<String> tradeEvents = appDataContext.getTradeEventsForOrders(orderId);
        return RestResponseUtil.setJsonResponse(request, tradeEvents);
    }
}
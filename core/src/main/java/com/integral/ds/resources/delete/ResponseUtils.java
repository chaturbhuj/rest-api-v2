package com.integral.ds.resources.delete;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;


@Component
public class ResponseUtils {

    @Autowired
    private Gson gson;


//    public


    public String asJson(Object m) {
        if (m == null)
            return null;
        return gson.toJson( m);

    }
    
    public String okayAsJson() {
    	return okayAsJson("okay");
    }
    
    public String okayAsJson(String msg) {
        String result = "{ \"status\":" + gson.toJson(msg) + "}";
        return result;
    }

    public String errorAsJson(String msg) {
        String result = "{ \"error\":" + gson.toJson(msg) + "}";
        return result;
    }

    public String errorNotImplemented() {
        return errorAsJson("TODO - this is not implemented yet.");
    }



}

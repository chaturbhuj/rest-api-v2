package com.integral.ds.resources;

import javax.ws.rs.Path;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Path("/staticdata")
@Component
@Scope("request")
public class StaticDataResource {

}

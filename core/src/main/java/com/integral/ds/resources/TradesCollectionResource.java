package com.integral.ds.resources;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.owlike.genson.TransformationException;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.integral.ds.dto.Trade;
import com.integral.ds.emscope.trades.TradeAppDataContext;
import com.integral.ds.model.data.DataQuery;
import com.integral.ds.representation.RepresentationModel;
import com.owlike.genson.Genson;

/**
 * Created with IntelliJ IDEA.
 * User: vchawla
 * Date: 10/10/13
 * Time: 6:56 AM
 * To change this template use File | Settings | File Templates.
 */
@Path("/trades2")
@Component
@Scope("request")
public class TradesCollectionResource {

    private final Log log = LogFactory.getLog( TradesCollectionResource.class );
    @Autowired
    TradeAppDataContext context;

        
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("tradedetails/orderid/{orderID}/tradeid/{tradeID}")
    public String getTrades(@PathParam("tradeID") String tradeId, @PathParam("orderID") String orderId) {


    	/*List <Trade> tradeList = context.getTradeList(tradeId);

        try	{
    		DataQuery dq = new DataQuery();
    		List <Map> returnMap = (List <Map>)dq.apply(tradeList, RepresentationModel.SUMMARY, false);
    		Genson genson = new Genson();
    		return genson.serialize(returnMap);
    	}
    	catch (Exception exception){
    		System.out.println("Exception: " + exception.getLocalizedMessage());
    	}
    	return "";*/
        try
        {
            return context.getTradeList(tradeId,orderId);
        }
        catch (Exception e)
        {
           log.error( "Exception while getting JSON String for trade id : "+tradeId+"; orderID : "+orderId );
        }
        return "";
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("tradesummarystats/{timerange}/{org}")
    public String getTradeSummaryStats(@PathParam("timerange") String timerange, @PathParam("org") String org) {

        // Returns the entire collection as a map of all results.
        // which will be displayed in the JS
        Genson genson = new Genson();
        Map<String,String> jsonMap = new HashMap<String,String>(4);

        String tradeStatsSummary = context.getTradeSummaryStatusStats( timerange,org );
        String tradeTypeSummary = context.getTradeSummaryTypeStats( timerange,org ) ;
        String tradeTopnCcyPairs =  context.getTopnCCyPairs(timerange,org);
        String tradeTopnOrgs =   context.getTopnOrgs( timerange, org );

        jsonMap.put("tradeStatsSummary",tradeStatsSummary);
        jsonMap.put("tradeTypeSummary",tradeTypeSummary);
        jsonMap.put("tradeTopnCcyPairs",tradeTopnCcyPairs);
        jsonMap.put("tradeTopnOrgs",tradeTopnOrgs);

        String returnJSON ="[]";
        try
        {
            returnJSON= genson.serialize(jsonMap);
        }
        catch ( TransformationException e )
        {
            log.warn( "Exception serializing map",e);
        }
        catch ( IOException f )
        {
            log.warn( "Exception serializing map", f );
        }
        catch ( Exception g )
        {
            log.warn( "Exception serializing map", g );
        }
        return returnJSON;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("tradetopnccypairs/{timerange}/{org}")
    public String getTopnCCyPairs(@PathParam("timerange") String timerange, @PathParam("org") String org) {

        try
        {
            return context.getTopnCCyPairs(timerange,org);
        }
        catch (Exception e)
        {
            log.error( "Exception while getting JSON String for timerange : "+timerange+"; org : "+org );
        }
        return "";
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("tradetopnorgs/{timerange}/{org}")
    public String getTopnOrgs(@PathParam("timerange") String timerange, @PathParam("org") String org) {

        try
        {
            return context.getTopnCCyPairs(timerange,org);
        }
        catch (Exception e)
        {
            log.error( "Exception while getting JSON String for timerange : "+timerange+"; org : "+org );
        }
        return "";
    }
    public void setContext(TradeAppDataContext context) {
        this.context = context;
    }

    private BasicDataSource getDataSource() {
         String url = "jdbc:postgresql://eastot.chgyeswrwlad.us-east-1.rds.amazonaws.com:5432/eastOT";
         String userName = "emscope";
         String password = "Integral4309";
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl(url);
        dataSource.setUsername(userName);
        dataSource.setPassword(password);
        return dataSource;
    }
}
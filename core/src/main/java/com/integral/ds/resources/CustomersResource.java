package com.integral.ds.resources;

import java.io.IOException;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.integral.ds.staticdata.StaticDataManager;
import com.owlike.genson.Genson;
import com.owlike.genson.TransformationException;

/**
 * Created with IntelliJ IDEA.
 * User: vchawla
 * Date: 10/10/13
 * Time: 4:57 PM
 * To change this template use File | Settings | File Templates.
 */

@Path("/customers")
@Component
@Scope("request")
public class CustomersResource {

	@GET
	@Path("/getAll")
	@Produces(MediaType.TEXT_PLAIN)
    public String getCustomers(){
        Set<String> customers = StaticDataManager.getInstance().getCustomer();
        Genson genson = new Genson();
        try	{
        	String value = genson.serialize(customers);
        	return value;
        }
        catch (IOException | TransformationException exception)	{}
        return "";
    }
}

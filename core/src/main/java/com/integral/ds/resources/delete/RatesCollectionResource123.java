package com.integral.ds.resources.delete;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.integral.ds.boot.AppConfig;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.model.data.AppDataContext;
import com.integral.ds.model.data.JdbcDao;

/**
 * Created with IntelliJ IDEA.
 * User: vchawla
 * Date: 10/10/13
 * Time: 6:57 AM
 * To change this template use File | Settings | File Templates.
 */

@Path("/rates2")
@Component
@Scope("request")
public class RatesCollectionResource123 {


    @Autowired
    AppDataContext appDataContext;

    @Autowired
    AppConfig appConfig;

/*
    @GET
    @Path("/search")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Rate> findRates(@Context UriInfo info) {

        JSONObject ret = new JSONObject();
        MultivaluedMap<String, String> searchStr = info.getQueryParameters();
        String tradeId = searchStr.getFirst("tradeId");
        String bounds = searchStr.getFirst("bounds");
        List<String> idsList = Arrays.asList(tradeId);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(JdbcDao.TRADE_IDS_PARAMETER, idsList);

        Collection<Trade> trades = appDataContext.getTradesForIds(params, null);
        Trade trade = (trades != null && !trades.isEmpty()) ? (Trade) trades.toArray()[0] : null;
        bounds = (bounds != null) ? bounds : "0";
//        params.put(JdbcDao.RATE_SEARCH_BOUND,bounds);
        params.put(JdbcDao.ORG_PARAMETER, trade.getCPTY());
        params.put(JdbcDao.CCYPAIR_PARAMETER, trade.getCCYPAIR().replace("/", ""));
        params.put(JdbcDao.STREAM_PARAMETER, trade.getSTREAM());

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS z");


        long lowerBound = trade.getEXECTIME().getTime() - Integer.valueOf(bounds);
        long upperBound = trade.getEXECTIME().getTime() + Integer.valueOf(bounds);
        params.put(JdbcDao.LOWER_BOUND_PARAMETER, lowerBound);
        params.put(JdbcDao.UPPER_BOUND_PARAMETER, upperBound);


        DataQuery query = new DataQuery();
        Collection<Rate> rates = appDataContext.getRates(params, null);

        return rates;
//        return Response.status(Response.Status.OK).type(MediaType.APPLICATION_JSON_TYPE).entity(rates).build();
    }
*/

    @GET
    @Path("/{orderid}/{bounds}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<RatesObject> findRatesAroundOrder(@PathParam("orderid") String order, @PathParam("bounds") int bounds) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(JdbcDao.ORDER_ID_PARAMETER, order);
        params.put(JdbcDao.RATE_SEARCH_BOUND, bounds);
        Collection<RatesObject> rates = appDataContext.getRates(params, null);

        return rates;
//        return Response.status(Response.Status.OK).type(MediaType.APPLICATION_JSON_TYPE).entity(rates).build();
    }


}

package com.integral.ds.resources.delete;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.integral.ds.model.LiquidityProvider;
import com.integral.ds.model.LiquidityProviderOrderSummary;
import com.integral.ds.model.data.AppDataContext;
import com.integral.ds.model.data.DataQuery;
import com.integral.ds.model.data.JdbcDao;
import com.integral.ds.util.ResourceUtils;

@Path("/providers")
@Component
@Scope("request")
public class ProvidersResource {


    @Autowired
    AppDataContext appDataContext;

    @Autowired
    ResponseUtils responseUtils;

    @Autowired
    ObjectMapper json;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<LiquidityProvider> getProviders() {

        return appDataContext.getProviders(null,null);
//        try {
//            return new JSONObject().
//                    put("thisURL", ResourceUtils.getContextUrl()).
//                    put("providers", getProviders(null));
//        } catch (JSONException je) {
//            return null;
//        }
    }

    /**
     * Returns a list of providers based on the passed in query
     *
     * @return
     */
    public Collection<JSONObject> getProviders(DataQuery queryElements) {
        Collection<JSONObject> retVal = new ArrayList<JSONObject>();
        Collection<LiquidityProvider> providers = appDataContext.getProviders(null, queryElements);
        if (providers != null && providers.size() > 0) {
            for (LiquidityProvider provider : providers) {
                retVal.add(provider.toJson());
            }
        }
        return retVal;
    }

    @POST
    @Path("/summary/{lp}/{from}/{to}")
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject getOrderSummaryForProvider(@PathParam("lp") String lp, @PathParam("from") String from, @PathParam("to") String to) {

        Map<String, String> params = new HashMap<String, String>();
        params.put(JdbcDao.FROM_DATE_PARAMETER, from);
        params.put(JdbcDao.TO_DATE_PARAMETER, to);
        params.put(JdbcDao.LP_PARAMETER, lp);
        try {

            return new JSONObject().
                    put("thisURL", ResourceUtils.getContextUrl()).
                    put("orders", getOrderSummary(params, null));
        } catch (JSONException jex) {
            return null;
        }

    }

    @POST
    @Path("/ordersummary/{from}/{to}")
    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
    public JSONObject getOrderSummary(@PathParam("from") String from, @PathParam("to") String to) {
//            System.out.println("--------->>>>" + query.toString());
        try {
//            DataQuery dq = json.readValue(query, DataQuery.class);
//            System.out.println("---------->>>" );
            Map<String, String> params = new HashMap<String, String>();
            params.put(JdbcDao.FROM_DATE_PARAMETER, from);
            params.put(JdbcDao.TO_DATE_PARAMETER, to);
            params.put(JdbcDao.LP_PARAMETER, "%");
            return new JSONObject().
                    put("thisURL", ResourceUtils.getContextUrl()).
                    put("orders", getOrderSummary(params, null));

        } catch(JSONException jex)    {
        return null;
    }


}

    public Collection<JSONObject> getOrderSummary(Map<String, String> params, DataQuery dataQuery) {
        Collection<JSONObject> retVal = new ArrayList<>();
        Collection<LiquidityProviderOrderSummary> summary = appDataContext.getProvidersOrderSummary(params, dataQuery);

        for (LiquidityProviderOrderSummary s : summary) {
            retVal.add(s.toJson());
        }
        return retVal;
    }


}



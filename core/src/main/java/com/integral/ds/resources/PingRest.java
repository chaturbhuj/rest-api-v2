package com.integral.ds.resources;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.integral.ds.util.RestResponseUtil;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.integral.ds.dto.Message;

/**
 * Ping rest service.
 *
 * Default home url of the web application. http://localhost:8080/emscope/index.jsp
 *
 * Rest invocation example : http://localhost:8080/emscope/rest/ping/{ping_message}
 *                           http://localhost:8080/emscope/rest/ping/rahul
 *
 * Rest service end point - /rest/ping
 *
 * @author Rahul Bhattacharjee
 */

@Path("/ping")
@Component
@Scope("request")
public class PingRest {

    private static final Logger LOGGER = Logger.getLogger(PingRest.class);

    @Path("{message}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String ping(@Context HttpServletRequest request,@PathParam("message") String msg) {
        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("Ping service");
        }
        Date currentDate = new Date();
        String currentDateString = currentDate.toString();
        Message message = new Message();

        message.setMessage(msg);
        message.setTimestampString(currentDateString);
        return RestResponseUtil.setJsonResponse(request, message);
    }
}

package com.integral.ds.resources;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.integral.ds.emscope.RestServiceRuntimeException;
import com.integral.ds.emscope.report.RateGranulizationReportAppDataContext;
import com.integral.ds.model.ErrorObject;
import com.integral.ds.util.RestResponseUtil;


@Path("/report/rategranulization")
@Component
@Scope("request")
public class RatesGranulizationReportResource {
	
	  private static final Logger LOGGER = Logger.getLogger(RatesGranulizationReportResource.class);

	  private static final String UNEXPECTED_ERROR = "Unexpected error in EMScope.";

	  @Autowired
	 private RateGranulizationReportAppDataContext rateGranulizationReportAppDataContext;

		@GET
		@Path("{prvdr}/{strm}/{ccyp}/{time}/{hour}")
		@Produces(MediaType.TEXT_PLAIN)
		public String findIsGranulizedRateAvailable(@Context HttpServletRequest request,
				@PathParam("prvdr") String prvdr_, @PathParam("strm") String strm_, @PathParam("ccyp") String ccyPair, @PathParam("time") long time_,
				@PathParam("hour") int hour_) {

	        try {
	            Date time = new Date(time_);
	            Object response = null;

	                try {
	                	LOGGER.log(Level.INFO, "Received Rest Request for: "+prvdr_+" "+strm_+" "+ccyPair+" "+time+" "+hour_);
	                    response = rateGranulizationReportAppDataContext.isGranulizedRateAvailable(prvdr_, strm_, ccyPair, time, hour_);
	                 } catch (Exception e) {
	                    LOGGER.error("Exception while serializing rates object list.",e);
	                    if(e instanceof RestServiceRuntimeException) {
	                        response = ((RestServiceRuntimeException)e).getErrorObject();
	                    } else {
	                        ErrorObject errorObject = new ErrorObject();
	                        errorObject.setType(ErrorObject.ErrorType.error);
	                        errorObject.setErrorMessage(UNEXPECTED_ERROR + " Message => " + e.getMessage());
	                        response = errorObject;
	                    }
	                }
	            assert response != null;
	            return RestResponseUtil.setJsonResponse(request, response);
	        }catch (Exception e) {
	            LOGGER.error(UNEXPECTED_ERROR,e);
	        }
	        return UNEXPECTED_ERROR;
		}

		
		@GET
		@Path("{prvdr}/{strm}/{ccyp}/{time}")
		@Produces(MediaType.TEXT_PLAIN)
		public String findIsGranulizedRateAvailable(@Context HttpServletRequest request,
				@PathParam("prvdr") String prvdr_, @PathParam("strm") String strm_, @PathParam("ccyp") String ccyPair, @PathParam("time") long time_
				) {

	        try {
	            Date time = new Date(time_);
	            Object response = null;

	                try {
	                	LOGGER.log(Level.INFO, "Received Rest Request for: "+prvdr_+" "+strm_+" "+ccyPair+" "+time);
	                    response = rateGranulizationReportAppDataContext.getGranulizedRateAvailabilityForDate(prvdr_, strm_, ccyPair, time);
	                 } catch (Exception e) {
	                    LOGGER.error("Exception while serializing rates object list.",e);
	                    if(e instanceof RestServiceRuntimeException) {
	                        response = ((RestServiceRuntimeException)e).getErrorObject();
	                    } else {
	                        ErrorObject errorObject = new ErrorObject();
	                        errorObject.setType(ErrorObject.ErrorType.error);
	                        errorObject.setErrorMessage(UNEXPECTED_ERROR + " Message => " + e.getMessage());
	                        response = errorObject;
	                    }
	                }
	            assert response != null;
	            return RestResponseUtil.setJsonResponse(request, response);
	        }catch (Exception e) {
	            LOGGER.error(UNEXPECTED_ERROR,e);
	        }
	        return UNEXPECTED_ERROR;
		}

}

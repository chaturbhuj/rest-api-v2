package com.integral.ds.resources;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.integral.ds.dto.BenchmarkObject;
import com.integral.ds.emscope.benchmark.BenchmarkAppDataContext;
import com.integral.ds.emscope.profile.RatesProfileObject;
import com.owlike.genson.Genson;

@Path("/benchmarks")
@Component
@Scope("request")
public class BenchmarkCollectionResource {

	@Autowired
	BenchmarkAppDataContext appDataContext;

	@Path("detail/provider/{provider}/stream/{stream}/ccypair/{ccypair}/tier/{tier}/fromtime/{fromtime}/totime/{totime}/")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
	public String getBenchmarkRatesDetail(
			@PathParam("provider") String provider, 
			@PathParam("stream") String stream,
			@PathParam("ccypair") String ccypair,
			@PathParam("fromtime") BigDecimal fromTime, 
			@PathParam("totime") BigDecimal toTime,
			@PathParam("tier") Integer tier
			)	
		{
		Map<String, Object> params = new HashMap<String, Object>();		
		params.put("PVDR", provider);
		params.put("STRM", stream);
		params.put("CCYP", ccypair);
		params.put("TIER", tier);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		
		
		params.put("FROMTIME", sdf.format(new Date(fromTime.longValue())));
		params.put("TOTIME", sdf.format(new Date(toTime.longValue())));
		//List<RatesProfileObject> listOfBenchmarks = appDataContext.getBenchMarkDetails(params);
		List<RatesProfileObject> listOfBenchmarks = null;
		Collections.sort(listOfBenchmarks);
		Genson genson = new Genson();
		String returnValue = "";
		try	{
			returnValue = genson.serialize(listOfBenchmarks);
		}
		catch (Exception exception){
			System.out.println("Exception: " +  exception.getLocalizedMessage());
		}
		return returnValue;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Path("/ccypair/{ccyPair}/fromtime/{fromTime}/totime/{toTime}/tier/{tier}/")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
	public String getBenchmarkRates(
			@PathParam("ccyPair") String ccyPair, 
			@PathParam("fromTime") BigDecimal fromTime, 
			@PathParam("toTime") BigDecimal toTime,
			@PathParam("tier") Integer tier)	{
		
		
		
		Map<String, Object> params = new HashMap();
		params.put("CCYP", ccyPair);
		params.put("TOTIME", toTime);
		params.put("FROMTIME", fromTime);
		params.put("TIER", tier);
		
		//List<BenchmarkObject> benchMarkRates = appDataContext.getBenchMarkRates(params);
		List<BenchmarkObject> benchMarkRates = null;
		Collections.sort(benchMarkRates);
		Genson genson = new Genson();
		String value = "";
		try	{
			value = genson.serialize(benchMarkRates);
		}
		catch (Exception exception){
		}
		return value;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Path("sample/{count}/ccypair/{ccyPair}/fromtime/{fromTime}/totime/{toTime}/tier/{tier}/")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
	public String getSampleBenchmarkRates(
			@PathParam("ccyPair") String ccyPair, 
			@PathParam("fromTime") BigDecimal fromTime, 
			@PathParam("toTime") BigDecimal toTime,
			@PathParam("tier") Integer tier,
			@PathParam("count") Integer count)	{

        Map<String, Object> params = new HashMap();
		List<BigDecimal> bigDecimalList = null;//getTimeIntervals(fromTime, toTime, count);
		params.put("CCYP", ccyPair);
		params.put("TOTIME", toTime);
		params.put("FROMTIME", fromTime);
		params.put("TIER", tier);
		params.put("TMSTMPLIST", bigDecimalList);
		
		//List<BenchmarkObject> benchMarkRates = appDataContext.getBenchMarkSampleRates(params);
		List<BenchmarkObject> benchMarkRates = null;
		Collections.sort(benchMarkRates);
		Genson genson = new Genson();
		String value = "";
		try	{
			value = genson.serialize(benchMarkRates);
		}
		catch (Exception exception){
		}
		return value;
	}

    
    @Path("example")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String sampleBenchmarkCall() {
        Date current = new Date();
        Genson genson = new Genson();

        List<BenchmarkObject> benchmarkObjects = appDataContext.getBenchmarkTest(current);
        try {
            return genson.serialize(benchmarkObjects);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

	/*
	public static void main(String[] args) throws Exception {
		SimpleDateFormat sfd = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		//Date d = sfd.parse("01-Dec-2013 00:00:00");
		//Date d2 = sfd.parse("01-Jan-2014 00:00:00");
		BenchmarkCollectionResource BCR = new BenchmarkCollectionResource();
		BigDecimal b = new BigDecimal("100000");
		BigDecimal b1 = new BigDecimal("900000");
		BCR.getTimeIntervals(b, b1, 600);
	}
	*/
}
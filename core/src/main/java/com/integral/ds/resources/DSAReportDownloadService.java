package com.integral.ds.resources;

import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.integral.ds.dto.S3ObjectIdentifier;
import com.integral.ds.s3.Filter;
import com.integral.ds.s3.S3StreamReader;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * DSA report download service.
 *
 *  WORK IN PROGRESS..!!
 *
 * http://localhost:75/emscope/rest/dsareport/download/ADSS/Stream1/EURUSD/1410137341000/1410137341030
 *
 * @author Rahul Bhattacharjee
 */
@Path("/dsareport")
@Component
public class DSAReportDownloadService {

    private static final Logger LOGGER = Logger.getLogger(DSAReportDownloadService.class);

    private static final String CONTENT_TYPE = "application/zip, application/octet-stream";
    private static final String CONTENT_DISPOSITION_KEY = "Content-Disposition";
    private static final String CONTENT_DISPOSITION = "attachment; filename=\"dsa_lpa_reports.zip\"";

    @GET
    @Path("/download/{provider}/{stream}/{ccypair}/{start}/{end}")
    @Produces(MediaType.TEXT_PLAIN)
    public void getStreams(@Context HttpServletRequest request,@Context HttpServletResponse response,
                                                                 @PathParam("provider") String provider,
                                                                 @PathParam("stream") String stream,
                                                                 @PathParam("ccypair") String ccypair,
                                                                 @PathParam("start") long startTimestamp,
                                                                 @PathParam("end") long endTimestamp) {

        response.setContentType(CONTENT_TYPE);
        response.setHeader(CONTENT_DISPOSITION_KEY, CONTENT_DISPOSITION);

        Date start = new Date(startTimestamp);
        Date end = new Date(endTimestamp);


        // put logger...
        System.out.println("PROVIDER " + provider);
        System.out.println("STREAM " + stream);
        System.out.println("CCYPAIR " + ccypair);


        S3StreamReader streamReader = new S3StreamReader();

        ZipOutputStream zipOutputStream = null;
        try {
            OutputStream outputStream = response.getOutputStream();
            zipOutputStream = new ZipOutputStream(outputStream);

            List<S3ObjectIdentifier> s3objectIdentifiers = getDSAFileLocation(provider,stream,ccypair,start,end,streamReader);

            for(S3ObjectIdentifier s3objectIdentifier : s3objectIdentifiers) {
                InputStream inputStream = getInputStream(s3objectIdentifier,streamReader);
                try {
                    ZipEntry entry = new ZipEntry(s3objectIdentifier.getFileName());
                    zipOutputStream.putNextEntry(entry);
                    IOUtils.copy(inputStream,zipOutputStream);
                    zipOutputStream.closeEntry();
                } catch (Exception e) {
                    e.printStackTrace();
                    // log for unavailable file.
                } finally {
                    IOUtils.closeQuietly(inputStream);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            //log error.
        } finally {
            if(zipOutputStream != null) {
                try {
                    zipOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private InputStream getInputStream(S3ObjectIdentifier s3Object,S3StreamReader streamReader) {

        System.out.println("Requesting content for bucket " + s3Object.getBucket() + " and key " + s3Object.getKey());

        String bucket = s3Object.getBucket();
        String key = s3Object.getKey();
        return streamReader.getInputStream(bucket,key);
    }

    private List<S3ObjectIdentifier> getDSAFileLocation(final String provider,final String stream,final String ccypair, Date start, Date end, S3StreamReader streamReader) throws Exception {
        SimpleDateFormat yearFormatter = new SimpleDateFormat("y");
        SimpleDateFormat monthYearFormatter = new SimpleDateFormat("MM-y");
        SimpleDateFormat monthDayYearFormatter = new SimpleDateFormat("MM-dd-y");

        String bucket = "testmrforrates";
        String year = yearFormatter.format(start);
        String yearMonth = monthYearFormatter.format(start);
        String date = monthDayYearFormatter.format(start);

        String baseDir = "LPA/" + year + "/" + yearMonth + "/" + date + "/" + provider + "/" + stream;

        System.out.println("Getting meta data for  " + baseDir);

        List<S3ObjectIdentifier> identifiers = streamReader.getDirectoryContent(bucket,baseDir, new Filter<S3ObjectSummary>() {
            @Override
            public boolean shouldFilter(S3ObjectSummary obj) {
                String key = obj.getKey();
                if(!key.contains(ccypair)) {
                    return true;
                }
                return false;
            }
        });
        return identifiers;
    }
}

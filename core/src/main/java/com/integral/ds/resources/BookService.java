package com.integral.ds.resources;

import com.integral.ds.dto.Message;
import com.integral.ds.dto.ProviderStream;
import com.integral.ds.emscope.quote.AggregatedQuoteService;
import com.integral.ds.emscope.quote.impl.AggregatedQuoteServiceImpl;
import com.integral.ds.model.QuoteBook;
import com.integral.ds.util.RestResponseUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.*;

/**
 * RestFull service for BookQuote service. Given the number of samples and depth of book. This service returns
 * list of top of book (depth specified.).
 *
 * @author Rahul Bhattacharjee
 */
@Path("/noauth/book")
@Component
@Scope("request")
public class BookService {

    private static final Logger LOGGER = Logger.getLogger(BookService.class);

    private static final String CONTENT_DELIMITER = "-";
    private static final AggregatedQuoteService AGGREGATED_QUOTE_SERVICE = new AggregatedQuoteServiceImpl();

    @Path("/sample/{provider_list}/{stream_list}/{ccypair}/{start}/{end}/{samples}/{depth}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String sample(@Context HttpServletRequest request,
                     @PathParam("provider_list") String providerList,
                     @PathParam("stream_list") String streamList,
                     @PathParam("ccypair") String ccyPair,
                     @PathParam("start") long start,
                     @PathParam("end") long end,
                     @PathParam("samples") int samples,
                     @PathParam("depth") int depth) {

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("Book sampling service.");
        }

        Date startTime = new Date(start);
        Date endTime = new Date(end);
        List<ProviderStream> providerStreams = getProviderStreamList(parseStringInput(providerList),parseStringInput(streamList));
        List<QuoteBook> quoteBooks = AGGREGATED_QUOTE_SERVICE.getAggregatedBook(providerStreams,ccyPair,startTime,endTime,samples,depth);
        return RestResponseUtil.setJsonResponse(request,quoteBooks);
    }

    private List<ProviderStream> getProviderStreamList(List<String> providers, List<String> streams) {
        int size = providers.size();
        List<ProviderStream> providerStreams = new ArrayList<ProviderStream>();
        for(int i = 0 ; i < size ; i++) {
            String provider = providers.get(i);
            String stream = streams.get(i);
            providerStreams.add(new ProviderStream(provider,stream));
        }
        return providerStreams;
    }

    private List<String> parseStringInput(String serContent) {
        if(serContent == null || StringUtils.isBlank(serContent)) {
            return Collections.EMPTY_LIST;
        }
        String [] splits = serContent.split(CONTENT_DELIMITER);
        return Arrays.asList(splits);
    }
}

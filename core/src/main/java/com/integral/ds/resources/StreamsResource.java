package com.integral.ds.resources;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.integral.ds.boot.AppConfig;
import com.integral.ds.model.data.AppDataContext;

/**
 * Created with IntelliJ IDEA.
 * User: vchawla
 * Date: 10/21/13
 * Time: 5:13 PM
 * To change this template use File | Settings | File Templates.
 */


@Path("/streams")
@Component
@Scope("request")
public class StreamsResource {

    @Autowired
    AppDataContext appDataContext;

    @Autowired
    AppConfig appConfig;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject getStreams(@Context HttpServletRequest request){

        JSONObject ret = new JSONObject();
        try {
            ret.put("appURL", appConfig.applicationURL());
            ret.put("thisURI", request.getRequestURI().substring(request.getContextPath().length()));

            for (String c : appConfig.streams())   {
                ret.accumulate("streams", c);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ret;
    }

}

package com.integral.ds.resources;

import com.integral.ds.model.ErrorObject;
import com.integral.ds.s3.S3BrokerRatesReader;
import com.integral.ds.util.RestResponseUtil;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.Date;

/**
 * RESTFull interface for exposing the broker rates service.
 *
 * @author Rahul Bhattacharjee
 */
@Path("/brokerrate")
@Component
public class BrokerRateService {

    private static final Logger LOGGER = Logger.getLogger(BrokerRateService.class);

    private S3BrokerRatesReader brokerRatesReader = new S3BrokerRatesReader();

    @GET
    @Path("/range/{prvdr}/{stream}/{ccyp}/{fromtime}/{totime}")
    @Produces(MediaType.TEXT_PLAIN)
    public String findRange(@Context HttpServletRequest request,
                          @PathParam("prvdr") String provider,
                          @PathParam("stream") String stream,
                          @PathParam("ccyp") String ccyPair,
                          @PathParam("fromtime") long fromtime,
                          @PathParam("totime") long toTime) {

        Object response = null;
        Date start = new Date(fromtime);
        Date end = new Date(toTime);

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("Invoking broker rates query for provider " + provider + " ,stream " + stream + " ,ccy pair " + ccyPair + " , start " + start
            +" ,end " + end);
        }

        try {
            response = brokerRatesReader.getRates(provider,stream,start,end,ccyPair);
        }catch (Exception e) {
            LOGGER.error("Exception while fetching broker rates.",e);
            response = new ErrorObject(e.getMessage());
        }
        return RestResponseUtil.setJsonResponse(request,response);
    }
}

package com.integral.ds.rmi.impl;

import com.integral.ds.rmi.LoadBalancer;
import com.integral.ds.rmi.RemoteRatesCallInterface;
//import de.root1.simon.Lookup;
//import de.root1.simon.Simon;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * simple un weighted round robin balancing.
 *
 * @author Rahul Bhattacharjee
 */
public class SimpleRoundRobinLoadBalancer implements LoadBalancer {

    private static final Logger LOGGER = Logger.getLogger(SimpleRoundRobinLoadBalancer.class);

    private static final String RATES_REMOTE_OBJECT_KEY = "RATES";

    //private static String serverPortList = "127.0.0.1:22222";
    private static String serverPortList = "127.0.0.1:22222,127.0.0.1:22223,127.0.0.1:22224";

    private static Map<Long,ServerPortPair> serverPortPairMap = new HashMap<Long,ServerPortPair>();
    private static int clusterSize;

    private AtomicLong counter = new AtomicLong();

    static {
        loadSlaveServerList();
    }

    private static void loadSlaveServerList() {
        long integer = 0;
        String [] serverList = serverPortList.split(",");
        for(String hostPort : serverList) {
            if(StringUtils.isNotBlank(hostPort)) {
                String [] splits = hostPort.split(":");
                String hostname = splits[0];
                long port = Long.parseLong(splits[1]);
                serverPortPairMap.put(integer++,new ServerPortPair(hostname,(int)port));
            }
        }
        clusterSize = serverPortPairMap.size();
    }

    @Override
    public RemoteRatesCallInterface getRemoteRatesDaoProxy() {
        counter.incrementAndGet();
        long index = (counter.get() % (clusterSize));
        ServerPortPair portPair = serverPortPairMap.get(index);
        if(portPair == null) {
            throw new IllegalArgumentException("No backend servers configured.");
        }

        RemoteRatesCallInterface proxy = null;
        try {
            String host = portPair.getServer();
            int port = portPair.getPort();
            //Lookup nameLookup = Simon.createNameLookup(host, port);
            //proxy = (RemoteRatesCallInterface) nameLookup.lookup(RATES_REMOTE_OBJECT_KEY);
        } catch (Exception e) {
            LOGGER.error("Exception while creating remote object's proxy" ,e);
            throw new IllegalArgumentException("Server not running in " + portPair.getServer() + " and port " + portPair.getPort());
        }
        return proxy;
    }

    private static class ServerPortPair {
        private String server;
        private int port;

        private ServerPortPair(String server, int port) {
            this.server = server;
            this.port = port;
        }

        private String getServer() {
            return server;
        }

        private int getPort() {
            return port;
        }

        @Override
        public String toString() {
            return "ServerPortPair{" +
                    "server='" + server + '\'' +
                    ", port=" + port +
                    '}';
        }
    }
}

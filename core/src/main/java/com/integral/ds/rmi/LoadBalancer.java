package com.integral.ds.rmi;

/**
 * @author Rahul Bhattacharjee
 */
public interface LoadBalancer {

    public RemoteRatesCallInterface getRemoteRatesDaoProxy();

}

package com.integral.ds.rmi;

import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.rmi.impl.RemoteRestCallParameterImpl;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class RemoteRatesInvocationDao {

    private static final Logger LOGGER = Logger.getLogger(RemoteRatesInvocationDao.class);

    private LoadBalancer balancer;

    public List<RestRatesObject> getRates(String provider, String stream, String ccyPair, Date fromTime, Date endTime) {
        RemoteRatesCallInterface remoteRatesCallInterface = balancer.getRemoteRatesDaoProxy();

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("Remote call proxy : " + remoteRatesCallInterface);
        }

        RemoteRestCallParameterImpl parameter = new RemoteRestCallParameterImpl();
        parameter.setProvider(provider).setStream(stream).setCcyPair(ccyPair)
                 .setFromDate(fromTime).setToDate(endTime);

        remoteRatesCallInterface.getRates(parameter);
        return parameter.getResult();
    }

    public void setBalancer(LoadBalancer balancer) {
        this.balancer = balancer;
    }
}

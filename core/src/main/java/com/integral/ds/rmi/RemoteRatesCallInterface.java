package com.integral.ds.rmi;

import com.integral.ds.emscope.rates.RestRatesObject;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public interface RemoteRatesCallInterface extends Serializable {

    //public List<RestRatesObject> getRates(String provider, String stream, String ccyPair, Date fromTime, Date endTime);

    public void getRates(RemoteRestCallParameter parameter);

}

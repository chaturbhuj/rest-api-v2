package com.integral.ds.rmi.impl;

import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.rmi.RemoteRestCallParameter;
//import de.root1.simon.annotation.SimonRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
//@SimonRemote(value = {RemoteRestCallParameter.class})
public class RemoteRestCallParameterImpl implements RemoteRestCallParameter {

    private static final long serialVersionUID = 1L;

    private Date fromDate;
    private Date toDate;
    private String provider;
    private String stream;
    private String ccyPair;
    private List<RestRatesObject> result;

    @Override
    public Date getStartTime() {
        return this.fromDate;
    }

    @Override
    public Date getEndTime() {
        return this.toDate;
    }

    @Override
    public String getProvider() {
        return this.provider;
    }

    @Override
    public String getStream() {
        return this.stream;
    }

    @Override
    public String getCcypair() {
        return this.ccyPair;
    }

    public RemoteRestCallParameterImpl setFromDate(Date fromDate) {
        this.fromDate = fromDate;
        return this;
    }

    public RemoteRestCallParameterImpl setToDate(Date toDate) {
        this.toDate = toDate;
        return this;
    }

    public RemoteRestCallParameterImpl setProvider(String provider) {
        this.provider = provider;
        return this;
    }

    public RemoteRestCallParameterImpl setStream(String stream) {
        this.stream = stream;
        return this;
    }

    public RemoteRestCallParameterImpl setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
        return this;
    }

    public void setResult(List<RestRatesObject> result) {
        this.result = result;
    }

    @Override
    public List<RestRatesObject> getResult() {
        return this.result;
    }

    @Override
    public String toString() {
        return "RemoteRestCallParameterImpl{" +
                "fromDate=" + fromDate +
                ", toDate=" + toDate +
                ", provider='" + provider + '\'' +
                ", stream='" + stream + '\'' +
                ", ccyPair='" + ccyPair + '\'' +
                ", result=" + result +
                '}';
    }
}

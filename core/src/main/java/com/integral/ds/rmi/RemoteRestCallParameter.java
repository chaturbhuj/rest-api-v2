package com.integral.ds.rmi;

import com.integral.ds.emscope.rates.RestRatesObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public interface RemoteRestCallParameter extends Serializable {

    public Date getStartTime();
    public Date getEndTime();
    public String getProvider();
    public String getStream();
    public String getCcypair();
    public List<RestRatesObject> getResult();
    public void setResult(List<RestRatesObject> result);

}

package com.integral.ds.rmi.impl;

import com.integral.ds.dao.s3.S3FilesRatesDao;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.rmi.RemoteRatesCallInterface;
import com.integral.ds.rmi.RemoteRestCallParameter;
//import de.root1.simon.annotation.SimonRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Rahul Bhattacharjee
 */
//@SimonRemote(value = {RemoteRatesCallInterface.class})
public class RemoteRatesCallImpl implements RemoteRatesCallInterface {

    private static final long serialVersionUID = 1L;

    private S3FilesRatesDao ratesDao = new S3FilesRatesDao();

    @Override
    public void getRates(RemoteRestCallParameter parameter) {
        long start = System.currentTimeMillis();
        List<RestRatesObject> ratesObjects = ratesDao.getRates(parameter.getProvider(),parameter.getStream(),parameter.getCcypair(),parameter.getStartTime()
        ,parameter.getEndTime());

        System.out.println("Tame taken to server request " + (System.currentTimeMillis()-start));

        parameter.setResult(ratesObjects);
    }
}

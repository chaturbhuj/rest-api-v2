package com.integral.ds.rmi;

import com.integral.ds.rmi.impl.RemoteRatesCallImpl;
//import de.root1.simon.Registry;
//import de.root1.simon.Simon;

/**
 * Simon server.
 *
 * @author Rahul Bhattacharjee
 */
public class SimonRmiServer {

    private static final String RATES_REMOTE_OBJECT_KEY = "RATES";

    private int port;

    public SimonRmiServer(int port) {
        this.port = port;
    }

    public void start() {
        try {
            RemoteRatesCallImpl ratesService = new RemoteRatesCallImpl();
            //Registry registry = Simon.createRegistry(port);
            //registry.bind(RATES_REMOTE_OBJECT_KEY, ratesService);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Server up and running at port " + port);
    }

    public static void main(String [] argv) {
        /*
        if(argv.length != 1) {
            System.out.print("This program takes one argument , thats port at which the server should run.");
        }
        int port = Integer.parseInt(argv[0]);
        */

        int port = 22224;
        System.out.println("Starting server at port " + port);

        SimonRmiServer server = new SimonRmiServer(port);
        server.start();
    }
}

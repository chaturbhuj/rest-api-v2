package com.integral.ds.controller;

import com.integral.ds.serializer.Serializer;
import com.integral.ds.serializer.SerializerFactory;
import com.integral.ds.util.RestResponseUtil;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import java.io.IOException;

/**
 * @author Rahul Bhattacharjee
 */
public class JerseyRestResponseFilter implements ContainerResponseFilter {

    private static final Logger LOGGER = Logger.getLogger(OutputSerializerFilter.class);

    private static SerializerFactory serializerFactory = new SerializerFactory();

    @Context
    private HttpServletRequest httpRequest;

    @Override
    public ContainerResponse filter(ContainerRequest containerRequest, ContainerResponse containerResponse) {
        String type = (String) httpRequest.getAttribute(RestResponseUtil.SERIALIZER_TYPE);
        Object responseObject = httpRequest.getAttribute(RestResponseUtil.RESPONSE_OBJECT_KEY);

        LOGGER.info("Type => "  + type + " response object => " + responseObject);

        if(type == null || responseObject == null) {
            // for backward compatibility.
            return containerResponse;
        }

        Serializer serializer = serializerFactory.getSerializer(type);
        String content = serializer.serialize(responseObject);
        try {
            containerResponse.getOutputStream().write(content.getBytes());
        } catch (IOException e) {
            LOGGER.error("Exception while writing the content to output stream.", e);
        }
        return containerResponse;
    }
}

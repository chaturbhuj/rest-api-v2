package com.integral.ds.controller;

import com.integral.ds.serializer.Serializer;
import com.integral.ds.serializer.SerializerFactory;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Rahul Bhattacharjee
 */
public class OutputSerializerFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(OutputSerializerFilter.class);

    private static SerializerFactory serializerFactory;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        serializerFactory = new SerializerFactory();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HTTPResponseWrapper responseWrapper = new HTTPResponseWrapper(request,servletResponse);
        filterChain.doFilter(servletRequest,responseWrapper);

        responseWrapper.populate();

        response.setContentLength(responseWrapper.getContentLength());
        response.getWriter().write(responseWrapper.getContent());
    }

    @Override
    public void destroy() {
    }

    private static class HTTPResponseWrapper extends ServletResponseWrapper {

        private CharArrayWriter writer = new CharArrayWriter();
        private PrintWriter printWriter = new PrintWriter(writer);

        private HttpServletRequest request;
        private String content;
        private int contentLength;

        private HTTPResponseWrapper(HttpServletRequest request, ServletResponse response) {
            super(response);
            this.request = request;
        }

        @Override
        public PrintWriter getWriter() throws IOException {
            return printWriter;
        }

        public int getContentLength() {
            return this.contentLength;
        }

        public void populate() {
            String type = (String) request.getAttribute("serializer");
            Object responseObject = request.getAttribute("response");

            LOGGER.debug("Serializer type " + type + " response " + responseObject);

            if(type == null || responseObject == null) {
                // for backward compatibility.
                content = writer.toString();
                contentLength = content.length();
                return;
            }

            Serializer serializer = serializerFactory.getSerializer(type);
            content = serializer.serialize(responseObject);
            contentLength = content.length();
        }

        public String getContent() {
            return this.content;
        }
    }
}

package com.integral.ds.s3.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.apache.http.ParseException;
import org.apache.log4j.Logger;

import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.s3.RecordTransformer;

/**
 * Transforms a rate row record into RateObject.
 * <p>
 * <B>Note:</B> Please avoid JDK {@link SimpleDateFormat} in this class
 * because instantiating this object and calling its parse method is 
 * too expensive and here this method is called very frequently. 
 * Joda time is also not recommended though it performs relatively better but 
 * not as better to be acceptable for this use case. 
 *
 * @author Rahul Bhattacharjee
 */
public class RateRecordTransformer implements RecordTransformer<RatesObject> {

    private static final Logger LOGGER = Logger.getLogger(RateRecordTransformer.class);

    @Override
    public RatesObject transform(String record) {
        RatesObject ratesObject = new RatesObject();

        try {
            String [] fields = record.split(",");

            String provider = fields[0];
            String stream = fields[1];
            String ccyPair = fields[2];
            String tmstmp = fields[3];
            String level = fields[4];
            String status = fields[5];
            String bidPrice = fields[6];
            String bidSize = fields[7];
            String askPrice = fields[8];
            String askSize = fields[9];
            String guid = fields[10];
            String quoteId = fields[11];
            String rtEffectiveTime = fields[12];
            ratesObject.setProvider(provider);
            ratesObject.setStream(stream);
            ratesObject.setCcyPair(ccyPair);
            ratesObject.setTmstmp(translateToEpoch(tmstmp));
            ratesObject.setTier(Integer.parseInt(level));
            ratesObject.setStatus(status);
            ratesObject.setBidPrice(new BigDecimal(bidPrice));
            ratesObject.setBidSize(new BigDecimal(bidSize));
            ratesObject.setAskPrice(new BigDecimal(askPrice));
            ratesObject.setAskSize(new BigDecimal(askSize));
            ratesObject.setGuid(guid);
            ratesObject.setQuoteId(quoteId);

            if(StringUtils.isNotBlank(rtEffectiveTime) && !("null".equalsIgnoreCase(rtEffectiveTime))) {
                ratesObject.setRtEffectiveTime(translateToEpoch(rtEffectiveTime));
            } else {
                ratesObject.setRtEffectiveTime(0L);
            }

            // Column 15 stands for multi-tier, multi-quote attribute.
            // Note that this column may or may not be present.
            String quoteType = null;
            if (fields.length >= 15) {
                quoteType = fields[14].trim();
            }
            ratesObject.setQuoteType(quoteType);
        } catch (Exception e) {
            throw new IllegalArgumentException("Exception while transforming raw Rate record into Rate object.",e);
        }
        return ratesObject;
    }

    /**
     * Helper method to translate timestamp in given string to equivalent epoch.
     * It translates string timestamp in yyyyMMdd HH:mm:ss:SSS to
     * equivalent epoch.
     * <p>
     * <B>Note:</B> Here JDK {@link SimpleDateFormat} is intentionally 
     * not used because instantiating this object and calling its parse 
     * method is too expensive and here this method is called very frequently. 
     * Joda time is also not used though it performs relatively better but 
     * not as better to be acceptable for this use case. 
     * 
     * @param dateString    Timestamp string in yyyyMMdd HH:mm:ss:SSS format
     * 
     * @return              Epoch equivalent to given timestamp string.
     * @throws Exception    In case it fails to parse string as timestamp.
     */
    private long parseEpochForNextMostFrequentFormat(String dateString) throws Exception
    {
        int year      = 0;
        int month     = 0;
        int day       = 0;
        int hour      = 0;
        int minute    = 0;
        int seconds   = 0;
        int milliSecs = 0;

        // Format yyyyMMdd HH:mm:ss:SSS
        String[] tokens = StringUtils.split(dateString);
        if (tokens.length < 2) {
            throw new ParseException("Failed to parse '" + dateString + 
                                     "' as date for format ");
        }

        String yyyyMMdd  = tokens[0];
        // First 4 chars constitute year
        int startIndex = 0;
        int endIndex   = startIndex + 4;
        year = Integer.parseInt(yyyyMMdd.substring(startIndex, endIndex));
        // Next 2 chars constitute month
        startIndex = endIndex;
        endIndex = startIndex + 2;
        month = Integer.parseInt(yyyyMMdd.substring(startIndex, endIndex));
        // Last 2 chars constitute day
        startIndex = endIndex;
        day = Integer.parseInt(yyyyMMdd.substring(startIndex));

        // This is HH:mm:ss:SSS
        String hhmmssSSS = tokens[1];
        String[] hhmmssSSSTokens = StringUtils.split(hhmmssSSS, ":");
        if (hhmmssSSSTokens.length < 4) {
            throw new ParseException("Failed to parse '" + dateString + 
                    "' as date for format ");
        }

        hour      = Integer.parseInt(hhmmssSSSTokens[0]);
        minute    = Integer.parseInt(hhmmssSSSTokens[1]);
        seconds   = Integer.parseInt(hhmmssSSSTokens[2]);
        milliSecs = Integer.parseInt(hhmmssSSSTokens[3]);

        return getTimeInMillis(year, month, day, 
                               hour, minute, seconds, milliSecs);

    }

    /**
     * Helper method to translate timestamp in given string to equivalent epoch.
     * It translates string timestamp in MM.dd.yyyy HH:mm:ss.SSS to
     * equivalent epoch
     * <p>
     * <B>Note:</B> Here JDK {@link SimpleDateFormat} is intentionally 
     * not used because instantiating this object and calling its parse 
     * method is too expensive and here this method is called very frequently. 
     * Joda time is also not used though it performs relatively better but 
     * not as better to be acceptable for this use case. 
     * 
     * @param dateString    Timestamp string in MM.dd.yyyy HH:mm:ss.SSS format
     * 
     * @return              Epoch equivalent to given timestamp string.
     * @throws Exception    In case it fails to parse string as timestamp.
     */
    private long parseEpochForMostFrequentFormat(String dateString) throws Exception
    {
        int year      = 0;
        int month     = 0;
        int day       = 0;
        int hour      = 0;
        int minute    = 0;
        int seconds   = 0;
        int milliSecs = 0;

        // Format MM.dd.yyyy HH:mm:ss.SSS
        String[] tokens = StringUtils.split(dateString);
        if (tokens.length < 2) {
            throw new ParseException("Failed to parse '" + dateString + 
                                     "' as date for format ");
        }
        String mmddyyyy = tokens[0];
        String hhmmssSSS = tokens[1];

        String[] mmddyyyyTokens = StringUtils.split(mmddyyyy, ".");
        String[] hhmmssSSSTokens = StringUtils.split(hhmmssSSS, ":");
        if (mmddyyyyTokens.length < 3 || hhmmssSSSTokens.length < 3) {
            throw new ParseException("Failed to parse '" + dateString + 
                    "' as date for format ");
        }

        String[] ssSSSTokens = StringUtils.split(hhmmssSSSTokens[2], ".");
        if (ssSSSTokens.length < 2) {
            throw new ParseException("Failed to parse '" + dateString + 
                    "' as date for format ");
        }

        month     = Integer.parseInt(mmddyyyyTokens[0]);
        day       = Integer.parseInt(mmddyyyyTokens[1]);
        year      = Integer.parseInt(mmddyyyyTokens[2]);
        hour      = Integer.parseInt(hhmmssSSSTokens[0]);
        minute    = Integer.parseInt(hhmmssSSSTokens[1]);
        seconds   = Integer.parseInt(ssSSSTokens[0]);
        milliSecs = Integer.parseInt(ssSSSTokens[1]);

        return getTimeInMillis(year, month, day, 
                               hour, minute, seconds, milliSecs);
    }

    /**
     * Utility method to derive time in epoch from given timestamp attributes.
     * <br>
     * Note: It assumes that month parameter is as in human readable date i.e
     * January = 1, February = 2... This method uses {@link Calendar} class
     * that internally understands January = 0, Februray = 1 ... for month 
     * fields. So while setting month attribute it is internally adjusted 
     * with -1 factor, caller should not do so.
     *  
     * @param year
     * @param month
     * @param day
     * @param hour
     * @param minute
     * @param second
     * @param milliSecs
     * 
     * @return Time in epoch which is equivalent to the given timestamp 
     * attributes.
     */
    private long getTimeInMillis(int year, int month, int day, 
                                 int hour, int minute, int second, 
                                 int milliSecs)
    {
        Calendar calendar = Calendar.getInstance();

        // Note Calendar understands January as 0 so subtracting 1.
        int monthIndex = month - 1;
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthIndex);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);
        calendar.set(Calendar.MILLISECOND, milliSecs);

        return calendar.getTimeInMillis();
    }

    /**
     * Utility method to translate string timestamp to equivalent epoch.
     */
    private long translateToEpoch(String dateString) throws Exception 
    {
        long epoch = 0;
        try {
            epoch = parseEpochForMostFrequentFormat(dateString);
        } catch (Exception e) {
            LOGGER.debug("Exception while formatting date using format MM.dd.y HH:mm:ss.SSS , will use alternate format.");

            epoch = parseEpochForNextMostFrequentFormat(dateString);
        }

        return epoch;
    }
}

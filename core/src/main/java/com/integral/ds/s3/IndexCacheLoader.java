package com.integral.ds.s3;

import com.google.common.cache.CacheLoader;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Index loader.
 *
 * @author Rahul Bhattacharjee
 */
public class IndexCacheLoader extends CacheLoader<String, Map<Integer, Long>> {

    private static final Logger LOGGER = Logger.getLogger(IndexCacheLoader.class);

    private final static  String INDEX_DELIMETER = "\t";

    private S3StreamReader s3StreamReader = null;

    public IndexCacheLoader(S3StreamReader s3StreamReader) {
        this.s3StreamReader = s3StreamReader;
    }

    @Override
    public Map<Integer, Long> load(String indexPath) throws Exception {
        InputStream inputStream = null;
        BufferedReader reader = null;
        Map<Integer,Long> index = new HashMap<Integer,Long>();

        try {
            inputStream = s3StreamReader.getInputStream(indexPath);
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line = "";
            while(line != null) {
                line = reader.readLine();
                if(StringUtils.isBlank(line)) {
                    continue;
                }
                String [] splits = line.split(INDEX_DELIMETER);
                int minute = Integer.parseInt(splits[0]);
                long offset = Long.parseLong(splits[1]);
                index.put(minute,offset);
            }
        } catch (Exception e) {
            LOGGER.error("Exception while creating index object.",e);
            throw e;
        } finally {
            if(inputStream != null) {
                inputStream.close();
            }
            if(reader != null) {
                reader.close();
            }
        }
        return index;
    }
}

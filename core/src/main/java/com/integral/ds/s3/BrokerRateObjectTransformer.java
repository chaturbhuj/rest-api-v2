package com.integral.ds.s3;

import com.integral.ds.emscope.rates.BrokerRateObject;
import com.integral.ds.emscope.rates.LPStream;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Broker rates transformer.
 *
 * @author Rahul Bhattacharjee
 */
public class BrokerRateObjectTransformer implements RecordTransformer<BrokerRateObject> {

    @Override
    public BrokerRateObject transform(String record) {
        BrokerRateObject brokerRate = new BrokerRateObject();

        try {
            String [] fields = record.split(",",-1);
            String broker = fields[0];
            String stream = fields[1];
            String ccyPair = fields[2];
            String timeStamp = fields[3];
            String lpStreams = fields[4];
            String aggregationStrategy = fields[5];
            String quoteType = fields[6];
            String updateType = fields[7];
            String guid = fields[8];
            String bidPrice = fields[9];
            String bidSize = fields[10];
            String bidLp = fields[11];
            String askPrice = fields[12];
            String askSize = fields[13];
            String askLp = fields[14];

            brokerRate.setBroker(broker);
            brokerRate.setBrokerStream(stream);
            brokerRate.setCcyPair(ccyPair);
            brokerRate.setTimeStamp(getParsedDate(timeStamp));
            brokerRate.setLpStreams(getLpStreams(lpStreams));
            brokerRate.setAggregationStrategy(aggregationStrategy);
            brokerRate.setQuoteType(quoteType);
            brokerRate.setUpdateType(updateType);
            brokerRate.setGuid(guid);

            if(StringUtils.isNotBlank(bidPrice)) {
                brokerRate.setBidPrice(new BigDecimal(bidPrice));
            }
            if(StringUtils.isNotBlank(bidPrice)) {
                brokerRate.setBidSize(new BigDecimal(bidSize));
            }
            brokerRate.setBidLp(bidLp);
            if(StringUtils.isNotBlank(askPrice)){
                brokerRate.setAskPrice(new BigDecimal(askPrice));
            }
            if(StringUtils.isNotBlank(askSize)){
                brokerRate.setAskSize(new BigDecimal(askSize));
            }

            brokerRate.setAskLp(askLp);

        } catch (Exception e) {
            throw new IllegalArgumentException("Exception while parsing record.Record = > " +record ,e);
        }
        return brokerRate;
    }

    private List<LPStream> getLpStreams(String lpStreams) {
        List<LPStream> streams = new ArrayList<>();
        String [] lpStreamCombination = lpStreams.split("\\|");
        for(String lpStream : lpStreamCombination) {
            if(StringUtils.isNotBlank(lpStream)) {
                String [] splits = lpStream.split("-");
                streams.add(new LPStream(splits[0],splits[1]));
            }
        }
        return streams;
    }

    private long getParsedDate(String timeStamp) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        return dateFormat.parse(timeStamp).getTime();
    }
}

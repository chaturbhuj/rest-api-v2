package com.integral.ds.s3.impl;

import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.s3.Filter;

/**
 *
 * @author Rahul Bhattacharjee
 */
public class RateCurrencyPairFilter implements Filter<RestRatesObject> {

    private String ccyPair;

    public RateCurrencyPairFilter(String ccyPair) {
        this.ccyPair = ccyPair;
    }

    @Override
    public boolean shouldFilter(RestRatesObject obj) {
        return !(ccyPair.equalsIgnoreCase(obj.getCcyPair()));
    }
}

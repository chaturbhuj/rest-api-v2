package com.integral.ds.s3;

/**
 * Record transformer for Rate object.
 *
 * @author Rahul Bhattacharjee
 */
public interface RecordTransformer<T> {

    T transform(String record);
}

package com.integral.ds.s3;

/**
 * Filter for removing unwanted rows.
 *
 * @author Rahul Bhattacharjee
 */
public interface Filter<T> {

    public boolean shouldFilter(T obj);

}

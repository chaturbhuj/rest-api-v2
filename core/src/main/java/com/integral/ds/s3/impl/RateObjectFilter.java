package com.integral.ds.s3.impl;

import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.s3.Filter;

import java.util.Date;

/**
 * Rates object filter.
 *
 * @author Rahul Bhattacharjee
 */
public class RateObjectFilter implements Filter<RestRatesObject> {

    private long startTime;
    private long endTime;

    public RateObjectFilter(Date start , Date end) {
        this.startTime = start.getTime();
        this.endTime = end.getTime();
    }

    @Override
    public boolean shouldFilter(RestRatesObject ratesObject) {
        long rateTime = ratesObject.getTmstmp();
        if(rateTime > startTime && rateTime < endTime){
            return false;
        }else {
            return true;
        }
    }
}

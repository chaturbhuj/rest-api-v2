package com.integral.ds.s3;

import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.util.DataQueryUtils;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Rates locator.
 *
 * @author Rahul Bhattacharjee
 */
public class RatesLocator {

    private static final Logger LOGGER = Logger.getLogger(RatesLocator.class);

    private String provider;
    private String stream;
    private String ccyPair;
    private Date startDate;
    private Date endDate;
    private NavigableMap<Long,List<RatesObject>> ratesCache = new TreeMap<>();

    public RatesLocator(String provider, String stream, String ccyPair, Date startDate, Date endDate) {
        this.provider = provider;
        this.stream = stream;
        this.ccyPair = ccyPair;
        this.startDate = startDate;
        this.endDate = endDate;
        initRateCache();
    }

    private void initRateCache() {
        try {
            S3RatesReader ratesReader = new S3RatesReader();
            List<RatesObject> rates = ratesReader.getRates(provider,stream,startDate,endDate,ccyPair);
            for(RatesObject ratesObject : rates) {
                long rateTime = ratesObject.getTmstmp();
                List<RatesObject> rateList = ratesCache.get(rateTime);
                if(rateList == null) {
                    rateList = new ArrayList<>();
                    ratesCache.put(rateTime,rateList);
                }
                rateList.add(ratesObject);
            }
        }catch (Exception e) {
            LOGGER.error("Exception while initializing the cache.",e);
            throw new IllegalArgumentException("Exception while creating cache , check arguments.Message => " + e.getMessage() ,e);
        }
    }

    public Map<Long,List<RatesObject>> getSubRates(Date startDate, Date endDate) {
        if(ratesCache.isEmpty()) {
            return Collections.EMPTY_MAP;
        }
        Long start = ratesCache.ceilingKey(startDate.getTime());
        if(start == null) {
            start = ratesCache.firstKey();
        }

        Long end = ratesCache.floorKey(endDate.getTime());
        if(end == null) {
            end = ratesCache.lastKey();
        }
        if(start.equals(end)) {
            Map<Long,List<RatesObject>> returnMapForSingleEntry = new HashMap<>();
            returnMapForSingleEntry.put(start,ratesCache.get(start));
            return returnMapForSingleEntry;
        }

        return ratesCache.subMap(start,end);
    }

    public Long getPrevailingRateTime(Date sampleTime) {
        return ratesCache.floorKey(sampleTime.getTime());
    }

    public List<Quote> getSamples(Date start ,Date end, int samples) {
        int millsInMinute = 60 * 1000;
        List<Quote> quotes = new ArrayList<>();
        long numberOfMillis = (end.getTime()-start.getTime());
        long gapInMilliseconds = numberOfMillis/samples;
        long pointInMills = start.getTime();
        int count = 0;

        while (count < samples && (pointInMills < end.getTime())) {
            Quote quote = getQuoteForSecond(pointInMills);
            if(quote.isEmpty()) {
                pointInMills += millsInMinute;
            } else {
                quotes.add(quote);
                pointInMills += gapInMilliseconds;
                count++;
            }
        }
        return quotes;
    }

    public List<RatesObject> getRate(long time) {
        return ratesCache.get(time);
    }

    public Quote getQuoteForSecond(long epoch) {
        Quote quote = new Quote();
        Date date = new Date(epoch);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND,0);
        Date start = calendar.getTime();
        calendar.set(Calendar.MILLISECOND, 999);
        Date end = calendar.getTime();

        Map<Long,List<RatesObject>> rates = getSubRates(start, end);

        Map<Long,List<RatesObject>> filteredRates = filterMultipleRatesPerMilliSecond(rates);
        Iterator<List<RatesObject>> iterator = filteredRates.values().iterator();

        while(iterator.hasNext()) {
            List<RatesObject> ratesObjectList = iterator.next();
            for(RatesObject rate :  ratesObjectList) {
                quote.addRate(DataQueryUtils.getRestRateObject(rate));
                quote.setSampleTime(end);
            }
        }
        return quote;
    }

    private Map<Long, List<RatesObject>> filterMultipleRatesPerMilliSecond(Map<Long, List<RatesObject>> rates) {
        Map<Long,List<RatesObject>> result = new HashMap<Long,List<RatesObject>>();
        Iterator<Long> timeIterator = rates.keySet().iterator();

        while(timeIterator.hasNext()) {
            Long timeInMilli = timeIterator.next();
            List<RatesObject> filteredRates = getFilteredRates(rates.get(timeInMilli));
            result.put(timeInMilli,filteredRates);
        }
        return result;
    }

    private List<RatesObject> getFilteredRates(List<RatesObject> ratesObjects) {
        List<RatesObject> result = new ArrayList<RatesObject>();
        Map<Integer,List<RatesObject>> ratesByTier = new HashMap<Integer,List<RatesObject>>();
        for(RatesObject rate : ratesObjects) {
            int tier = rate.getLvl();
            List<RatesObject> rates = ratesByTier.get(tier);
            if(rates == null) {
                rates = new ArrayList<RatesObject>();
                ratesByTier.put(tier,rates);
            }
            rates.add(rate);
        }

        for(Map.Entry<Integer,List<RatesObject>> entry : ratesByTier.entrySet()) {
            List<RatesObject> rates = entry.getValue();
            if(!rates.isEmpty()) {
                int size = rates.size();
                result.add(rates.get(size-1));
            }
        }
        return result;
    }
}

























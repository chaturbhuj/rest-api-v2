package com.integral.ds.s3;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;
import com.integral.ds.dto.S3ObjectIdentifier;
import org.apache.log4j.Logger;
import org.archive.util.zip.OpenJDK7GZIPInputStream;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Reads streams from S3.
 *
 * @author Rahul Bhattacharjee
 */
public class S3StreamReader {

    private static final Logger LOGGER = Logger.getLogger(S3StreamReader.class);

    /**
     * Flag to disable index cache if required.
     */
    private static boolean IS_INDEX_CACHE_ENABLED = true;

    private final static String accessKey = "AKIAI4JZNEOZDPEU2R6A";
    private final static String secretKey = "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";
    private final static String bucketName = "integral-rates";

    private IndexCacheLoader cacheLoader = null;
    private LoadingCache<String,Map<Integer,Long>> cache = null;

    private AmazonS3 s3Client;

    public S3StreamReader() {
        ClientConfiguration config = new ClientConfiguration();
        config.setSocketTimeout(0);
        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        s3Client = new AmazonS3Client(credentials,config);

        cacheLoader = new IndexCacheLoader(this);
        cache = CacheBuilder.newBuilder().softValues().build(cacheLoader);
    }

    public InputStream getGzipInputStream(String filePath) throws Exception {
        return new GZIPInputStream(getInputStream(filePath));
    }

    public  InputStream getInputStream(String filePath) throws Exception {
        return getInputStream(bucketName,filePath);
    }

    public List<S3ObjectIdentifier> getDirectoryContent(String bucket, String key, Filter<S3ObjectSummary> filter) {
        List<S3ObjectIdentifier> result = new ArrayList<S3ObjectIdentifier>();
        ObjectListing listing = s3Client.listObjects(bucket,key);
        List<S3ObjectSummary> summaryList = listing.getObjectSummaries();
        for(S3ObjectSummary s3ObjectSummary : summaryList) {
            if(filter == null) {
                String objectKey = s3ObjectSummary.getKey();
                String objectName = getFileName(objectKey);
                result.add(new S3ObjectIdentifier(bucket,objectKey,objectName));
            } else {
                if(!filter.shouldFilter(s3ObjectSummary)){
                    String objectKey = s3ObjectSummary.getKey();
                    String objectName = getFileName(objectKey);
                    result.add(new S3ObjectIdentifier(bucket,objectKey,objectName));
                }
            }
        }
        return result;
    }

    private static String getFileName(String key) {
        int index = key.lastIndexOf("/");
        return key.substring(index+1);
    }

    public InputStream getInputStream(String bucketName,String fileName) {
        GetObjectRequest request = new GetObjectRequest(bucketName,fileName);
        S3Object object = s3Client.getObject(request);
        InputStream inputStream = object.getObjectContent();
        return inputStream;
    }

    public void uploadLocalFile(String bucketName,String s3Key, String localFilePath) {
        File localFile = new File(localFilePath);

        if(!localFile.exists()) {
            throw new IllegalArgumentException("File not found." + localFilePath);
        }
        PutObjectRequest request = new PutObjectRequest(bucketName,s3Key,localFile);
        s3Client.putObject(request);
    }

    /**
     * Returns a seekable stream from a S3 location.
     *
     * @param filePath
     * @return
     */
    public OpenJDK7GZIPInputStream getSeekableStream(String filePath) throws Exception {
        InputStream inputStream = getInputStream(filePath);
        OpenJDK7GZIPInputStream seekableInputStream = new OpenJDK7GZIPInputStream(inputStream);
        return seekableInputStream;
    }

    public InputStream getSeekableStreamForMinute(String filePath,int minute) throws Exception {
        String indexFileName = getIndexFileName(filePath);
        Map<Integer,Long> index = getIndex(indexFileName);

        OpenJDK7GZIPInputStream stream = getSeekableStream(filePath);
        stream.skip(index.get(minute));
        return stream;
    }

    private String getIndexFileName(String filePath) {
        int separator = filePath.indexOf(".");
        String fileName = filePath.substring(0,separator);
        return fileName + "." + "idx";
    }


    public Map<Integer,Long> getIndex(String filepath) throws Exception {
        Map<Integer,Long> index = null;

        if(isCacheEnabled()) {
            index = cache.get(filepath);
        } else {
            index = cacheLoader.load(filepath);
        }
        return index;
    }

    private boolean isCacheEnabled() {
        return IS_INDEX_CACHE_ENABLED;
    }
}
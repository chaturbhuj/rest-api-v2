package com.integral.ds.s3;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.google.common.base.Optional;

public final class S3Utils {

	private static final Logger LOGGER = Logger.getLogger(S3Utils.class);

	private static final AWSCredentials _credentials;
	private final static String accessKey = "AKIAI4JZNEOZDPEU2R6A";
	private final static String secretKey = "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";

	static {
		_credentials = new BasicAWSCredentials(accessKey, secretKey);

	}

	/**
	 * Extracts the list of files from the S3 bucket and a specific folder
	 * 
	 * @param bucketName
	 * @param folderName
	 * @return
	 */
	public static List<String> getListOfFiles(String bucketName,
			String folderName) {
		AmazonS3 _s3 = new AmazonS3Client(_credentials);
		ListObjectsRequest request = new ListObjectsRequest().withBucketName(
				bucketName).withPrefix(folderName);
		ObjectListing objectList = _s3.listObjects(request);
		List<String> filesList = new ArrayList<>();
		do {
			for (S3ObjectSummary objectSummary : objectList
					.getObjectSummaries()) {

				filesList.add(objectSummary.getKey());
			}
			objectList = _s3.listNextBatchOfObjects(objectList);
		} while (objectList.isTruncated());

		return filesList;
	}

	public static Optional<File> downloadFile(String bucketName,
			String s3FileName) {
		File localFile = new File(s3FileName);

		TransferManager transferManager = new TransferManager(_credentials);

		try {
			transferManager.download(bucketName, s3FileName, localFile)
					.waitForCompletion();
			return Optional.of(localFile);

		} catch (Exception ex) {
			LOGGER.log(Level.ERROR,
					"Transfer failed for copying file from s3: " + bucketName
							+ " s3FileName " + s3FileName + " to local : "
							+ localFile.getAbsolutePath() + " with reason as "
							+ ex);
			return Optional.absent();
		} finally {
			if (transferManager != null) {
				transferManager.shutdownNow();
			}
		}
	}

}

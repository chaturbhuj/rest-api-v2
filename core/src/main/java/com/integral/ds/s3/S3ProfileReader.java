package com.integral.ds.s3;

import com.integral.ds.emscope.profile.ProfileRecord;
import com.integral.ds.s3.impl.ProfileRecordTransformer;
import com.integral.ds.util.DataQueryUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.GZIPInputStream;

/**
 * Reads profile from S3 profile file dumps.
 *
 * @author Rahul Bhattacharjee
 */
public class S3ProfileReader {

    private static final Logger LOGGER = Logger.getLogger(S3StreamReader.class);

    private static final RecordTransformer<ProfileRecord> TRANSFORMER = new ProfileRecordTransformer();
    private static final S3StreamReader S3_READER = new S3StreamReader();

    /**
     * Returns a list of profile record for the given parameters.
     *
     * @param provider
     * @param stream
     * @param ccyPair
     * @param start
     * @param end
     * @return
     */
    public List<ProfileRecord> getProfileRecords(String provider,String stream,String ccyPair,Date start,Date end) {
        List<ProfileRecord> result = new ArrayList<ProfileRecord>();

        List<DataQueryUtils.S3FileInfo> listOfS3Files = DataQueryUtils.getS3FileInfoForProfile(provider, stream, ccyPair,start, end);
        for(DataQueryUtils.S3FileInfo fileInfo : listOfS3Files) {
            addProfileRecordsToResult(fileInfo, result, start, end);
        }
        return result;
    }

    private void addProfileRecordsToResult(DataQueryUtils.S3FileInfo fileInfo, List<ProfileRecord> result, Date start, Date end) {
        String fileName = fileInfo.getFilePath();
        Filter<ProfileRecord> breakFilter = new BreakFilter(end);
        Filter<ProfileRecord> rangeFilter = new TimeRangeFilter(start,end);

        InputStream inputStream = null;
        GZIPInputStream gzipInputStream = null;
        BufferedReader reader = null;
        try {
            inputStream = S3_READER.getInputStream(fileName);
            gzipInputStream = new GZIPInputStream(inputStream);
            reader = new BufferedReader(new InputStreamReader(gzipInputStream));
            String line = "";

            while(line != null) {
                line = reader.readLine();
                if(StringUtils.isBlank(line)) {
                    continue;
                }
                ProfileRecord profile = TRANSFORMER.transform(line);
                if(breakFilter.shouldFilter(profile)) {
                    break;
                }
                if(!rangeFilter.shouldFilter(profile)) {
                    result.add(profile);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Exception while reading profile file.",e);
        } finally {
            closeStreams(inputStream,gzipInputStream,reader);
        }
    }

    private void closeStreams(InputStream inputStream, GZIPInputStream gzipInputStream, BufferedReader reader) {
        if(reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                LOGGER.error("Exception while closing reader.",e);
            }
        }
        if(gzipInputStream != null) {
            try {
                gzipInputStream.close();
            } catch (Exception e) {
                LOGGER.error("Exception while closing gzip input stream.",e);
            }
        }
        if(inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                LOGGER.error("Exception while closing input stream.",e);
            }
        }
    }

    private static class TimeRangeFilter implements Filter<ProfileRecord> {
        private long start;
        private long end;

        private TimeRangeFilter(Date start, Date end) {
            this.start = start.getTime();
            this.end = end.getTime();
        }

        @Override
        public boolean shouldFilter(ProfileRecord profile) {
            long currentEpoch = profile.getTime();
            return (currentEpoch > start && currentEpoch < end) ? false : true;
        }
    }

    private static class BreakFilter implements Filter<ProfileRecord> {
        private long endTime;

        private BreakFilter(Date endTime) {
            this.endTime = endTime.getTime();
        }

        @Override
        public boolean shouldFilter(ProfileRecord profile) {
            return profile.getTime() > endTime ? true : false;
        }
    }
}

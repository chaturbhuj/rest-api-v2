package com.integral.ds.s3;

import com.integral.ds.emscope.rates.BrokerRateObject;
import com.integral.ds.util.DataQueryUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Duplicate logic from S3RatesReader. Refactor this class to separate common code to abstract class and two extended classes
 * for LPRates and BrokerRates.
 *
 * @author Rahul Bhattacharjee
 */
public class S3BrokerRatesReader {

    private static final Logger LOGGER = Logger.getLogger(S3BrokerRatesReader.class);

    private final S3StreamReader s3StreamReader = new S3StreamReader();

    /**
     *  Fetches rates for provider , stream , currency pair for a given time period.
     *
     * @param provider
     * @param stream
     * @param fromTime
     * @param endTime
     * @param ccyPair
     * @return
     */
    public List<BrokerRateObject> getRates(String provider, String stream,Date fromTime, Date endTime,String ccyPair) {
        List<BrokerRateObject> ratesObjectList = new ArrayList<BrokerRateObject>();
        List<DataQueryUtils.S3FileInfo> listOfS3Files = DataQueryUtils.getS3FileListForLongerDuration(fromTime,endTime,provider,stream,ccyPair);

        LOGGER.debug("List of files to fetch => " + listOfS3Files);

        for(DataQueryUtils.S3FileInfo fileInfo : listOfS3Files) {
            populateListWithRatesObject(fileInfo, ccyPair, ratesObjectList);
        }
        return ratesObjectList;
    }

    private void populateListWithRatesObject(DataQueryUtils.S3FileInfo fileInfo ,final String ccyPair, List<BrokerRateObject> ratesObjectList) {
        final String filePath = fileInfo.getFilePath();
        final Date fromTime = fileInfo.getFromTime();
        final Date toTime = fileInfo.getToTime();

        Filter<BrokerRateObject> ratesObjectFilter = new TimeRangeFilter(fromTime,toTime);
        Filter<BrokerRateObject> ratesBreakFilter = new TimeRangeFilter(fromTime,toTime);
        Filter<BrokerRateObject> ccyPairFilter = new CurrencyPairFilter(ccyPair);

        RecordTransformer<BrokerRateObject> ratesTransformer = new BrokerRateObjectTransformer();
        InputStream inputStream = null;
        ZipInputStream zipStream = null;
        BufferedReader reader = null;
        BufferedInputStream bufferedInputStream = null;

        try {
            inputStream = s3StreamReader.getInputStream(filePath);
            bufferedInputStream = new BufferedInputStream(inputStream);
            zipStream = new ZipInputStream(bufferedInputStream);

            List<String> filters = getFilterList(fileInfo.getStartMinute(),fileInfo.getEndMinute());
            ZipEntry entry = null;

            while((entry = zipStream.getNextEntry()) != null) {
                if(entryContains(entry.getName(),filters)) {
                    reader = new BufferedReader(new InputStreamReader(zipStream));
                    String record = "";

                    while(record != null) {
                        record = reader.readLine();
                        if(StringUtils.isBlank(record)){
                            continue;
                        }
                        BrokerRateObject ratesObject = ratesTransformer.transform(record);
                        if(ratesBreakFilter.shouldFilter(ratesObject)) {
                            break;
                        } else {
                            if(!ccyPairFilter.shouldFilter(ratesObject)) {
                                if(!ratesObjectFilter.shouldFilter(ratesObject)){
                                    ratesObjectList.add(ratesObject);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Exception while fetching rates from S3.S3 Object path => " + fileInfo.getFilePath() , e);
        } finally {
            close(inputStream,bufferedInputStream,zipStream,reader);
        }
    }

    private void close(InputStream inputStream ,BufferedInputStream bufferedInputStream ,ZipInputStream zipStream ,BufferedReader reader) {
        if(inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                LOGGER.error("Exception while closing inputstream.",e);
            }
        }
        if(bufferedInputStream != null) {
            try {
                bufferedInputStream.close();
            } catch (IOException e) {
                LOGGER.error("Exception while closing buffered input stream.", e);
            }
        }
        if(zipStream != null) {
            try {
                zipStream.close();
            } catch (IOException e) {
                LOGGER.error("Exception while closing zip stream.", e);
            }
        }
        if(reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                LOGGER.error("Exception while closing buffered reader.", e);
            }
        }
    }

    public static List<String> getFilterList(int start , int end) {
        List<String> filters = new ArrayList<>();
        for(int i = start ; i <= end ; i++) {
            filters.add("_" + i + ".csv");
        }
        return filters;
    }

    public static boolean entryContains(String entryName , List<String> filters) {
        for(String filter : filters) {
            if(entryName.contains(filter)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Filter to allow records only with the time range specified.
     */
    private static class TimeRangeFilter implements Filter<BrokerRateObject> {
        private long startTime;
        private long endTime;
        public TimeRangeFilter(Date fromTime,Date toTime) {
            this.startTime = fromTime.getTime();
            this.endTime = toTime.getTime();
        }

        @Override
        public boolean shouldFilter(BrokerRateObject obj) {
            long rateTime = obj.getTimeStamp();
            if(rateTime > startTime && rateTime < endTime){
                return false;
            }else {
                return true;
            }
        }
    }

    /**
     * Filter to stop iterating further after crossing the end time provided.
     */
    private static class BreakIterationFilter implements Filter<BrokerRateObject> {
        private long endTime;
        public BreakIterationFilter(Date endTime) {
            this.endTime = endTime.getTime();
        }
        @Override
        public boolean shouldFilter(BrokerRateObject obj) {
            if(obj.getTimeStamp() > endTime) {
                return true;
            }
            return false;
        }
    }

    /**
     * Filter records based on currency pairs.
     */
    private static class CurrencyPairFilter implements Filter<BrokerRateObject> {
        private String ccyPair;
        public CurrencyPairFilter(String ccyPair) {
            this.ccyPair = ccyPair;
        }

        @Override
        public boolean shouldFilter(BrokerRateObject obj) {
            return !(ccyPair.equalsIgnoreCase(obj.getCcyPair()));
        }
    }
}

package com.integral.ds.s3.impl;

import com.integral.ds.emscope.profile.ProfileRecord;
import com.integral.ds.s3.RecordTransformer;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public class ProfileRecordTransformer implements RecordTransformer<ProfileRecord> {

    //Provider, Stream, Currency Pair, Date Time, Tier, Has Data For Current Second, Two Sided Tick Count,
    // Mid Price Mean, Spread Average, Spread Range, Last Tick Timestamp, Last Tick Age (in milliseconds),
    // Average Bid Size, Average Ask Size, Max. bid price, Min. bid price, Max. ask price, Min. ask price,
    // Last Active Bid, Last Active Ask, Active Tick Count, Inactive Tick Count, Last Declared Status, Number of Bad Data

    //BOAN,BandC,AUDUSD,05.13.2014 15:59:25.000,1,true,8,
    // 0.9368237500000001,7.750000000006363E-5,1.0000000000065512E-5,05.13.2014 15:59:19.531,269,
    // 500000.0,500000.0,0.9368,0.93678,0.93686,0.93686,
    // 0.9368,0.93687,8,0,0,Active

    @Override
    public ProfileRecord transform(String recordString) {
        ProfileRecord record = new ProfileRecord();
        if(StringUtils.isBlank(recordString)) {
            return record;
        }

        try {
            String [] fields = recordString.split(",");
            String provider = fields[0];
            String stream = fields[1];
            String ccyPair = fields[2];

            SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
            Date date = dateFormat.parse(fields[3]);
            int tier = Integer.parseInt(fields[4]);
            boolean hasDataForCurrentSecond = Boolean.parseBoolean(fields[5]);
            long twoSidedTickCount = Long.parseLong(fields[6]);
            BigDecimal midMeanPrice = new BigDecimal(fields[7]);
            BigDecimal spreadAverage = new BigDecimal(fields[8]);
            BigDecimal spreadRange = new BigDecimal(fields[9]);
            Date lastTickTimeStamp = dateFormat.parse(fields[10]);
            long lastTickAge = Long.parseLong(fields[11]);
            BigDecimal averageBidSize = new BigDecimal(fields[12]);
            BigDecimal averageAskSize = new BigDecimal(fields[13]);
            BigDecimal maxBidPrice = new BigDecimal(fields[14]);
            BigDecimal minBidPrice = new BigDecimal(fields[15]);
            BigDecimal maxAskPrice = new BigDecimal(fields[16]);
            BigDecimal minAskPrice = new BigDecimal(fields[17]);
            BigDecimal lastActiveBid = new BigDecimal(fields[18]);
            BigDecimal lastActiveAsk = new BigDecimal(fields[19]);
            long activeTickCount = Long.parseLong(fields[20]);
            long inactiveTickCount = Long.parseLong(fields[21]);
            long noOfBadRecords = Long.parseLong(fields[22]);
            String status = fields[23];

            record.setProvider(provider);
            record.setStream(stream);
            record.setCcyPair(ccyPair);
            record.setTime(date.getTime());
            record.setTier(tier);
            record.setHasDataForCurrentSecond(hasDataForCurrentSecond);
            record.setTwoSidedTickCount(twoSidedTickCount);
            record.setMidPriceMean(midMeanPrice);
            record.setSpreadAverage(spreadAverage);
            record.setSpreadRange(spreadRange);
            record.setLastTickTimeStamp(lastTickTimeStamp.getTime());
            record.setLastTickAge(lastTickAge);
            record.setAverageBidSize(averageBidSize);
            record.setAverageAskSize(averageAskSize);
            record.setMaxBidPrice(maxBidPrice);
            record.setMinBidPrice(minBidPrice);
            record.setMaxAskPrice(maxAskPrice);
            record.setMinAskPrice(minAskPrice);
            record.setLastActiveBid(lastActiveBid);
            record.setLastActiveAsk(lastActiveAsk);
            record.setActiveTickCount(activeTickCount);
            record.setInactiveTickCount(inactiveTickCount);
            record.setNumberOfBadRecords(noOfBadRecords);
            record.setLastDeclaredStatus(status);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return record;
    }
}

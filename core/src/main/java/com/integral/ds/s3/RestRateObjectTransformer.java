package com.integral.ds.s3;

import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.emscope.rates.RestRatesObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Transforms a rate record into rest rates object.
 *
 * @author Rahul Bhattacharjee
 */
public class RestRateObjectTransformer implements RecordTransformer<RestRatesObject> {

    @Override
    public RestRatesObject transform(String record) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        RestRatesObject ratesObject = new RestRatesObject();
        try {
            String [] fields = record.split(",");

            String ccyPair = fields[2];
            String tmstmp = fields[3];
            String level = fields[4];
            String status = fields[5];
            String bidPrice = fields[6];
            String bidSize = fields[7];
            String askPrice = fields[8];
            String askSize = fields[9];
            String guid = fields[10];
            String meta = fields.length > 11 ? fields[11]:"";
            
            Date parsedDate = dateFormat.parse(tmstmp);

            ratesObject.setTmstmp(parsedDate.getTime());
            ratesObject.setTier(Integer.parseInt(level));
            ratesObject.setStatus(status);
            ratesObject.setBidPrice(new BigDecimal(bidPrice));
            ratesObject.setBidSize(new BigDecimal(bidSize));
            ratesObject.setAskPrice(new BigDecimal(askPrice));
            ratesObject.setAskSize(new BigDecimal(askSize));
            ratesObject.setGuid(guid);
            ratesObject.setCcyPair(ccyPair);
            ratesObject.setMetaInfo(meta);
        } catch (Exception e) {
            throw new IllegalArgumentException("Exception while transforming raw Rate record into Rate object.",e);
        }
        return ratesObject;
    }
}

package com.integral.ds.s3;

import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.s3.impl.RateRecordTransformer;
import com.integral.ds.util.DataQueryUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * S3 Rates zip stream reader.
 *
 * @author Rahul Bhattacharjee
 */
public class S3RatesReader {

    private static final Logger LOGGER = Logger.getLogger(S3RatesReader.class);

    private final S3StreamReader s3StreamReader = new S3StreamReader();

    /**
     *  Fetches rates for provider , stream , currency pair for a given time period.
     *
     * @param provider
     * @param stream
     * @param fromTime
     * @param endTime
     * @param ccyPair
     * @return
     */
    public List<RatesObject> getRates(String provider, String stream,Date fromTime, Date endTime,String ccyPair) {
        Map<String,List<RatesObject>> result = new HashMap<String,List<RatesObject>>();
        List<DataQueryUtils.S3FileInfo> listOfS3Files = DataQueryUtils.getS3FileListForLongerDuration(fromTime,endTime,provider,stream,ccyPair);

        LOGGER.debug("List of files to fetch => " + listOfS3Files);

        for(DataQueryUtils.S3FileInfo fileInfo : listOfS3Files) {
            try{
                List<String> ccyPairList = Collections.singletonList(ccyPair);
                populateListWithRatesObjectForCurrencyPairs(fileInfo,ccyPairList,result);
            } catch (Exception e) {
                LOGGER.error("Exception while fetching rate log files." + e.getMessage());
            }
        }
        List<RatesObject> rates = result.get(ccyPair);
        if(rates == null) {
            rates = Collections.EMPTY_LIST;
        }
        return rates;
    }

    /**
     *
     * Fetches rates for provider , stream , list of currency pairs for a given time period.
     *
     * @param provider
     * @param stream
     * @param start
     * @param end
     * @param ccyPairs
     * @return
     */
    public Map<String,List<RatesObject>> getRates(String provider , String stream , Date start , Date end , List<String> ccyPairs) {
        Map<String,List<RatesObject>> result = new HashMap<String,List<RatesObject>>();
        String exoticCurrencyIdentifier = "EURRUB";
        List<DataQueryUtils.S3FileInfo> listOfS3Files = DataQueryUtils.getS3FileListForLongerDuration(start,end,provider,stream,exoticCurrencyIdentifier);

        LOGGER.debug("List of files to fetch => " + listOfS3Files);

        for(DataQueryUtils.S3FileInfo fileInfo : listOfS3Files) {
            try{
                populateListWithRatesObjectForCurrencyPairs(fileInfo,ccyPairs,result);
            } catch (Exception e) {
                LOGGER.error("Exception while fetching rate log files." + e.getMessage());
            }
        }
        return result;
    }

    private void populateListWithRatesObjectForCurrencyPairs(DataQueryUtils.S3FileInfo fileInfo,
                                                             List<String> currencyPairs,Map<String,List<RatesObject>> result) {
        final String filePath = fileInfo.getFilePath();
        final Date fromTime = fileInfo.getFromTime();
        final Date toTime = fileInfo.getToTime();

        Filter<RatesObject> ratesObjectFilter = new TimeRangeFilter(fromTime,toTime);
        Filter<RatesObject> ratesBreakFilter = new BreakIterationFilter(toTime);
        Filter<RatesObject> ccyPairFilter = new MultipleCurrencyPairIdentifier(currencyPairs);

        RecordTransformer<RatesObject> ratesTransformer = new RateRecordTransformer();
        InputStream inputStream = null;
        ZipInputStream zipStream = null;
        BufferedReader reader = null;
        BufferedInputStream bufferedInputStream = null;

        try {
            inputStream = s3StreamReader.getInputStream(filePath);
            bufferedInputStream = new BufferedInputStream(inputStream);
            zipStream = new ZipInputStream(bufferedInputStream);
            List<String> filters = getFilterList(fileInfo.getStartMinute(),fileInfo.getEndMinute());
            ZipEntry entry = null;

            while((entry = zipStream.getNextEntry()) != null) {
                try {
                    if(entryContains(entry.getName(),filters)) {
                        reader = new BufferedReader(new InputStreamReader(zipStream));
                        String record = "";

                        while(record != null) {
                            record = reader.readLine();
                            if(StringUtils.isBlank(record)){
                                continue;
                            }
                            RatesObject ratesObject = ratesTransformer.transform(record);
                            if(ratesBreakFilter.shouldFilter(ratesObject)) {
                                break;
                            } else {
                                if(!ccyPairFilter.shouldFilter(ratesObject)) {
                                    if(!ratesObjectFilter.shouldFilter(ratesObject)){
                                        addToAppropriateRatesList(result,ratesObject);
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    LOGGER.warn("Failed to parse file " + entry.getName());
                }
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Exception while fetching rates from S3.S3 Object path => " + fileInfo.getFilePath());
        } finally {
            close(inputStream,bufferedInputStream,zipStream,reader);
        }
    }

    private void addToAppropriateRatesList(Map<String,List<RatesObject>> result,RatesObject rate) {
        String ccyPair = rate.getCcyp();
        List<RatesObject> rates = result.get(ccyPair);
        if(rates == null) {
            rates = new ArrayList<RatesObject>();
            result.put(ccyPair,rates);
        }
        rates.add(rate);
    }

    private void close(InputStream inputStream ,BufferedInputStream bufferedInputStream ,ZipInputStream zipStream ,BufferedReader reader) {
        if(inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                LOGGER.error("Exception while closing inputstream.",e);
            }
        }
        if(bufferedInputStream != null) {
            try {
                bufferedInputStream.close();
            } catch (IOException e) {
                LOGGER.error("Exception while closing buffered input stream.", e);
            }
        }
        if(zipStream != null) {
            try {
                zipStream.close();
            } catch (IOException e) {
                LOGGER.error("Exception while closing zip stream.", e);
            }
        }
        if(reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                LOGGER.error("Exception while closing buffered reader.", e);
            }
        }
    }

    public static List<String> getFilterList(int start , int end) {
        List<String> filters = new ArrayList<>();
        for(int i = start ; i <= end ; i++) {
            filters.add("_" + i + ".csv");
        }
        return filters;
    }

    public static boolean entryContains(String entryName , List<String> filters) {
        for(String filter : filters) {
            if(entryName.contains(filter)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Filter to allow records only with the time range specified.
     */
    private static class TimeRangeFilter implements Filter<RatesObject> {
        private long startTime;
        private long endTime;
        public TimeRangeFilter(Date fromTime,Date toTime) {
            this.startTime = fromTime.getTime();
            this.endTime = toTime.getTime();
        }

        @Override
        public boolean shouldFilter(RatesObject obj) {
            long rateTime = obj.getTmstmp();
            if(rateTime > startTime && rateTime < endTime){
                return false;
            }else {
                return true;
            }
        }
    }

    /**
     * Filter to stop iterating further after crossing the end time provided.
     */
    private static class BreakIterationFilter implements Filter<RatesObject> {
        private long endTime;
        public BreakIterationFilter(Date endTime) {
            this.endTime = endTime.getTime();
        }
        @Override
        public boolean shouldFilter(RatesObject obj) {
            if(obj.getTmstmp() > endTime) {
                return true;
            }
            return false;
        }
    }

    /**
     * Filter to select multiple currency pairs.
     */
    private static class MultipleCurrencyPairIdentifier implements Filter<RatesObject> {
        private List<String> ccyPairs;

        public MultipleCurrencyPairIdentifier(List<String> ccyPairs) {
            this.ccyPairs = ccyPairs;
        }

        @Override
        public boolean shouldFilter(RatesObject obj) {
            String ccyPair = obj.getCcyp();
            return (ccyPairs.contains(ccyPair)) ? false : true ;
        }
    }
}

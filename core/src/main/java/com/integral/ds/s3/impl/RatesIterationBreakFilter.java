package com.integral.ds.s3.impl;

import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.s3.Filter;

import java.util.Date;

/**
 * Hints the processor to not fetch anymore records.
 *
 * @author Rahul Bhattacharjee
 */
public class RatesIterationBreakFilter implements Filter<RestRatesObject> {

    private long endTime;

    public RatesIterationBreakFilter(Date endTime) {
        this.endTime = endTime.getTime();
    }

    @Override
    public boolean shouldFilter(RestRatesObject ratesObject) {
        if(ratesObject.getTmstmp() > endTime) {
            return true;
        }
        return false;
    }
}

package com.integral.ds.staticdata;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.integral.ds.util.ResourceUtils;
import com.owlike.genson.Genson;

public class StaticDataManager {

    private static final Logger LOGGER = Logger.getLogger(StaticDataManager.class);

    private Set<String> currencyPairList = null;
	private Set <String> providerList = null;
	private Map<String, List<String>> streamMappings = null;
	private Set<String> customerList = null;
	private Set<String> orderTypeList = null;
	private Map<ProviderKey, ProviderValue> providerKeyValueMap;
    private Properties appConfig;
	
    private static final ResourceUtils RESOURCE_UTILS = new ResourceUtils();
    private static final StaticDataManager localInstance = new StaticDataManager();

    private static final String CCYPAIR_FILE = "ccypair.txt";
	private static final String PROVIDER_STREAM = "providerStream.txt";
	private static final String CUSTOMER_FILE = "customers.txt";
	private static final String ORDERTYPE_FILE = "orderTypes.txt";
	private static final String PRITAM_FILE = "providers.txt";
    private static final String APP_CONFIG = "application.properties";

	private StaticDataManager ()	{
		currencyPairList = new HashSet<>();
		providerList = new HashSet<>();
		streamMappings = new HashMap<String, List<String>>();
		customerList = new HashSet<>();
		orderTypeList = new HashSet<>();
		providerKeyValueMap = new HashMap<>();
        appConfig = new Properties();
		init();
	}
	
	public static StaticDataManager getInstance ()	{
		return localInstance;
	}
	
	private void init()	{
		LOGGER.info("Loading static data manager components.");
		initCurrencyPair();
		initProviderList();
		initCustomer();
		initOrderTypes();
		initPritmFile();
        initAppConfig();
	}

    private void initAppConfig() {
        InputStream in = null;
        try {
            in = RESOURCE_UTILS.getInputStream(APP_CONFIG);
            appConfig.load(in);
        } catch (Exception e) {
            LOGGER.error("Exception in loading application config.",e);
        } finally {
            RESOURCE_UTILS.close(in);
        }
    }

    public Set<String> getListOfCurrencyPairs ()	{
		return currencyPairList;
	}
	
    public String getCurrencyPairsAsJson() {
		Set <String> listOfCurrencyPairs = getListOfCurrencyPairs();
		Genson genson = new Genson();
		try	{
			String returnValue = genson.serialize(listOfCurrencyPairs);
			return returnValue;
		}
		catch (Exception exception)	{
			
		}
		return "";
	}
    
	public Set<String> getProviderList ()	{
		return providerList;
	}
	
	public List<String> getStreamList (String provider)	{
		return streamMappings.get(provider);
	}
	
	public Set<String> getCustomer ()	{
		return customerList;
	}
	
	public Set<String> getOrderType ()	{
		return orderTypeList;
	}
	
	public Map<ProviderKey, ProviderValue> getProviderMappingForTrades ()	{
		return providerKeyValueMap;
	}
	
	private void initCurrencyPair()	{
        InputStream in = null;
		try	{
			in = RESOURCE_UTILS.getInputStream(CCYPAIR_FILE);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line = "";
			while (line != null) {
	            line = br.readLine();
                if(StringUtils.isNotBlank(line)){
                    String ccyPair = line.split(",")[0];
                    currencyPairList.add(ccyPair);
                }
	        }
		} catch (Exception exception) {
            LOGGER.error("Exception while initCurrencyPair",exception);
        } finally {
            RESOURCE_UTILS.close(in);
        }
	}
	
	private void initCustomer()	{
        InputStream in = null;
		try	{
            in = RESOURCE_UTILS.getInputStream(CUSTOMER_FILE);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line = "";
			while (line != null) {
	            line = br.readLine().trim();
                if(StringUtils.isNotBlank(line)) {
	                customerList.add(line);
                }
	        }
		} catch (Exception exception)	{
			LOGGER.error("Exception while initCustomers",exception);
		} finally {
            RESOURCE_UTILS.close(in);
        }
	}

	private void initOrderTypes()	{
        InputStream in = null;
		try	{
			in = RESOURCE_UTILS.getInputStream(ORDERTYPE_FILE);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line = "";
			while (line != null) {
	            line = br.readLine().trim();
                if(StringUtils.isNotBlank(line)) {
	                orderTypeList.add(line);
                }
	        }
		} catch (Exception exception)	{
			LOGGER.error("Exception while initOrderTypes",exception);
		} finally {
            RESOURCE_UTILS.close(in);
        }
	}

	private void initProviderList()	{
        InputStream in = null;
		try	{
			in = RESOURCE_UTILS.getInputStream(PROVIDER_STREAM);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line = "";
			while (line != null) {
	            line = br.readLine();
                    if(StringUtils.isNotBlank(line)) {
                    String [] array = line.split("-");
                    String provider = array[0];
                    String stream = array[1];
                    providerList.add(provider);

                    List<String> listOfStreams = streamMappings.get(provider);
                    if (listOfStreams == null){
                        listOfStreams = new ArrayList<>();
                        streamMappings.put(provider, listOfStreams);
                    }
                    listOfStreams.add(stream);
                }
	        }
		} catch (Exception exception)	{
		    LOGGER.error("Exception while initProviderStream.",exception);
		} finally {
            RESOURCE_UTILS.close(in);
        }
	}

    private void initPritmFile() {
        InputStream in = null;
		try	{
			in = RESOURCE_UTILS.getInputStream(PRITAM_FILE);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line = "";
			while (line != null) {
				line = br.readLine().trim();
                if(StringUtils.isNotBlank(line)) {
                    ProviderKey key = new ProviderKey();
                    ProviderValue value = new ProviderValue();
                    String [] valueArray = line.split(",");
                    key.setCpty(valueArray[0]);
                    key.setStreamMK(valueArray[1]);

                    value.setTickcpty(valueArray[2]);
                    value.setTickstream(valueArray[3]);
                    providerKeyValueMap.put(key, value);
                }
			}
		} catch (Exception exception)	{
			LOGGER.error("Exception while loading file",exception);
		} finally {
            RESOURCE_UTILS.close(in);
        }
	}

    public String getApplicationProperty(String key) {
        return (String) appConfig.get(key);
    }
}
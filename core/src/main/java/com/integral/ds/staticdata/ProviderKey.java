package com.integral.ds.staticdata;

public class ProviderKey {

	public ProviderKey( String cpty, String streamMK) {
		super();
		this.streamMK = streamMK;
		this.cpty = cpty;
	}
	private String streamMK;
	private String cpty;
	
	
	public String getStreamMK() {
		return streamMK;
	}
	public void setStreamMK(String streamMK) {
		this.streamMK = streamMK;
	}
	public String getCpty() {
		return cpty;
	}
	public void setCpty(String cpty) {
		this.cpty = cpty;
	}
	
	public ProviderKey() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpty == null) ? 0 : cpty.hashCode());
		result = prime * result
				+ ((streamMK == null) ? 0 : streamMK.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProviderKey other = (ProviderKey) obj;
		if (cpty == null) {
			if (other.cpty != null)
				return false;
		} else if (!cpty.equals(other.cpty))
			return false;
		if (streamMK == null) {
			if (other.streamMK != null)
				return false;
		} else if (!streamMK.equals(other.streamMK))
			return false;
		return true;
	}
	
}

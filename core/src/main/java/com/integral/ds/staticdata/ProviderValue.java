package com.integral.ds.staticdata;

public class ProviderValue {
	
	private String tickcpty;
	private String tickstream;
	
	public String getTickcpty() {
		return tickcpty;
	}
	public void setTickcpty(String tickcpty) {
		this.tickcpty = tickcpty;
	}
	public String getTickstream() {
		return tickstream;
	}
	public void setTickstream(String tickstream) {
		this.tickstream = tickstream;
	}
}

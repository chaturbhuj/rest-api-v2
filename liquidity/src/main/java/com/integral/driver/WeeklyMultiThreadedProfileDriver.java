package com.integral.driver;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Weekly runner of LPA.
 *
 * @author Rahul Bhattacharjee
 */
public class WeeklyMultiThreadedProfileDriver extends BaseMultiThreadedProfileDriver {

    private static final String NO_OF_SAMPLES = "1000";

    public static void main(String [] argv) throws Exception {
        WeeklyMultiThreadedProfileDriver weeklyRun = new WeeklyMultiThreadedProfileDriver();
        weeklyRun.run(argv);
    }

    public void run(String [] argv) throws Exception {
        String baseLocation = ".";
        if(argv.length == 1) {
            baseLocation = argv[0];
        }

        //String startOfWeek = "06.23.2014";
        //String endOfWeek = "06.28.2014";

        //String startOfWeek = "06.24.2014";
        //String endOfWeek = "06.28.2014";

        //String startOfWeek = "07.14.2014";
        //String endOfWeek = "07.17.2014";

        // conf - 1
        //String startOfWeek = "08.04.2014";
        //String endOfWeek = "08.09.2014";

        String startOfWeek = "09.08.2014";
        String endOfWeek = "09.12.2014";

        //String startOfWeek = "08.06.2014";
        //String endOfWeek = "08.09.2014";

        //String startOfWeek = "07.17.2014";
        //String endOfWeek = "07.19.2014";

        //String startOfWeek = "06.01.2014";
        //String endOfWeek = "07.01.2014";

        //String startOfWeek = "07.01.2014";
        //String endOfWeek = "07.02.2014";

        List<TimeRange> timeRanges = getTimeRange(startOfWeek,endOfWeek);

        long startTime = System.currentTimeMillis();

        for(TimeRange range : timeRanges) {
            Date start = range.getStart();
            Date end = range.getEnd();

            String startString = getFormattedDate(start);
            String endString = getFormattedDate(end);

            String reportDir = getReportsDir(baseLocation,start);
            System.out.println("Running for Start " + startString + " End " + endString + " for samples " + NO_OF_SAMPLES
                    + " reports base dir " + reportDir);

            runPreviousDay(startString,endString,NO_OF_SAMPLES,reportDir);
        }

        System.out.println("Time taken for the entire run : " + (System.currentTimeMillis() - startTime));
    }

    private String getFormattedDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        return dateFormat.format(date);
    }
}











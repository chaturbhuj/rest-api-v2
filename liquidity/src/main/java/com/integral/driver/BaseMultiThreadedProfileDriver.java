package com.integral.driver;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class BaseMultiThreadedProfileDriver {

    protected void runPreviousDay(String startTime,String endTime,String noOfSamples,String reportDir) throws Exception {
        String [] arguments = new String[4];
        arguments[0] = startTime;
        arguments[1] = endTime;
        arguments[2] = noOfSamples;
        arguments[3] = reportDir;
        MultiThreadedLiquidityProfileDriver.main(arguments);
    }

    protected String getReportsDir(String baseLocation ,Date currentDay) {
        SimpleDateFormat dateFormatYear = new SimpleDateFormat("y");
        SimpleDateFormat dateFormatYearMonth = new SimpleDateFormat("MM-y");
        SimpleDateFormat dateFormatDay = new SimpleDateFormat("MM-dd-y");

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(baseLocation+ "/reports/");
        stringBuilder.append(dateFormatYear.format(currentDay)+"/");
        stringBuilder.append(dateFormatYearMonth.format(currentDay)+"/");
        stringBuilder.append(dateFormatDay.format(currentDay));

        return stringBuilder.toString();
    }

    protected String getTimeStamp(Date day) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y");
        String dayString = dateFormat.format(day);
        return dayString + " 00:00:00.000";
    }

    protected Date getPreviousDay(Date day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(day);
        calendar.add(Calendar.DATE,-1);
        return calendar.getTime();
    }

    protected List<TimeRange> getTimeRange(String startOfWeek, String endOfWeek) {
        List<TimeRange> timeRanges = new ArrayList<TimeRange>();
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y");
            Date start = dateFormat.parse(startOfWeek);
            Date end = dateFormat.parse(endOfWeek);
            Date currentStart = start;
            Date currentEnd = null;

            while(currentStart.getTime() < end.getTime()) {
                currentEnd = addDay(currentStart);
                TimeRange timeRange = new TimeRange();
                timeRange.setStart(currentStart);
                timeRange.setEnd(currentEnd);
                timeRanges.add(timeRange);
                currentStart = currentEnd;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return timeRanges;
    }

    private Date addDay(Date currentStart) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentStart);
        calendar.add(Calendar.DATE,1);
        return calendar.getTime();
    }
}

package com.integral.driver;

import com.integral.Constants;
import com.integral.aggregate.AggregatedBookCalculator;
import com.integral.aggregate.AggregatedReportGenerator;
import com.integral.aggregate.ContributionReportGenerator;
import com.integral.driver.task.LiquidityProfileRunner;
import com.integral.ds.dto.ProviderStream;
import com.integral.profile.LiquidityProfileGenerator;
import com.integral.profile.ProfileConfig;
import com.integral.reporter.ReportFilter;
import com.integral.reporter.bean.*;
import com.integral.reporter.impl.ReportFilterImpl;
import com.integral.serializer.ReportSerializer;
import com.integral.serializer.TQDSerializer;
import com.integral.serializer.impl.CSVMetaReportSerializer;
import com.integral.serializer.impl.TQDFileSerializerImpl;
import com.integral.ts.TSReportGenerator;
import com.integral.util.ResourceUtil;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Multi threaded LP runner.
 *
 * @author Rahul Bhattacharjee
 */
public class MultiThreadedLiquidityProfileDriver {

    private static final Logger logger = Logger.getLogger(MultiThreadedLiquidityProfileDriver.class);

    public static void main(String [] arguments) throws Exception {
        configureLogger();

        if(arguments.length != 4) {
            System.out.println("Program parameters : start_time end_time no_of_samples report_dir");
        }

        String startTimestamp = arguments[0];
        String endTimestamp = arguments[1];
        int noOfSamples = Integer.parseInt(arguments[2]);
        String reportDir = arguments[3];
        logger.info("Starting to run lp reporting. Start " + startTimestamp + " end " + endTimestamp + " no of samples " + noOfSamples);

        long startTime = System.currentTimeMillis();

        List<String> ccyPairs = ResourceUtil.getCurrencyPairs();
        List<ProviderStream> providerStreams = ResourceUtil.getProviderStreamList();

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        Date start = dateFormat.parse(startTimestamp);
        Date end = dateFormat.parse(endTimestamp);

        int noOfProcessors = Runtime.getRuntime().availableProcessors();
        ExecutorService executorService = Executors.newFixedThreadPool(noOfProcessors+1);

        List<Future<ReportPair>> futures = new ArrayList<Future<ReportPair>>();
        LiquidityProfileGenerator profileGenerator = new LiquidityProfileGenerator();

        for(ProviderStream providerStream : providerStreams) {
            for(String ccyPair : ccyPairs) {
                ProfileConfig profileConfig = getProfileConfig(providerStream,ccyPair,start,end,noOfSamples);
                Future<ReportPair> future = executorService.submit(new LiquidityProfileRunner(profileGenerator,profileConfig));
                futures.add(future);
            }
        }

        ReportHolder reportHolder = getReportHolderFor(futures);
        executorService.shutdown();

        AggregatedReportGenerator reportGenerator = new AggregatedReportGenerator();
        reportGenerator.generateAggregatedReport(reportHolder);

        ContributionReportGenerator contributionReportGenerator = new ContributionReportGenerator();
        contributionReportGenerator.generateAggregatedReport(reportHolder);

        ReportSerializer serializer = getSerializer(reportDir,start);

        try {
            serializer.serialize(reportHolder);
        } finally {
            serializer.close();
        }
        long endTime = System.currentTimeMillis();

        generateTQDFile(reportDir,reportHolder);

        System.out.println("Time taken in sec " + ((endTime-startTime)/1000));
        logger.info("Lp report took (sec) " + ((endTime-startTime)/1000));
    }

    private static void generateTQDFile(String reportDir, ReportHolder reportHolder) {
        if(ResourceUtil.isTQDFileGenerationEnabled()) {
            TSReportGenerator reportGenerator = new TSReportGenerator();
            TQDSerializer serializer = new TQDFileSerializerImpl();

            SerializerConfig config = new SerializerConfig();
            config.setFixedAttribute("Date/Time,String");
            config.addDynamicAttribute("bid","Float");
            config.addDynamicAttribute("offer","Float");

            Map<String,List<ProviderStream>> inputs = ResourceUtil.getAggregatedReportInputs();
            Iterator<Map.Entry<String,List<ProviderStream>>> iterator = inputs.entrySet().iterator();

            while(iterator.hasNext()) {
                Map.Entry<String,List<ProviderStream>> entry = iterator.next();
                String aggKey = entry.getKey();
                List<ProviderStream> providerStreams = entry.getValue();

                for(String ccyPair : ResourceUtil.getCurrencyPairs()) {
                    List<TSReport> result = reportGenerator.getReports(aggKey,providerStreams,ccyPair,reportHolder);
                    String fileName = reportDir + "/"  + aggKey + "-" + ccyPair + ".tqd";
                    config.setOutputFile(fileName);
                    serializer.serializer(config,result);
                }
            }
        }
    }

    private static void configureLogger() {
        String configFilename = "logger-config.xml";
        File file = new File(configFilename);
        System.out.println("File exists => " + file.exists());
        if(file.exists()) {
            DOMConfigurator.configure(configFilename);
        }
    }

    private static ProfileConfig getProfileConfig(ProviderStream providerStream, String ccyPair, Date start, Date end, int noOfSamples) {
        ProfileConfig profileConfig = new ProfileConfig();
        profileConfig.setProvider(providerStream.getProvider()).setStream(providerStream.getStream())
                .setCcyPair(ccyPair)
                .setProfileStartTime(start)
                .setProfileEndTime(end).setSamples(noOfSamples).setQuoteType(providerStream.getQuoteType());
        return profileConfig;
    }

    private static ReportHolder getReportHolderFor(List<Future<ReportPair>> futures) {
        ReportHolder holder = new ReportHolder();
        for(Future<ReportPair> future : futures) {
            try{
                ReportPair pair = future.get();
                holder.addSampleReport(pair.getSampleReport());
                holder.addPriceAtSizeReports(pair.getPriceAtSize());
                holder.addProfileEntries(pair.getProfileEntry());
            }catch (Exception e) {
                e.printStackTrace();
                System.out.println("ERROR: Exception while getting future." + e.getMessage());
            }
        }
        return holder;
    }

    private static ReportSerializer getSerializer(String reportDir,Date start) {
        Properties properties = new Properties();
        properties.put(Constants.REPORT_BASE_DIR,reportDir);
        ReportSerializer serializer = new CSVMetaReportSerializer();
        serializer.init(properties,start);
        return serializer;
    }
}

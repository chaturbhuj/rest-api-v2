package com.integral.driver;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Job to be schedules for every end of day.
 *
 * @author Rahul Bhattacharjee
 */
public class PreviousDayMultiThreadedProfileDriver extends BaseMultiThreadedProfileDriver {

    private static final String NO_OF_SAMPLES = "1000";

    public static void main(String [] argv) throws Exception {
        PreviousDayMultiThreadedProfileDriver driver = new PreviousDayMultiThreadedProfileDriver();
        driver.run(argv);
    }

    public void run(String [] argv) throws Exception {
        String baseLocation = ".";
        if(argv.length == 1) {
            baseLocation = argv[0];
        }

        Date currentDay = new Date();
        Date previousDay = getPreviousDay(currentDay);

        //String startTimestamp = getTimeStamp(previousDay);
        //String endTimestamp = getTimeStamp(currentDay);
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        String startTimestamp = "07.23.2014 00:00:00.000";
        String endTimestamp = "07.23.2014 18:00:00.000";

        String reportsDir = getReportsDir(baseLocation ,previousDay);

        System.out.println("Running for Start " + startTimestamp + " End " + endTimestamp + " for samples " + NO_OF_SAMPLES
                + " reports base dir " + reportsDir);

        runPreviousDay(startTimestamp, endTimestamp, NO_OF_SAMPLES, reportsDir);
    }
}

package com.integral.driver.task;

import com.integral.profile.LiquidityProfileGenerator;
import com.integral.profile.ProfileConfig;
import com.integral.reporter.bean.Report;
import com.integral.reporter.bean.ReportPair;

import java.util.concurrent.Callable;

/**
 * @author Rahul Bhattacharjee
 */
public class LiquidityProfileRunner implements Callable<ReportPair> {

    private LiquidityProfileGenerator profileGenerator;
    private ProfileConfig config;

    public LiquidityProfileRunner(LiquidityProfileGenerator profileGenerator, ProfileConfig config) {
        this.profileGenerator = profileGenerator;
        this.config = config;
    }

    @Override
    public ReportPair call() throws Exception {
        ReportPair report = profileGenerator.generateLiquidityProfile(config);
        System.out.println("Completed for " + report.getSampleReport().getProvider() + " ,Stream " + report.getSampleReport().getStream() +
                " ,Currency Pair " + report.getSampleReport().getCcyPair());
        return report;
    }
}

package com.integral;

/**
 * @author Rahul Bhattacharjee
 */
public class Constants {

    public static final String REPORT_BASE_DIR = "REPORT_DIR";
    public static final String APP_CCY_PAIRS = "app.ccy.pairs";
    public static final String APP_VOLUME = "app.volume";
    public static final String APP_CONFIG = "application.config";
    public static final String PROVIDER_STREAM_CONFIG_FILE_NAME = "ProviderStream.config";
    public static final String AGG_REPORT_CONFIG_FILE_NAME = "AggregatedReport.config";
    public static final String PROVIDER_STREAM_META_FILE_NAME = "ProviderStream.meta";

    public static final String DMSA_KEY = "enable.dsma";
    public static final String ENABLE_DEBUG_REPORT_KEY = "enable.debug.reports";
    public static final String ENVIRONMENT_KEY = "environment.type.dev";
    public static final String TQD_FILE_GENERATION = "enable.tqd.generation";
    public static final String IS_CONT_REPORT_ENABLED = "enable.contribution.report";
    public static final String PREVAILING_RATE_FILTER = "app.prevailing.rate.filter";
    public static final String IS_PROFILE_REPORT_ENABLED = "enable.profile.report";

}

package com.integral.tb;

import com.integral.reporter.bean.ReportEntry;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.Map;

/**
 * ReflectionUtil. At runtime retrospects a bean and invokes getter method on an object at runtime.
 *
 * @author Rahul Bhattacharjee
 */
public class ReflectionUtil {

    private static PropertyDescriptor[] propertyDescriptors;

    static {
        try {
            propertyDescriptors = Introspector.getBeanInfo(ReportEntry.class).getPropertyDescriptors();
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Exception while creating property descriptor.",e);
        }
    }

    public static boolean match(ReportEntry entry, Map<String, String> filter) throws Exception {
        int count = filter.size();
        for(Map.Entry<String,String> filterEntry : filter.entrySet()) {
            String key = filterEntry.getKey();
            String value = filterEntry.getValue();
            String beanValue = getValue(key,entry);
            if(value.equalsIgnoreCase(beanValue)) {
                count--;
            } else {
                break;
            }
        }
        if(count == 0) {
            return true;
        }
        return false;
    }

    public static String getValue(String key, ReportEntry entry) throws Exception {
        for (PropertyDescriptor pd : propertyDescriptors) {
            if (pd.getName().equalsIgnoreCase(key)) {
                Object val = pd.getReadMethod().invoke(entry);
                return val == null ? null : val.toString();
            }
        }
        return null;
    }
}

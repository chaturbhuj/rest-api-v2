package com.integral.tb.parser;

import com.integral.profile.ErrorType;
import com.integral.reporter.bean.ReportEntry;
import com.integral.reporter.bean.ReportEntryType;
import com.integral.ts.util.Transformer;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public class ReportEntryTransformer implements Transformer<String,ReportEntry>{

    //"PROVIDER,STREAM,QUOTE TYPE,CCYPAIR,RATE DATE(MM/dd/yyyy),RATE TIME,SAMPLE DATE,SAMPLE TIME,AGE,LEVEL,BID,BID VOLUME,OFFER,OFFER VOLUME,
    // AT VOLUME,SPREAD,TYPE,ERROR CODE,AGGREGATION KEY,MAX AGE,MIN AGE\n";

    @Override
    public ReportEntry transform(String input) {
        try {
            String [] splits = input.split(",",20);
            String provider = splits[0];
            String stream = splits[1];
            String ccypair = splits[3];
            String tier = splits[9];

            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");

            String rateDate = splits[4];
            String rateTime = splits[5];

            String rateDateTime = rateDate + " " + rateTime;

            Date rateTimestamp = format.parse(rateDateTime);

            String sampleDate = splits[6];
            String sampleTime = splits[7];

            String sampleDateTime = sampleDate + " " + sampleTime;

            Date sampleTimestamp = format.parse(sampleDateTime);

            String level =  splits[9];
            String bid = splits[10];
            String offer = splits[12];
            String type =  splits[16];
            String errorType = splits[17];
            String aggKey = splits[18];

            ReportEntry entry = new ReportEntry();
            entry.setProvider(provider);
            entry.setStream(stream);
            entry.setCcyPair(ccypair);
            entry.setTier(Integer.parseInt(tier));
            entry.setQuoteTime(rateTimestamp);
            entry.setSampleDateTime(sampleTimestamp);
            entry.setTier(Integer.parseInt(level));
            entry.setBid(new BigDecimal(bid));
            entry.setOffer(new BigDecimal(offer));
            entry.setEntryType(ReportEntryType.getTypeForId(type));
            entry.setErrorType(ErrorType.valueOf(errorType));
            entry.setAggregationType(aggKey);

            return entry;

        }catch (Exception e) {
            System.out.println("Exception while parsing record " + e.getMessage());
            //e.printStackTrace();
        }
        return null;
    }
}

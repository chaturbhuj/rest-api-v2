package com.integral.tb.parser;

import net.sf.jsqlparser.expression.*;
import net.sf.jsqlparser.expression.operators.arithmetic.*;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.*;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SubSelect;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Parses the SQL for generation of TQD metadata from LPA files.
 *
 * @author Rahul Bhattacharjee
 */
public class SQLQueryParser {

    public SQLTQDQuery getSqlQuery(String sql) {
        try {
            SQLTQDQuery query = new SQLTQDQuery();
            CCJSqlParserManager pm = new CCJSqlParserManager();
            PlainSelect plainSelect = (PlainSelect) ((Select) pm.parse(new StringReader(sql))).getSelectBody();

            query.setProjections(getProjections(plainSelect.getSelectItems()));
            query.setLpaDirectory(plainSelect.getFromItem().toString());

            Expression expression = plainSelect.getWhere();
            CustomExpressionVisitor expressionVisitor = new CustomExpressionVisitor(query);
            expression.accept(expressionVisitor);
            if(query.getFilters().size() == 0){
                query.addFilters(parser(expression));
            }
            return query;
        }catch (Exception e) {
            throw new IllegalArgumentException("Exception while parsing sql.SQL => " + sql,e);
        }
    }

    private List<String> getProjections(List<SelectExpressionItem> selectItems) {
        List<String> result = new ArrayList<String>();
        for(SelectExpressionItem item : selectItems) {
            result.add(item.toString());
        }
        return result;
    }

    private static class CustomExpressionVisitor implements ExpressionVisitor {

        private SQLTQDQuery query;

        public CustomExpressionVisitor(SQLTQDQuery query) {
            this.query = query;
        }

        @Override
        public void visit(NullValue nullValue) {
        }

        @Override
        public void visit(Function function) {
        }

        @Override
        public void visit(InverseExpression inverseExpression) {
        }

        @Override
        public void visit(JdbcParameter jdbcParameter) {
        }

        @Override
        public void visit(DoubleValue doubleValue) {
        }

        @Override
        public void visit(LongValue longValue) {
        }

        @Override
        public void visit(DateValue dateValue) {
        }

        @Override
        public void visit(TimeValue timeValue) {
        }

        @Override
        public void visit(TimestampValue timestampValue) {
        }

        @Override
        public void visit(Parenthesis parenthesis) {
        }

        @Override
        public void visit(StringValue stringValue) {
        }

        @Override
        public void visit(Addition addition) {
        }

        @Override
        public void visit(Division division) {
        }

        @Override
        public void visit(Multiplication multiplication) {
        }

        @Override
        public void visit(Subtraction subtraction) {
        }

        @Override
        public void visit(AndExpression andExpression) {
        }

        @Override
        public void visit(OrExpression orExpression) {
            Expression left = orExpression.getLeftExpression();
            Expression right = orExpression.getRightExpression();

            if(left instanceof OrExpression) {
                left.accept(this);
            } else {
                query.addFilters(parser(left));
            }

            if(right instanceof OrExpression) {
                right.accept(this);
            } else {
                query.addFilters(parser(right));
            }
        }

        @Override
        public void visit(Between between) {
        }

        @Override
        public void visit(EqualsTo equalsTo) {
        }

        @Override
        public void visit(GreaterThan greaterThan) {
        }

        @Override
        public void visit(GreaterThanEquals greaterThanEquals) {
        }

        @Override
        public void visit(InExpression inExpression) {
        }

        @Override
        public void visit(IsNullExpression isNullExpression) {
        }

        @Override
        public void visit(LikeExpression likeExpression) {
        }

        @Override
        public void visit(MinorThan minorThan) {
        }

        @Override
        public void visit(MinorThanEquals minorThanEquals) {
        }

        @Override
        public void visit(NotEqualsTo notEqualsTo) {
        }

        @Override
        public void visit(Column column) {
        }

        @Override
        public void visit(SubSelect subSelect) {
        }

        @Override
        public void visit(CaseExpression caseExpression) {
        }

        @Override
        public void visit(WhenClause whenClause) {
        }

        @Override
        public void visit(ExistsExpression existsExpression) {
        }

        @Override
        public void visit(AllComparisonExpression allComparisonExpression) {
        }

        @Override
        public void visit(AnyComparisonExpression anyComparisonExpression) {
        }

        @Override
        public void visit(Concat concat) {
        }

        @Override
        public void visit(Matches matches) {
        }

        @Override
        public void visit(BitwiseAnd bitwiseAnd) {
        }

        @Override
        public void visit(BitwiseOr bitwiseOr) {
        }

        @Override
        public void visit(BitwiseXor bitwiseXor) {
        }
    }

    private static Map<String,String> parser(Expression expression) {
        String expressionString = expression.toString();
        if(!expressionString.startsWith("(") || !expressionString.endsWith(")")) {
            throw new IllegalArgumentException("Parenthesis mismatch exception");
        }
        expressionString = expressionString.substring(1,(expressionString.length()-1));
        String [] splits = expressionString.split("AND");

        Map<String,String> filter = new HashMap<String,String>();

        for(String split : splits) {
            String [] innerSplits = split.split("=");
            String key = innerSplits[0].trim();
            String val = innerSplits[1].trim();
            filter.put(key,val);
        }
        return filter;
    }
}

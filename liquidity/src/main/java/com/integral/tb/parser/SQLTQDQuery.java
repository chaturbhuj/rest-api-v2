package com.integral.tb.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * SQL TQD query.
 *
 * @author Rahul Bhattacharjee
 */
public class SQLTQDQuery {

    private List<String> projections = null;
    private List<Map<String,String>> filters = new ArrayList<Map<String,String>>();
    private String lpaDirectory;

    public List<String> getProjections() {
        return projections;
    }

    public void setProjections(List<String> projections) {
        this.projections = projections;
    }

    public List<Map<String, String>> getFilters() {
        return filters;
    }

    public void addFilters(Map<String, String> filter) {
        this.filters.add(filter);
    }

    public String getLpaDirectory() {
        return lpaDirectory;
    }

    public void setLpaDirectory(String lpaDirectory) {
        this.lpaDirectory = lpaDirectory;
    }

    public void queryPlan() {
        System.out.println("***** Query Plan *****");
        System.out.println("Projections => " + projections);
        System.out.println("Filters applied => ");
        int count = 1;
        for(Map<String,String> filter : filters) {
            System.out.println(count++ + ") " + filter.toString());
        }
        System.out.println("LPA location => " + lpaDirectory);
        System.out.println("**********************");
    }
}

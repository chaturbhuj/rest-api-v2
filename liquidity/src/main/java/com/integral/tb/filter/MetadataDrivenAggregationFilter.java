package com.integral.tb.filter;

import com.integral.reporter.bean.ReportEntry;
import com.integral.tb.ReflectionUtil;

import java.util.*;

/**
 * Ensures that we have one entry per sample time from each of the filter.If there are multiple then we have to take
 * the last report entry.
 *
 * @author Rahul Bhattacharjee
 */
public class MetadataDrivenAggregationFilter {

    public Map<Long, List<ReportEntry>> filterIncompleteEntries(Map<Long, List<ReportEntry>> entriesGroupedByTime, List<Map<String, String>> filters) {
        Map<Long,List<ReportEntry>> result = new HashMap<Long,List<ReportEntry>>();
        for(Map.Entry<Long,List<ReportEntry>> entry : entriesGroupedByTime.entrySet()){
            long sampleTime = entry.getKey();
            List<ReportEntry> entries = entry.getValue();
            List<ReportEntry> uniqueEntries = getUniqueEntries(entries,filters);

            if(uniqueEntries.size() == filters.size()) {
                result.put(sampleTime,uniqueEntries);
            }
        }
        return result;
    }

    private List<ReportEntry> getUniqueEntries(List<ReportEntry> entries, List<Map<String, String>> filters) {
        List<ReportEntry> result = new ArrayList<ReportEntry>();

        Map<Long,List<ReportEntry>> entriesGroupedByTime = new HashMap<Long,List<ReportEntry>>();
        Set<Long> epochTimeList = new HashSet<Long>();
        for(ReportEntry entry : entries) {
            Long rateTime = entry.getQuoteDateTime().getTime();
            epochTimeList.add(rateTime);
            List<ReportEntry> subEntry = entriesGroupedByTime.get(rateTime);
            if(subEntry == null) {
                subEntry = new ArrayList<ReportEntry>();
                entriesGroupedByTime.put(rateTime,subEntry);
            }
            subEntry.add(entry);
        }

        List<Long> sortedLongTime = new ArrayList<Long>(epochTimeList);
        Collections.sort(sortedLongTime,Collections.reverseOrder());

        List<Map<String,String>> clonedFilter = null;
        if(filters instanceof ArrayList){
            clonedFilter = (List<Map<String,String>>)((ArrayList)filters).clone();
        }  else if(filters instanceof LinkedList) {
            clonedFilter = (List<Map<String,String>>)((LinkedList)filters).clone();
        } else {
            throw new UnsupportedOperationException("Unknown implementation of List been used." + filters.getClass().getCanonicalName());
        }

        Iterator<Long> reverseIterator = sortedLongTime.iterator();
        while(reverseIterator.hasNext()) {
            if(clonedFilter.size() == 0) {
                break;
            }
            Long rateTime = reverseIterator.next();
            List<ReportEntry> entryPerMilliSecond = entriesGroupedByTime.get(rateTime);
            result.addAll(getFilteredEntries(entryPerMilliSecond, clonedFilter));
        }
        return result;
    }

    private List<ReportEntry> getFilteredEntries(List<ReportEntry> entryPerMilliSecond, List<Map<String, String>> clonedFilter) {
        List<ReportEntry> result = new ArrayList<ReportEntry>();
        try {
            for(ReportEntry entry : entryPerMilliSecond) {
                Iterator<Map<String,String>> iterator = clonedFilter.iterator();
                while(iterator.hasNext()) {
                    Map<String,String> filter = iterator.next();
                    boolean isMatch = ReflectionUtil.match(entry, filter);
                    if(isMatch) {
                        iterator.remove();
                        result.add(entry);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

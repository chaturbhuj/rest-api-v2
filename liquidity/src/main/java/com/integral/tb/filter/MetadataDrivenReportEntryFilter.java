package com.integral.tb.filter;

import com.integral.reporter.bean.ReportEntry;
import com.integral.tb.ReflectionUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Filters report dynamically using reflection at runtime using the metadata supplied.
 *
 * @author Rahul Bhattacjaree
 */
public class MetadataDrivenReportEntryFilter {

    public List<ReportEntry> filteredReportEntries(List<ReportEntry> reportEntries, List<Map<String, String>> filters) {
        if(filters == null || (filters.size() == 0)){
            return reportEntries;
        }
        List<ReportEntry> result = new ArrayList<ReportEntry>();
        try {
            for(Map<String,String> filter : filters) {
                result.addAll(getFilteredReport(reportEntries, filter));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private static List<ReportEntry> getFilteredReport(List<ReportEntry> reportEntries, Map<String, String> filter) {
        List<ReportEntry> result = new ArrayList<ReportEntry>();
        try {
            for(ReportEntry entry : reportEntries) {
                boolean isMatch = ReflectionUtil.match(entry, filter);
                if(isMatch) {
                    result.add(entry);
                }
            }
        }catch (Exception e) {
             e.printStackTrace();
        }
        return result;
    }
}

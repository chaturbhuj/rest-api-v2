package com.integral.tb.driver;

import com.integral.reporter.bean.ReportEntry;
import com.integral.tb.filter.MetadataDrivenAggregationFilter;
import com.integral.tb.filter.MetadataDrivenReportEntryFilter;
import com.integral.tb.ReflectionUtil;
import com.integral.tb.parser.ReportEntryTransformer;
import com.integral.ts.util.Transformer;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Time box metadata generator code.
 *
 * @author Rahul Bhattacharjee
 */
public class TimeBoxMetadataGenerator {

    //
    // THE IDEA HERE IS TO HAVE A SQL LIKE INTERFACE FOR QUERYING THE LPA FILES AND GENERATING REQUIRED TQD FILES FOR TOOLS
    // CONSUMPTION.\
    //
    // $lpa = location of input file.
    // $output = location to create tqd file.
    //
    // select bid,offer from $lpa group by provider,stream,ccypair,level where (provider,stream,ccypair,level) = [("BARX","SWISSQUOTE","AUDUSD","1"),
    // ("ALFN","RUB_METALS","1")]
    // dump $output;
    //

    private static final String INPUT_LOCATION = "C:\\Users\\bhattacharjeer\\Desktop\\TimeBoxMetadata";

    private static final String PROJECTION = "bid,offer";

    public static void main(String [] argv) throws Exception {
        String location = INPUT_LOCATION;
        if(argv.length == 1) {
            location = argv[0];
        }

        // CONFIGURATION ..

        Map<String,String> filter1 = new HashMap<String,String>();
        filter1.put("provider","BOAN");
        filter1.put("stream","BandB");
        filter1.put("ccypair","EURUSD");
        filter1.put("tier","1");

        Map<String,String> filter2 = new HashMap<String,String>();
        filter2.put("provider","CITI");
        filter2.put("stream","Integral1");
        filter2.put("ccypair","EURUSD");
        filter2.put("tier","1");

        List<Map<String,String>> filters = new ArrayList<Map<String,String>>();
        filters.add(filter1);
        filters.add(filter2);

        // END OF CONFIGURATION

        /*
        List<ReportEntry> reportEntries = getReportEntries(location);
        System.out.println("Size of Reports " + reportEntries.size());

        List<ReportEntry> filteredEntries = filteredReportEntries(reportEntries,filters);
        System.out.println("Size of Filtered Reports " + filteredEntries.size());

        Map<Long,List<ReportEntry>> entriesGroupedByTime = groupEntryByTime(filteredEntries);
        System.out.println("Size of entries grouped by sample time : " + entriesGroupedByTime.size());

        Map<Long,List<ReportEntry>> filterIncompleteEntries = filterIncompleteEntries(entriesGroupedByTime,filters);

        System.out.println("Size of reports after aggregated filter is applied " + filterIncompleteEntries.size());

        serialize(filterIncompleteEntries, filters,getProjections(),location);
        */

        String [] splits = PROJECTION.split(",");
        List<String> projections = new ArrayList<String>();

        for(String split : splits) {
            projections.add(split);
        }
        TQDGenerator generator = new TQDGenerator();
        generator.generate(projections,filters,location);
    }
}

















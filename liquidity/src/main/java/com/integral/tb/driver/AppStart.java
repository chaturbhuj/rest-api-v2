package com.integral.tb.driver;

import asg.cliche.Command;
import asg.cliche.ShellFactory;
import com.integral.tb.parser.*;

/**
 * Command line application.
 *
 * java -cp ./rate-profiler.jar com.integral.tb.driver.AppStart
 *
 * @author Rahul Bhattacharjee
 */
public class AppStart {

    private SQLQueryParser parser = new SQLQueryParser();


    public static void main(String [] arguments) throws Exception {
        ShellFactory.createConsoleShell("lpa_shell", "", new AppStart()).commandLoop();
    }

    @Command
    public void help(){
        System.out.print("Command line SQL like interface to query and generate TQD files from LPA reports.\n");
    }

    @Command
    public void sql(String sql) {
        try {
            SQLTQDQuery query = parser.getSqlQuery(sql);
            query.queryPlan();

            TQDGenerator generator = new TQDGenerator();
            generator.generate(query.getProjections(),query.getFilters(),query.getLpaDirectory());
        }catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
        }
    }
}

package com.integral.tb.driver;

import com.integral.reporter.bean.ReportEntry;
import com.integral.tb.ReflectionUtil;
import com.integral.tb.filter.MetadataDrivenAggregationFilter;
import com.integral.tb.filter.MetadataDrivenReportEntryFilter;
import com.integral.tb.parser.ReportEntryTransformer;
import com.integral.ts.util.Transformer;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * TQD file generator.
 *
 * @author Rahul Bhattacharjee
 */
public class TQDGenerator {

    private static final Transformer<String,ReportEntry> TRANSFORMER = new ReportEntryTransformer();

    private static final MetadataDrivenReportEntryFilter FILTER = new MetadataDrivenReportEntryFilter();
    private static final MetadataDrivenAggregationFilter AGGREGATION_FILTER = new MetadataDrivenAggregationFilter();

    public void generate(List<String> predicate,List<Map<String,String>> filters,String directory){
        try {
            List<ReportEntry> reportEntries = getReportEntries(directory);
            System.out.println("Size of Reports " + reportEntries.size());

            List<ReportEntry> filteredEntries = filteredReportEntries(reportEntries,filters);
            System.out.println("Size of Filtered Reports " + filteredEntries.size());

            Map<Long,List<ReportEntry>> entriesGroupedByTime = groupEntryByTime(filteredEntries);
            System.out.println("Size of entries grouped by sample time : " + entriesGroupedByTime.size());

            Map<Long,List<ReportEntry>> filterIncompleteEntries = filterIncompleteEntries(entriesGroupedByTime,filters);

            System.out.println("Size of reports after aggregated filter is applied " + filterIncompleteEntries.size());
            serialize(filterIncompleteEntries, filters ,predicate,directory);
        }catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
        }
    }

    private static Map<Long, List<ReportEntry>> filterIncompleteEntries(Map<Long, List<ReportEntry>> entriesGroupedByTime, List<Map<String, String>> filters) {
        return AGGREGATION_FILTER.filterIncompleteEntries(entriesGroupedByTime, filters);
    }

    private static List<ReportEntry> filteredReportEntries(List<ReportEntry> reportEntries, List<Map<String, String>> filters) {
        return FILTER.filteredReportEntries(reportEntries,filters);
    }

    private static List<ReportEntry> getReportEntries(String location) {
        List<ReportEntry> entries = new ArrayList<ReportEntry>();
        File file = new File(location);

        File [] fileNames = file.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith("csv") ? true : false;
            }
        });
        for(File fileName : fileNames) {
            entries.addAll(parseLPAFile(fileName));
        }
        return entries;
    }

    private static List<ReportEntry> parseLPAFile(File fileName) {
        List<ReportEntry> entries = new ArrayList<ReportEntry>();

        FileReader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(new FileReader(fileName));
            String line = "";
            while(line != null){
                if(StringUtils.isNotBlank(line)) {
                    ReportEntry entry = parseReportEntry(line);
                    if(entry != null) {
                        entries.add(entry);
                    }
                }
                line = bufferedReader.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(bufferedReader);
            IOUtils.closeQuietly(reader);
        }
        return entries;
    }

    private static ReportEntry parseReportEntry(String line) {
        return TRANSFORMER.transform(line);
    }

    private static void serialize(Map<Long, List<ReportEntry>> entriesGroupedByTime, List<Map<String,String>> filters, List<String> predicate,String location) throws Exception{
        Map<Long,Map<String,ReportEntry>> tqdMap = new HashMap<Long,Map<String,ReportEntry>>();

        Map<String,Map<String,String>> filterIdMap = createFilterId(filters);

        List<Long> timestamps = new ArrayList<Long>(entriesGroupedByTime.keySet());
        Collections.sort(timestamps);

        Iterator<Long> iterator = timestamps.iterator();
        while(iterator.hasNext()) {
            Long timestamp = iterator.next();
            List<ReportEntry> entries = entriesGroupedByTime.get(timestamp);

            for(Map.Entry<String,Map<String,String>> filterIdEntry : filterIdMap.entrySet()) {
                String key = filterIdEntry.getKey();
                Map<String,String> filter = filterIdEntry.getValue();

                for(ReportEntry entry : entries) {
                    boolean isMatch = ReflectionUtil.match(entry, filter);
                    if(isMatch) {
                        Map<String,ReportEntry> tqdMamEntry = tqdMap.get(timestamp);
                        if(tqdMamEntry == null) {
                            tqdMamEntry = new HashMap<String,ReportEntry>();
                            tqdMap.put(timestamp,tqdMamEntry);
                        }
                        tqdMamEntry.put(key,entry);
                    }
                }
            }
        }

        System.out.println("START GENERATING TQD.");

        String outputFile = getOutputFile(location);

        String timeStampList = getTimestampList(timestamps);

        FileWriter writer = null;
        BufferedWriter bufferedWriter = null;

        try {
            writer = new FileWriter(outputFile);
            bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write(timestamps.size() + "\n");
            bufferedWriter.write(filters.size() + "\n");
            bufferedWriter.write(timeStampList + "\n");

            Map<String,List<String>> writerMap = getWriterMap(filterIdMap.keySet());

            for(long timestamp : timestamps) {
                Map<String,ReportEntry> entriesForTimestamp = tqdMap.get(timestamp);
                Iterator<String> keyIterator = filterIdMap.keySet().iterator();
                while(keyIterator.hasNext()) {
                    String key = keyIterator.next();
                    ReportEntry entry = entriesForTimestamp.get(key);
                    StringBuilder stringBuilder = new StringBuilder();
                    boolean isFirst = true;
                    for(String projection : predicate) {
                        if(isFirst) {
                            isFirst = false;
                        } else {
                            stringBuilder.append(",");
                        }
                        String value = ReflectionUtil.getValue(projection,entry);
                        stringBuilder.append(value);
                    }
                    List<String> values = writerMap.get(key);
                    values.add(stringBuilder.toString());
                }
            }

            for(Map.Entry<String,List<String>> entry : writerMap.entrySet()) {
                String key = entry.getKey();
                String value = StringUtils.join(entry.getValue(),",");
                bufferedWriter.write(key+","+value+"\n");
            }

            System.out.println("Generated output file : " + outputFile);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(bufferedWriter);
            IOUtils.closeQuietly(writer);
        }
    }

    private static Map<String, List<String>> getWriterMap(Set<String> filterKeys) {
        Map<String,List<String>> result = new HashMap<String,List<String>>();
        for(String key : filterKeys) {
            result.put(key,new ArrayList<String>());
        }
        return result;
    }

    private static String getTimestampList(List<Long> timestamps) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/y HH:mm:ss");
        StringBuilder stringBuilder = new StringBuilder();
        boolean isFirst = true;
        for(long timestamp : timestamps) {
            Date date = new Date(timestamp);
            if(isFirst){
                isFirst = false;
            } else {
                stringBuilder.append(",");
            }
            stringBuilder.append(dateFormat.format(date));
        }
        return stringBuilder.toString();
    }

    private static Map<String, Map<String, String>> createFilterId(List<Map<String, String>> filters) {
        Map<String,Map<String,String>> result = new HashMap<String,Map<String,String>>();
        for(Map<String,String> filter : filters) {
            Iterator<String> iterator = filter.values().iterator();
            StringBuilder stringBuilder = new StringBuilder();
            while(iterator.hasNext()) {
                stringBuilder.append(iterator.next());
            }
            result.put(stringBuilder.toString(),filter);
        }
        return result;
    }

    private static Map<Long, List<ReportEntry>> groupEntryByTime(List<ReportEntry> filteredEntries) {
        Map<Long,List<ReportEntry>> timeGroupedEntries = new HashMap<Long,List<ReportEntry>>();
        for(ReportEntry entry : filteredEntries) {
            long sampleTime = entry.getSampleDateTime().getTime();
            List<ReportEntry> entries = timeGroupedEntries.get(sampleTime);
            if(entries == null) {
                entries = new ArrayList<ReportEntry>();
                timeGroupedEntries.put(sampleTime,entries);
            }
            entries.add(entry);
        }
        return timeGroupedEntries;
    }

    private static String getOutputFile(String location) {
        String fileSeparator = File.separator;
        return location + fileSeparator + "output-" + new Date().getTime()+".tqd";
    }
}

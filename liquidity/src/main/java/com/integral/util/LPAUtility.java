package com.integral.util;

import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.profile.ErrorType;
import com.integral.reporter.bean.ReportEntry;

import java.math.BigDecimal;
import java.util.List;

/**
 * General purpose utility class for LPA.
 *
 * @author Rahul Bhattacharjee
 */
public class LPAUtility {

    public static void setEntriesStatus(List<ReportEntry> entries) {
        for(ReportEntry reportEntry : entries) {
            setEntryStatus(reportEntry);
        }
    }

    public static void setEntryStatus(ReportEntry reportEntry) {
        if(isInActive(reportEntry)) {
            return;
        }
        int window = ResourceUtil.getPrevailingRateFilter();
        long age = Math.abs(ReportWriterUtil.distanceFromSampleTime(reportEntry));
        if(age > window) {
            reportEntry.setErrorType(ErrorType.RATE_OUT_OF_WINDOW);
        }
    }

    public static ErrorType getErrorType(List<ReportEntry> entries) {
        ErrorType returnType = ErrorType.NONE;
        for(ReportEntry entry : entries) {
            if(entry.getErrorType() != ErrorType.NONE) {
                returnType = ErrorType.PARTIAL_OUT_OF_WINDOW_RATE;
                break;
            }
        }
        return returnType;
    }

    public static ErrorType getErrorType(RestRatesObject rateObject) {
        if(isInActive(rateObject)) {
            return ErrorType.INACTIVE_RATE;
        } else {
            if(isOneSidedRate(rateObject)){
                return ErrorType.ONE_SIDED_RATE;
            } else {
                return ErrorType.NONE;
            }
        }
    }

    private static boolean isOneSidedRate(RestRatesObject rateObject) {
        BigDecimal bidVolume = rateObject.getBid_size();
        BigDecimal offerVolume = rateObject.getAsk_size();

        if(BigDecimal.ZERO.equals(bidVolume) || BigDecimal.ZERO.equals(offerVolume)) {
            return true;
        }
        return false;
    }

    public static boolean isInActive(RestRatesObject ratesObject) {
        String status = ratesObject.getStatus();
        return "inactive".equalsIgnoreCase(status) ? true : false;
    }

    public static boolean isInActive(ReportEntry reportEntry) {
        if(reportEntry.getErrorType() == ErrorType.INACTIVE_RATE) {
            return true;
        }
        return false;
    }
}

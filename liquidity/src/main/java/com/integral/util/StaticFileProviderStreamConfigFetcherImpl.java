package com.integral.util;

import com.integral.ds.dao.ProviderStreamConfigFetcher;
import com.integral.ds.dto.ProviderStream;
import com.integral.ds.dto.QuoteType;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * Provider stream config fetcher impl which would read a static file to give out a list of ProviderStream objects.
 * @author Rahul Bhattacharjee
 */
public class StaticFileProviderStreamConfigFetcherImpl implements ProviderStreamConfigFetcher {

    private static final Logger logger = Logger.getLogger(StaticFileProviderStreamConfigFetcherImpl.class);

    private static final String DEFAULT_PROVIDER_STREAM_FILENAME = "ProviderStream.config";
    private List<ProviderStream> providerStreams;
    private Properties config;

    @Override
    public void init(Properties config) {
        try {
            this.config = config;
            initProviderStream();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception while fetching provider stream.",e);
        }
    }

    @Override
    public List<ProviderStream> getProviderStreams() {
        return providerStreams;
    }

    private void initProviderStream() throws Exception {
        List<ProviderStream> localProviderStreams = new ArrayList<ProviderStream>();
        String configFileName = getConfigFileName();
        InputStream inputStream = ResourceUtil.getStream(configFileName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String line = "";
            while(line != null) {
                line = reader.readLine();
                if(StringUtils.isNotBlank(line)) {
                    String [] splits = line.split(",");
                    ProviderStream providerStream = new ProviderStream();
                    providerStream.setProvider(splits[0]);
                    providerStream.setStream(splits[1]);
                    providerStream.setQuoteType(QuoteType.valueOf(splits[2]));
                    localProviderStreams.add(providerStream);
                }
            }
            providerStreams = Collections.unmodifiableList(localProviderStreams);
        } finally {
            if(reader != null) {
                reader.close();
            }
        }
    }

    private String getConfigFileName() {
        if(config != null) {
            String fileName = (String) config.get(CONFIG_FILENAME_KEY);
            if(StringUtils.isNotBlank(fileName)) {
                return fileName;
            }
        }
        return DEFAULT_PROVIDER_STREAM_FILENAME;
    }
}

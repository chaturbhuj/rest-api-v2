package com.integral.util;

import com.integral.ds.dao.ProviderStreamConfigFetcher;

/**
 * Factory to give out instances of ProviderStreamConfigFetcher based on key.
 *
 * @author Rahul Bhattacharjee
 */
public class ProviderStreamConfigFetcherFactory {

    public static enum FetcherType {STATIC, S3}

    public static ProviderStreamConfigFetcher getProviderStreamConfigFetcher(FetcherType type) {
        switch (type) {
            case S3: return new AllAvailableProviderStreamConfigFetcherImpl();
            case STATIC: return new StaticFileProviderStreamConfigFetcherImpl();
            default: return getProviderStreamConfigFetcher(FetcherType.S3);
        }
    }
}

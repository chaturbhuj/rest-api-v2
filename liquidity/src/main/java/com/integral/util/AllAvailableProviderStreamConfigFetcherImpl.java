package com.integral.util;

import com.integral.ds.dao.ProviderStreamConfigFetcher;
import com.integral.ds.dao.s3.S3ReadAndParseUtility;
import com.integral.ds.dto.ProviderStream;
import com.integral.ds.s3.RecordTransformer;

import java.util.List;
import java.util.Properties;

/**
 * Implementation to fetch all provider stream pairs in our S3 repo.
 *
 * @author Rahul Bhattacharjee
 */
public class AllAvailableProviderStreamConfigFetcherImpl implements ProviderStreamConfigFetcher {

    private static final String BUCKET_NAME = "integral-rates";
    private static final String S3_CONFIG_FOR_ALL_AVAILABLE_STREAMS = "config/AvailableStreams.txt";

    @Override
    public void init(Properties properties) {}

    @Override
    public List<ProviderStream> getProviderStreams() {
        return S3ReadAndParseUtility.downloadAndParseS3File(BUCKET_NAME, S3_CONFIG_FOR_ALL_AVAILABLE_STREAMS,
                new RecordTransformer<ProviderStream>() {
                    @Override
                    public ProviderStream transform(String input) {
                        String[] splits = input.split(",");
                        String provider = splits[0];
                        String stream = splits[1];
                        return new ProviderStream(provider, stream);
                    }
                });
    }
}

package com.integral.util;

import com.integral.Constants;
import com.integral.ds.dao.ProviderStreamConfigFetcher;
import com.integral.ds.dto.ProviderStream;
import com.integral.ds.dto.QuoteType;
import org.apache.commons.lang.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * @author Rahul Bhattacharjee
 */
public class ResourceUtil {

    private static ProviderStreamConfigFetcherFactory providerStreamConfigFetcherFactory = new ProviderStreamConfigFetcherFactory();

    private static Properties applicationConfig = new Properties();
    private static Map<String,List<ProviderStream>> aggregatedMap;
    private static List<String> currencyPairs;
    private static List<ProviderStream> staticProviderStreamList = null;

    static {
        initResources();
    }

    private static void initResources() {
        try {
            initApplicationConfiguration();
            Properties configuration = new Properties();
            configuration.put(ProviderStreamConfigFetcher.CONFIG_FILENAME_KEY ,getFileNameForProviderStream());
            initAggregatedReportInputs();
        } catch (Exception e) {
            throw new IllegalArgumentException("Exception while init static resources.",e);
        }
    }

    private static String getFileNameForProviderStream() {
        String fileName = Constants.PROVIDER_STREAM_CONFIG_FILE_NAME;
        return getEnvironmentSpecificFilename(fileName);
    }

    private static List<ProviderStream> getProviderStreamFromConfig(String fileName) throws Exception {
        List<ProviderStream> localProviderStreams = new ArrayList<ProviderStream>();
        InputStream inputStream = ResourceUtil.getStream(fileName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String line = "";
            while(line != null) {
                line = reader.readLine();
                if(StringUtils.isNotBlank(line)) {
                    String [] splits = line.split(",");
                    ProviderStream providerStream = new ProviderStream();
                    providerStream.setProvider(splits[0]);
                    providerStream.setStream(splits[1]);
                    providerStream.setQuoteType(QuoteType.valueOf(splits[2]));
                    localProviderStreams.add(providerStream);
                }
            }
        } finally {
            if(reader != null) {
                reader.close();
            }
        }
        return localProviderStreams;
    }

    private static void initApplicationConfiguration() throws Exception {
        InputStream inputStream = getStream(Constants.APP_CONFIG);
        try {
            applicationConfig.load(inputStream);
        } finally {
            if(inputStream != null) {
                inputStream.close();
            }
        }
    }

    public static List<ProviderStream> getProviderStreamList() {
        if(staticProviderStreamList == null) {
            ProviderStreamConfigFetcher fetcher = providerStreamConfigFetcherFactory.getProviderStreamConfigFetcher(ProviderStreamConfigFetcherFactory.FetcherType.STATIC);
            fetcher.init(null);
            staticProviderStreamList = fetcher.getProviderStreams();
        }
        return staticProviderStreamList;
    }

    public static List<Long> getVolumeList() {
        List<Long> volumes = new ArrayList<Long>();
        String volumeList = applicationConfig.getProperty(Constants.APP_VOLUME);
        if(StringUtils.isNotBlank(volumeList)) {
            String [] volumeInStrings = volumeList.split(",");
            try {
                for(String volume : volumeInStrings) {
                    volumes.add(Long.parseLong(volume));
                }
            } catch (Exception e) {
                throw new IllegalArgumentException("Vol list cannot be empty.",e);
            }
        }
        return volumes;
    }

    public static List<String> getCurrencyPairs() {
        if(currencyPairs == null) {
           List<String> localCurrencyPairs = new ArrayList<String>();
           String ccyPair = applicationConfig.getProperty(Constants.APP_CCY_PAIRS);
           if(StringUtils.isNotBlank(ccyPair)) {
              String [] ccyPairs = ccyPair.split(",");
              for(String ccy : ccyPairs) {
                 localCurrencyPairs.add(ccy);
              }
           }
           currencyPairs = Collections.unmodifiableList(localCurrencyPairs);
        }
        return currencyPairs;
    }

    public static InputStream getStream(String configName) {
        InputStream inputStream = ResourceUtil.class.getResourceAsStream(configName);
        if(inputStream == null) {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            inputStream = classLoader.getResourceAsStream(configName);
        }
        if(inputStream == null) {
            ClassLoader classLoader = ClassLoader.getSystemClassLoader();
            inputStream = classLoader.getResourceAsStream(configName);
        }
        return inputStream;
    }

    private static boolean isDevelopmentEnv() {
        String value = (String) applicationConfig.get(Constants.ENVIRONMENT_KEY);
        if(StringUtils.isNotBlank(value)) {
            return Boolean.parseBoolean(value);
        }
        return false;
    }

    private static String getEnvironmentSpecificFilename(String fileName) {
        if(isDevelopmentEnv()) {
            fileName += ".dev";
        }
        return fileName;
    }

    public static Map<Integer,ProviderStream> getProviderStreamMetadata() {
        Map<Integer,ProviderStream> result = new HashMap<Integer,ProviderStream>();
        InputStream inputStream = ResourceUtil.getStream(Constants.PROVIDER_STREAM_META_FILE_NAME);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String line = "";
            while(line != null) {
                line = reader.readLine();
                if(StringUtils.isNotBlank(line)) {
                    String [] splits = line.split("~");
                    ProviderStream providerStream = new ProviderStream();
                    int index = Integer.parseInt(splits[0]);
                    String [] providerStreamList = splits[1].split(",");
                    providerStream.setProvider(providerStreamList[0]);
                    providerStream.setStream(providerStreamList[1]);
                    providerStream.setQuoteType(QuoteType.valueOf(providerStreamList[2]));
                    result.put(index,providerStream);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException(e);
        } finally {
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public static void initAggregatedReportInputs() {
        Map<Integer,ProviderStream> metadata = getProviderStreamMetadata();
        Map<String,List<ProviderStream>> result = new HashMap<String,List<ProviderStream>>();

        InputStream inputStream = ResourceUtil.getStream(getEnvironmentSpecificFilename(Constants.AGG_REPORT_CONFIG_FILE_NAME));
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String line = "";
            while(line != null) {
                line = reader.readLine();
                if(StringUtils.isNotBlank(line)) {
                    String [] splits = line.split("=");
                    String reportName = splits[0];
                    String providerStreamIdList = splits[1];
                    String [] providerStreamIds = providerStreamIdList.split(",");
                    List<ProviderStream> providerStreams = new ArrayList<ProviderStream>();
                    for(String providerStreamId : providerStreamIds) {
                        int index = Integer.parseInt(providerStreamId);
                        ProviderStream providerStream = metadata.get(index);
                        if(providerStream == null) {
                            System.out.println("PROVIDER STREAM NOT AVAILABLE FOR REPORT " + reportName);
                            continue;
                        }
                        providerStreams.add(providerStream);
                    }
                    result.put(reportName,providerStreams);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException(e);
        } finally {
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        aggregatedMap = result;
    }

    public static Map<String,List<ProviderStream>> getAggregatedReportInputs() {
        if(aggregatedMap == null) {
            initAggregatedReportInputs();
        }
        return aggregatedMap;
    }

    private static String getConfigValue(String key) {
        return (String) applicationConfig.get(key);
    }

    public static boolean isDMSAEnabled() {
        return getBooleanValueFor(Constants.DMSA_KEY);
    }

    public static boolean isDebugReportsEnabled() {
        return getBooleanValueFor(Constants.ENABLE_DEBUG_REPORT_KEY);
    }

    public static boolean isContributionReportEnabled() {
        return getBooleanValueFor(Constants.IS_CONT_REPORT_ENABLED);
    }

    public static boolean isProfileReportEnabled() {
        return getBooleanValueFor(Constants.IS_PROFILE_REPORT_ENABLED);
    }

    public static boolean isTQDFileGenerationEnabled() {
        return getBooleanValueFor(Constants.TQD_FILE_GENERATION);
    }

    private static boolean getBooleanValueFor(String key) {
        String value = getConfigValue(key);
        if(StringUtils.isNotBlank(value)) {
            return Boolean.parseBoolean(value);
        }
        return false;
    }

    private static int getIntegerValue(String value) {
        return Integer.parseInt(value);
    }

    public static int getPrevailingRateFilter() {
        String value = getConfigValue(Constants.PREVAILING_RATE_FILTER);
        return getIntegerValue(value);
    }
}









































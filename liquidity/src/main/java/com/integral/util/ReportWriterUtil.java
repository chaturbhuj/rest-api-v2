package com.integral.util;

import com.integral.ds.commons.util.CcyPairPipMultiplyFactor;
import com.integral.ds.dto.QuoteType;
import com.integral.reporter.bean.ContributionEntry;
import com.integral.reporter.bean.ProfileEntry;
import com.integral.reporter.bean.Report;
import com.integral.reporter.bean.ReportEntry;

import java.io.File;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class ReportWriterUtil {

    public static final String SINGLE_LP_REPORT_IDENTIFIER = "SINGLE LP";

    public static String getBlankForNull(Object obj) {
        if(obj == null) {
            return "";
        } else {
            return obj.toString();
        }
    }

    public static String getBlankForZeroValue(double val) {
        if(val == 0.0) {
            return "";
        }else {
            return val+"";
        }
    }

    public static long distanceFromSampleTime(ReportEntry entry) {
        return (entry.getSampleDateTime().getTime() - entry.getQuoteDateTime().getTime());
    }

    public static void writeReportToWriter1(List<ReportEntry> reports,PrintWriter writer) {
        for(ReportEntry entry : reports) {
            String provider = entry.getProvider();
            String stream = entry.getStream();
            String quoteType = getQuoteType(entry.getQuoteType());
            String ccyPair = entry.getCcyPair();
            String quoteDate = entry.getQuoteDate();
            String quoteTime = entry.getQuoteTime();
            String sampleDate = entry.getSampleDate();
            String sampleTime = entry.getSampleTime();
            BigDecimal bid = entry.getBid();
            BigDecimal offer = entry.getOffer();
            double spread = entry.getSpread();
            int level = entry.getTier();
            BigDecimal bidVolume = entry.getBidVolume();
            BigDecimal offerVolume = entry.getOfferVolume();
            BigDecimal atVolume = entry.getAtVolume();
            String reportType = entry.getEntryType().getTypeId();
            String aggregationType = entry.getAggregationType();
            long age1 = entry.getMaxAgeOfRate();
            long age2 = entry.getMinAgeOfRate();

            long maxAge = Math.max(age1,age2);
            long minAge = Math.min(age1,age2);

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(provider+",");
            stringBuilder.append(stream+",");
            stringBuilder.append(quoteType+",");
            stringBuilder.append(ccyPair+",");
            stringBuilder.append(quoteDate + ",");
            stringBuilder.append(quoteTime + ",");
            stringBuilder.append(sampleDate + ",");
            stringBuilder.append(sampleTime + ",");
            stringBuilder.append(distanceFromSampleTime(entry) + ",");
            stringBuilder.append(level + ",");
            stringBuilder.append(getBlankForNull(bid)+",");
            stringBuilder.append(bidVolume+",");
            stringBuilder.append(getBlankForNull(offer)+",");
            stringBuilder.append(offerVolume+",");
            stringBuilder.append(getBlankForNull(atVolume)+",");

            //stringBuilder.append(ReportWriterUtil.getBlankForZeroValue(spread)+",");
            // below for spread in pip.
            stringBuilder.append(getSpreadInPip(spread,ccyPair)+",");
            stringBuilder.append(reportType+",");
            stringBuilder.append(aggregationType+",");
            stringBuilder.append(getBlankForZeroValue(maxAge)+",");
            stringBuilder.append(getBlankForZeroValue(minAge));
            writer.write(stringBuilder.toString() + "\n");
        }
    }

    public static void writeReportToWriter(List<ReportEntry> reports,PrintWriter writer) {
        for(ReportEntry entry : reports) {
            writer.write(serializeReportEntry(entry) + "\n");
        }
    }

    public static String serializeReportEntry(ReportEntry entry) {
        String provider = entry.getProvider();
        String stream = entry.getStream();
        String quoteType = getQuoteType(entry.getQuoteType());
        String ccyPair = entry.getCcyPair();
        String quoteDate = entry.getQuoteDate();
        String quoteTime = entry.getQuoteTime();
        String sampleDate = entry.getSampleDate();
        String sampleTime = entry.getSampleTime();
        BigDecimal bid = entry.getBid();
        BigDecimal offer = entry.getOffer();
        double spread = entry.getSpread();
        int level = entry.getTier();
        BigDecimal bidVolume = entry.getBidVolume();
        BigDecimal offerVolume = entry.getOfferVolume();
        BigDecimal atVolume = entry.getAtVolume();
        String reportType = entry.getEntryType().getTypeId();
        String aggregationType = entry.getAggregationType();
        long age1 = entry.getMaxAgeOfRate();
        long age2 = entry.getMinAgeOfRate();
        String errorCode = entry.getErrorType().name();

        long maxAge = Math.max(age1,age2);
        long minAge = Math.min(age1,age2);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(provider+",");
        stringBuilder.append(stream+",");
        stringBuilder.append(quoteType+",");
        stringBuilder.append(ccyPair+",");
        stringBuilder.append(quoteDate + ",");
        stringBuilder.append(quoteTime + ",");
        stringBuilder.append(sampleDate + ",");
        stringBuilder.append(sampleTime + ",");
        stringBuilder.append(distanceFromSampleTime(entry) + ",");
        stringBuilder.append(level + ",");
        stringBuilder.append(getBlankForNull(bid)+",");
        stringBuilder.append(bidVolume+",");
        stringBuilder.append(getBlankForNull(offer)+",");
        stringBuilder.append(offerVolume+",");
        stringBuilder.append(getBlankForNull(atVolume)+",");

        //stringBuilder.append(ReportWriterUtil.getBlankForZeroValue(spread)+",");
        // below for spread in pip.
        stringBuilder.append(getSpreadInPip(spread,ccyPair)+",");
        stringBuilder.append(reportType+",");
        stringBuilder.append(errorCode+",");
        stringBuilder.append(aggregationType+",");
        stringBuilder.append(getBlankForZeroValue(maxAge)+",");
        stringBuilder.append(getBlankForZeroValue(minAge));
        return stringBuilder.toString();
    }

    public static void writeContributionReportToWriter(List<ContributionEntry> contributionEntries ,PrintWriter writer) {
        for(ContributionEntry entry : contributionEntries) {
            writer.write(serializeContributionEntry(entry) + "\n");
        }
    }

    public static void writeContributionReportToWriter1(List<ContributionEntry> contributionEntries ,PrintWriter writer) {
        for(ContributionEntry entry : contributionEntries) {
            String provider = entry.getProvider();
            String stream = entry.getStream();
            String ccyPair = entry.getCcyPair();
            String sampleDate = entry.getSampleDate();
            String sampleTime = entry.getSampleTime();
            String aggregationType = entry.getAggregationKey();
            BigDecimal bidRate = entry.getBid();
            BigDecimal bidVol = entry.getBidVolume();
            BigDecimal offerRate = entry.getOffer();
            BigDecimal offerVol = entry.getOfferVolume();
            BigDecimal atVolume = entry.getAtVolume();
            String type = entry.getType().getTypeId();
            BigDecimal bidContribution = entry.getBidContribution();
            BigDecimal offerContribution = entry.getOfferContribution();

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(provider+",");
            stringBuilder.append(stream+",");
            stringBuilder.append(ccyPair+",");
            stringBuilder.append(sampleDate+",");
            stringBuilder.append(sampleTime+",");
            stringBuilder.append(getBlankForNull(bidRate)+",");
            stringBuilder.append(getBlankForNull(bidVol)+",");
            stringBuilder.append(getBlankForNull(offerRate)+",");
            stringBuilder.append(getBlankForNull(offerVol)+",");
            stringBuilder.append(getBlankForNull(atVolume)+",");
            stringBuilder.append(type+",");
            stringBuilder.append(getBlankForNull(aggregationType)+",");
            if(bidContribution == null) {
                stringBuilder.append("0,");
            } else {
                stringBuilder.append(bidContribution.floatValue()+",");
            }
            if(offerContribution == null) {
                stringBuilder.append("0");
            }else {
                stringBuilder.append(offerContribution.floatValue());
            }
            writer.write(stringBuilder.toString() + "\n");
        }
    }

    public static String serializeContributionEntry(ContributionEntry entry) {
        String provider = entry.getProvider();
        String stream = entry.getStream();
        String ccyPair = entry.getCcyPair();
        String sampleDate = entry.getSampleDate();
        String sampleTime = entry.getSampleTime();
        String aggregationType = entry.getAggregationKey();
        BigDecimal bidRate = entry.getBid();
        BigDecimal bidVol = entry.getBidVolume();
        BigDecimal offerRate = entry.getOffer();
        BigDecimal offerVol = entry.getOfferVolume();
        BigDecimal atVolume = entry.getAtVolume();
        String type = entry.getType().getTypeId();
        BigDecimal bidContribution = entry.getBidContribution();
        BigDecimal offerContribution = entry.getOfferContribution();
        String errorType = entry.getErrorType().name();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(provider+",");
        stringBuilder.append(stream+",");
        stringBuilder.append(ccyPair+",");
        stringBuilder.append(sampleDate+",");
        stringBuilder.append(sampleTime+",");
        stringBuilder.append(getBlankForNull(bidRate)+",");
        stringBuilder.append(getBlankForNull(bidVol)+",");
        stringBuilder.append(getBlankForNull(offerRate)+",");
        stringBuilder.append(getBlankForNull(offerVol)+",");
        stringBuilder.append(getBlankForNull(atVolume)+",");
        stringBuilder.append(type+",");
        stringBuilder.append(getBlankForNull(aggregationType)+",");
        if(bidContribution == null) {
            stringBuilder.append("0,");
        } else {
            stringBuilder.append(bidContribution.floatValue()+",");
        }
        if(offerContribution == null) {
            stringBuilder.append("0,");
        }else {
            stringBuilder.append(offerContribution.floatValue()+",");
        }
        stringBuilder.append(errorType);
        return stringBuilder.toString();
    }


    private static String getSpreadInPip(double spread, String ccyPair) {
        if(spread == 0.0) {
            return "";
        }
        Long pipFactor = CcyPairPipMultiplyFactor.getPipConversionFactor(ccyPair);
        if(pipFactor != null) {
            double spreadInPip = pipFactor.longValue() * spread;
            return spreadInPip + "";
        }
        return "";
    }

    private static String getQuoteType(QuoteType quoteType) {
        if(quoteType != null) {
            return quoteType.toString();
        }
        return "";
    }

    public static void ensureDirectory(String dirName) {
        File file = new File(dirName);
        file.mkdirs();
    }

    public static void addReportTypeToEntries(Report report, String reportName) {
        addReportTypeToEntries(report.getEntries(),reportName);
    }

    public static void addReportTypeToEntries(List<ReportEntry> entries,String reportName) {
        for(ReportEntry entry : entries) {
            entry.setAggregationType(reportName);
        }
    }

    public static void writeProfileReportToWriter(List<ProfileEntry> profileEntries, PrintWriter profileReportWriter) {
        for(ProfileEntry entry : profileEntries) {
            profileReportWriter.write(writeProfileEntry(entry) + "\n");
        }
    }

    private static String writeProfileEntry(ProfileEntry entry) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(entry.getProvider()+",");
        stringBuilder.append(entry.getStream()+",");
        stringBuilder.append(entry.getCcyPair()+",");
        stringBuilder.append(dateFormat.format(entry.getTime())+",");
        stringBuilder.append(entry.getTier()+",");
        stringBuilder.append(entry.getTwoSidedTickCount()+",");
        stringBuilder.append(entry.getMidPriceMean()+",");
        stringBuilder.append(entry.getSpreadAverage()+",");
        stringBuilder.append(entry.getSpreadRange()+",");
        stringBuilder.append(dateFormat.format(entry.getLastTickTimeStamp())+",");
        stringBuilder.append(entry.getLastTickAge()+",");
        stringBuilder.append(entry.getAverageBidSize()+",");
        stringBuilder.append(entry.getAverageAskSize()+",");
        stringBuilder.append(entry.getMaxBidPrice()+",");
        stringBuilder.append(entry.getMinBidPrice()+",");
        stringBuilder.append(entry.getMaxAskPrice()+",");
        stringBuilder.append(entry.getMinAskPrice()+",");
        stringBuilder.append(entry.getLastActiveBid()+",");
        stringBuilder.append(entry.getLastActiveAsk()+",");
        stringBuilder.append(entry.getActiveTickCount()+",");
        stringBuilder.append(entry.getInactiveTickCount()+",");
        stringBuilder.append(entry.getLastDeclaredStatus()+",");
        stringBuilder.append(entry.getNumberOfBadRecords());
        return stringBuilder.toString();
    }
}

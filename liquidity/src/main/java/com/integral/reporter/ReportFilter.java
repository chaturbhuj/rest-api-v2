package com.integral.reporter;

import com.integral.ds.dto.ProviderStream;
import com.integral.reporter.bean.Report;

import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public interface ReportFilter {

    List<Report> filteredReports(List<Report> reports,List<ProviderStream> providerStreams,String ccyPair);

}

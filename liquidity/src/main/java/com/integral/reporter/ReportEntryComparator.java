package com.integral.reporter;

import com.integral.reporter.bean.ReportEntry;

import java.util.Comparator;
import java.util.Date;

/**
 * Report Entry comparator based on time.
 *
 * @author Rahul Bhattacharjee
 */
public class ReportEntryComparator implements Comparator<ReportEntry> {

    @Override
    public int compare(ReportEntry o1, ReportEntry o2) {
        Date date1 = o1.getQuoteDateTime();
        Date date2 = o2.getQuoteDateTime();
        return  date1.compareTo(date2);
    }
}

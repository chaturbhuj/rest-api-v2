package com.integral.reporter.impl;

import com.integral.ds.dto.ProviderStream;
import com.integral.reporter.ReportFilter;
import com.integral.reporter.bean.Report;

import java.util.ArrayList;
import java.util.List;

/**
 * Report filter implementation.
 *
 * @author Rahul Bhattacharjee
 */
public class ReportFilterImpl implements ReportFilter {

    @Override
    public List<Report> filteredReports(List<Report> quoteReports, List<ProviderStream> providerStreams, String ccyPair) {
        List<Report> filteredReports = new ArrayList<Report>();
        for(Report report : quoteReports) {
            if(!shouldIgnoreReport(report,providerStreams,ccyPair)) {
                filteredReports.add(report);
            }
        }
        return filteredReports;
    }

    private boolean shouldIgnoreReport(Report report, List<ProviderStream> providerStreams,String ccyPair) {
        String provider = report.getProvider();
        String stream = report.getStream();
        ProviderStream providerStream = new ProviderStream(provider,stream);
        String currencyPair = report.getCcyPair();

        if(currencyPair.equalsIgnoreCase(ccyPair)) {
            return providerStreams.contains(providerStream) ? false : true;
        }
        return true;
    }
}

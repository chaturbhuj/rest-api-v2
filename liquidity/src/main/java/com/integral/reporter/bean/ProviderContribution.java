package com.integral.reporter.bean;

/**
 * @author Rahul Bhattacharjee
 */
public class ProviderContribution {

    private String provider;
    private String stream;
    private double contribution;
    private double price;
    private double size;

    public String getProvider() {
        return provider;
    }

    public ProviderContribution setProvider(String provider) {
        this.provider = provider;
        return this;
    }

    public String getStream() {
        return stream;
    }

    public ProviderContribution setStream(String stream) {
        this.stream = stream;
        return this;
    }

    public double getContribution() {
        return contribution;
    }

    public ProviderContribution setContribution(double contribution) {
        this.contribution = contribution;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public ProviderContribution setPrice(double price) {
        this.price = price;
        return this;
    }

    public double getSize() {
        return size;
    }

    public ProviderContribution setSize(double size) {
        this.size = size;
        return this;
    }
}

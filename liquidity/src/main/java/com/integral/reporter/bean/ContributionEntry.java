package com.integral.reporter.bean;

import com.integral.profile.ErrorType;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Contribution report entry.
 *
 * @author Rahul Bhattacharjee
 */
public class ContributionEntry {

    private String provider;
    private String stream;
    private String ccyPair;
    private Date sampleTime;
    private BigDecimal bid;
    private BigDecimal bidVolume;
    private BigDecimal offer;
    private BigDecimal offerVolume;
    private BigDecimal atVolume;
    private BigDecimal bidContribution;
    private BigDecimal offerContribution;
    private String aggregationKey;
    private ReportEntryType type;
    private ErrorType errorType;

    public String getProvider() {
        return provider;
    }

    public ContributionEntry setProvider(String provider) {
        this.provider = provider;
        return this;
    }

    public String getStream() {
        return stream;
    }

    public ContributionEntry setStream(String stream) {
        this.stream = stream;
        return this;
    }

    public Date getSampleDateTime() {
        return sampleTime;
    }

    public ContributionEntry setSampleTime(Date sampleTime) {
        this.sampleTime = sampleTime;
        return this;
    }

    public String getSampleDate() {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        return format.format(getSampleDateTime());
    }

    public String getSampleTime() {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss.SSS");
        return format.format(getSampleDateTime());
    }


    public BigDecimal getBid() {
        return bid;
    }

    public ContributionEntry setBid(BigDecimal bid) {
        this.bid = bid;
        return this;
    }

    public BigDecimal getBidVolume() {
        return bidVolume;
    }

    public ContributionEntry setBidVolume(BigDecimal bidVolume) {
        this.bidVolume = bidVolume;
        return this;
    }

    public BigDecimal getOffer() {
        return offer;
    }

    public ContributionEntry setOffer(BigDecimal offer) {
        this.offer = offer;
        return this;
    }

    public BigDecimal getOfferVolume() {
        return offerVolume;
    }

    public ContributionEntry setOfferVolume(BigDecimal offerVolume) {
        this.offerVolume = offerVolume;
        return this;
    }

    public BigDecimal getBidContribution() {
        return bidContribution;
    }

    public ContributionEntry setBidContribution(BigDecimal bidContribution) {
        this.bidContribution = bidContribution;
        return this;
    }

    public BigDecimal getOfferContribution() {
        return offerContribution;
    }

    public ContributionEntry setOfferContribution(BigDecimal offerContribution) {
        this.offerContribution = offerContribution;
        return this;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public ContributionEntry setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
        return this;
    }

    public BigDecimal getAtVolume() {
        return atVolume;
    }

    public ContributionEntry setAtVolume(BigDecimal atVolume) {
        this.atVolume = atVolume;
        return this;
    }

    public ReportEntryType getType() {
        return type;
    }

    public ContributionEntry setType(ReportEntryType type) {
        this.type = type;
        return this;
    }

    public String getAggregationKey() {
        return aggregationKey;
    }

    public ContributionEntry setAggregationKey(String aggregationKey) {
        this.aggregationKey = aggregationKey;
        return this;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public ContributionEntry setErrorType(ErrorType errorType) {
        this.errorType = errorType;
        return this;
    }
}

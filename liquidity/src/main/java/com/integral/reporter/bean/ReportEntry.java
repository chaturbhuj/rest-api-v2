package com.integral.reporter.bean;

import com.integral.ds.dto.QuoteType;
import com.integral.profile.ErrorType;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Rahul Bhattacjarjee
 */
public class ReportEntry {

    private String provider;
    private String stream;
    private QuoteType quoteType;
    private String ccyPair;
    private int tier;
    private Date quoteTime;
    private Date sampleTime;
    private BigDecimal bid;
    private BigDecimal offer;
    private BigDecimal bidVolume;
    private BigDecimal offerVolume;
    private double spread;
    private BigDecimal atVolume;
    private ReportEntryType entryType;
    private String aggregationType = "";
    private long minAgeOfRate;
    private long maxAgeOfRate;
    private List<ProviderContribution> bidContribution;
    private List<ProviderContribution> offerContribution;
    private ErrorType errorType = ErrorType.NONE;

    public String getProvider() {
        return provider;
    }

    public ReportEntry setProvider(String provider) {
        this.provider = provider;
        return this;
    }

    public String getStream() {
        return stream;
    }

    public ReportEntry setStream(String stream) {
        this.stream = stream;
        return this;
    }

    public Date getQuoteDateTime() {
        return quoteTime;
    }

    public String getQuoteDate() {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        return format.format(getQuoteDateTime());
    }

    public String getQuoteTime() {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss.SSS");
        return format.format(getQuoteDateTime());
    }

    public String getSampleDate() {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        return format.format(getSampleDateTime());
    }

    public String getSampleTime() {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss.SSS");
        return format.format(getSampleDateTime());
    }

    public ReportEntry setQuoteTime(Date quoteTime) {
        this.quoteTime = quoteTime;
        return this;
    }

    public BigDecimal getBid() {
        return bid;
    }

    public ReportEntry setBid(BigDecimal bid) {
        this.bid = bid;
        return this;
    }

    public BigDecimal getOffer() {
        return offer;
    }

    public ReportEntry setOffer(BigDecimal offer) {
        this.offer = offer;
        return this;
    }

    public double getSpread() {
        return spread;
    }

    public ReportEntry setSpread(double spread) {
        this.spread = spread;
        return this;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public ReportEntry setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
        return this;
    }

    public int getTier() {
        return tier;
    }

    public ReportEntry setTier(int tier) {
        this.tier = tier;
        return this;
    }

    public BigDecimal getBidVolume() {
        return bidVolume;
    }

    public ReportEntry setBidVolume(BigDecimal bidVolume) {
        this.bidVolume = bidVolume;
        return this;
    }

    public BigDecimal getOfferVolume() {
        return offerVolume;
    }

    public ReportEntry setOfferVolume(BigDecimal offerVolume) {
        this.offerVolume = offerVolume;
        return this;
    }

    public BigDecimal getAtVolume() {
        return atVolume;
    }

    public ReportEntry setAtVolume(BigDecimal atVolume) {
        this.atVolume = atVolume;
        return this;
    }

    public Date getSampleDateTime() {
        return sampleTime;
    }

    public ReportEntry setSampleDateTime(Date sampleTime) {
        this.sampleTime = sampleTime;
        return this;
    }

    public ReportEntryType getEntryType() {
        return entryType;
    }

    public ReportEntry setEntryType(ReportEntryType entryType) {
        this.entryType = entryType;
        return this;
    }

    public QuoteType getQuoteType() {
        return quoteType;
    }

    public ReportEntry setQuoteType(QuoteType quoteType) {
        this.quoteType = quoteType;
        return this;
    }

    public String getAggregationType() {
        return aggregationType;
    }

    public void setAggregationType(String aggregationType) {
        this.aggregationType = aggregationType;
    }

    public long getMinAgeOfRate() {
        return minAgeOfRate;
    }

    public ReportEntry setMinAgeOfRate(long minAgeOfRate) {
        this.minAgeOfRate = minAgeOfRate;
        return this;
    }

    public long getMaxAgeOfRate() {
        return maxAgeOfRate;
    }

    public ReportEntry setMaxAgeOfRate(long maxAgeOfRate) {
        this.maxAgeOfRate = maxAgeOfRate;
        return this;
    }

    public List<ProviderContribution> getBidContribution() {
        return bidContribution;
    }

    public ReportEntry setBidContribution(List<ProviderContribution> bidContribution) {
        this.bidContribution = bidContribution;
        return this;
    }

    public List<ProviderContribution> getOfferContribution() {
        return offerContribution;
    }

    public ReportEntry setOfferContribution(List<ProviderContribution> offerContribution) {
        this.offerContribution = offerContribution;
        return this;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public ReportEntry setErrorType(ErrorType errorType) {
        this.errorType = errorType;
        return this;
    }
}

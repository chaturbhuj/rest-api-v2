package com.integral.reporter.bean;

import java.math.BigDecimal;

/**
 * @author Rahul Bhattacharjee
 */
public class ProfileEntry {

    private String provider;
    private String stream;
    private String ccyPair;
    private long time;
    private int tier;
    private long twoSidedTickCount;
    private BigDecimal midPriceMean;
    private BigDecimal spreadAverage;
    private BigDecimal spreadRange;
    private long lastTickTimeStamp;
    private long lastTickAge;
    private BigDecimal averageBidSize;
    private BigDecimal averageAskSize;
    private BigDecimal maxBidPrice;
    private BigDecimal minBidPrice;
    private BigDecimal maxAskPrice;
    private BigDecimal minAskPrice;
    private BigDecimal lastActiveBid;
    private BigDecimal lastActiveAsk;
    private long activeTickCount;
    private long inactiveTickCount;
    private String lastDeclaredStatus;
    private long numberOfBadRecords;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public void setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getTier() {
        return tier;
    }

    public void setTier(int tier) {
        this.tier = tier;
    }

    public long getTwoSidedTickCount() {
        return twoSidedTickCount;
    }

    public void setTwoSidedTickCount(long twoSidedTickCount) {
        this.twoSidedTickCount = twoSidedTickCount;
    }

    public BigDecimal getMidPriceMean() {
        return midPriceMean;
    }

    public void setMidPriceMean(BigDecimal midPriceMean) {
        this.midPriceMean = midPriceMean;
    }

    public BigDecimal getSpreadAverage() {
        return spreadAverage;
    }

    public void setSpreadAverage(BigDecimal spreadAverage) {
        this.spreadAverage = spreadAverage;
    }

    public BigDecimal getSpreadRange() {
        return spreadRange;
    }

    public void setSpreadRange(BigDecimal spreadRange) {
        this.spreadRange = spreadRange;
    }

    public long getLastTickTimeStamp() {
        return lastTickTimeStamp;
    }

    public void setLastTickTimeStamp(long lastTickTimeStamp) {
        this.lastTickTimeStamp = lastTickTimeStamp;
    }

    public long getLastTickAge() {
        return lastTickAge;
    }

    public void setLastTickAge(long lastTickAge) {
        this.lastTickAge = lastTickAge;
    }

    public BigDecimal getAverageBidSize() {
        return averageBidSize;
    }

    public void setAverageBidSize(BigDecimal averageBidSize) {
        this.averageBidSize = averageBidSize;
    }

    public BigDecimal getAverageAskSize() {
        return averageAskSize;
    }

    public void setAverageAskSize(BigDecimal averageAskSize) {
        this.averageAskSize = averageAskSize;
    }

    public BigDecimal getMaxBidPrice() {
        return maxBidPrice;
    }

    public void setMaxBidPrice(BigDecimal maxBidPrice) {
        this.maxBidPrice = maxBidPrice;
    }

    public BigDecimal getMinBidPrice() {
        return minBidPrice;
    }

    public void setMinBidPrice(BigDecimal minBidPrice) {
        this.minBidPrice = minBidPrice;
    }

    public BigDecimal getMaxAskPrice() {
        return maxAskPrice;
    }

    public void setMaxAskPrice(BigDecimal maxAskPrice) {
        this.maxAskPrice = maxAskPrice;
    }

    public BigDecimal getMinAskPrice() {
        return minAskPrice;
    }

    public void setMinAskPrice(BigDecimal minAskPrice) {
        this.minAskPrice = minAskPrice;
    }

    public BigDecimal getLastActiveBid() {
        return lastActiveBid;
    }

    public void setLastActiveBid(BigDecimal lastActiveBid) {
        this.lastActiveBid = lastActiveBid;
    }

    public BigDecimal getLastActiveAsk() {
        return lastActiveAsk;
    }

    public void setLastActiveAsk(BigDecimal lastActiveAsk) {
        this.lastActiveAsk = lastActiveAsk;
    }

    public long getActiveTickCount() {
        return activeTickCount;
    }

    public void setActiveTickCount(long activeTickCount) {
        this.activeTickCount = activeTickCount;
    }

    public long getInactiveTickCount() {
        return inactiveTickCount;
    }

    public void setInactiveTickCount(long inactiveTickCount) {
        this.inactiveTickCount = inactiveTickCount;
    }

    public String getLastDeclaredStatus() {
        return lastDeclaredStatus;
    }

    public void setLastDeclaredStatus(String lastDeclaredStatus) {
        this.lastDeclaredStatus = lastDeclaredStatus;
    }

    public long getNumberOfBadRecords() {
        return numberOfBadRecords;
    }

    public void setNumberOfBadRecords(long numberOfBadRecords) {
        this.numberOfBadRecords = numberOfBadRecords;
    }

    @Override
    public String toString() {
        return "ProfileEntry{" +
                "twoSidedTickCount=" + twoSidedTickCount +
                ", midPriceMean=" + midPriceMean +
                ", spreadAverage=" + spreadAverage +
                ", spreadRange=" + spreadRange +
                ", lastTickTimeStamp=" + lastTickTimeStamp +
                ", lastTickAge=" + lastTickAge +
                ", averageBidSize=" + averageBidSize +
                ", averageAskSize=" + averageAskSize +
                ", maxBidPrice=" + maxBidPrice +
                ", minBidPrice=" + minBidPrice +
                ", maxAskPrice=" + maxAskPrice +
                ", minAskPrice=" + minAskPrice +
                ", lastActiveBid=" + lastActiveBid +
                ", lastActiveAsk=" + lastActiveAsk +
                ", activeTickCount=" + activeTickCount +
                ", inactiveTickCount=" + inactiveTickCount +
                ", lastDeclaredStatus='" + lastDeclaredStatus + '\'' +
                ", numberOfBadRecords=" + numberOfBadRecords +
                '}';
    }
}

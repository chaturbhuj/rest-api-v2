package com.integral.reporter.bean;

import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 * TSReportEntry.
 *
 * @author Rahul Bhattacharjee
 */
public class TSReportEntry{

    private long epochTime;
    private String reportIdentifier;
    private Map<String,String> attributeMap = new HashMap<String,String>();

    public long getEpochTime() {
        return epochTime;
    }

    public void setEpochTime(long epochTime) {
        this.epochTime = epochTime;
    }

    public String getReportIdentifier() {
        return reportIdentifier;
    }

    public void setReportIdentifier(String reportIdentifier) {
        this.reportIdentifier = reportIdentifier;
    }

    public void addAttribute(String key,String value) {
        attributeMap.put(key,value);
    }

    public String serialize() {
        List<String> values = new ArrayList<String>();
        Iterator<String> iterator = attributeMap.values().iterator();
        while(iterator.hasNext()) {
            values.add(iterator.next());
        }
        return StringUtils.join(values,",");
    }
}

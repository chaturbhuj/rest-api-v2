package com.integral.reporter.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Holds reports for all the providers.
 *
 * @author Rahul Bhattacharjee
 */
public class ReportHolder {

    private static String reportColumns = "PROVIDER,STREAM,QUOTE TYPE,CCYPAIR,RATE DATE(MM/dd/yyyy),RATE TIME,SAMPLE DATE,SAMPLE TIME,AGE,LEVEL,BID,BID VOLUME,OFFER,OFFER VOLUME,AT VOLUME,SPREAD,TYPE,ERROR CODE,AGGREGATION KEY,MAX AGE,MIN AGE\n";

    private static String reportColumnsForContributionReport = "PROVIDER,STREAM,CCYPAIR,SAMPLE DATE,SAMPLE TIME,BID,BID VOLUME,OFFER,OFFER VOLUME,AT VOLUME,TYPE,AGGREGATION KEY,BID CONTRIBUTION,OFFER CONTRIBUTION,ERROR CODE\n";

    private static String profileReportColumns = "PROVIDER,STREAM,CCYPAIR,TIME,TIER,TWO SIDED TICK COUNT,MID PRICE MEAN,SPREAD AVERAGE,SPREAD RANGE,LAST TICK TS,LAST TICK AGE," +
            "AVG BIG SIZE,AVG ASK SIZE,MAX BID PRICE,MIN BID PRICE,MAX ASK PRICE,MIN ASK PRICE,LAST ACTIVE BID,LAST ACTIVE ASK,ACTIVE TICK COUNT,INACTIVE TICK COUNT," +
            "LAST DECLARED STATUS,BAD RECORDS COUNT\n";

    private List<Report> sampleReports = new ArrayList<Report>();
    private List<Report> priceAtSizeReports = new ArrayList<Report>();

    private Map<String,List<Report>> aggregatedReportMap = new HashMap<String,List<Report>>();

    private List<ContributionEntry> contributionEntries = new ArrayList<ContributionEntry>();

    private List<ProfileEntry> profileEntries = new ArrayList<ProfileEntry>();

    public String getReportHeading() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Provider,Stream,Currency Pair\n");

        for(Report report : sampleReports) {
            stringBuilder.append(report.toContent()+"\n");
        }
        return stringBuilder.toString();
    }

    public static String getReportColumns() {
        return reportColumns;
    }

    public static String getReportColumnsForContributionReport() {
        return reportColumnsForContributionReport;
    }

    public static String getProfileReportColumns() {
        return profileReportColumns;
    }

    public List<Report> getSampleReports() {
        return sampleReports;
    }

    public void addSampleReport(Report sampleReport) {
        this.sampleReports.add(sampleReport);
    }

    public List<Report> getPriceAtSizeReports() {
        return priceAtSizeReports;
    }

    public void addPriceAtSizeReports(Report priceAtSizeReport) {
        this.priceAtSizeReports.add(priceAtSizeReport);
    }

    public void addAggregatedReportToMap(String aggregationName,List<Report> reports) {
        this.aggregatedReportMap.put(aggregationName,reports);
    }

    public Map<String,List<Report>> getAggregatedReportMap() {
        return this.aggregatedReportMap;
    }

    public List<Report> getReportsForAggregatedStream(String dsma) {
        return this.aggregatedReportMap.get(dsma);
    }

    public void addContributionEntry(ContributionEntry entry) {
        this.contributionEntries.add(entry);
    }

    public List<ContributionEntry> getContributionEntries(){
        return this.contributionEntries;
    }

    public void addProfileEntries(List<ProfileEntry> profileEntries) {
        this.profileEntries.addAll(profileEntries);
    }

    public List<ProfileEntry> getProfileEntries() {
        return this.profileEntries;
    }
}

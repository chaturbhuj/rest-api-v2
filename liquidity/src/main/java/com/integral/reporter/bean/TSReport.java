package com.integral.reporter.bean;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * TS report .
 *
 * @author Rahul Bhattacharjee
 */
public class TSReport {

    private String reportIdentifier;
    private List<TSReportEntry> entries = new ArrayList<TSReportEntry>();

    public String getReportIdentifier() {
        return reportIdentifier;
    }

    public void setReportIdentifier(String reportIdentifier) {
        this.reportIdentifier = reportIdentifier;
    }

    public List<TSReportEntry> getEntries() {
        return this.entries;
    }

    public void addEntry(TSReportEntry entry) {
        entry.setReportIdentifier(this.reportIdentifier);
        this.entries.add(entry);
    }

    public void setEntries(List<TSReportEntry> entryList) {
        this.entries.addAll(entryList);
    }

    public String serialize() {
        List<String> serializedEntries = new ArrayList<String>();
        serializedEntries.add(getReportIdentifier());
        for(TSReportEntry entry : entries) {
            serializedEntries.add(entry.serialize());
        }
        return StringUtils.join(serializedEntries,",");
    }
}

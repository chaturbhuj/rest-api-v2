package com.integral.reporter.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class Report {

    private String provider;
    private String stream;
    private String ccyPair;
    private Date start;
    private Date end;
    private int noOfSamples;

    private List<ReportEntry> entries = new ArrayList<ReportEntry>();

    public Report() {}

    public void addEntry(ReportEntry entry) {
        entries.add(entry);
    }

    public void addEntry(List<ReportEntry> entry) {
        entries.addAll(entry);
    }

    public List<ReportEntry> getEntries() {
        return entries;
    }

    public boolean isEmptyReport() {
        return this.entries.isEmpty() && this.entries.isEmpty();
    }

    public String getProvider() {
        return provider;
    }

    public Report setProvider(String provider) {
        this.provider = provider;
        return this;
    }

    public String getStream() {
        return stream;
    }

    public Report setStream(String stream) {
        this.stream = stream;
        return this;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public Report setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
        return this;
    }

    public Date getStart() {
        return start;
    }

    public Report setStart(Date start) {
        this.start = start;
        return this;
    }

    public Date getEnd() {
        return end;
    }

    public Report setEnd(Date end) {
        this.end = end;
        return this;
    }

    public int getNoOfSamples() {
        return noOfSamples;
    }

    public Report setNoOfSamples(int noOfSamples) {
        this.noOfSamples = noOfSamples;
        return this;
    }

    public String toContent() {
        return getProvider()+ "," + getStream() + "," + getCcyPair();
    }
}

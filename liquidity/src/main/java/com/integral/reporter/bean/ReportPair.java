package com.integral.reporter.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Report Pair representing two reports.One for samples and another for price-at-size.
 *
 * @author Rahul Bhattacharjee
 */
public class ReportPair {

    private Report sampleReport;
    private Report priceAtSize;
    private List<ProfileEntry> profileEntryList = new ArrayList<ProfileEntry>();

    public Report getSampleReport() {
        return sampleReport;
    }

    public void setSampleReport(Report sampleReport) {
        this.sampleReport = sampleReport;
    }

    public Report getPriceAtSize() {
        return priceAtSize;
    }

    public void setPriceAtSize(Report priceAtSize) {
        this.priceAtSize = priceAtSize;
    }

    public void addProfileEntry(ProfileEntry entry) {
        this.profileEntryList.add(entry);
    }

    public List<ProfileEntry> getProfileEntry() {
        return this.profileEntryList;
    }
}

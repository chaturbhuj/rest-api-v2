package com.integral.reporter.bean;

/**
 * @author Rahul Bhattacharjee
 */
public class ProviderStream{

    private String provider;
    private String stream;
    private boolean multiQuote;

    public ProviderStream() {
    }

    public ProviderStream(String provider, String stream) {
        this.provider = provider;
        this.stream = stream;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public boolean isMultiQuote() {
        return multiQuote;
    }

    public void setMultiQuote(boolean multiQuote) {
        this.multiQuote = multiQuote;
    }

    @Override
    public String toString() {
        return "ProviderStream{" +
                "provider='" + provider + '\'' +
                ", stream='" + stream + '\'' +
                ", multiQuote=" + multiQuote +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProviderStream that = (ProviderStream) o;

        if (provider != null ? !provider.equals(that.provider) : that.provider != null) return false;
        if (stream != null ? !stream.equals(that.stream) : that.stream != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = provider != null ? provider.hashCode() : 0;
        result = 31 * result + (stream != null ? stream.hashCode() : 0);
        return result;
    }
}

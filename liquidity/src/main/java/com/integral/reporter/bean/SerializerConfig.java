package com.integral.reporter.bean;

import com.integral.ds.commons.util.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Serializer configuration for TQD serializer.
 *
 * @author Rahul Bhattacharjee
 */
public class SerializerConfig {

    private String outputFile;
    private String fixedAttribute;
    private List<Pair<String,String>> dynamicAttribute = new ArrayList<Pair<String,String>>();

    public String getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
    }

    public String getFixedAttribute() {
        return fixedAttribute;
    }

    public void setFixedAttribute(String fixedAttribute) {
        this.fixedAttribute = fixedAttribute;
    }

    public SerializerConfig addDynamicAttribute(String key , String type) {
        dynamicAttribute.add(new Pair<String,String>(key,type));
        return this;
    }

    public List<Pair<String,String>> getDynamicAttribute() {
        return this.dynamicAttribute;
    }
}

package com.integral.reporter.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * Entry type enumeration.
 *
 * @author Rahul Bhattacharjee
 */
public enum ReportEntryType {

    TICK_DATA("TICK"),
    PRICE_AT_SIZE_TICK("TA"),
    DIRECT_SINGLE_STREAM_AGGREGATION("DSA"),
    DIRECT_MULTIPLE_STREAM_AGGREGATION("DMSA"),
    CONTRIBUTION_DATA("CONT");

    private static Map<String,ReportEntryType> NAME_TO_TYPE_MAPPING = new HashMap<String,ReportEntryType>();

    static {
        populateTypeMapping();
    }

    private final String typeId;

    ReportEntryType(String typeId) {
        this.typeId = typeId;
    }
    public String getTypeId(){
        return this.typeId;
    }

    private static void populateTypeMapping() {
        ReportEntryType [] types = values();
        for(ReportEntryType type : types) {
            NAME_TO_TYPE_MAPPING.put(type.getTypeId(),type);
        }
    }

    public static ReportEntryType getTypeForId(String id) {
        return NAME_TO_TYPE_MAPPING.get(id);
    }
}

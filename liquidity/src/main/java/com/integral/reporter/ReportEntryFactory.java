package com.integral.reporter;

import com.integral.ds.dto.QuoteType;
import com.integral.reporter.bean.ReportEntry;
import com.integral.reporter.bean.ReportEntryType;

/**
 * @author Rahul Bhattacharjee
 */
public class ReportEntryFactory {

    private String provider;
    private String stream;
    private String ccyPair;
    private ReportEntryType entryType;
    private QuoteType quoteType;

    public ReportEntryFactory(String provider, String stream, String ccyPair,ReportEntryType entryType,QuoteType quoteType) {
        this.provider = provider;
        this.stream = stream;
        this.ccyPair = ccyPair;
        this.entryType = entryType;
        this.quoteType = quoteType;
    }

    public ReportEntry getNewEntry() {
        ReportEntry entry = new ReportEntry();
        entry.setProvider(provider).setStream(stream).setCcyPair(ccyPair).setEntryType(entryType).setQuoteType(quoteType);
        return entry;
    }

    public String getProvider() {
        return provider;
    }

    public String getStream() {
        return stream;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public ReportEntryType getEntryType() {
        return entryType;
    }

    public QuoteType getQuoteType() {
        return quoteType;
    }
}

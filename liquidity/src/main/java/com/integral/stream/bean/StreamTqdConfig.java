package com.integral.stream.bean;

import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public class StreamTqdConfig {

    private String provider;
    private String stream;
    private String ccyPair;
    private Date start;
    private Date end;
    private int noOfSamples;

    public String getProvider() {
        return provider;
    }

    public StreamTqdConfig setProvider(String provider) {
        this.provider = provider;
        return this;
    }

    public String getStream() {
        return stream;
    }

    public StreamTqdConfig setStream(String stream) {
        this.stream = stream;
        return this;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public StreamTqdConfig setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
        return this;
    }

    public Date getStart() {
        return start;
    }

    public StreamTqdConfig setStart(Date start) {
        this.start = start;
        return this;
    }

    public Date getEnd() {
        return end;
    }

    public StreamTqdConfig setEnd(Date end) {
        this.end = end;
        return this;
    }

    public int getNoOfSamples() {
        return noOfSamples;
    }

    public StreamTqdConfig setNoOfSamples(int noOfSamples) {
        this.noOfSamples = noOfSamples;
        return this;
    }

    public void validate() {
        boolean status = provider != null && stream != null & ccyPair != null && start != null && end != null;
        if(!status) {
            throw new IllegalArgumentException("One or more fields are null.");
        }
    }
}

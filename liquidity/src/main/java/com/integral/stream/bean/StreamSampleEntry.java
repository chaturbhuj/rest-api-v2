package com.integral.stream.bean;

import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public class StreamSampleEntry {
    private float bidPrice;
    private float askPrice;
    private Date sampleTime;

    public float getBidPrice() {
        return bidPrice;
    }

    public StreamSampleEntry setBidPrice(float bidPrice) {
        this.bidPrice = bidPrice;
        return this;
    }

    public float getAskPrice() {
        return askPrice;
    }

    public StreamSampleEntry setAskPrice(float askPrice) {
        this.askPrice = askPrice;
        return  this;
    }

    public Date getSampleTime() {
        return sampleTime;
    }

    public StreamSampleEntry setSampleTime(Date sampleTime) {
        this.sampleTime = sampleTime;
        return this;
    }

    @Override
    public String toString() {
        return "StreamSampleEntry{" +
                "bidPrice=" + bidPrice +
                ", askPrice=" + askPrice +
                ", sampleTime=" + sampleTime +
                '}';
    }
}

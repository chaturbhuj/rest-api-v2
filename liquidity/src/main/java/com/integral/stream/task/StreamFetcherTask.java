package com.integral.stream.task;

import com.integral.ds.emscope.quote.RatesQuoteService;
import com.integral.ds.emscope.quote.impl.RateQuoteServiceDenseSamples;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.stream.bean.StreamSampleEntry;
import com.integral.stream.bean.StreamTqdConfig;

import java.util.*;
import java.util.concurrent.Callable;

/**
 * Stream fetcher task to fetch quotes from S3 and bundle it based on provider-stream-tier.
 *
 * @author Rahul Bhattacharjee
 */
public class StreamFetcherTask implements Callable<Map<String,List<StreamSampleEntry>>> {

    private RatesQuoteService quoteService = new RateQuoteServiceDenseSamples();
    private StreamTqdConfig config = null;

    public StreamFetcherTask(StreamTqdConfig config) {
        this.config = config;
    }

    @Override
    public Map<String,List<StreamSampleEntry>> call() throws Exception {
        config.validate();

        Map<String,List<StreamSampleEntry>> result = new HashMap<String,List<StreamSampleEntry>>();

        String provider = config.getProvider();
        String stream = config.getStream();

        List<Quote> quotes = quoteService.getSampledQuotes(config.getProvider(),config.getStream(),
                config.getCcyPair(),config.getStart(),config.getEnd(),config.getNoOfSamples());

        for(Quote quote : quotes) {
            List<RestRatesObject> rates = getPrevailingRateFromQuote(quote);
            Date sampleTime = quote.getSampleTime();
            for(RestRatesObject rate : rates) {
                int tier = rate.getLvl();
                String key = getKey(provider,stream,tier);
                List<StreamSampleEntry> entries = result.get(key);
                if(entries == null) {
                    entries = new ArrayList<StreamSampleEntry>();
                    result.put(key,entries);
                }
                entries.add(getEntryForRate(rate,sampleTime));
            }
        }
        return result;
    }

    private List<RestRatesObject> getPrevailingRateFromQuote(Quote quote) {
        NavigableMap<Long,List<RestRatesObject>> rateMap = new TreeMap<Long,List<RestRatesObject>>();
        for(RestRatesObject rate : quote.getRates()) {
            long time = rate.getTmstmp();
            List<RestRatesObject> rateEntries = rateMap.get(time);
            if(rateEntries == null) {
                rateEntries = new ArrayList<RestRatesObject>();
                rateMap.put(time,rateEntries);
            }
            rateEntries.add(rate);
        }
        return rateMap.firstEntry().getValue();
    }

    private StreamSampleEntry getEntryForRate(RestRatesObject rate, Date sampleTime) {
        StreamSampleEntry entry = new StreamSampleEntry();
        entry.setAskPrice(rate.getAsk_price().floatValue()).setBidPrice(rate.getBid_price().floatValue()).setSampleTime(sampleTime);
        return entry;
    }

    private String getKey(String provider, String stream, int tier) {
        return provider+ "-" + stream + "-" + tier;
    }
}

package com.integral.stream.util;

import com.integral.reporter.bean.TSReport;
import com.integral.reporter.bean.TSReportEntry;
import com.integral.stream.bean.StreamSampleEntry;
import com.integral.ts.TSReportGenerator;

import java.util.List;

/**
 * Utility for transforming beans of one kind to another.
 *
 * @author Rahul Bhattacharjee
 */
public class TSReportGeneratorUtil {

    public static TSReport getReportForStreamEntry(String key , List<StreamSampleEntry> entries) {
        TSReport result = new TSReport();
        result.setReportIdentifier(key);
        for(StreamSampleEntry entry : entries) {
            result.addEntry(getReportEntryForStreamSampleEntry(key,entry));
        }
        return result;
    }

    public static TSReportEntry getReportEntryForStreamSampleEntry(String key, StreamSampleEntry inputEntry) {
        TSReportEntry entry = new TSReportEntry();
        entry.setEpochTime(inputEntry.getSampleTime().getTime());
        entry.setReportIdentifier(key);
        entry.addAttribute(TSReportGenerator.BID_ID,inputEntry.getBidPrice()+"");
        entry.addAttribute(TSReportGenerator.OFFER_ID,inputEntry.getAskPrice()+"");
        return entry;
    }
}

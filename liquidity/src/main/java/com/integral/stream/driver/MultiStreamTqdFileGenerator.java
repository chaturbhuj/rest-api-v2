package com.integral.stream.driver;

import com.integral.reporter.bean.SerializerConfig;
import com.integral.reporter.bean.TSReport;
import com.integral.serializer.TQDSerializer;
import com.integral.serializer.impl.TQDFileSerializerImpl;
import com.integral.stream.bean.StreamSampleEntry;
import com.integral.stream.bean.StreamTqdConfig;
import com.integral.stream.task.StreamFetcherTask;
import com.integral.stream.util.TSReportGeneratorUtil;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;

/**
 * @author Rahul Bhattacharjee
 */
public class MultiStreamTqdFileGenerator {

    public static void main(String [] argv) throws Exception {
        List<StreamTqdConfig> configs = getConfigs();

        ExecutorService service = Executors.newFixedThreadPool(5,new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
        });

        List<Future<Map<String,List<StreamSampleEntry>>>> futures = new ArrayList<>();
        for(StreamTqdConfig config : configs) {
            Future<Map<String,List<StreamSampleEntry>>> future = service.submit(new StreamFetcherTask(config));
            futures.add(future);
        }

        Map<String,List<StreamSampleEntry>> tqdEntries =  waitForCompletion(futures);
        System.out.println("Here");
        serialize(tqdEntries);
    }

    private static void serialize(Map<String, List<StreamSampleEntry>> tqdEntries) {
        List<TSReport> reports = new ArrayList<TSReport>();
        Iterator<Map.Entry<String,List<StreamSampleEntry>>> iterator = tqdEntries.entrySet().iterator();

        while(iterator.hasNext()) {
            Map.Entry<String,List<StreamSampleEntry>> entry = iterator.next();
            TSReport report = TSReportGeneratorUtil.getReportForStreamEntry(entry.getKey(),entry.getValue());
            reports.add(report);
        }

        String fileName = "C:\\Users\\bhattacharjeer\\Desktop\\tqd_output\\multi-stream-input.tqd";

        TQDSerializer serializer = new TQDFileSerializerImpl();

        SerializerConfig config = new SerializerConfig();
        config.setOutputFile(fileName);
        config.setFixedAttribute("Date/Time,String");
        config.addDynamicAttribute("Bid","Float");
        config.addDynamicAttribute("Offer","Float");
        serializer.serializer(config,reports);
    }

    private static Map<String,List<StreamSampleEntry>> waitForCompletion(List<Future<Map<String, List<StreamSampleEntry>>>> futures) {
        Map<String,List<StreamSampleEntry>> result = new HashMap<String,List<StreamSampleEntry>>();
        for(Future<Map<String,List<StreamSampleEntry>>> future : futures) {
            try {
                Map<String,List<StreamSampleEntry>> taskResult = future.get();
                result.putAll(taskResult);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private static List<StreamTqdConfig> getConfigs() throws Exception {
        String startTimestamp = "05.08.2014 07:00:00.000";
        String endTimestamp =   "05.08.2014 10:00:00.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        String provider1 = "BOAN";
        String stream1 = "BandC";
        String ccypair1 = "EURUSD";

        String provider2 = "CITI";
        String stream2 = "Integral1";
        String ccypair2 = "EURUSD";

        StreamTqdConfig config1 = new StreamTqdConfig();
        config1.setProvider(provider1).setStream(stream1).setCcyPair(ccypair1).setStart(startTime).setEnd(endTime).setNoOfSamples(100);

        StreamTqdConfig config2 = new StreamTqdConfig();
        config2.setProvider(provider2).setStream(stream2).setCcyPair(ccypair2).setStart(startTime).setEnd(endTime).setNoOfSamples(100);

        List<StreamTqdConfig> result = new ArrayList<StreamTqdConfig>();
        result.add(config1);
        result.add(config2);

        return result;
    }
}

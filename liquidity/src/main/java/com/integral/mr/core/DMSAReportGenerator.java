package com.integral.mr.core;

import com.integral.aggregate.AggregatedBookCalculator;
import com.integral.mr.job.lpa.ReportWriter;
import com.integral.reporter.ReportFilter;
import com.integral.reporter.bean.Report;
import com.integral.reporter.bean.ReportEntry;
import com.integral.reporter.bean.ReportHolder;
import com.integral.util.LPAUtility;
import com.integral.util.ReportWriterUtil;
import com.integral.util.ResourceUtil;
import org.apache.hadoop.io.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class DMSAReportGenerator {

    private Text VALUE = new Text();

    private static AggregatedBookCalculator reportBookCalculator = new AggregatedBookCalculator();
    private static ReportFilter reportFilter = new CurrencyPairReportFilter();

    public void generateAggregatedReport(ReportHolder reportHolder,String reportName, ReportWriter reportWriter) throws IOException, InterruptedException {
        List<String> ccyPairs = ResourceUtil.getCurrencyPairs();
        List<Long> volumes = ResourceUtil.getVolumeList();
        List<Report> sampleReports = reportHolder.getSampleReports();

        List<Report> aggReports = new ArrayList<Report>();

        for (String ccyPair : ccyPairs) {
            List<Report> filteredReports = reportFilter.filteredReports(sampleReports, null, ccyPair);
            Report report = reportBookCalculator.getReportForAggregatedStreams(reportName, filteredReports, volumes, ccyPair);
            ReportWriterUtil.addReportTypeToEntries(report, reportName);
            aggReports.add(report);
        }
        reportHolder.addAggregatedReportToMap(reportName, aggReports);
        for (Report report : aggReports) {
            for (ReportEntry reportEntry : report.getEntries()) {
                LPAUtility.setEntryStatus(reportEntry);
                VALUE.set(ReportWriterUtil.serializeReportEntry(reportEntry));
                reportWriter.writeToLPReport(VALUE);
            }
        }
    }
}

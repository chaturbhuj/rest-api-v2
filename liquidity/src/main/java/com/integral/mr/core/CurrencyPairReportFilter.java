package com.integral.mr.core;

import com.integral.ds.dto.ProviderStream;
import com.integral.reporter.ReportFilter;
import com.integral.reporter.bean.Report;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class CurrencyPairReportFilter implements ReportFilter {

    @Override
    public List<Report> filteredReports(List<Report> reports, List<ProviderStream> providerStreams, String ccyPair) {
        List<Report> result = new ArrayList<Report>();
        for(Report report : reports) {
            String reportCcyPair = report.getCcyPair();
            if(reportCcyPair.equalsIgnoreCase(ccyPair)) {
                result.add(report);
            }
        }
        return result;
    }
}

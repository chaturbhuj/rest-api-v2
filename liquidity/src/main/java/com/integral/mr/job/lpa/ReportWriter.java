package com.integral.mr.job.lpa;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;

public class ReportWriter {

    private MultipleOutputs multipleOutputs;

    public ReportWriter(Reducer.Context context) {
        multipleOutputs = new MultipleOutputs(context);
    }

    public void writeToLPReport(Text value) throws IOException , InterruptedException {
        multipleOutputs.write("LPA", NullWritable.get(),value);
    }

    public void writeToContributionReport(Text value) throws IOException , InterruptedException {
        multipleOutputs.write("CONTRIBUTION",NullWritable.get(),value,"contribution/CONTRIBUTION");
    }

    public void close() throws IOException, InterruptedException {
        multipleOutputs.close();
    }
}

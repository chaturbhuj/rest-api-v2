package com.integral.mr.job.dsa;

import com.integral.ds.dto.QuoteType;
import com.integral.ds.emscope.rates.Quote;
import com.integral.mr.io.ProviderStreamCurrencyPair;
import com.integral.mr.io.ProviderStreamCurrencyPairWritable;
import com.integral.mr.io.QuoteWritable;
import com.integral.mr.util.LPAHelper;
import com.integral.mr.util.MRJobUtil;
import com.integral.reporter.bean.Report;
import com.integral.reporter.bean.ReportEntry;
import com.integral.reporter.bean.ReportHolder;
import com.integral.util.LPAUtility;
import com.integral.util.ReportWriterUtil;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * LPADSA reducer.
 *
 * @author Rahul Bhattacharjee
 */
public class LPADSAReducer extends Reducer<ProviderStreamCurrencyPairWritable,QuoteWritable,NullWritable,Text> {

    private Text VALUE = new Text();
    private MultipleOutputs multipleOutputs;
    private Set<ProviderStreamCurrencyPair> headerAvailable = new HashSet<ProviderStreamCurrencyPair>();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        multipleOutputs = new MultipleOutputs(context);
    }

    @Override
    protected void reduce(ProviderStreamCurrencyPairWritable key, Iterable<QuoteWritable> values, Context context) throws IOException, InterruptedException {
        generateDSA(key,values);
    }

    private ReportHolder generateDSA(ProviderStreamCurrencyPairWritable key , Iterable<QuoteWritable> values) throws IOException, InterruptedException {
        Iterator<QuoteWritable> iterator = values.iterator();
        LPAHelper helper = new LPAHelper();
        while(iterator.hasNext()) {
            QuoteWritable quoteWritable = iterator.next();
            String provider = quoteWritable.getProvider();
            String stream = quoteWritable.getStream();
            String ccyPair = quoteWritable.getCurrencyPair();
            String quoteType = quoteWritable.getQuoteType();
            Quote quote = MRJobUtil.getQuote(quoteWritable);
            helper.addToReport(provider,stream,ccyPair, QuoteType.valueOf(quoteType),quote);
        }

        ReportHolder reportHolder = helper.getReportHolder();

        for(Report report : reportHolder.getSampleReports()) {
            for(ReportEntry reportEntry : report.getEntries()) {
                LPAUtility.setEntryStatus(reportEntry);
                VALUE.set(ReportWriterUtil.serializeReportEntry(reportEntry));
                write(key,VALUE);
            }
        }

        for(Report report : reportHolder.getPriceAtSizeReports()) {
            for(ReportEntry reportEntry : report.getEntries()) {
                LPAUtility.setEntryStatus(reportEntry);
                VALUE.set(ReportWriterUtil.serializeReportEntry(reportEntry));
                write(key,VALUE);
            }
        }
        return reportHolder;
    }

    private void write(ProviderStreamCurrencyPairWritable key,Text value) throws InterruptedException ,IOException {
        addHeader(key);
        multipleOutputs.write(NullWritable.get(),value,getHeaderFolderStructure(key));
    }

    private void addHeader(ProviderStreamCurrencyPairWritable key) throws IOException,InterruptedException {
        ProviderStreamCurrencyPair pair = key.getPOJO();
        if(!headerAvailable.contains(pair)){
            VALUE.set(ReportHolder.getReportColumns());
            multipleOutputs.write(NullWritable.get(),VALUE,getHeaderFolderStructure(key));
            headerAvailable.add(pair);
        }
    }

    private String getHeaderFolderStructure(ProviderStreamCurrencyPairWritable key) {
        return key.getProvider()+"/" + key.getStream() + "/" + key.getCcyPair();
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        multipleOutputs.close();
    }
}

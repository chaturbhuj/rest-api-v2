package com.integral.mr.job.dsa;

import com.integral.mr.input.S3ConfigReaderInputFormatter;
import com.integral.mr.io.ProviderStreamCurrencyPairWritable;
import com.integral.mr.io.QuoteWritable;
import com.integral.mr.job.lpa.DMSAPartitioner;
import com.integral.mr.job.lpa.LPAJob;
import com.integral.mr.job.lpa.LPAMapper;
import com.integral.mr.output.CSVFileOutputFormatter;
import com.integral.mr.util.MRJobUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Generates DSA profile for configured provider stream. It's a map only job.
 *
 * @author Rahul Bhattacharjee
 */
public class LPADSAJob extends Configured implements Tool {

    @Override
    public int run(String[] arguments) throws Exception {
        String outputDir = arguments[0];
        Configuration conf = getConf();

        // prod
        conf.set(MRJobUtil.LPA_START_DATE_KEY,"08.26.2014 00:00:00.000");
        conf.set(MRJobUtil.LPA_END_DATE_KEY,"08.27.2014 00:00:00.000");
        conf.set(MRJobUtil.LPA_NO_OF_SAMPLES,"1000");
        conf.set(MRJobUtil.LPA_PROVIDER_STREAM_CONFIG_FETCHER_TYPE,"S3");
        conf.set("mapred.child.java.opts","-Xmx2096m");
        conf.set("mapred.task.timeout","1200000");
        conf.set("mapreduce.map.maxattempts","1");
        conf.set("mapreduce.map.failures.maxpercent","20");

        // dev
        //conf.set(MRJobUtil.LPA_START_DATE_KEY,"08.11.2014 00:00:00.000");
        //conf.set(MRJobUtil.LPA_END_DATE_KEY,"08.12.2014 00:00:00.000");
        //conf.set(MRJobUtil.LPA_NO_OF_SAMPLES,"10");
        //conf.set(MRJobUtil.LPA_PROVIDER_STREAM_CONFIG_FETCHER_TYPE,"STATIC");

        conf.set("fs.s3.awsAccessKeyId","AKIAI4JZNEOZDPEU2R6A");
        conf.set("fs.s3.awsSecretAccessKey","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");

        Job job = new Job(conf, "LPA-DSA-job");

        job.setJarByClass(LPADSAJob.class);
        job.setMapperClass(LPADSAMapper.class);
        job.setReducerClass(LPADSAReducer.class);

        job.setMapOutputKeyClass(ProviderStreamCurrencyPairWritable.class);
        job.setMapOutputValueClass(QuoteWritable.class);

        job.setPartitionerClass(LPADSAPartitioner.class);

        job.setNumReduceTasks(2);

        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Text.class);

        job.setInputFormatClass(S3ConfigReaderInputFormatter.class);

        LazyOutputFormat.setOutputFormatClass(job, CSVFileOutputFormatter.class);
        FileOutputFormat.setOutputPath(job, new Path(outputDir));

        return job.waitForCompletion(true)? 0:1;
    }

    public static void main(String[] args) throws Exception {
        int result = ToolRunner.run(new Configuration(), new LPADSAJob(), args);
        System.exit(result);
    }
}

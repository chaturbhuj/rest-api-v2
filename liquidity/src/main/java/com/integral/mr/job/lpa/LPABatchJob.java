package com.integral.mr.job.lpa;

import com.integral.driver.TimeRange;
import com.integral.mr.util.MRJobUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Running LPA batch job.
 *
 * @author Rahul Bhattacharjee
 */
public class LPABatchJob {

    public static void main(String [] argv) throws Exception {

        String baseOutputDirectory = argv[0];

        // PROD
        //String startTime = "08.11.2014 00:00:00.000";
        //String endTime = "08.16.2014 00:00:00.000";
        //int samples = 1000;

        // DEV
        String startTime = "08.18.2014 00:00:00.000";
        String endTime = "08.23.2014 00:00:00.000";
        int samples = 1000;

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date start = dateFormat.parse(startTime);
        Date end = dateFormat.parse(endTime);

        List<TimeRange> timeRangeList = MRJobUtil.getTimeRangeList(start,end);

        for(TimeRange timeRange : timeRangeList) {
            getTaskRunnable(timeRange.getStart(),timeRange.getEnd(),samples,baseOutputDirectory).run();
        }
    }

    private static Runnable getTaskRunnable(Date start, Date end, int samples, String baseOutputDirectory) {
        Runnable runnable = new LPAJobRunner(start, end, samples, baseOutputDirectory);
        return runnable;
    }
}

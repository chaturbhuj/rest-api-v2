package com.integral.mr.job.dsa;

import com.integral.mr.io.ProviderStreamCurrencyPairWritable;
import com.integral.mr.io.QuoteWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Mapper for LPA DSA. Idea is to partition based upon provider-stream-ccypair and then generate TICK and DSA reports in reducer.
 *
 * @author Rahul Bhattacharjee
 */
public class LPADSAMapper extends Mapper<NullWritable,QuoteWritable,ProviderStreamCurrencyPairWritable,QuoteWritable> {

    private ProviderStreamCurrencyPairWritable providerStreamCurrencyPairWritable = new ProviderStreamCurrencyPairWritable();

    @Override
    protected void map(NullWritable key, QuoteWritable rate, Context context) throws IOException, InterruptedException {
        providerStreamCurrencyPairWritable.setProvider(rate.getProvider());
        providerStreamCurrencyPairWritable.setStream(rate.getStream());
        providerStreamCurrencyPairWritable.setCcyPair(rate.getCurrencyPair());

        context.write(providerStreamCurrencyPairWritable,rate);
    }
}

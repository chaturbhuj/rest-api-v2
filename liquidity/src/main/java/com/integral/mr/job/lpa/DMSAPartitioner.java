package com.integral.mr.job.lpa;

import com.integral.ds.dto.ProviderStream;
import com.integral.mr.io.QuoteWritable;
import com.integral.mr.util.MRJobUtil;
import com.integral.util.ResourceUtil;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Partitioner to partition the K,V pairs based on DMSA id.
 *
 * @author Rahul Bhattacharjee
 */
public class DMSAPartitioner extends Partitioner<Text,QuoteWritable>{

    private Map<String,Integer> partitionIndex = new HashMap<String,Integer>();

    public DMSAPartitioner(){
        Map<String,List<ProviderStream>> dmsaConfig = ResourceUtil.getAggregatedReportInputs();
        Iterator<String> keyIterator = dmsaConfig.keySet().iterator();
        int count = 0;
        while(keyIterator.hasNext()) {
            String dmsaKey = keyIterator.next();
            partitionIndex.put(dmsaKey,count++);
        }
        partitionIndex.put(MRJobUtil.SINGLE_LP.toString(),count);
    }

    @Override
    public int getPartition(Text text, QuoteWritable quoteWritable, int index) {
        return partitionIndex.get(text.toString());
    }
}

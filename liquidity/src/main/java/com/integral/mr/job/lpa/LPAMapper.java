package com.integral.mr.job.lpa;

import com.integral.ds.dto.ProviderStream;
import com.integral.mr.io.QuoteWritable;
import com.integral.mr.io.RateObjectWritable;
import com.integral.mr.util.MRJobUtil;
import com.integral.util.ResourceUtil;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.*;

/**
 * @author Rahul Bhattacharjee
 */
public class LPAMapper extends Mapper<NullWritable,QuoteWritable,Text,QuoteWritable> {

    private Map<ProviderStream,Set<String>> reverseIndex = null;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        Map<String,List<ProviderStream>> dmsaConfig = ResourceUtil.getAggregatedReportInputs();
        createReverseIndex(dmsaConfig);
    }

    @Override
    protected void map(NullWritable key, QuoteWritable rate, Context context) throws IOException, InterruptedException {
        ProviderStream providerStream = new ProviderStream();
        providerStream.setProvider(rate.getProvider());
        providerStream.setStream(rate.getStream());

        Set<String> profileList = reverseIndex.get(providerStream);
        if(profileList != null) {
            for(String profile : profileList) {
                context.write(new Text(profile),rate);
            }
        } else {
            context.write(MRJobUtil.SINGLE_LP,rate);
        }
    }

    private void createReverseIndex(Map<String, List<ProviderStream>> dmsaConfig) {
        reverseIndex = new HashMap<ProviderStream,Set<String>>();
        Iterator<Map.Entry<String,List<ProviderStream>>> iterator = dmsaConfig.entrySet().iterator();

        while(iterator.hasNext()) {
            Map.Entry<String,List<ProviderStream>> entry = iterator.next();
            String profile = entry.getKey();
            List<ProviderStream> providerStreams = entry.getValue();
            for(ProviderStream providerStream : providerStreams) {
                Set<String> values = reverseIndex.get(providerStream);
                if(values == null) {
                    values = new HashSet<String>();
                    reverseIndex.put(providerStream,values);
                }
                values.add(profile);
            }
        }
    }
}

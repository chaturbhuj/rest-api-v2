package com.integral.mr.job.lpa;

import com.integral.mr.input.S3ConfigReaderInputFormatter;
import com.integral.mr.io.QuoteWritable;
import com.integral.mr.output.CSVFileOutputFormatter;
import com.integral.mr.util.MRJobUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * LPA Job runner.
 * @author Rahul Bhattacharjee
 */
public class LPAJobRunner extends Configured implements Runnable , Tool {

    private Date start;
    private Date end;
    private int samples;
    private String baseOutputDir;

    public LPAJobRunner(){}

    public LPAJobRunner(Date start, Date end, int samples, String baseOutputDir) {
        this.start = start;
        this.end = end;
        this.samples = samples;
        this.baseOutputDir = baseOutputDir;
    }

    @Override
    public void run() {
        int result = 0;
        try {
            result = ToolRunner.run(new Configuration(), new LPAJobRunner(), getArguments());
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Exception while running job.",e);
        }
        if(result != 0) {
            throw new IllegalArgumentException("Exception while running job.");
        }
    }

    private String[] getArguments() throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        String startTime = dateFormat.format(start);
        String endTime = dateFormat.format(end);

        SimpleDateFormat outputDirPathFormatter = new SimpleDateFormat("MMddy");
        String suffix = outputDirPathFormatter.format(start);
        String fullOutputDir = baseOutputDir + "/" + suffix;

        System.out.println("Fulloputput dir " + fullOutputDir);

        String [] arguments = new String[4];
        arguments[0] = startTime;
        arguments[1] = endTime;
        arguments[2] = Integer.toString(samples);
        arguments[3] = fullOutputDir;
        return arguments;
    }

    @Override
    public int run(String[] arguments) throws Exception {
        Configuration conf = getConf();
        conf.set(MRJobUtil.LPA_START_DATE_KEY,arguments[0]);
        conf.set(MRJobUtil.LPA_END_DATE_KEY,arguments[1]);
        conf.set(MRJobUtil.LPA_NO_OF_SAMPLES,arguments[2]);
        conf.set(MRJobUtil.LPA_PROVIDER_STREAM_CONFIG_FETCHER_TYPE,"STATIC");

        // Prod configuration
        conf.set("mapred.child.java.opts","-Xmx2096m");
        conf.set("mapred.task.timeout","1200000");

        conf.set("fs.s3.awsAccessKeyId","AKIAI4JZNEOZDPEU2R6A");
        conf.set("fs.s3.awsSecretAccessKey","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");

        Job job = new Job(conf, "LPA job");

        job.setJarByClass(LPAJob.class);
        job.setMapperClass(LPAMapper.class);
        job.setReducerClass(LPAReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(QuoteWritable.class);

        job.setPartitionerClass(DMSAPartitioner.class);

        int numberOfReducers = MRJobUtil.numberOfReducers();
        job.setNumReduceTasks(numberOfReducers);

        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Text.class);

        job.setInputFormatClass(S3ConfigReaderInputFormatter.class);

        LazyOutputFormat.setOutputFormatClass(job, CSVFileOutputFormatter.class);
        FileOutputFormat.setOutputPath(job, new Path(arguments[3]));
        MultipleOutputs.addNamedOutput(job, "LPA", CSVFileOutputFormatter.class, NullWritable.class, Text.class);
        MultipleOutputs.addNamedOutput(job, "CONTRIBUTION", CSVFileOutputFormatter.class,NullWritable.class, Text.class);

        return job.waitForCompletion(true)? 0:1;
    }

    @Override
    public String toString() {
        return "LPAJobRunner{" +
                "start=" + start +
                ", end=" + end +
                ", samples=" + samples +
                ", baseOutputDir='" + baseOutputDir + '\'' +
                '}';
    }
}

package com.integral.mr.job.lpa;

import com.integral.aggregate.ContributionReportGenerator;
import com.integral.ds.dto.QuoteType;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.mr.core.DMSAReportGenerator;
import com.integral.mr.io.QuoteWritable;
import com.integral.mr.io.RateObjectWritable;
import com.integral.mr.util.LPAHelper;
import com.integral.mr.util.MRJobUtil;
import com.integral.reporter.bean.*;
import com.integral.util.LPAUtility;
import com.integral.util.ReportWriterUtil;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * LPA reducer for generating the output csv files.
 *
 * @author Rahul Bhattacharjee
 */
public class LPAReducer extends Reducer<Text,QuoteWritable,NullWritable,Text> {

    private Text VALUE = new Text();
    private ReportWriter reportWriter;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        reportWriter = new ReportWriter(context);
        createHeaders();
    }

    private void createHeaders() throws IOException,InterruptedException {
        VALUE.set(ReportHolder.getReportColumns());
        reportWriter.writeToLPReport(VALUE);
        VALUE.set(ReportHolder.getReportColumnsForContributionReport());
        reportWriter.writeToContributionReport(VALUE);
    }

    @Override
    protected void reduce(Text key, Iterable<QuoteWritable> values, Context context) throws IOException, InterruptedException {
        ReportHolder reportHolder =  generateDSA(values);
        if(MRJobUtil.isNotSingleLP(key)) {
            generateDMSA(reportHolder,key.toString());
            generateContributionFactorReport(reportHolder);
        }
    }

    private void generateContributionFactorReport(ReportHolder reportHolder) throws IOException , InterruptedException {
        ContributionReportGenerator contributionReportGenerator = new ContributionReportGenerator();
        contributionReportGenerator.generateAggregatedReport(reportHolder);
        List<ContributionEntry> contributionEntries = reportHolder.getContributionEntries();
        for(ContributionEntry contributionEntry : contributionEntries) {
            VALUE.set(ReportWriterUtil.serializeContributionEntry(contributionEntry));
            reportWriter.writeToContributionReport(VALUE);
        }
    }

    private void generateDMSA(ReportHolder reportHolder,String key) throws IOException,InterruptedException {
        DMSAReportGenerator reportGenerator = new DMSAReportGenerator();
        reportGenerator.generateAggregatedReport(reportHolder,key,reportWriter);
    }

    private ReportHolder generateDSA(Iterable<QuoteWritable> values) throws IOException , InterruptedException {
        Iterator<QuoteWritable> iterator = values.iterator();
        LPAHelper helper = new LPAHelper();
        while(iterator.hasNext()) {
            QuoteWritable quoteWritable = iterator.next();
            String provider = quoteWritable.getProvider();
            String stream = quoteWritable.getStream();
            String ccyPair = quoteWritable.getCurrencyPair();
            String quoteType = quoteWritable.getQuoteType();
            Quote quote = MRJobUtil.getQuote(quoteWritable);
            helper.addToReport(provider,stream,ccyPair, QuoteType.valueOf(quoteType),quote);
        }

        ReportHolder reportHolder = helper.getReportHolder();

        for(Report report : reportHolder.getSampleReports()) {
            for(ReportEntry reportEntry : report.getEntries()) {
                LPAUtility.setEntryStatus(reportEntry);
                VALUE.set(ReportWriterUtil.serializeReportEntry(reportEntry));
                reportWriter.writeToLPReport(VALUE);
            }
        }

        for(Report report : reportHolder.getPriceAtSizeReports()) {
            for(ReportEntry reportEntry : report.getEntries()) {
                LPAUtility.setEntryStatus(reportEntry);
                VALUE.set(ReportWriterUtil.serializeReportEntry(reportEntry));
                reportWriter.writeToLPReport(VALUE);
            }
        }
        return reportHolder;
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        reportWriter.close();
    }
}

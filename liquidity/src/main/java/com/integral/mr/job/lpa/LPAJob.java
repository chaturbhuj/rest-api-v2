package com.integral.mr.job.lpa;

import com.integral.mr.input.S3ConfigReaderInputFormatter;
import com.integral.mr.io.QuoteWritable;
import com.integral.mr.output.CSVFileOutputFormatter;
import com.integral.mr.util.MRJobUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Driver job for starting LPA MR job.
 *
 * @author Rahul Bhattacharjee
 */
public class LPAJob extends Configured implements Tool {

    @Override
    public int run(String[] arguments) throws Exception {

        String outputDir = arguments[0];

        Configuration conf = getConf();

        // prod
        conf.set(MRJobUtil.LPA_START_DATE_KEY,"08.11.2014 00:00:00.000");
        conf.set(MRJobUtil.LPA_END_DATE_KEY,"08.16.2014 00:00:00.000");
        conf.set(MRJobUtil.LPA_NO_OF_SAMPLES,"1000");
        conf.set(MRJobUtil.LPA_PROVIDER_STREAM_CONFIG_FETCHER_TYPE,"STATIC");

        // dev
        //conf.set(MRJobUtil.LPA_START_DATE_KEY,"08.11.2014 00:00:00.000");
        //conf.set(MRJobUtil.LPA_END_DATE_KEY,"08.12.2014 00:00:00.000");
        //conf.set(MRJobUtil.LPA_NO_OF_SAMPLES,"10");
        //conf.set(MRJobUtil.LPA_PROVIDER_STREAM_CONFIG_FETCHER_TYPE,"STATIC");

        //conf.set(MRJobUtil.LPA_END_DATE_KEY,"08.13.2014 00:00:00.000");

        // Prod configuration
        conf.set("mapred.child.java.opts","-Xmx2096m");
        conf.set("mapred.task.timeout","1200000");

        conf.set("fs.s3.awsAccessKeyId","AKIAI4JZNEOZDPEU2R6A");
        conf.set("fs.s3.awsSecretAccessKey","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");

        Job job = new Job(conf, "LPA job");

        job.setJarByClass(LPAJob.class);
        job.setMapperClass(LPAMapper.class);
        job.setReducerClass(LPAReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(QuoteWritable.class);

        job.setPartitionerClass(DMSAPartitioner.class);

        int numberOfReducers = MRJobUtil.numberOfReducers();
        job.setNumReduceTasks(numberOfReducers);

        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Text.class);

        job.setInputFormatClass(S3ConfigReaderInputFormatter.class);

        LazyOutputFormat.setOutputFormatClass(job,CSVFileOutputFormatter.class);
        FileOutputFormat.setOutputPath(job, new Path(outputDir));
        MultipleOutputs.addNamedOutput(job, "LPA", CSVFileOutputFormatter.class,NullWritable.class, Text.class);
        MultipleOutputs.addNamedOutput(job, "CONTRIBUTION", CSVFileOutputFormatter.class,NullWritable.class, Text.class);

        return job.waitForCompletion(true)? 0:1;
    }

    public static void main(String[] args) throws Exception {
        int result = ToolRunner.run(new Configuration(), new LPAJob(), args);
        System.exit(result);
    }
}

package com.integral.mr.job.dsa;

import com.integral.mr.io.ProviderStreamCurrencyPairWritable;
import com.integral.mr.io.QuoteWritable;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * Partitioner which partitions based on provider , stream , ccypair.
 *
 * @author Rahul Bhattacharjee
 */
public class LPADSAPartitioner extends Partitioner<ProviderStreamCurrencyPairWritable,QuoteWritable> {

    @Override
    public int getPartition(ProviderStreamCurrencyPairWritable providerStreamCurrencyPairWritable, QuoteWritable quoteWritable, int num) {
        return Math.abs(providerStreamCurrencyPairWritable.hashCode() % num);
    }
}

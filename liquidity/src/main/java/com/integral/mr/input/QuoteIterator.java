package com.integral.mr.input;

import com.integral.driver.TimeRange;
import com.integral.ds.emscope.quote.RatesQuoteService;
import com.integral.ds.emscope.quote.impl.RateQuoteServiceDenseSamples;
import com.integral.ds.emscope.rates.Quote;
import com.integral.mr.util.LPAConfig;

import java.util.Iterator;
import java.util.List;

/**
 * Iterator which given a seamless view of all the quotes from various time ranges.Avoids bringing
 * all the quote data to memory all at once.Brings quotes in chunks and brings the next chunk when
 * the already available quotes are exhausted.
 *
 * @author Rahul Bhattacharjee
 */
public class QuoteIterator implements Iterator<Quote> {

    private final RatesQuoteService QUOTE_SERVICE = new RateQuoteServiceDenseSamples();

    private LPAConfig config;
    private Quote quote;
    private Iterator<TimeRange> timeRanges;
    private Iterator<Quote> quoteIterator;

    public QuoteIterator(LPAConfig config, List<TimeRange> timeRanges) {
        this.config = config;
        this.timeRanges = timeRanges.iterator();
    }

    // TODO: The below implementation is flawed. Calling hasNext multiple times proceeds
    // the pointer ahead.It is wrong.
    @Override
    public boolean hasNext() {
        if(quoteIterator != null) {
            boolean hasValue = quoteIterator.hasNext();
            if(hasValue) {
                quote = quoteIterator.next();
                return true;
            } else {
                quoteIterator = null;
                return hasNext();
            }
        } else {
            boolean hasMoreTimeRange = timeRanges.hasNext();
            if(hasMoreTimeRange) {
                TimeRange timeRange = timeRanges.next();
                List<Quote> quotes = QUOTE_SERVICE.getSampledQuotes(config.getProvider(), config.getStream(),
                        config.getCcyPair(), timeRange.getStart(), timeRange.getEnd(), config.getSamples());
                quoteIterator = quotes.iterator();
                return hasNext();
            } else {
                return false;
            }
        }
    }

    @Override
    public Quote next() {
        return quote;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Remove method is not supported in this implementation of iterator.");
    }
}

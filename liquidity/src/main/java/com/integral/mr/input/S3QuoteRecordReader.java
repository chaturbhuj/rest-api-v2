 package com.integral.mr.input;

import com.integral.driver.TimeRange;
import com.integral.ds.emscope.quote.RatesQuoteService;
import com.integral.ds.emscope.quote.impl.RateQuoteServiceDenseSamples;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.mr.io.QuoteWritable;
import com.integral.mr.io.RateObjectWritable;
import com.integral.mr.util.LPAConfig;
import com.integral.mr.util.MRJobUtil;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.IOException;
import java.util.*;

 /**
  * S3 record reader for reading records from S3 rates file and emit RateObjectWritable.
  * @author Rahul Bhattacharjee
  */
public class S3QuoteRecordReader extends RecordReader<NullWritable,QuoteWritable> {

    private final NullWritable KEY = NullWritable.get();

    private Iterator<Quote> quoteIterator;
    private QuoteWritable value;
    private LPAConfig config;
    private int index = 0;
    private int samples = 0;

    @Override
    public void initialize(InputSplit inputSplit, TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {
        LPAConfigInputSplit split = (LPAConfigInputSplit) inputSplit;
        String serializedConfig = split.getSerializedLpaConfig();
        this.config = MRJobUtil.deserializeLPAConfig(serializedConfig);

        List<TimeRange> timeRanges = MRJobUtil.getTimeRangeList(config.getStartDate(),config.getEndDate());

        quoteIterator = new QuoteIterator(config,timeRanges);
        samples = (this.config.getSamples() * timeRanges.size());
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        boolean hasNext = quoteIterator.hasNext();
        if(hasNext) {
            value = getQuoteWritable(quoteIterator.next());
        }
        return hasNext;
    }

    @Override
    public NullWritable getCurrentKey() throws IOException, InterruptedException {
        return KEY;
    }

    @Override
    public QuoteWritable getCurrentValue() throws IOException, InterruptedException {
        index++;
        return value;
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
        try {
            return index/samples;
        } catch (Exception e) {
            return 1;
        }
    }

    @Override
    public void close() throws IOException {}

    private QuoteWritable getQuoteWritable(Quote quote) {
        long sampleTime = quote.getSampleTime().getTime();
        List<RestRatesObject> rates = quote.getRates();
        List<RateObjectWritable> ratesWritableList = new ArrayList<RateObjectWritable>();
        for(RestRatesObject rate : rates) {
            RateObjectWritable rateObjectWritable = new RateObjectWritable(config.getProvider(),config.getStream(),rate);
            ratesWritableList.add(rateObjectWritable);
        }
        return new QuoteWritable(config.getProvider(),config.getStream(),config.getCcyPair(),config.getQuoteType(),sampleTime,ratesWritableList);
    }
}

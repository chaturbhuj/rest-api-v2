package com.integral.mr.input;

import com.integral.mr.io.QuoteWritable;
import com.integral.mr.util.LPAConfig;
import com.integral.mr.util.MRJobUtil;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Custom input format for splitting LPA inputs into input splits.
 *
 * @author Rahul Bhattacharjee
 */
public class S3ConfigReaderInputFormatter extends InputFormat<NullWritable,QuoteWritable> {

    @Override
    public List<InputSplit> getSplits(JobContext jobContext) throws IOException, InterruptedException {
        List<LPAConfig> lpaConfigs = MRJobUtil.getLpaConfigList(jobContext.getConfiguration());

        List<InputSplit> result = new ArrayList<InputSplit>();
        for(LPAConfig config : lpaConfigs) {
            LPAConfigInputSplit split = new LPAConfigInputSplit(MRJobUtil.serializeLPAConfig(config));
            result.add(split);
        }
        return result;
    }

    @Override
    public RecordReader<NullWritable,QuoteWritable> createRecordReader(InputSplit inputSplit,
                                                                       TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {
        return new S3QuoteRecordReader();
    }
}

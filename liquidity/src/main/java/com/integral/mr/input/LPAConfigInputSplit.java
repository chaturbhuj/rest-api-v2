package com.integral.mr.input;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.InputSplit;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Custom split for reading S3 objects.
 *
 * @author Rahul Bhattacharjee
 */
public class LPAConfigInputSplit extends InputSplit implements Writable {

    public String serializedLpaConfig;

    public LPAConfigInputSplit() {}

    public LPAConfigInputSplit(String lpaConfig) {
        this.serializedLpaConfig = lpaConfig;
    }

    @Override
    public long getLength() throws IOException {
        return serializedLpaConfig.length();
    }

    @Override
    public String[] getLocations() throws IOException {
        return new String[] {};
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(serializedLpaConfig);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        serializedLpaConfig = dataInput.readUTF();
    }

    public String getSerializedLpaConfig() {
        return serializedLpaConfig;
    }
}

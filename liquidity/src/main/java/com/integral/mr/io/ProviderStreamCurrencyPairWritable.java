package com.integral.mr.io;

import com.google.common.collect.ComparisonChain;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Writable wrapper over ProviderStreamCurrencyPair.
 *
 * @author Rahul Bhattacharjee
 */
public class ProviderStreamCurrencyPairWritable extends ProviderStreamCurrencyPair implements WritableComparable<ProviderStreamCurrencyPairWritable> {

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(getProvider());
        dataOutput.writeUTF(getStream());
        dataOutput.writeUTF(getCcyPair());
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        setProvider(dataInput.readUTF());
        setStream(dataInput.readUTF());
        setCcyPair(dataInput.readUTF());
    }

    @Override
    public int compareTo(ProviderStreamCurrencyPairWritable object) {
        return ComparisonChain.start().compare(getProvider(),object.getProvider())
                                      .compare(getStream(),object.getStream())
                                      .compare(getCcyPair(),object.getCcyPair())
                                      .result();
    }

    public ProviderStreamCurrencyPair getPOJO() {
        return new ProviderStreamCurrencyPair(getProvider(),getStream(),getCcyPair());
    }
}

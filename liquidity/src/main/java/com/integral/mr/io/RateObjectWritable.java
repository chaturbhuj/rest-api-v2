package com.integral.mr.io;

import com.integral.ds.emscope.rates.RestRatesObject;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @author Rahul Bhattacharjee
 */
public class RateObjectWritable implements Writable {

    private String provider;
    private String stream;
    private long rateTime;
    private double bidPrice;
    private double offerPrice;
    private long bidVolume;
    private long offerVolume;
    private String status;
    private int tier;
    private String ccyPair;

    public RateObjectWritable() {}

    public RateObjectWritable(String provider , String stream ,RestRatesObject rate) {
        this.provider = provider;
        this.stream = stream;
        rateTime = rate.getTmstmp();
        bidPrice = rate.getBid_price().doubleValue();
        offerPrice = rate.getAsk_price().doubleValue();
        bidVolume = rate.getBid_size().longValue();
        offerVolume = rate.getAsk_size().longValue();
        status = rate.getStatus();
        tier = rate.getLvl().intValue();
        ccyPair = rate.getCcyPair();
    }

    public long getRateTime() {
        return rateTime;
    }

    public void setRateTime(long rateTime) {
        this.rateTime = rateTime;
    }

    public double getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(double bidPrice) {
        this.bidPrice = bidPrice;
    }

    public double getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(double offerPrice) {
        this.offerPrice = offerPrice;
    }

    public long getBidVolume() {
        return bidVolume;
    }

    public void setBidVolume(long bidVolume) {
        this.bidVolume = bidVolume;
    }

    public long getOfferVolume() {
        return offerVolume;
    }

    public void setOfferVolume(long offerVolume) {
        this.offerVolume = offerVolume;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTier() {
        return tier;
    }

    public void setTier(int tier) {
        this.tier = tier;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public void setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(provider);
        dataOutput.writeUTF(stream);
        dataOutput.writeLong(rateTime);
        dataOutput.writeDouble(bidPrice);
        dataOutput.writeDouble(offerPrice);
        dataOutput.writeLong(bidVolume);
        dataOutput.writeLong(offerVolume);
        dataOutput.writeUTF(status);
        dataOutput.writeInt(tier);
        dataOutput.writeUTF(ccyPair);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        provider = dataInput.readUTF();
        stream = dataInput.readUTF();
        rateTime = dataInput.readLong();
        bidPrice = dataInput.readDouble();
        offerPrice = dataInput.readDouble();
        bidVolume = dataInput.readLong();
        offerVolume = dataInput.readLong();
        status = dataInput.readUTF();
        tier = dataInput.readInt();
        ccyPair = dataInput.readUTF();
    }

    @Override
    public String toString() {
        return "RateObjectWritable{" +
                "provider='" + provider + '\'' +
                ", stream='" + stream + '\'' +
                ", rateTime=" + rateTime +
                ", bidPrice=" + bidPrice +
                ", offerPrice=" + offerPrice +
                ", bidVolume=" + bidVolume +
                ", offerVolume=" + offerVolume +
                ", status='" + status + '\'' +
                ", tier=" + tier +
                ", ccyPair='" + ccyPair + '\'' +
                '}';
    }
}

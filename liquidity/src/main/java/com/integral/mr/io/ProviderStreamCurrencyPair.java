package com.integral.mr.io;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Bean of provider , stream , currency pairs.
 *
 * @author Rahul Bhattacharjee
 */
public class ProviderStreamCurrencyPair{

    private String provider;
    private String stream;
    private String ccyPair;

    public ProviderStreamCurrencyPair() {}

    public ProviderStreamCurrencyPair(String provider, String stream, String ccyPair) {
        this.provider = provider;
        this.stream = stream;
        this.ccyPair = ccyPair;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public void setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProviderStreamCurrencyPair that = (ProviderStreamCurrencyPair) o;

        if (ccyPair != null ? !ccyPair.equals(that.ccyPair) : that.ccyPair != null) return false;
        if (provider != null ? !provider.equals(that.provider) : that.provider != null) return false;
        if (stream != null ? !stream.equals(that.stream) : that.stream != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = provider != null ? provider.hashCode() : 0;
        result = 31 * result + (stream != null ? stream.hashCode() : 0);
        result = 31 * result + (ccyPair != null ? ccyPair.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProviderStreamCurrencyPair{" +
                "provider='" + provider + '\'' +
                ", stream='" + stream + '\'' +
                ", ccyPair='" + ccyPair + '\'' +
                '}';
    }
}

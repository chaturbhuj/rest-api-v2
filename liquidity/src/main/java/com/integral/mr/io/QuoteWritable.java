package com.integral.mr.io;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Quote writable object.
 * @author Rahul Bhattacharjee
 */
public class QuoteWritable implements Writable {

    private String provider;
    private String stream;
    private String currencyPair;
    private String quoteType;
    private long sampleTime;
    private List<RateObjectWritable> rateObjectWritableList;

    public QuoteWritable() {}

    public QuoteWritable(String provider, String stream, String currencyPair, String quoteType, long sampleTime,
                         List<RateObjectWritable> rateObjectWritableList) {
        this.provider = provider;
        this.stream = stream;
        this.currencyPair = currencyPair;
        this.quoteType = quoteType;
        this.sampleTime = sampleTime;
        this.rateObjectWritableList = rateObjectWritableList;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public long getSampleTime() {
        return sampleTime;
    }

    public void setSampleTime(long sampleTime) {
        this.sampleTime = sampleTime;
    }

    public List<RateObjectWritable> getRateObjectWritableList() {
        return rateObjectWritableList;
    }

    public void setRateObjectWritableList(List<RateObjectWritable> rateObjectWritableList) {
        this.rateObjectWritableList = rateObjectWritableList;
    }

    public String getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
    }

    public String getQuoteType() {
        return quoteType;
    }

    public void setQuoteType(String quoteType) {
        this.quoteType = quoteType;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        int size = rateObjectWritableList.size();
        dataOutput.writeInt(size);
        dataOutput.writeLong(sampleTime);
        dataOutput.writeUTF(provider);
        dataOutput.writeUTF(stream);
        dataOutput.writeUTF(quoteType);
        dataOutput.writeUTF(currencyPair);
        for(RateObjectWritable rate : rateObjectWritableList) {
            rate.write(dataOutput);
        }
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        int size = dataInput.readInt();
        sampleTime = dataInput.readLong();
        provider = dataInput.readUTF();
        stream = dataInput.readUTF();
        quoteType = dataInput.readUTF();
        currencyPair = dataInput.readUTF();
        rateObjectWritableList = new ArrayList<RateObjectWritable>();
        for(int i = 0 ; i < size ; i++) {
            RateObjectWritable rate = new RateObjectWritable();
            rateObjectWritableList.add(rate);
            rate.readFields(dataInput);
        }
    }
}

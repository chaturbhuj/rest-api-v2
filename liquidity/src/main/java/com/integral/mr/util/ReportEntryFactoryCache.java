package com.integral.mr.util;

import com.integral.aggregate.AggregatedQuoteCalculator;
import com.integral.ds.dto.QuoteType;
import com.integral.reporter.ReportEntryFactory;
import com.integral.reporter.bean.ReportEntryType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Rahul Bhattacharjee
 */
public class ReportEntryFactoryCache {

    private Map<CacheKey,ReportEntryFactory> TICK_ENTRY_FACTORY = new HashMap<CacheKey,ReportEntryFactory>();
    private Map<CacheKey,ReportEntryFactory> AGGREGATED_ENTRY_FACTORY = new HashMap<CacheKey,ReportEntryFactory>();
    private Map<CacheKey,AggregatedQuoteCalculator> AGGREGATOR_QUOTE_CALCULATOR = new HashMap<CacheKey,AggregatedQuoteCalculator>();

    public ReportEntryFactory getReportEntryFactoryForTick(String provider, String stream, String ccyPair, QuoteType quoteType) {
        CacheKey key = getKey(provider,stream,ccyPair);
        ReportEntryFactory value = TICK_ENTRY_FACTORY.get(key);
        if(value == null) {
            value = new ReportEntryFactory(provider,stream,ccyPair, ReportEntryType.TICK_DATA,quoteType);
            TICK_ENTRY_FACTORY.put(key,value);
        }
        return value;
    }

    public ReportEntryFactory getReportEntryFactoryForAggregation(String provider, String stream, String ccyPair, QuoteType quoteType) {
        CacheKey key = getKey(provider,stream,ccyPair);
        ReportEntryFactory value = AGGREGATED_ENTRY_FACTORY.get(key);
        if(value == null) {
            value = new ReportEntryFactory(provider,stream,ccyPair, ReportEntryType.DIRECT_SINGLE_STREAM_AGGREGATION,quoteType);
            AGGREGATED_ENTRY_FACTORY.put(key,value);
        }
        return value;
    }

    public AggregatedQuoteCalculator getAggregatorQuoteCalculator(String provider, String stream) {
        CacheKey key = getKey(provider,stream,null);
        AggregatedQuoteCalculator value = AGGREGATOR_QUOTE_CALCULATOR.get(key);
        if(value == null) {
            value = new AggregatedQuoteCalculator(provider,stream);
            AGGREGATOR_QUOTE_CALCULATOR.put(key,value);
        }
        return value;
    }

    private CacheKey getKey(String provider, String stream, String ccyPair) {
        return new CacheKey(provider,stream,ccyPair);
    }

    private static class CacheKey {
        private String provider;
        private String stream;
        private String ccyPair;

        private CacheKey(String provider, String stream, String ccyPair) {
            this.provider = provider;
            this.stream = stream;
            this.ccyPair = ccyPair;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CacheKey cacheKey = (CacheKey) o;

            if (ccyPair != null ? !ccyPair.equals(cacheKey.ccyPair) : cacheKey.ccyPair != null) return false;
            if (provider != null ? !provider.equals(cacheKey.provider) : cacheKey.provider != null) return false;
            if (stream != null ? !stream.equals(cacheKey.stream) : cacheKey.stream != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = provider != null ? provider.hashCode() : 0;
            result = 31 * result + (stream != null ? stream.hashCode() : 0);
            result = 31 * result + (ccyPair != null ? ccyPair.hashCode() : 0);
            return result;
        }
    }
}

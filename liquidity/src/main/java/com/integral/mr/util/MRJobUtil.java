package com.integral.mr.util;

import com.integral.driver.TimeRange;
import com.integral.ds.commons.util.ProviderStreamMetaData;
import com.integral.ds.dao.ProviderStreamConfigFetcher;
import com.integral.ds.dao.s3.S3ReadAndParseUtility;
import com.integral.ds.dto.ProviderStream;
import com.integral.ds.dto.QuoteType;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.s3.RecordTransformer;
import com.integral.mr.io.QuoteWritable;
import com.integral.mr.io.RateObjectWritable;
import com.integral.util.ProviderStreamConfigFetcherFactory;
import com.integral.util.ResourceUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Rahul Bhattacharjee
 */
public class MRJobUtil {

    public static final Text SINGLE_LP = new Text("Single_LP");
    private static final String BUCKET = "integral-configuration";
    private static final String DELIMETER = ",";

    public static final String S3_LPA_CONFIG_KEY = "com.integral.lpa.config.key";

    public static final String LPA_START_DATE_KEY = "com.integral.lpa.start";
    public static final String LPA_END_DATE_KEY = "com.integral.lpa.end";
    public static final String LPA_NO_OF_SAMPLES = "com.integral.lpa.samples";
    public static final String LPA_PROVIDER_STREAM_CONFIG_FETCHER_TYPE = "com.integral.lap.provider.stream.fetcher.type";

    private static final RecordTransformer<LPAConfig> LPA_CONFIG_PARSER = new LPAConfigParser();

    public static List<LPAConfig> getLpaConfigList(Configuration configuration) {
        try {
            List<LPAConfig> configs = new ArrayList<LPAConfig>();
            String startTimestamp = configuration.get(LPA_START_DATE_KEY);
            String endTimestamp = configuration.get(LPA_END_DATE_KEY);
            String noOfSamples = getNumberOfSamples(configuration);
            ProviderStreamConfigFetcherFactory.FetcherType fetcherType = getFetcherType(configuration);

            SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
            Date start = dateFormat.parse(startTimestamp);

            Date end = dateFormat.parse(endTimestamp);
            int samples = Integer.parseInt(noOfSamples);

            List<String> ccyPairs = ResourceUtil.getCurrencyPairs();
            List<ProviderStream> providerStreams = getProviderStreamsFromSource(fetcherType);

            for(ProviderStream providerStream : providerStreams) {
                for(String ccyPair : ccyPairs) {
                    String provider = providerStream.getProvider();
                    String stream = providerStream.getStream();
                    LPAConfig config = new LPAConfig();
                    config.setProvider(provider);
                    config.setStream(stream);
                    config.setCcyPair(ccyPair);
                    config.setSamples(samples);
                    config.setStartDate(start);
                    config.setEndDate(end);

                    try {
                        config.setQuoteType(providerStream.getQuoteType().name());
                    } catch (Exception e) {
                        config.setQuoteType(QuoteType.MQ.name());
                    }
                    configs.add(config);
                }
            }
            return configs;
        } catch (Exception e) {
            throw new IllegalArgumentException("Exception while forming configuration." , e);
        }
    }

    private static ProviderStreamConfigFetcherFactory.FetcherType getFetcherType(Configuration configuration) {
        String fetcherType = configuration.get(LPA_PROVIDER_STREAM_CONFIG_FETCHER_TYPE);
        if(StringUtils.isNotBlank(fetcherType)) {
            return ProviderStreamConfigFetcherFactory.FetcherType.valueOf(fetcherType);
        }
        return ProviderStreamConfigFetcherFactory.FetcherType.STATIC;
    }

    private static List<ProviderStream> getProviderStreamsFromSource(ProviderStreamConfigFetcherFactory.FetcherType type) {
        ProviderStreamConfigFetcher configFetcher = ProviderStreamConfigFetcherFactory.getProviderStreamConfigFetcher(type);
        configFetcher.init(null);
        return configFetcher.getProviderStreams();
    }

    public static String getNumberOfSamples(Configuration configuration) {
        return configuration.get(LPA_NO_OF_SAMPLES);
    }

    public static String serializeLPAConfig(LPAConfig config) {
        String provider = config.getProvider();
        String stream = config.getStream();
        String ccyPair = config.getCcyPair();
        Date startDate = config.getStartDate();
        Date endDate = config.getEndDate();
        int noOfSamples = config.getSamples();
        String quoteType = config.getQuoteType();

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        List<String> splits = new ArrayList<String>();
        splits.add(provider);
        splits.add(stream);
        splits.add(ccyPair);
        splits.add(quoteType);
        splits.add(dateFormat.format(startDate));
        splits.add(dateFormat.format(endDate));
        splits.add(Integer.toString(noOfSamples));
        return StringUtils.join(splits, DELIMETER);
    }

    public static LPAConfig deserializeLPAConfig(String serializedLpaConfig) {
        if(StringUtils.isNotBlank(serializedLpaConfig)) {
            return LPA_CONFIG_PARSER.transform(serializedLpaConfig);
        }
        return null;
    }

    public static String getBucket() {
        return BUCKET;
    }

    public static String getLPAConfigKey(Configuration configuration) {
        return configuration.get(S3_LPA_CONFIG_KEY);
    }

    public static boolean isNotSingleLP(Text key) {
        if(SINGLE_LP.equals(key)) {
            return false;
        }
        return true;
    }

    public static int numberOfReducers() {
        Map<String,List<ProviderStream>> dmsaConfig = ResourceUtil.getAggregatedReportInputs();
        return (dmsaConfig.size() + 1);
    }

    public static List<TimeRange> getTimeRangeList(Date startDate, Date endDate) {
        List<TimeRange> result = new ArrayList<TimeRange>();

        Date start = startDate;
        Date end = addHour(start);

        while(end.getTime() <= endDate.getTime()) {
            TimeRange timeRange = new TimeRange();
            timeRange.setStart(start);
            timeRange.setEnd(end);
            result.add(timeRange);
            start = end;
            end = addHour(end);
        }

        TimeRange timeRange = new TimeRange();
        timeRange.setStart(start);
        timeRange.setEnd(end);

        return result;
    }

    public static Date addHour(Date start) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(start);
        calendar.add(Calendar.HOUR_OF_DAY,24);
        return calendar.getTime();
    }

    public static Quote getQuote(QuoteWritable quoteWritable) {
        long sampleTime = quoteWritable.getSampleTime();
        List<RateObjectWritable> ratesList = quoteWritable.getRateObjectWritableList();
        List<RestRatesObject> rates = new ArrayList<RestRatesObject>();
        for(RateObjectWritable rate : ratesList) {
            rates.add(getRestRateObject(rate));
        }
        Quote quote = new Quote();
        quote.setSampleTime(new Date(sampleTime));
        quote.addRates(rates);
        return quote;
    }

    public static RestRatesObject getRestRateObject(RateObjectWritable rate) {
        RestRatesObject result = new RestRatesObject();
        result.setAskPrice(new BigDecimal(rate.getOfferPrice()));
        result.setBidPrice(new BigDecimal(rate.getBidPrice()));
        result.setAskSize(new BigDecimal(rate.getOfferVolume()));
        result.setBidSize(new BigDecimal(rate.getBidVolume()));
        result.setCcyPair(rate.getCcyPair());
        result.setStatus(rate.getStatus());
        result.setTier(rate.getTier());
        result.setTmstmp(rate.getRateTime());
        return result;
    }

    public static String getEndDayString(String startDate) throws Exception {
        Date providedDate = dateFromInput(startDate);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(providedDate);
        calendar.add(Calendar.DATE,1);
        return dateStringFromDate(calendar.getTime());
    }

    public static String getStartDayString(String startDate) throws Exception {
        Date providedDate = dateFromInput(startDate);
        return dateStringFromDate(providedDate);
    }

    public static Date dateFromInput(String inputDateString) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y");
        return dateFormat.parse(inputDateString);
    }

    public static String dateStringFromDate(Date date) {
        SimpleDateFormat jobDateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        return jobDateFormat.format(date);
    }

    public static String getOutputDirectory(String outputDir, String startDate) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        Date date = dateFromInput(startDate);

        SimpleDateFormat yearFormatter = new SimpleDateFormat("y");
        SimpleDateFormat yearMonthFormatter = new SimpleDateFormat("MM-y");
        SimpleDateFormat dayFormatter = new SimpleDateFormat("MM-dd-y");

        if(!outputDir.endsWith("/")) {
            outputDir += "/";
        }

        stringBuilder.append(outputDir);
        stringBuilder.append(yearFormatter.format(date) +"/");
        stringBuilder.append(yearMonthFormatter.format(date)+"/");
        stringBuilder.append(dayFormatter.format(date)+"/");
        return stringBuilder.toString();
    }
}

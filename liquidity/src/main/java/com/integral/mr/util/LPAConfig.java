package com.integral.mr.util;

import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public class LPAConfig {

    private String provider;
    private String stream;
    private String ccyPair;
    private String quoteType;
    private Date startDate;
    private Date endDate;
    private int samples;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public void setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getSamples() {
        return samples;
    }

    public void setSamples(int samples) {
        this.samples = samples;
    }

    public String getQuoteType() {
        return quoteType;
    }

    public void setQuoteType(String quoteType) {
        this.quoteType = quoteType;
    }

    @Override
    public String toString() {
        return "LPAConfig{" +
                "provider='" + provider + '\'' +
                ", stream='" + stream + '\'' +
                ", ccyPair='" + ccyPair + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", samples=" + samples +
                '}';
    }
}

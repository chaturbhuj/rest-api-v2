package com.integral.mr.util;

import com.integral.ds.s3.RecordTransformer;
import org.apache.commons.lang.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public class LPAConfigParser implements RecordTransformer<LPAConfig> {

    @Override
    public LPAConfig transform(String record) {
        if(StringUtils.isNotBlank(record) && !record.startsWith("#")) {
            try {
                String [] splits = record.split(",");
                String provider = splits[0];
                String stream = splits[1];
                String ccyPair = splits[2];
                String quoteType = splits[3];
                String startDateString = splits[4];
                String endDateString = splits[5];
                String samples = splits[6];

                SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

                Date startDate = dateFormat.parse(startDateString);
                Date endDate = dateFormat.parse(endDateString);
                int noOfSamples = Integer.parseInt(samples);

                LPAConfig config = new LPAConfig();
                config.setProvider(provider);
                config.setStream(stream);
                config.setCcyPair(ccyPair);
                config.setStartDate(startDate);
                config.setEndDate(endDate);
                config.setSamples(noOfSamples);
                config.setQuoteType(quoteType);
                return config;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}

package com.integral.mr.util;

import com.integral.aggregate.AggregatedQuoteCalculator;
import com.integral.ds.dto.QuoteType;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.reporter.ReportEntryComparator;
import com.integral.reporter.ReportEntryFactory;
import com.integral.reporter.bean.*;
import com.integral.util.LPAUtility;
import com.integral.util.ReportWriterUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * LPAHelper class.
 *
 * @author Rahul Bhattacharjee
 */
public class LPAHelper {

    private final ReportEntryComparator REPORT_ENTRY_COMPARATOR = new ReportEntryComparator();
    private ReportHolder reportHolder = new ReportHolder();
    private ReportEntryFactoryCache factoryCache = new ReportEntryFactoryCache();

    public LPAHelper(){}

    public void addToReport(String provider,String stream,String ccyPair,QuoteType quoteType, Quote quote){
        ReportEntryFactory entryFactoryForTick = factoryCache.getReportEntryFactoryForTick(provider,stream,ccyPair,quoteType);

        ReportEntryFactory entryFactoryForSingleStreamAgg = factoryCache.getReportEntryFactoryForAggregation(provider,stream,ccyPair,quoteType);
        Report sampleReport = new Report().setProvider(provider).setStream(stream).setCcyPair(ccyPair);
        Report priceAtSize = new Report().setProvider(provider).setStream(stream).setCcyPair(ccyPair);

        AggregatedQuoteCalculator quoteCalculator = factoryCache.getAggregatorQuoteCalculator(provider,stream);

        List<ReportEntry> sampleEntries = addQuoteToReport(quote,entryFactoryForTick);
        ReportWriterUtil.addReportTypeToEntries(sampleEntries, ReportWriterUtil.SINGLE_LP_REPORT_IDENTIFIER);

        sampleReport.addEntry(sampleEntries);
        List<ReportEntry> aggRates = quoteCalculator.addWVAPEntryForQuote(quote,entryFactoryForSingleStreamAgg);

        ReportWriterUtil.addReportTypeToEntries(aggRates,ReportWriterUtil.SINGLE_LP_REPORT_IDENTIFIER);
        priceAtSize.addEntry(aggRates);

        reportHolder.addSampleReport(sampleReport);
        reportHolder.addPriceAtSizeReports(priceAtSize);
    }

    private List<ReportEntry> addQuoteToReport(Quote quote, ReportEntryFactory reportEntryFactory) {
        List<ReportEntry> entries = new ArrayList<ReportEntry>();
        for(RestRatesObject rateObject : quote.getRates()) {
            ReportEntry entry = reportEntryFactory.getNewEntry();
            BigDecimal bidPrice = rateObject.getBid_price();
            BigDecimal offerPrice = rateObject.getAsk_price();
            entry.setBid(bidPrice);
            entry.setOffer(offerPrice);
            entry.setTier(rateObject.getLvl());
            entry.setQuoteTime(new Date(rateObject.getTmstmp()));
            entry.setBidVolume(rateObject.getBid_size());
            entry.setOfferVolume(rateObject.getAsk_size());
            entry.setSampleDateTime(quote.getSampleTime());
            entry.setErrorType(LPAUtility.getErrorType(rateObject));

            if(offerPrice.floatValue() != 0.0f && bidPrice.floatValue() != 0.0f) {
                BigDecimal spread = offerPrice.subtract(bidPrice);
                entry.setSpread(spread.doubleValue());
            }
            entries.add(entry);
        }
        Collections.sort(entries, REPORT_ENTRY_COMPARATOR);
        return entries;
    }

    public ReportHolder getReportHolder() {
        return reportHolder;
    }
}

package com.integral.ts.task;

import com.integral.ds.emscope.profile.ProfileRecord;
import com.integral.ds.s3.S3ProfileReader;
import com.integral.ts.bean.FetcherConfig;
import com.integral.ts.bean.ProfileTuple;
import com.integral.ts.bean.TupleGroup;
import com.integral.ts.util.Transformer;
import com.integral.ts.util.impl.ProfileRecordTransformer;

import java.util.*;
import java.util.concurrent.Callable;

/**
 * Fetches profile for any given provider stream.
 *
 * @author Rahul Bhattacharjee
 */
public class ProfileFetcherCallable implements Callable<TupleGroup> {

    private FetcherConfig config;

    private static final Transformer<ProfileRecord,ProfileTuple> TRANSFORMER = new ProfileRecordTransformer();

    public ProfileFetcherCallable(FetcherConfig config) {
        this.config = config;
    }

    @Override
    public TupleGroup call() throws Exception {
        TupleGroup group = new TupleGroup(config);
        NavigableMap<Long,ProfileTuple> profileMap = getProfileMap();
        Date start = config.getStart();
        int samples = config.getNoOfSamples();
        long gapBetweenSamples = getGapBetweenSamples();

        long startTime = start.getTime();
        int count = 0;
        while(count < samples) {
            ProfileTuple tuple = getProfileTupleFor(startTime,profileMap);
            if(tuple != null){
                group.addTuple(tuple);
            }
            startTime += gapBetweenSamples;
            count++;
        }
        return group;
    }

    private ProfileTuple getProfileTupleFor(long startTime, NavigableMap<Long, ProfileTuple> profileMap) {
        Map.Entry<Long, ProfileTuple> tupleEntry = profileMap.floorEntry(startTime);
        if(tupleEntry == null) {
            return profileMap.firstEntry().getValue();
        }
        return tupleEntry.getValue();
    }

    private long getGapBetweenSamples() {
        Date start = config.getStart();
        Date end = config.getEnd();
        int samples = config.getNoOfSamples();
        return ((end.getTime()-start.getTime())/samples);
    }

    private NavigableMap<Long,ProfileTuple> getProfileMap() {
        S3ProfileReader profileReader = new S3ProfileReader();
        List<ProfileRecord> profiles = profileReader.getProfileRecords(config.getProvider(),config.getStream(),config.getCcyPair(),
                config.getStart(),config.getEnd());

        NavigableMap<Long,ProfileTuple> profileCache = new TreeMap<Long,ProfileTuple>();
        for(ProfileRecord profile : profiles) {
            long time = profile.getTime();
            ProfileTuple profileTuple = TRANSFORMER.transform(profile);
            profileCache.put(time,profileTuple);
        }
        return profileCache;
    }
}

package com.integral.ts.util;

/**
 * Transforms a object of one type to another.
 * @author Rahul Bhattacharjee
 */
public interface Transformer<I,O> {
    public O transform(I input);
}

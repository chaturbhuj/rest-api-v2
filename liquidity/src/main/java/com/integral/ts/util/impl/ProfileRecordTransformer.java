package com.integral.ts.util.impl;

import com.integral.ds.emscope.profile.ProfileRecord;
import com.integral.ts.bean.ProfileTuple;
import com.integral.ts.util.Transformer;

import java.util.Date;

/**
 * Transforms profile record to profile tuple.
 *
 * @author Rahul Bhattacharjee
 */
public class ProfileRecordTransformer implements Transformer<ProfileRecord,ProfileTuple> {

    @Override
    public ProfileTuple transform(ProfileRecord input) {
        ProfileTuple tuple = new ProfileTuple();
        tuple.setDateTime(new Date(input.getTime()));
        tuple.setMaxAskPrice(input.getMaxAskPrice());
        tuple.setMaxBidPrice(input.getMaxBidPrice());
        tuple.setMidPriceMean(input.getMidPriceMean());
        return tuple;
    }
}

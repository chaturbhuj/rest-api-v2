package com.integral.ts.util;

import com.integral.ds.dto.ProviderStream;
import com.integral.ts.TSConstants;
import com.integral.util.ResourceUtil;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * TS utility.
 *
 * @author Rahul Bhattacharjee
 */
public class TSUtil {

    private static final String TS_CONFIG_FILE_NAME = "TS.config";
    private static final Properties config = new Properties();

    public TSUtil() {}

    static {
        loadConfig();
    }

    private static void loadConfig() {
        InputStream inputStream = null;
        try {
            inputStream = ResourceUtil.getStream(TS_CONFIG_FILE_NAME);
            config.load(inputStream);
        }catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Check config.",e);
        } finally {
            if(inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static List<ProviderStream> getProviderStreamList() {
        List<ProviderStream> result = new ArrayList<ProviderStream>();
        String serializedProviderStream = config.getProperty(TSConstants.TS_PROVIDER_STREAM);
        if(StringUtils.isNotBlank(serializedProviderStream)) {
            String [] providers = serializedProviderStream.split(",");
            for(String providerStream : providers) {
                String [] splits = providerStream.split("-");
                String provider = splits[0];
                String stream = splits[1];
                ProviderStream config = new ProviderStream(provider,stream);
                result.add(config);
            }
        }
        return result;
    }

    public static int noOfSamples() {
        String samples = config.getProperty(TSConstants.TS_SAMPLES);
        return Integer.parseInt(samples);
    }

    public static Date getStartTime() {
        String startTimeString = config.getProperty(TSConstants.TS_START_TIME);
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        try {
            return dateFormat.parse(startTimeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date getEndTime() {
        String startTimeString = config.getProperty(TSConstants.TS_END_TIME);
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        try {
            return dateFormat.parse(startTimeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getOutputDirectory() {
        return config.getProperty(TSConstants.TS_OUTPUT_DIR);
    }

    public static String getCcyPair() {
        return config.getProperty(TSConstants.TS_CCYPAIR);
    }
}

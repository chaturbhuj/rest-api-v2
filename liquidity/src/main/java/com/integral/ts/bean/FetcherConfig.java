package com.integral.ts.bean;

import java.util.Date;

/**
 * Fetcher config.
 *
 * @author Rahul Bhattacharjee
 */
public class FetcherConfig {

    private String provider;
    private String stream;
    private String ccyPair;
    private int noOfSamples;
    private Date start;
    private Date end;

    public String getProvider() {
        return provider;
    }

    public FetcherConfig setProvider(String provider) {
        this.provider = provider;
        return this;
    }

    public String getStream() {
        return stream;
    }

    public FetcherConfig setStream(String stream) {
        this.stream = stream;
        return this;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public FetcherConfig setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
        return this;
    }

    public int getNoOfSamples() {
        return noOfSamples;
    }

    public FetcherConfig setNoOfSamples(int noOfSamples) {
        this.noOfSamples = noOfSamples;
        return this;
    }

    public Date getStart() {
        return start;
    }

    public FetcherConfig setStart(Date start) {
        this.start = start;
        return this;
    }

    public Date getEnd() {
        return end;
    }

    public FetcherConfig setEnd(Date end) {
        this.end = end;
        return this;
    }
}

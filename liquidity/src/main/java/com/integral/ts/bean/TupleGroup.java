package com.integral.ts.bean;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rahul Bhattacharjee
 */
public class TupleGroup {

    private FetcherConfig config;
    private List<ProfileTuple> profiles = new ArrayList<ProfileTuple>();

    public TupleGroup(FetcherConfig config){
        this.config = config;
    }

    public void addTuple(ProfileTuple tuple) {
        this.profiles.add(tuple);
    }

    public List<ProfileTuple> getTuples() {
        return this.profiles;
    }

    public String getIdentifier() {
        String provider = config.getProvider();
        String stream = config.getStream();
        return provider + "-" + stream;
    }

    public TupleGroup copy() {
        TupleGroup tupleGroup = new TupleGroup(this.config);
        return tupleGroup;
    }
}

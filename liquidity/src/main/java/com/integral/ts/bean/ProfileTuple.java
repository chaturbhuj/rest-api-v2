package com.integral.ts.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Profile bean.
 *
 * @author Rahul Bhattacharjee
 */
public class ProfileTuple {

    private Date dateTime;
    private BigDecimal midPriceMean;
    private BigDecimal maxBidPrice;
    private BigDecimal maxAskPrice;

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public BigDecimal getMidPriceMean() {
        return midPriceMean;
    }

    public void setMidPriceMean(BigDecimal midPriceMean) {
        this.midPriceMean = midPriceMean;
    }

    public BigDecimal getMaxBidPrice() {
        return maxBidPrice;
    }

    public void setMaxBidPrice(BigDecimal maxBidPrice) {
        this.maxBidPrice = maxBidPrice;
    }

    public BigDecimal getMaxAskPrice() {
        return maxAskPrice;
    }

    public void setMaxAskPrice(BigDecimal maxAskPrice) {
        this.maxAskPrice = maxAskPrice;
    }

    @Override
    public String toString() {
        return "ProfileTuple{" +
                "dateTime=" + dateTime +
                ", midPriceMean=" + midPriceMean +
                ", maxBidPrice=" + maxBidPrice +
                ", maxAskPrice=" + maxAskPrice +
                '}';
    }
}

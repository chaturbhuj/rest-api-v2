package com.integral.ts;

import com.integral.ds.dto.ProviderStream;
import com.integral.reporter.bean.*;

import java.util.*;

/**
 * Time searcher report generator.
 *
 * @author Rahul Bhattacharjee
 */
public class TSReportGenerator {

    private static final int TIER = 1;
    public static final String BID_ID = "bid";
    public static final String OFFER_ID = "offer";

    public List<TSReport> getReports(String dsmaKey,List<ProviderStream> providerStreams ,String ccyPair ,ReportHolder reportHolder) {
        List<TSReport> reports = new ArrayList<TSReport>();
        TSReport dsmaReport = getDSMAReport(reportHolder.getReportsForAggregatedStream(dsmaKey),ccyPair,dsmaKey);
        reports.add(dsmaReport);
        for(ProviderStream providerStream : providerStreams) {
            TSReport streamReport = getIndividualStream(reportHolder.getSampleReports(),providerStream,ccyPair);
            reports.add(streamReport);
        }
        return reports;
    }

    private TSReport getDSMAReport(List<Report> reportsForAggregated,String ccyPair,String dsmaKey) {
        TSReport result = new TSReport();
        result.setReportIdentifier(dsmaKey);

        for(Report report : reportsForAggregated) {
            for(ReportEntry entry : report.getEntries()) {
                String currentCcyPair = entry.getCcyPair();
                if(currentCcyPair.equalsIgnoreCase(ccyPair)) {
                    if(entry.getTier() == TIER) {
                        result.addEntry(getTSReportEntry(entry,dsmaKey));
                    }
                }
            }
        }
        return result;
    }

    private TSReport getIndividualStream(List<Report> sampleReports, ProviderStream providerStream, String ccyPair) {
        TSReport result = new TSReport();
        String identifier = providerStream.getProvider() + "-" + providerStream.getStream();
        result.setReportIdentifier(identifier);

        NavigableMap<Long,NavigableMap<Integer,List<ReportEntry>>> intermediateMap = new TreeMap<Long,NavigableMap<Integer,List<ReportEntry>>>();

        for(Report report : sampleReports) {
            if(!report.getCcyPair().equalsIgnoreCase(ccyPair)){
                continue;
            }

            List<ReportEntry> entries = report.getEntries();
            for(ReportEntry entry : entries) {
                long epoch = entry.getSampleDateTime().getTime();
                int tier = entry.getTier();
                if(tier != TIER) {
                    continue;
                }
                NavigableMap<Integer,List<ReportEntry>> tierMap = intermediateMap.get(epoch);
                if(tierMap == null) {
                    tierMap = new TreeMap<Integer,List<ReportEntry>>();
                    intermediateMap.put(epoch,tierMap);
                }
                List<ReportEntry> innerList = tierMap.get(tier);
                if(innerList == null) {
                    innerList = new ArrayList<ReportEntry>();
                    tierMap.put(tier,innerList);
                }
                innerList.add(entry);
            }
        }

        Iterator<Map.Entry<Long,NavigableMap<Integer,List<ReportEntry>>>> iterator = intermediateMap.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry<Long,NavigableMap<Integer,List<ReportEntry>>> entry = iterator.next();
            long epoch = entry.getKey();
            NavigableMap<Integer,List<ReportEntry>> value = entry.getValue();
            List<ReportEntry> entries = value.get(TIER);
            if(entries != null && !entries.isEmpty()) {
                result.addEntry(getTSReportEntry(entries.get(entries.size()-1),identifier));
            }
        }
        return result;
    }

    // Original method with bug.
    private TSReport getIndividualStream0(List<Report> sampleReports, ProviderStream providerStream, String ccyPair) {
        TSReport result = new TSReport();
        String identifier = providerStream.getProvider() + "-" + providerStream.getStream();
        result.setReportIdentifier(identifier);

        for(Report report : sampleReports) {
            String reportCcyPair = report.getCcyPair();
            if(reportCcyPair.equals(ccyPair)){
                List<ReportEntry> entries = report.getEntries();
                for(ReportEntry entry : entries) {
                    String provider = entry.getProvider();
                    String stream = entry.getStream();
                    ProviderStream currentProviderStream = new ProviderStream();
                    currentProviderStream.setProvider(provider);
                    currentProviderStream.setStream(stream);
                    if(currentProviderStream.equals(providerStream)) {
                        int tier = entry.getTier();
                        if(tier == TIER) {
                            result.addEntry(getTSReportEntry(entry, identifier));
                        }
                    }
                }
            }
        }
        return result;
    }

    private TSReportEntry getTSReportEntry(ReportEntry entry, String identifier) {
        TSReportEntry reportEntry = new TSReportEntry();
        reportEntry.setReportIdentifier(identifier);

        long epoch = entry.getSampleDateTime().getTime();
        double bid = entry.getBid().doubleValue();
        double offer = entry.getOffer().doubleValue();

        reportEntry.setEpochTime(epoch);
        reportEntry.addAttribute(BID_ID , bid + "");
        reportEntry.addAttribute(OFFER_ID , offer + "");
        return reportEntry;
    }
}

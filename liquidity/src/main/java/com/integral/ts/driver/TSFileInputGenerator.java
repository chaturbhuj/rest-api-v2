package com.integral.ts.driver;

import com.integral.ds.commons.util.Pair;
import com.integral.ds.dto.ProviderStream;
import com.integral.ts.bean.FetcherConfig;
import com.integral.ts.bean.ProfileTuple;
import com.integral.ts.bean.TupleGroup;
import com.integral.ts.task.ProfileFetcherCallable;
import com.integral.ts.util.TSUtil;

import java.util.*;
import java.util.concurrent.*;

/**
 * Driver class for generating TS input file (*.tqd).
 *
 * @author Rahul Bhattacharjee
 */
public class TSFileInputGenerator {

    private ExecutorService service;

    public TSFileInputGenerator() {
        this(Runtime.getRuntime().availableProcessors()+1);
    }
    public TSFileInputGenerator(int noOfCores) {
        service = Executors.newFixedThreadPool(noOfCores+1);
    }

    public void run(List<FetcherConfig> fetcherConfigList) {
        List<Future<TupleGroup>> futures = new ArrayList<Future<TupleGroup>>();

        for(FetcherConfig config : fetcherConfigList) {
            Future<TupleGroup> future = service.submit(getTaskForConfig(config));
            futures.add(future);
        }

        List<TupleGroup> tupleGroups = getTupleGroups(futures);
        List<TupleGroup> normalizedTupleGroups = normalizeTupleGroup(tupleGroups);
        service.shutdown();
    }
    private List<TupleGroup> getTupleGroups(List<Future<TupleGroup>> futures) {
        List<TupleGroup> tupleGroups = new ArrayList<TupleGroup>();
        for(Future<TupleGroup> future : futures) {
            try {
                tupleGroups.add(future.get());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return tupleGroups;
    }

    private Callable<TupleGroup> getTaskForConfig(FetcherConfig config) {
        return new ProfileFetcherCallable(config);
    }

    private List<TupleGroup> normalizeTupleGroup(List<TupleGroup> tupleGroups) {
        int count = tupleGroups.size();

        Map<Long,List<Pair<String,ProfileTuple>>> map = new TreeMap<Long,List<Pair<String,ProfileTuple>>>();

        for(TupleGroup tupleGroup : tupleGroups) {
            List<ProfileTuple> profileTuples = tupleGroup.getTuples();
            String groupIdentifier = tupleGroup.getIdentifier();
            for(ProfileTuple tuple : profileTuples) {

                System.out.print(tuple);

                long time = tuple.getDateTime().getTime();
                Pair<String,ProfileTuple> pair = new Pair<String,ProfileTuple>();
                pair.setFirst(groupIdentifier);
                pair.setSecond(tuple);

                List<Pair<String,ProfileTuple>> profileList = map.get(time);
                if(profileList == null) {
                    profileList = new ArrayList<Pair<String,ProfileTuple>>();
                    map.put(time,profileList);
                }
                profileList.add(pair);
            }
        }

        Map<String,TupleGroup> emptyProfileGroupMap = getEmptyProfileGroup(tupleGroups);

        Iterator<Long> iterator = map.keySet().iterator();
        while(iterator.hasNext()) {
            Long time = iterator.next();
            List<Pair<String,ProfileTuple>> pairs = map.get(time);
            if(pairs.size() == count) {
                for(Pair<String,ProfileTuple> pair : pairs) {
                    String key = pair.getFirst();
                    emptyProfileGroupMap.get(key).addTuple(pair.getSecond());
                }
            } else {
                System.out.println("ERROR ENCOUNTERED.");
            }
        }
        return getGroups(emptyProfileGroupMap);
    }

    private List<TupleGroup> getGroups(Map<String, TupleGroup> emptyProfileGroupMap) {
        List<TupleGroup> groups = new ArrayList<TupleGroup>();
        Iterator<TupleGroup> iterator = emptyProfileGroupMap.values().iterator();
        while(iterator.hasNext()) {
            groups.add(iterator.next());
        }
        return groups;
    }

    private Map<String, TupleGroup> getEmptyProfileGroup(List<TupleGroup> tupleGroups) {
        Map<String,TupleGroup> result = new HashMap<String,TupleGroup>();
        for(TupleGroup group : tupleGroups) {
            result.put(group.getIdentifier(),group.copy());
        }
        return result;
    }

    public static void main(String [] argv) {
        List<FetcherConfig> fetcherConfigs = new ArrayList<FetcherConfig>();

        Date start = TSUtil.getStartTime();
        Date end = TSUtil.getEndTime();
        int samples = TSUtil.noOfSamples();
        String ccyPair = TSUtil.getCcyPair();

        List<ProviderStream> providerStreams = TSUtil.getProviderStreamList();

        for(ProviderStream providerStream : providerStreams) {
            FetcherConfig config = new FetcherConfig();
            config.setProvider(providerStream.getProvider());
            config.setStream(providerStream.getStream());
            config.setStart(start);
            config.setEnd(end);
            config.setNoOfSamples(samples);
            config.setCcyPair(ccyPair);
            fetcherConfigs.add(config);
        }

        TSFileInputGenerator generator = new TSFileInputGenerator();
        generator.run(fetcherConfigs);
    }
}

package com.integral.ts;

/**
 * TS constants.
 *
 * @author Rahul Bhattacharjee
 */
public class TSConstants {

    public static final String TS_PROVIDER_STREAM = "ts.provider.stream";
    public static final String TS_SAMPLES = "ts.samples";
    public static final String TS_START_TIME = "ts.start";
    public static final String TS_END_TIME = "ts.end";
    public static final String TS_OUTPUT_DIR = "ts.output.dir";
    public static final String TS_CCYPAIR = "ts.ccypair";
}

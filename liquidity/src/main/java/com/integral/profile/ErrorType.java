package com.integral.profile;

/**
 * Error type enumeration.
 *
 * @author Rahul Bhattacharjee
 */
public enum ErrorType {

    NONE("No errors"),RATE_OUT_OF_WINDOW("Prevailing rate found out of configured window."),INACTIVE_RATE("Inactive rate"),
    ONE_SIDED_RATE("One sided rate"),PARTIAL_MISSING_RATE("Aggregation is partial because of missing rate."),
    PARTIAL_OUT_OF_WINDOW_RATE("Aggregation is partial because of one or more rates were out of prevailing time window.");

    private String errorMeaning;

    ErrorType(String errorMeaning) {
        this.errorMeaning = errorMeaning;
    }

    public void setErrorMeaning(String errorMeaning) {
        this.errorMeaning = errorMeaning;
    }
}

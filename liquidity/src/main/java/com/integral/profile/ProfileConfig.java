package com.integral.profile;

import com.integral.ds.dto.QuoteType;

import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public class ProfileConfig {

    private String provider;
    private String stream;
    private String ccyPair;
    private long volume;
    private Date profileStartTime;
    private Date profileEndTime;
    private int samples;
    private QuoteType quoteType;

    public String getProvider() {
        return provider;
    }

    public ProfileConfig setProvider(String provider) {
        this.provider = provider;
        return this;
    }

    public String getStream() {
        return stream;
    }

    public ProfileConfig setStream(String stream) {
        this.stream = stream;
        return this;
    }

    public long getVolume() {
        return volume;
    }

    public ProfileConfig setVolume(long volume) {
        this.volume = volume;
        return this;
    }

    public Date getProfileStartTime() {
        return profileStartTime;
    }

    public ProfileConfig setProfileStartTime(Date profileStartTime) {
        this.profileStartTime = profileStartTime;
        return this;
    }

    public Date getProfileEndTime() {
        return profileEndTime;
    }

    public ProfileConfig setProfileEndTime(Date profileEndTime) {
        this.profileEndTime = profileEndTime;
        return this;
    }

    public int getSamples() {
        return samples;
    }

    public ProfileConfig setSamples(int samples) {
        this.samples = samples;
        return this;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public ProfileConfig setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
        return this;
    }

    public QuoteType getQuoteType() {
        return quoteType;
    }

    public ProfileConfig setQuoteType(QuoteType quoteType) {
        this.quoteType = quoteType;
        return this;
    }
}

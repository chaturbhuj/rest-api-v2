package com.integral.profile;

import com.integral.aggregate.AggregatedQuoteCalculator;
import com.integral.aggregate.ProfileQuoteCalculator;
import com.integral.ds.dto.QuoteType;
import com.integral.ds.emscope.quote.RatesQuoteService;
import com.integral.ds.emscope.quote.impl.RateQuoteServiceDenseSamples;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.reporter.ReportEntryComparator;
import com.integral.reporter.ReportEntryFactory;
import com.integral.reporter.bean.*;
import com.integral.util.LPAUtility;
import com.integral.util.ReportWriterUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class LiquidityProfileGenerator {

    private final RatesQuoteService QUOTE_SERVICE = new RateQuoteServiceDenseSamples();
    private final ReportEntryComparator REPORT_ENTRY_COMPARATOR = new ReportEntryComparator();

    public ReportPair generateLiquidityProfile(ProfileConfig config) {
        //1. get rates for the required duration of time.
        //2. calculate bid , offer.
        //3. generate and add report to reporter.
        String provider = config.getProvider();
        String stream = config.getStream();
        QuoteType quoteType = config.getQuoteType();
        String ccyPair = config.getCcyPair();
        Date startTime = config.getProfileStartTime();
        Date endTime = config.getProfileEndTime();
        int noOfSamples = config.getSamples();

        ReportEntryFactory entryFactoryForTick = new ReportEntryFactory(provider,stream,ccyPair, ReportEntryType.TICK_DATA,quoteType);
        ReportEntryFactory entryFactoryForSingleStreamAgg = new ReportEntryFactory(provider,stream,ccyPair, ReportEntryType.DIRECT_SINGLE_STREAM_AGGREGATION,quoteType);

        List<Quote> quotes = QUOTE_SERVICE.getSampledQuotes(provider, stream, ccyPair, startTime, endTime, noOfSamples);
        ReportPair reportPair = new ReportPair();

        Report sampleReport = new Report().setProvider(provider).setStream(stream)
                .setCcyPair(ccyPair).setStart(startTime)
                .setEnd(endTime).setNoOfSamples(noOfSamples);

        Report priceAtSize = new Report().setProvider(provider).setStream(stream)
                .setCcyPair(ccyPair).setStart(startTime)
                .setEnd(endTime).setNoOfSamples(noOfSamples);

        reportPair.setSampleReport(sampleReport);
        reportPair.setPriceAtSize(priceAtSize);

        AggregatedQuoteCalculator quoteCalculator = new AggregatedQuoteCalculator(provider,stream);
        ProfileQuoteCalculator profileQuoteCalculator = new ProfileQuoteCalculator();

        for(Quote quote : quotes) {
            List<ReportEntry> sampleEntries = addQuoteToReport(quote,entryFactoryForTick);
            LPAUtility.setEntriesStatus(sampleEntries);

            ReportWriterUtil.addReportTypeToEntries(sampleEntries,ReportWriterUtil.SINGLE_LP_REPORT_IDENTIFIER);
            sampleReport.addEntry(sampleEntries);

            List<ReportEntry> aggRates = quoteCalculator.addWVAPEntryForQuote(quote,entryFactoryForSingleStreamAgg);
            LPAUtility.setEntriesStatus(aggRates);

            ReportWriterUtil.addReportTypeToEntries(aggRates,ReportWriterUtil.SINGLE_LP_REPORT_IDENTIFIER);
            priceAtSize.addEntry(aggRates);

            //TODO: Defensive check while profile calculation gets stablized.
            ProfileEntry entry = profileQuoteCalculator.getProfileEntryFor(quote,provider,stream,ccyPair);
            if(entry != null) {
                reportPair.addProfileEntry(entry);
            }
        }
        return reportPair;
    }

    private List<ReportEntry> addQuoteToReport(Quote quote, ReportEntryFactory reportEntryFactory) {
        List<ReportEntry> entries = new ArrayList<ReportEntry>();
        for(RestRatesObject rateObject : quote.getRates()) {
            ReportEntry entry = reportEntryFactory.getNewEntry();
            BigDecimal bidPrice = rateObject.getBid_price();
            BigDecimal offerPrice = rateObject.getAsk_price();
            entry.setBid(bidPrice);
            entry.setOffer(offerPrice);
            entry.setTier(rateObject.getLvl());
            entry.setQuoteTime(new Date(rateObject.getTmstmp()));
            entry.setBidVolume(rateObject.getBid_size());
            entry.setOfferVolume(rateObject.getAsk_size());
            entry.setSampleDateTime(quote.getSampleTime());
            entry.setErrorType(LPAUtility.getErrorType(rateObject));
            if(offerPrice.floatValue() != 0.0f && bidPrice.floatValue() != 0.0f) {
                BigDecimal spread = offerPrice.subtract(bidPrice);
                entry.setSpread(spread.doubleValue());
            }
            entries.add(entry);
        }
        Collections.sort(entries,REPORT_ENTRY_COMPARATOR);
        return entries;
    }

}

package com.integral.serializer;

import com.integral.reporter.bean.SerializerConfig;
import com.integral.reporter.bean.TSReport;

import java.util.List;

/**
 * TQD file serializer.
 *
 * @author Rahul Bhattacharjee
 */
public interface TQDSerializer {

    /**
     * Serializer for serializing TS reports using configuration serializer config.
     *
     * @param config
     * @param reports
     */
    public void serializer(SerializerConfig config,List<TSReport> reports);

}

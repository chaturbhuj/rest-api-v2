package com.integral.serializer;

import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public interface FilenameGenerator {

    /**
     * Initialize with date.
     * @param date
     */
    public void init(Date date);

    /**
     * Returns the name of the sampled rate file.
     * @return
     */
    public String getSampleName();

    /**
     * Returns the name of the meta file for this run.
     * @return
     */
    public String getMetaName();

    /**
     * Returns the name of the aggregated quote file.
     * @return
     */
    public String getAggregatedName();

    /**
     * File name to store the book.
     * @return
     */
    public String getBookFileName();

    /**
     * contribution report filename.
     * @return
     */
    public String getContributionReportFileName();

    /**
     * Profile report filename.
     * @return
     */
    public String getProfileReportFileName();

}

package com.integral.serializer.impl;

import com.integral.ds.commons.util.Pair;
import com.integral.reporter.bean.SerializerConfig;
import com.integral.reporter.bean.TSReport;
import com.integral.reporter.bean.TSReportEntry;
import com.integral.serializer.TQDSerializer;
import org.apache.commons.lang.StringUtils;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Rahul Bhattacharjee
 */
public class TQDFileSerializerImpl implements TQDSerializer {

    @Override
    public void serializer(SerializerConfig config,List<TSReport> reports) {
        NavigableMap<Long,List<TSReportEntry>> timeAggregatedMap = getTimeAggregatedMap(reports);
        NavigableMap<Long,List<TSReportEntry>> normalizedAggregatedMap = normalize(timeAggregatedMap, reports.size());
        Content report = getContent(normalizedAggregatedMap,config,reports.size());
        writeReport(config,report);
    }

    private Content getContent(NavigableMap<Long, List<TSReportEntry>> normalizedAggregatedMap, SerializerConfig config, int noOfReports) {
        Content result = new Content();
        result.setFixedAttribute(config.getFixedAttribute());
        result.setDynamicAttribute(getDynamicContentString(config.getDynamicAttribute()));
        result.setSamples(normalizedAggregatedMap.size());
        result.setNumberOfReports(noOfReports);
        result.setTimePoints(getTimePoints(normalizedAggregatedMap));

        List<TSReport> reports = getReportsFromEntries(normalizedAggregatedMap);
        for(TSReport report : reports) {
            String serializedReport = report.serialize();
            result.addReportContents(serializedReport);
        }
        return result;
    }

    private List<TSReport> getReportsFromEntries(NavigableMap<Long, List<TSReportEntry>> normalizedAggregatedMap) {
        List<TSReport> result = new ArrayList<TSReport>();
        Map<String,List<TSReportEntry>> resultReportMap = new HashMap<String,List<TSReportEntry>>();
        Iterator<Long> iterator = normalizedAggregatedMap.keySet().iterator();

        while(iterator.hasNext()) {
            Long epochTime = iterator.next();
            List<TSReportEntry> reportEntries = normalizedAggregatedMap.get(epochTime);
            for(TSReportEntry reportEntry : reportEntries) {
                String reportIdentifier = reportEntry.getReportIdentifier();
                List<TSReportEntry> entries = resultReportMap.get(reportIdentifier);
                if(entries == null) {
                    entries = new ArrayList<TSReportEntry>();
                    resultReportMap.put(reportIdentifier,entries);
                }
                entries.add(reportEntry);
            }
        }

        Iterator<Map.Entry<String,List<TSReportEntry>>> entryIterator = resultReportMap.entrySet().iterator();
        while(entryIterator.hasNext()) {
            Map.Entry<String,List<TSReportEntry>> entry = entryIterator.next();
            String identifier = entry.getKey();
            List<TSReportEntry> reportEntries = entry.getValue();
            TSReport report = new TSReport();
            report.setReportIdentifier(identifier);
            report.setEntries(reportEntries);
            result.add(report);
        }
        return result;
    }

    private String getTimePoints(NavigableMap<Long, List<TSReportEntry>> normalizedAggregatedMap) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/y HH:mm:ss");
        List<String> timePoints = new ArrayList<String>();

        Iterator<Long> iterator = normalizedAggregatedMap.keySet().iterator();
        while(iterator.hasNext()) {
            Long epochTime = iterator.next();
            Date date = new Date(epochTime);
            timePoints.add(dateFormat.format(date));
        }
        return StringUtils.join(timePoints,",");
    }

    private String getDynamicContentString(List<Pair<String, String>> dynamicAttributes) {
        List<String> attributes = new ArrayList<String>();
        for(Pair<String,String> dynamicAttribute : dynamicAttributes) {
            String attribute = dynamicAttribute.getFirst();
            String type = dynamicAttribute.getSecond();
            attributes.add(attribute+","+type);
        }
        return StringUtils.join(attributes," ; ");
    }

    private NavigableMap<Long, List<TSReportEntry>> getTimeAggregatedMap0(List<TSReport> reports) {
        NavigableMap<Long,List<TSReportEntry>> result = new TreeMap<Long,List<TSReportEntry>>();
        for(TSReport report : reports) {
            List<TSReportEntry> entries = report.getEntries();
            for(TSReportEntry entry : entries) {
                long epochTime = entry.getEpochTime();
                List<TSReportEntry> reportEntryList = result.get(epochTime);
                if(reportEntryList == null) {
                    reportEntryList = new ArrayList<TSReportEntry>();
                    result.put(epochTime,reportEntryList);
                }
                reportEntryList.add(entry);
            }
        }
        return result;
    }

    private NavigableMap<Long, List<TSReportEntry>> getTimeAggregatedMap(List<TSReport> reports) {
        NavigableMap<Long,List<TSReportEntry>> result = new TreeMap<Long,List<TSReportEntry>>();
        for(TSReport report : reports) {
            List<TSReportEntry> entries = report.getEntries();
            for(TSReportEntry entry : entries) {
                long epochTime = entry.getEpochTime();
                List<TSReportEntry> reportEntryList = result.get(epochTime);
                if(reportEntryList == null) {
                    reportEntryList = new ArrayList<TSReportEntry>();
                    result.put(epochTime,reportEntryList);
                }
                reportEntryList.add(entry);
            }
        }
        return result;
    }

    private NavigableMap<Long, List<TSReportEntry>> normalize(NavigableMap<Long, List<TSReportEntry>> timeAggregatedMap, int size) {
        NavigableMap<Long,List<TSReportEntry>> result = new TreeMap<Long,List<TSReportEntry>>();
        Iterator<Long> iterator = timeAggregatedMap.keySet().iterator();
        while(iterator.hasNext()) {
            Long epoch = iterator.next();
            List<TSReportEntry> entries = timeAggregatedMap.get(epoch);
            if(entries.size() == size) {
                result.put(epoch,entries);
            }
        }
        return result;
    }

    private static class Content {
        private String fixedAttribute;
        private String dynamicAttribute;
        private int samples;
        private int numberOfReports;
        private String timePoints;
        private List<String> reportContents = new ArrayList<String>();

        private String getFixedAttribute() {
            return fixedAttribute;
        }

        private void setFixedAttribute(String fixedAttribute) {
            this.fixedAttribute = fixedAttribute;
        }

        private String getDynamicAttribute() {
            return dynamicAttribute;
        }

        private void setDynamicAttribute(String dynamicAttribute) {
            this.dynamicAttribute = dynamicAttribute;
        }

        private int getSamples() {
            return samples;
        }

        private void setSamples(int samples) {
            this.samples = samples;
        }

        private int getNumberOfReports() {
            return numberOfReports;
        }

        private void setNumberOfReports(int numberOfReports) {
            this.numberOfReports = numberOfReports;
        }

        private String getTimePoints() {
            return timePoints;
        }

        private void setTimePoints(String timePoints) {
            this.timePoints = timePoints;
        }

        private List<String> getReportContents() {
            return reportContents;
        }

        private void addReportContents(String reportContent) {
            this.reportContents.add(reportContent);
        }
    }

    private void writeReport(SerializerConfig config, Content report) {
        String fileName = config.getOutputFile();
        PrintWriter writer = null;

        try {
            writer = new PrintWriter(new FileWriter(fileName));
            writeWithNewline(writer,"cvx");
            writeWithNewline(writer,report.getFixedAttribute());
            writeWithNewline(writer,report.getDynamicAttribute());
            writeWithNewline(writer,report.getSamples());
            writeWithNewline(writer,report.getNumberOfReports());
            writeWithNewline(writer,report.getTimePoints());
            List<String> reportContents = report.getReportContents();
            for(String reportContent : reportContents) {
                writeWithNewline(writer,reportContent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Exception while generating input file for TS.",e);
        } finally {
            writer.close();
        }
    }

    private void writeWithNewline(PrintWriter writer, Object text) {
        String message = text.toString();
        message += "\n";
        writer.write(message);
    }
}

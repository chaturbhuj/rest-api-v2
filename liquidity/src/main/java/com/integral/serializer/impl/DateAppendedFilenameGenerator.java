package com.integral.serializer.impl;

import com.integral.serializer.FilenameGenerator;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public class DateAppendedFilenameGenerator implements FilenameGenerator {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyy-MM-dd");
    private String fileName;

    @Override
    public void init(Date date) {
        fileName = dateFormat.format(date);
    }

    @Override
    public String getSampleName() {
        return "Report-" + fileName + ".csv";
    }

    @Override
    public String getMetaName() {
        return "Report-" + fileName + ".meta";
    }

    @Override
    public String getAggregatedName() {
        return "Report-DSA-" + fileName + ".csv";
    }

    @Override
    public String getBookFileName() {
        return "Report-DSMA-" + fileName + ".csv";
    }

    @Override
    public String getContributionReportFileName() {
        return "Report-CONTRIB-" + fileName + ".csv";
    }

    @Override
    public String getProfileReportFileName() {
        return "Report-PROFILE-" + fileName + ".csv";
    }
}

package com.integral.serializer.impl;

import com.integral.Constants;
import com.integral.reporter.bean.Report;
import com.integral.reporter.bean.ReportHolder;
import com.integral.serializer.FilenameGenerator;
import com.integral.serializer.ReportSerializer;
import com.integral.util.ReportWriterUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Properties;

/**
 * @author Rahul Bhattacharjee
 */
public class CSVReportSerializer implements ReportSerializer {

    private FilenameGenerator filenameGenerator = new DateAppendedFilenameGenerator();
    private PrintWriter writer;
    private Properties config;
    private Date start;

    @Override
    public void init(Properties config,Date start) {
        this.config = config;
        this.start = start;
        createWriter();
    }

    @Override
    public void serialize(ReportHolder reportHolder) {
        addHeader(reportHolder);
    }

    private void addHeader(ReportHolder reportHolder) {
        writer.write(reportHolder.getReportHeading());
        addBlank();
        writer.write(reportHolder.getReportColumns());
    }

    private void addReport(Report report) {
        ReportWriterUtil.writeReportToWriter(report.getEntries(), writer);
    }

    private void addBlank() {
        writer.write("\n\n");
    }

    private void createWriter() {
        try {
            filenameGenerator.init(start);
            String directory = config.getProperty(Constants.REPORT_BASE_DIR);
            ReportWriterUtil.ensureDirectory(directory);
            String fileName = config.getProperty(Constants.REPORT_BASE_DIR) + File.separator + filenameGenerator.getSampleName();
            writer = new PrintWriter(new FileWriter(new File(fileName)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        if(writer != null){
            writer.close();
        }
    }

    public FilenameGenerator getFilenameGenerator() {
        return filenameGenerator;
    }
}

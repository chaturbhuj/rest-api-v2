package com.integral.serializer.impl;

import com.integral.Constants;
import com.integral.reporter.bean.ProfileEntry;
import com.integral.reporter.bean.Report;
import com.integral.reporter.bean.ReportEntry;
import com.integral.reporter.bean.ReportHolder;
import com.integral.serializer.FilenameGenerator;
import com.integral.serializer.ReportSerializer;
import com.integral.util.ReportWriterUtil;
import com.integral.util.ResourceUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.*;

/**
 * @author Rahul Bhattacharjee
 */
public class CSVMetaReportSerializer implements ReportSerializer {

    private FilenameGenerator filenameGenerator = new DateAppendedFilenameGenerator();
    private PrintWriter sampleWriter;
    private PrintWriter priceAtSizeWriter;
    private PrintWriter metaWriter;
    private PrintWriter aggregatedReportWriter;
    private PrintWriter contributionReportWriter;
    private PrintWriter profileReportWriter;
    private Properties config;
    private Date start;

    @Override
    public void init(Properties config,Date date) {
        this.config = config;
        this.start = date;
        createWriters();
    }

    @Override
    public void serialize(ReportHolder reportHolder) {
        addHeader(reportHolder);

        List<Report> sampleReports = reportHolder.getSampleReports();
        List<Report> priceAtSize = reportHolder.getPriceAtSizeReports();

        List<ReportEntry> sampleEntries = collectReportEntries(sampleReports);
        List<ReportEntry> priceAtSizeEntries = collectReportEntries(priceAtSize);

        if(isDebugReportsGenerationEnabled()) {
            ReportWriterUtil.writeReportToWriter(sampleEntries, sampleWriter);
            ReportWriterUtil.writeReportToWriter(priceAtSizeEntries, priceAtSizeWriter);
        }

        if(isContributionReportEnabled()) {
            try{
                ReportWriterUtil.writeContributionReportToWriter(reportHolder.getContributionEntries(),contributionReportWriter);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(isBookGenerationEnabled()) {
            ReportWriterUtil.writeReportToWriter(getAggregatedReportEntries(reportHolder), aggregatedReportWriter);
        }

        if(isProfileGenerationEnabled()) {
            ReportWriterUtil.writeProfileReportToWriter(reportHolder.getProfileEntries(),profileReportWriter);
        }
    }

    private List<ReportEntry> collectReportEntries(List<Report> reports) {
        List<ReportEntry> reportEntries = new ArrayList<ReportEntry>();
        for(Report report: reports) {
            reportEntries.addAll(report.getEntries());
        }
        return reportEntries;
    }

    private void addHeader(ReportHolder reportHolder) {
        if(isDebugReportsGenerationEnabled()) {
            metaWriter.write(reportHolder.getReportHeading());
            sampleWriter.write(reportHolder.getReportColumns());
            priceAtSizeWriter.write(reportHolder.getReportColumns());
        }

        if(isContributionReportEnabled()) {
            contributionReportWriter.write(reportHolder.getReportColumnsForContributionReport());
        }

        if(isBookGenerationEnabled()) {
            aggregatedReportWriter.write(reportHolder.getReportColumns());
        }

        if(isProfileGenerationEnabled()) {
            profileReportWriter.write(reportHolder.getProfileReportColumns());
        }
    }

    private void createWriters() {
        try {
            filenameGenerator.init(start);
            String directory = config.getProperty(Constants.REPORT_BASE_DIR);
            ReportWriterUtil.ensureDirectory(directory);

            if(isDebugReportsGenerationEnabled()) {
                String fileName = config.getProperty(Constants.REPORT_BASE_DIR) + File.separator + filenameGenerator.getSampleName();
                sampleWriter = new PrintWriter(new FileWriter(new File(fileName)));
                String metaFileName = config.getProperty(Constants.REPORT_BASE_DIR) + File.separator + filenameGenerator.getMetaName();
                metaWriter = new PrintWriter(new FileWriter(new File(metaFileName)));
                String derivedFileName = config.getProperty(Constants.REPORT_BASE_DIR) + File.separator + filenameGenerator.getAggregatedName();
                priceAtSizeWriter = new PrintWriter(new FileWriter(new File(derivedFileName)));
            }

            if(isContributionReportEnabled()) {
                String fileName = config.getProperty(Constants.REPORT_BASE_DIR) + File.separator + filenameGenerator.getContributionReportFileName();
                contributionReportWriter = new PrintWriter(new FileWriter(new File(fileName)));
            }

            if(isBookGenerationEnabled()) {
                String bookName = config.getProperty(Constants.REPORT_BASE_DIR) + File.separator + filenameGenerator.getBookFileName();
                aggregatedReportWriter = new PrintWriter(new FileWriter(new File(bookName)));
            }

            if(isProfileGenerationEnabled()) {
                String profileFilename = config.getProperty(Constants.REPORT_BASE_DIR) + File.separator + filenameGenerator.getProfileReportFileName();
                profileReportWriter = new PrintWriter(new FileWriter(new File(profileFilename)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        if(sampleWriter != null){
            sampleWriter.close();
        }
        if(metaWriter != null) {
            metaWriter.close();
        }
        if(priceAtSizeWriter != null) {
            priceAtSizeWriter.close();
        }
        if(aggregatedReportWriter != null) {
            aggregatedReportWriter.close();
        }
        if(contributionReportWriter != null) {
            contributionReportWriter.close();
        }
    }

    private List<ReportEntry> getAggregatedReportEntries(ReportHolder holder) {
        List<ReportEntry> entries = new ArrayList<ReportEntry>();
        Map<String,List<Report>> aggregatedReportMap = holder.getAggregatedReportMap();
        Iterator<Map.Entry<String,List<Report>>> iterator = aggregatedReportMap.entrySet().iterator();

        while(iterator.hasNext()) {
            Map.Entry<String,List<Report>> entry = iterator.next();
            String reportKey = entry.getKey();
            List<Report> reports = entry.getValue();
            for(Report report : reports) {
                entries.addAll(report.getEntries());
            }
        }
        return entries;
    }

    private boolean isBookGenerationEnabled() {
        return ResourceUtil.isDMSAEnabled();
    }

    private boolean isDebugReportsGenerationEnabled() {
        return ResourceUtil.isDebugReportsEnabled();
    }

    private boolean isContributionReportEnabled(){
        return ResourceUtil.isContributionReportEnabled();
    }

    private boolean isProfileGenerationEnabled() {
        return ResourceUtil.isProfileReportEnabled();
    }
}

package com.integral.serializer;

import com.integral.reporter.bean.ReportHolder;

import java.util.Date;
import java.util.Properties;

/**
 * @author Rahul Bhattacharjee
 */
public interface ReportSerializer {

    void init(Properties config,Date startDate);

    void serialize(ReportHolder reportHolder);

    void close();
}

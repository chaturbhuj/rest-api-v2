package com.integral.aggregate;

import com.google.common.base.Optional;
import com.integral.ds.commons.model.IRate;
import com.integral.ds.commons.model.VwapCalculationResult;
import com.integral.ds.dto.ProviderStream;
import com.integral.ds.dto.QuoteType;
import com.integral.profile.ErrorType;
import com.integral.reporter.ReportEntryFactory;
import com.integral.reporter.bean.Report;
import com.integral.reporter.bean.ReportEntry;
import com.integral.reporter.bean.ReportEntryType;
import com.integral.util.LPAUtility;
import com.integral.util.ResourceUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 * Aggregated price at size calculator.
 *
 * @author Rahul Bhattacharjee
 */
public class AggregatedBookCalculator {

    private static final String AGGREGATED_PROVIDER = "AGG_PROVIDER";
    private static final String AGGREGATED_STREAM = "AGG_STREAM";

    public Report getReportForAggregatedStreams(String reportName, List<Report> filteredReports, List<Long> sizes, String ccyPair) {
        Report result = new Report().setProvider(AGGREGATED_PROVIDER).setStream(AGGREGATED_STREAM)
                .setCcyPair(ccyPair);

        Map<Long,List<ReportEntry>> reportEntrySortedByTime = getEntriesSortedByTime(filteredReports);
        Iterator<Map.Entry<Long,List<ReportEntry>>> iterator = reportEntrySortedByTime.entrySet().iterator();

        while(iterator.hasNext()) {
            Map.Entry<Long,List<ReportEntry>> entry = iterator.next();
            ReportEntryFactory reportEntryFactory = new ReportEntryFactory(AGGREGATED_PROVIDER,AGGREGATED_STREAM,ccyPair,
                    ReportEntryType.DIRECT_MULTIPLE_STREAM_AGGREGATION, QuoteType.NA);

            List<ReportEntry> entries = entry.getValue();

            entries = filterInActiveRates(entries);


            ErrorType errorType = LPAUtility.getErrorType(entries);
            List<IRate> rates = getRatesFromEntry(entries, errorType);
            int tier = 1;
            for(Long size : sizes) {
                tier = addAggregatedVWAPToReport(reportName, entry.getKey(), rates,size,tier,result,reportEntryFactory,errorType);
            }
        }
        return result;
    }

    private List<ReportEntry> filterInActiveRates(List<ReportEntry> entries) {
        List<ReportEntry> result = new ArrayList<ReportEntry>();
        for(ReportEntry entry : entries) {
            if(LPAUtility.isInActive(entry)) {
                continue;
            }
            result.add(entry);
        }
        return result;
    }

    private int addAggregatedVWAPToReport(String reportName, Long sampleTime, List<IRate> rates, Long size, int tier, Report result, ReportEntryFactory reportEntryFactory, ErrorType errorType) {
        ReportEntry entry = reportEntryFactory.getNewEntry();
        entry.setErrorType(errorType);

        Date time = new Date(sampleTime);

        Optional<VwapCalculationResult> bidVwapResult = BidOfferCalculatorCommonsImpl.getVWAPBidAtVolume(rates,size);
        Optional<VwapCalculationResult> offerVwapResult = BidOfferCalculatorCommonsImpl.getVWAPOfferAtVolume(rates,size);

        BigDecimal bidPrice = BidOfferCalculatorCommonsImpl.getBidPrice(bidVwapResult);
        BigDecimal offerPrice = BidOfferCalculatorCommonsImpl.getOfferPrice(offerVwapResult);

        entry.setBid(bidPrice);
        entry.setOffer(offerPrice);
        entry.setQuoteTime(time);
        entry.setBidVolume(new BigDecimal(size));
        entry.setOfferVolume(new BigDecimal(size));
        entry.setAtVolume(new BigDecimal(size));
        entry.setQuoteTime(time);
        entry.setSampleDateTime(time);

        entry.setBidContribution(BidOfferCalculatorCommonsImpl.getBidContributionAsList(bidVwapResult));
        entry.setOfferContribution(BidOfferCalculatorCommonsImpl.getOfferContributionAsList(offerVwapResult));

        BigDecimal spread = null;

        if(offerPrice != null && bidPrice != null) {
            spread = offerPrice.subtract(bidPrice);
            entry.setSpread(spread.doubleValue());
            entry.setMaxAgeOfRate(getMaxAge(rates,sampleTime));
            entry.setMinAgeOfRate(getMinAge(rates,sampleTime));
        }
        if(bidPrice != null || offerPrice != null) {
            entry.setTier(tier++);
            setErrorStatus(entry,rates,reportName);
            result.addEntry(entry);
        }
        return tier;
    }

    private long getMinAge(List<IRate> rates, Long sampleTime) {
        long min = Long.MAX_VALUE;
        for(IRate rate : rates) {
            if(rate.getTmstmp() < min) {
                min = rate.getTmstmp();
            }
        }
        return Math.abs(sampleTime-min);
    }

    private long getMaxAge(List<IRate> rates, Long sampleTime) {
        long max = 0;
        for(IRate rate : rates) {
            if(rate.getTmstmp() > max) {
                max = rate.getTmstmp();
            }
        }
        return Math.abs(sampleTime - max);
    }

    private List<IRate> getRatesFromEntry(List<ReportEntry> value, ErrorType errorType) {
        List<IRate> rates = null;
        if(errorType == ErrorType.NONE) {
            rates = BidOfferCalculatorCommonsImpl.transformReportEntries(value);
        } else {
            rates = new ArrayList<IRate>();
            for(ReportEntry entry : value) {
                if(entry.getErrorType() == ErrorType.NONE) {
                    rates.add(BidOfferCalculatorCommonsImpl.transformReportEntry(entry));
                }
            }
        }
        return rates;
    }

    private Map<Long, List<ReportEntry>> getEntriesSortedByTime(List<Report> quoteReport) {
        Map<Long,List<ReportEntry>> entryMap = new TreeMap<Long,List<ReportEntry>>();
        List<ReportEntry> prevailingReportEntries = getPrevailingReportEntries(quoteReport);
        for(ReportEntry entry : prevailingReportEntries) {
            Long time = entry.getSampleDateTime().getTime();
            List<ReportEntry> entryForTime = entryMap.get(time);
            if(entryForTime == null) {
               entryForTime = new ArrayList<ReportEntry>();
               entryMap.put(time,entryForTime);
            }
            entryForTime.add(entry);
        }
        return entryMap;
    }

    private List<ReportEntry> getPrevailingReportEntries(List<Report> reports) {
        List<ReportEntry> result = new ArrayList<ReportEntry>();
        for(Report report : reports) {
            result.addAll(getPrevailingReportEntries(report));
        }
        return result;
    }

    private List<ReportEntry> getPrevailingReportEntries(Report report) {
        List<ReportEntry> result = new ArrayList<ReportEntry>();
        List<ReportEntry> entries = report.getEntries();
        Map<Long,NavigableMap<Long,List<ReportEntry>>> reportByTime = new TreeMap<Long,NavigableMap<Long,List<ReportEntry>>>();

        for(ReportEntry entry : entries) {
            Date sampleTime = entry.getSampleDateTime();
            Date quoteTime = entry.getQuoteDateTime();
            NavigableMap<Long,List<ReportEntry>> subMap = reportByTime.get(sampleTime.getTime());
            if(subMap == null) {
                subMap = new TreeMap<Long,List<ReportEntry>>();
                reportByTime.put(sampleTime.getTime(),subMap);
            }
            List<ReportEntry> subReportEntries = subMap.get(quoteTime.getTime());
            if(subReportEntries == null) {
                subReportEntries = new ArrayList<ReportEntry>();
                subMap.put(quoteTime.getTime(),subReportEntries);
            }
            subReportEntries.add(entry);
        }

        Iterator<Map.Entry<Long,NavigableMap<Long,List<ReportEntry>>>> iterator = reportByTime.entrySet().iterator();

        while(iterator.hasNext()) {
            Map.Entry<Long,NavigableMap<Long,List<ReportEntry>>> mapEntry = iterator.next();
            NavigableMap<Long,List<ReportEntry>> ratesMap = mapEntry.getValue();
            result.addAll(ratesMap.lastEntry().getValue());
        }
        return result;
    }

    private void setErrorStatus(ReportEntry entry, List<IRate> rates, String reportName) {
        if(entry.getErrorType() != ErrorType.NONE){
            return;
        }
        Map<String,List<ProviderStream>> aggregationConfig = ResourceUtil.getAggregatedReportInputs();
        List<ProviderStream> providerStreams = aggregationConfig.get(reportName);
        Set<ProviderStream> providerStreamSet = new HashSet<ProviderStream>(providerStreams);
        Set<ProviderStream> providerStreamSetFromRates = new HashSet<ProviderStream>();

        for(IRate rate : rates) {
            String provider = rate.getPrvdr();
            String stream = rate.getStream();
            providerStreamSetFromRates.add(new ProviderStream(provider,stream));
        }
        providerStreamSet.removeAll(providerStreamSetFromRates);
        if(providerStreamSet.size() > 0) {
            entry.setErrorType(ErrorType.PARTIAL_MISSING_RATE);
        }
    }
}






























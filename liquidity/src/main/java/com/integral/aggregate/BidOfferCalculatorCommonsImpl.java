package com.integral.aggregate;

import com.google.common.base.Optional;
import com.integral.ds.commons.aggregator.VwapCalculator;
import com.integral.ds.commons.model.*;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.reporter.bean.ProviderContribution;
import com.integral.reporter.bean.ReportEntry;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class BidOfferCalculatorCommonsImpl {

    public static Optional<VwapCalculationResult> getVWAPBidAtVolume(List<IRate> rates, long vol) {
        Side side = Side.BID;
        Tier tier = new Tier("NO_NAME",vol);
        Optional<VwapCalculationResult> result = VwapCalculator.calculateVwapPriceAtSizeForUnsortedRates(rates, side, tier);
        return result;
    }

    public static Optional<VwapCalculationResult> getVWAPOfferAtVolume(List<IRate> rates, long vol) {
        Side side = Side.OFFER;
        Tier tier = new Tier("NO_NAME",vol);
        Optional<VwapCalculationResult> result = VwapCalculator.calculateVwapPriceAtSizeForUnsortedRates(rates, side, tier);
        return result;
    }

    public static IRate transformToIRate(RestRatesObject rate , String provider , String stream) {
        Rate result = new Rate();
        result.setTmstmp(rate.getTmstmp());
        result.setCcyp(rate.getCcyPair());
        result.setStatus(rate.getStatus());
        result.setLevel(rate.getLvl());
        result.setBidPrice(rate.getBid_price().doubleValue());
        result.setBidSize(rate.getBid_size().longValue());
        result.setAskSize(rate.getAsk_size().longValue());
        result.setAskPrice(rate.getAsk_price().doubleValue());
        result.setGuid(rate.getGuid());
        result.setPrvdr(provider);
        result.setStrm(stream);
        return result;
    }

    public static List<IRate> transformToIRateList(List<RestRatesObject> rates , String provider ,String stream) {
        List<IRate> result = new ArrayList<IRate>();
        for(RestRatesObject rate : rates) {
            result.add(transformToIRate(rate,provider,stream));
        }
        return result;
    }

    public static List<IRate> transformToIRateList(Quote quote,String provider,String stream) {
        List<RestRatesObject> rates = quote.getRates();
        return transformToIRateList(rates,provider,stream);
    }

    public static List<IRate> transformReportEntries(List<ReportEntry> reportEntries) {
        List<IRate> result = new ArrayList<IRate>();
        for(ReportEntry entry : reportEntries) {
            result.add(transformReportEntry(entry));
        }
        return result;
    }

    public static IRate transformReportEntry(ReportEntry entry) {
        Rate result = new Rate();
        result.setTmstmp(entry.getQuoteDateTime().getTime());
        result.setCcyp(entry.getCcyPair());
        result.setStatus("Active");
        result.setLevel(entry.getTier());
        result.setBidPrice(entry.getBid().doubleValue());
        result.setBidSize(entry.getBidVolume().longValue());
        result.setAskSize(entry.getOfferVolume().longValue());
        result.setAskPrice(entry.getOffer().doubleValue());
        result.setPrvdr(entry.getProvider());
        result.setStrm(entry.getStream());
        return result;
    }

    public static List<ProviderContribution> getOfferContributionAsList(Optional<VwapCalculationResult> offerVwapResult) {
        List<ProviderContribution> result = new ArrayList<ProviderContribution>();
        if(offerVwapResult.isPresent()) {
            VwapCalculationResult innerResult = offerVwapResult.get();
            List<VwapCalculationResult.ProviderStreamDetails> providerStreamDetails = innerResult.getProviderStreamDetails();
            for(VwapCalculationResult.ProviderStreamDetails detail : providerStreamDetails) {
                String provider = detail.getProvider();
                String stream = detail.getStream();
                double price = detail.getPrice();
                double size = detail.getSize();
                double contribution = detail.contribution();
                ProviderContribution providerContribution = new ProviderContribution();
                providerContribution.setProvider(provider).setStream(stream).setContribution(contribution).setPrice(price).setSize(size);
                result.add(providerContribution);
            }
        }
        return result;
    }

    public static String getOfferContribution(Optional<VwapCalculationResult> offerVwapResult) {
        String result = "";
        if(offerVwapResult.isPresent()) {
            List<String> serValues = new ArrayList<String>();
            VwapCalculationResult innerResult = offerVwapResult.get();
            List<VwapCalculationResult.ProviderStreamDetails> providerStreamDetails = innerResult.getProviderStreamDetails();
            for(VwapCalculationResult.ProviderStreamDetails detail : providerStreamDetails) {
                String provider = detail.getProvider();
                String stream = detail.getStream();
                double contribution = detail.contribution();
                if(contribution != 0.0) {
                    serValues.add(provider+"-"+stream+"-"+contribution);
                }
            }
            result = StringUtils.join(serValues,"|");
        }
        return result;
    }

    public static String getBidContribution(Optional<VwapCalculationResult> bidVwapResult) {
        String result = "";
        if(bidVwapResult.isPresent()) {
            List<String> serValues = new ArrayList<String>();
            VwapCalculationResult innerResult = bidVwapResult.get();
            List<VwapCalculationResult.ProviderStreamDetails> providerStreamDetails = innerResult.getProviderStreamDetails();
            for(VwapCalculationResult.ProviderStreamDetails detail : providerStreamDetails) {
                String provider = detail.getProvider();
                String stream = detail.getStream();
                double contribution = detail.contribution();
                if(contribution != 0.0) {
                    serValues.add(provider+"-"+stream+"-"+contribution);
                }
            }
            result = StringUtils.join(serValues,"|");
        }
        return result;
    }

    public static List<ProviderContribution> getBidContributionAsList(Optional<VwapCalculationResult> bidVwapResult) {
        List<ProviderContribution> result = new ArrayList<ProviderContribution>();
        if(bidVwapResult.isPresent()) {
            VwapCalculationResult innerResult = bidVwapResult.get();
            List<VwapCalculationResult.ProviderStreamDetails> providerStreamDetails = innerResult.getProviderStreamDetails();
            for(VwapCalculationResult.ProviderStreamDetails detail : providerStreamDetails) {
                String provider = detail.getProvider();
                String stream = detail.getStream();
                double price = detail.getPrice();
                double size = detail.getSize();
                double contribution = detail.contribution();
                ProviderContribution providerContribution = new ProviderContribution();
                providerContribution.setProvider(provider).setStream(stream).setContribution(contribution).setPrice(price).setSize(size);
                result.add(providerContribution);
            }
        }
        return result;
    }

    public static BigDecimal getOfferPrice(Optional<VwapCalculationResult> offerVwapResult) {
        if(offerVwapResult.isPresent()) {
            return new BigDecimal(offerVwapResult.get().getVwapPrice());
        }
        return null;

    }

    public static BigDecimal getBidPrice(Optional<VwapCalculationResult> bidVwapResult) {
        if(bidVwapResult.isPresent()) {
            return new BigDecimal(bidVwapResult.get().getVwapPrice());
        }
        return null;
    }
}

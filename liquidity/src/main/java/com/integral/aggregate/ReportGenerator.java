package com.integral.aggregate;

import com.integral.reporter.bean.ReportHolder;

/**
 * @author Rahul Bhattacharjee
 */
public interface ReportGenerator {

    public void generateAggregatedReport(ReportHolder reportHolder);
}

package com.integral.aggregate;

import com.integral.profile.ErrorType;
import com.integral.reporter.bean.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * Generates contribution reports.
 * @author Rahul Bhattacharjee
 */
public class ContributionReportGenerator implements ReportGenerator {

    @Override
    public void generateAggregatedReport(ReportHolder reportHolder) {
        Map<String,List<Report>> dmsaMap = reportHolder.getAggregatedReportMap();
        Iterator<List<Report>> iterator = dmsaMap.values().iterator();
        while(iterator.hasNext()) {
            List<Report> reports = iterator.next();
            for(Report report : reports) {
                List<ReportEntry> entries = report.getEntries();
                for(ReportEntry entry : entries) {
                    addContributionReportEntry(entry, reportHolder);
                }
            }
        }
    }

    private void addContributionReportEntry(ReportEntry entry, ReportHolder reportHolder) {
        BigDecimal bid = entry.getBid();
        BigDecimal bidVolume = entry.getBidVolume();
        BigDecimal offer = entry.getOffer();
        BigDecimal offerVolume = entry.getOfferVolume();

        List<ProviderContribution> bidContributions = entry.getBidContribution();
        List<ProviderContribution> offerContributions = entry.getOfferContribution();

        ContributionEntry contributionEntry = getBaseContributionReport(entry);
        contributionEntry.setType(ReportEntryType.DIRECT_MULTIPLE_STREAM_AGGREGATION);


        if(bid != null) {
            contributionEntry.setBid(bid).setBidVolume(bidVolume).setBidContribution(BigDecimal.ONE);

        }
        if(offer != null) {
            contributionEntry.setOffer(offer).setOfferVolume(offerVolume).setOfferContribution(BigDecimal.ONE);
        }

        reportHolder.addContributionEntry(contributionEntry);
        Map<ProviderStream,ContributionEntry> contributionEntryMap = new HashMap<ProviderStream,ContributionEntry>();

        if(bid != null){
            for(ProviderContribution contribution : bidContributions) {
                String provider = contribution.getProvider();
                String stream = contribution.getStream();
                ProviderStream key = new ProviderStream(provider,stream);
                ContributionEntry contribEntry = getBaseContributionReport(entry);
                contribEntry.setProvider(provider);
                contribEntry.setStream(stream);
                contribEntry.setBidContribution(new BigDecimal(contribution.getContribution()));
                contribEntry.setBid(new BigDecimal(contribution.getPrice()));
                contribEntry.setBidVolume(new BigDecimal(contribution.getSize()));
                contribEntry.setType(ReportEntryType.CONTRIBUTION_DATA);
                contributionEntryMap.put(key,contribEntry);
            }
        }

        if(offer != null) {
            for(ProviderContribution contribution : offerContributions) {
                String provider = contribution.getProvider();
                String stream = contribution.getStream();
                ProviderStream key = new ProviderStream(provider,stream);
                ContributionEntry existingEntry = contributionEntryMap.get(key);
                if(existingEntry == null) {
                    existingEntry = getBaseContributionReport(entry);
                    existingEntry.setType(ReportEntryType.CONTRIBUTION_DATA);
                    existingEntry.setProvider(provider);
                    existingEntry.setStream(stream);
                    contributionEntryMap.put(key,existingEntry);
                }
                existingEntry.setOffer(new BigDecimal(contribution.getPrice())).setOfferVolume(new BigDecimal(contribution.getSize()));
                existingEntry.setOfferContribution(new BigDecimal(contribution.getContribution()));
            }
        }

        Iterator<ContributionEntry> contributionEntryIterator = contributionEntryMap.values().iterator();
        while(contributionEntryIterator.hasNext()) {
            reportHolder.addContributionEntry(contributionEntryIterator.next());
        }
    }

    private ContributionEntry getBaseContributionReport(ReportEntry entry) {
        String provider = entry.getProvider();
        String stream = entry.getStream();
        Date sampleTime = entry.getSampleDateTime();
        String ccyPair = entry.getCcyPair();
        String aggregationType = entry.getAggregationType();
        BigDecimal atVolume = entry.getAtVolume();
        ErrorType errorType = entry.getErrorType();

        ContributionEntry contributionEntry = new ContributionEntry();
        contributionEntry.setProvider(provider).setStream(stream).setSampleTime(sampleTime).setCcyPair(ccyPair)
                .setAggregationKey(aggregationType).setAtVolume(atVolume).setErrorType(errorType);
        return contributionEntry;
    }
}

package com.integral.aggregate;

import com.google.common.base.Optional;
import com.integral.ds.commons.model.IRate;
import com.integral.ds.commons.model.VwapCalculationResult;
import com.integral.ds.dto.QuoteType;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.util.BidOfferCalculator;
import com.integral.reporter.ReportEntryFactory;
import com.integral.reporter.bean.ReportEntry;
import com.integral.reporter.bean.ReportEntryType;
import com.integral.util.LPAUtility;
import com.integral.util.ResourceUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 * Calculator for calculating price at size for given size.
 *
 * @author Rahul Bhattacharjee
 */
public class AggregatedQuoteCalculator {

    private String provider;
    private String stream;

    public AggregatedQuoteCalculator(String provider, String stream) {
        this.provider = provider;
        this.stream = stream;
    }

    public List<ReportEntry> addWVAPEntryForQuote(Quote quote, ReportEntryFactory reportEntryFactory) {
        List<ReportEntry> result = new ArrayList<ReportEntry>();
        String provider = reportEntryFactory.getProvider();
        String stream = reportEntryFactory.getStream();
        String ccyPair = reportEntryFactory.getCcyPair();
        QuoteType quoteType = reportEntryFactory.getQuoteType();

        ReportEntryFactory factoryForPriceAtSizeTick = new ReportEntryFactory(provider,stream,ccyPair,
                                                                                ReportEntryType.PRICE_AT_SIZE_TICK,quoteType);

        ReportEntryFactory factoryForPriceAtSizeAggregatedTick = new ReportEntryFactory(provider,stream,ccyPair,
                ReportEntryType.DIRECT_SINGLE_STREAM_AGGREGATION,quoteType);

        List<RestRatesObject> ratesObjects = quote.getRates();
        Date sampleTime = quote.getSampleTime();

        // All rates grouped per millisecond.
        Map<Long,List<RestRatesObject>> timeAggregatedRateObject = new HashMap<Long,List<RestRatesObject>>();

        for(RestRatesObject rateObject : ratesObjects) {
            long time = rateObject.getTmstmp();
            List<RestRatesObject> rates = timeAggregatedRateObject.get(time);
            if(rates == null) {
                rates = new ArrayList<RestRatesObject>();
                timeAggregatedRateObject.put(time,rates);
            }
            rates.add(rateObject);
        }

        Map<Long,List<RestRatesObject>> priceAtSizeForRates = getPriceAtSizeForRatesInMillisecond(timeAggregatedRateObject);
        addPriceAtSizeForRates(priceAtSizeForRates,result,factoryForPriceAtSizeTick,sampleTime);

        List<RestRatesObject> aggregatedPriceForVolume = getAggregatedPriceAtSize(priceAtSizeForRates);
        addPriceAtSizeForAggregatedRates(aggregatedPriceForVolume,result,factoryForPriceAtSizeAggregatedTick,sampleTime);
        return result;
    }

    /**
     * Returns the price at size for the rate list for that millisecond.
     *
     * @param timeAggregatedRateObject
     * @return
     */
    private Map<Long, List<RestRatesObject>> getPriceAtSizeForRatesInMillisecond(Map<Long, List<RestRatesObject>> timeAggregatedRateObject) {
        Map<Long,List<RestRatesObject>> result = new TreeMap<Long,List<RestRatesObject>>();
        Iterator<Map.Entry<Long,List<RestRatesObject>>> iterator = timeAggregatedRateObject.entrySet().iterator();

        while(iterator.hasNext()) {
            Map.Entry<Long,List<RestRatesObject>> entry = iterator.next();
            Long millisecond = entry.getKey();
            List<RestRatesObject> ratesObjects = entry.getValue();

            List<RestRatesObject> priceAtSizeRates = getPriceAtSizeFromRates(ratesObjects);
            result.put(millisecond,priceAtSizeRates);
        }
        return result;
    }

    private List<RestRatesObject> getPriceAtSizeFromRates(List<RestRatesObject> ratesObjects) {
        List<RestRatesObject> result = new ArrayList<RestRatesObject>();
        List<IRate> rates = BidOfferCalculatorCommonsImpl.transformToIRateList(ratesObjects,provider,stream);
        long quoteTime = rates.get(ratesObjects.size()-1).getTmstmp();
        List<Long> volumes = ResourceUtil.getVolumeList();
        int tier = 1;
        for(Long volume : volumes) {
            RestRatesObject rate = new RestRatesObject();

            Optional<VwapCalculationResult> bidVwapResult = BidOfferCalculatorCommonsImpl.getVWAPBidAtVolume(rates,volume);
            Optional<VwapCalculationResult> offerVwapResult = BidOfferCalculatorCommonsImpl.getVWAPOfferAtVolume(rates,volume);

            BigDecimal bidPrice = BidOfferCalculatorCommonsImpl.getBidPrice(bidVwapResult);
            BigDecimal offerPrice = BidOfferCalculatorCommonsImpl.getOfferPrice(offerVwapResult);

            if(bidPrice != null) {
                rate.setBidPrice(bidPrice);
            }
            rate.setBidSize(new BigDecimal(volume));
            if(offerPrice != null){
                rate.setAskPrice(offerPrice);
            }
            rate.setAskSize(new BigDecimal(volume));
            rate.setTmstmp(quoteTime);
            if(bidPrice != null || offerPrice != null) {
                rate.setTier(tier++);
                result.add(rate);
            }
          }
        return result;
    }

    private List<RestRatesObject> getAggregatedPriceAtSize(Map<Long, List<RestRatesObject>> priceAtSizeForRates) {
        List<RestRatesObject> result = new ArrayList<RestRatesObject>();
        List<Long> volumes = ResourceUtil.getVolumeList();

        RestRatesObject sampleRate = getSampleRates(priceAtSizeForRates);

        int tier = 1;
        for(Long volume : volumes) {
            BigDecimal bid = bidAtSize(volume,priceAtSizeForRates);
            BigDecimal offer = offerAtSize(volume,priceAtSizeForRates);
            BigDecimal atVolume = new BigDecimal(volume);
            if(bid == null && offer == null) {
                continue;
            }
            RestRatesObject aggRate = new RestRatesObject();
            aggRate.setBidPrice(bid);
            aggRate.setBidSize(atVolume);
            aggRate.setAskPrice(offer);
            aggRate.setAskSize(atVolume);
            aggRate.setCcyPair(sampleRate.getCcyPair());
            aggRate.setTmstmp(sampleRate.getTmstmp());
            aggRate.setTier(tier++);
            result.add(aggRate);
        }
        return result;
    }

    private RestRatesObject getSampleRates(Map<Long, List<RestRatesObject>> priceAtSizeForRates) {
        RestRatesObject rate = null;
        Iterator<Map.Entry<Long,List<RestRatesObject>>> iterator = priceAtSizeForRates.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry<Long,List<RestRatesObject>> entry = iterator.next();
            List<RestRatesObject> rates = entry.getValue();
            if(rates.isEmpty())
                continue;
            rate = rates.get(0);
            break;
        }
        return rate;
    }

    private BigDecimal offerAtSize(Long volume, Map<Long, List<RestRatesObject>> priceAtSizeForRates) {
        BigDecimal totalOffer = new BigDecimal(0);
        int totalOfferCount = 0;
        BigDecimal volumeInBigDecimal = new BigDecimal(volume);

        Iterator<Map.Entry<Long,List<RestRatesObject>>> iterator = priceAtSizeForRates.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry<Long,List<RestRatesObject>> entry = iterator.next();
            List<RestRatesObject> rates = entry.getValue();
            for(RestRatesObject rate : rates) {
                BigDecimal offer = rate.getAsk_price();
                BigDecimal offerVolume = rate.getAsk_size();
                if(offer != null) {
                    if(offerVolume.equals(volumeInBigDecimal)){
                        totalOffer= totalOffer.add(offer);
                        totalOfferCount++;
                    }
                }
            }
        }
        if(!totalOffer.equals(BigDecimal.ZERO)) {
            return totalOffer.divide(new BigDecimal(totalOfferCount),5);
        }
        return null;
    }

    private BigDecimal bidAtSize(Long volume, Map<Long, List<RestRatesObject>> priceAtSizeForRates) {
        BigDecimal totalBid = new BigDecimal(0);
        BigDecimal volumeInBigDecimal = new BigDecimal(volume);
        int totalBidCount = 0;

        Iterator<Map.Entry<Long,List<RestRatesObject>>> iterator = priceAtSizeForRates.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry<Long,List<RestRatesObject>> entry = iterator.next();
            List<RestRatesObject> rates = entry.getValue();
            for(RestRatesObject rate : rates) {
                BigDecimal bid = rate.getBid_price();
                BigDecimal bigVolume = rate.getBid_size();
                if(bid != null) {
                    if(bigVolume.equals(volumeInBigDecimal)){
                        totalBid = totalBid.add(bid);
                        totalBidCount++;
                    }
                }
            }
        }
        if(!totalBid.equals(BigDecimal.ZERO)) {
            return totalBid.divide(new BigDecimal(totalBidCount),5);
        }
        return null;
    }

    private void addPriceAtSizeForRates(Map<Long, List<RestRatesObject>> priceAtSizeForRates, List<ReportEntry> result,
                                        ReportEntryFactory factoryForPriceAtSizeTick,Date sampleTime) {
        Iterator<List<RestRatesObject>> listIterator = priceAtSizeForRates.values().iterator();
        while(listIterator.hasNext()) {
            List<RestRatesObject> rates = listIterator.next();
            for(RestRatesObject rate : rates) {
                ReportEntry entry = factoryForPriceAtSizeTick.getNewEntry();
                entry.setQuoteTime(new Date(rate.getTmstmp()));
                entry.setSampleDateTime(sampleTime);
                entry.setBid(rate.getBid_price());
                entry.setBidVolume(rate.getBid_size());
                entry.setOffer(rate.getAsk_price());
                entry.setOfferVolume(rate.getAsk_size());
                entry.setTier(rate.getLvl());
                entry.setAtVolume(rate.getAsk_size());

                BigDecimal offerPrice = rate.getAsk_price();
                BigDecimal bidPrice = rate.getBid_price();

                if(offerPrice != null && bidPrice != null) {
                    BigDecimal spread = offerPrice.subtract(bidPrice);
                    entry.setSpread(spread.doubleValue());
                }
                result.add(entry);
            }
        }
    }

    private void addPriceAtSizeForAggregatedRates(List<RestRatesObject> aggregatedPriceForVolume,
                                                  List<ReportEntry> result, ReportEntryFactory factoryForPriceAtSizeAggregatedTick, Date sampleTime) {
        for(RestRatesObject rate : aggregatedPriceForVolume) {
            ReportEntry entry = factoryForPriceAtSizeAggregatedTick.getNewEntry();
            entry.setQuoteTime(new Date(rate.getTmstmp()));
            entry.setSampleDateTime(sampleTime);
            entry.setBid(rate.getBid_price());
            entry.setBidVolume(rate.getBid_size());
            entry.setOffer(rate.getAsk_price());
            entry.setOfferVolume(rate.getAsk_size());
            entry.setTier(rate.getLvl());
            entry.setAtVolume(rate.getAsk_size());

            BigDecimal offerPrice = rate.getAsk_price();
            BigDecimal bidPrice = rate.getBid_price();

            if(offerPrice != null && bidPrice != null) {
                BigDecimal spread = offerPrice.subtract(bidPrice);
                entry.setSpread(spread.doubleValue());
            }
            result.add(entry);
        }
    }
}

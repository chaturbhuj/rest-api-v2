package com.integral.aggregate;

import com.integral.ds.dto.ProviderStream;
import com.integral.reporter.ReportFilter;
import com.integral.reporter.bean.Report;
import com.integral.reporter.bean.ReportEntry;
import com.integral.reporter.bean.ReportHolder;
import com.integral.reporter.impl.ReportFilterImpl;
import com.integral.util.ReportWriterUtil;
import com.integral.util.ResourceUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Aggregated report generator.
 *
 * @author Rahul Bhattacharjee
 */
public class AggregatedReportGenerator implements ReportGenerator {

    private static AggregatedBookCalculator reportBookCalculator = new AggregatedBookCalculator();
    private static ReportFilter reportFilter = new ReportFilterImpl();

    @Override
    public void generateAggregatedReport(ReportHolder reportHolder) {
        Map<String,List<ProviderStream>> aggregatedConfig = ResourceUtil.getAggregatedReportInputs();
        List<String> ccyPairs = ResourceUtil.getCurrencyPairs();
        List<Long> volumes = ResourceUtil.getVolumeList();
        Iterator<Map.Entry<String,List<ProviderStream>>> iterator = aggregatedConfig.entrySet().iterator();
        List<Report> sampleReports = reportHolder.getSampleReports();

        while(iterator.hasNext()) {
            Map.Entry<String,List<ProviderStream>> entry = iterator.next();
            String reportName = entry.getKey();
            List<ProviderStream> providerStreams = entry.getValue();
            List<Report> aggReports = new ArrayList<Report>();

            for(String ccyPair : ccyPairs) {
                List<Report> filteredReports = reportFilter.filteredReports(sampleReports,providerStreams,ccyPair);
                Report report = reportBookCalculator.getReportForAggregatedStreams(reportName,filteredReports,volumes,ccyPair);
                ReportWriterUtil.addReportTypeToEntries(report,reportName);
                aggReports.add(report);
            }
            reportHolder.addAggregatedReportToMap(reportName,aggReports);
        }
    }
}

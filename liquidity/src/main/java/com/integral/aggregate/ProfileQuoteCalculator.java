package com.integral.aggregate;

import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.reporter.bean.ProfileEntry;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Calculates profile from a given quote.
 *
 * @author Rahul Bhattacharjee
 */
public class ProfileQuoteCalculator {

    /**
     *
     * @param quote
     * @return
     */
    public ProfileEntry getProfileEntryFor(Quote quote,String provider,String stream,String ccyPair) {
        try{
            return calculateProfile(quote,provider,stream,ccyPair);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception while generating profile.");
        }
        return null;
    }

    private ProfileEntry calculateProfile(Quote quote,String provider,String stream,String ccyPair){

        ProfileEntry result = new ProfileEntry();
        List<RestRatesObject> rates = getTopTierFromQuote(quote);

        List<RestRatesObject> activeRates = new ArrayList<RestRatesObject>();
        String lastTickStatus = "InActive";
        int inactiveTickCount = 0;
        int numberOfBadData = 0;
        double lastActiveBid = 0;
        double lastActiveOffer = 0;

        int numTicks = 0;
        int bidCount = 0;
        int askCount = 0;

        double maxBidPrice = 0;
        double minBidPrice = 0;
        double maxOfferPrice = 0;
        double minOfferPrice = 0;

        double bid_price_max = 0d;
        double bid_price_min = 0d;
        double ask_price_max = 0d;
        double ask_price_min = 0d;
        long maxTimestamp = 0;
        int total_num_ticks = 0;

        ArrayList<Double> midPrices = new ArrayList<Double>();
        double sumMidPrices = 0d;

        double sumSpreads = 0d;
        double spread_max = 0d;
        double spread_min = 0d;
        long timeStamp = 0;
        for(RestRatesObject rate : rates) {
            timeStamp = rate.getTmstmp();
            if (isInActiveTick(rate)) {
                inactiveTickCount++;
                lastTickStatus = "Inactive";
            } else {
                activeRates.add(rate);
                lastTickStatus = rate.getStatus();
            }
        }

        double lastPrevailingAsk = 0;
        double lastPrevailingBid = 0;

        long averageBidSize = 0;
        long averageOfferSize = 0;

        for(RestRatesObject rate : activeRates) {

            boolean escapeBadData = false;

            if (lastPrevailingAsk == 0d) {
                lastPrevailingAsk = getDoubleValue(rate.getAsk_price());
            } else {
                if ((getDoubleValue(rate.getAsk_price()) >= (2 * lastPrevailingAsk))
                        || (getDoubleValue(rate.getAsk_price()) <= (0.5 * lastPrevailingAsk))) {
                    numberOfBadData++;
                } else {
                    lastPrevailingAsk = getDoubleValue(rate.getAsk_price());
                }
            }

            if(escapeBadData) {
                if (lastPrevailingBid == 0d) {
                    lastPrevailingBid = getDoubleValue(rate.getBid_price());
                } else {
                    if ((getDoubleValue(rate.getBid_price()) >= (2 * lastPrevailingBid))
                            || (getDoubleValue(rate.getBid_price()) <= (0.5 * lastPrevailingBid))) {
                        numberOfBadData++;
                    } else {
                        lastPrevailingBid = getDoubleValue(rate.getBid_price());
                    }
                }
            }

            lastActiveBid = getDoubleValue(rate.getBid_price());
            lastActiveOffer = getDoubleValue(rate.getAsk_price());

            double currentBidPrice = getDoubleValue(rate.getBid_price());
            double currentAskPrice = getDoubleValue(rate.getAsk_price());

            double currentBidSize = getDoubleValue(rate.getBid_size());
            double currentAskSize = getDoubleValue(rate.getAsk_size());

            if(currentBidSize !=0) {
                averageBidSize += currentBidSize;
                bidCount++;
            }

            if(currentAskSize != 0) {
                averageOfferSize += currentAskSize;
                askCount++;
            }

            if (bid_price_max == 0d && bid_price_min == 0d) {
                bid_price_max = currentBidPrice;
                bid_price_min = currentBidPrice;
                ask_price_max = currentAskPrice;
                ask_price_min = currentAskPrice;
            } else {
                bid_price_max = Math.max(bid_price_max, currentBidPrice);
                bid_price_min = Math.min(bid_price_min, currentBidPrice);
                ask_price_max = Math.max(ask_price_max, currentAskPrice);
                ask_price_min = Math.min(ask_price_min, currentAskPrice);
            }

            total_num_ticks++;

            if (currentBidPrice == 0d || currentAskPrice == 0d) {
                continue;
            }
            numTicks++;
            long tmstmp = rate.getTmstmp();

            maxTimestamp = Math.max(maxTimestamp, tmstmp);

            double midprice = (currentBidPrice + currentAskPrice) / 2.0;
            midPrices.add(midprice);
            sumMidPrices += midprice;

            double spread = Math.abs(currentBidPrice - currentAskPrice);
            sumSpreads += spread;
            if (spread_min == 0d) {
                spread_min = spread;
                spread_max = spread;
            } else {
                spread_min = Math.min(spread, spread_min);
                spread_max = Math.max(spread, spread_max);
            }
        }

        double midprice_mean = sumMidPrices / numTicks;

        double spread_mean = sumSpreads / numTicks;

        double spread_range = (spread_max - spread_min);

        if(bidCount != 0) {
            averageBidSize = averageBidSize / bidCount;
        }
        if(askCount != 0) {
            averageOfferSize = averageOfferSize / askCount;
        }

        //numTicks is two sided tick count.

        result.setProvider(provider);
        result.setStream(stream);
        result.setCcyPair(ccyPair);
        result.setActiveTickCount(numTicks);
        result.setAverageAskSize(getBigDecimal(averageOfferSize));
        result.setAverageBidSize(getBigDecimal(averageBidSize));
        result.setInactiveTickCount(inactiveTickCount);
        result.setLastActiveAsk(getBigDecimal(lastActiveOffer));
        result.setLastActiveBid(getBigDecimal(lastActiveBid));
        result.setLastDeclaredStatus(lastTickStatus);
        result.setLastTickAge(Math.abs((quote.getSampleTime().getTime() - timeStamp)));
        result.setLastTickTimeStamp(timeStamp);
        result.setMaxAskPrice(getBigDecimal(ask_price_max));
        result.setMaxBidPrice(getBigDecimal(bid_price_max));
        result.setMidPriceMean(getBigDecimal(midprice_mean));
        result.setNumberOfBadRecords(numberOfBadData);
        result.setMinAskPrice(getBigDecimal(ask_price_min));
        result.setMinBidPrice(getBigDecimal(bid_price_min));
        result.setSpreadAverage(getBigDecimal(spread_mean));
        result.setTier(1);
        result.setSpreadRange(getBigDecimal(spread_range));
        result.setTwoSidedTickCount(total_num_ticks);
        result.setTime(quote.getSampleTime().getTime());
        return result;

    }

    private List<RestRatesObject> getTopTierFromQuote(Quote quote) {
        List<RestRatesObject> result = new ArrayList<RestRatesObject>();
        for(RestRatesObject rate : quote.getRates()) {
            int tier = rate.getLvl();
            if(tier == 1)
                result.add(rate);
        }
        return result;
    }

    private BigDecimal getBigDecimal(long value) {
        if(value == 0)
            return BigDecimal.ZERO;
        return new BigDecimal(value);
    }

    private BigDecimal getBigDecimal(double value) {
        if(value == 0 || Double.isInfinite(value) || Double.isNaN(value))
            return BigDecimal.ZERO;
        return new BigDecimal(value);
    }

    private boolean isInActiveTick(RestRatesObject rate) {
        if((getDoubleValue(rate.getAsk_price()) == 0 && getDoubleValue(rate.getBid_price()) == 0)
            || (getDoubleValue(rate.getAsk_size()) == 0 && getDoubleValue(rate.getBid_size()) == 0)
            ||  !("Active".equalsIgnoreCase(rate.getStatus()))) {
            return true;
        }
        return false;
    }

    private double getDoubleValue(BigDecimal value) {
        if(value == null) {
            return 0;
        } else {
            return value.doubleValue();
        }
    }
}

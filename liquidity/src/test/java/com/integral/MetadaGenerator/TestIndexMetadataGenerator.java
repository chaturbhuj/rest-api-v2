package com.integral.MetadaGenerator;

import org.junit.Test;

import java.io.*;

/**
 * @author Rahul Bhattacharjee
 */
public class TestIndexMetadataGenerator {

    //@Test
    public void testMetadataGenerator() throws Exception {
        String input = "C:\\Users\\bhattacharjeer\\Desktop\\provider_meta_generator\\new_Provider_list.txt";
        String output = "C:\\Users\\bhattacharjeer\\Desktop\\provider_meta_generator\\new_Provider_list_out.txt";

        File inFile = new File(input);
        File outFile = new File(output);

        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        BufferedWriter writer = new BufferedWriter(new FileWriter(outFile));

        String line = "";
        int counter = 1;

        try {
            while(line != null) {
                line = reader.readLine();
                writer.write(counter++ + "~" + line+"\n");
            }
        } finally {
            reader.close();
            writer.close();
        }
    }
}

package com.integral.serializer;

import com.integral.serializer.impl.DateAppendedFilenameGenerator;

import static junit.framework.Assert.assertTrue;

/**
 * @author Rahul Bhattacharjee
 */
public class TestDateAppendedFilenameGenerator {

    //@Test
    public void testFileName() {
        FilenameGenerator generator = new DateAppendedFilenameGenerator();
        String fileName = generator.getSampleName();
        assertTrue(fileName != null);
        assertTrue(fileName.contains("Report"));
    }
}

package com.integral.serializer;

import com.integral.reporter.bean.SerializerConfig;
import com.integral.reporter.bean.TSReport;
import com.integral.reporter.bean.TSReportEntry;
import com.integral.serializer.impl.TQDFileSerializerImpl;
import org.junit.Test;

import java.util.*;

/**
 * @author Rahul Bhattacharjee
 */
public class TestTQDFileSerializer {

    private static final Random random = new Random(new Object().hashCode());

    //@Test
    public void testTQDFileSerializer() throws Exception {
        String fileName = "C:\\Users\\bhattacharjeer\\Desktop\\tqd_output\\test.tqd";

        TQDSerializer serializer = new TQDFileSerializerImpl();

        SerializerConfig config = new SerializerConfig();
        config.setOutputFile(fileName);
        config.setFixedAttribute("Date/Time,String");
        config.addDynamicAttribute("MidPrice","Float");
        config.addDynamicAttribute("MaxAskPrice","Float");
        config.addDynamicAttribute("MaxBidPrice","Float");

        List<TSReport> reports = getTSReports();
        serializer.serializer(config,reports);
    }

    private TSReport getReport(String reportName, List<Date> samples) {
        String attr1 = "MidPrice";
        String attr2 = "MaxAskPrice";
        String attr3 = "MaxBidPrice";

        TSReport report = new TSReport();

        report.setReportIdentifier(reportName);
        for(Date sample : samples) {
            TSReportEntry reportEntry = new TSReportEntry();
            reportEntry.addAttribute(attr1, getRandomFloatValue());
            reportEntry.addAttribute(attr2, getRandomFloatValue());
            reportEntry.addAttribute(attr3, getRandomFloatValue());
            reportEntry.setEpochTime(sample.getTime());
            report.addEntry(reportEntry);
        }
        return report;
    }

    private List<TSReport> getTSReports() throws Exception {
        List<TSReport> reports = new ArrayList<TSReport>();
        List<Date> samples = getSamples(100);

        reports.add(getReport("First",samples));
        reports.add(getReport("Second",samples));
        reports.add(getReport("Third",samples));
        return reports;
    }

    private List<Date> getSamples(int number) {
        List<Date> samples = new ArrayList<Date>();
        long gap = 60 * 1000; // 1 Min.
        Date date = new Date();
        int count = 0 ;

        while(count < number) {
            samples.add(date);
            count++;
            date = new Date(date.getTime()+ gap);
        }
        return samples;
    }

    private String getRandomFloatValue() {
        return random.nextFloat()+"";
    }
}

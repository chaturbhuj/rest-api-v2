package com.integral.util;

import com.integral.ds.dao.ProviderStreamConfigFetcher;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;

/**
 * Tests the available provider stream fetcher from S3.
 *
 * @author Rahul Bhattacharjee
 */
public class TestAllAvailableProviderStreamConfigFetcherImpl {

    //@Test
    public void testFetchAllAvailableProviderStream() {
        ProviderStreamConfigFetcher providerStreamConfigFetcher = new AllAvailableProviderStreamConfigFetcherImpl();
        int totalStreams = providerStreamConfigFetcher.getProviderStreams().size();
        assertTrue(totalStreams>0);
    }
}

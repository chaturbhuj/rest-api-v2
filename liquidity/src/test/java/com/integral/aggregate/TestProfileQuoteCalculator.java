package com.integral.aggregate;

import com.integral.ds.emscope.quote.RatesQuoteService;
import com.integral.ds.emscope.quote.impl.RateQuoteServiceCompleteDownload;
import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.reporter.bean.ProfileEntry;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Tests the profile quote calculator.
 *
 * @author Rahul Bhattacharjee
 */
public class TestProfileQuoteCalculator {

    private RatesQuoteService quoteService = new RateQuoteServiceCompleteDownload();

    //@Test
    public void testProfileQuoteCalculatorSingleRate()throws Exception {
        String point = "01.23.2014 02:55:01.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        Date time = dateFormat.parse(point);

        String provider = "BOAN";
        String stream = "BandC";
        String ccypair = "AUDCAD";

        Quote quote = quoteService.getQuoteAtTime(provider,stream,ccypair,time);

        System.out.println("Quote time " + quote.getSampleTime());

        for(RestRatesObject rate : quote.getRates()) {
            if(rate.getLvl() == 1) {
                System.out.println(rate);
            }
        }

        ProfileQuoteCalculator profileQuoteCalculator = new ProfileQuoteCalculator();
        ProfileEntry entry = profileQuoteCalculator.getProfileEntryFor(quote,provider,stream,ccypair);

        System.out.println(entry);
    }
}

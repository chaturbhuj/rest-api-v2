package com.integral.tb.parser;

import org.junit.Test;

/**
 * @author Rahul Bhattacharjee
 */
public class TestSQLTQDQueryParser {

    //@Test
    public void testSQLTQDParser() {
        String queryString = "select bid,offer from \"/tmp/test\" where ( provider=prvdr1 and stream= str1 ) or (provider=prvdr2 and stream=str2)" +
                " or (provider=prvdr2 and stream=stem3)";

        SQLQueryParser parser = new SQLQueryParser();

        SQLTQDQuery query = parser.getSqlQuery(queryString);
        query.queryPlan();
    }

    //@Test
    public void testSQLTQDParserSingleWhere() {
        String queryString = "select bid,offer from \"/tmp/test\" where ( provider=prvdr1 and stream= str1 )";

        SQLQueryParser parser = new SQLQueryParser();

        SQLTQDQuery query = parser.getSqlQuery(queryString);
        query.queryPlan();
    }

    //@Test
    public void testSQLTQDParserFiveWhere() {
        String queryString = "select bid,offer from \"/tmp/test\" where ( provider=prvdr1 and stream= str1 ) or (provider=prvdr2 and stream=str2)" +
                " or (provider=prvdr2 and stream=stem3) or ( provider=prvdr1 and stream= str1 ) or (provider=prvdr2 and stream=str2)";

        SQLQueryParser parser = new SQLQueryParser();

        SQLTQDQuery query = parser.getSqlQuery(queryString);
        query.queryPlan();
    }
}

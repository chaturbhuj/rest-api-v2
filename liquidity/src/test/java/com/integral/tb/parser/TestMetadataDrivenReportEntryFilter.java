package com.integral.tb.parser;

import com.integral.reporter.bean.ReportEntry;
import com.integral.tb.filter.MetadataDrivenReportEntryFilter;
import org.junit.Test;

import java.util.*;

/**
 * Test runtime filtering of Report entries.
 * @author Rahul Bhattacharjee
 */
public class TestMetadataDrivenReportEntryFilter {

    @Test
    public void testDynamicReportEntryFilter() {

        MetadataDrivenReportEntryFilter dynamicFilters = new MetadataDrivenReportEntryFilter();

        Map<String,String> filter1 = new HashMap<String,String>();
        filter1.put("provider","ADSS");
        filter1.put("ccypair","USDAUD");
        filter1.put("tier","1");

        Map<String,String> filter2 = new HashMap<String,String>();
        filter2.put("provider","CITI");
        filter2.put("ccypair","USDAUD");
        filter2.put("tier","1");

        List<Map<String,String>> filters = new ArrayList<Map<String,String>>();
        filters.add(filter1);
        filters.add(filter2);

        ReportEntry reportEntry1 = new ReportEntry();
        reportEntry1.setProvider("ADSS");
        reportEntry1.setCcyPair("USDAUD");
        reportEntry1.setTier(1);

        ReportEntry reportEntry2 = new ReportEntry();
        reportEntry2.setProvider("CITI");
        reportEntry2.setCcyPair("USDAUD");
        reportEntry2.setTier(1);

        ReportEntry reportEntry3 = new ReportEntry();
        reportEntry3.setProvider("CITIP");
        reportEntry3.setCcyPair("USDAUG");
        reportEntry3.setTier(1);

        List<ReportEntry> entries = new ArrayList<ReportEntry>();
        entries.add(reportEntry1);
        entries.add(reportEntry2);
        entries.add(reportEntry3);

        List<ReportEntry> filteredEntries = dynamicFilters.filteredReportEntries(entries, filters);

        System.out.println(filteredEntries.size());
    }
}

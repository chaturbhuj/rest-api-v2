package com.integral.tb.parser;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Tests an instance of Sql Tqd query.
 *
 * @author Rahul Bhattacharjee
 */
public class TestSqlTdqQuery {

    //@Test
    public void testSqlTqdQuery() {
        SQLTQDQuery query = new SQLTQDQuery();
        List<String> projections = new ArrayList<String>();

        projections.add("bid");
        projections.add("offer");

        query.setProjections(projections);

        Map<String,String> filter = new HashMap<String,String>();
        filter.put("provider","BOAN");
        filter.put("stream","BandD");
        filter.put("tier","1");

        Map<String,String> filter1 = new HashMap<String,String>();
        filter1.put("provider","BOAN");
        filter1.put("stream","BandD");
        filter1.put("tier","1");

        query.addFilters(filter);
        query.addFilters(filter1);

        query.setLpaDirectory("/tmp/lpa/input");

        query.queryPlan();

    }
}

package com.integral.driver;

/**
 * Starts a local LPA reports generation.
 *
 * @author Rahul Bhattacharjee
 */
public class TestLpaDriver {

    public static void main(String [] args) throws Exception {
        String reportDir = "C:\\Users\\bhattacharjeer\\Desktop\\aggregated-book";

        String startTimestamp = "07.01.2014 00:00:00.000";
        String endTimestamp = "07.01.2014 00:01:00.000";
        int noOfSamples = 50;

        String [] arguments = new String[4];
        arguments[0] = startTimestamp;
        arguments[1] = endTimestamp;
        arguments[2] = noOfSamples+"";
        arguments[3] = reportDir;

        MultiThreadedLiquidityProfileDriver.main(arguments);
    }
}

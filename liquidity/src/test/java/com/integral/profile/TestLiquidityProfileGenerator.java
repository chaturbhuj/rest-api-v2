package com.integral.profile;

import com.integral.reporter.bean.Report;
import com.integral.reporter.bean.ReportPair;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public class TestLiquidityProfileGenerator {

    //@Test
    public void testLiquidityProfileGenerator() throws Exception {
        String startTimestamp = "02.12.2014 02:10:01.796";
        String endTimestamp = "02.12.2014 07:40:01.796";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");
        Date start = dateFormat.parse(startTimestamp);
        Date end = dateFormat.parse(endTimestamp);

        String provider = "BOAN";
        String stream = "BandC";
        String ccypair = "AUDUSD";

        ProfileConfig config = new ProfileConfig();
        config.setProvider(provider).setStream(stream)
              .setCcyPair(ccypair).setSamples(10).setProfileStartTime(start)
              .setProfileEndTime(end);

        LiquidityProfileGenerator profileGenerator = new LiquidityProfileGenerator();
        ReportPair reportPair = profileGenerator.generateLiquidityProfile(config);

        System.out.println("Report => " + reportPair);
    }
}

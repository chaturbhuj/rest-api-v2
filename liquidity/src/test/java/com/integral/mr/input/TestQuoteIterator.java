package com.integral.mr.input;

import com.integral.driver.TimeRange;
import com.integral.ds.emscope.rates.Quote;
import com.integral.mr.util.LPAConfig;
import com.integral.mr.util.MRJobUtil;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class TestQuoteIterator {

    //@Test
    public void testQuoteIterator() throws Exception {

        String start = "08.11.2014";
        String end = "08.13.2014";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y");

        Date startDate = dateFormat.parse(start);
        Date endDate = dateFormat.parse(end);

        List<TimeRange> list = MRJobUtil.getTimeRangeList(startDate,endDate);
        LPAConfig config = new LPAConfig();

        config.setCcyPair("EURUSD");
        config.setProvider("BOAN");
        config.setStream("BandC");
        config.setSamples(5);

        Iterator<Quote> quoteIterator = new QuoteIterator(config,list);
        while(quoteIterator.hasNext()) {
            System.out.println(quoteIterator.next());
        }
    }
}

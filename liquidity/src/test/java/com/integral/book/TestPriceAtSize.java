package com.integral.book;

import com.integral.ds.emscope.rates.Quote;
import com.integral.ds.emscope.rates.RatesObject;
import com.integral.ds.emscope.rates.RestRatesObject;
import com.integral.ds.s3.S3RatesReader;
import com.integral.ds.util.BidOfferCalculator;
import com.integral.ds.util.DataQueryUtils;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * This is not a unit test.Its a test to create a book at certain point of time and replay the activities for generation
 * price-at-size for that sample.
 *
 * @author Rahul Bhattacharjee
 */
public class TestPriceAtSize {

    private static BidOfferCalculator CALCULATOR = new BidOfferCalculator();

    //@Test
    public void testPriceAtSize() throws Exception {
        // 1. Fetch all rates for a duration of one second.
        // 2. Create a book using these rates.
        // 3. Calculate price at size from that book.
        // log all the related activities.

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        //05/14/2014,06:54:43.015

        String startTimeString = "05.14.2014 06:54:43.000";
        String endTimeString = "05.14.2014 06:54:43.999";
        String provider = "BOAN";
        String stream = "BandC";
        String ccyPair = "EURUSD";
        long volume = 1000000;

        Date start = dateFormat.parse(startTimeString);
        Date end = dateFormat.parse(endTimeString);

        Quote quote = getQuoteForAnEntireSecond(start,end,provider,stream,ccyPair);
        printQuote(quote);

        //===== BID CALCULATION =====//

        List<RestRatesObject> rates = quote.getRates();
        rates = CALCULATOR.getFilteredRates(rates, false);

        log("Sorted Bid's as follows");
        CALCULATOR.sortBids(rates);

        //==== PRINT BOOK =====//

        printBidSideOfBook(rates);
        printOfferSideOfBook(rates);

        log("Bid price at size " + volume + " is " + CALCULATOR.getVWAPBidAtVolume(quote,volume));
        log("Offer price at size " + volume + " is " + CALCULATOR.getVWAPOfferAtVolume(quote, volume));
    }

    private void printQuote(Quote quote) {
        log("Quote time => " + quote.getQuoteTime());
        for(RestRatesObject rate : quote.getRates()) {
            log("Offer " + rate.getAsk_price() + " Offer Volume " + rate.getAsk_size() +
                    " Bid " + rate.getBid_price() + " Bid Volume " + rate.getBid_size());
        }
    }

    private void printBidSideOfBook(List<RestRatesObject> rates) {
        log("Bid side of book.");
        for(RestRatesObject rate : rates) {
            log("Bid " + rate.getBid_price() + " size " + rate.getBid_size());
        }
    }

    private void printOfferSideOfBook(List<RestRatesObject> rates) {
        log("Offer side of book.");
        for(RestRatesObject rate : rates) {
            log("Bid " + rate.getBid_price() + " size " + rate.getBid_size());
        }
    }

    private Quote getQuoteForAnEntireSecond(Date start, Date end, String provider, String stream, String ccyPair) {
        Quote quote = new Quote();
        S3RatesReader ratesReader = new S3RatesReader();
        List<RatesObject> ratesObjectList = ratesReader.getRates(provider, stream, start, end, ccyPair);
        for(RatesObject rate : ratesObjectList) {
            quote.addRate(DataQueryUtils.getRestRateObject(rate));
        }
        return quote;
    }

    private static void log(String msg) {
        System.out.println(msg);
    }
}

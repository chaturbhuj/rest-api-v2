package com.integral.stream;

import com.integral.stream.bean.StreamSampleEntry;
import com.integral.stream.bean.StreamTqdConfig;
import com.integral.stream.task.StreamFetcherTask;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Rahul Bhattacharjee
 */
public class TestStreamFetcherTask {

    //@Test
    public void testStreamFetcherTask() throws Exception {
        String startTimestamp = "05.08.2014 12:39:00.000";
        String endTimestamp =   "05.08.2014 12:40:00.000";

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.y HH:mm:ss.SSS");

        Date startTime = dateFormat.parse(startTimestamp);
        Date endTime = dateFormat.parse(endTimestamp);

        String provider = "DB";
        String stream = "EU0308852";
        String ccypair = "EURUSD";

        StreamTqdConfig config = new StreamTqdConfig();
        config.setProvider(provider).setStream(stream).setCcyPair(ccypair).setStart(startTime).setEnd(endTime).setNoOfSamples(10);

        StreamFetcherTask task = new StreamFetcherTask(config);
        Map<String,List<StreamSampleEntry>> result = task.call();
        System.out.println(result);
    }
}

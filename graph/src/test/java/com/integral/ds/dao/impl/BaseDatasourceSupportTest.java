package com.integral.ds.dao.impl;

import com.integral.ds.util.DBUtils;
import org.apache.commons.dbcp.BasicDataSource;

import java.sql.SQLException;

/**
 * To be extended by DAO tests for connection,datasource.
 * @author Rahul Bhattacharjee
 */
public class BaseDatasourceSupportTest {

    protected BasicDataSource getDataSource() {
        return DBUtils.getDataSource();
    }

    protected void close(BasicDataSource dataSource) {
        DBUtils.close(dataSource);
    }
}

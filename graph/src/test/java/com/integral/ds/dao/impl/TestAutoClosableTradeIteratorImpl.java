package com.integral.ds.dao.impl;

import com.integral.ds.dao.AutoClosableTradeIterator;
import com.integral.ds.dao.TradesDao;
import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Rahul Bhattacharjee
 */
public class TestAutoClosableTradeIteratorImpl extends BaseDatasourceSupportTest {

    //@Test
    public void testTradeIterator() throws Exception{
        String startString = "2014-09-10 02:10:00";
        String endString =   "2014-09-10 03:15:00";

        SimpleDateFormat format = new SimpleDateFormat("y-MM-dd HH:mm:ss");

        Date start = format.parse(startString);
        Date end = format.parse(endString);

        System.out.println("Start " + start);
        System.out.println("End " + end);

        BasicDataSource dataSource = getDataSource();
        TradesDao tradesDao = new TradesDao(dataSource);
        try{
            try(AutoClosableTradeIterator iterator = tradesDao.getIterator(start,end)){
                int count = 0;
                while(iterator.hasNext()) {
                    count++;
                    System.out.println(iterator.next());
                }
                System.out.print("Total trades count " + count);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            super.close(dataSource);
        }
    }
}

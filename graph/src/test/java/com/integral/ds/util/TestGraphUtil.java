package com.integral.ds.util;

import com.integral.ds.bean.GraphEntity;
import com.integral.ds.bean.GraphRelation;
import com.integral.ds.core.EntityType;
import org.junit.Test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * @author Rahul Bhattacharjee
 */
public class TestGraphUtil {

    private static final String NODE_NAME = "Person";
    private static final String RELATION_NAME = "Knows";

    //@Test
    public void testGraphNode() {
        GraphEntity entity = new GraphEntity();
        entity.setEntityName(NODE_NAME);
        entity.setPrimaryEntityValue("Rahul");

        Map<String, String> attribute = new HashMap<String, String>();
        attribute.put("age", "30");
        attribute.put("location", "palo alto");
        entity.setAttributes(attribute);

        String serializedGraphNode = entity.serialize();

        GraphEntity deserializedEntity = (GraphEntity) GraphUtil.getGraphNodeFromLine(serializedGraphNode);

        assertEquals(deserializedEntity.getEntityType(), EntityType.NODE);
        assertEquals(deserializedEntity.getEntityName(), NODE_NAME);
        assertEquals(deserializedEntity.getPrimaryEntityValue(), "Rahul");
        assertEqualsMap(attribute, deserializedEntity.getAttributes());
    }

    //@Test
    public void testRelationshipNode() {
        GraphRelation relationshipNode = new GraphRelation();

        relationshipNode.setEntityName(RELATION_NAME);
        relationshipNode.setPrimaryEntityValue("Tom");
        relationshipNode.setSecondaryEntityValue("Jack");

        Map<String, String> attributeMap = new HashMap<String, String>();
        attributeMap.put("relationship", "brother");
        attributeMap.put("knows_since", "2000");

        relationshipNode.setAttributes(attributeMap);

        String serializedRelationship = relationshipNode.serialize();

        GraphRelation relationship = (GraphRelation) GraphUtil.getGraphNodeFromLine(serializedRelationship);

        assertEquals(relationship.getEntityType(), EntityType.RELATIONSHIP);
        assertEquals(relationship.getEntityName(), RELATION_NAME);
        assertEquals(relationship.getPrimaryEntityValue(), "Tom");
        assertEquals(relationship.getSecondaryEntityValue(), "Jack");
        assertEqualsMap(relationship.getAttributes(), attributeMap);
    }

    //@Test
    public void testGraphNodeWithNoAttributes() {
        GraphEntity entity = new GraphEntity();
        entity.setEntityName(NODE_NAME);
        entity.setPrimaryEntityValue("Rahul");


        String serializedGraphNode = entity.serialize();

        System.out.print("SER " + serializedGraphNode);

        GraphEntity deserializedEntity = (GraphEntity) GraphUtil.getGraphNodeFromLine(serializedGraphNode);

        assertEquals(deserializedEntity.getEntityType(), EntityType.NODE);
        assertEquals(deserializedEntity.getEntityName(), NODE_NAME);
        assertEquals(deserializedEntity.getPrimaryEntityValue(), "Rahul");
        assertEqualsMap(null, deserializedEntity.getAttributes());
    }

    //@Test
    public void testRelationshipNodeWithNoAttributes() {
        GraphRelation relationshipNode = new GraphRelation();

        relationshipNode.setEntityName(RELATION_NAME);
        relationshipNode.setPrimaryEntityValue("Tom");
        relationshipNode.setSecondaryEntityValue("Jack");

        String serializedRelationship = relationshipNode.serialize();

        GraphRelation relationship = (GraphRelation) GraphUtil.getGraphNodeFromLine(serializedRelationship);

        assertEquals(relationship.getEntityType(), EntityType.RELATIONSHIP);
        assertEquals(relationship.getEntityName(), RELATION_NAME);
        assertEquals(relationship.getPrimaryEntityValue(), "Tom");
        assertEquals(relationship.getSecondaryEntityValue(), "Jack");
        assertEqualsMap(relationship.getAttributes(), null);
    }

    /**
     * Asserting map contents.
     *
     * @param in  - Map
     * @param out - Map
     */
    private void assertEqualsMap(Map<?, ?> in, Map<?, ?> out) {
        if(in == null && out == null) {
            return;
        }
        if (in == null || out == null) {
            throw new AssertionError("empty collection passed as input.");
        }
        if (in.size() != out.size()) {
            throw new AssertionError("Unequal maps provided as input.");
        }

        Iterator firstIterator = in.entrySet().iterator();
        Iterator secondIterator = out.entrySet().iterator();

        while (firstIterator.hasNext() && secondIterator.hasNext()) {

            Map.Entry firstEntry = (Map.Entry) firstIterator.next();
            Map.Entry secondEntry = (Map.Entry) secondIterator.next();

            if (firstEntry.getKey().equals(secondEntry.getKey())) {
                if (!(firstEntry.getValue().equals(secondEntry.getValue()))) {
                    throw new AssertionError("Exception , value not same." + "value 1 " + firstEntry.getValue() + " value 2 " + secondEntry.getValue());
                }
            } else {
                throw new AssertionError("Exception , Key not same");
            }
        }
    }
}














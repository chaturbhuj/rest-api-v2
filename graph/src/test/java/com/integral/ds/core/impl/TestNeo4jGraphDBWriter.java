package com.integral.ds.core.impl;

import com.integral.ds.bean.GraphNode;
import com.integral.ds.core.GraphDBWriter;
import com.integral.ds.util.GraphUtil;
import org.junit.Test;

import java.util.List;

/**
 * @author Rahul Bhattacharjee
 */
public class TestNeo4jGraphDBWriter {

    private static final String DB_LOCATION = "C:\\Users\\bhattacharjeer\\Desktop\\NEO4J\\Neo4j-installation\\data\\emscope.db";
    private static final String NODES_CONFIG = "C:\\Users\\bhattacharjeer\\Desktop\\NEO4J\\test\\nodes.txt";
    private static final String RELATIONSHIP_CONFIG = "C:\\Users\\bhattacharjeer\\Desktop\\NEO4J\\test\\relationship.txt";

    //@Test
    public void testGraphWriter() {
        String dbLocation = DB_LOCATION;

        String nodesConfigLocation = NODES_CONFIG;
        String relationshipConfigLocation = RELATIONSHIP_CONFIG;

        GraphDBWriter dbWriter = new Neo4JGraphDBWriter();
        try {
            dbWriter.init(dbLocation,true);
            List<GraphNode> nodes = GraphUtil.parseNodes(nodesConfigLocation);
            writeToGraph(nodes, dbWriter);

            List<GraphNode> relationships = GraphUtil.parseNodes(relationshipConfigLocation);
            writeToGraph(relationships, dbWriter);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbWriter.close();
        }
    }

    private static void writeToGraph(List<GraphNode> nodes, GraphDBWriter dbWriter) {
        for (GraphNode node : nodes) {
            dbWriter.persistGraphEntity(node);
        }
    }
}

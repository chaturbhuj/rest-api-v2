package com.integral.ds.dao;

import com.integral.ds.bean.TradeObject;
import com.integral.ds.dao.impl.AutoClosableTradeIteratorImpl;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.*;
import java.util.Date;
import java.util.List;

/**
 * Its a stateful class.
 *
 * @author Rahul Bhattacharjee
 */
public class TradesDao {

    private static final Logger LOGGER = Logger.getLogger(TradesDao.class);

    private static final String TIME_BOUNDED_TRADE_FETCHER_QUERY = "SELECT ORDERID,TRADEID,ORG,COVEREDTRADEID,CPTYBORG FROM TRADE_MASTER WHERE EXECTIME > ? and EXECTIME < ?";

    private NamedParameterJdbcTemplate jdbcTemplate;
    private BasicDataSource dataSource;

    public TradesDao(BasicDataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public AutoClosableTradeIterator getIterator(Date start , Date end) {
        try{
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(TIME_BOUNDED_TRADE_FETCHER_QUERY);

            System.out.println(start.getTime());
            System.out.println(end.getTime());
            statement.setTimestamp(1,new Timestamp(start.getTime()));
            statement.setTimestamp(2,new Timestamp(end.getTime()));
            ResultSet resultSet = statement.executeQuery();
            return new AutoClosableTradeIteratorImpl(connection,statement,resultSet);
        }catch (Exception e) {
            LOGGER.error("Exception while initializing the TradesDao.",e);
            throw new IllegalArgumentException("Exception while initializing the TradesDao",e);
        }
    }
}

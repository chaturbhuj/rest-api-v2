package com.integral.ds.dao;

import com.integral.ds.bean.TradeObject;

import java.util.Iterator;

/**
 * @author Rahul Bhattacharjee
 */
public interface AutoClosableTradeIterator extends Iterator<TradeObject> , AutoCloseable {

}

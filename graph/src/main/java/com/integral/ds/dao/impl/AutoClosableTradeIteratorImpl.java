package com.integral.ds.dao.impl;

import com.integral.ds.bean.TradeObject;
import com.integral.ds.dao.AutoClosableTradeIterator;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


/**
 * This impl has flaws.
 *
 * @author Rahul Bhattacharjee
 */
public class AutoClosableTradeIteratorImpl implements AutoClosableTradeIterator {

    private static final Logger LOGGER = Logger.getLogger(AutoClosableTradeIteratorImpl.class);

    private Connection connection;
    private PreparedStatement statement;
    private ResultSet resultSet;
    private boolean hasNext;
    private TradeObject tradeObject;

    public AutoClosableTradeIteratorImpl(Connection connection, PreparedStatement statement, ResultSet resultSet) {
        this.connection = connection;
        this.statement = statement;
        this.resultSet = resultSet;
    }

    @Override
    public boolean hasNext() {
        try{
            hasNext = resultSet.next();
            if(hasNext) {
                String orderId = resultSet.getString(1);
                String tradeId = resultSet.getString(2);
                String org = resultSet.getString(3);
                String coverTradeId = resultSet.getString(4);
                String cptyBOrg = resultSet.getString(5);
                tradeObject = new TradeObject(orderId,tradeId,org,coverTradeId,cptyBOrg);
                return true;
            }
            return false;
        } catch (Exception e) {
            LOGGER.error("Exception while iterating through the result set.",e);
            throw new IllegalArgumentException("Exception while iterating through the result set.",e);
        }
    }

    @Override
    public TradeObject next() {
        return tradeObject;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    @Override
    public void close() {
        try {
            if(resultSet != null) {
                resultSet.close();
            }
            if(statement != null) {
                statement.close();
            }
            if(connection != null) {
                connection.close();
            }
        } catch (Exception e) {
            LOGGER.error("Exception while closing TradesDao.",e);
            throw new IllegalArgumentException("Exception while closing TradesDao.",e);
        }
    }
}

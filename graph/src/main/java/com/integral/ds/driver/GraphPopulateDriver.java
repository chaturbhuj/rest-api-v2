package com.integral.ds.driver;

import com.integral.ds.bean.GraphEntity;
import com.integral.ds.bean.GraphNode;
import com.integral.ds.bean.GraphRelation;
import com.integral.ds.bean.TradeObject;
import com.integral.ds.core.GraphDBWriter;
import com.integral.ds.core.impl.Neo4JGraphDBWriter;
import com.integral.ds.dao.AutoClosableTradeIterator;
import com.integral.ds.dao.TradesDao;
import com.integral.ds.util.DBUtils;
import com.integral.ds.util.GraphUtil;
import org.apache.commons.dbcp.BasicDataSource;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Driver class to create configuration and write to the Graph DB.
 *
 * @author Rahul Bhattacharjee
 */
public class GraphPopulateDriver {

    private static final String DB_LOCATION = "C:\\Users\\bhattacharjeer\\Desktop\\NEO4J\\Neo4j-installation\\data\\trades_orders.db";
    private static final String NODES_CONFIG = "C:\\Users\\bhattacharjeer\\Desktop\\NEO4J\\trades\\nodes.txt";
    private static final String RELATIONSHIP_CONFIG = "C:\\Users\\bhattacharjeer\\Desktop\\NEO4J\\trades\\relationship.txt";

    public static void main(String[] argv) throws Exception {
        createConfig();
        persistToGraph();
    }

    private static void createConfig() throws Exception {
        String startString = "2014-09-10 02:10:00";
        String endString =   "2014-09-10 02:15:00";

        SimpleDateFormat format = new SimpleDateFormat("y-MM-dd HH:mm:ss");

        List<GraphEntity> graphEntities = new ArrayList<GraphEntity>();
        List<GraphRelation> graphRelations = new ArrayList<GraphRelation>();

        Date start = format.parse(startString);
        Date end = format.parse(endString);

        System.out.println("Start " + start);
        System.out.println("End " + end);

        BasicDataSource dataSource = DBUtils.getDataSource();
        TradesDao tradesDao = new TradesDao(dataSource);
        try{
            try(AutoClosableTradeIterator iterator = tradesDao.getIterator(start,end)){
                int count = 0;
                while(iterator.hasNext()) {
                    count++;
                    TradeObject tradeObject = iterator.next();
                    GraphUtil.addNodesAndRelationships(tradeObject,graphEntities,graphRelations);
                }
                System.out.print("Total trades count " + count);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(dataSource);
        }

        System.out.println("Nodes " + graphEntities.size());
        System.out.println("Relationships " + graphRelations.size());

        write(NODES_CONFIG,graphEntities);
        write(RELATIONSHIP_CONFIG,graphRelations);
    }

    private static void write(String nodesConfig, List<? extends GraphNode> graphEntities) throws Exception {
        File file = new File(nodesConfig);
        file.createNewFile();
        FileWriter writer = new FileWriter(file);
        try {
            for(GraphNode graphEntity : graphEntities) {
                writer.write(graphEntity.serialize()+"\n");
            }
        }finally{
            if(writer != null)
                writer.close();
        }
    }

    private static void persistToGraph() {
        String dbLocation = DB_LOCATION;

        String nodesConfigLocation = NODES_CONFIG;
        String relationshipConfigLocation = RELATIONSHIP_CONFIG;

        GraphDBWriter dbWriter = new Neo4JGraphDBWriter();
        try {
            dbWriter.init(dbLocation,true);
            List<GraphNode> nodes = GraphUtil.parseNodes(nodesConfigLocation);
            writeToGraph(nodes, dbWriter);

            List<GraphNode> relationships = GraphUtil.parseNodes(relationshipConfigLocation);
            writeToGraph(relationships, dbWriter);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbWriter.close();
        }
    }

    private static void writeToGraph(List<GraphNode> nodes, GraphDBWriter dbWriter) {
        for (GraphNode node : nodes) {
            dbWriter.persistGraphEntity(node);
        }
    }
}

package com.integral.ds.core.impl;

import com.integral.ds.bean.GraphEntity;
import com.integral.ds.bean.GraphNode;
import com.integral.ds.bean.GraphRelation;
import com.integral.ds.core.EntityType;
import com.integral.ds.core.GraphDBWriter;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.unsafe.batchinsert.BatchInserter;
import org.neo4j.unsafe.batchinsert.BatchInserters;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Rahul Bhattacharjee
 */
public class Neo4JGraphDBWriter implements GraphDBWriter {

    private BatchInserter inserter;
    private boolean tolerance;
    private Map<String,Long> labelToIdMapping = new HashMap<String,Long>();

    @Override
    public void init(String dbLocation,boolean tolerance) {
        this.tolerance = tolerance;
        inserter = BatchInserters.inserter(dbLocation);
    }

    @Override
    public void persistGraphEntity(GraphNode node) {
        EntityType entityType = node.getEntityType();
        switch(entityType) {
            case NODE: persistNode((GraphEntity)node);
                       break;
            case RELATIONSHIP: persistRelationship((GraphRelation)node);
                               break;
        }
    }

    private void persistRelationship(GraphRelation node) {
        String name = node.getEntityName();
        RelationshipType relationshipType = DynamicRelationshipType.withName(name);
        String primaryValue = node.getPrimaryEntityValue();
        String secondaryValue = node.getSecondaryEntityValue();

        Long idForPrimary = labelToIdMapping.get(primaryValue);
        Long idForSecondary = labelToIdMapping.get(secondaryValue);

        if(idForPrimary == null || idForSecondary == null) {
            System.out.print("Either one of the endpoints of relationship is not available in the graph database.");
            if(tolerance) {
                return;
            }else {
                throw new IllegalArgumentException("One of the end points of relationship is not available in the graph.");
            }
        }

        Map<String,String> attributes = node.getAttributes();
        inserter.createRelationship(idForPrimary,idForSecondary,relationshipType,(Map)attributes);
    }

    private void persistNode(GraphEntity node) {
        String name = node.getEntityName();
        Label entityLabel = DynamicLabel.label(name);
        String primaryValue = node.getPrimaryEntityValue();

        Map<String,String> attributes = node.getAttributes();
        //attributes.put("ID",primaryValue);
        if(!labelToIdMapping.containsKey(primaryValue)) {
            long id = inserter.createNode((Map)attributes,entityLabel);
            labelToIdMapping.put(primaryValue,id);
        }
    }

    @Override
    public void close() {
        showCreatedRelationships();
        if(inserter != null) {
            inserter.shutdown();
        }
    }

    private void showCreatedRelationships() {
        System.out.println("Show node list");
        for(Map.Entry<String,Long> entry : labelToIdMapping.entrySet()){
            System.out.println(entry.getKey() + " mapped to " + entry.getValue());
        }
    }
}

package com.integral.ds.core;

import com.integral.ds.bean.GraphNode;

/**
 * @author Rahul Bhattacharjee
 */
public interface GraphDBWriter {

    public void init(String dbLocation,boolean tolerance);

    public void persistGraphEntity(GraphNode node);

    public void close();
}

package com.integral.ds.core;

/**
 * @author Rahul Bhattacharjee
 */
public enum EntityType {
    NODE, RELATIONSHIP
}

package com.integral.ds.bean;

import com.integral.ds.core.EntityType;
import com.integral.ds.util.GraphUtil;

/**
 * @author Rahul Bhattacharjee
 */
public class GraphRelation extends GraphEntity implements GraphNode {

    private String secondaryEntityValue;

    @Override
    public EntityType getEntityType() {
        return EntityType.RELATIONSHIP;
    }

    public String getSecondaryEntityValue() {
        return secondaryEntityValue;
    }

    public void setSecondaryEntityValue(String secondaryEntityValue) {
        this.secondaryEntityValue = secondaryEntityValue;
    }

    @Override
    public String serialize() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(EntityType.RELATIONSHIP.name() + ",");
        stringBuilder.append(getEntityName() + ",");
        stringBuilder.append(getPrimaryEntityValue() + ",");
        stringBuilder.append(getSecondaryEntityValue() + ",");
        stringBuilder.append(GraphUtil.serAttributeMap(getAttributes()));
        return stringBuilder.toString();
    }
}

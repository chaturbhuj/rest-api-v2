package com.integral.ds.bean;

import com.integral.ds.core.EntityType;

/**
 * @author Rahul Bhattacharjee
 */
public interface GraphNode {

    public EntityType getEntityType();

    public String serialize();
}

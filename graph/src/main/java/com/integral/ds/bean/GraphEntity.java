package com.integral.ds.bean;

import com.integral.ds.core.EntityType;
import com.integral.ds.util.GraphUtil;

import java.util.Map;

/**
 * Graph entity.
 *
 * @author Rahul Bhattacharjee
 */
public class GraphEntity implements GraphNode {

    private String entityName;
    private String primaryEntityValue;
    private Map<String, String> attribute;

    @Override
    public EntityType getEntityType() {
        return EntityType.NODE;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getPrimaryEntityValue() {
        return primaryEntityValue;
    }

    public void setPrimaryEntityValue(String primaryEntityValue) {
        this.primaryEntityValue = primaryEntityValue;
    }

    public Map<String, String> getAttributes() {
        return attribute;
    }

    public void setAttributes(Map<String, String> attribute) {
        this.attribute = attribute;
    }

    @Override
    public String serialize() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(EntityType.NODE.name() + ",");
        stringBuilder.append(getEntityName() + ",");
        stringBuilder.append(getPrimaryEntityValue() + ",");
        stringBuilder.append(GraphUtil.serAttributeMap(getAttributes()));
        return stringBuilder.toString();
    }
}

package com.integral.ds.bean;

/**
 *
 * @author Rahul Bhattacharjee
 */
public class TradeObject {

    private String orderId;
    private String tradeId;
    private String org;
    private String coverTradeId;
    private String cptyBOrg;

    public TradeObject(String orderId, String tradeId, String org, String coverTradeId, String cptyBOrg) {
        this.orderId = orderId;
        this.tradeId = tradeId;
        this.org = org;
        this.coverTradeId = coverTradeId;
        this.cptyBOrg = cptyBOrg;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getCoverTradeId() {
        return coverTradeId;
    }

    public void setCoverTradeId(String coverTradeId) {
        this.coverTradeId = coverTradeId;
    }

    public String getCptyBOrg() {
        return cptyBOrg;
    }

    public void setCptyBOrg(String cptyBOrg) {
        this.cptyBOrg = cptyBOrg;
    }

    @Override
    public String toString() {
        return "TradeObject{" +
                "orderId='" + orderId + '\'' +
                ", tradeId='" + tradeId + '\'' +
                ", org='" + org + '\'' +
                ", coverTradeId='" + coverTradeId + '\'' +
                ", cptyBOrg='" + cptyBOrg + '\'' +
                '}';
    }
}

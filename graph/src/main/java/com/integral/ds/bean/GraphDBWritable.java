package com.integral.ds.bean;

import com.integral.ds.core.GraphDBWriter;

/**
 * @author Rahul Bhattacharjee
 */
public interface GraphDBWritable {

    long write(GraphDBWriter graphWriter);
}

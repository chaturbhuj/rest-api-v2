package com.integral.ds.util;

import com.integral.ds.bean.GraphEntity;
import com.integral.ds.bean.GraphNode;
import com.integral.ds.bean.GraphRelation;
import com.integral.ds.bean.TradeObject;
import com.integral.ds.core.EntityType;
import org.apache.commons.lang.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

import static org.apache.commons.io.IOUtils.closeQuietly;
import static org.apache.commons.io.IOUtils.toString;

/**
 * @author Rahul Bhattacharjee
 */
public class GraphUtil {

    private static final String KEY_VALUE_SEP = "~";
    private static final String PAIR_SEP = "|";
    private static final String PAIR_SPLIT_SEP = "\\|";

    public static List<GraphNode> parseNodes(String nodesConfigLocation) {
        List<GraphNode> result = new ArrayList<GraphNode>();

        FileReader fileReader = null;
        BufferedReader reader = null;

        try {
            File file = new File(nodesConfigLocation);
            fileReader = new FileReader(file);
            reader = new BufferedReader(fileReader);

            String line = reader.readLine();
            while (line != null) {
                if (StringUtils.isNotBlank(line)) {
                    result.add(getGraphNodeFromLine(line));
                }
                line = reader.readLine();
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Exception while parsing input file.", e);
        } finally {
            closeQuietly(fileReader);
            closeQuietly(reader);
        }
        return result;
    }

    //Format : (1)type,entity_name,entity_value,{serialized_attributes}
    //         (2)type,entity_name,entity_value_1,entity_value_2,{serialized_attributes}
    public static GraphNode getGraphNodeFromLine(String serializedNode) {

        System.out.println("CONTENT " + serializedNode);

        GraphNode result = null;
        String[] splits = serializedNode.split(",");
        String type = splits[0];
        EntityType nodeType = EntityType.valueOf(type);

        switch (nodeType) {
            case NODE:
                result = parseEntityNode(splits);
                break;
            case RELATIONSHIP:
                result = parseRelationship(splits);
                break;
        }
        return result;
    }

    private static GraphNode parseRelationship(String[] splits) {
        String entityName = splits[1];
        String entityValue1 = splits[2];
        String entityValue2 = splits[3];
        GraphRelation relationship = new GraphRelation();
        relationship.setEntityName(entityName);
        relationship.setPrimaryEntityValue(entityValue1);
        relationship.setSecondaryEntityValue(entityValue2);

        if(splits.length == 5) {
            String entityAttributes = splits[4];
            Map<String, String> attributes = deserAttributeMap(entityAttributes);
            relationship.setAttributes(attributes);
        }
        return relationship;
    }

    private static GraphNode parseEntityNode(String[] splits) {
        GraphEntity entity = new GraphEntity();
        String entityName = splits[1];
        String entityValue = splits[2];
        entity.setEntityName(entityName);
        entity.setPrimaryEntityValue(entityValue);

        if(splits.length == 4) {
            String serializedAttribute = splits[3];
            Map<String, String> attributes = deserAttributeMap(serializedAttribute);
            entity.setAttributes(attributes);
        }
        return entity;
    }

    public static Map<String, String> deserAttributeMap(String serializedAttribute) {
        String[] splits = serializedAttribute.split(PAIR_SPLIT_SEP);
        Map<String, String> attribute = new HashMap<String, String>();
        for (String split : splits) {
            if (StringUtils.isNotBlank(split)) {
                String[] keyval = split.split(KEY_VALUE_SEP);
                if(keyval.length == 2) {
                    attribute.put(keyval[0], keyval[1]);
                }
            }
        }
        return attribute;
    }

    public static String serAttributeMap(Map<String, String> attributes) {
        if(attributes == null) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        Iterator<Map.Entry<String, String>> iterator = attributes.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            String key = entry.getKey();
            String value = entry.getValue();
            stringBuilder.append(key + KEY_VALUE_SEP + value + PAIR_SEP);
        }
        return stringBuilder.toString();
    }

    public static Map<String, Object> normalizeAttributeMap(Map<String, String> attributes) {
        Map<String,Object> result = new HashMap<String,Object>();
        Iterator<Map.Entry<String,String>> iterator = attributes.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry<String,String> entry = iterator.next();
            result.put(entry.getKey(),entry.getValue());
        }
        return result;
    }

    public static void addNodesAndRelationships(TradeObject tradeObject, List<GraphEntity> graphEntities, List<GraphRelation> graphRelations) {
        String tradeId = tradeObject.getTradeId();
        String orderId = tradeObject.getOrderId();
        String org = tradeObject.getOrg();
        String coverTradeId = tradeObject.getCoverTradeId();
        String cptyBOrg = tradeObject.getCptyBOrg();

        if(StringUtils.isNotBlank(tradeId)) {
            GraphEntity entity = new GraphEntity();
            entity.setEntityName("TRADE");
            entity.setPrimaryEntityValue(tradeId);
            graphEntities.add(entity);
        }

        if(StringUtils.isNotBlank(orderId)) {
            GraphEntity entity = new GraphEntity();
            entity.setEntityName("ORDER");
            entity.setPrimaryEntityValue(orderId);
            graphEntities.add(entity);
        }

        if(StringUtils.isNotBlank(org)) {
            GraphEntity entity = new GraphEntity();
            entity.setPrimaryEntityValue(org);
            entity.setEntityName("ORGANIZATION");
            graphEntities.add(entity);
        }

        if(StringUtils.isBlank(coverTradeId)) {
            GraphRelation relation = new GraphRelation();
            relation.setEntityName("ORIG_ORGANIZATION");
            if(StringUtils.isNotBlank(orderId) && StringUtils.isNotBlank(org)) {
                relation.setPrimaryEntityValue(orderId);
                relation.setSecondaryEntityValue(org);
                graphRelations.add(relation);
            }
        }

        if(StringUtils.isNotBlank(tradeId) && StringUtils.isNotBlank(orderId)) {
            GraphRelation relation = new GraphRelation();
            relation.setEntityName("HAS_TRADE");
            relation.setPrimaryEntityValue(orderId);
            relation.setSecondaryEntityValue(tradeId);
            graphRelations.add(relation);
        }

        if(StringUtils.isNotBlank(tradeId) && StringUtils.isNotBlank(cptyBOrg)) {
            GraphRelation relation = new GraphRelation();
            relation.setEntityName("MAKER_ORGANIZATION");
            relation.setPrimaryEntityValue(tradeId);
            relation.setSecondaryEntityValue(cptyBOrg);
            graphRelations.add(relation);
        }

        if(StringUtils.isNotBlank(org) && StringUtils.isNotBlank(orderId)) {
            GraphRelation relation = new GraphRelation();
            relation.setEntityName("HAS_PLACED_ORDER");
            relation.setPrimaryEntityValue(org);
            relation.setSecondaryEntityValue(orderId);
            graphRelations.add(relation);
        }
    }
}

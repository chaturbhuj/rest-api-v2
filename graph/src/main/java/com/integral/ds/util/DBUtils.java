package com.integral.ds.util;

import org.apache.commons.dbcp.BasicDataSource;

import java.sql.SQLException;

/**
 * @author Rahul Bhattacharjee
 */
public class DBUtils {

    private final static String url = "jdbc:postgresql://eastot.chgyeswrwlad.us-east-1.rds.amazonaws.com:5432/eastOT";
    private final static String userName = "emscope";
    private final static String password = "Integral4309";


    public static BasicDataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl(url);
        dataSource.setUsername(userName);
        dataSource.setPassword(password);
        return dataSource;
    }

    public static void close(BasicDataSource dataSource) {
        if(dataSource != null) {
            try {
                dataSource.close();
            } catch (SQLException e) {
                // ok to eat.
            }
        }
    }
}

// javascript file for js functions for the application.

var hiddenField = document.getElementsByName("landing_url");
var pathValue = gup('path');

hiddenField[0].value = pathValue;

function gup( name ) {
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results == null ) {
        return null;
    } else {
        return results[1];
    }
}

function focusUsername() {
	var username = document.getElementsByName("username")[0];
	username.focus();
}

function focusCustomerName() {
    var customerName = document.getElementsByName("customer")[0];
	customerName.focus();
}
